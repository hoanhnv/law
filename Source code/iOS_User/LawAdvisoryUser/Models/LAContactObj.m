//
//  LAContactObj.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAContactObj.h"
NSString *const kLAContactID = @"id";
NSString *const kLAContactName = @"name";
NSString *const kLAContactDescription = @"description";
NSString *const kLAContactDeleteAt = @"deleted_at";
NSString *const kLAContactUpdateAt = @"updated_at";
NSString *const kLAContactCreateAt = @"created_at";
@implementation LAContactObj
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.contactID = [dict integerForKey:kLAContactID];
        self.contactName = [dict stringForKey:kLAContactName];
        self.contactDescription = [dict stringForKey:kLAContactDescription];
        self.contactCreateAt = [dict doubleForKey:kLAContactCreateAt];
        self.contactUpdateAt = [dict doubleForKey:kLAContactUpdateAt];
        self.contactDeleteAt = [dict doubleForKey:kLAContactDeleteAt];
    }
    
    return self;
    
}
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end

//
//  LAAccountObj.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LAAccountObj : NSObject
@property (nonatomic) NSString* accountEmail;
@property (nonatomic) NSString* accountPassword;
@property (nonatomic) NSString* accountName;
@property (nonatomic) NSString* accountCellphone;
@property (nonatomic) NSString* accountPhone;
@property (nonatomic) NSString* accountAddress;
@property (nonatomic) NSString* accountNumber;
@property (nonatomic) NSString* accountComplement;
@property (nonatomic) NSString* accountNeighborhood;
@property (nonatomic) NSString* accountCity;
@property (nonatomic) NSString* accountUF;
@property (nonatomic) double accountDateOfBirth;
@property (nonatomic) BOOL accountAcceptNew;
@property (nonatomic) NSString* avataURL;
@end

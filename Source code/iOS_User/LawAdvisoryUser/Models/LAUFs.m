//
//  LAUFs.m
//
//  Created by   on 9/23/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAUFs.h"


NSString *const kLAUFsSuccess = @"success";
NSString *const kLAUFsData = @"data";
NSString *const kLAUFsMessage = @"message";


@interface LAUFs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAUFs

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.success = [[self objectOrNilForKey:kLAUFsSuccess fromDictionary:dict] boolValue];
            self.data = [self objectOrNilForKey:kLAUFsData fromDictionary:dict];
            self.message = [self objectOrNilForKey:kLAUFsMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAUFsSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLAUFsData];
    [mutableDict setValue:self.message forKey:kLAUFsMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.success = [aDecoder decodeBoolForKey:kLAUFsSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAUFsData];
    self.message = [aDecoder decodeObjectForKey:kLAUFsMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeBool:_success forKey:kLAUFsSuccess];
    [aCoder encodeObject:_data forKey:kLAUFsData];
    [aCoder encodeObject:_message forKey:kLAUFsMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAUFs *copy = [[LAUFs alloc] init];
    
    if (copy) {

        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LAContactSubjectObj.h
//
//  Created by   on 9/23/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LAContactSubjectObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *dataDescription;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

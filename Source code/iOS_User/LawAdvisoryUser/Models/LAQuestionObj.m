//
//  LAQuestionObj.m
//
//  Created by   on 9/21/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAQuestionObj.h"


NSString *const kLAQuestionObjDeletedAt = @"deleted_at";
NSString *const kLAQuestionObjId = @"id";
NSString *const kLAQuestionObjAnswer = @"answer";
NSString *const kLAQuestionObjUpdatedAt = @"updated_at";
NSString *const kLAQuestionObjQuestion = @"question";
NSString *const kLAQuestionObjCreatedAt = @"created_at";


@interface LAQuestionObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAQuestionObj

@synthesize deletedAt = _deletedAt;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize answer = _answer;
@synthesize updatedAt = _updatedAt;
@synthesize question = _question;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [self objectOrNilForKey:kLAQuestionObjDeletedAt fromDictionary:dict];
        self.dataIdentifier = [[self objectOrNilForKey:kLAQuestionObjId fromDictionary:dict] doubleValue];
        self.answer = [self objectOrNilForKey:kLAQuestionObjAnswer fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLAQuestionObjUpdatedAt fromDictionary:dict] doubleValue];
        self.question = [self objectOrNilForKey:kLAQuestionObjQuestion fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAQuestionObjCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLAQuestionObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLAQuestionObjId];
    [mutableDict setValue:self.answer forKey:kLAQuestionObjAnswer];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAQuestionObjUpdatedAt];
    [mutableDict setValue:self.question forKey:kLAQuestionObjQuestion];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAQuestionObjCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.deletedAt = [aDecoder decodeObjectForKey:kLAQuestionObjDeletedAt];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAQuestionObjId];
    self.answer = [aDecoder decodeObjectForKey:kLAQuestionObjAnswer];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAQuestionObjUpdatedAt];
    self.question = [aDecoder decodeObjectForKey:kLAQuestionObjQuestion];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAQuestionObjCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_deletedAt forKey:kLAQuestionObjDeletedAt];
    [aCoder encodeDouble:_dataIdentifier forKey:kLAQuestionObjId];
    [aCoder encodeObject:_answer forKey:kLAQuestionObjAnswer];
    [aCoder encodeDouble:_updatedAt forKey:kLAQuestionObjUpdatedAt];
    [aCoder encodeObject:_question forKey:kLAQuestionObjQuestion];
    [aCoder encodeDouble:_createdAt forKey:kLAQuestionObjCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAQuestionObj *copy = [[LAQuestionObj alloc] init];
    
    if (copy) {
        
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.answer = [self.answer copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.question = [self.question copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

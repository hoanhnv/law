//
//  LAListFaqs.m
//
//  Created by   on 9/21/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAListFaqs.h"
#import "LAQuestionObj.h"


NSString *const kLAListFaqsSuccess = @"success";
NSString *const kLAListFaqsData = @"data";
NSString *const kLAListFaqsMessage = @"message";


@interface LAListFaqs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAListFaqs

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAListFaqsSuccess fromDictionary:dict] boolValue];
        NSObject *receivedLAQuestionObj = [dict objectForKey:kLAListFaqsData];
        NSMutableArray *parsedLAQuestionObj = [NSMutableArray array];
        if ([receivedLAQuestionObj isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAQuestionObj) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAQuestionObj addObject:[LAQuestionObj modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAQuestionObj isKindOfClass:[NSDictionary class]]) {
            [parsedLAQuestionObj addObject:[LAQuestionObj modelObjectWithDictionary:(NSDictionary *)receivedLAQuestionObj]];
        }
        
        self.data = [NSArray arrayWithArray:parsedLAQuestionObj];
        self.message = [self objectOrNilForKey:kLAListFaqsMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAListFaqsSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLAListFaqsData];
    [mutableDict setValue:self.message forKey:kLAListFaqsMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAListFaqsSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAListFaqsData];
    self.message = [aDecoder decodeObjectForKey:kLAListFaqsMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAListFaqsSuccess];
    [aCoder encodeObject:_data forKey:kLAListFaqsData];
    [aCoder encodeObject:_message forKey:kLAListFaqsMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAListFaqs *copy = [[LAListFaqs alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}

@end

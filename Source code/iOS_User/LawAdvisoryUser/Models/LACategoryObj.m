//
//  LACategoryObj.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACategoryObj.h"
#import "NSDictionary+Law.h"
NSString *const kLACatID = @"id";
NSString *const kLACatName = @"name";
NSString *const kLACatDescription = @"description";
NSString *const kLACatDeleteAt = @"deleted_at";
NSString *const kLACatUpdateAt = @"updated_at";
NSString *const kLACatCreateAt = @"created_at";
@implementation LACategoryObj
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.catID = [dict integerForKey:kLACatID];
        self.catName = [dict stringForKey:kLACatName];
        self.catDescription = [dict stringForKey:kLACatDescription];
        self.catDeleteAt = [dict doubleForKey:kLACatDescription];
        self.catUpdateAt = [dict doubleForKey:kLACatDescription];
        self.catCreatedAt = [dict doubleForKey:kLACatDescription];
    }
    
    return self;
    
}
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end

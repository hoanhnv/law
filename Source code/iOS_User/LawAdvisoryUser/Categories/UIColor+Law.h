//
//  UIColor+Law.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/23/16.
//  Copyright © 2016 HOANHNV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Law)
+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str;
- (UIColor *)colorWithHexString:(NSString *)str_HEX  alpha:(CGFloat)alpha_range;
@end

//
//  LAAnswerForgotPass.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAnswerForgotPass.h"

@implementation LAAnswerForgotPass

-(void)viewDidLoad{
    [super viewDidLoad];
    if (self.bSuccess) {
        self.lbMessage.text = MSG_005;
    }
    else{
        self.lbMessage.text = [NSString stringWithFormat:MSG_006,self.strEmail];
    }
    self.lbMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.lbMessage.numberOfLines = 0;
    
    self.btnClose.titleLabel.text =  [self.btnClose.titleLabel.text uppercaseString];
    self.btnClose.titleLabel.textColor = [LAUtilitys jusTapColor];
}

- (IBAction)doClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.parentViewController.navigationController popViewControllerAnimated:YES];
    }];
}

@end

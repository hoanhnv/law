//
//  LAForgotPassViewController.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAForgotPassViewController.h"
#import "MBProgressHUD.h"
#import "LAAccountService.h"
#import "LAAnswerForgotPass.h"

@interface LAForgotPassViewController ()

@end

@implementation LAForgotPassViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShouldBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initUI
{
    self.title = @"Esqueci minha shenha";
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (BOOL)checkValid
{
    BOOL check = YES;
    if ([LAUtilitys isEmptyOrNull:self.txtEmail.text])
    {
        [self showMessage:NSLocalizedString(@"EMPTY_INFO", "@") withPoistion:nil];
        return NO;
    }
    //    else
    //        if ([LAUtilitys validateEmailWithString:self.txtEmail.text])
    //        {
    //            [self showMessage:NSLocalizedString(@"INVALID_EMAIL", "@") withPoistion:nil];
    //            return NO;
    //        }
    return check;
}
- (IBAction)onForgotPassSendEmailPressed:(id)sender {
    [self.view endEditing:YES];
    if ([self checkValid])
    {
        [self.view endEditing:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [LAAccountService resetPassWithEmail:self.txtEmail.text wCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                NSString* message = [data objectForKey:KEY_RESPONSE_MESSAGE];
                
                LAAnswerForgotPass *answerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LAAnswerForgotPass"];
                answerVC.bSuccess = success;
                answerVC.strEmail = self.txtEmail.text;
                answerVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
                [self presentViewController:answerVC animated:YES completion:^{
                    
                }];
            }
        }];
    }
}

@end

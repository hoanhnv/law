//
//  LACategoriesViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "iCarousel.h"
#import "LACategoriesCollectionViewCell.h"


@interface LACategoriesViewController : LABaseViewController<iCarouselDataSource, iCarouselDelegate>
{
    NSMutableArray* arrBannerObj;
    NSMutableArray* arrCategories;
    NSMutableArray* arrSlider;
    UIBarButtonItem* btSearchBarItem;
}
@property (nonatomic) IBOutlet iCarousel* bannerView;
@property (nonatomic) IBOutlet UITableView* tbCategories;
@property (nonatomic) IBOutlet UIView* viewFullData;
@property (nonatomic) IBOutlet UIView* viewNavigation;
@property (nonatomic) IBOutlet UITextField* txtSearch;
- (IBAction)onViewNavigationTap:(id)sender;
@end

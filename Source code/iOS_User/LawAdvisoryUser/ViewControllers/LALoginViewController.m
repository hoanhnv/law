//
//  LALoginViewController.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LALoginViewController.h"
#import "LAForgotPassViewController.h"
#import "LARegisterViewController.h"
#import "LAFaleViewController.h"
#import "MBProgressHUD.h"
#import "LAAccountService.h"
#import "LAFrequentesViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>
#import <Twitter/Twitter.h>
#import "AppDelegate.h"

static NSString * const kGooglePlus_ClientId = @"759901828094-u9ck4bouh1sdfkakqcafq5gldmcaqnvs.apps.googleusercontent.com";

@interface LALoginViewController ()

@end

@implementation LALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}
- (void)initUI
{
    [LAUtilitys setButtonTextUnderLine:self.btFogotPass];
    [LAUtilitys setButtonTextUnderLine:self.btDuvidas];
    [LAUtilitys setButtonTextUnderLine:self.btFale];
}
- (void)initData
{
    
}
- (IBAction)onForgotPassPressed:(id)sender {
    LAForgotPassViewController* forgotPassVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAForgotPassViewController class])];
    forgotPassVC.isShouldBackButton = YES;
    forgotPassVC.isTransparentNavigation = YES;
    [self.navigationController pushViewController:forgotPassVC animated:YES];
}
- (IBAction)onFacebookPressed:(id)sender {
    FBSDKLoginManager *loginFace = [[FBSDKLoginManager alloc] init];
    loginFace.loginBehavior = FBSDKLoginBehaviorWeb;
    [loginFace logInWithReadPermissions:@[@"public_profile",@"email",@"contact_email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        LAAccountObj* accountObj = [LAAccountObj new];
        if (result.isCancelled)
        {
            [self showMessage:error.localizedFailureReason withPoistion:nil];
        }
        else
            if (error == nil) {
                if ([FBSDKAccessToken currentAccessToken]) {
                    if ([result.grantedPermissions containsObject:@"email"])
                    {
                        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                        [parameters setValue:@"id,name,email,birthday" forKey:@"fields"];
                        FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:parameters];
                        
                        FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
                        
                        [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id resultHere, NSError *error) {
                            
                            if(resultHere)
                            {
                                if ([resultHere objectForKey:@"email"]) {
                                    
                                    accountObj.accountEmail = [resultHere objectForKey:@"email"];
                                }
                                if ([resultHere objectForKey:@"first_name"]) {
                                    accountObj.accountName = [resultHere objectForKey:@"first_name"];
                                    
                                }
                                if ([resultHere objectForKey:@"birthday"])
                                {
                                    accountObj.accountDateOfBirth = [LAUtilitys getNSDateFromDateString:[resultHere objectForKey:@"birthday"]].timeIntervalSince1970;
                                }
                            }
                            [self goToRegisterViewController:accountObj];
                        }];
                        [connection start];
                    }
                    else
                    {
                        [self goToRegisterViewController:nil];
                    }
                }
                else{
                    // TODO
                    [self goToRegisterViewController:nil];
                }
                [LADataCenter shareInstance].facebookToken = [FBSDKAccessToken currentAccessToken].tokenString;
                
            }
            else{
                [self showMessage:error.localizedFailureReason withPoistion:nil];
            }
    }];
}

- (IBAction)onTwitterPressed:(id)sender {
    if([Twitter sharedInstance].sessionStore.session != nil){
        [LADataCenter shareInstance].twitterID = [Twitter sharedInstance].sessionStore.session.userID;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self goLoginViewFromTwitter:[Twitter sharedInstance].sessionStore.session.userID];
    }
    else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (session) {
                [LADataCenter shareInstance].twitterID = session.userID;
                [self goLoginViewFromTwitter:session.userID];
            } else {
                [self showMessage:[error localizedDescription] withPoistion:nil];
            }
        }];
    }
}

- (IBAction)onGPlusPressed:(id)sender {
    GIDGoogleUser *currentUser = [[GIDSignIn sharedInstance] currentUser];
    if(currentUser != nil){
        LAAccountObj* objAccount = [LAAccountObj new];
        objAccount.accountEmail = currentUser.profile.email;
        objAccount.accountName = currentUser.profile.name;
        objAccount.avataURL = [[currentUser.profile imageURLWithDimension:100]absoluteString];
        [self goToRegisterViewController:objAccount];
    }
    else{
        [[GIDSignIn sharedInstance] signIn];
    }
}
- (BOOL)checkValid
{
    BOOL check = YES;
    if ([LAUtilitys isEmptyOrNull:self.txtPassword.text] || [LAUtilitys isEmptyOrNull:self.txtEmail.text])
    {
        [self showMessage:NSLocalizedString(@"EMPTY_INFO", "@") withPoistion:nil];
        return NO;
    }
    return check;
}
- (IBAction)onLoginPressed:(id)sender {
    if ([self checkValid])
    {
        [self.view endEditing:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [LAAccountService loginWithEmail:self.txtEmail.text andPassword:self.txtPassword.text wCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                NSString* message = [data objectForKey:KEY_RESPONSE_MESSAGE];
                if (success)
                {
                    NSString* token = data[KEY_RESPONSE_DATA][KEY_RESPONSE_TOKEN];
                    if (![LAUtilitys NullOrEmpty:token]) {
                        [[LADataCenter shareInstance] saveToken:token error:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KEY_CHANGE_ACCOUNT object:nil];
                        [self gotoHome];
                    }
                    else{                        
                        [self showMessage:MSG_0001 withPoistion:nil];
                    }
                }
                else
                {
                    [self showMessage:message withPoistion:nil];
                }
            }
        }];
    }
}
- (void)goLoginViewFromTwitter:(NSString *)userID
{
    NSString *urlRequest = @"https://api.twitter.com/1.1/users/show.json";
    NSDictionary *params = @{@"user_id": userID};
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
    NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                     URL:urlRequest
                                              parameters:params
                                                   error:nil];
    
    [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSDictionary *resData = [NSJSONSerialization JSONObjectWithData: data options:kNilOptions error:nil];
        LAAccountObj* objAccount = [LAAccountObj new];
        objAccount.accountEmail = resData[@"screen_name"];
        objAccount.accountCity = resData[@"location"];
        objAccount.accountName = resData[@"name"];
        objAccount.avataURL = resData[@"profile_image_url"];
        NSLog(@"Twitter info :%@",resData);
        [self goToRegisterViewController:objAccount];
    }];
}

- (IBAction)onRegisterPressed:(id)sender {
    LARegisterViewController* registerVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARegisterViewController class])];
    registerVC.isShouldBackButton = YES;
    registerVC.isTransparentNavigation = YES;
    [self.navigationController pushViewController:registerVC animated:YES];
}
-(void)goToRegisterViewController:(id)dataInfo{
    LARegisterViewController* registerVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARegisterViewController class])];
    registerVC.isShouldBackButton = YES;
    registerVC.isTransparentNavigation = YES;
    registerVC.currentAccount = dataInfo;
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (IBAction)onDuvidasPressed:(id)sender {
    LAFrequentesViewController* duvidasVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAFrequentesViewController class])];
    duvidasVC.isShouldBackButton = YES;
    duvidasVC.isShouldShowBottombar = YES;
    duvidasVC.isShowGreenNavigation = NO;
    duvidasVC.isShouldShowBigHeightNavigationBar = NO;
    [self.navigationController pushViewController:duvidasVC animated:YES];
}

- (IBAction)onFalePressed:(id)sender {
    LAFaleViewController* faleVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAFaleViewController class])];
    [self.navigationController pushViewController:faleVC animated:YES];
}

#pragma mark - GIDSignInDelegate
// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    if (error == nil) {
        [LADataCenter shareInstance].googlePlusToken = user.userID;
        LAAccountObj* objAccount = [LAAccountObj new];
        objAccount.accountEmail = user.profile.email;
        objAccount.accountName = user.profile.name;
        objAccount.avataURL = [[user.profile imageURLWithDimension:100]absoluteString];
        [self goToRegisterViewController:objAccount];
    }
    else{
        // TODO
    }
}

-(void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error{
    [self showMessage:MSG_0002 withPoistion:nil];
}

@end

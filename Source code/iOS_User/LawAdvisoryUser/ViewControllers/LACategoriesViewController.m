//
//  LACategoriesViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACategoriesViewController.h"
#import "LACategoriesTableViewCell.h"
#import "LACategoriesHeaderView.h"
#import "LAPageViewControl.h"
#import "LASubCatViewController.h"
#import "LACategoryObj.h"
#import "LAAddressService.h"
#define HEIGHT_NAVIGATION_CUSTOM_FULL 160.0f
#define HEIGH_HEADER_SECTION_CATEGORIES 50.0f
#define HEIGH_CELL_CATEGORIES 60.0f
#define COLOR_BACKGOUND_SEARCH_FIELD [UIColor colorWithRed:149.0/255.0 green:216.0/255.0 blue:253.0/255.0 alpha:1.0]
@interface LACategoriesViewController ()<UINavigationBarDelegate,UITableViewDelegate,UITableViewDataSource>
{
    BOOL isShowSearch;
}
@property (nonatomic) IBOutlet LAPageViewControl* pageControl;
@end

@implementation LACategoriesViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = YES;
        self.isShouldShowBottombar  = YES;
        isShowSearch = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}
- (void)initRightButton
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
    return UIBarPositionBottom;
}
- (void)initUI
{
    UIButton* btRight = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [btRight setImage:[UIImage imageNamed:@"ic_searchBar"] forState:UIControlStateNormal];
    [btRight addTarget:self action:@selector(onRightButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    btRight.transform = CGAffineTransformMakeTranslation(0, 0);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 90)];
    [backButtonView addSubview:btRight];
    btSearchBarItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.rightBarButtonItem = btSearchBarItem;
    [self.txtSearch setBackgroundColor:COLOR_BACKGOUND_SEARCH_FIELD];
#warning FIX NUMBER PAGE
    self.pageControl.iNumberPage = 3;
//    self.pageControl.iNumberPage = arrSlider.count;
    [self.pageControl setUpImageActive:@"ic_scrubactive_category" andInActive:@"ic_scrubinactive_category"];
}

-(void) hideAndDisableRightNavigationItem
{
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    self.navigationItem.rightBarButtonItem = nil;
}

-(void) showAndEnableRightNavigationItem
{
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor blackColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    self.navigationItem.rightBarButtonItem = btSearchBarItem;
}
- (IBAction)onRightButtonPressed:(id)sender
{
    isShowSearch = !isShowSearch;
    CGFloat height = HEIGHT_BIG_NAVIGATION;
    if (isShowSearch)
    {
        height = 140.0f;
    }
    [self.navigationController.navigationBar setHeight:height];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if (isShowSearch)
    {
        [self.txtSearch setCenter:CGPointMake(self.navigationController.navigationBar.center.x, self.navigationController.navigationBar.center.y - 60)];
        [self.navigationController.navigationBar addSubview:self.txtSearch];
        [LAUtilitys setRightImage:@"ic_searchBar" forTextField:self.txtSearch];
        [self hideAndDisableRightNavigationItem];
    }
    else
    {
        if ([self.txtSearch superview])
        {
            [self.txtSearch removeFromSuperview];
        }
        [self showAndEnableRightNavigationItem];
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGFloat height = 90.0f;
    if (isShowSearch)
    {
        height = 160.0f;
    }
    [self.navigationController.navigationBar setHeight:height];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (void)initData
{
    [self.tbCategories registerNib:[UINib nibWithNibName:NSStringFromClass([LACategoriesTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LACategoriesTableViewCell class])];
    [self.tbCategories reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return arrBannerObj.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view
{
    return nil;
}
#pragma mark UITABLEVIEW DATASOURCE
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    LACategoriesHeaderView *view = (LACategoriesHeaderView*)[[[NSBundle mainBundle]
                     loadNibNamed:@"LACategoriesHeaderView"
                     owner:self options:nil]
                    firstObject];
    CGRect rect = self.tbCategories.frame;
    rect.size.height = HEIGH_HEADER_SECTION_CATEGORIES;
    rect.size.width = CGRectGetWidth(self.tbCategories.frame);
    [view setFrame:rect];
    switch (section) {
        case 0:
        {
            [view.imgIconHeader setImage:[UIImage imageNamed:@"ic_viagens"]];
            [view.txtTitle setTextColor:COLOR_CATEGORY_HEADER_TITLE_1];
            view.txtTitle.text = @"Viagens";
        }
            break;
        case 1:
        {
            [view.imgIconHeader setImage:[UIImage imageNamed:@"ic_saude"]];
            [view.txtTitle setTextColor:COLOR_CATEGORY_HEADER_TITLE_2];
            view.txtTitle.text = @"Saúde";
        }
            break;
        default:
            break;
    }
    
    return view;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGH_CELL_CATEGORIES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEIGH_HEADER_SECTION_CATEGORIES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LACategoriesTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LACategoriesTableViewCell class]) forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.delegate = self;
    if (indexPath.section == 0)
    {
        cell.arrTitle = [NSMutableArray arrayWithObjects:@"CANCELAMENTO",@"ATRASO",@"OVERBOOKING", nil];
    }
    else
    {
        cell.arrTitle = [NSMutableArray arrayWithObjects:@"ATRASO",@"CARÊNCIA",@"CANCELAMENTO", nil];
    }
    [cell.clCategories reloadData];
    return cell;
}
#pragma mark USER ACTION
- (void)onChooseAtIndex:(NSIndexPath*)indexPath
{
    LASubCatViewController* laSubMenu = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LASubCatViewController class])];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if (arrCategories.count > indexPath.row)
    {
        laSubMenu.catObj = [arrCategories objectAtIndex:indexPath.row];
    }
    if ([self.txtSearch superview])
    {
        isShowSearch = NO;
        [self.txtSearch removeFromSuperview];
        [self showAndEnableRightNavigationItem];
    }
    [self.navigationController pushViewController:laSubMenu animated:YES];
}
- (IBAction)onNavigationBarTap:(id)sender
{
    [self onRightButtonPressed:nil];
}
- (IBAction)onViewNavigationTap:(id)sender
{
    [self onRightButtonPressed:nil];
}
@end

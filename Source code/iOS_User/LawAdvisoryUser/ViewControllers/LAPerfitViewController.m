//
//  LAPerfitViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPerfitViewController.h"
#import "AppDelegate.h"
#import "LAAccountObj.h"
#import "LAAccountService.h"
#import "MBProgressHUD.h"
@interface LAPerfitViewController ()

@end

@implementation LAPerfitViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = YES;
        self.isShouldShowBottombar  = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAccount) name:NOTIFICATION_KEY_CHANGE_ACCOUNT object:nil];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)initUI
{
    CGRect frameScroll = self.mainScroll.frame;
    UIView *vHeader = (UIView*)[self.view viewWithTag:110];
    frameScroll.size.height = self.view.frame.size.height - CGRectGetMaxY(vHeader.frame);
    self.mainScroll.frame = frameScroll;
    CGPoint pointCenter = self.mainScroll.center;
    pointCenter.x = self.view.center.x;
    [self.mainScroll setCenter:pointCenter];
    //CONTENT SIZE
    CGSize size = self.mainScroll.contentSize;
    size.height = CGRectGetMaxY(self.btnConfirmEdit.frame) + 10;
    [self.mainScroll setContentSize:size];
    [self updateAccount];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:HEIGHT_BIG_NAVIGATION];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)updateAccount
{
    NSString* strTokenAccess = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:strTokenAccess])
    {
        [self.btLogout setHidden:YES];
    }
    else
    {
        [self.btLogout setHidden:NO];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [LAAccountService logoutwCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                NSString* messageHere = [data objectForKey:KEY_RESPONSE_MESSAGE];
                if (success)
                {
                    [self showMessage:messageHere withPoistion:nil];
                    [[LADataCenter shareInstance] clearKeyChain];
                    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LALoginViewController"]];
                    [AppDelegate sharedDelegate].nav = nav;
                    [AppDelegate sharedDelegate].window.rootViewController = [AppDelegate sharedDelegate].nav;
                }
                else
                {
                    [self showMessage:message withPoistion:nil];
                }
            }
            else
            {
                [self showMessage:message withPoistion:nil];
            }
            
        }];
        
    }
}
- (IBAction)onLogoutPresseed:(id)sender {
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Do you want logout ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
    
}

- (IBAction)onEditProfilePressed:(id)sender {
}
@end

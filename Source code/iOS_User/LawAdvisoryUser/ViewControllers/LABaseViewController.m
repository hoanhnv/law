//
//  LABaseViewController.m
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPopupMessageViewController.h"
#import "AppDelegate.h"
#import "LAMainTabbarViewController.h"
#import "UIView+Toast.h"
@interface LABaseViewController ()

@end

@implementation LABaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self initData];
    [self initUI];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.isShowGreenNavigation)
    {
        self.navigationController.navigationBar.barTintColor = NAVIGATION_COLOR_GREEN;
    }
    else
    {
        self.navigationController.navigationBar.barTintColor = NAVIGATION_COLOR;
    }
    
    if (self.isShouldShowBigHeightNavigationBar)
    {
        id view = [self.navigationController.navigationBar viewWithTag:9999];
        if (view == nil)
        {
            UIImageView* viewIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo"]];
            [viewIcon setContentMode:UIViewContentModeScaleAspectFit];
            viewIcon.tag = 9999;
            [viewIcon setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
            [viewIcon setFrame:CGRectMake(0, 35, 115, 50)];
            [viewIcon setCenter:CGPointMake(self.navigationController.navigationBar.center.x, self.navigationController.navigationBar.center.y - 40)];
            [self.navigationController.navigationBar addSubview:viewIcon];
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onNavigationBarTap:)];
            tapGesture.numberOfTapsRequired = 1;
            [self.navigationController.navigationBar addGestureRecognizer:tapGesture];
        }
        
    }
    else
    {
        id view = [self.navigationController.navigationBar viewWithTag:9999];
        if (view && [view isKindOfClass:[UIImageView class]] && [view superview])
        {
            [(UIView*)view removeFromSuperview];
        }
    }
    if (self.isShouldBackButton)
    {
        [self initLeftButton];
        self.navigationController.navigationBarHidden = NO;
    }
}
#pragma mark Base Method
- (IBAction)onNavigationBarTap:(id)sender
{
    
}
- (IBAction)onLeftButtonPressed:(id)sender
{
    
}

- (void)initLeftButton
{
    UIImage *image = nil;
    if (self.isShouldBackButton)
    {
        image = [UIImage imageNamed:@"ic_backnavigation"];
    }
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(14, 0, fminf(image.size.width * 2, 26), image.size.height)];
    [button addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [button setContentEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    }
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}
- (void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)initUI
{
    
}
- (void)initData
{
    
}
- (void)showMessageViewControllerWithContent:(NSString*)contentText andTitleButton:(NSString*)titleButton
{
    NSString *className = NSStringFromClass([LAPopupMessageViewController class]);
    LAPopupMessageViewController *controller = [[LAPopupMessageViewController alloc] initWithNibName:className bundle:nil];
    controller.lbContent.text = contentText;
    controller.delegate = self;
    [controller.btExit setTitle:titleButton forState:UIControlStateNormal];
    [controller.view setFrame:[AppDelegate sharedDelegate].window.bounds];
    [self addChildViewController:controller];
    [self.view addSubview:controller.view];
    [UIView transitionWithView:controller.view duration:3.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
    } completion:^(BOOL finished) {
        [controller didMoveToParentViewController:self];
    }];
    
    
}

- (void)gotoHome
{
    LAMainTabbarViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LAMainTabbarViewControllery"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];

}
#pragma mark SHOW MESSSAGE NOTIFICATION
- (void)showMessage:(NSString*)message withPoistion:(id)poistion
{
    if (poistion == nil)
    {
        [[AppDelegate sharedDelegate].window makeToast:message
                                              duration:1.0
                                              position:CSToastPositionBottom
                                                 title:nil
                                                 image:nil
                                                 style:nil
                                            completion:^(BOOL didTap) {
                                                if (didTap) {
                                                } else {
                                                }
                                            }];
    }
    else
    {
        [[AppDelegate sharedDelegate].window makeToast:message
                                              duration:1.0
                                              position:poistion
                                                 title:nil
                                                 image:nil
                                                 style:nil
                                            completion:^(BOOL didTap) {
                                                if (didTap) {
                                                } else {
                                                }
                                            }];
    }
    
}
@end

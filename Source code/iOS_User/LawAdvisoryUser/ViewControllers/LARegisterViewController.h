//
//  LARegisterViewController.h
//  LawAdvisoryUser
//
//  Created by Apple on 8/28/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AwesomeTextField.h"
#import "LAAccountObj.h"
#import "LAAccountService.h"
#import "LAAddressService.h"
#import "LAUFs.h"
@interface LARegisterViewController : LABaseViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UIView* viewIndicator;
    IBOutlet UILabel* lbTabLeft;
    IBOutlet UILabel* lbTabRight;
    //LEFT CONTENT
    IBOutlet UIButton* btConfirm;
    LAAccountObj* objAccount;
    NSMutableArray* arrRequiredField;
    NSMutableArray* arrEmail;
    NSMutableArray* listCity;
    NSMutableArray* listState;
    LAUFs *listUFS;
}
@property (nonatomic) UIImage* imgAttach;
@property (nonatomic) LAAccountObj* currentAccount;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtEmail;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtName;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtPassWord;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtConfirmPass;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtConfirmEmail;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtNeighborhood;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtState;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCPF;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCEP;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtDateOfBirth;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnChooseImage;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtMobilePhone;
@property (nonatomic) LACityObj *currentCity;
@property (nonatomic) NSString *currentState;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtEndereco;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtComplete;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtStreet;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCity;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtGender;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtPhone;
@property (weak, nonatomic) IBOutlet UIButton *cbNew;
@property (nonatomic) IBOutlet UIScrollView* scrFullData;
@property (nonatomic) IBOutlet TPKeyboardAvoidingScrollView* scrLeft;
@property (nonatomic) IBOutlet UIScrollView* scrRight;
@end

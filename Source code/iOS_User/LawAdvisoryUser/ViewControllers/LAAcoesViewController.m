//
//  LAAcoesViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAcoesViewController.h"
#import "HMSegmentedControl.h"
#import "LANovasTableViewCell.h"
#import "LAEmAndAmentoTableViewCell.h"
#import "LAHistoryTableViewCell.h"

float const kLAHeightHistoryCell = 300.0f;
float const kLAHeightNovasCell = 215.0f;
float const kLAHeightPendenteCell = 215.0f;
float const kLAHeightEmandamentoCell = 218.0f;

@interface LAAcoesViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,AcoesNovaCellDelegate,AcoesEmAndamentoCellDelegate,AcoesHistoricoCellDelegate>
{
    IBOutlet UITableView* tbNovas;
    IBOutlet UITableView* tbEmAndamento;
    IBOutlet UITableView* tbhistorico;
    IBOutlet UITableView* tbPendente;
}
@property (nonatomic) IBOutlet UIView* viewFullData;
@property (nonatomic) IBOutlet UIScrollView* scrViewData;
@property (nonatomic) HMSegmentedControl* segmentedControl;
@end

@implementation LAAcoesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Ações";
    [self.navigationController.navigationBar setHeight:64.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupData
{
    // Tying up the segmented control to a scroll view
    //    self.pageControl.iNumberPage = arrSlider.count;
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    self.segmentedControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 50)];
    self.segmentedControl.sectionTitles = @[@"PENDENTE", @"NOVAS", @"EM ANDAMENTO",@"HISTÓRICO"];
    self.segmentedControl.selectedSegmentIndex = 1;
    self.segmentedControl.backgroundColor = [LAUtilitys jusTapColor];
    self.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};;
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl.tag = 3;
    self.segmentedControl.type = HMSegmentedControlTypeText;
    self.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.scrViewData scrollRectToVisible:CGRectMake(viewWidth * index, 0, viewWidth, 200) animated:YES];
    }];
    [self.view addSubview:self.segmentedControl];
    [self.scrViewData setFrame:CGRectMake(0, CGRectGetMaxY(self.segmentedControl.frame) + 5, viewWidth, CGRectGetHeight(self.view.frame) - CGRectGetMaxY(self.segmentedControl.frame) + 5 - 50)];
    self.scrViewData.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    self.scrViewData.pagingEnabled = YES;
    self.scrViewData.showsHorizontalScrollIndicator = NO;
    self.scrViewData.contentSize = CGSizeMake(self.segmentedControl.sectionTitles.count * viewWidth, 200);
    [self.scrViewData scrollRectToVisible:CGRectMake(viewWidth, 0, viewWidth, 200) animated:NO];
    for (int i = 0; i < arrTableview.count; i ++)
    {
        UITableView* tb = [arrTableview objectAtIndex:i];
        [tb setFrame:CGRectMake(i*viewWidth, 0, viewWidth, CGRectGetHeight(self.scrViewData.frame))];
    }
}
- (void)initData
{
    arrTableview = [NSMutableArray arrayWithObjects:tbPendente,tbNovas,tbEmAndamento,tbhistorico, nil];
    [self setupData];
    arrNovas = [NSMutableArray arrayWithObjects:@"a",@"b",@"c",@"d",@"e",@"f", nil];
    arrEmAnDamento = [NSMutableArray arrayWithObjects:@"a",@"b",@"c",@"d",@"e",@"f", nil];
    arrHistorico = [NSMutableArray arrayWithObjects:@"a",@"b",@"c",@"d",@"e",@"f", nil];
    [tbNovas registerNib:[UINib nibWithNibName:NSStringFromClass([LANovasTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LANovasTableViewCell class])];
    [tbEmAndamento registerNib:[UINib nibWithNibName:NSStringFromClass([LAEmAndAmentoTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAEmAndAmentoTableViewCell class])];
    [tbhistorico registerNib:[UINib nibWithNibName:NSStringFromClass([LAHistoryTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAHistoryTableViewCell class])];
    
}
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.scrViewData)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        
        [self.segmentedControl setSelectedSegmentIndex:page animated:YES];
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark PENDENTE
#pragma mark NOVAS
//DELEGATE
- (void)onAceiterDidTouch:(NSIndexPath*)indexPath
{
    
}
- (void)onRecusarDidTouch:(NSIndexPath*)indexPath
{
    
}
#pragma mark EM ANDAMENTO
//DELEGATE
- (void)onVerDetalhesEmandAmentoDidTouch:(NSIndexPath*)indexPath
{
    
}
#pragma mark HISTORICO
//DELEGATE
- (void)onVerDetalhesHistoryDidTouch:(NSIndexPath*)indexPath
{
    
}
#pragma mark UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tbNovas)
    {
        return arrNovas.count;
    }
    else if (tableView == tbEmAndamento)
    {
        return arrEmAnDamento.count;
    }
    if (tableView == tbhistorico)
    {
        return arrHistorico.count;
    }else
    {
        return arrPendente.count;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tbNovas)
    {
        LANovasTableViewCell* cell = [tbNovas dequeueReusableCellWithIdentifier:NSStringFromClass([LANovasTableViewCell class]) forIndexPath:indexPath];
        if (indexPath.row < arrNovas.count)
        {
            id obj = [arrNovas objectAtIndex:indexPath.row];
            [cell setupData:obj];
        }
        return cell;
    }
    else if (tableView == tbEmAndamento)
    {
        LAEmAndAmentoTableViewCell* cell = [tbEmAndamento dequeueReusableCellWithIdentifier:NSStringFromClass([LAEmAndAmentoTableViewCell class]) forIndexPath:indexPath];
        if (indexPath.row < arrEmAnDamento.count)
        {
            id obj = [arrEmAnDamento objectAtIndex:indexPath.row];
            [cell setupData:obj];
        }
        return cell;
    }
    if (tableView == tbhistorico)
    {
        LAHistoryTableViewCell* cell = [tbhistorico dequeueReusableCellWithIdentifier:NSStringFromClass([LAHistoryTableViewCell class]) forIndexPath:indexPath];
        if (indexPath.row < arrHistorico.count)
        {
            id obj = [arrHistorico objectAtIndex:indexPath.row];
            [cell setupData:obj];
        }
        return cell;
    }else
    {
        LANovasTableViewCell* cell = [tbPendente dequeueReusableCellWithIdentifier:NSStringFromClass([LANovasTableViewCell class]) forIndexPath:indexPath];
        if (indexPath.row < arrPendente.count)
        {
            id obj = [arrPendente objectAtIndex:indexPath.row];
            [cell setupData:obj];
        }
        return cell;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tbhistorico)
    {
        return kLAHeightHistoryCell;
    }
    else if (tableView == tbEmAndamento)
    {
        return kLAHeightEmandamentoCell;
    }
    return kLAHeightNovasCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
@end

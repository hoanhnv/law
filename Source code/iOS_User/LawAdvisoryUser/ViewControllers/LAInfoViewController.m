//
//  LAInfoViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAInfoViewController.h"
#import "InfoCell.h"
#import "HeaderInfo.h"
@interface LAInfoViewController ()
{
    NSMutableDictionary * mSelected;
}
@end

@implementation LAInfoViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = YES;
        self.isShouldShowBottombar  = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _scroll_View.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    _btnContato.layer.borderWidth = 2.0f;
    _btnContato.layer.borderColor = NAVIGATION_COLOR.CGColor;
    _btnFrequentes.layer.borderWidth = 2.0f;
    _btnFrequentes.layer.borderColor = NAVIGATION_COLOR.CGColor;

    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:HEIGHT_BIG_NAVIGATION];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

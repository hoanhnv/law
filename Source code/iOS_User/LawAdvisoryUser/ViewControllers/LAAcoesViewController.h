//
//  LAAcoesViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface LAAcoesViewController : LABaseViewController
{
    NSMutableArray* arrPendente;
    NSMutableArray* arrNovas;
    NSMutableArray* arrEmAnDamento;
    NSMutableArray* arrHistorico;
    NSMutableArray* arrTableview;
}
@end

//
//  LAFaleViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAFaleViewController.h"

#define MAXIMUM_CHARACTER 1000
#import "LAContactSubjects.h"
#import "MainService.h"
#import "MBProgressHUD.h"
#import "LAContactSubjectObj.h"
#import "AppDelegate.h"
#import "FaleResponse.h"

@interface LAFaleViewController ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>{
    LAContactSubjects *lstSubject;
    LAContactSubjectObj *currentContact;
    LAContactSubjects *responseSubject;
}

@end

@implementation LAFaleViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShouldBackButton = YES;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [self.btChooseType setImage:[UIImage imageNamed:@"60x60_updown"] forState:UIControlStateNormal];
    [self fetchSubjects];
}

-(void)fetchSubjects{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchContactSubjects:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        lstSubject = [[LAContactSubjects alloc]initWithDictionary:data];
        if ([lstSubject.data count]) {
            currentContact = [lstSubject.data objectAtIndex:0];
            self.txtCurrentType.text = currentContact.name;
        }
        else{
            self.txtCurrentType.text = MSG_0003;
        }
        [self.tbTypeAsk reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"ShowResponeFale"]) {
        FaleResponse *falePoup = segue.destinationViewController;
        falePoup.lbContent.text = responseSubject.message;
        falePoup.lbContent.lineBreakMode = NSLineBreakByWordWrapping;
        falePoup.lbContent.numberOfLines = 0;
    }
}

- (void)initUI
{
    self.btEnviar.layer.borderWidth = 2.0f;
    self.btEnviar.layer.borderColor = NAVIGATION_COLOR.CGColor;
    self.title = @"Fale Conosco";
    self.txtEnterText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.txtEnterText.layer.borderWidth = 1.0f;
    self.popupView.layer.borderColor = NAVIGATION_COLOR.CGColor;
    self.popupView.layer.borderWidth = 3.0f;
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= MAXIMUM_CHARACTER;
}

-(CGSize)sizeOfText:(NSString *)textToMesure widthOfTextView:(CGFloat)width withFont:(UIFont*)font
{
    CGSize ts = [textToMesure sizeWithFont:font constrainedToSize:CGSizeMake(width-20.0, FLT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    return ts;
}
- (void)initData
{
    
}
- (IBAction)onChooseType:(id)sender
{
    if ([lstSubject.data count]) {
        //        [self.btChooseType setImage:[UIImage imageNamed:@"60x60_dropdown"] forState:UIControlStateNormal];
        [self.popupView setHidden:!self.popupView.hidden];
    }
    else{
        // TODO
    }
}
- (IBAction)onEnviarPressed:(id)sender
{
    if (currentContact == nil) {
        [self showMessage:MSG_0003 withPoistion:nil];
    }
    else  if ([LAUtilitys NullOrEmpty:self.txtEnterText.text]){
        [self showMessage:MSG_0004 withPoistion:nil];
    }
    else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [MainService sendContact:currentContact.dataIdentifier withMessage:_txtEnterText.text withEmail:MAIL_CONTACT withBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            responseSubject = [[LAContactSubjects alloc]initWithDictionary:data];
            if ([responseSubject.data count]) {
                FaleResponse *falePoup = [self.storyboard instantiateViewControllerWithIdentifier:@"FaleResponse"];
                falePoup.responseSubject = responseSubject;
                falePoup.modalPresentationStyle = UIModalPresentationOverFullScreen;
                [self presentViewController:falePoup animated:YES completion:^{
//                    falePoup.lbContent.text = responseSubject.message;
//                    falePoup.lbContent.lineBreakMode = NSLineBreakByWordWrapping;
//                    falePoup.lbContent.numberOfLines = 0;
                }];
                
            }
            else{
                [self showMessage:responseSubject.message withPoistion:nil];
            }
            
        }];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [lstSubject.data count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"cellIdentier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if (cell)
    {
        cell.textLabel.textColor = [LAUtilitys jusTapColor];
        LAContactSubjectObj *obj = [lstSubject.data objectAtIndex:indexPath.row];
        cell.textLabel.text = obj.name;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.popupView setHidden:!self.popupView.hidden];
    LAContactSubjectObj *obj = [lstSubject.data objectAtIndex:indexPath.row];
    self.txtCurrentType.text = obj.name;
    //    [self.btChooseType setImage:[UIImage imageNamed:@"60x60_updown"] forState:UIControlStateNormal];
}
@end

//
//  LAFrequentesViewController.m
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAFrequentesViewController.h"
#import "InfoCell.h"
#import "HeaderInfo.h"
#import "MainService.h"
#import "LAListFaqs.h"
#import "LAQuestionObj.h"
#import "MBProgressHUD.h"
@interface LAFrequentesViewController ()<UITableViewDelegate,UITableViewDataSource,UINavigationBarDelegate>
{
    NSMutableDictionary * mSelected;
    LAListFaqs *lstFaqs;
}
@end

@implementation LAFrequentesViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Perguntas frequentes";
    _tbvInfo.estimatedRowHeight = 180;
    _tbvInfo.rowHeight = UITableViewAutomaticDimension;
    [_tbvInfo registerNib:[UINib nibWithNibName:NSStringFromClass([InfoCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoCell class])];
    UIBarButtonItem * barItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_backnavigation"] style:UIBarButtonItemStylePlain target:self action:@selector(backView)];
    self.navigationItem.leftBarButtonItem = barItem;
    mSelected = [NSMutableDictionary new];
    [mSelected setObject:@(1) forKey:@"0"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchFaqs:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        lstFaqs = [[LAListFaqs alloc]initWithDictionary:data];
        if (lstFaqs.success) {
            [self reloadData];
        }
        else{
            [self showMessage:lstFaqs.message withPoistion:nil];
        }
    }];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)backView{
}
-(void)reloadData{
    [self.tbvInfo reloadData];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [lstFaqs.data count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]]integerValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InfoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoCell class]) forIndexPath:indexPath];
    LAQuestionObj *questionInfo = (LAQuestionObj*)lstFaqs.data[indexPath.section];
    cell.lbContent.text = questionInfo.question;
    [cell.btnAccept addTarget:self action:@selector(doAccept:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnAccept setTag:indexPath.row];
    [cell.btnCancel addTarget:self action:@selector(doCancel:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCancel setTag:indexPath.row];
    
    return cell;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderInfo * header = [[HeaderInfo alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    for (NSString * key in mSelected.allKeys) {
        if (section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]] integerValue] == 1;
            [header setupColor:isShow];
        }
    }
    [header setShowBlock:^(BOOL isShow){
        if (isShow) {
            [mSelected setObject:@(1) forKey:[NSString stringWithFormat:@"%ld",section]];
            
        } else{
            [mSelected setObject:@(0) forKey:[NSString stringWithFormat:@"%ld",section]];
        }
        [tableView reloadData];
    }];
    return header;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  44;
}

-(void)doAccept:(id)sender{
    
}
-(void)doCancel:(id)sender{
    
}
@end

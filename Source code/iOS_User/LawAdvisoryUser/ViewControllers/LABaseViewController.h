//
//  LABaseViewController.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UINavigationBar+CustomHeight.h"
@interface LABaseViewController : UIViewController
@property (nonatomic) BOOL isTransparentNavigation;
@property (nonatomic) BOOL isBlueNavigationBar;
@property (nonatomic) BOOL isShouldBackButton;
@property (nonatomic) BOOL isShouldLeftButton;
@property (nonatomic) BOOL isShouldRightButton;
@property (nonatomic) BOOL isShowGreenNavigation;
@property (nonatomic) BOOL isShouldShowBottombar;
@property (nonatomic) UILabel* lbNavigationTitle;
@property (nonatomic) IBOutlet UIView* viewNavigationBar;
@property (nonatomic) IBOutlet UIButton* btRightBar;
@property (nonatomic) IBOutlet UIButton* btLeftBar;
@property (nonatomic) BOOL isShouldShowBigHeightNavigationBar;
#pragma mark Base Method
- (void)initUI;
- (void)initData;
- (void)showMessageViewControllerWithContent:(NSString*)contentText andTitleButton:(NSString*)titleButton;
- (void)gotoHome;
- (IBAction)onLeftButtonPressed:(id)sender;
- (IBAction)onNavigationBarTap:(id)sender;
#pragma mark SHOW MESSSAGE NOTIFICATION
- (void)showMessage:(NSString*)message withPoistion:(id)poistion;
@end

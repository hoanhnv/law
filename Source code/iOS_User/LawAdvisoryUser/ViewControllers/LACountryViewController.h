//
//  CountryViewController.h
//  GoInk
//
//  Created by Mac on 9/21/15.
//  Copyright (c) 2015 MJH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LABaseViewController.h"
#import "LAAddressService.h"
typedef enum TYPE_POPUP
{
    TYPE_CITY = 0,
    TYPE_STATE = 1
}TYPE_POPUP;
@interface LACountryViewController : LABaseViewController{
    NSIndexPath *indexSelectPath;
}
@property (nonatomic,strong) id delegate;
@property (nonatomic, strong) NSArray *arrCountry;
@property (nonatomic,assign) TYPE_POPUP popType;
@property (nonatomic) LACityObj* currentCity;
@property (nonatomic) NSString* currentState;
@property (weak, nonatomic) IBOutlet UITableView *tblCity;
-(IBAction)pressRightBarItem:(id)sender;
@end

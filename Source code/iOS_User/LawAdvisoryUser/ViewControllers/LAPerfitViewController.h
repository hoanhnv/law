//
//  LAPerfitViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface LAPerfitViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIImageView *vAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbProfissional;
@property (weak, nonatomic) IBOutlet UILabel *lbService;
@property (nonatomic) IBOutlet UIButton* btLogout;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmEdit;
- (IBAction)onLogoutPresseed:(id)sender;
- (IBAction)onEditProfilePressed:(id)sender;
@end

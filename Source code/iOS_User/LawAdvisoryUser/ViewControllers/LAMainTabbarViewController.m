//
//  LAMainTabbarViewController.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAMainTabbarViewController.h"
#import "AppDelegate.h"
@interface LAMainTabbarViewController ()

@end

@implementation LAMainTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [AppDelegate sharedDelegate].mainTabbar = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIImage *musicImageSel1 = [UIImage imageNamed:@"categorias_active"];
    UIImage *musicImageSel2 = [UIImage imageNamed:@"acoes_active"];
    UIImage *musicImageSel3 = [UIImage imageNamed:@"info_active"];
    UIImage *musicImageSel4 = [UIImage imageNamed:@"perfil_active"];
    musicImageSel1 = [musicImageSel1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    musicImageSel2 = [musicImageSel2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    musicImageSel3 = [musicImageSel3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    musicImageSel4 = [musicImageSel4 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    if ([[self.tabBar.items objectAtIndex:0] respondsToSelector:@selector(setSelectedImage:)])
    {
        [[self.tabBar.items objectAtIndex:0] setSelectedImage:musicImageSel1];
        [[self.tabBar.items objectAtIndex:1] setSelectedImage:musicImageSel2];
        [[self.tabBar.items objectAtIndex:2] setSelectedImage:musicImageSel3];
        [[self.tabBar.items objectAtIndex:3] setSelectedImage:musicImageSel4];
    }
    [UITabBarItem.appearance setTitleTextAttributes:@{
                                                        NSForegroundColorAttributeName : NAVIGATION_COLOR
                                                      }     forState:UIControlStateSelected];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  LAIntroViewController.m
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAIntroViewController.h"
#import "LAIntroView.h"
#import "LALoginViewController.h"
#define NUMBER_SLIDE 4
@interface LAIntroViewController ()
{
    NSInteger currenPageIndex;
}
@end

@implementation LAIntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)initUI
{
    self.pgControl.iNumberPage = NUMBER_SLIDE;
    [self.pgControl setUpImage];
}
- (void)initData
{
    currenPageIndex = 0;
    arrData = [NSMutableArray arrayWithCapacity:NUMBER_SLIDE];
    NSDictionary* dict1 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro1",KEY_IMAGE,@"1 - Encontre seu assunto",KEY_TITLE,@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc bibendum accumsan rhoncus.  primis in faucibus. Quisque rutrum sit amet.",KEY_DESCRIPTION, nil];
    NSDictionary* dict2 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro2",KEY_IMAGE,@"2 - informações gerais",KEY_TITLE,@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc bibendum accumsan rhoncus.  primis in faucibus. Quisque rutrum sit amet.",KEY_DESCRIPTION, nil];
    NSDictionary* dict3 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro3",KEY_IMAGE,@"3 - anexe os arquivos",KEY_TITLE,@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc bibendum accumsan rhoncus.  primis in faucibus. Quisque rutrum sit amet.",KEY_DESCRIPTION, nil];
    NSDictionary* dict4 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro4",KEY_IMAGE,@"4 - ache o advogado ideal",KEY_TITLE,@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc bibendum accumsan rhoncus.  primis in faucibus. Quisque rutrum sit amet.",KEY_DESCRIPTION, nil];
    [arrData addObject:dict1];
    [arrData addObject:dict2];
    [arrData addObject:dict3];
    [arrData addObject:dict4];
    CGSize sizeScroll = self.scrIntroPath.contentSize;
    sizeScroll.width = CGRectGetWidth(self.scrIntroPath.frame) * NUMBER_SLIDE;
    [self.scrIntroPath setContentSize:sizeScroll];
    CGRect rectScroll = self.scrIntroPath.frame;
    for (int  i = 0; i < arrData.count; i ++)
    {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"LAIntroView"
                                                          owner:self
                                                        options:nil];
        LAIntroView* viewCustom = (LAIntroView*)[nibViews objectAtIndex:0];
        CGRect frameView = CGRectMake(CGRectGetWidth(rectScroll)*i, 0, CGRectGetWidth(rectScroll), CGRectGetHeight(rectScroll));
        [self.scrIntroPath addSubview:viewCustom];
        [viewCustom setFrame:frameView];
        NSDictionary* dictData = [arrData objectAtIndex:i];
        [viewCustom setupDataWith:dictData];
    }
}
#pragma mark UIScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrIntroPath.frame.size.width;
    float currentPageHere = self.scrIntroPath.contentOffset.x / pageWidth;
    if (0.0f != fmodf(currentPageHere, 1.0f))
    {
        currentPageHere = 0;
    }
    currenPageIndex = currentPageHere;
    [self gotoCurrentPage:currentPageHere];
}
- (void)gotoCurrentPage:(NSInteger)page
{
    [self.pgControl setCurrentPage:page];
    
}
- (void)whenFinishIntro
{
    [self onEntrantPressed:nil];
}
- (IBAction)onEntrantPressed:(id)sender
{
    [[LADataCenter shareInstance]updateFlagShowScreenIntroduction:NO];
    LALoginViewController* loginVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LALoginViewController class])];
    [self.navigationController pushViewController:loginVC animated:YES];
}
@end

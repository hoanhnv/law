//
//  LASubCatViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LACategoryObj.h"
@interface LASubCatViewController : LABaseViewController
{
    
}
@property (nonatomic) LACategoryObj* catObj;
@end

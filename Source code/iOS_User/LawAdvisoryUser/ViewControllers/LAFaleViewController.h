//
//  LAFaleViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface LAFaleViewController : LABaseViewController
@property (nonatomic) IBOutlet UIButton* btChooseType;
@property (nonatomic) IBOutlet UILabel* txtCurrentType;
@property (nonatomic) IBOutlet UIView* popupType;
@property (nonatomic) IBOutlet UIButton* btEnviar;
@property (nonatomic) IBOutlet UITextView* txtEnterText;
@property (weak, nonatomic) IBOutlet UIButton *btChangeSize;
@property (weak, nonatomic) IBOutlet UILabel *lbMaximomCharacter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlViewData;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UITableView *tbTypeAsk;
- (IBAction)onChooseType:(id)sender;
- (IBAction)onEnviarPressed:(id)sender;
@end

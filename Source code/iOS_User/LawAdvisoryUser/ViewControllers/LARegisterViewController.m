//
//  LARegisterViewController.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/28/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LARegisterViewController.h"
#import "MBProgressHUD.h"
#import "LAAccountObj.h"
#import "LACountryViewController.h"
#import "LAAccountService.h"
#import "UIViewController+MJPopupViewController.h"
#import "LAPopupMessageViewController.h"
#import "AppDelegate.h"
#import "LAUFs.h"
#import "MainService.h"
@interface LARegisterViewController ()<UITextFieldDelegate>

@end

@implementation LARegisterViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShouldBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)initUI
{
    [self setupFrame];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtCity];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtState];
}
- (void)initData
{
    arrRequiredField = [NSMutableArray arrayWithObjects:self.txtEmail,self.txtConfirmEmail,self.txtPassWord,self.txtName, nil];
    arrEmail = [NSMutableArray arrayWithObjects:self.txtEmail,self.txtConfirmEmail, nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didselectAddress:) name:@"didSelectCountry" object:nil];
    [self getCityList];
    [self setupData];
    [self getUFs];
}
- (void)setupData
{
    if (self.currentAccount)
    {
        self.txtEmail.text = [LAUtilitys getDefaultString:self.currentAccount.accountEmail];
        self.txtConfirmEmail.text = [LAUtilitys getDefaultString:self.currentAccount.accountEmail];
        self.txtName.text = [LAUtilitys getDefaultString:self.currentAccount.accountName];
        if (self.currentAccount.accountDateOfBirth != 0)
        {
            self.txtDateOfBirth.text = [LAUtilitys getDateStringFromTimeInterVal:self.currentAccount.accountDateOfBirth];
        }
        
    }
}
- (void)getCityList
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [LAAddressService getlistCitywCompleteBlock:^(NSInteger errorCode, NSString *message, id data, NSInteger total) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data) {
            listCity = [NSMutableArray arrayWithArray:data];
        }
    }];
}
-(void)getUFs{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchUFs:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        listUFS = [[LAUFs alloc]initWithDictionary:data];
    }];
}
- (void)setupFrame
{
    CGPoint pointCenter = self.scrLeft.center;
    pointCenter.x = self.view.center.x;
    [self.scrLeft setCenter:pointCenter];
    pointCenter = viewIndicator.center;
    pointCenter.x = lbTabLeft.center.x;
    [viewIndicator setCenter:pointCenter];
    //CONTENT SIZE
    CGSize size = self.scrLeft.contentSize;
    size.height = CGRectGetMaxY(btConfirm.frame) + 10;
    [self.scrLeft setContentSize:size];
}
- (BOOL)checkIsValid
{
    BOOL isCheck = YES;
    for (UITextField* txt in arrRequiredField)
    {
        if ([LAUtilitys isEmptyOrNull:txt.text])
        {
            [self showMessage:[NSString stringWithFormat:@"%@ not empty",txt.placeholder] withPoistion:nil];
            isCheck = NO;
            break;
        }
    }
    if (isCheck)
    {
        for (UITextField* txt in arrEmail)
        {
            if ([LAUtilitys isEmptyOrNull:txt.text])
            {
                [self showMessage:[NSString stringWithFormat:@"%@ is invalid email",txt.placeholder] withPoistion:nil];
                isCheck = NO;
                break;
            }
        }
    }
    //    if (isCheck)
    //    {
    //        if (![self.txtConfirmPass.text isEqualToString:self.txtPassWord.text])
    //        {
    //            [self showMessage:@"confirm pass not match" withPoistion:nil];
    //            isCheck = NO;
    //        }
    //    }
    return isCheck;
}
- (IBAction)changeGetNew:(id)sender {
    _cbNew.selected = !_cbNew.selected;
    if (_cbNew.selected)
    {
        [_cbNew setHighlighted:YES];
    }
    else
    {
        [_cbNew setHighlighted:NO];
    }
}
- (IBAction)onRegisterPressed:(id)sender
{
    objAccount = [LAAccountObj new];
    if ([self checkIsValid])
    {
        objAccount.accountEmail = self.txtEmail.text;
        objAccount.accountPassword = self.txtPassWord.text;
        objAccount.accountCity = self.txtCity.text;
        objAccount.accountCellphone = self.txtMobilePhone.text;
        objAccount.accountPhone = self.txtPhone.text;
        objAccount.accountCity = self.txtCity.text;
        objAccount.accountDateOfBirth = [self.txtDateOfBirth.text doubleValue];
        objAccount.accountNeighborhood = self.txtNeighborhood.text;
        objAccount.accountComplement = self.txtComplete.text;
        objAccount.accountUF = self.txtCEP.text;
        objAccount.accountName = _txtName.text;
        objAccount.accountAddress = @"";
        objAccount.avataURL = @"";
        objAccount.accountNumber = @"";
        if (_cbNew.selected)
        {
            objAccount.accountAcceptNew = 1;
        }
        else
        {
            objAccount.accountAcceptNew = 0;
        }
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [LAAccountService registerWithAccount:objAccount wCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                NSString* message = [data objectForKey:KEY_RESPONSE_MESSAGE];
                if (success)
                {
                    NSString* token = [data objectForKey:KEY_RESPONSE_TOKEN];
                    [LAHTTPClient sharedClient].tokenRequest = token;
                    if (![LAUtilitys NullOrEmpty:token]) {
                        [[LADataCenter shareInstance] saveToken:token error:nil];
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KEY_CHANGE_ACCOUNT object:nil];
                    
                    if (self.imgAttach)
                    {
                        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        hud.mode = MBProgressHUDModeAnnularDeterminate;
                        hud.labelText = @"Image uploading...";
                        [LABaseService uploadImage:self.imgAttach andParams:nil andPath:API_PATH_UPLOAD_IMAGE withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
                            [hud hide:YES];
                        } progressBlock:^(double currentProgress) {
                            hud.progress = currentProgress;
                        }];
                    }
                    
                    [self showMessageViewControllerWithContent:[NSString stringWithFormat:@"Foi enviada uma nova senha para o seu email %@",[LAUtilitys getDefaultString:objAccount.accountEmail]] andTitleButton:REGISTER_PRESSED_TITLE_BUTTON];
                }
                else
                {
                    [self showMessage:message withPoistion:nil];
                }
            }
            
        }];
        
    }
}
- (void)showMessageViewControllerWithContent:(NSString*)contentText andTitleButton:(NSString*)titleButton
{
    NSString *className = NSStringFromClass([LAPopupMessageViewController class]);
    LAPopupMessageViewController *controller = [[LAPopupMessageViewController alloc] initWithNibName:className bundle:nil];
    controller.lbContent.text = contentText;
    [controller.btExit setTitle:titleButton forState:UIControlStateNormal];
    controller.delegate = self;
    controller.content = contentText;
    controller.titleButton = titleButton;
    [controller.view setFrame:[AppDelegate sharedDelegate].window.bounds];
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
    
}
- (void)onFinishPopupPressed
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self gotoHome];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    NSData *imgData = UIImageJPEGRepresentation(img, 1.0);
    NSLog(@"size %lu",[imgData length]);
    if ([imgData length] > 13421772)
    {
        self.imgAttach  = [UIImage imageWithData:UIImageJPEGRepresentation(img, 0.3)];
    }
    else
        if ([imgData length] > 8421772)
        {
            self.imgAttach  = [UIImage imageWithData:UIImageJPEGRepresentation(img, 0.5)];
        }
        else
            if ([imgData length] > 5421772)
            {
                self.imgAttach = [UIImage imageWithData:UIImageJPEGRepresentation(img, 0.75)];
            }
            else
            {
                self.imgAttach = info[UIImagePickerControllerOriginalImage];
            }
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)onTakePic:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:^{
        }];
    }
    else
    {
        [self showMessage:@"Camera not avaible" withPoistion:nil];
    }
}
- (IBAction)onShowCity:(id)sender
{
    [self showCityList:TYPE_CITY];
}
- (IBAction)onShowState:(id)sender
{
    listState = [NSMutableArray arrayWithArray:listUFS.data];
    [self showCityList:TYPE_STATE];
}
- (void)showCityList:(TYPE_POPUP)type
{
    LACountryViewController* countryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LACountryViewController"];
    countryVC.isShouldBackButton = NO;
    if (type == TYPE_CITY)
    {
        countryVC.arrCountry = listCity;
        countryVC.currentCity = self.currentCity;
    }
    else
    {
        countryVC.arrCountry = listState;
        countryVC.currentState = self.currentState;
    }
    countryVC.popType = type;
    countryVC.isTransparentNavigation = YES;
    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:countryVC];
    [self presentViewController:navi animated:YES completion:nil];
}
- (void)didselectAddress:(NSNotification*)notif
{
    id obj = [notif object];
    if (obj && [obj isKindOfClass:[LACityObj class]])
    {
        self.currentCity = obj;
        self.txtCity.text = self.currentCity.cityName;
    }
    else{
        self.currentState = obj;
        self.txtState.text = self.currentState;
    }
}
- (IBAction)onSelectImage:(id)sender
{
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:^{}];
}
@end

//
//  LABaseService.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseService.h"

@implementation LABaseService
+ (void) requestAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    [[LAHTTPClient sharedClient] GET:path parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        block(error.code,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),error);
    }];
}
+ (void) postAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    NSLog(@"path_postAndParseBasic %@", path);
    [[LAHTTPClient sharedClient] POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        block(error.code,NSLocalizedString(@"", @""),resData);
    }];
}
+ (void) putAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    [[LAHTTPClient sharedClient] POST:path parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        block(error.code,NSLocalizedString(@"", @""),resData);
    }];
}
+ (void) deleteAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    [[LAHTTPClient sharedClient] DELETE:path parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        block(error.code,NSLocalizedString(@"", @""),resData);
    }];
}
+ (void) uploadImage:(UIImage*)imageUpload andParams:(NSDictionary*)params andPath:(NSString*)path withCompletionBlock:(CompletionBlock) block progressBlock:(ProgressBlockUpload)blockProgress
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [[LAHTTPClient sharedClient] POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData: UIImagePNGRepresentation(imageUpload) name:@"image_rg" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        double currentProgress = uploadProgress.fractionCompleted;
        blockProgress(currentProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        block(0,@"",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        block(error.code,NSLocalizedString(@"", @""),resData);
    }];
}
@end

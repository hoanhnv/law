//
//  LAAddressService.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseService.h"
#import "LACityObj.h"
#import "LAStateObj.h"
//CITYS KEY
#define REQUEST_ACCOUNT_KEY_SIGLA @"sigla"
#define REQUEST_ACCOUNT_KEY_NOME @"nome"
#define REQUEST_ACCOUNT_KEY_CITY_LIST @"cidades"
@interface LAAddressService : LABaseService
+ (void)getlistCitywCompleteBlock:(CompletionBlockPagination)block;
@end

//
//  CPHTTPClient.m
//  CompassUtility
//
//  Created by Long Hoang on 7/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAHTTPClient.h"
#import "LAConstant.h"
static LAHTTPClient *_sharedClient = nil;
static dispatch_once_t onceToken;
@implementation LAHTTPClient
+ (instancetype)sharedClient {
    dispatch_once(&onceToken, ^{
        _sharedClient = [[LAHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_API_URL]];
        AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
        [securityPolicy setAllowInvalidCertificates:YES];
        [_sharedClient setSecurityPolicy:securityPolicy];
        _sharedClient.requestSerializer.timeoutInterval = 400;
        
    });
    return _sharedClient;
}
+ (instancetype)sharedClientWithUrl:(NSString*)strUrl {
    LAHTTPClient *_sharedClient = nil;
    _sharedClient = [[LAHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:_sharedClient.responseSerializer.acceptableContentTypes];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [contentTypes addObject:@"text/html"];
    [contentTypes addObject:@"application/json"];
    _sharedClient.responseSerializer = responseSerializer;
    _sharedClient.responseSerializer.acceptableContentTypes = contentTypes;
    return _sharedClient;
}
- (void)loadCachedSessionUid {
    NSArray *storedCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:BASE_API_URL]];
    for (NSHTTPCookie *cookie in storedCookies) {
        if ([cookie.name isEqualToString:@"PHPSESSID"]) {
            [self.requestSerializer setValue:[NSString stringWithFormat:@"%@=%@", cookie.name, cookie.value] forHTTPHeaderField:@"Cookie"];
            break;
        }
    }
}
@end

//
//  MainService.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseService.h"

#define API_PATH_FAQS @"faqs"
#define API_PATH_CATEGORY @"lawsuit_categories"
#define API_PATH_GET_CONTACT_SUBJECTS @"contact_subjects"

#define API_PATH_UFS @"ufs"
#define API_PATH_SEND_CONTACT @"contacts"

@interface MainService : LABaseService

+(void)fetchFaqs:(CompletionBlock)block;

+(void)fetchCategories:(CompletionBlockPagination)block;

+(void)fetchContactSubjects:(CompletionBlock)block;

+(void)fetchUFs:(CompletionBlock)block;

+(void)sendContact:(double)subjectId withMessage:(NSString*)message
         withEmail:(NSString*)email withBlock:(CompletionBlock)block;
@end

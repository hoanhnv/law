//
//  MainService.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "MainService.h"
#import "LAHTTPClient.h"
#import "LACategoryObj.h"
#import "LAContactObj.h"
@implementation MainService

+(void)fetchFaqs:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_FAQS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)fetchCategories:(CompletionBlockPagination)block
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil,0);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_CATEGORY params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        NSMutableArray* listCategories = [NSMutableArray new];
        NSString* responseMessage = message;
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            NSArray* arr = [data objectForKey:KEY_RESPONSE_DATA];
            responseMessage = [data objectForKey:KEY_RESPONSE_MESSAGE];
            if (arr && [arr isKindOfClass:[NSArray class]] && arr.count)
            {
                for (NSDictionary* dict in arr)
                {
                    LACategoryObj* obj = [[LACategoryObj alloc] initWithDictionary:dict];
                    if (obj)
                    {
                        [listCategories addObject:obj];
                    }
                }
            }
        }
        block(errorCode,message,listCategories,listCategories.count);
    }];
}
+(void)fetchContactSubjects:(CompletionBlock)block
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_GET_CONTACT_SUBJECTS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchUFs:(CompletionBlock)block{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_UFS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    
}

+(void)sendContact:(double)subjectId withMessage:(NSString *)message
         withEmail:(NSString *)email withBlock:(CompletionBlock)block{
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:message] ||[LAUtilitys NullOrEmpty:message]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [LABaseService postAndParseBasic:API_PATH_SEND_CONTACT params:@{@"subject_id":[NSNumber numberWithDouble:subjectId],@"message":message,@"email":email} withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
@end

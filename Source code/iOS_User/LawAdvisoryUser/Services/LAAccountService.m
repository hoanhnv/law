//
//  LAAccountService.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAccountService.h"

@implementation LAAccountService
// Login with username (email) and password.
+ (void)loginWithEmail:(NSString*)email andPassword:(NSString*)password wCompleteBlock:(CompletionBlock)block
{
    if ([LAUtilitys isEmptyOrNull:email] || [LAUtilitys isEmptyOrNull:password])
    {
        block(INPUT_ERROR,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),nil);
    }
    else
    {
        NSDictionary* dict = @{REQUEST_ACCOUNT_KEY_EMAIL:email,REQUEST_ACCOUNT_KEY_PASSWORD:password};

        [LABaseService postAndParseBasic:API_PATH_LOGIN params:dict withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
            block(errorCode,message,data);
        }];
    }
    
}
+ (void)registerWithAccount:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block
{
    if ([LAUtilitys isEmptyOrNull:obj.accountEmail] || [LAUtilitys isEmptyOrNull:obj.accountPassword])
    {
        block(INPUT_ERROR,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),nil);
    }
    else
    {
        [[LAHTTPClient sharedClient].requestSerializer setValue:@"" forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
        NSDictionary* dict = @{REQUEST_ACCOUNT_KEY_EMAIL:obj.accountEmail,REQUEST_ACCOUNT_KEY_PASSWORD:obj.accountPassword,REQUEST_ACCOUNT_KEY_NAME:obj.accountName,REQUEST_ACCOUNT_KEY_CELLPHONE:obj.accountCellphone,REQUEST_ACCOUNT_KEY_PHONE:obj.accountPhone,REQUEST_ACCOUNT_KEY_ADDRESS:obj.accountAddress,REQUEST_ACCOUNT_KEY_NUMBER:obj.accountNumber,REQUEST_ACCOUNT_KEY_COMPLEMENT:obj.accountComplement,REQUEST_ACCOUNT_KEY_NEIGHBORHOOD:obj.accountNeighborhood,REQUEST_ACCOUNT_KEY_CITY:obj.accountCity,REQUEST_ACCOUNT_KEY_UF:obj.accountUF,REQUEST_ACCOUNT_KEY_DATEOFBIRTH:[NSNumber numberWithDouble:obj.accountDateOfBirth],REQUEST_ACCOUNT_KEY_ACCEPTSNEW:[NSNumber numberWithInteger:obj.accountAcceptNew]};
        [LABaseService postAndParseBasic:API_PATH_REGISTER params:dict withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
            block(errorCode,message,data);
        }];
    }
    
}
+ (void)logoutwCompleteBlock:(CompletionBlock)block
{
    [LABaseService requestAndParseBasic:API_PATH_LOGOUT params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+ (void)resetPassWithEmail:(NSString*)email wCompleteBlock:(CompletionBlock)block;
{
    if ([LAUtilitys isEmptyOrNull:email])
    {
        block(INPUT_ERROR,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),nil);
    }
    else
    {
        NSDictionary* dict = @{REQUEST_ACCOUNT_KEY_EMAIL:email};
        [LABaseService postAndParseBasic:API_PATH_RESET_PASS params:dict withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
            block(errorCode,message,data);
        }];
    }
}
@end

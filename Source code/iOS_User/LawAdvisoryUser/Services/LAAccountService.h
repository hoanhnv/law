//
//  LAAccountService.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseService.h"
#import "LAAccountObj.h"
//LOGIN KEY
#define REQUEST_ACCOUNT_KEY_EMAIL @"email"
#define REQUEST_ACCOUNT_KEY_PASSWORD @"password"
//REGISTER KEY
#define REQUEST_ACCOUNT_KEY_NAME @"name"
#define REQUEST_ACCOUNT_KEY_CELLPHONE @"cellphone"
#define REQUEST_ACCOUNT_KEY_PHONE @"phone"
#define REQUEST_ACCOUNT_KEY_ADDRESS @"address"
#define REQUEST_ACCOUNT_KEY_NUMBER @"number"
#define REQUEST_ACCOUNT_KEY_COMPLEMENT @"complement"
#define REQUEST_ACCOUNT_KEY_NEIGHBORHOOD @"neighborhood"
#define REQUEST_ACCOUNT_KEY_CITY @"city"
#define REQUEST_ACCOUNT_KEY_UF @"uf"
#define REQUEST_ACCOUNT_KEY_DATEOFBIRTH @"date_of_birth"
#define REQUEST_ACCOUNT_KEY_ACCEPTSNEW @"accepts_news"

@interface LAAccountService : LABaseService
// Login with username (email) and password.
+ (void)loginWithEmail:(NSString*)email andPassword:(NSString*)password wCompleteBlock:(CompletionBlock)block;
+ (void)registerWithAccount:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block;
+ (void)logoutwCompleteBlock:(CompletionBlock)block;
+ (void)resetPassWithEmail:(NSString*)email wCompleteBlock:(CompletionBlock)block;
@end

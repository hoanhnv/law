//
//  LADataCenter.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HOANHNV. All rights reserved.
//

#import "LADataCenter.h"
#import <SAMKeychain/SAMKeychain.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>
#import <Twitter/Twitter.h>

@implementation LADataCenter

+(instancetype)shareInstance{
    static  LADataCenter *_shareInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_shareInstance == nil) {
            _shareInstance = [LADataCenter new];
        }
    });
    return _shareInstance;
    
}
-(void)saveToken:(NSString *)token error:(NSError**)err{
    [SAMKeychain setPassword:[NSString stringWithFormat:@"Bearer %@",token] forService:@"JusTap" account:@"JusTapUser" error:err];
}

-(void)setAccessToken:(NSString *)accessToken{
    
}

-(NSString *)currentAccessToken{
    NSError *err = nil;
    NSString *strAccessToken = [SAMKeychain passwordForService:@"JusTap" account:@"JusTapUser" error:&err];
    if (err == nil) {
        return strAccessToken;
    }
    else{
        return STRING_EMPTY;
    }
}
-(void)updateFlagShowScreenIntroduction:(BOOL)bValue{
    [[NSUserDefaults standardUserDefaults]setBool:bValue forKey:@"hasLaunchedOnce"];
}
+ (BOOL)isFirstTime{
    static BOOL flag=NO;
    static BOOL result;
    
    if(!flag){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"hasLaunchedOnce"]){
            result=NO;
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasLaunchedOnce"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            result=YES;
        }
        
        flag=YES;
    }
    return result;
}

-(void)clearKeyChain{
    [SAMKeychain deletePasswordForService:@"JusTap" account:@"JusTapUser"];
    
    [[GIDSignIn sharedInstance] signOut];
    
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    NSString *userID = store.session.userID;
    [store logOutUserID:userID];
    
    [[FBSDKLoginManager new] logOut];
    
}
@end

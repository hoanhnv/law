//
//  LAIntroView.m
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAIntroView.h"

@implementation LAIntroView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)setupDataWith:(NSDictionary*)dictData
{
    if (dictData)
    {
        NSString* img = [dictData objectForKey:KEY_IMAGE];
        NSString* txtTitle = [[dictData objectForKey:KEY_TITLE] uppercaseString];
        NSString* txtDescription = [dictData objectForKey:KEY_DESCRIPTION];
        [self.imgShow setImage:[UIImage imageNamed:img]];
        [self.lbTitle setText:txtTitle];
        [self.lbDescription setText:txtDescription];
    }
}
@end

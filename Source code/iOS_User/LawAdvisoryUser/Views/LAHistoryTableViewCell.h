//
//  LAHistoryTableViewCell.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAHistoryTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UIButton* btVerdetalhes;
@property (nonatomic) IBOutlet UILabel* lbStatus;
@property (nonatomic) IBOutlet UILabel* lbCategoria;
@property (nonatomic) IBOutlet UILabel* lbAssunto;
@property (nonatomic) IBOutlet UILabel* lbProcesso;
@property (nonatomic) IBOutlet UILabel* lbInicio;
@property (nonatomic) IBOutlet UILabel* lbTermino;
@property (nonatomic) IBOutlet UILabel* lbCliente;
@property (nonatomic) IBOutlet UIButton* btStar1;
@property (nonatomic) IBOutlet UIButton* btStar2;
@property (nonatomic) IBOutlet UIButton* btStar3;
@property (nonatomic) IBOutlet UIButton* btStar4;
@property (nonatomic) IBOutlet UIButton* btStar5;
@property (nonatomic) id delegate;
@property (nonatomic) NSIndexPath* currentPath;
- (void)setupData:(id)dataObj;
- (IBAction)onVerdetalhesPresserd:(id)sender;
@end
@protocol AcoesHistoricoCellDelegate <NSObject>

- (void)onVerDetalhesHistoryDidTouch:(NSIndexPath*)indexPath;

@end
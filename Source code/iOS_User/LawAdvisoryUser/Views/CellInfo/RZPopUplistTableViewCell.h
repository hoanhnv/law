//
//  RZTableViewCell.h
//  RZCellSizeManagerDemo
//
//  Created by Alex Rouse on 12/20/13.
//  Copyright (c) 2013 Raizlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RZPopUplistTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel* txtTitle;
+ (NSString *)reuseIdentifier;

+ (UINib *)reuseNib;

+ (CGFloat)estimatedCellHeight;
- (void)setCellData:(NSString *)title;
@end

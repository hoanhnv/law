//
//  InfoCell.m
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "InfoCell.h"

@implementation InfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  RZTableViewCell.m
//  RZCellSizeManagerDemo
//
//  Created by Alex Rouse on 12/20/13.
//  Copyright (c) 2013 Raizlabs. All rights reserved.
//

#import "RZPopUplistTableViewCell.h"

@interface RZPopUplistTableViewCell ()

@end

@implementation RZPopUplistTableViewCell

+ (NSString *)reuseIdentifier
{
    static NSString* s_reuseIdentifier = nil;
    if (!s_reuseIdentifier)
    {
        s_reuseIdentifier = NSStringFromClass([RZPopUplistTableViewCell class]);
    }
    return s_reuseIdentifier;
}

+ (UINib *)reuseNib
{
    UINib* nib = [UINib nibWithNibName:[RZPopUplistTableViewCell reuseIdentifier] bundle:nil];
    return nib;
}

+ (CGFloat)estimatedCellHeight
{
    return 44.0f;
}
- (void)setCellData:(NSString *)title
{
    self.txtTitle.text = title;
}
@end

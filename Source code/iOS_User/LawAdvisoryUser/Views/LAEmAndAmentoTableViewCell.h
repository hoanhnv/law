//
//  LAEmAndAmentoTableViewCell.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAEmAndAmentoTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UIButton* btVerdetalhes;
@property (nonatomic) IBOutlet UILabel* lbStatus;
@property (nonatomic) IBOutlet UILabel* lbCategoria;
@property (nonatomic) IBOutlet UILabel* lbAssunto;
@property (nonatomic) IBOutlet UILabel* lbProcesso;
@property (nonatomic) IBOutlet UILabel* lbData;
@property (nonatomic) IBOutlet UILabel* lbCliente;
@property (nonatomic) IBOutlet UIButton* btStatus;
@property (nonatomic) id delegate;
@property (nonatomic) NSIndexPath* currentPath;
- (void)setupData:(id)dataObj;
- (IBAction)onVerdetalhesPresserd:(id)sender;
@end
@protocol AcoesEmAndamentoCellDelegate <NSObject>

- (void)onVerDetalhesEmandAmentoDidTouch:(NSIndexPath*)indexPath;

@end
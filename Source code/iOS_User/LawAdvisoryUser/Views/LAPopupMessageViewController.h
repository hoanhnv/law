//
//  LAPopupMessageViewController.h
//  LawAdvisoryUser
//
//  Created by Apple on 8/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface LAPopupMessageViewController : LABaseViewController
@property (nonatomic) id delegate;
@property (nonatomic) IBOutlet UILabel* lbContent;
@property (nonatomic) IBOutlet UIButton* btExit;
@property (nonatomic) IBOutlet NSString* content;
@property (nonatomic) IBOutlet NSString* titleButton;
- (IBAction)onExitPressed:(id)sender;
@end
@protocol MessagePopupDelegate <NSObject>

- (void)onFinishPopupPressed;

@end
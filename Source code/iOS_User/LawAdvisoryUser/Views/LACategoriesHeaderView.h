//
//  LACategoriesHeaderView.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/9/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LACategoriesHeaderView : UIView
@property (nonatomic) IBOutlet UIImageView* imgIconHeader;
@property (nonatomic) IBOutlet UILabel* txtTitle;
@end

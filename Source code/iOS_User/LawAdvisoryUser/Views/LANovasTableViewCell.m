//
//  LANovasTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LANovasTableViewCell.h"

@implementation LANovasTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)onAceiterPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onAceiterDidTouch:)])
    {
        [self.delegate onAceiterDidTouch:self.currentPath];
    }
}
- (IBAction)onRecusarPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onRecusarDidTouch:)])
    {
        [self.delegate onRecusarDidTouch:self.currentPath];
    }
}
- (void)setupData:(id)dataObj
{
    
}
@end

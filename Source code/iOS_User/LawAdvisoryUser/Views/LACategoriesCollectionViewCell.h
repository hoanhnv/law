//
//  LACategoriesCollectionViewCell.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/9/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface LACategoriesCollectionViewCell : UICollectionViewCell
@property (nonatomic) IBOutlet UILabel* txtTitleCat;
@property (nonatomic) IBOutlet UIImageView* imgIndicator;
@property (nonatomic) IBOutlet UIView* viewUnderline;
@end

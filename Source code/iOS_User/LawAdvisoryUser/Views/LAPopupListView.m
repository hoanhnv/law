//
//  LAPopupListView.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/15/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPopupListView.h"
#import "RZCellSizeManager.h"

@implementation LAPopupListView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self configureDataSource];
    [self configureTableView];

}
- (void)configureDataSource
{
    self.sizeManager = [[RZCellSizeManager alloc] init];
    [self.sizeManager registerCellClassName:@"RZPopUplistTableViewCell"
                               withNibNamed:nil
                             forObjectClass:nil
                     withConfigurationBlock:^(RZPopUplistTableViewCell* cell, id object) {
                         [cell setCellData:object];
                     }];
}
- (void)configureTableView
{
    [self.tbViewList registerNib:[RZPopUplistTableViewCell reuseNib] forCellReuseIdentifier:[RZPopUplistTableViewCell reuseIdentifier]];
    
}
- (void)reloadPressed
{
    [self configureDataSource];
    
    // In this case we are going to invalidate our entire height cache since we are changing the entire datasource.  It is possible
    //  to just invalidate a speific indexpath or an array of them, and you should so long as you know what is being invalidated.
    [self.sizeManager invalidateCellSizeCache];
    
    [self.tbViewList reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = nil;
    id object = [self.arrTitle objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[RZPopUplistTableViewCell class]])
    {
        RZPopUplistTableViewCell* tableCell = [tableView dequeueReusableCellWithIdentifier:[RZPopUplistTableViewCell reuseIdentifier]];
        [tableCell setCellData:object];
        cell = tableCell;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Retrieve our object to give to our size manager.
    id object = [self.arrTitle objectAtIndex:indexPath.row];
    
    // Since we are using a tableView we are using the cellHeightForObject:indexPath: method.
    //  It uses the indexPath as the key for cacheing so it is important to pass in the correct one.
    return [self.sizeManager cellHeightForObject:object indexPath:indexPath];
}

// If you have very complex cells or a large number implementing this method speeds up initial load time.
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [RZPopUplistTableViewCell estimatedCellHeight];
}
@end

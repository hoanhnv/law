//
//  LACategoriesTableViewCell.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/9/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LACategoriesTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) IBOutlet NSMutableArray* arrTitle;
@property (nonatomic) IBOutlet UICollectionView* clCategories;
@property (nonatomic) id delegate;
@property (nonatomic) NSIndexPath* indexPath;
@end
@protocol CategoriesChooseDelegate <NSObject>

- (void)onChooseAtIndex:(NSIndexPath*)indexPath;

@end
//
//  LAPageViewControl.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#define WIDTH_DOT 8
#define HEIGHT_DOT 8
@interface LAPageViewControl : UIView
{
    NSMutableArray* arrImage;
    UIImage* activeImage;
    UIImage* inactiveImage;
}
@property(nonatomic, retain) UIImage* activeImage;
@property(nonatomic, retain) UIImage* inactiveImage;
@property(nonatomic) NSInteger iNumberPage;
- (void)setUpImage;
- (void)setCurrentPage:(NSInteger)page;
- (void)setUpImageActive:(NSString*)activeImageHere andInActive:(NSString*)inActiveImageHere;
@end

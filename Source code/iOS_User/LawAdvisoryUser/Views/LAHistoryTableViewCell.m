//
//  LAHistoryTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAHistoryTableViewCell.h"

@implementation LAHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)onVerdetalhesPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onVerDetalhesHistoryDidTouch:)])
    {
        [self.delegate onVerDetalhesHistoryDidTouch:self.currentPath];
    }
}
- (void)setupData:(id)dataObj
{
    
}
- (IBAction)onChooseStar:(id)sender
{
    NSMutableArray* arr = [NSMutableArray arrayWithObjects:self.btStar1,self.btStar2,self.btStar3,self.btStar4,self.btStar5, nil];
    UIButton* bt = (UIButton*)sender;
    NSInteger index = [arr indexOfObject:bt];
    for (int i = 0; i < arr.count; i ++)
    {
        UIButton* btCheck = [arr objectAtIndex:i];
        if (i <= index)
        {
            [btCheck setSelected:YES];
        }
        else
        {
            [btCheck setSelected:NO];
        }
    }
}
@end

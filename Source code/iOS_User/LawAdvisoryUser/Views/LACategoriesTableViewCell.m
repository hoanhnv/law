//
//  LACategoriesTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/9/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACategoriesTableViewCell.h"
#import "LACategoriesCollectionViewCell.h"
@implementation LACategoriesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initData];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)initData
{
    [self.clCategories registerNib:[UINib nibWithNibName:NSStringFromClass([LACategoriesCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LACategoriesCollectionViewCell class])];
    [self.clCategories reloadData];
}
#pragma mark COLLECTION VIEW DATASOURCE
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrTitle.count;
}
- (CGSize)collectionView:(UICollectionView *)_collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    return CGSizeMake(160, CGRectGetHeight(self.frame) - 10);
    if (self.arrTitle.count > indexPath.row)
    {
        CGSize wordSize = [[self.arrTitle objectAtIndex:indexPath.row] sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]}];
        return CGSizeMake(wordSize.width + 25, CGRectGetHeight(self.frame) - 10);
        
    }
    else
    {
        return CGSizeMake(160, CGRectGetHeight(self.frame) - 10);
    }
    
}
// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LACategoriesCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LACategoriesCollectionViewCell class]) forIndexPath:indexPath];
    if (self.arrTitle.count > indexPath.row)
    {
        NSString* title = [self.arrTitle objectAtIndex:indexPath.row];
        cell.txtTitleCat.text = title;
        
    }
    switch (self.indexPath.section)
    {
        case 0:
        {
            [cell.viewUnderline setBackgroundColor:COLOR_CATEGORY_HEADER_TITLE_1];
        }
            break;
        case 1:
        {
            [cell.viewUnderline setBackgroundColor:COLOR_CATEGORY_HEADER_TITLE_2];
        }
            break;
        default:
            break;
    }
    return cell;
}
#pragma mark COLLECTION VIEW DELEGATE
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onChooseAtIndex:)])
    {
        NSIndexPath* indexPathHere = [NSIndexPath indexPathForRow:indexPath.row inSection:self.indexPath.section];
        [self.delegate onChooseAtIndex:indexPathHere];
    }
}
@end

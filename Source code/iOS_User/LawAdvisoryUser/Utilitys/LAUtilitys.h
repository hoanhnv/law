//
//  LAUtilitys.h
//  LawAdvisoryUser
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LAUtilitys : NSObject
#pragma mark BUTTON UTILITYS
+ (void)setButtonTextUnderLine:(UIButton*)sender;
#pragma mark STRING UTILITYS
+ (BOOL)isEmptyOrNull:(NSString*) cached;
+ (NSString*)getDefaultString:(NSString*)sourceString;
+ (BOOL)validateEmailWithString:(NSString*)checkString;
+ (BOOL)NullOrEmpty:(NSString *)strInput;
#pragma mark UITEXTFIELD UTILITYS
+ (void)setRightImage:(NSString*)imgName forTextField:(UITextField*)textField;
+ (void) setPaddingForTextField:(UITextField *) textField andPaddingLeftMargin:(CGFloat)left;
#pragma mark INTRO UTILITYS
+ (void)setIntroIsPresented;
+ (BOOL)checkIntroIsPresent;
#pragma mark DATE UTILITY
+ (NSDate*)getNSDateFromDateString:(NSString*)dateTxt;
+ (NSString*)getDateStringFromNSDate:(NSDate*)date;
+ (NSString*)getDateStringFromTimeInterVal:(double)time;

+ (UIColor *)jusTapColor;
@end

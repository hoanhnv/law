//
//  LAUtilitys.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAUtilitys.h"
#import "UIColor+Law.h"

@implementation LAUtilitys
#pragma mark BUTTON UTILITYS
+ (void)setButtonTextUnderLine:(UIButton*)sender
{
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:sender.titleLabel.text];
    // making text property to underline text-
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    // using text on button
    [sender setAttributedTitle: titleString forState:UIControlStateNormal];
}
#pragma mark STRING UTILITYS
+ (BOOL)isEmptyOrNull:(NSString*) cached
{
    if ([cached class] == [NSNull class] || cached == Nil || [cached isKindOfClass:[NSNull class]])
    {
        return YES;
    }
    NSString* trim = [cached stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([trim length] == 0) {
        return YES;
    }
    return NO;
}
+ (NSString*)getDefaultString:(NSString*)sourceString
{
    if ([LAUtilitys isEmptyOrNull:sourceString] || [LAUtilitys NullOrEmpty:sourceString])
    {
        return @"";
    }
    return sourceString;
}
+ (BOOL)validateEmailWithString:(NSString*)checkString {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)NullOrEmpty:(NSString *)strInput{
    BOOL bResult = NO;
    if (strInput == nil || strInput == [NSNull class]
        || [strInput isEqualToString:@""]
        || [strInput isEqualToString:@"(null)"]) {
        
        bResult = YES;
    }
    return bResult;
}
#pragma mark UITEXTFIELD UTILITYS
//Create image right mode for textfield
+ (void)setRightImage:(NSString*)imgName forTextField:(UITextField*)textField
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(textField.frame)) - 15, 13, 13)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    imgView.image = [UIImage imageNamed:imgName];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame))];
    [paddingView addSubview:imgView];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    [textField setRightView:paddingView];
    CGPoint centerIMG = imgView.center;
    centerIMG.y = paddingView.center.y;
    [imgView setCenter:centerIMG];
}
+ (void) setPaddingForTextField:(UITextField *) textField andPaddingLeftMargin:(CGFloat)left {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left, CGRectGetHeight(textField.bounds))];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
#pragma mark INTRO UTILITYS
+ (void)setIntroIsPresented
{
    NSUserDefaults* defaultUser = [NSUserDefaults standardUserDefaults];
    [defaultUser setObject:[NSNumber numberWithBool:YES] forKey:@"introShow"];
    [defaultUser synchronize];
}
+ (BOOL)checkIntroIsPresent
{
    BOOL isCheck = NO;
    NSUserDefaults* defaultUser = [NSUserDefaults standardUserDefaults];
    NSNumber* nNumber = [defaultUser objectForKey:@"introShow"];
    if (nNumber && [nNumber boolValue])
    {
        isCheck = YES;
    }
    return isCheck;
}
#pragma mark DATE UTILITY
+ (NSDate*)getNSDateFromDateString:(NSString*)dateTxt
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/DD/YYYYY"];
    NSDate *date = [dateFormat dateFromString:dateTxt];
    return date;
}
+ (NSString*)getDateStringFromNSDate:(NSDate*)date
{
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"MM/DD/YYYYY"]; // Date formater
    NSString *dateTXT = [dateformate stringFromDate:date];
    return dateTXT;
}
+ (NSString*)getDateStringFromTimeInterVal:(double)time
{
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"MM/DD/YYYYY"]; // Date formater
    NSString *dateTXT = [dateformate stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
    return dateTXT;
}

+(UIColor *)jusTapColor{
    return [UIColor colorWithHexString:MAIN_COLOR];
}
@end

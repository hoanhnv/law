//
//  LAConstant.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TWITTER_CONSUMER_KEY @"gzzz7gvbSva4arm9ZxKvsIdZB"
#define TWITTER_CONSUMER_SECRET_KEY @"O6gQt4ig1hnIbNCzMxrsVIy23DZN7RAbUrbafJvn9M7Uw8YNGw"

#define STRING_EMPTY    @""

//COLOR
#define MAIN_COLOR @"#4bbbfd"
#define MAIL_CONTACT   @"hoanhvp@gmail.com"
#define NAVIGATION_COLOR [UIColor colorWithRed:75.0/255.0 green:187.0/255.0 blue:253.0/255.0 alpha:1.0]
#define NAVIGATION_COLOR_GREEN [UIColor colorWithRed:46.0/255.0 green:138.0/255.0 blue:90.0/255.0 alpha:1.0]
#define COLOR_CATEGORY_HEADER_TITLE_1 [UIColor colorWithRed:40.0/255.0 green:139.0/255.0 blue:88.0/255.0 alpha:1.0]
#define COLOR_CATEGORY_HEADER_TITLE_2 [UIColor colorWithRed:255.0/255.0 green:162.0/255.0 blue:0.0/255.0 alpha:1.0]
//HEIGHT
#define HEIGHT_BIG_NAVIGATION 90.0f

#define FORGOT_PASS_SEND_EMAIL @"forgot pass mail was sent"
#define FORGOT_PASS_SEND_EMAIL_TITLE_BUTTON @"IR PARA TELA DE ACESSO"
#define REGISTER_PRESSED_MESSAGE @"Foi enviada uma nova senha para o seu email"
#define REGISTER_PRESSED_TITLE_BUTTON @"IR PARA TELA DE ACESSO"

// NOTIFICATION KEY
#define NOTIFICATION_KEY_CHANGE_ACCOUNT @"accountChange"
// SegueID
#define PushToRegister @"PushToRegister"
#define PushToLogin @"PushToLogin"

@interface LAConstant : NSObject

@end

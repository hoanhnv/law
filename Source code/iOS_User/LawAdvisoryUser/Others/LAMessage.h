//
//  LAMessage.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MSG_0001    @"The response return token null"
#define MSG_0002    @"Can not signIn with google"
#define MSG_0003    @"No subjects to contact"
#define MSG_0004    @"The message must be has value"
#define MSG_005     @"E-mail não cadastrado!\n Tente um novo e-mail."
#define MSG_006     @"Foi enviada uma nova senha para o seu email %@.\nSigas as instruções para alterá-la"

@interface LAMessage : NSObject

@end

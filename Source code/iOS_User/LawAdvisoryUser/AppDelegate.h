//
//  AppDelegate.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAMainTabbarViewController.h"
#import "LABaseViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (AppDelegate *)sharedDelegate;
@property (nonatomic) LAMainTabbarViewController* mainTabbar;
@property (nonatomic) LABaseViewController* currentViewController;
@property (nonatomic, strong) UINavigationController *nav;
-(void)goLoginViewController;
@end


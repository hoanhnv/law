//
//  AppDelegate.m
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "LAAddressService.h"
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import "LALoginViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>

static NSString * const kClientID =
@"759901828094-u9ck4bouh1sdfkakqcafq5gldmcaqnvs.apps.googleusercontent.com";

@interface AppDelegate ()

@end

@implementation AppDelegate

//SINGLETON
+ (AppDelegate *)sharedDelegate
{
    static id <UIApplicationDelegate> _sharedDelegate;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDelegate = [[UIApplication sharedApplication] delegate];
    });
    return (AppDelegate *)_sharedDelegate;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Fabric with:@[[Twitter class]]];
    [GIDSignIn sharedInstance].clientID = kClientID;
    // Remove bottom line view of navigationbar
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [UINavigationBar appearance].titleTextAttributes = @{
                                                         NSFontAttributeName:[UIFont systemFontOfSize:21.0f],
                                                         NSForegroundColorAttributeName: [UIColor whiteColor]
                                                         };
    NSLog(@"Token:%@",[LADataCenter shareInstance].currentAccessToken);
    [self goLoginViewController];
    
    //    if (![LADataCenter isFirstTime]) {
    //        NSString *strAccessToken = [LADataCenter shareInstance].currentAccessToken;
    //        NSLog(@"%@",strAccessToken);
    //        if (![LAUtilitys NullOrEmpty:strAccessToken]) {
    //            // User sigout
    //            [self goHomeViewController];
    //        }
    //        else{
    //            // User Logged
    //            [self goLoginViewController];
    //        }
    //    }
    //    else{
    //        [self goIntroViewController];
    //    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    UIBackgroundTaskIdentifier taskID = [application beginBackgroundTaskWithExpirationHandler:^{
        // Code to ensure your background processing stops executing
        // so it reaches the call to endBackgroundTask:
    }];
    
    // Put the code you want executed in the background here
    
    if (taskID != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:taskID];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                       openURL:url
                                             sourceApplication:sourceApplication
                                                    annotation:annotation]) {
        
        return  [[FBSDKApplicationDelegate sharedInstance] application:application
                                                               openURL:url
                                                     sourceApplication:sourceApplication
                                                            annotation:annotation];
    }
    else if([[GIDSignIn sharedInstance] handleURL:url
                                sourceApplication:sourceApplication
                                       annotation:annotation]){
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
    return NO;
}


-(void)goHomeViewController{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LAMainTabbarViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"LAMainTabbarViewControllery"];
    self.window.rootViewController = rootViewController;
    [self.window makeKeyAndVisible];
}

-(void)goLoginViewController{
    self.nav = (UINavigationController *) self.window.rootViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.nav pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"LALoginViewController"] animated:NO];
    self.window.rootViewController = self.nav;
}
-(void)goIntroViewController{
    self.nav = (UINavigationController *) self.window.rootViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.nav pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"LAIntroViewController"] animated:NO];
    self.window.rootViewController = self.nav;
}

@end

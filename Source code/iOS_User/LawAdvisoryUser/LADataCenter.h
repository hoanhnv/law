//
//  LADataCenter.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HOANHNV. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TOKEN_AUTHORIZATION_KEY @"token"
#define SHOW_SCREEN_INTRODUCTION_FLAG @"hasLaunchedOnce"

@interface LADataCenter : NSObject

@property (nonatomic,readonly,getter=currentAccessToken) NSString *accessToken;
@property (nonatomic, strong) NSString *facebookToken;
@property (nonatomic, strong) NSString *twitterID;
@property (nonatomic, strong) NSString *googlePlusToken;

+(instancetype)shareInstance;

-(void)saveToken:(NSString *)token error:(NSError**)err;
-(void)updateFlagShowScreenIntroduction:(BOOL)bValue;
+ (BOOL)isFirstTime;
-(void)clearKeyChain;

@end

//
//  UIFont+Law.h
//  JustapLawyer
//
//  Created by Mac on 9/30/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Law)

+(UIFont*)robotoBlack:(CGFloat)fontSize;
+(UIFont*)robotoBlackItalic:(CGFloat)fontSize;
+(UIFont*)robotoBold:(CGFloat)fontSize;
+(UIFont*)robotoBoldItalic:(CGFloat)fontSize;
+(UIFont*)robotoLight:(CGFloat)fontSize;
+(UIFont*)robotoLightItalic:(CGFloat)fontSize;
+(UIFont*)robotoMedium:(CGFloat)fontSize;
+(UIFont*)robotoMediumItalic:(CGFloat)fontSize;
+(UIFont*)robotoRegular:(CGFloat)fontSize;
+(UIFont*)robotoThin:(CGFloat)fontSize;
+(UIFont*)robotoThinItalic:(CGFloat)fontSize;

@end

//
//  UIColor+Law.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/23/16.
//  Copyright © 2016 HOANHNV. All rights reserved.
//

#import "UILabel+Law.h"

@implementation UILabel (Law)
+ (void)setHightLightText:(UILabel*)labelSource andTextHightLight:(NSString*)textHighlight
{
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:labelSource.text];
    textHighlight = [textHighlight uppercaseString];
    NSString* string = [labelSource.text uppercaseString];
    NSRange searchRange = NSMakeRange(0,string.length);
    NSRange foundRange;
    while (searchRange.location < string.length) {
        searchRange.length = string.length-searchRange.location;
        foundRange = [string rangeOfString:textHighlight options:nil range:searchRange];
        if (foundRange.location != NSNotFound) {
            // found an occurrence of the substring! do stuff here
            searchRange.location = foundRange.location+foundRange.length;
            [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:foundRange];
            NSLog(@"(%lu-%lu)",(unsigned long)foundRange.location,(unsigned long)foundRange.length);
        } else {
            // no more substring to find
            break;
        }
    }
    [labelSource setAttributedText:mutableAttributedString];
}
+ (void)searchText:(NSString*)string andTextHightLight:(NSString*)textHighlight
{
    textHighlight = [textHighlight uppercaseString];
    string = [string uppercaseString];
    NSRange searchRange = NSMakeRange(0,string.length);
    NSRange foundRange;
    while (searchRange.location < string.length) {
        searchRange.length = string.length-searchRange.location;
        foundRange = [string rangeOfString:textHighlight options:nil range:searchRange];
        if (foundRange.location != NSNotFound) {
            // found an occurrence of the substring! do stuff here
            searchRange.location = foundRange.location+foundRange.length;
            NSLog(@"(%lu-%lu)",(unsigned long)foundRange.location,(unsigned long)foundRange.length);
        } else {
            // no more substring to find
            break;
        }
    }
}
@end

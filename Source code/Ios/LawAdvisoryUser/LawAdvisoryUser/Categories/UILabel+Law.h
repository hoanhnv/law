//
//  UIColor+Law.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/23/16.
//  Copyright © 2016 HOANHNV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Law)
+ (void)setHightLightText:(UILabel*)labelSource andTextHightLight:(NSString*)textHighlight;
+ (void)searchText:(NSString*)string andTextHightLight:(NSString*)textHighlight;
@end

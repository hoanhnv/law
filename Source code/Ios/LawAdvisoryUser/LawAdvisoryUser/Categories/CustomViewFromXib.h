//
//  PSCustomViewFromXib.h
//  CustomView
//
//  Created by Paul Solt on 4/28/14.
//  Copyright (c) 2014 Paul Solt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewFromXib : UIView

@property (nonatomic, strong) CustomViewFromXib *customView;

@end

//
//  NSDictionary+Law.m
//  Law
//
//  Created by Hoang Mai Long on 4/28/14.
//  Copyright (c) 2014 SUSOFT. All rights reserved.
//

#import "NSDictionary+Law.h"

@implementation NSDictionary (Law)

- (NSString *)stringForKey:(NSString *)key {
    if (![[self objectForKey:key] isEqual:[NSNull null]]) {
        return [self objectForKey:key];
    }
    return @"";
}

- (NSInteger)integerForKey:(NSString *)key {
//    return [[self objectForKey:key] integerValue];
    NSObject* msgo = [self objectForKey:key];
    int value;
    if ([msgo isKindOfClass:[NSNull class]])
        value = 0;
    else value = [(NSNumber*)msgo intValue];
    
    return value;

}
- (double)doubleForKey:(NSString *)key {
    //    return [[self objectForKey:key] integerValue];
    NSObject* msgo = [self objectForKey:key];
    double value;
    if ([msgo isKindOfClass:[NSNull class]])
        value = 0;
    else value = [(NSNumber*)msgo doubleValue];
    
    return value;
    
}
- (BOOL)boolForKey:(NSString *)key {
    return [[self objectForKey:key] boolValue];
}


@end

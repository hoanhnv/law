//
//  UITabBarController+ShowHideBar.h
//  JustapLawyer
//
//  Created by Mac on 11/6/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (ShowHideBar)
- (void) setHidden:(BOOL)hidden;

@end

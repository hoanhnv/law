//
//  UIFont+Law.m
//  JustapLawyer
//
//  Created by Mac on 9/30/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "UIFont+Law.h"

@implementation UIFont (Law)

+(UIFont *)robotoBold:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-Bold" size:fontSize];
}
+(UIFont *)robotoBoldItalic:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-BoldItalic" size:fontSize];
}
+(UIFont *)robotoBlack:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Roboto-Black" size:fontSize];
    
}
+(UIFont *)robotoBlackItalic:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-BlackItalic" size:fontSize];
}

+(UIFont *)robotoThin:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-Thin" size:fontSize];
    
}
+(UIFont *)robotoThinItalic:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-ThinItalic" size:fontSize];
    
}
+(UIFont *)robotoLight:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-Light" size:fontSize];
    
}
+(UIFont *)robotoLightItalic:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-LightItalic" size:fontSize];
    
}
+(UIFont *)robotoMedium:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-Medium" size:fontSize];
    
}
+(UIFont *)robotoMediumItalic:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-MediumItalic" size:fontSize];
    
}
+(UIFont *)robotoRegular:(CGFloat)fontSize{
    return [UIFont fontWithName:@"Roboto-Regular" size:fontSize];
    
}

@end

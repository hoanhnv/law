//
//  NSDictionary+Law.h
//  Law
//
//  Created by Hoang Mai Long on 4/28/14.
//  Copyright (c) 2014 SUSOFT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Law)

- (NSString *)stringForKey:(NSString *)key;
- (NSInteger)integerForKey:(NSString *)key;
- (BOOL)boolForKey:(NSString *)key;
- (double)doubleForKey:(NSString *)key;
@end

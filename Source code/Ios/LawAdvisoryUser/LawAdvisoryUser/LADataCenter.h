//
//  LADataCenter.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HOANHNV. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TOKEN_AUTHORIZATION_KEY @"token"
#define SHOW_SCREEN_INTRODUCTION_FLAG @"hasLaunchedOnce"

#define CURRENT_PLAN_CART_ITEM   @"CurrentPlanCartItem"
#define CURRENT_SIGNATURE_CART_ITEM   @"CurrentSignatureCartItem"
#define CURRENT_DISTRICT_CART_ITEM   @"CurrentDistrictCartItem"
#define CURRENT_SIGNATURE_REGISTER   @"CurrentSignatureRegister"

@interface LADataCenter : NSObject

@property (nonatomic,readonly,getter=currentAccessToken) NSString *accessToken;
@property (nonatomic,readonly,getter=currentStatus) NSString *curentStaus;

@property (nonatomic, strong) NSString *facebookToken;
@property (nonatomic, strong) NSString *twitterID;
@property (nonatomic, strong) NSString *googlePlusToken;

@property (nonatomic, strong) NSString *registerToken;

+(instancetype)shareInstance;

-(void)saveToken:(NSString *)token error:(NSError**)err;
-(void)saveEmail:(NSString *)email error:(NSError**)err;

-(void)saveAccountStatus:(NSString *)status error:(NSError**)err;

-(NSString*)currentEmail;

-(void)updateFlagShowScreenIntroduction:(BOOL)bValue;

-(void)saveTimerValue:(double)actionId withValue:(NSString*)strTime;
-(NSString*)getTimerValue:(double)actionId;

+(BOOL)isFirstTime;
-(void)clearKeyChain;

@end

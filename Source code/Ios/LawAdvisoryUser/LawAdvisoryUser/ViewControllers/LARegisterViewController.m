//
//  LARegisterViewController.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/28/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LARegisterViewController.h"
#import "MBProgressHUD.h"
#import "LAAccountObj.h"
#import "LACountryViewController.h"
#import "LAAccountService.h"
#import "UIViewController+MJPopupViewController.h"
#import "LAPopupMessageViewController.h"
#import "AppDelegate.h"
#import "LAUFs.h"
#import "MainService.h"
#import "DynamicTitleView.h"
#import "LAStatusAccountObj.h"
#import "Photos/Photos.h"
#import "IQKeyboardManager.h"
#import "UIImage+ResizeMagick.h"
#import "LABankInfo.h"
#import "LABankObj.h"
#import "TermAndServiceViewController.h"


#define MAX_SIZE_IMAGE_DEFAULT 1024
#define MAX_SIZE_IMAGE_AVATAR 512

@interface LARegisterViewController ()<UITextFieldDelegate,DynamicTitleViewDelegate>
{
    UIDatePicker *datePicker;
    CGRect currentFrame;
    LABankInfo *bankInfo;
    NSAttributedString *strLink;
}
@end

@implementation LARegisterViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShouldBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.txtMobilePhone setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self.txtPhone setClearButtonMode:UITextFieldViewModeWhileEditing];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uploadImage:) name:ARGS_UPLOAD_IMAGE object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeImage:) name:ARGS_REMOVE_IMAGE object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
    [self.lbDescriptionLink addGestureRecognizer:tap];
    [self setupText];
    currentFrame = self.scrFullData.frame;
    currentFrame.size.height += 50;
    self.scrFullData.frame = currentFrame;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText:@"Done"];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if (self.cbAcceptTerm.isSelected)
    {
        [self.btnConfirm setBackgroundColor:[LAUtilitys jusTapColor]];
    }
    else
    {
        [self.btnConfirm setBackgroundColor:[UIColor lightGrayColor]];
    }
    UITapGestureRecognizer *tapShowMail = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapShowMail)];
    [self.vShowMail addGestureRecognizer:tapShowMail];
    
    UITapGestureRecognizer *tapShowCellPhone = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeShowCellPhone)];
    [self.vShowCellPhone addGestureRecognizer:tapShowCellPhone];
    
    UITapGestureRecognizer *tapShowPhone = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeShowPhone)];
    [self.vShowPhone addGestureRecognizer:tapShowPhone];
    
    [self.vDynamicTitle setDelegate:self];
    [self updateViewIfNeed];
    self.txtEmail.enabled = YES;
    
}
-(void)viewDidAppear:(BOOL)animated{
    currentFrame = self.scrFullData.frame;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupText
{
    self.lbDescriptionLink.userInteractionEnabled = YES;
    UIColor *color1 = [UIColor grayColor];
    NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : color1 ,NSFontAttributeName :[UIFont robotoRegular:14]};
    UIColor *color2 = [LAUtilitys jusTapColor];
    NSDictionary *attrs2 = @{ NSForegroundColorAttributeName : color2 ,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),NSFontAttributeName :[UIFont robotoRegular:14],@"myCustomTag" : @(YES)};
    UIColor *color3 = [UIColor grayColor];
    NSDictionary *attrs3 = @{ NSForegroundColorAttributeName : color3,NSFontAttributeName :[UIFont robotoRegular:14]};
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Concordo com os, " attributes:attrs1];
    strLink = [[NSAttributedString alloc] initWithString:@" termos e condições " attributes:attrs2];
    
    //    NSAttributedString *str3 = [[NSAttributedString alloc] initWithString:@"\nlorem ipsum dolor lorem ipus." attributes:attrs3];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] init];
    [string appendAttributedString:str1];
    [string appendAttributedString:strLink];
    //    [string appendAttributedString:str3];
    self.lbDescriptionLink.attributedText = string;
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)initUI
{
    [self setupFrame];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtCity];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtUF];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtNewState];
}
- (void)initData
{
    arrRequiredField = [NSMutableArray arrayWithObjects:self.txtEmail,self.txtPassWord,self.txtName,self.txtCPF,self.txtCEP,self.txtAddress,self.txtNumber,self.txtUF,self.txtCity,self.txtMobilePhone,self.txtNumberOAB,self.txtNewState,self.txtBankAgenda,self.txtBankConta,self.txtBankVerify, nil];
    arrEmail = [NSMutableArray arrayWithObjects:self.txtEmail, nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didselectAddress:) name:@"didSelectCountry" object:nil];
    [self setupData];
    [self getUFs];
    [self loadBank];
    [self onShowBirthDaySelect];
    //    [self fakeDataInit];
}
- (void)setupData
{
    if (self.currentAccount)
    {
        self.txtEmail.text = [LAUtilitys getDefaultString:self.currentAccount.accountEmail];
        self.txtName.text = [LAUtilitys getDefaultString:self.currentAccount.accountName];
        self.txtAbout.text = [LAUtilitys getDefaultString:self.currentAccount.about];
        self.txtSite.text = [LAUtilitys getDefaultString:self.currentAccount.accountSite];
        self.txtAddress.text = [LAUtilitys getDefaultString:self.currentAccount.accountCity];
        if (self.currentAccount.accountDateOfBirth != 0)
        {
            self.txtDateOfBirth.text = [LAUtilitys getDateStringFromTimeInterVal:self.currentAccount.accountDateOfBirth withFormat:FORMAT_MM_DD_YY];
        }
        
    }
}
- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id value = [textView.attributedText attribute:@"myCustomTag" atIndex:characterIndex effectiveRange:&range];
        
        // Handle as required...
        if (value) {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
            TermAndServiceViewController *termAndServiceVC  = [storyBoard instantiateViewControllerWithIdentifier:@"TermAndServiceViewController"];
            [self.navigationController pushViewController:termAndServiceVC animated:YES];
        }
    }
}
#pragma mark Auto correct
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.txtCPF) {
        if (newString.length == 4 || newString.length == 8)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"."];
            }
        }
        else if (newString.length == 12)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 14;
        }
        return YES;
    }
    else if (textField == self.txtCEP)
    {
        if (newString.length == 6)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 9;
        }
        return YES;
    }
    else if (textField == self.txtMobilePhone)
    {
        if (range.location == 3 || range.location == 9 || (range.location == 0 && [self.txtMobilePhone.text containsString:@"("]))
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength < textField.text.length)
            {
                if (range.location == 9 && ![newString containsString:@"."])
                {
                    return YES;
                }
                if (range.location == 3 && ![newString containsString:@")"])
                {
                    return YES;
                }
                return NO;
            }
        }
        if (range.location == 2)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [NSString stringWithFormat:@"(%@)",textField.text];
            }
            else
            {
                textField.text = [textField.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
            }
        }
        else if (range.location == 9 && ![textField.text containsString:@"-"])
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 14;
        }
        return YES;
    }
    else if (textField == self.txtPhone)
    {
        if (range.location == 3 || range.location == 8 || (range.location == 0 && [self.txtPhone.text containsString:@"("]))
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength < textField.text.length)
            {
                if (newLength < textField.text.length)
                {
                    if (range.location == 8 && ![newString containsString:@"."])
                    {
                        return YES;
                    }
                    if (range.location == 3 && ![newString containsString:@")"])
                    {
                        return YES;
                    }
                    return NO;
                }
                return NO;
            }
        }
        if (range.location == 2)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [NSString stringWithFormat:@"(%@)",textField.text];
            }
            else
            {
                textField.text = [textField.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
            }
        }
        else if (range.location == 8 && ![textField.text containsString:@"-"])
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 13;
        }
        return YES;
    }
    else if(self.txtNumberOAB == textField){
        if (textField.text.length >= 8 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
    }
    else if(self.txtBankAgenda == textField){
        if (textField.text.length >= 5 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
    }
    else if(self.txtBankConta == textField){
        if (textField.text.length >= 20 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
        
    }
    else if(self.txtBankVerify == textField){
        if (textField.text.length >=1 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
        
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txtUF || textField == self.txtCity)
    {
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (self.txtCEP == textField) {
        [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        [MainService checkCEP:self.txtCEP.text withBlock:^(NSInteger errorCode, NSString *message, id data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                if ([data isKindOfClass:[NSDictionary class]] && [data count]) {
                    if (![LAUtilitys NullOrEmpty:data[@"uf"]]) {
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        [LAAddressService getlistCityWithUF:data[@"uf"] wCompleteBlock:^(NSInteger errorCode, NSString *message, id data, NSInteger total) {
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            if (data) {
                                listCity = [NSMutableArray arrayWithArray:data];
                            }
                        }];
                    }
                    self.txtUF.text = data[@"uf"];
                    self.txtAddress.text = data[@"logradouro"];
                    self.txtCity.text = data[@"localidade"];
                    self.txtNeighborhood.text = data[@"bairro"];
                }
            });
        }];
    }
    return YES;
}
- (void)getCityList
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [LAAddressService getlistCityWithUF:self.txtUF.text wCompleteBlock:^(NSInteger errorCode, NSString *message, id data, NSInteger total) {
        self.txtCity.text = STRING_EMPTY;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data) {
            listCity = [NSMutableArray arrayWithArray:data];
        }
    }];
}
-(void)getUFs{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchUFs:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        listUFS = [[LAUFs alloc]initWithDictionary:data];
    }];
}
- (void)setupFrame
{
    CGPoint pointCenter = self.scrFullData.center;
    pointCenter.x = self.view.center.x;
    [self.scrFullData setCenter:pointCenter];
    //CONTENT SIZE
    CGSize size = self.scrFullData.contentSize;
    size.height = CGRectGetMaxY(self.btnConfirm.frame) + 60;
    [self.scrFullData setContentSize:size];
    NSLog(@"current frame:%@",NSStringFromCGRect(self.scrFullData.frame));
    
    NSLog(@"size:%@",NSStringFromCGSize(self.scrFullData.frame.size));
}
- (BOOL)checkIsValid
{
    BOOL isCheck = YES;
    for (AwesomeTextField* txt in arrRequiredField)
    {
        if ([LAUtilitys isEmptyOrNull:txt.text])
        {
            [self showMessage:[NSString stringWithFormat:@"%@ inválido",txt.placeholder] withPoistion:nil];
            isCheck = NO;
            break;
        }
    }
    if (isCheck)
    {
        for (AwesomeTextField* txt in arrEmail)
        {
            if (![LAUtilitys validateEmailWithString:txt.text])
            {
                [self showMessage:[NSString stringWithFormat:@"%@ inválido",txt.placeholder] withPoistion:nil];
                isCheck = NO;
                break;
            }
        }
    }
    if (isCheck)
    {
        if (![LAUtilitys isValidCellPhone:self.txtMobilePhone.text])
        {
            [self showMessage:[NSString stringWithFormat:@"%@ inválido",self.txtMobilePhone.placeholder] withPoistion:nil];
            isCheck = NO;
        }
    }
    if (isCheck)
    {
        if (![LAUtilitys isValidCPF:_txtCPF.text])
        {
            [self showMessage:@" CPF* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if([self.txtCEP.text length]<=8){
            [self showMessage:@" CEP* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if(self.imgAttach1 == nil) {
            [self showMessage:@" Foto de perfil* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if(self.imgAttach2 == nil) {
            [self showMessage:@" Comprovante de endereço* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if(self.imgAttach3 == nil) {
            [self showMessage:@" Cópia da OAB* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if(self.imgAttach4 == nil) {
            [self showMessage:@" Cópia da OAB* inválido" withPoistion:nil];
            isCheck = NO;
        }
    }
    return isCheck;
}

- (IBAction)changeGetNew:(id)sender {
    UIButton* bt = (UIButton*)sender;
    [bt setSelected:!bt.selected];
}

- (void)changeShowCellPhone {
    [self.cbShowCellphone setSelected:!self.cbShowCellphone.selected];
}

- (void)tapShowMail {
    [self.ckShowEmail setSelected:!self.ckShowEmail.selected];
}

- (void)changeShowPhone{
    [self.cbShowPhone setSelected:!self.cbShowPhone.selected];
}

- (void)fakeDataInit
{
    self.txtEmail.text = @"hoanhvp@gmail.com";
    self.txtPassWord.text = @"123456";
    self.txtName.text = @"Hoanh";
    self.txtDateOfBirth.text = @"24/11/1990";
    self.txtCPF.text = @"123.123.123-11";
    self.txtAddress.text = @"Cau giay, Ha Noi";
    self.txtNumber.text = @"123";
    self.txtNeighborhood.text = @"NeightBorHood Here";
    self.txtUF.text = @"CE";
    self.txtCity.text = @"Fortaleza";
    self.txtMobilePhone.text = @"(85)99999-9999";
    self.txtPhone.text = @"(85)9999-9999";
    self.txtNumberOAB.text = @"12345678";
    self.txtSite.text = @"ttp://www.mobileplus.vn";;
    self.txtLinkedin.text = @"hoang mai long";
    self.txtCEP.text = @"12345-111";
    self.txtComplemento.text = @"Complement here";
    for (UITextField* txt in self.vDynamicTitle.arrData)
    {
        txt.text = @"Test title";
    }
}
- (IBAction)onRegisterPressed:(id)sender
{
    isSuccess = NO;
    objAccount = [LAAccountObj new];;
    [self.view endEditing:YES];
    if(self.cbAcceptTerm.isSelected){
    if ([self checkIsValid])
    {
        objAccount.accountEmail = self.txtEmail.text;
        objAccount.accountPassword = self.txtPassWord.text;
        objAccount.accountName = self.txtName.text;
        objAccount.accountCPF = self.txtCPF.text;
        if([LAUtilitys NullOrEmpty:self.txtDateOfBirth.text]){
            objAccount.accountDateOfBirth = 0;
        }
        else{
            objAccount.accountDateOfBirth = [LAUtilitys getNSDateFromDateString:self.txtDateOfBirth.text withFormat:FORMAT_DD_MM_YY].timeIntervalSince1970;
        }        objAccount.accountCEP = self.txtCEP.text;
        objAccount.accountAddress = self.txtAddress.text;
        objAccount.accountNumber = self.txtNumber.text;
        objAccount.accountNeighborhood = self.txtNeighborhood.text;
        objAccount.accountUF = self.txtUF.text;
        objAccount.accountCity = self.txtCity.text;
        objAccount.accountCellphone = self.txtMobilePhone.text;
        objAccount.accountPhone = self.txtPhone.text;
        objAccount.avataURL = @"";
        objAccount.accountNumberOAB = self.txtNumberOAB.text;
        objAccount.accountSite = self.txtSite.text;
        objAccount.accountLinkedin = self.txtLinkedin.text;
        objAccount.accountPostalCode = self.txtCEP.text;
        objAccount.accountComplement = self.txtComplemento.text;
        objAccount.state = self.txtNewState.text;
        objAccount.about = self.txtAbout.text;
        
        NSMutableArray* arrTitles = [NSMutableArray new];
        for (UITextField* txt in self.vDynamicTitle.arrData)
        {
            [arrTitles addObject:txt.text];
        }
        objAccount.accountTitles = arrTitles;
        objAccount.accountShowEmail = self.ckShowEmail.selected;
        objAccount.accountShowPhone = self.cbShowPhone.selected;
        objAccount.accountShowMobilePhone = self.cbShowCellphone.selected;
        
        objAccount.bank_id = self.currentBank.dataIdentifier;
        objAccount.bank_agency = self.txtBankAgenda.text;
        objAccount.bank_account = self.txtBankConta.text;
        objAccount.bank_account_dac = self.txtBankVerify.text;
        
        objAccount.image_oab = [LAUtilitys encodeToBase64String:self.imgAttach3];
        objAccount.image_avatar = [LAUtilitys encodeToBase64String:self.imgAttach1];
        objAccount.image_oab_verse = [LAUtilitys encodeToBase64String:self.imgAttach4];
        objAccount.image_house_proof = [LAUtilitys encodeToBase64String:self.imgAttach2];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [LAAccountService registerLawyerWithAccount:objAccount wCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                
                NSString* message = [data objectForKey:KEY_RESPONSE_MESSAGE];
                if (success)
                {
                    id responseData = [data objectForKey:KEY_RESPONSE_DATA];
                    if (responseData && [responseData isKindOfClass:[NSDictionary class]])
                    {
                        NSString* token = [responseData objectForKey:KEY_RESPONSE_TOKEN];
                        [LAHTTPClient sharedClient].tokenRequest = token;
                        if (![LAUtilitys NullOrEmpty:token]) {
                            isSuccess = success;
                            [[LADataCenter shareInstance] saveToken:token error:nil];
                            [LADataCenter shareInstance].registerToken = token;
                        }
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KEY_CHANGE_ACCOUNT object:nil];
                         [self checkStatus:[LADataCenter shareInstance].currentAccessToken];
                        //                        [self uploadImages:[NSString stringWithFormat:@"Bearer %@",token]];
//                        [self uploadImageProcess];
                        //                        [self uploadImageonServers:[NSString stringWithFormat:@"Bearer %@",token]];
                    }
                    else{
                        [self showMessage:message withPoistion:nil];
                    }
                }
                else
                {
                    [self showMessage:message withPoistion:nil];
                }
            }
            
        }];
        
    }
    }
}
- (void)showMessageViewControllerWithContent:(NSString*)contentText andTitleButton:(NSString*)titleButton
{
    NSString *className = NSStringFromClass([LAPopupMessageViewController class]);
    LAPopupMessageViewController *controller = [[LAPopupMessageViewController alloc] initWithNibName:className bundle:nil];
    controller.lbContent.text = contentText;
    [controller.btExit setTitle:titleButton forState:UIControlStateNormal];
    controller.delegate = self;
    controller.content = contentText;
    controller.titleButton = titleButton;
    [controller.view setFrame:[AppDelegate sharedDelegate].window.bounds];
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
    
}
- (void)onFinishPopupPressed
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self gotoHome];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self fillImageForUploadControl:picker didFinishPickingMediaWithInfo:info];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)onTakePic:(id)sender andTag:(NSInteger)tag
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        imagePickerController.view.tag = tag;
        [self presentViewController:imagePickerController animated:YES completion:^{
        }];
    }
    else
    {
        [self showMessage:@"Camera not avaible" withPoistion:nil];
    }
}
- (IBAction)onChooseImage:(id)sender andTag:(NSInteger)tag
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:
                            @"Câmera",
                            @"Escolher dos arquivos",
                            nil];
    popup.tag = tag;
    [popup showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self onTakePic:nil andTag:popup.tag];
            break;
        case 1:
            [self chooseImageForControl:popup.tag  withSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        default:
            break;
    }
    
}
- (IBAction)onShowCity:(id)sender
{
    if ([LAUtilitys NullOrEmpty:self.txtUF.text])
    {
        [self.txtUF becomeFirstResponder];
        [self onShowState:nil];
    }
    else
    {
        [self showCityList:TYPE_CITY];
    }
    
}
- (IBAction)onShowState:(id)sender
{
    listState = [NSMutableArray arrayWithArray:listUFS.data];
    [self showCityList:TYPE_STATE];
}
- (IBAction)onShowNewState:(id)sender
{
    listState = [NSMutableArray arrayWithArray:listUFS.data];
    [self showCityList:TYPE_NEWSTATE];
}
- (void)showCityList:(TYPE_POPUP)type
{
    LACountryViewController* countryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LACountryViewController"];
    countryVC.isShouldBackButton = NO;
    if (type == TYPE_CITY)
    {
        countryVC.arrCountry = listCity;
        countryVC.currentCity = self.currentCity;
    }
    else if(type == TYPE_BANK){
        countryVC.arrCountry = bankInfo.data;
        countryVC.currentBank = self.currentBank;
    }
    else
    {
        countryVC.arrCountry = listState;
        countryVC.currentState = self.currentState;
    }
    countryVC.popType = type;
    countryVC.isTransparentNavigation = YES;
    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:countryVC];
    [self presentViewController:navi animated:YES completion:nil];
}
- (void)didselectAddress:(NSNotification*)notif
{
    id obj = [notif object];
    if (obj && [obj isKindOfClass:[LACityObj class]])
    {
        self.currentCity = obj;
        self.txtCity.text = self.currentCity.cityName;
    }
    else{
        long typePoup = [[[notif userInfo] objectForKey:@"TYPEPOUP"] integerValue];
        if (typePoup == TYPE_STATE) {
            self.currentState = obj;
            self.txtUF.text = self.currentState;
            [self getCityList];
        }
        else if(typePoup == TYPE_NEWSTATE){
            self.currentState = obj;
            self.txtNewState.text = self.currentState;
        }
        else if(typePoup == TYPE_BANK){
            self.currentBank = obj;
            self.txtBankName.text = self.currentBank.name;
        }
    }
}

-(void)fillImageForUploadControl:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    //    UIImage* img1 = [UIImage imageWithImage:img scaledToMaxWidth:1024 maxHeight:1024];
    //    NSData *imgData = UIImageJPEGRepresentation(img, 1.0);
    //    CGFloat ratio = 1024.0*1024.0/([imgData length]);
    
    UIImage* img1;
    NSData *imgData;
    CGFloat ratio;
    if (picker.view.tag == self.uploadView1.tag) {
        img1 = [UIImage imageWithImage:img scaledToMaxWidth:MAX_SIZE_IMAGE_AVATAR maxHeight:MAX_SIZE_IMAGE_AVATAR];
        imgData = UIImageJPEGRepresentation(img, 1.0);
        ratio = 512.0*512.0/([imgData length]);
    }
    else{
        img1 = [UIImage imageWithImage:img scaledToMaxWidth:MAX_SIZE_IMAGE_DEFAULT maxHeight:MAX_SIZE_IMAGE_DEFAULT];
        imgData = UIImageJPEGRepresentation(img, 1.0);
        ratio = 1024.0*1024.0/([imgData length]);
    }
    
    //    UIImage* imgUpload = [UIImage imageWithData:UIImageJPEGRepresentation(img1, ratio)];
    img = img1;
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSString *filename = STRING_EMPTY;
    if (refURL == nil) {
        NSCalendar *gregorian = [[NSCalendar alloc]
                                 initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components =
        [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:[NSDate date]];
        
        NSInteger minute = [components minute];
        NSInteger second = [components second];
        if (minute < 10 && second < 10) {
            filename = [NSString stringWithFormat:@"IMG_0%ld%0ld.jpg",(long)minute,(long)second];
        }
        else if (minute < 10) {
            filename = [NSString stringWithFormat:@"IMG_0%ld%ld.jpg",(long)minute,(long)second];
        }
        else if (second < 10) {
            filename = [NSString stringWithFormat:@"IMG_%ld%0ld.jpg",(long)minute,(long)second];
        }
        else{
            filename = [NSString stringWithFormat:@"IMG_%ld%ld.jpg",(long)minute,(long)second];
        }
        
    }
    else{
        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
        filename = [[result firstObject] filename];
    }
    
    if (picker.view.tag == self.uploadView1.tag) {
        self.imgAttach1 = img;
        self.uploadView1.imgThumb.image = self.imgAttach1;
        self.uploadView1.bHasImage = YES;
        self.uploadView1.lbName.text = filename;
        [self.uploadView1 layoutSubviews];
    }
    else if(picker.view.tag == self.uploadView2.tag) {
        self.imgAttach2 = img;
        self.uploadView2.imgThumb.image = self.imgAttach2;
        self.uploadView2.bHasImage = YES;
        self.uploadView2.lbName.text = filename;
        [self.uploadView2 layoutSubviews];
        
    }
    else if(picker.view.tag == self.uploadView3.tag){
        self.imgAttach3 = img;
        self.uploadView3.lbName.text = filename;
        
        self.uploadView3.imgThumb.image = self.imgAttach3;
        self.uploadView3.bHasImage = YES;
        [self.uploadView3 layoutSubviews];
    }
    else if(picker.view.tag == self.uploadView4.tag){
        self.imgAttach4 = img;
        self.uploadView4.lbName.text = filename;
        
        self.uploadView4.imgThumb.image = self.imgAttach4;
        self.uploadView4.bHasImage = YES;
        [self.uploadView4 layoutSubviews];
    }
    //    NSURL *localPath = [NSURL fileURLWithPath:NSTemporaryDirectory().pa]
    [self updateViewIfNeed];
}
- (void)chooseImageForControl:(NSInteger)tagControl withSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.view.tag = tagControl;
    [self presentViewController:imagePickerController animated:YES completion:^{}];
}
- (void)uploadImageProcess
{
    NSMutableArray* imgUploads = [NSMutableArray new];
    NSMutableArray* keyUploads = [NSMutableArray new];
    NSMutableArray* nameUploads = [NSMutableArray new];
    if (self.imgAttach1)
    {
        [imgUploads addObject:self.imgAttach1];
        //        [imgUploads addObject:[UIImage imageNamed:@"star"]];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_AVATA];
        [nameUploads addObject:self.uploadView1.lbName.text];
    }
    if (self.imgAttach2)
    {
        [imgUploads addObject:self.imgAttach2];
        //        [imgUploads addObject:[UIImage imageNamed:@"star"]];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF];
        [nameUploads addObject:self.uploadView2.lbName.text];
    }
    if (self.imgAttach3)
    {
        [imgUploads addObject:self.imgAttach3];
        //        [imgUploads addObject:[UIImage imageNamed:@"star"]];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_OAB];
        [nameUploads addObject:self.uploadView3.lbName.text];
    }
    if (self.imgAttach4)
    {
        [imgUploads addObject:self.imgAttach4];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_OAB_VERSE];
        [nameUploads addObject:self.uploadView4.lbName.text];
    }
    
    
    if (imgUploads.count)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeAnnularDeterminate;
        hud.labelText = @"Image uploading...";
        NSLog(@"%@",[imgUploads description]);
        [LABaseService uploadImages:imgUploads andListKeys:keyUploads andListName:nameUploads andParams:nil andPath:API_PATH_UPLOAD_IMAGE_LAWYER withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
            NSLog(@"data upload:%@",data);
            if ([data isKindOfClass:[NSDictionary class]]) {
                NSDictionary *lawyer = [[data objectForKey:@"data"] objectForKey:@"lawyer"] ;
                if ([lawyer count]) {
                    if (isSuccess&&![LAUtilitys isEmptyOrNull:[lawyer objectForKey:@"image_avatar_with_url"]]&&![LAUtilitys isEmptyOrNull:[lawyer objectForKey:@"image_house_proof_with_url"]]&&![LAUtilitys isEmptyOrNull:[lawyer objectForKey:@"image_oab_verse_with_url"]]&&![LAUtilitys isEmptyOrNull:[lawyer objectForKey:@"image_oab_with_url"]] )
                    {
                        [self checkStatus:[LADataCenter shareInstance].currentAccessToken];
                    }
                    else{
                        [self showMessage:@"Have error when upload to server" withPoistion:nil];
                    }
                }
            }
            
            [hud hide:YES];
        } progressBlock:^(double currentProgress) {
            hud.progress = currentProgress;
        }];
    }
    //    else if (imgUploads.count < 3)
    //    {
    //        [self showMessage:@"All photo upload fields is required" withPoistion:nil];
    //    }
    else
    {
        if (isSuccess)
        {
            [self checkStatus:[LADataCenter shareInstance].currentAccessToken];
        }
    }
}
- (void)checkStatus:(NSString*)token
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService checkStatus:token withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[LAStatusAccountObj class]])
        {
            [self navigationWithStatusAccount:data];
        }
        else
        {
            [self showMessage:message withPoistion:nil];
        }
    }];
}

-(void)uploadImage:(NSNotification*)notification{
    [self.view endEditing:YES];
    NSNumber *tagControl = (NSNumber*)[notification object];
    [self onChooseImage:nil andTag:[tagControl integerValue]];
}

-(void)updateViewIfNeed{
    self.lbComprovante.frame = CGRectMake(self.lbComprovante.frame.origin.x, CGRectGetMaxY(self.uploadView1.frame) + 10, self.lbComprovante.frame.size.width, self.lbComprovante.frame.size.height);
    
    self.lbSeu.frame = CGRectMake(self.lbSeu.frame.origin.x, CGRectGetMaxY(self.lbComprovante.frame), self.lbSeu.frame.size.width, self.lbSeu.frame.size.height);
    
    self.uploadView2.frame = CGRectMake(self.uploadView2.frame.origin.x, CGRectGetMaxY(self.lbSeu.frame) + 10, self.uploadView2.frame.size.width, self.uploadView2.frame.size.height);
    
    self.viewMiddle.frame = CGRectMake(self.viewMiddle.frame.origin.x, CGRectGetMaxY(self.uploadView2.frame) + 10, self.viewMiddle.frame.size.width, self.viewMiddle.frame.size.height);
    self.uploadView3.frame = CGRectMake(self.uploadView3.frame.origin.x, CGRectGetMaxY(self.viewMiddle.frame) + 10, self.uploadView3.frame.size.width, self.uploadView3.frame.size.height);
    
    self.lbCpioTitle.frame = CGRectMake(self.lbCpioTitle.frame.origin.x, CGRectGetMaxY(self.uploadView3.frame) + 10, self.lbCpioTitle.frame.size.width, self.lbCpioTitle.frame.size.height);
    
    self.uploadView4.frame = CGRectMake(self.uploadView4.frame.origin.x, CGRectGetMaxY(self.lbCpioTitle.frame) + 10, self.uploadView4.frame.size.width, self.uploadView4.frame.size.height);
    
    self.vDynamicTitle.frame = CGRectMake(self.vDynamicTitle.frame.origin.x, CGRectGetMaxY(self.uploadView4.frame) + 10, self.vDynamicTitle.frame.size.width, self.vDynamicTitle.frame.size.height);
    
    self.vBankInfo.frame = CGRectMake(self.vBankInfo.frame.origin.x, CGRectGetMaxY(self.vDynamicTitle.frame) + 10, self.vBankInfo.frame.size.width, self.vBankInfo.frame.size.height);
    
    self.btnConfirm.frame = CGRectMake(self.btnConfirm.frame.origin.x, CGRectGetMaxY(self.vBankInfo.frame) + 10, self.btnConfirm.frame.size.width, self.btnConfirm.frame.size.height);
    
    CGSize size = self.scrFullData.contentSize;
    size.height = CGRectGetMaxY(self.btnConfirm.frame) + self.btnConfirm.frame.size.height + 20;
    [self.scrFullData setContentSize:size];
    NSLog(@"content frame:%@",NSStringFromCGRect(self.scrFullData.frame));
    NSLog(@"content size:%@",NSStringFromCGSize(self.scrFullData.contentSize));
}
-(void)didFinishInsertSubView:(NSMutableArray *)arrData{
    [self updateViewIfNeed];
}
- (IBAction)doShowBankName:(id)sender {
    [self showCityList:TYPE_BANK];
}

- (IBAction)onAcceptPressed:(id)sender {
    [self.cbAcceptTerm setSelected:!self.cbAcceptTerm.selected];
    if (self.cbAcceptTerm.selected)
    {
        [self.btnConfirm setBackgroundColor:[LAUtilitys jusTapColor]];
    }
    else
    {
        [self.btnConfirm setBackgroundColor:[UIColor lightGrayColor]];
    }

}

- (IBAction)onShowEmailPressed:(id)sender;
{
    [self.ckShowEmail setSelected:!self.ckShowEmail.selected];
}
#pragma mark BirthDay
- (void)onShowBirthDaySelect
{
    datePicker = [[UIDatePicker alloc]init];
    if (![LAUtilitys NullOrEmpty:self.txtDateOfBirth.text]) {
        NSDate *date = [LAUtilitys getNSDateFromDateString:self.txtDateOfBirth.text withFormat:FORMAT_DD_MM_YY];
        if (date) {
            [datePicker setDate:date];
        }
    }
    //this returns today's date
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = FORMAT_DD_MM_YY;
    // converting string to date
    
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
    //    [datePicker setMaximumDate:[NSDate date]]; //the min age restriction
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    //    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"US"];
    //    [datePicker setLocale:locale];
    datePicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3*3600];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // and finally set the datePicker as the input mode of your textfield
    [self.txtDateOfBirth setInputView:datePicker];
}
-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)self.txtDateOfBirth.inputView;
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3600*3];
    [dateFormatter setDateFormat:FORMAT_DD_MM_YY];
    self.txtDateOfBirth.text =  [dateFormatter stringFromDate:picker.date];
}

#define kStartTag   @"--%@\r\n"
#define kEndTag     @"\r\n"
#define kContent    @"Content-Disposition: form-data; name=\"%@\"\r\n\r\n"
#define kBoundary   @"---------------------------14737809831466499882746641449"

-(void)uploadImageonServers:(NSString*)strToken
{
    NSMutableArray* imgUploads = [NSMutableArray new];
    NSMutableArray* keyUploads = [NSMutableArray new];
    NSMutableArray* nameUploads = [NSMutableArray new];
    if (self.imgAttach1)
    {
        [imgUploads addObject:self.imgAttach1];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_AVATA];
        [nameUploads addObject:[NSString stringWithFormat:@"%@%@",REQQUEST_ACCOUNT_KEY_IMAGE_AVATA,@".jpg"]];
    }
    if (self.imgAttach2)
    {
        [imgUploads addObject:self.imgAttach2];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_OAB];
        [nameUploads addObject:[NSString stringWithFormat:@"%@%@",REQQUEST_ACCOUNT_KEY_IMAGE_OAB,@".jpg"]];
    }
    if (self.imgAttach3)
    {
        [imgUploads addObject:self.imgAttach3];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF];
        [nameUploads addObject:[NSString stringWithFormat:@"%@%@",REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF,@".jpg"]];
    }
    
    NSMutableURLRequest *request = nil;
    NSLog(@"image upload");
    
    NSMutableData *body = [NSMutableData data];
    request = [[NSMutableURLRequest alloc] init];
    NSString *requestURL = [NSString stringWithFormat:@"http://rest.justap.com.br/api/v1/lawyers/upload_files"];
    
    [request setURL:[NSURL URLWithString:requestURL]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Authorization" forHTTPHeaderField:strToken];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",kBoundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    for (int i=0; i<[imgUploads count];i++ )
    {
        NSData *image_Data = UIImageJPEGRepresentation([imgUploads objectAtIndex:i], 1.0);
        if (image_Data)
        {
            // image File
            [body appendData:[[NSString stringWithFormat:kStartTag, kBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
            NSString *strData = [NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=%@\r\n",keyUploads[i],nameUploads[i]];
            
            [body appendData:[strData dataUsingEncoding: NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:image_Data];
            [body appendData:[[NSString stringWithFormat:kEndTag] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else
        {
            [body appendData:[[NSString stringWithFormat:kStartTag, kBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    
    // close form
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", kBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (error == nil)
                               {
                                   NSError *error;
                                   id receivedData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                   
                                   NSDictionary *dicResponse = (NSDictionary *)receivedData;
                                   
                                   NSLog(@"dicResponse:%@",dicResponse);
                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                               }
                               else
                               {
                                   NSLog(@"Upload image of drawing error:%@",[error description]);
                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                               }
                           }];
    
    //    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
}
-(void)uploadImages:(NSString*)strToken{
    //    NSMutableArray* imgUploads = [NSMutableArray new];
    //    NSMutableArray* keyUploads = [NSMutableArray new];
    //    NSMutableArray* nameUploads = [NSMutableArray new];
    //    if (self.imgAttach1)
    //    {
    //        [imgUploads addObject:self.imgAttach1];
    //        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_AVATA];
    //        [nameUploads addObject:[NSString stringWithFormat:@"%@%@",REQQUEST_ACCOUNT_KEY_IMAGE_AVATA,@".jpg"]];
    //    }
    //    if (self.imgAttach2)
    //    {
    //        [imgUploads addObject:self.imgAttach2];
    //        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_OAB];
    //        [nameUploads addObject:[NSString stringWithFormat:@"%@%@",REQQUEST_ACCOUNT_KEY_IMAGE_OAB,@".jpg"]];
    //    }
    //    if (self.imgAttach3)
    //    {
    //        [imgUploads addObject:self.imgAttach3];
    //        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF];
    //        [nameUploads addObject:[NSString stringWithFormat:@"%@%@",REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF,@".jpg"]];
    //    }
    //    if (self.imgAttach4)
    //    {
    //        [imgUploads addObject:self.imgAttach4];
    //        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_OAB_VERSE];
    //        [nameUploads addObject:[NSString stringWithFormat:@"%@%@",REQQUEST_ACCOUNT_KEY_IMAGE_OAB_VERSE,@".jpg"]];
    //    }
    //    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:@"http://rest.justap.com.br/api/v1/lawyers/upload_files" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    //        //        for (int i; i<[imgUploads count]; i++) {
    //        //            [formData appendPartWithFormData:UIImageJPEGRepresentation(imgUploads[i], 1.0) name:keyUploads[i]];
    //        //        }
    //        formData appendPartWithFileData:<#(nonnull NSData *)#> name:<#(nonnull NSString *)#> fileName:<#(nonnull NSString *)#> mimeType:<#(nonnull NSString *)#>
    //        [formData appendPartWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"1_avatar" ofType:@"jpg"]] name:REQQUEST_ACCOUNT_KEY_IMAGE_AVATA fileName:@"1_avatar.jpg" mimeType:@"image/jpeg" error:nil];
    //        [formData appendPartWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"2_comprovante" ofType:@"jpg"]] name:REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF fileName:@"2_comprovante.jpg" mimeType:@"image/jpeg" error:nil];
    //        [formData appendPartWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"3_oab" ofType:@"jpg"]] name:REQQUEST_ACCOUNT_KEY_IMAGE_OAB fileName:@"3_oab.jpg" mimeType:@"image/jpeg" error:nil];
    //        [formData appendPartWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"4" ofType:@"jpg"]] name:REQQUEST_ACCOUNT_KEY_IMAGE_OAB_VERSE fileName:@"4.jpg" mimeType:@"image/jpeg" error:nil];
    //        
    //    } error:nil];
    //    
    //    [MBProgressHUD hideHUDForView:self.view animated:YES];
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.mode = MBProgressHUDModeAnnularDeterminate;
    //    hud.labelText = @"Image uploading...";
    //    
    //    
    //    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    //    [request setValue:strToken forHTTPHeaderField:@"Authorization"];
    //    request.timeoutInterval = 60;
    //    NSURLSessionUploadTask *uploadTask;
    //    uploadTask = [manager
    //                  uploadTaskWithStreamedRequest:request
    //                  progress:^(NSProgress * _Nonnull uploadProgress) {
    //                      // This is not called back on the main queue.
    //                      // You are responsible for dispatching to the main queue for UI updates
    //                      dispatch_async(dispatch_get_main_queue(), ^{
    //                          //Update the progress view
    //                          //[progressView setProgress:uploadProgress.fractionCompleted];
    //                          hud.progress = uploadProgress.fractionCompleted;
    //                      });
    //                  }
    //                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
    //                      [hud hide:YES];
    //                      if (error) {
    //                          NSLog(@"Error: %@", error);
    //                      } else {
    //                          NSLog(@"%@ %@", response, responseObject);
    //                      }
    //                  }];
    //    
    //    [uploadTask resume];
}

-(void)removeImage:(NSNotification*)notification{
    NSNumber *tagControl = (NSNumber*)[notification object];
    switch ([tagControl integerValue]) {
        case 10001:
            [self.uploadView1.imgThumb setImage:nil];
            self.uploadView1.bHasImage = NO;
            [self.uploadView1 layoutSubviews];
            break;
        case 10002:
            [self.uploadView2.imgThumb setImage:nil];
            self.uploadView2.bHasImage = NO;
            [self.uploadView2 layoutSubviews];
            break;
        case 10003:
            [self.uploadView3.imgThumb setImage:nil];
            self.uploadView3.bHasImage = NO;
            [self.uploadView3 layoutSubviews];
            break;
        case 10004:
            [self.uploadView4.imgThumb setImage:nil];
            self.uploadView4.bHasImage = NO;
            [self.uploadView4 layoutSubviews];
            break;
        default:
            break;
    }
    [self updateViewIfNeed];
}

-(void)loadBank{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchBanks:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        bankInfo = [[LABankInfo alloc]initWithDictionary:data];
    }];
}

-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    navigationController.delegate = self;
    [self changeCancelToAnythingUWantOf:navigationController andReplacementString:@""];
    [self changeCancelToAnythingUWantOf:viewController andReplacementString:@""];
}
- (void)loopChangeTitle:(UIView*)viewChange
{
    for(UIView *subView in viewChange.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)subView;
            NSLog(@"BUTTON: %@",btn.titleLabel.text);
            if([btn.titleLabel.text isEqualToString:@"Retake"]) {
                [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Use Photo"]){
                [btn setTitle:@"Usar foto" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Cancel"]){
                [btn setTitle:@"Cancelar" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Photos"]){
                [btn setTitle:@"Fotos" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"PHOTO"]){
                [btn setTitle:@"FOTO" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"On"]){
                [btn setTitle:@"Em" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Off"]){
                [btn setTitle:@"Fora" forState:UIControlStateNormal];
                break;
            }
        }
        else if([subView isKindOfClass:[UILabel class]]){
            UILabel *lb = (UILabel*)subView;
            if([lb.text isEqualToString:@"Photos"]){
                lb.text = @"Fotos";
                break;
            }
            else if([lb.text isEqualToString:@"PHOTO"]){
                lb.text = @"FOTO";
                break;
            }
            else if([lb.text isEqualToString:@"Use Photo"]){
                lb.text = @"Usar foto";
                break;
            }
            else if([lb.text isEqualToString:@"Cancel"]){
                lb.text = @"Cancelar";
                break;
            }
            else if([lb.text isEqualToString:@"Retake"]){
                lb.text = @"Tirar outra";
                break;
            }
            else if([lb.text isEqualToString:@"On"]){
                lb.text = @"Em";
                break;
            }
            else if([lb.text isEqualToString:@"Off"]){
                lb.text = @"Fora";
                break;
            }
            
        }
        if (subView.subviews) {
            [self loopChangeTitle:subView];
        }
    }
}
- (void)changeCancelToAnythingUWantOf:(UIViewController*)vc andReplacementString:(NSString*)title {
    [self loopChangeTitle:vc.view];
    vc.view.backgroundColor = [UIColor clearColor];
}
@end

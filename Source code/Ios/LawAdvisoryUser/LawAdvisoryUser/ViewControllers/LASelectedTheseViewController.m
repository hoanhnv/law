//
//  LASelectedTheseViewController.m
//  JustapLawyer
//
//  Created by Mac on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASelectedTheseViewController.h"
#import "LASelectedTheseHeader.h"
#import "LASelectedThesesFooter.h"
#import "UIView+NibInitializer.h"
#import "LAPricePlanosViewController.h"
#import "MainService.h"
#import "UIFont+Law.h"
#import "LATesesMainViewController.h"
#import "AppDelegate.h"

@interface LASelectedTheseViewController ()
@property (nonatomic, strong) NSMutableArray *arrData;
@end

@implementation LASelectedTheseViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tblData registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    [self.tblData setDelegate:self];
    [self.tblData setDataSource:self];
    self.arrData = [NSMutableArray array];
    for (LALawSuitCategory* cateItem in self.selectedTheseData) {
        [self.arrData addObjectsFromArray:cateItem.arrData];
    }
    self.lbHeaderTitle.text = [NSString stringWithFormat:kTitleHeader,[self getCountSelectedThese],[AppDelegate sharedDelegate].countThese];
    
    LASelectedTheseHeader *vHeader = [[LASelectedTheseHeader alloc]initWithNibNamed:@"LASelectedTheseHeaderView"];
    vHeader.lbTitle.text = [NSString stringWithFormat:kTitleHeader,[self getCountSelectedThese],[AppDelegate sharedDelegate].countThese];
    self.tblData.tableHeaderView = vHeader;
    [self.tblData reloadData];
    
    LASelectedThesesFooter *footer = [[LASelectedThesesFooter alloc]initWithNibNamed:@"LASelectedTheseFooterView"];
    [footer.btnConfirmThese addTarget:self action:@selector(onConfirm:) forControlEvents:UIControlEventTouchUpInside];
    self.tblData.tableFooterView = footer;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.title = @"Teses";
    
    UIView *v = self.tblData.tableHeaderView;
    CGRect fr = v.frame;
    fr.size.height =120;;
    v.frame = fr;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrData count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    LALawsuitObj *objItem = [self.arrData objectAtIndex:indexPath.row];
    cell.textLabel.text = objItem.name;
    cell.textLabel.font = [UIFont robotoRegular:14];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}

- (IBAction)onConfirm:(id)sender
{    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoPriceConfirm" object:nil];
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(void)updateThese{
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *arrTheses = [NSMutableArray array];
    [params setObject:arrTheses forKey:@"theses"];
    
    [MainService lawyerUpdateThese:params withSuccess:^(NSInteger errorCode, NSString *message, id data) {
    }];
}

-(NSInteger)getCountSelectedThese{
    NSInteger count = 0;
    for (LALawSuitCategory *cate in self.selectedTheseData) {
        count += [cate.arrData count];
    }
    return count;
}
@end

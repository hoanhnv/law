//
//  LAFrequentesViewController.m
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAFrequentesViewController.h"
#import "InfoCell.h"
#import "HeaderInfo.h"
#import "MainService.h"
#import "LAListFaqs.h"
#import "LAQuestionObj.h"
#import "MBProgressHUD.h"
#import "LAFaqCategoriesObj.h"
#import "LAFaqCategories.h"

@interface LAFrequentesViewController ()<UITableViewDelegate,UITableViewDataSource,UINavigationBarDelegate>
{
    NSMutableDictionary * mSelected;
    LAListFaqs *lstFaqs;
    LAFaqCategories *lstFaqCategory;
}
@property (weak, nonatomic) IBOutlet UITableView *tblCategories;
@property (weak, nonatomic) IBOutlet UIView *poupView;
@end

@implementation LAFrequentesViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Perguntas frequentes";
    _tbvInfo.estimatedRowHeight = 180;
    _tbvInfo.rowHeight = UITableViewAutomaticDimension;
    [_tbvInfo registerNib:[UINib nibWithNibName:NSStringFromClass([InfoCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoCell class])];
    UIBarButtonItem * barItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_backnavigation"] style:UIBarButtonItemStylePlain target:self action:@selector(backView)];
    self.navigationItem.leftBarButtonItem = barItem;
    mSelected = [NSMutableDictionary new];
    [mSelected setObject:@(1) forKey:@"0"];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tapSelectCate = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doOpenCategories:)];
    [self.vHeaderCate addGestureRecognizer:tapSelectCate];
    [self fecthAllCategories];
    [self fecthAllFaqs];
}

-(void)fecthAllFaqs{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchFaqs:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        lstFaqs = [[LAListFaqs alloc]initWithDictionary:data];
        if (lstFaqs.success) {
            [self reloadData];
        }
        else{
            [self showMessage:lstFaqs.message withPoistion:nil];
        }
    }];
}
-(void)initUI{
    self.tblCategories.layer.borderColor = NAVIGATION_COLOR.CGColor;
    self.tblCategories.layer.borderWidth = 3.0f;
    self.tblCategories.hidden = YES;
    self.poupView.hidden = YES;
    
}
-(void)fetchFaqByCateId:(NSInteger)cateId{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchFaqCategories:cateId withSuccess:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        lstFaqs = [[LAListFaqs alloc]initWithDictionary:data];
        if (lstFaqs.success) {
            [self reloadData];
        }
        else{
            [self showMessage:lstFaqs.message withPoistion:nil];
        }
    }];
}
-(void)fecthAllCategories{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchFaqCategories:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        lstFaqCategory = [[LAFaqCategories alloc]initWithDictionary:data];
        if (lstFaqCategory.success) {
            [self.tblCategories reloadData];
        }
        else{
            [self showMessage:lstFaqCategory.message withPoistion:nil];
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)backView{
}
-(void)reloadData{
    [self.tbvInfo reloadData];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.tblCategories) {
        return 1;
    }
    else{
        return [lstFaqs.data count];
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tbvInfo) {
        return [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]]integerValue];
    }
    else{
        return [lstFaqCategory.data count];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tbvInfo) {
        for (NSString * key in mSelected.allKeys) {
            if (indexPath.section == [key integerValue]) {
                BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",indexPath.section]] integerValue] == 1;
                if (isShow)
                {
                    LAQuestionObj *questionInfo = (LAQuestionObj*)lstFaqs.data[indexPath.section] ;
                    return [InfoCell getHeight:questionInfo.answer inWith:_tbvInfo];
                }
            }
            
        }
        return 0.01f;
    }
    else
    {
        return 44.0f;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tblCategories) {
        static NSString* cellIdentifier = @"cellIdentier";
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        if (cell)
        {
            cell.textLabel.textColor = [LAUtilitys jusTapColor];
            LAFaqCategoriesObj *obj = [lstFaqCategory.data objectAtIndex:indexPath.row];
            cell.textLabel.text = obj.name;
        }
        return cell;
    }
    else{
        InfoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoCell class]) forIndexPath:indexPath];
        LAQuestionObj *questionInfo = (LAQuestionObj*)lstFaqs.data[indexPath.section];
        cell.lbContent.text = questionInfo.answer;
        [cell.btnAccept addTarget:self action:@selector(doAccept:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAccept setTag:indexPath.row];
        [cell.btnCancel addTarget:self action:@selector(doCancel:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnCancel setTag:indexPath.row];
        return cell;
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.tbvInfo) {
        HeaderInfo * header = [[HeaderInfo alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
        LAQuestionObj *questionInfo = (LAQuestionObj*)lstFaqs.data[section];
        header.questionInfo = questionInfo;
        [header setupView];
        for (NSString * key in mSelected.allKeys) {
            if (section == [key integerValue]) {
                BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]] integerValue] == 1;
                [header setupColor:isShow];
            }
        }
        [header setShowBlock:^(BOOL isShow){
            if (isShow) {
                [mSelected setObject:@(1) forKey:[NSString stringWithFormat:@"%ld",section]];
            } else{
                [mSelected setObject:@(0) forKey:[NSString stringWithFormat:@"%ld",section]];
            }
            [tableView reloadData];
        }];
        
        return header;
    }
    else{
        return nil;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tblCategories) {
        self.tblCategories.hidden = !self.tblCategories.hidden;
        LAFaqCategoriesObj *obj = [lstFaqCategory.data objectAtIndex:indexPath.row];
        self.lbCateName.text = obj.name;
        [self fetchFaqByCateId:obj.dataIdentifier];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.tblCategories) {
        return 0;
    }
    else{
        LAQuestionObj *questionInfo = (LAQuestionObj*)lstFaqs.data[section];
        return  [HeaderInfo getHeight:questionInfo inWith:tableView];
    }
}

-(void)doAccept:(id)sender{
    
}
-(void)doCancel:(id)sender{
    
}
- (IBAction)doOpenCategories:(id)sender {
    self.tblCategories.hidden = !self.tblCategories.hidden;
}
@end

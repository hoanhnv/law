//
//  LAPerfitViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPerfitViewController.h"
#import "AppDelegate.h"
#import "LAAccountObj.h"
#import "LAAccountService.h"
#import "MBProgressHUD.h"
#import "MainService.h"
#import "LANewPerfitViewController.h"

@interface LAPerfitViewController ()

@end

@implementation LAPerfitViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showEditForm:) name:ARGS_SHOW_EDIT_FORM object:nil];
}
- (void)initUI
{
    //    [self setUpUI];
    profileView = [[ProfileView alloc]initWithNibNamed:@"ProfileView"];
    [profileView.btnEdit addTarget:self action:@selector(showEditForm:) forControlEvents:UIControlEventTouchUpInside];
    [profileView.btnEditImage addTarget:self action:@selector(showEditForm:)
                       forControlEvents:UIControlEventTouchUpInside];
    self.mainScroll.frame = CGRectMake(0, CGRectGetMaxY(self.segmentedControl.frame), CGRectGetWidth(self.mainScroll.frame),self.mainScroll.frame.size.height);
    profileView.frame = CGRectMake(0,0 , self.mainScroll.frame.size.width, profileView.frame.size.height);
    profileView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//    self.mainScroll.contentSize = profileView.frame.size;
    [self.mainScroll setContentSize:CGSizeMake(1, CGRectGetHeight(profileView.frame) + 50)];
    [self.mainScroll addSubview:profileView];
    self.mainScroll.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//    self.mainScroll.contentSize = CGSizeMake(1,CGRectGetHeight(profileView.frame) + 50);
    [self.navigationController setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    [self getData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onLogoutPresseed:(id)sender {
    [[LADataCenter shareInstance] clearKeyChain];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [LAAccountService logoutwCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            NSString* messageHere = [data objectForKey:KEY_RESPONSE_MESSAGE];
            if (success)
            {
                [self showMessage:messageHere withPoistion:nil];
                [[LADataCenter shareInstance] clearKeyChain];
                UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LALoginViewController"]];
                [AppDelegate sharedDelegate].nav = nav;
                [AppDelegate sharedDelegate].window.rootViewController = [AppDelegate sharedDelegate].nav;
            }
            else
            {
                [self showMessage:message withPoistion:nil];
            }
        }
        else
        {
            [self showMessage:message withPoistion:nil];
        }
        
    }];
}

#pragma mark USER ACTION
- (IBAction)onEdit:(id)sender
{
    [self.scrFullDataProfile setHidden:NO];
}

- (IBAction)onEditProfilePressed:(id)sender {
}

-(void)showEditForm:(id)sender {
    LANewPerfitViewController *editVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LANewPerfitViewController"];
    editVC.objInfo = self.objInfo;
    self.navigationController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:editVC animated:YES];
    
}

- (void)getData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawyerInfo:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            BOOL success = [data objectForKey:KEY_RESPONSE_ERROR_CODE];
            NSString* message = [data objectForKey:KEY_RESPONSE_MESSAGE];
            if (success)
            {
                LALawyerInfo* obj = [[LALawyerInfo alloc] initWithDictionary:data];
                self.objInfo = obj;
                [profileView loadData:self.objInfo];
                self.mainScroll.contentSize = CGSizeMake(1, CGRectGetHeight(profileView.frame) + 30);
            }
            else
            {
                [self showMessage:message withPoistion:nil];
            }
            
        }
        else
        {
            [self showMessage:message withPoistion:nil];
        }
    }];
}
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}

@end

//
//  LASignatureCancelViewController.m
//  JustapLawyer
//
//  Created by Mac on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASignatureCancelViewController.h"
#import "MainService.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#define kTextCancelSuccess  @"Pedido de cancelamento enviado com sucesso, em até 48 horas a equipe TusTap entrará em contato com você."

#define kTextCancel1  @"Tem certeza que deseja finalizar esta ação?"
#define kTextCancel2  @"Esta ação fará com que sua assinatura seja cancelada. Tem certeza disto?"



@interface LASignatureCancelViewController ()

@end

@implementation LASignatureCancelViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.btnCancel.hidden = NO;
    if(self.isFinishAction){
        self.navigationController.navigationBar.barTintColor = self.navigationColor;
        self.lbMessage.text = kTextCancel1;
        self.navigationItem.title = self.titleNavigation;
    }
    else{
        self.navigationItem.title = @"Perfil";
        self.lbMessage.text = kTextCancel2;
        [self.btnCancel setTitle:@"CANCELAR" forState:UIControlStateNormal];
//        FINALIZAR ESTA AÇÃO

    }
}

- (IBAction)doCancel:(id)sender {
    if(self.isFinishAction){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"finishAction" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService lawyerCancelSignature:nil withSuccess:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([data isKindOfClass:[NSDictionary class]]) {
            if ([[data objectForKey:@"success"] boolValue]) {
                if ([[data objectForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
                    if ([[data objectForKey:@"data"][@"plan_id"] integerValue] > 0) {
                        [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_SIGNATURE_REGISTER];
                        [AppDelegate sharedDelegate].buyTheseData = nil;
                        [AppDelegate sharedDelegate].lstTese = nil;
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"FinishDeletedBuyThesis" object:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else{
                        [self showMessage:data[@"message"] withPoistion:nil];
                    }
                }
            }
            else{
                [self showMessage:data[@"message"] withPoistion:nil];
            }
        }
        else{
            [self showMessage:message withPoistion:nil];
        }
        //        self.btnCancel.hidden = YES;
        //        self.lbMessage.text = kTextCancelSuccess;
    }];
    }
}
@end

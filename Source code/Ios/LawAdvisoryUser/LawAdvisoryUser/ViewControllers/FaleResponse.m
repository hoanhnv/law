//
//  FaleResponse.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "FaleResponse.h"
@interface FaleResponse()
@end
@implementation FaleResponse

-(void)viewDidLoad{
    [super viewDidLoad];
    self.lbContent.text = self.responseSubject.message;
    self.lbContent.lineBreakMode = NSLineBreakByWordWrapping;
    self.lbContent.numberOfLines = 0;
    self.lbContent.textAlignment = NSTextAlignmentJustified;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (IBAction)doClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(doClosePopup:)])
        {
            [self.delegate doClosePopup:self.isSuccess];
        }
    }];
}
@end

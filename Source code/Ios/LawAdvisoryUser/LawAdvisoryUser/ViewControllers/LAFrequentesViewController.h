//
//  LAFrequentesViewController.h
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LABaseViewController.h"
@interface LAFrequentesViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UILabel *lbCateName;
@property (weak, nonatomic) IBOutlet UITableView *tbvInfo;
@property (weak, nonatomic) IBOutlet UIView *vHeaderCate;

- (IBAction)doOpenCategories:(id)sender;

@end

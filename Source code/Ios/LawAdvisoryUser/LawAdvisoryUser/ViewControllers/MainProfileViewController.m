//
//  MainProfileViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/15/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "MainProfileViewController.h"
#import "LAAccountService.h"
#import "AppDelegate.h"
#import "LAPlanosConfirmViewController.h"
#import "LAPlanosSelectionComarcaViewController.h"
#import "LAServiceTesesViewController.h"
#import "LAPerfitViewController.h"
#import "LASignoutViewController.h"
#import "LASignaturesViewController.h"
#import "LAPricePlanosViewController.h"
#import "MainService.h"
#import "LAAssinature.h"
#import "LASignatureTese.h"
#import "LAPlanObj.h"
#import "AppDelegate.h"

@interface MainProfileViewController ()
{
    LASignaturesViewController *assignatureVC;
    LAPlanosSelectionComarcaViewController *planVC;
    LAPerfitViewController *perfitVC;
    LASignoutViewController *signoutVC;
    BOOL isLoaded1;
    BOOL isLoaded2;
    BOOL isLoaded3;
}
@end

@implementation MainProfileViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ChangeTabSelectedIfNeed:) name:@"ChangeTabSelectedIfNeed" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(needReloadThesis:) name:@"FinishBuyThesis" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FinishDeletedBuyThesis:) name:@"FinishDeletedBuyThesis" object:nil];
    
    [self reloadDataNew];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Perfil";
}

- (void)initUI
{
    // Tying up the segmented control to a scroll view
    //    self.pageControl.iNumberPage = arrSlider.count;
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    self.segmentedControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 50)];
    self.segmentedControl.isCheck = NO;
    self.segmentedControl.sectionTitles = @[@"PLANOS", @"ASSINATURA", @"DADOS",@"SAIR"];
    self.segmentedControl.backgroundColor = [LAUtilitys jusTapColor];
    self.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};;
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl.tag = 400;
    self.segmentedControl.userDraggable = NO;
    self.segmentedControl.type = HMSegmentedControlTypeText;
    self.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    [self.view addSubview:self.segmentedControl];
    
    [self.segmentedControl setSelectedSegmentIndex:2 animated:YES];
    [self.segmentedControl setFrame:CGRectMake(0, 0, viewWidth, 50)];
    if (self.selectedInitIndex == 0) {
        self.segmentedControl.selectedSegmentIndex = 0;
        [self  gotoIndex:0];
    }
    else{
        self.segmentedControl.selectedSegmentIndex = 2;
        [self  gotoIndex:2];
    }
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf gotoIndex:index];
    }];
}
#pragma mark - UIScrollViewDelegate
- (void)gotoIndex:(NSInteger)index
{
    if (index == 3)
    {
        [self resetStateAddNewComarca];
        signoutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LASignoutViewController"];
        [self.mainView addSubview:signoutVC.view];
        [self addChildViewController:signoutVC];
        [signoutVC didMoveToParentViewController:self];
    }
    else if (index == 2) {
        [self resetStateAddNewComarca];
        perfitVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LAPerfitViewController"];
        [self.mainView addSubview:perfitVC.view];
        [self addChildViewController:perfitVC];
        [perfitVC didMoveToParentViewController:self];
    }
    else if(index == 1)
    {
        [self resetStateAddNewComarca];
        self.addNewComarcaVC = nil;
        assignatureVC = [[UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil] instantiateViewControllerWithIdentifier:@"LASignaturesViewController"];
        assignatureVC.assinatureInfo = self.assinatureInfo;
        assignatureVC.lstTese = self.lstTese;
        [self.mainView addSubview:assignatureVC.view];
        [self addChildViewController:assignatureVC];
        [assignatureVC didMoveToParentViewController:self];
    }
    else if(index == 0){
        if (self.addNewComarcaVC) {
            [self.mainView addSubview:self.addNewComarcaVC.view];
            [self addChildViewController:self.addNewComarcaVC];
            self.addNewComarcaVC.assinatureInfo = self.assinatureInfo;
            [self.addNewComarcaVC didMoveToParentViewController:self];
        }
        else{
            planVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LAPlanosSelectionComarcaViewController"];
            planVC.assinatureInfo = self.assinatureInfo;
            [self.mainView addSubview:planVC.view];
            [self addChildViewController:planVC];
            
            [planVC didMoveToParentViewController:self];
        }
    }
}


-(void)resetStateAddNewComarca{
    self.addNewComarcaVC = nil;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_PLAN_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_DISTRICT_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_SIGNATURE_CART_ITEM];
}

-(void)ChangeTabSelectedIfNeed:(NSNotification*)notification{
    [self.segmentedControl setSelectedSegmentIndex:0 animated:YES];
    self.segmentedControl.selectedSegmentIndex = 0;
    [self  gotoIndex:0];
}

-(void)resetViewExcept:(NSInteger)index{
    [perfitVC.view removeFromSuperview];
    [perfitVC removeFromParentViewController];
    [signoutVC.view removeFromSuperview];
    [signoutVC removeFromParentViewController];
    [planVC.view removeFromSuperview];
    [planVC removeFromParentViewController];
    [assignatureVC.view removeFromSuperview];
    [assignatureVC removeFromParentViewController];
    [self.view layoutSubviews];
}


-(void)reloadDataNew{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawyerSignatures:^(NSInteger errorCode, NSString *message, id data) {
        isLoaded1 = YES;
        if (isLoaded2) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        self.assinatureInfo = [[LAAssinatureInfo alloc]initWithDictionary:data];
        if (self.assinatureInfo.success) {
            if (self.assinatureInfo.data && self.assinatureInfo.data.plan != nil && self.assinatureInfo.data.plan.dataIdentifier > 0) {
                [AppDelegate sharedDelegate].isDiscountAll = YES;
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:self.assinatureInfo.data] forKey:CURRENT_SIGNATURE_REGISTER];
                [def synchronize];
            }
            else{
                [AppDelegate sharedDelegate].isDiscountAll = NO;
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_SIGNATURE_REGISTER];
            }
        }
        else{
            [self showMessage:self.assinatureInfo.message withPoistion:nil];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_SIGNATURE_REGISTER];
        }
    }];
    [MainService lawyerGetSignatureTheses:^(NSInteger errorCode, NSString *message, id data) {
        isLoaded2 = YES;
        if (isLoaded1) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        [AppDelegate sharedDelegate].lstTese = self.lstTese;
        self.lstTese = [[LASignatureTese alloc]initWithDictionary:data];
        if (self.lstTese.success) {
        }
        else{
            [self showMessage:self.lstTese.message withPoistion:nil];
        }
    }];
    //    [MainService fetchUFs:^(NSInteger errorCode, NSString *message, id data) {
    //        isLoaded3 = YES;
    //        if (isLoaded1 && isLoaded2) {
    //            [MBProgressHUD hideHUDForView:self.view animated:YES];
    //        }
    //        [MBProgressHUD hideHUDForView:self.view animated:YES];
    //        self.listUFS = [[LAUFs alloc]initWithDictionary:data];
    //    }];
}
-(void)FinishBuyThesis{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawyerSignatures:^(NSInteger errorCode, NSString *message, id data) {
        isLoaded1 = YES;
        if (isLoaded2) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        self.assinatureInfo = [[LAAssinatureInfo alloc]initWithDictionary:data];
        if (self.assinatureInfo.success) {
            if (self.assinatureInfo.data && self.assinatureInfo.data.plan != nil && self.assinatureInfo.data.plan.dataIdentifier > 0) {
                planVC.assinatureInfo  = self.assinatureInfo;
                [AppDelegate sharedDelegate].isDiscountAll = YES;
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:self.assinatureInfo.data] forKey:CURRENT_SIGNATURE_REGISTER];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"NeedUpdatePlanosView" object:nil];
            }
            else{
                [AppDelegate sharedDelegate].isDiscountAll = NO;
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_SIGNATURE_REGISTER];
            }
        }
        else{
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_SIGNATURE_REGISTER];
            [self showMessage:self.assinatureInfo.message withPoistion:nil];
        }
    }];
    [MainService lawyerGetSignatureTheses:^(NSInteger errorCode, NSString *message, id data) {
        isLoaded2 = YES;
        if (isLoaded1) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        self.lstTese = [[LASignatureTese alloc]initWithDictionary:data];
        [AppDelegate sharedDelegate].lstTese = self.lstTese;
        if (self.lstTese.success) {
        }
        else{
            [self showMessage:self.lstTese.message withPoistion:nil];
        }
    }];
}
-(void)needReloadThesis:(NSNotification*)notification{
    [self FinishBuyThesis];
}
-(void)FinishDeletedBuyThesis:(NSNotification*)notification{
    self.assinatureInfo = nil;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CURRENT_SIGNATURE_REGISTER"];
    [AppDelegate sharedDelegate].isDiscountAll = NO;
    
}
@end

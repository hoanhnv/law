//
//  LAChangePassViewController.m
//  JustapLawyer
//
//  Created by MAC on 10/4/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAChangePassViewController.h"
#import "MBProgressHUD.h"
#import "LAAccountService.h"
#import "MainService.h"
#import "LAPopupMessageChangePass.h"
#import "AppDelegate.h"
@interface LAChangePassViewController ()

@end

@implementation LAChangePassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShouldBackButton = YES;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden = YES;
}
- (void)initUI
{
    self.title = @"Alterar senha";
}
- (BOOL)checkValid
{
    BOOL check = YES;
    if ([LAUtilitys isEmptyOrNull:self.txtPassword.text])
    {
        [self showMessage:NSLocalizedString(@"EMPTY_INFO", "@") withPoistion:nil];
        return NO;
    }
    if (![self.txtConfirmPassword.text isEqualToString:self.txtNewPassPassword.text])
    {
        [self showMessage:NSLocalizedString(@"COMFIRM PASS NOT MATCH", "@") withPoistion:nil];
        return NO;
    }
    return check;
}
- (IBAction)onChangePassPressed:(id)sender {
    [self.view endEditing:YES];
    if ([self checkValid])
    {
        [self.view endEditing:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [MainService changePassWord:self.txtPassword.text passWord:self.txtNewPassPassword.text confirmPassWord:self.txtConfirmPassword.text :^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([data isKindOfClass:[NSDictionary class]]) {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                NSString* message = [data objectForKey:KEY_RESPONSE_MESSAGE];
                [self showMessage:message andIsSuccess:success];
            }
        }];
    }
}
- (void)showMessage:(NSString*)message andIsSuccess:(BOOL)isSuccess
{
    LAPopupMessageChangePass *falePoup = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPopupMessageChangePass class])];
    NSString* strShow = message;
    if (isSuccess)
    {
        strShow = @"Senha alterada com sucesso";
    }
    LAContactSubjects* obj = [LAContactSubjects new];
    obj.message = strShow;
    obj.success = isSuccess;
    falePoup.responseSubject = obj;
    falePoup.delegate = self;
    falePoup.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    [self presentViewController:falePoup animated:YES completion:^{
        
    }];
}
- (void)doClosePopup:(BOOL)isSusccess
{
    if (isSusccess)
    {
        [[AppDelegate sharedDelegate] gotoLoginViewControllerNew];
    }
}
- (void)onExitPressed
{
    [self.navigationController popViewControllerAnimated:NO];
}
@end

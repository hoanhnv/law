//
//  LAMainInfoViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPageViewControl.h"
#import "LAIntroView.h"

@interface LAMainInfoViewController : LABaseViewController{
    NSMutableArray* arrData;
    NSMutableArray* imgDots;
}

@property (weak, nonatomic) IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UIButton *btnFAQ;
@property (weak, nonatomic) IBOutlet UIScrollView *scrIntroPath;
@property (weak, nonatomic) IBOutlet LAPageViewControl *pgControl;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;

@property (weak, nonatomic) IBOutlet UIButton *btnTermos;
- (IBAction)goFaqs:(id)sender;
- (IBAction)goContact:(id)sender;

- (IBAction)doTermos:(id)sender;
@end

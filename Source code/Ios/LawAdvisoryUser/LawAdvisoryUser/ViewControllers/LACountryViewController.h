//
//  CountryViewController.h
//  GoInk
//
//  Created by Mac on 9/21/15.
//  Copyright (c) 2015 MJH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LABaseViewController.h"
#import "LAAddressService.h"
#import "LADistrictObj.h"
#import "LABankObj.h"
#import "LABankInfo.h"

typedef enum TYPE_POPUP
{
    TYPE_CITY = 0,
    TYPE_STATE = 1,
    TYPE_NEWSTATE = 2,
    TYPE_NEWCITY = 3,
    TYPE_BANK = 4
}TYPE_POPUP;
@interface LACountryViewController : LABaseViewController{
    NSIndexPath *indexSelectPath;
}
@property (nonatomic,strong) id delegate;
@property (nonatomic, strong) NSArray *arrCountry;
@property (nonatomic,assign) TYPE_POPUP popType;
@property (nonatomic) LACityObj* currentCity;
@property (nonatomic) LADistrictObj* currentDistrict;
@property (nonatomic) LABankObj* currentBank;
@property (nonatomic) NSString* currentState;
@property (weak, nonatomic) IBOutlet UITableView *tblCity;
@property (nonatomic) BOOL isMultipleSelect;
@property (nonatomic) NSMutableArray* arrSelected;
-(IBAction)pressRightBarItem:(id)sender;
@end
@protocol LAMutilpleSelectedDelegate <NSObject>

- (void)didselectedList:(NSMutableArray*)listSelected;

@end

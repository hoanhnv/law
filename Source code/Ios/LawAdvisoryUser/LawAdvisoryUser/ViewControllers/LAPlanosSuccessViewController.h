//
//  LAPlanosSuccessViewController.h
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPlanObj.h"
#import "LASignatureObj.h"
@class LASignatureTese;
@interface LAPlanosSuccessViewController : LABaseViewController

@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic) NSMutableArray* arrSignature;

@property (weak, nonatomic) IBOutlet UIView *vBottom;

@property (nonatomic) IBOutlet UILabel* lbPlanosName;
@property (nonatomic) IBOutlet UILabel* lbTotalPlanSinging;

@property (nonatomic, weak) IBOutlet UITableView *tblSignature;
@property (nonatomic) IBOutlet UIScrollView* scrFullData;

@property (strong, nonatomic) LASignatureTese *lstTese;


@end

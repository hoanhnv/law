//
//  LAMessagePopupViewController.m
//  JustapLawyer
//
//  Created by Tùng Nguyễn on 2/23/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import "LAMessagePopupViewController.h"
#import "FooterTableViewCell.h"
#import "LAUserObj.h"
#import "LAMessageObj.h"

@interface LAMessagePopupViewController ()

@end

@implementation LAMessagePopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tbvMessages registerNib:[UINib nibWithNibName:@"FooterTableViewCell" bundle:nil] forCellReuseIdentifier:@"FooterTableViewCell"];
//    self.tbvMessages.frame = CGRectMake(self.tbvMessages.frame.origin.x, self.tbvMessages.frame.origin.y, self.tbvMessages.frame.size.width, self.view.bounds.size.height-60);
    
    self.tbvMessages.scrollEnabled = true;
    
    [self.tbvMessages setContentSize:CGSizeMake(1, 120*_arrData.count)];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tbvMessages reloadData];
//    self.contentOffset = self.tbvMessages.contentOffset;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FooterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FooterTableViewCell" forIndexPath:indexPath];
    cell.userInteractionEnabled = false;
    cell.textLabel.enabled = false;
    //    cell.lbContentSms.text = _arrMes[indexPath.row];
    UIImage *img = [UIImage new];
    LAMessageObj *mess = self.arrData[indexPath.row];
    cell.lbContentSms.text = mess.body;
    NSString *dateFormat = @"dd/MM";
    cell.lbDate.text = [LAUtilitys getDateStringFromTimeInterVal:mess.createdAt withFormat:dateFormat];
    cell.lbName.text = mess.user.name;
    if(mess.user) {
        img = [[UIImage imageNamed:@"bg_sms_content_send"] stretchableImageWithLeftCapWidth:24  topCapHeight:40];
        //        img.fram = cell.vAll.frame;
        cell.imgBgr.image = img;
    }
    else {
        img = [[UIImage imageNamed:@"bg_sms_content_receive"] stretchableImageWithLeftCapWidth:24  topCapHeight:40];
        //        img = cell.vAll.frame;
        cell.imgBgr.image=img;
    }
    img = [[UIImage imageNamed:@"bg_sms_content_send"] stretchableImageWithLeftCapWidth:24  topCapHeight:40];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    return self.arrData.count;
    return self.arrData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    LAMessageObj *mess = self.arrData[indexPath.row];
    return [FooterTableViewCell getHeight:mess.body inWith:tableView];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

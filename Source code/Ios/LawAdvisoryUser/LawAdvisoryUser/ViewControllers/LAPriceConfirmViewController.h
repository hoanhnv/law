//
//  LAPriceConfirmViewController.h
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface LAPriceConfirmViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (strong, nonatomic)  NSMutableArray *selectedTheseData;
@property (assign, nonatomic)  NSInteger countThese;

@property (strong, nonatomic)  NSString* inititalPriceAll;
@property (strong, nonatomic)  NSString* percentageSuccess;
@property (nonatomic)          BOOL isAll;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectionTeses;
@property (weak, nonatomic) IBOutlet UILabel* lbHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel* lbTextTitle;
@end

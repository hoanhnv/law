//
//  LAInfoViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "UINavigationBar+CustomHeight.h"
@interface LAInfoViewController : LABaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_View;
@property (weak, nonatomic) IBOutlet UIButton *btnFrequentes;
@property (weak, nonatomic) IBOutlet UIButton *btnContato;
@end

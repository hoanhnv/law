//
//  LAPagarCatartaoViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPagarCatartaoViewController.h"
#import "LADistrictObj.h"
#import "LACartItemViewCell.h"
#import "MainService.h"
#import "LAPlanosSuccessViewController.h"
#import "IQKeyboardManager.h"
#import "AppDelegate.h"
#import "LAPlanObj.h"
#import "LAAssinatureBuyObj.h"
#import "LAContactSubjects.h"
#import "FaleResponse.h"

@interface LAPagarCatartaoViewController ()<UITextFieldDelegate,MessagePopupDelegate>
@property (nonatomic, strong) NSString *strMethodCode;
@property (nonatomic, strong) NSString *strCompanyCode;
@end

@implementation LAPagarCatartaoViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tblSignature registerNib:[UINib nibWithNibName:@"LACartItemViewCell" bundle:nil] forCellReuseIdentifier:@"LACartItemViewCell"];
    [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText:@"Done"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initData
{
    self.lbPlanosName.text = [LAUtilitys getDefaultString:self.lanObj.name];
    NSString *strFirst = [LAUtilitys getDefaultString:self.lanObj.name];
    NSString *strSecond = [NSString stringWithFormat:@"%.0f%% das teses",self.lanObj.quantityOfTheses];
    
    NSString *strPlanInfo = [NSString stringWithFormat:@"%@ %@",strFirst,strSecond];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strPlanInfo];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont robotoRegular:14]
                             range:[strPlanInfo rangeOfString:strFirst]];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont robotoRegular:12]
                             range:[strSecond rangeOfString:strSecond]];
    
    self.lbPlanosName.attributedText = attributedString;
    
    self.lbTotalPlanSinging.text = [NSString stringWithFormat:@"Valor Total: R$ %@",[ LAUtilitys getCurrentcyFromDecimal:[self getTotalValue]]];
    [self.tblSignature reloadData];
    self.strMethodCode = @"credit_card";
    self.strCompanyCode = @"";
    //    [self fakeData];
}
- (void)initUI
{
    self.scrFullData.frame = CGRectMake(self.scrFullData.frame.origin.x, self.scrFullData.frame.origin.y, self.scrFullData.frame.size.width, self.view.frame.size.height );
}

-(void)fakeData{
    self.txtName.text = @"José da Silva";
    self.txtMonth.text = @"12";
    self.txtYear.text = @"20";
    self.txtCardNumber.text =@"4444 4444 4444 4448";
    self.txtSecurityCode.text = @"123";
    self.strMethodCode = @"credit_card";
    self.strCompanyCode = @"visa";
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"Planos";
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    [self setupFrame];
    if (_needUpdateView) {
        [self setupViewIfNeed];
    }
//    if (self.checkChangeCard) {
//        [self setupViewIfNeed];
//    }
    
}
- (void)setupFrame
{
    self.tblSignature.frame = CGRectMake(self.tblSignature.frame.origin.x, self.tblSignature.frame.origin.y, self.tblSignature.frame.size.width, [self.arrSignature count]*77);
    
    self.lbTotalPlanSinging.frame = CGRectMake(self.lbTotalPlanSinging.frame.origin.x, CGRectGetMaxY(self.tblSignature.frame) + 15, self.lbTotalPlanSinging.frame.size.width, self.lbTotalPlanSinging.frame.size.height);
    
    self.vSubBottom.frame = CGRectMake(self.vSubBottom.frame.origin.x, CGRectGetMaxY(self.lbTotalPlanSinging.frame) + 7, self.vSubBottom.frame.size.width,self.vSubBottom.frame.size.height);
    self.scrFullData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.vSubBottom.frame) + 64.0f);
    
}
-(void)setupViewIfNeed{
    
    [self.lbCatao setHidden:YES];
    [self.imgLine setHidden:YES];
    [self.tblSignature setHidden:YES];
    [self.lbTotalPlanSinging setHidden:YES];
    self.vSubBottom.frame = CGRectMake(self.vSubBottom.frame.origin.x, _lbCatao.frame.origin.y, self.vSubBottom.frame.size.width, self.vSubBottom.frame.size.height);
    self.scrFullData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.vSubBottom.frame) + 64.0f);
}
//-(void)setupViewIfNeed {
//    self.viewHeader.hidden = YES;
//    self.vSubBottom.frame = CGRectMake(0, 0, self.vSubBottom.frame.size.width, self.vSubBottom.frame.size.height);
//    self.scrFullData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.vSubBottom.frame) + 64.0f);
//}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.listDistric count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LACartItemViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LACartItemViewCell" forIndexPath:indexPath];
    [cell setupCell:self.arrSignature[indexPath.row] andIndexPath:indexPath andLan:self.lanObj];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77.0f;
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath isEqual:((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject])]){
        [self setupFrame];
    }
}
- (IBAction)doConfirm:(id)sender {
    @try {
        if(!self.needUpdateView){
            LASignatureObj *signatureObj = [self.arrSignature lastObject];
            //        double identifier = self.natureInfo.data.dataIdentifier;
            NSString *strMessage = STRING_EMPTY;
            if (signatureObj) {
                if ([self isValidForm:&strMessage]) {
                    double identifier = 0;
                    identifier = signatureObj.dataIdentifier;
                    NSString *strCardExpire = [NSString stringWithFormat:@"%@/%@",self.txtMonth.text,self.txtYear.text];
                    NSString *strCardNumber = [self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    NSDictionary *params = @{@"payment_method":@"cartao",
                                             @"card":@{@"holder_name":self.txtName.text,
                                                       @"card_expiration":strCardExpire,
                                                       @"card_number":strCardNumber,
                                                       @"card_cvv":self.txtSecurityCode.text,
                                                       @"payment_method_code":self.strMethodCode,
                                                       @"payment_company_code":self.strCompanyCode}
                                             };
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [MainService lawSignatures4:params withSignaruteId:identifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        if (data != nil) {
                            if ([data isKindOfClass:[NSDictionary class]]) {
                                if ([data[@"success"] boolValue]) {
                                    [self nextView];
                                }
                                else{
                                    [self showMessage:data[@"message"] withPoistion:nil];
                                }
                            }
                            else{
                                [self showMessage:message withPoistion:nil];
                            }
                        }
                        else{
                            [self showMessage:message withPoistion:nil];
                        }
                    }];
                }
                else{
                    [self showMessage:strMessage withPoistion:nil];
                }
            }
            else{
                [self showMessage:@"Signature inválido" withPoistion:nil];
            }
        }
        else{
            [self updateCardInfo];
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception:%@",exception.reason);
        NSLog(@"Exception userInfo:%@",exception.userInfo);
    } @finally {
        // TODO
    }
    
}
-(void)updateCardInfo{
    NSString *strCardExpire = [NSString stringWithFormat:@"%@/%@",self.txtMonth.text,self.txtYear.text];
    NSString *strCardNumber = [self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSDictionary *params = @{@"card":@{@"holder_name":self.txtName.text,
//                                       @"card_expiration":strCardExpire,
//                                       @"card_number":strCardNumber,
//                                       @"card_cvv":self.txtSecurityCode.text,
//                                       @"payment_method_code":self.strMethodCode,
//                                       @"payment_company_code":self.strCompanyCode}
//                             };
    NSMutableDictionary *param = [ NSMutableDictionary dictionary];
    NSMutableDictionary *dictParam = [NSMutableDictionary dictionary];
    NSString *message = STRING_EMPTY;
    [self isValidForm:&message];
    if (![LAUtilitys NullOrEmpty:self.txtName.text]) {
        [param setObject:self.txtName.text forKey:@"holder_name"];
    }
    if (![LAUtilitys NullOrEmpty:strCardExpire]) {
        [param setObject:strCardExpire forKey:@"card_expiration"];
    }
    if (![LAUtilitys NullOrEmpty:strCardNumber]) {
        [param setObject:strCardNumber forKey:@"card_number"];
    }
    if (![LAUtilitys NullOrEmpty:self.txtSecurityCode.text]) {
        [param setObject:self.txtSecurityCode.text forKey:@"card_cvv"];
    }
    if (![LAUtilitys NullOrEmpty:self.strMethodCode]) {
        [param setObject:self.strMethodCode forKey:@"payment_method_code"];
    }
    if (![LAUtilitys NullOrEmpty:self.strCompanyCode]) {
        [param setObject:self.strCompanyCode forKey:@"payment_company_code"];
    }
    if (param) {
        [dictParam setObject:param forKey:@"card"];
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService updateCard:dictParam withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSString* showMessage = message;
        BOOL isSuccess = NO;
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            isSuccess = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            showMessage    = [data objectForKey:KEY_RESPONSE_MESSAGE];
            
        }
        LAContactSubjects* iobj = [LAContactSubjects new];
        if (isSuccess) {
            iobj.message = MSG_023;
        }
        else{
            iobj.message = showMessage;
        }
        UIStoryboard *Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FaleResponse *falePoup = [Storyboard instantiateViewControllerWithIdentifier:@"FaleResponse"];
        falePoup.lbContent.textAlignment = NSTextAlignmentCenter;
        falePoup.responseSubject = iobj;
        falePoup.delegate = self;
        falePoup.isSuccess = isSuccess;
        falePoup.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [self presentViewController:falePoup animated:YES completion:^{
            
        }];
    }];
}
-(void) doClosePopup:(BOOL)isSusccess {
    if (isSusccess) {
        [self.navigationController popViewControllerAnimated:true];
    }
}
-(void)backview{
    [self showMessage:@"Assinatura atualizada com sucesso." withPoistion:nil];
    [self.navigationController popViewControllerAnimated:true];
}
-(BOOL)isValidForm:(NSString**)message{
    BOOL bCheck = YES;
    NSString *strCardNumber = [self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.strCompanyCode = [LAUtilitys cardTypeFromCardNumber:strCardNumber andCVV:self.txtSecurityCode.text];
    *message = STRING_EMPTY;
    if ([LAUtilitys NullOrEmpty:self.txtName.text]) {
        bCheck = NO;
        *message = MSG_011;
    }
    else if([LAUtilitys NullOrEmpty:self.txtCardNumber.text]){
        bCheck = NO;
        *message = MSG_012;
    }
    else if([self.txtCardNumber.text length] != 19){
        bCheck = NO;
        *message = MSG_013;
    }
    else if([LAUtilitys NullOrEmpty:self.txtMonth.text]){
        bCheck = NO;
        *message = MSG_014;
    }
    else if([self.txtMonth.text length] != 2){
        bCheck = NO;
        *message = MSG_015;
    }
    else if([LAUtilitys NullOrEmpty:self.txtYear.text]){
        bCheck = NO;
        *message = MSG_016;
    }
    else if([self.txtYear.text length] != 2){
        bCheck = NO;
        *message = MSG_017;
    }
    else if([LAUtilitys NullOrEmpty:self.txtSecurityCode.text]){
        bCheck = NO;
        *message = MSG_018;
    }
    else if([self.txtSecurityCode.text length] > 4 || [self.txtSecurityCode.text length]<3){
        bCheck = NO;
        *message = MSG_019;
    }
    else if ([LAUtilitys NullOrEmpty:self.strCompanyCode]){
        bCheck = NO;
        *message = MSG_020;
    }
    
    
    return bCheck;
}
-(void)nextView{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
        LAPlanosSuccessViewController *planSuccessVC = [storyboard instantiateViewControllerWithIdentifier:@"LAPlanosSuccessViewController"];
        planSuccessVC.arrSignature = self.arrSignature;
        planSuccessVC.lanObj = self.lanObj;
        planSuccessVC.listDistric = self.listDistric;
        [self.navigationController pushViewController:planSuccessVC animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int maxLength = 250;
    
    if (textField.tag == 10) {
        maxLength = 150;
    }
    else if(textField.tag == 11){
        maxLength = 19;
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength < textField.text.length)
        {
            
        }
        else
        {
            NSString* str = [textField.text stringByReplacingCharactersInRange:range withString:string];
            if ([str containsString:@"  "])
            {
                return NO;
            }
            if (range.location == 4 && ![string containsString:@" "]) {
                textField.text = [textField.text stringByAppendingString:@" "];
            }
            else if(range.location == 9 && ![string containsString:@" "]){
                textField.text = [textField.text stringByAppendingString:@" "];
            }
            else if(range.location == 14 && ![string containsString:@" "]){
                textField.text = [textField.text stringByAppendingString:@" "];
            }
        }
        
    }
    else if (textField.tag == 12){
        maxLength = 2;
    }
    else if (textField.tag == 13){
        maxLength = 2;
    }
    else if (textField.tag == 14){
        maxLength = 4;
    }
    if ([textField.text length] >= maxLength &&  ![string isEqualToString:STRING_EMPTY]) {
        textField.text = [textField.text substringToIndex:maxLength];
        return NO;
    }
    return YES;
}

-(float)getTotalValue{
    float totalValue = self.lanObj.value;
    if ([AppDelegate sharedDelegate].isDiscountAll) {
        totalValue = self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    
    for (int i=1;i<[self.arrSignature count];i++) {
        totalValue += self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    return totalValue;
    //    float totalValue = self.lanObj.value;
    //    
    //    for (int i=1;i<[self.arrSignature count];i++) {
    //        totalValue += self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    //    }
    //    return totalValue;
}

@end

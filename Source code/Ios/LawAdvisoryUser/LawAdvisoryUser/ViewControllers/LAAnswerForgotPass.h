//
//  LAAnswerForgotPass.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LAAnswerPopupDelegate <NSObject>

- (void)onExitPressed;
- (void)onForgotPassPressed;
@end

@interface LAAnswerForgotPass : UIViewController

@property (nonatomic, strong) NSString*strEmail;
@property (nonatomic, assign) BOOL bSuccess;
@property (nonatomic) id<LAAnswerPopupDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

- (IBAction)doClose:(id)sender;

@end


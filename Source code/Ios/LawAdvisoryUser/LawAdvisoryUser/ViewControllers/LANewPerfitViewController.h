//
//  LANewPerfitViewController.h
//  JustapLawyer
//
//  Created by MAC on 10/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AwesomeTextField.h"
#import "LAAccountObj.h"
#import "LAAccountService.h"
#import "LAAddressService.h"
#import "LAUFs.h"
#import "UploadView.h"
#import "DynamicTitleView.h"
#import "HMSegmentedControl.h"
#import "LABankObj.h"

@class  LALawyerInfo;

@interface LANewPerfitViewController : LABaseViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    //LEFT CONTENT
    IBOutlet UIButton* btConfirm;
    LAAccountObj* objAccount;
    NSMutableArray* arrImagesRequired;
    NSMutableArray* arrRequiredField;
    NSMutableArray* arrEmail;
    NSMutableArray* listCity;
    NSMutableArray* listState;
    LAUFs *listUFS;
    BOOL isSuccess;
}
@property (nonatomic) IBOutlet UITableView* tbPerfil;
@property (nonatomic) IBOutlet UIScrollView* scrDados;
@property (nonatomic) HMSegmentedControl* segmentedControl;
@property (nonatomic,strong) LALawyerInfo *objInfo;
@property (nonatomic) UIImage* imgAttach1;
@property (nonatomic) UIImage* imgAttach2;
@property (nonatomic) UIImage* imgAttach3;
@property (nonatomic) UIImage* imgAttach4;
@property (nonatomic) LAAccountObj* currentAccount;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btChangePassword;
@property (weak, nonatomic) IBOutlet UIButton *ckShowEmail;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtName;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtAddress;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtPassWord;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtNeighborhood;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtNewState;

@property (weak, nonatomic) IBOutlet AwesomeTextField *txtUF;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCPF;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCEP;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtDateOfBirth;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtMobilePhone;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtSite;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtLinkedin;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtNumberOAB;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtTitle;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtComplemento;
@property (nonatomic) LACityObj *currentCity;
@property (nonatomic) NSString *currentState;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtEndereco;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtNumber;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCity;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtPhone;
@property (weak, nonatomic) IBOutlet UIButton *cbShowCellphone;
@property (weak, nonatomic) IBOutlet UIButton *cbShowPhone;
@property (weak, nonatomic) IBOutlet UploadView *uploadView1;
@property (weak, nonatomic) IBOutlet UploadView *uploadView2;
@property (weak, nonatomic) IBOutlet UploadView *uploadView3;
@property (weak, nonatomic) IBOutlet UploadView *uploadView4;
@property (weak, nonatomic) IBOutlet DynamicTitleView *vDynamicTitle;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtBankAgenda;
@property (weak, nonatomic) IBOutlet UILabel *lbComprovante;
@property (weak, nonatomic) IBOutlet UIView *viewMiddle;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtAbout;
@property (nonatomic) IBOutlet UIScrollView* scrFullDataProfile;
//@property (nonatomic) IBOutlet UIScrollView* scrFullData;
@property (weak, nonatomic) IBOutlet UIView *vShowMail;
@property (weak, nonatomic) IBOutlet UIView *vShowCellPhone;
@property (weak, nonatomic) IBOutlet UIView *vShowPhone;
@property (nonatomic) IBOutlet TPKeyboardAvoidingScrollView* scrFullData;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UILabel *lbCpioTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbSeu;

@property (weak, nonatomic) IBOutlet AwesomeTextField *txtBankVerify;
@property (weak, nonatomic) IBOutlet UIView *vBankInfo;

@property (weak, nonatomic) IBOutlet AwesomeTextField *txtBankConta;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtBankName;
@property (strong, nonatomic) LABankObj *currentBank;

- (IBAction)doShowBankName:(id)sender;

@property (nonatomic,assign) BOOL isEditView;

- (IBAction)onShowEmailPressed:(id)sender;
- (IBAction)onChangePass:(id)sender;
@end

//
//  LAMainInfoViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAMainInfoViewController.h"
#import "LAFaleViewController.h"
#import "LAFrequentesViewController.h"
#import "TermAndServiceViewController.h"

#define NUMBER_SLIDE 4
@interface LAMainInfoViewController (){
    NSInteger currenPageIndex;    
}

@end

@implementation LAMainInfoViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShouldBackButton = NO;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Infomações";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont robotoBold:16],NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [LAUtilitys setButtonTextUnderLine:self.btnFAQ];
    [LAUtilitys setButtonTextUnderLine:self.btnContact];
    [LAUtilitys setButtonTextUnderLine:self.btnTermos];
    //    self.mainScroll.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,[);
    self.mainScroll.contentSize = CGSizeMake(1, 620);
    [self.mainScroll setShowsHorizontalScrollIndicator:NO];
    [self.mainScroll setShowsVerticalScrollIndicator:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initUI
{
    self.pgControl.iNumberPage = NUMBER_SLIDE;
    [self.pgControl setUpImage];
}
- (void)initData
{
    currenPageIndex = 0;
    arrData = [NSMutableArray arrayWithCapacity:NUMBER_SLIDE];
    NSDictionary* dict1 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro1",KEY_IMAGE,@"1- Encontre o plano ideal",KEY_TITLE,@"Você poderá se inscrever na quantidade de teses que desejar e atuar nas mais variadas causas do direito ao toque do seu celular. Comece agora a aparecer para seus futuros clientes.",KEY_DESCRIPTION, nil];
    NSDictionary* dict2 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro2",KEY_IMAGE,@"2- Precifique suas teses",KEY_TITLE,@"Saber precificar suas teses ajudará você a ser competitivo dentro do aplicativo. Proporcione aos seus futuros clientes preços convidativos e construa uma relação de confiança para trabalhos constantes.",KEY_DESCRIPTION, nil];
    NSDictionary* dict3 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro3",KEY_IMAGE,@"3- Seja sempre bem avaliado",KEY_TITLE,@"Tudo que você faz no aplicativo proporciona possibilidade de avaliação por seus clientes. O tempo de resposta, sua prontidão, iniciativa e trabalho organizado geram confiança e segurança para os usuários do JusTap. Isso garante a você melhor posicionamento na lista de busca de profissionais.",KEY_DESCRIPTION, nil];
    NSDictionary* dict4 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro4",KEY_IMAGE,@"4- Promova a justiça",KEY_TITLE,@"O JusTap é seu ajudante jurídico de bolso, com ele você poderá indicar para seus amigos e familiares e construir uma rede de negócios que vai além dos seus limites físicos. Indique!",KEY_DESCRIPTION, nil];
    [arrData addObject:dict1];
    [arrData addObject:dict2];
    [arrData addObject:dict3];
    [arrData addObject:dict4];
    CGSize sizeScroll = self.scrIntroPath.contentSize;
    sizeScroll.width = CGRectGetWidth(self.scrIntroPath.frame) * NUMBER_SLIDE;
    [self.scrIntroPath setContentSize:sizeScroll];
    CGRect rectScroll = self.scrIntroPath.frame;
    for (int  i = 0; i < arrData.count; i ++)
    {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"LAIntroView"
                                                          owner:self
                                                        options:nil];
        LAIntroView* viewCustom = (LAIntroView*)[nibViews objectAtIndex:0];
        CGRect frameView = CGRectMake(CGRectGetWidth(rectScroll)*i, 0, CGRectGetWidth(rectScroll), CGRectGetHeight(rectScroll));
        [self.scrIntroPath addSubview:viewCustom];
        [viewCustom setFrame:frameView];
        NSDictionary* dictData = [arrData objectAtIndex:i];
        [viewCustom setupDataWith:dictData];
    }
}
#pragma mark UIScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrIntroPath.frame.size.width;
    float currentPageHere = self.scrIntroPath.contentOffset.x / pageWidth;
    if (0.0f != fmodf(currentPageHere, 1.0f))
    {
        currentPageHere = 0;
    }
    currenPageIndex = currentPageHere;
    [self gotoCurrentPage:currentPageHere];
}
- (void)gotoCurrentPage:(NSInteger)page
{
    [self.pgControl setCurrentPage:page];
    
}
- (IBAction)goFaqs:(id)sender {
    LAFrequentesViewController* duvidasVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAFrequentesViewController class])];
    duvidasVC.isShouldBackButton = YES;
    duvidasVC.isShouldShowBottombar = YES;
    duvidasVC.isShowGreenNavigation = NO;
    duvidasVC.isShouldShowBigHeightNavigationBar = NO;
    [self.navigationController pushViewController:duvidasVC animated:YES];
}

- (IBAction)goContact:(id)sender {
    LAFaleViewController* faleVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAFaleViewController class])];
    [self.navigationController pushViewController:faleVC animated:YES];
}

- (IBAction)doTermos:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    TermAndServiceViewController *termAndServiceVC  = [storyBoard instantiateViewControllerWithIdentifier:@"TermAndServiceViewController"];
    [self.navigationController pushViewController:termAndServiceVC animated:YES];
}
@end

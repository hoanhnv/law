//
//  LAPagarCatartaoViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"
#import "LAPlanObj.h"
#import "AwesomeTextField.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "LASignatureObj.h"
#import "LAAssinatureInfo.h"

@interface LAPagarCatartaoViewController : LABaseViewController
{
    
}
@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic) LASignatureObj* signatureObj;
@property (nonatomic) NSMutableArray* arrSignature;

//@property (nonatomic) BOOL* checkChangeCard;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (nonatomic) IBOutlet UILabel* lbPlanosName;
@property (nonatomic) IBOutlet UILabel* lbTotalPlanSinging;
@property (weak, nonatomic) IBOutlet UILabel *lbCatao;
@property (weak, nonatomic) IBOutlet UIImageView *imgLine;
@property (strong, nonatomic) LAAssinatureInfo *natureInfo;
@property (nonatomic, weak) IBOutlet UITableView *tblSignature;

@property (weak, nonatomic) IBOutlet AwesomeTextField *txtName;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCardNumber;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtMonth;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtYear;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtSecurityCode;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrFullData;
@property (weak, nonatomic) IBOutlet UIView *vSubBottom;
@property (assign,nonatomic) BOOL needUpdateView;
- (IBAction)doConfirm:(id)sender;
-(void)setupViewIfNeed;
@end

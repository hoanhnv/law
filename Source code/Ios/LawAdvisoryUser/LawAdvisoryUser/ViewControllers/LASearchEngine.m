//
//  LASearchEngine.m
//  JustapLawyer
//
//  Created by MAC on 10/10/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASearchEngine.h"
#import "LALawSuitCategory.h"
#import "LALawsuitObj.h"
#import "LAUtilitys.h"
#import "LALawSuitCategory.h"
@implementation LASearchEngine
+ (instancetype)sharedInstance {
    static LASearchEngine *_shareSearchEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareSearchEngine = [[LASearchEngine alloc] init];
        _shareSearchEngine.isGettingData = NO;
        _shareSearchEngine.isSearching = NO;
    });
    
    return _shareSearchEngine;
}
- (void)loadData
{
    self.arrAllData = [NSMutableArray new];
    self.isGettingData = YES;
    [MainService fetchAllsuit:^(NSInteger errorCode, NSString *message, id data) {
        self.isGettingData = NO;
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            if (success)
            {
                id responseData = [data objectForKey:KEY_RESPONSE_DATA];
                if (responseData && [responseData isKindOfClass:[NSArray class]])
                {
                    for (NSDictionary* dict in responseData)
                    {
                        LALawsuitObj* objSub = [[LALawsuitObj alloc] initWithDictionary:dict];
                        if (objSub)
                        {
                            [self.arrAllData addObject:objSub];
                        }
                        
                    }
                }
            }
        }
        if (self.isSearching && self.searchBlock && ![LAUtilitys isEmptyOrNull:self.currentKeyword])
        {
            [self filterWithKeyword:self.currentKeyword wComPletionBlock:self.searchBlock];
        }
    }];
}
- (void)filterNameWithKeyword:(NSString*)keyword fromSourceData:(NSMutableArray*)arrData wComPletionBlock:(CompletionBlock)block
{
    NSMutableArray* allData = [NSMutableArray new];
    if (arrData && arrData.count)
    {
        
        for (LALawSuitCategory* objCategory in arrData)
        {
            if ([objCategory isKindOfClass:[LALawSuitCategory class]])
            {
                if (objCategory.arrData.count)
                {
                    for (LALawsuitObj* objSub in objCategory.arrData) {
                        [allData addObject:objSub];
                    }
                }
            }
        }
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(legalName CONTAINS[cd] %@)",
                             keyword];
        NSMutableArray* arrResult = [[allData filteredArrayUsingPredicate:pred] mutableCopy];
        block(0,@"",arrResult);
    }
    else
    {
        block(0,@"",[NSMutableArray new]);
    }
}

- (void)filterWithKeyword:(NSString*)keyword wComPletionBlock:(CompletionBlock)block
{
    self.isSearching = YES;
    NSArray *arrKeyWord = [keyword componentsSeparatedByString:@" "];
    if ([arrKeyWord count]) {
        
    }
    if (self.isGettingData)
    {
        self.currentKeyword = keyword;
        self.searchBlock = block;
        return;
    }
    if (self.arrAllData)
    {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name CONTAINS[cd] %@) or (legalName CONTAINS[cd] %@) or (documentation CONTAINS[cd] %@) or (yourRights CONTAINS[cd] %@) or (identification CONTAINS[cd] %@) or (howToAct CONTAINS[cd] %@) or (name CONTAINS[cd] %@) or (legalName CONTAINS[cd] %@) or (documentation CONTAINS[cd] %@) or (yourRights CONTAINS[cd] %@) or (identification CONTAINS[cd] %@) or (howToAct CONTAINS[cd] %@) or (name CONTAINS[cd] %@) or (legalName CONTAINS[cd] %@) or (documentation CONTAINS[cd] %@) or (yourRights CONTAINS[cd] %@) or (identification CONTAINS[cd] %@) or (howToAct CONTAINS[cd] %@)",
                             keyword, keyword,keyword,keyword,keyword,keyword,
                             [keyword uppercaseString],[keyword uppercaseString],
                             [keyword uppercaseString],[keyword uppercaseString],
                             [keyword uppercaseString],[keyword uppercaseString],
                             [keyword lowercaseString],[keyword lowercaseString],
                             [keyword lowercaseString],[keyword lowercaseString],
                             [keyword lowercaseString],[keyword lowercaseString]];
        
        self.arrResult = [[self.arrAllData filteredArrayUsingPredicate:pred] mutableCopy];
        self.isSearching = YES;
        block(0,@"",self.arrResult);
    }
    else
    {
        self.isSearching = YES;
        block(0,@"",self.arrResult);
    }
}
@end

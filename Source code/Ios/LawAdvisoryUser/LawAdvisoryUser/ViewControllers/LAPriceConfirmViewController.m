//
//  LAPriceConfirmViewController.m
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPriceConfirmViewController.h"
#import "LATesesHeaderView.h"
#import "LATesesMainTableViewCell.h"
#import "UIView+NibInitializer.h"
#import "LATesesPriceHeaderView.h"
#import "ServiceTesesFooterView.h"
#import "LACancelamentViewController.h"
#import "HeaderSeviceTeses.h"
#import "MainService.h"
#import "ServicePriceFooterView.h"
#import "LAPriceConfirmTableViewCell.h"
#import "LAPriceAllConfirmTableViewCell.h"
#import "AppDelegate.h"
#import "FaleResponse.h"
#import "LAAnswerForgotPass.h"
#import "LAAddNewComarcaViewController.h"
#import "MainProfileViewController.h"
@interface LAPriceConfirmViewController ()<MessagePopupDelegate>
{
    NSMutableArray *arrCategories;
    NSMutableDictionary *mSelected;
    NSInteger iNumberRequest;
}
@property (nonatomic, strong) NSMutableArray *arrTheseData;
@end

@implementation LAPriceConfirmViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
        self.isAll = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tblContent registerNib:[UINib nibWithNibName:NSStringFromClass([LAPriceConfirmTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPriceConfirmTableViewCell class])];
    [self.tblContent registerNib:[UINib nibWithNibName:NSStringFromClass([LAPriceAllConfirmTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPriceAllConfirmTableViewCell class])];
    self.lbHeaderTitle.text = [NSString stringWithFormat:kTitleHeader,[self getCountSelectedThese],[AppDelegate sharedDelegate].countThese];
    [LAUtilitys setButtonTextUnderLine:self.btnSelectionTeses];
    self.btnSelectionTeses.hidden = YES;
    self.arrTheseData = [NSMutableArray array];
    for (LALawSuitCategory *suitCate in self.selectedTheseData) {
        if ([suitCate.arrData count]) {
            [self.arrTheseData addObjectsFromArray:suitCate.arrData];
        }
    }
    
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Teses";
}
- (void)initData
{
    if (self.isAll)
    {
        self.lbTextTitle.text = @"Você está precificando todas as teses que atende em:";
    }
    [self.tblContent reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - HEADER TABLEVIEW DELEGATE
- (IBAction)onSelectionMaisTeses:(id)sender
{
}
- (IBAction)onConfirmPressed:(id)sender
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *arrTheses = [NSMutableArray new];;
    if ([self.arrTheseData count])
    {
        for (LALawsuitObj* objSuitObj in self.arrTheseData)
        {
            NSString* initPrice = objSuitObj.initialPrice;
            NSString* percentagePrice = objSuitObj.percentagePrice;
            if (self.isAll)
            {
                initPrice = self.inititalPriceAll;
                percentagePrice = self.percentageSuccess;
            }
            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:objSuitObj.dataIdentifier],@"thesis_id",initPrice,@"initial_fees",percentagePrice,@"percentage_in_success", nil];
            [arrTheses addObject:dict];
        }
    }
    if (arrTheses.count)
    {
        //        [params setObject:arrTheses forKey:@"theses"];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [MainService lawyerUpdateThese:@{@"theses":arrTheses} withSuccess:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.tblContent reloadData];
            NSLog(@"%@",[data description]);
            NSString* showMessage = message;
            BOOL isSuccess = NO;
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                isSuccess = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                showMessage    = [data objectForKey:KEY_RESPONSE_MESSAGE];
                
            }
            LAContactSubjects* iobj = [LAContactSubjects new];
            if (isSuccess) {
                iobj.message = MSG_023;
            }
            else{
                iobj.message = showMessage;
            }
            
            FaleResponse *falePoup = [self.storyboard instantiateViewControllerWithIdentifier:@"FaleResponse"];
            falePoup.lbContent.textAlignment = NSTextAlignmentCenter;
            falePoup.responseSubject = iobj;
            falePoup.delegate = self;
            falePoup.isSuccess = isSuccess;
            falePoup.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [self presentViewController:falePoup animated:YES completion:^{
            }];
        }];
    }
    
}
- (void)doClosePopup:(BOOL)isSusccess
{
    if (isSusccess)
    {
        MainProfileViewController* mainVC = nil;
        NSArray* arrVC = self.navigationController.viewControllers;
        for (UIViewController* vc  in arrVC) {
            if ([vc isKindOfClass:[MainProfileViewController class]])
            {
                mainVC = (MainProfileViewController*)vc;
                break;
            }
        }
        if (mainVC)
        {
            mainVC.addNewComarcaVC = nil;
            [mainVC.segmentedControl setSelectedSegmentIndex:0];
            [mainVC  gotoIndex:0];
            [mainVC FinishBuyThesis];
            [self.navigationController popToViewController:mainVC animated:YES];
        }
    }
}

#pragma mark - TABLEVIEW DATASOUCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isAll) {
        return 1;
    }
    return [self.selectedTheseData count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isAll)
    {
        LAPriceAllConfirmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAPriceAllConfirmTableViewCell class])];
        if (indexPath.row < self.arrTheseData.count)
        {
            LALawsuitObj* obj = self.arrTheseData[indexPath.row];
            [cell setupData:obj];
        }        return  cell;
    }
    else
    {
        LAPriceConfirmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAPriceConfirmTableViewCell class])];
        LALawsuitObj* obj = self.arrTheseData[indexPath.row];
        [cell setupData:obj];
        return  cell;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isAll)
    {
        return 90.0;
    }
    return 126.0f;
}

-(NSInteger)getCountSelectedThese{
    NSInteger count = 0;
    for (LALawSuitCategory *cate in self.selectedTheseData) {
        count += [cate.arrData count];
    }
    return count;
}

@end


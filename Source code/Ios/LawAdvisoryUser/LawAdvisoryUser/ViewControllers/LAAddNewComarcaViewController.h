//
//  LAAddNewComarcaViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "AwesomeTextField.h"
#import "LADistrictObj.h"
#import "LAUFs.h"
#import "LAListDistrictByUF.h"
#import "LAPlanObj.h"
#import "LASignatureObj.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "LAAssinatureInfo.h"
@interface LAAddNewComarcaViewController : LABaseViewController{
    
    NSMutableArray* listCity;
    NSMutableArray* listEstado;
    LAUFs *listUFS;
    LAListDistrictByUF *lstData;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lbPlanosRegisted;
@property (weak, nonatomic) IBOutlet UIView *vBottom;
@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic) NSMutableArray* arrSignature;

@property (nonatomic) IBOutlet UILabel* lbHeader;
@property (nonatomic) IBOutlet UILabel* lbDescription;
@property (nonatomic) IBOutlet TPKeyboardAvoidingScrollView* scrFullData;
@property (nonatomic) IBOutlet UIButton* btConfirm;
@property (nonatomic) IBOutlet UIView* viewMiddle;
@property (weak, nonatomic) IBOutlet UIView *vTopHeader;

@property (weak, nonatomic) IBOutlet AwesomeTextField *txtEstado;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCity;

@property (nonatomic) LADistrictObj *currentDistrict;
@property (nonatomic) NSMutableArray* listDistrictSelected;
@property (nonatomic) NSMutableArray* listOldDistrictSelected;

@property (nonatomic) NSString *currentState;
@property (nonatomic) NSMutableArray* arrRequiredField;

@property (strong, nonatomic) LAAssinatureInfo *assinatureInfo;

@property (weak, nonatomic) IBOutlet UILabel *lbComacar;
@property (weak, nonatomic) IBOutlet UILabel *lbTotal;

- (IBAction)onConfirm:(id)sender;
@end

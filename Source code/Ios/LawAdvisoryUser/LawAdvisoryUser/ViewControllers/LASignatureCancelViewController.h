//
//  LASignatureCancelViewController.h
//  JustapLawyer
//
//  Created by Mac on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LABaseViewController.h"

@interface LASignatureCancelViewController : LABaseViewController
@property (nonatomic, strong) NSDictionary *objCancel;
@property (weak, nonatomic) IBOutlet UILabel *lbMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property(nonatomic, strong) NSString *titleNavigation;
@property(nonatomic, strong) UIColor *navigationColor;
@property(nonatomic) BOOL isFinishAction;

- (IBAction)doCancel:(id)sender;

@end

//
//  LAPlanosSuccessViewController.m
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanosSuccessViewController.h"
#import "LACartItemViewCell.h"
#import "LAServiceTesesViewController.h"
#import "LATesesMainViewController.h"
#import "AppDelegate.h"
#import "LASignatureTese.h"
#import "MainService.h"
#import "LASignatureTeseObj.h"

@interface LAPlanosSuccessViewController ()

@end

@implementation LAPlanosSuccessViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tblSignature registerNib:[UINib nibWithNibName:@"LACartItemViewCell" bundle:nil] forCellReuseIdentifier:@"LACartItemViewCell"];
    
    self.scrFullData.frame = CGRectMake(self.scrFullData.frame.origin.x, self.scrFullData.frame.origin.y, self.scrFullData.frame.size.width, self.scrFullData.frame.size.height);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initData
{
    self.lbPlanosName.text = [LAUtilitys getDefaultString:self.lanObj.name];
    NSString *strFirst = [LAUtilitys getDefaultString:self.lanObj.name];
    NSString *strSecond = [NSString stringWithFormat:@"%.0f%% das teses",self.lanObj.quantityOfTheses];
    
    NSString *strPlanInfo = [NSString stringWithFormat:@"%@ %@",strFirst,strSecond];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strPlanInfo];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont robotoRegular:14]
                             range:[strPlanInfo rangeOfString:strFirst]];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont robotoRegular:12]
                             range:[strSecond rangeOfString:strSecond]];
    
    self.lbPlanosName.attributedText = attributedString;
    
    self.lbTotalPlanSinging.text = [NSString stringWithFormat:@"Valor Total: R$ %@",[ LAUtilitys getCurrentcyFromDecimal:[self getTotalValue]]];
    [self.tblSignature reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"Planos";
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    [self setupFrame];
    [self checkThesisIfNeed];
}

-(void)setupFrame{
    self.tblSignature.frame = CGRectMake(self.tblSignature.frame.origin.x, self.tblSignature.frame.origin.y, self.tblSignature.frame.size.width, [self.arrSignature count]*77);
    
    self.lbTotalPlanSinging.frame = CGRectMake(self.lbTotalPlanSinging.frame.origin.x, CGRectGetMaxY(self.tblSignature.frame) + 15, self.lbTotalPlanSinging.frame.size.width, self.lbTotalPlanSinging.frame.size.height);
    
    self.vBottom.frame = CGRectMake(self.vBottom.frame.origin.x, CGRectGetMaxY(self.lbTotalPlanSinging.frame) + 7, self.vBottom.frame.size.width,self.vBottom.frame.size.height);
    
    self.scrFullData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.vBottom.frame));
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.listDistric count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LACartItemViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LACartItemViewCell" forIndexPath:indexPath];
    [cell setupCell:self.arrSignature[indexPath.row] andIndexPath:indexPath andLan:self.lanObj];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77.0f;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath isEqual:((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject])]){
        [self setupFrame];
    }
}

- (IBAction)doConfirm:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    
    LATesesMainViewController *serviceTesesMain = [storyboard instantiateViewControllerWithIdentifier:@"LATesesMainViewController"];
    serviceTesesMain.arrSignature = self.arrSignature;
    serviceTesesMain.lanObj = self.lanObj;
    serviceTesesMain.listDistric = self.listDistric;
    serviceTesesMain.buyThese = [self buildSelectedThese];
    [self.navigationController pushViewController:serviceTesesMain animated:YES];
}

-(float)getTotalValue{
    float totalValue = self.lanObj.value;
    if ([AppDelegate sharedDelegate].isDiscountAll) {
        totalValue = self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    
    for (int i=1;i<[self.arrSignature count];i++) {
        totalValue += self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    return totalValue;
}


-(NSMutableArray*)buildSelectedThese{
    NSMutableArray *arrData = [NSMutableArray array];
    NSInteger countItem = [[AppDelegate sharedDelegate].lstTese.data count];
    for (NSInteger i=0;i< countItem;i++) {
        LASignatureTeseObj *s1 = [self.lstTese.data objectAtIndex:i];
        s1.lawsuitThesis.initialPrice = [NSString stringWithFormat:@"%.2f",s1.initialFees];
        s1.lawsuitThesis.percentagePrice = [NSString stringWithFormat:@"%.2f",s1.percentageInSuccess];
        LALawSuitCategory* suitCate = [[LALawSuitCategory alloc]init];
        suitCate = s1.lawsuitThesis.category;
        if (!suitCate.arrData) {
            suitCate.arrData = [NSMutableArray array];
        }
        else{
            [suitCate.arrData removeAllObjects];
        }
        [suitCate.arrData addObject:s1.lawsuitThesis];
        
        for (NSInteger j = countItem - 1; j>i;j--) {
            LASignatureTeseObj *s2 = [self.lstTese.data objectAtIndex:j];
            if (s1.lawsuitThesis.categoryId == s2.lawsuitThesis.categoryId) {
                [suitCate.arrData addObject:s2.lawsuitThesis];
            }
            if (j == countItem - 1 && ![arrData containsObject:suitCate]) {
                [arrData addObject:suitCate];
            }
        }
        if (countItem == 1) {
            [arrData addObject:suitCate];
        }
    }
    return arrData;
}

-(void)checkThesisIfNeed{
    self.lstTese = [AppDelegate sharedDelegate].lstTese;
    if (![[AppDelegate sharedDelegate].lstTese.data count]) {
        [MainService lawyerGetSignatureTheses:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [AppDelegate sharedDelegate].lstTese = self.lstTese;
            self.lstTese = [[LASignatureTese alloc]initWithDictionary:data];
            if (self.lstTese.success) {
            }
            else{
                [self showMessage:self.lstTese.message withPoistion:nil];
            }
        }];
    }
}
@end

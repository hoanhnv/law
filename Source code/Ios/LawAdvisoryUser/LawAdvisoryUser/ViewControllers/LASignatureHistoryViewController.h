//
//  LASignatureHistoryViewController.h
//  JustapLawyer
//
//  Created by Mac on 11/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "MainService.h"

@interface LASignatureHistoryViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tblHistory;

@end

//
//  LANovaAoesDetailViewController.m
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LANovaAoesDetailViewController.h"
#import "LACustomHeaderNovasDetalView.h"
#import "LABasicInfoDetalTableViewCell.h"
#import "LARelatoDoCasoTableViewCell.h"
#import "LADocumentCollectionCell.h"
#import "LAMoreInfoDetailCell.h"
#import "LADocumentNovasDetailCell.h"
#import "LAAudioPlayerController.h"
#import "LAPhotoShowViewController.h"
#import "LAPopupMessageChangePass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LAMedias.h"
#import "LAPopupWitnessViewController.h"
#import "LAWitnesses.h"
#import "MainService.h"
#import "LAActionDataObj.h"
#import "LAActionResponseInfo.h"
#import "LARefusalObj.h"
#import "LARecusaReasonViewController.h"
#import "LAAcoesViewController.h"
#import "UIColor+Law.h"
#import "LAWitnessTableViewCell.h"
#import "LANovasDetailFooter.h"
#import "UIView+NibInitializer.h"

@import AVFoundation;
@import AVKit;
@import MediaPlayer;

static float klHeightHeaderDetail = 50.0f;
@interface LANovaAoesDetailViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,AcoesNovaDetailCellDelegate,RecusaReasonDelegate,RelatoDelegate>
{
    LANovasDetailFooter *footer;
}
@end

@implementation LANovaAoesDetailViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.isShouldShowBottombar = NO;
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LAAudioPlayerController sharedInstance] stop];
    
    LABasicInfoDetalTableViewCell *cell = (LABasicInfoDetalTableViewCell*)[tbData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell saveTimer:NO];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)initUI
{
    //    self.title = @"Detalhe do assunto";
    self.title = self.objData.happening.thesis.name;
    
}
- (void)initData
{
    arrTitleHeader = [NSMutableArray arrayWithObjects:@"", nil];
    arrDocuments = [NSMutableArray new];
    arrAudio = [NSMutableArray new];
//    LAMedias *audio = [LAMedias new];
//#warning REMOVE DATA TEST
//    audio.mediaWithUrl = @"http://320.s1.mp3.zdn.vn/005c3b7ce6380f665629/4853722157520406933?key=0jRSfdkpA4RFDnE7SVENzQ&expires=1493228751";
//    audio.type = @"mp3";
//    [arrAudio addObject:audio];
//    LAMedias *audio2 = [LAMedias new];
//    audio2.mediaWithUrl = @"http://320.s1.mp3.zdn.vn/65f714fdfbb912e74ba8/3957087340509527650?key=jKHPCYGgZelzo541zQkgwg&expires=1493228383";
//    audio2.type = @"mp3";
//    [arrAudio addObject:audio2];
    [tbData registerNib:[UINib nibWithNibName:NSStringFromClass([LABasicInfoDetalTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LABasicInfoDetalTableViewCell class])];
    
    [tbData registerNib:[UINib nibWithNibName:NSStringFromClass([LARelatoDoCasoTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LARelatoDoCasoTableViewCell class])];
    
    [tbData registerNib:[UINib nibWithNibName:NSStringFromClass([LADocumentNovasDetailCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LADocumentNovasDetailCell class])];
    
    [tbData registerNib:[UINib nibWithNibName:NSStringFromClass([LAMoreInfoDetailCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAMoreInfoDetailCell class])];
    [tbData registerNib:[UINib nibWithNibName:NSStringFromClass([LAWitnessTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAWitnessTableViewCell class])];

    if (self.objData && self.objData.happening.medias.count)
    {
        for (LAMedias* obj in self.objData.happening.medias)
        {
            if ([obj.type isEqualToString:@"document"])
            {
                [arrDocuments addObject:obj];
            }
            else if ([obj.type isEqualToString:@"audio"]||[obj.type isEqualToString:@"video"])
            {
                [arrAudio addObject:obj];
            }
        }
    }
    if (arrAudio !=nil) {
        [arrTitleHeader addObject:@"RELATO DO CASO"];
    }
    if ([arrDocuments count]) {
        [arrTitleHeader addObject:@"DOCUMENTOS ANEXADOS"];
    }
    if ([self.objData.happening.witnesses count]) {
        [arrTitleHeader addObject:@"TESTEMUNHAS"];
        
    }
    if (![LAUtilitys NullOrEmpty:self.objData.happening.otherInformation]) {
        [arrTitleHeader addObject:@"INFORMAÇÕES ADICIONAIS"];
        
    }
    self.title = self.objData.happening.thesis.name;
    tbData.separatorColor = [UIColor clearColor];
    tbData.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    NSArray* arrFooter = [[NSBundle mainBundle] loadNibNamed:@"LANovasDetailFooter"
//                                                        owner:self
//                                                      options:nil];

    footer = [[LANovasDetailFooter alloc]initWithNibNamed:@"LANovasDetailFooter"];
    [footer setDelegate:self];
    tbData.tableFooterView = footer;
    [tbData reloadData];
}
#pragma mark TableView DataSource
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return nil;
    }
    LACustomHeaderNovasDetalView* view = [[[NSBundle mainBundle]
                                           loadNibNamed:NSStringFromClass([LACustomHeaderNovasDetalView class])
                                           owner:self options:nil]
                                          firstObject];
    view.lbTitle.text = [arrTitleHeader objectAtIndex:(section)];
    view.delegate = self;
    if([[arrTitleHeader objectAtIndex:section]isEqualToString:@"TESTEMUNHAS"]){
        [view.lbNumbers setHidden:NO];
        view.lbNumbers.text = [NSString stringWithFormat:@"%ld",self.objData.happening.witnesses.count];
        view.userInteractionEnabled = YES;
    }
    else
    {
        [view.lbNumbers setHidden:YES];
        view.userInteractionEnabled = NO;
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0.0f;
    }
    return klHeightHeaderDetail;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0://BASIC INFO
        {
            return 180;
        }
            break;
        case 1://VOICE RECORD
        {
            if ([[arrTitleHeader objectAtIndex:1]isEqualToString:@"RELATO DO CASO"]) {
                return 75.0;
            }
            else if([[arrTitleHeader objectAtIndex:1]isEqualToString:@"DOCUMENTOS ANEXADOS"]){
                if (arrDocuments.count)
                {
                    return ([arrDocuments count]/2 + [arrDocuments count]%2)*140;
                }
                else
                {
                    return 0;
                }
            }
            else if([[arrTitleHeader objectAtIndex:1]isEqualToString:@"TESTEMUNHAS"]){
                return 44.0;
            }
            else if([[arrTitleHeader objectAtIndex:1]isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
                return [LAMoreInfoDetailCell getHeight:self.objData.happening.otherInformation inWith:tbData];
            }
        }
            break;
        case 2://DOCUMENTS
        {
            if([[arrTitleHeader objectAtIndex:2]isEqualToString:@"DOCUMENTOS ANEXADOS"]){
                if (arrDocuments.count)
                {
                    return ([arrDocuments count]/2 + [arrDocuments count]%2)*140;
                }
                else
                {
                    return 0;
                }
            }
            else if([[arrTitleHeader objectAtIndex:2]isEqualToString:@"TESTEMUNHAS"]){
                return 44.0;
            }
            else if([[arrTitleHeader objectAtIndex:2]isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
                return [LAMoreInfoDetailCell getHeight:self.objData.happening.otherInformation inWith:tbData];
            }
        }
            break;
        case 3://TESTERMUNHAS
        {
            if([[arrTitleHeader objectAtIndex:3]isEqualToString:@"TESTEMUNHAS"]){
                return 44.0;
            }
            else if([[arrTitleHeader objectAtIndex:3]isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
                return [LAMoreInfoDetailCell getHeight:self.objData.happening.otherInformation inWith:tbData];
            }
        }
            break;
        case 4://MORE INFO
        {
            return [LAMoreInfoDetailCell getHeight:self.objData.happening.otherInformation inWith:tbData];
        }
            break;
        default:
            break;
    }
    return 100;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrTitleHeader count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *header = [arrTitleHeader objectAtIndex:section];
    if ([header isEqualToString:@"DOCUMENTOS ANEXADOS"]) {
        return [arrDocuments count];
    }
    else if ([header isEqualToString:@"TESTEMUNHAS"]){
//        return 1;
    }
    else if([header isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
//        return 1;
    }
    else if([header isEqualToString:@"RELATO DO CASO"]){
        return [arrAudio count];
    }
    else if([header isEqualToString:@""]){
        
    }
    return 1;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
#pragma mark DetailDelegate 
#pragma  mark LARecusaReasonViewController
- (void)onchooseRecusae:(LARefusalObj*)objChoose andIndexPath:(NSIndexPath*)indexPath
{
    if (objChoose)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        LACustomerOpenObj *obj = self.objData;
        [MainService lawyerRefuse:obj.dataIdentifier withrefusal_id:objChoose.dataIdentifier refusalDescription:@"Reason from text field" withBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            LAActionResponseInfo *actionInfo = [[LAActionResponseInfo alloc]initWithDictionary:data];
            if (actionInfo.success) {
                LABasicInfoDetalTableViewCell *cell = (LABasicInfoDetalTableViewCell*)[tbData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                [cell saveTimer:YES];
                
                if (actionInfo.data.dataIdentifier > 0) {
                    [self doBackView:3];
                }
                else{
                    [self showMessage:actionInfo.message withPoistion:nil];
                }
            }
            else{
                [self showMessage:actionInfo.message withPoistion:nil];
            }
            
        }];
        
    }
    
}
- (void)onRecusarNovasDidTouch:(NSIndexPath*)indexPath
{
    LARecusaReasonViewController* reasonVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARecusaReasonViewController class])];
    reasonVC.indexPath = nil;
    reasonVC.delegate = self;
    reasonVC.lstRefusal = self.lstRefusal;
    [self presentViewController:reasonVC animated:YES completion:nil];
}
- (void)onAceiterDetailDidTouch:(NSIndexPath*)indexPath
{
    LACustomerOpenObj *obj = self.objData;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService lawSuitAccept:obj.dataIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        LAActionResponseInfo *actionInfo = [[LAActionResponseInfo alloc]initWithDictionary:data];
        if (actionInfo.success) {
            LABasicInfoDetalTableViewCell *cell = (LABasicInfoDetalTableViewCell*)[tbData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [cell saveTimer:YES];
            if (actionInfo.data.dataIdentifier > 0) {
                [self doBackView:1];
            }
            else{
                [self showMessage:@"Action failure" withPoistion:nil];
            }
        }
        else{
            [self showMessage:actionInfo.message withPoistion:nil];
        }
    }];
}
- (void)onRecusarDetailDidTouch:(NSIndexPath*)indexPath
{
    LARecusaReasonViewController* reasonVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARecusaReasonViewController class])];
    reasonVC.indexPath = nil;
    reasonVC.delegate = self;
    reasonVC.lstRefusal = self.lstRefusal;
    [self presentViewController:reasonVC animated:YES completion:nil];
    //        LACustomerOpenObj *obj = self.objData;
    //        LARefusalObj *reasonObj;
    //        [MainService lawyerRefuse:obj.happening.thesisId withrefusal_id:reasonObj.dataIdentifier refusalDescription:@"Reason from text field" withBlock:^(NSInteger errorCode, NSString *message, id data) {
    //        }];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            LABasicInfoDetalTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LABasicInfoDetalTableViewCell class]) forIndexPath:indexPath];
            cell.objData = self.objData;
            [cell setupData:self.objData];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
        case 1:
        {
            if ([[arrTitleHeader objectAtIndex:1]isEqualToString:@"RELATO DO CASO"]) {
                LARelatoDoCasoTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LARelatoDoCasoTableViewCell class]) forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                if (arrAudio)
                {
                    cell.objAudio = [arrAudio objectAtIndex:indexPath.row];
                    cell.Id = indexPath.row;
                    cell.btPausePlay.tag = indexPath.row;
                    cell.delegate = self;
                }
                return cell;
            }
            else if([[arrTitleHeader objectAtIndex:1]isEqualToString:@"DOCUMENTOS ANEXADOS"]){
                LADocumentNovasDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LADocumentNovasDetailCell class]) forIndexPath:indexPath];
                [cell.clDocuments registerNib:[UINib nibWithNibName:NSStringFromClass([LADocumentCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LADocumentCollectionCell class])];
                cell.clDocuments.delegate = self;
                cell.clDocuments.dataSource = self;
                cell.clDocuments.showsVerticalScrollIndicator = NO;
                cell.clDocuments.scrollEnabled = NO;
                [cell.clDocuments reloadData];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([[arrTitleHeader objectAtIndex:1]isEqualToString:@"TESTEMUNHAS"]){
                LAWitnesses* obj = [self.objData.happening.witnesses objectAtIndex:indexPath.row];
                LAWitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LAWitnessTableViewCell" forIndexPath:indexPath];
                cell.lbName.text = obj.name;
                return cell;
            }
            else if([[arrTitleHeader objectAtIndex:1]isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
                LAMoreInfoDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAMoreInfoDetailCell class]) forIndexPath:indexPath];
//                cell.delegate = self;
                cell.lbDescription.text = self.objData.happening.otherInformation;
//                cell.currentPath = indexPath;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            
            
        }
            break;
        case 2:
        {
            if([[arrTitleHeader objectAtIndex:2]isEqualToString:@"DOCUMENTOS ANEXADOS"]){
                LADocumentNovasDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LADocumentNovasDetailCell class]) forIndexPath:indexPath];
                [cell.clDocuments registerNib:[UINib nibWithNibName:NSStringFromClass([LADocumentCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([LADocumentCollectionCell class])];
                cell.clDocuments.delegate = self;
                cell.clDocuments.dataSource = self;
                cell.clDocuments.showsVerticalScrollIndicator = NO;
                cell.clDocuments.scrollEnabled = NO;
                [cell.clDocuments reloadData];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([[arrTitleHeader objectAtIndex:2]isEqualToString:@"TESTEMUNHAS"]){
                LAWitnesses* obj = [self.objData.happening.witnesses objectAtIndex:indexPath.row];
                LAWitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LAWitnessTableViewCell" forIndexPath:indexPath];
                cell.lbName.text = obj.name;
                return cell;

            }
            else if([[arrTitleHeader objectAtIndex:2]isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
                LAMoreInfoDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAMoreInfoDetailCell class]) forIndexPath:indexPath];
//                cell.delegate = self;
                cell.lbDescription.text = self.objData.happening.otherInformation;
//                cell.currentPath = indexPath;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
            break;
        case 3:{
            if([[arrTitleHeader objectAtIndex:3]isEqualToString:@"TESTEMUNHAS"]){
                LAWitnesses* obj = [self.objData.happening.witnesses objectAtIndex:indexPath.row];
                LAWitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LAWitnessTableViewCell" forIndexPath:indexPath];
                cell.lbName.text = obj.name;
                return cell;
            }
            else if([[arrTitleHeader objectAtIndex:3]isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
                LAMoreInfoDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAMoreInfoDetailCell class]) forIndexPath:indexPath];
//                cell.delegate = self;
                cell.lbDescription.text = self.objData.happening.otherInformation;
//                cell.currentPath = indexPath;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            
        }
        case 4:
        {
            if([[arrTitleHeader objectAtIndex:4]isEqualToString:@"INFORMAÇÕES ADICIONAIS"]){
                LAMoreInfoDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAMoreInfoDetailCell class]) forIndexPath:indexPath];
//                cell.delegate = self;
                cell.lbDescription.text = self.objData.happening.otherInformation;
//                cell.currentPath = indexPath;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
            break;
        default:
            break;
    }
    return nil;
}

#pragma mark PopupWitness
- (void)doClosePopup
{
    
}
#pragma mark TableView Delegate
//Header click
- (void)didSelectNovasDetalHeader
{
    LAPopupWitnessViewController* popupVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPopupWitnessViewController class])];
    NSArray* obj = self.objData.happening.witnesses;
    popupVC.arrData = obj;
    popupVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self.navigationController presentViewController:popupVC animated:YES completion:^{
        
    }];
}
#pragma mark UICollectionView Datasource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //You may want to create a divider to scale the size by the way..
    return CGSizeMake(CGRectGetWidth(collectionView.bounds)/2 - 10, 140);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrDocuments.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LADocumentCollectionCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([LADocumentCollectionCell class]) forIndexPath:indexPath];
    if (indexPath.row < arrDocuments.count)
    {
        LAMedias* obj = [arrDocuments objectAtIndex:indexPath.row];
        [cell.imgDocThumb sd_setImageWithURL:[NSURL URLWithString:obj.mediaWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        cell.imgDocName.text = [LAUtilitys getDefaultString:obj.media];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < arrDocuments.count)
    {
        
        LAMedias* obj = [arrDocuments objectAtIndex:indexPath.row];
        if ([obj.mimeType isEqualToString:@"video/quicktime"]) {
            [self playVideoWithURL:obj.mediaWithUrl];
        }
        else{
            LAPhotoShowViewController* photoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPhotoShowViewController class])];
            photoVC.delegate = self;
            photoVC.lbName.text = obj.media;
            [self.navigationController presentViewController:photoVC animated:YES completion:^{
                
            }];
            [photoVC.imgPhotoShow sd_setImageWithURL:[NSURL URLWithString:obj.mediaWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        }
    }
}
-(void)doBackView:(NSInteger)index{
    [self.tabBarController setSelectedIndex:1];
    UINavigationController *nav3 = (UINavigationController*)[self.tabBarController.viewControllers objectAtIndex:1];
    if ([nav3.viewControllers count]) {
        UIViewController *rootVC = [nav3.viewControllers objectAtIndex:0];
        if ([rootVC isKindOfClass:[LAAcoesViewController class]]) {
            LAAcoesViewController *acoeVC = (LAAcoesViewController*)rootVC;
            acoeVC.selectedInitIndex = index;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ChangeSelectedTabIndex" object:[NSNumber numberWithInteger:index]];
}

-(void)playVideoWithURL:(NSString*)strURL{
//    NSURL *url = [[NSURL alloc]initWithString:strURL];
    MPMoviePlayerViewController *controller = [[MPMoviePlayerViewController alloc]initWithContentURL:[NSURL URLWithString:strURL]];
//    [[LAAudioPlayerController sharedInstance] createSteammerWithUrl:url];
    [self presentMoviePlayerViewControllerAnimated:controller];
}
@end

//
//  LASelectedTheseViewController.h
//  JustapLawyer
//
//  Created by Mac on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LALawSuitCategory.h"
#import "LALawsuitObj.h"

@interface LASelectedTheseViewController : LABaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblData;
@property (strong, nonatomic)  NSMutableArray *selectedTheseData;
@property (strong, nonatomic)  NSMutableArray *allTheseData;
@property (weak, nonatomic) IBOutlet UILabel* lbHeaderTitle;
@end

//
//  LAPlanosSelectionComarcaViewController.h
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "AwesomeTextField.h"
#import "LACityObj.h"
#import "LAUFs.h"
#import "LADistrictObj.h"
#import "LAAssinatureInfo.h"

@interface LAPlanosSelectionComarcaViewController : LABaseViewController
{
    NSMutableArray* listCity;
    NSMutableArray* listEstado;
}
@property (weak, nonatomic) IBOutlet UILabel *lbPlanosBought;
@property (nonatomic) IBOutlet UILabel* lbHeader;
@property (nonatomic) IBOutlet UILabel* lbDescription;
@property (nonatomic) IBOutlet UIScrollView* scrFullData;
@property (nonatomic) IBOutlet UIButton* btConfirm;
@property (nonatomic) IBOutlet UIView* viewMiddle;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtEstado;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtCity;
@property (nonatomic) LADistrictObj *currentDistrict;
@property (nonatomic) NSMutableArray* listDistrictSelected;
@property (nonatomic) NSMutableArray* listOldDistrictSelected;

@property (nonatomic) NSString *currentState;
@property (nonatomic) NSMutableArray* arrRequiredField;
@property (nonatomic, strong) LAUFs *listUFS;
@property (weak, nonatomic) IBOutlet UIView *vTop;

@property (strong, nonatomic) LAAssinatureInfo *assinatureInfo;

- (IBAction)onConfirm:(id)sender;
- (void)reset;
-(void)getUFs;
@end

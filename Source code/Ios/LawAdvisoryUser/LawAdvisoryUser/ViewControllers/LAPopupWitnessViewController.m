//
//  LAPopupWitnessViewController.m
//  JustapLawyer
//
//  Created by MAC on 11/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPopupWitnessViewController.h"
#import "LAWitnessPopupCell.h"
@interface LAPopupWitnessViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView* tbData;
}
@end

@implementation LAPopupWitnessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [tbData registerNib:[UINib nibWithNibName:NSStringFromClass([LAClientTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAClientTableViewCell class])];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)doClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    return  333.0;
    return 44.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.arrData)
    {
        return self.arrData.count;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LAClientTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAClientTableViewCell class]) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    [cell setupData:[self.arrData objectAtIndex:indexPath.row]];
    return cell;
}
@end

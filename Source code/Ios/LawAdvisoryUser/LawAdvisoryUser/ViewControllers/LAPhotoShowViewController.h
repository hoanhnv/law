//
//  LAPhotoShowViewController.h
//  JustapLawyer
//
//  Created by MAC on 10/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "UIImageView+JMImageCache.h"
@interface LAPhotoShowViewController : LABaseViewController
@property (nonatomic) UIImage* imgShow;
@property (nonatomic) IBOutlet UIImageView* imgPhotoShow;
@property (nonatomic) IBOutlet UIScrollView* scrPhoto;
@property (nonatomic) IBOutlet UILabel* lbName;
@property (nonatomic) id delegate;
@end

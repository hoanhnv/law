//
//  LALoginViewController.h
//  LawAdvisoryUser
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "AwesomeTextField.h"
#import <GoogleSignIn/GoogleSignIn.h>

@interface LALoginViewController : LABaseViewController<GIDSignInDelegate,GIDSignInUIDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnTermos;
@property (nonatomic) IBOutlet AwesomeTextField* txtEmail;
@property (nonatomic) IBOutlet AwesomeTextField* txtPassword;
@property (nonatomic) IBOutlet UIButton* btLogin;
@property (nonatomic) IBOutlet UIButton* btFogotPass;
@property (nonatomic) IBOutlet UIButton* btRegister;
@property (nonatomic) IBOutlet UIButton* btDuvidas;
@property (nonatomic) IBOutlet UIButton* btFale;
@property (nonatomic) IBOutlet UIScrollView* scrFullData;
@property (nonatomic) UIImage* imgAttach;
- (IBAction)onForgotPassPressed:(id)sender;
- (IBAction)onFacebookPressed:(id)sender;
- (IBAction)onTwitterPressed:(id)sender;
- (IBAction)onGPlusPressed:(id)sender;
- (IBAction)onLoginPressed:(id)sender;
- (IBAction)onRegisterPressed:(id)sender;
- (IBAction)onDuvidasPressed:(id)sender;
- (IBAction)onFalePressed:(id)sender;
- (IBAction)doTermos:(id)sender;

@end

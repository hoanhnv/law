//
//  LANovasPoupViewController.h
//  JustapLawyer
//
//  Created by Mac on 2/9/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LACustomerOpenObj.h"
#import "LAListRefusal.h"

@protocol NovasPopupDelegate <NSObject>

- (void)onRefuse:(LAListRefusal*)refusal withIndex:(NSIndexPath*)indexPath;
- (void)onAccept:(LACustomerOpenObj*)obj;
@end

@interface LANovasPoupViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UILabel *lbPercentSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lbTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbUserName;
@property (weak, nonatomic) IBOutlet UILabel *lbLawyerName;
@property (weak, nonatomic) IBOutlet UILabel *lbValue;
@property (weak, nonatomic) IBOutlet UILabel *lbPercent;
@property (weak, nonatomic) IBOutlet UILabel *lbInitValue;

@property (nonatomic, strong) LACustomerOpenObj *objData;
@property (nonatomic) LAListRefusal *lstRefusal;
@property (nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic,assign) id<NovasPopupDelegate> delegate;

- (IBAction)doAccept:(id)sender;
- (IBAction)doRefuse:(id)sender;
- (IBAction)doClose:(id)sender;

@end

//
//  CountryViewController.m
//  GoInk
//
//  Created by Mac on 9/21/15.
//  Copyright (c) 2015 MJH. All rights reserved.
//

#import "LACountryViewController.h"
#import "UIFont+Law.h"

@interface LACountryViewController ()

@end

@implementation LACountryViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.popType = TYPE_CITY;
    self.arrCountry = [NSMutableArray new];
    self.arrSelected = [NSMutableArray new];
    self.isMultipleSelect = NO;
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.arrCountry == nil)
    {
        switch (self.popType) {
            case TYPE_CITY:
                [self getCityList];
                break;
            case TYPE_NEWCITY:
                [self getNewCityList];
                break;
            case TYPE_STATE:
                [self getStateList];
                break;
            case TYPE_NEWSTATE:
                [self getStateList];
                break;
            case TYPE_BANK:
                [self getBankList];
                break;
            default:
                break;
        }
    }
    else
    {
        [self.tblCity reloadData];
    }
    // Do any additional setup after loading the view.
    
}
- (void)getCityList
{
    
}
- (void)getNewCityList
{
    
}
- (void)getBankList
{
    
}
- (void)getStateList
{
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupRightButtonBigNavigation];
}
- (void)setupRightButtonBigNavigation
{
    if (self.btRightBar == nil)
    {
        self.btRightBar = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 80, 40)];
        [self.btRightBar addTarget:self action:@selector(pressRightBarItem:) forControlEvents:UIControlEventTouchUpInside];
        self.btRightBar.titleLabel.font = [UIFont robotoRegular:15];
        [self.btRightBar setTitle:@"FECHAR" forState:UIControlStateNormal];
        UIBarButtonItem* bt = [[UIBarButtonItem alloc] initWithCustomView:self.btRightBar];
        self.navigationItem.rightBarButtonItem = bt;
    }
}
-(void)viewDidAppear:(BOOL)animated{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma TableView Delegate
#pragma mark -- STEP 2&5&7 DATA AND ACTION
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.arrCountry count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellIdentifier"];
    }
    [cell.textLabel setTextColor:[UIColor blackColor]];
    switch (self.popType)
    {
        case TYPE_CITY:
        {
            if (indexPath.row < self.arrCountry.count)
            {
                LACityObj* obj = [self.arrCountry objectAtIndex:indexPath.row];
                cell.textLabel.text = obj.cityName;
                if (self.currentCity && [obj isEqual:self.currentCity])
                {
                    [cell.textLabel setTextColor:NAVIGATION_COLOR];
                }
            }
        }
            break;
        case TYPE_NEWCITY:
        {
            if (indexPath.row < self.arrCountry.count)
            {
                LADistrictObj* obj = [self.arrCountry objectAtIndex:indexPath.row];
                cell.textLabel.text = obj.name;
                if (self.isMultipleSelect)
                {
                    if ([self.arrSelected containsObject:obj])
                    {
                        [cell.textLabel setTextColor:NAVIGATION_COLOR];
                    }
                }
                else
                {
                    if (self.currentDistrict && [obj isEqual:self.currentDistrict])
                    {
                        [cell.textLabel setTextColor:NAVIGATION_COLOR];
                    }
                }
                
            }
        }
            break;
        case TYPE_STATE:
        {
            if (indexPath.row < self.arrCountry.count)
            {
                NSString *strState = [self.arrCountry objectAtIndex:indexPath.row];
                cell.textLabel.text = strState;
                if (self.currentState && [strState isEqual:self.currentState])
                {
                    [cell.textLabel setTextColor:NAVIGATION_COLOR];
                }
            }
        }
            break;
        case TYPE_NEWSTATE:
        {
            if (indexPath.row < self.arrCountry.count)
            {
                NSString *strState = [self.arrCountry objectAtIndex:indexPath.row];
                cell.textLabel.text = strState;
                if (self.currentState && [strState isEqual:self.currentState])
                {
                    [cell.textLabel setTextColor:NAVIGATION_COLOR];
                }
            }
            
        }
            break;
        case TYPE_BANK:
        {
            if (indexPath.row < self.arrCountry.count)
            {
                LABankObj *bankObj = [self.arrCountry objectAtIndex:indexPath.row];
                cell.textLabel.text = bankObj.name;
                if (self.currentBank && [bankObj isEqual:self.currentBank])
                {
                    [cell.textLabel setTextColor:NAVIGATION_COLOR];
                }
                
            }
        }
            break;
        default:
        {
            cell.textLabel.text = @"";
        }
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.isMultipleSelect)
    {
        [self checkMutilpleSelect:indexPath];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didSelectCountry" object:[self.arrCountry objectAtIndex:indexPath.row] userInfo:@{@"TYPEPOUP":[NSNumber numberWithInteger:self.popType]}];
        [self pressRightBarItem:nil];
    }
    
    
}
- (void)checkMutilpleSelect:(NSIndexPath*)indexPath
{
    if (indexPath.row < self.arrCountry.count)
    {
        switch (self.popType) {
            case TYPE_NEWCITY:
            {
                if (self.arrSelected == nil)
                {
                    self.arrSelected = [NSMutableArray new];
                }
                LADistrictObj* obj = [self.arrCountry objectAtIndex:indexPath.row];
                if (![self.arrSelected containsObject:obj])
                {
                    [self.arrSelected addObject:obj];
                }
                else
                {
                    [self.arrSelected removeObject:obj];
                }
                [self.tblCity reloadData];
            }
                break;
                
            default:
                break;
        }
    }
}
-(IBAction)pressRightBarItem:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didselectedList:)])
    {
        [self.delegate didselectedList:self.arrSelected];
    }
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
@end

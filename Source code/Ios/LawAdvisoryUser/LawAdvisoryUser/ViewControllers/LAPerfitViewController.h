//
//  LAPerfitViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "ProfileView.h"
#import "UIView+NibInitializer.h"
#import "HMSegmentedControl.h"
#import "LAAccountObj.h"

@interface LAPerfitViewController : LABaseViewController{
    ProfileView *profileView;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (nonatomic) HMSegmentedControl* segmentedControl;
@property (nonatomic) IBOutlet UIScrollView* scrFullDataProfile;

@property (nonatomic,strong) LALawyerInfo *objInfo;

@end

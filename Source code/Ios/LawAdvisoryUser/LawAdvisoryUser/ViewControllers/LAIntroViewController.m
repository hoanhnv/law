//
//  LAIntroViewController.m
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAIntroViewController.h"
#import "LAIntroView.h"
#import "LALoginViewController.h"
#import "AppDelegate.h"
#import "MainService.h"
#import "LAStatusAccountObj.h"
#import "LAStatusAccountViewController.h"


#define NUMBER_SLIDE 4
@interface LAIntroViewController ()
{
    NSInteger currenPageIndex;
}
@end

@implementation LAIntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *strAccessToken = [LADataCenter shareInstance].currentAccessToken;
    NSLog(@"%@",strAccessToken);
    if (![LAUtilitys NullOrEmpty:strAccessToken]) {
        NSString *strUUID = [LAUtilitys UUIDString];
        NSString*strDeviceToken = [[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"];
        ;
        if ([LAUtilitys NullOrEmpty:strDeviceToken]) {
            strDeviceToken = @"justap";
        }
        NSDictionary *dictDeviceData = @{@"device_token":strDeviceToken,
                               @"uuid":strUUID,
                               @"type":[NSNumber numberWithInteger:1],
                               @"model":[[UIDevice currentDevice]model],
                               @"platform":[[UIDevice currentDevice]systemName],
                               @"version":[[UIDevice currentDevice] systemVersion]};
            
        NSDictionary *deviceData = @{@"device_data": dictDeviceData};
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [MainService reValidateToken:deviceData withBlock:^(NSInteger errorCode, NSString *message, id data){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]]) {
                if ([data[@"success"] boolValue]) {
                    NSString *newToken = [[data objectForKey:@"data"] objectForKey:@"token"];
                    if (![LAUtilitys NullOrEmpty:newToken]){
                        [[LADataCenter shareInstance] saveToken:newToken error:nil];
                    }
                }
                else{
                    // TODO
                    if ([message isEqualToString:@"Request failed: unauthorized (401)"] || [message containsString:@"401"]) {
                        [[LADataCenter shareInstance]clearKeyChain];
                    }
                }
            }
            else {
                [[LADataCenter shareInstance]clearKeyChain];
            }
        }];
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)initUI
{
    self.pgControl.iNumberPage = NUMBER_SLIDE;
    [self.pgControl setUpImage];
}
- (void)initData
{
    currenPageIndex = 0;
    arrData = [NSMutableArray arrayWithCapacity:NUMBER_SLIDE];
    NSDictionary* dict1 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro1",KEY_IMAGE,@"1- Encontre o plano ideal",KEY_TITLE,@"Você poderá se inscrever na quantidade de teses que desejar e atuar nas mais variadas causas do direito ao toque do seu celular. Comece agora a aparecer para seus futuros clientes.",KEY_DESCRIPTION, nil];
    NSDictionary* dict2 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro2",KEY_IMAGE,@"2- Precifique suas teses",KEY_TITLE,@"Saber precificar suas teses ajudará você a ser competitivo dentro do aplicativo. Proporcione aos seus futuros clientes preços convidativos e construa uma relação de confiança para trabalhos constantes.",KEY_DESCRIPTION, nil];
    NSDictionary* dict3 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro3",KEY_IMAGE,@"3- Seja sempre bem avaliado",KEY_TITLE,@"Tudo que você faz no aplicativo proporciona possibilidade de avaliação por seus clientes. O tempo de resposta, sua prontidão, iniciativa e trabalho organizado geram confiança e segurança para os usuários do JusTap. Isso garante a você melhor posicionamento na lista de busca de profissionais.",KEY_DESCRIPTION, nil];
    NSDictionary* dict4 = [NSDictionary dictionaryWithObjectsAndKeys:@"imgIntro4",KEY_IMAGE,@"4- Promova a justiça",KEY_TITLE,@"O JusTap é seu ajudante jurídico de bolso, com ele você poderá indicar para seus amigos e familiares e construir uma rede de negócios que vai além dos seus limites físicos. Indique!",KEY_DESCRIPTION, nil];
    [arrData addObject:dict1];
    [arrData addObject:dict2];
    [arrData addObject:dict3];
    [arrData addObject:dict4];
    CGSize sizeScroll = self.scrIntroPath.contentSize;
    sizeScroll.width = CGRectGetWidth(self.scrIntroPath.frame) * NUMBER_SLIDE;
    [self.scrIntroPath setContentSize:sizeScroll];
    CGRect rectScroll = self.scrIntroPath.frame;
    for (int  i = 0; i < arrData.count; i ++)
    {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"LAIntroView"
                                                          owner:self
                                                        options:nil];
        LAIntroView* viewCustom = (LAIntroView*)[nibViews objectAtIndex:0];
        CGRect frameView = CGRectMake(CGRectGetWidth(rectScroll)*i, 0, CGRectGetWidth(rectScroll), CGRectGetHeight(rectScroll));
        [self.scrIntroPath addSubview:viewCustom];
        [viewCustom setFrame:frameView];
        NSDictionary* dictData = [arrData objectAtIndex:i];
        [viewCustom setupDataWith:dictData];
    }
    CGSize size = self.scrFull.frame.size;
    size.height = CGRectGetMaxY(self.btEntrant.frame) + 10;
    [self.scrFull setContentSize:size];
}
#pragma mark UIScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrIntroPath.frame.size.width;
    float currentPageHere = self.scrIntroPath.contentOffset.x / pageWidth;
    if (0.0f != fmodf(currentPageHere, 1.0f))
    {
        currentPageHere = 0;
    }
    currenPageIndex = currentPageHere;
    [self gotoCurrentPage:currentPageHere];
}
- (void)gotoCurrentPage:(NSInteger)page
{
    [self.pgControl setCurrentPage:page];
    
}
- (void)whenFinishIntro
{
    [self onEntrantPressed:nil];
}
- (IBAction)onEntrantPressed:(id)sender
{
    
    //    [[AppDelegate sharedDelegate] goHomeViewController];
    
    NSString *strAccessToken = [LADataCenter shareInstance].currentAccessToken;
    NSLog(@"%@",strAccessToken);
       // Check status
    if (![LAUtilitys NullOrEmpty:strAccessToken]) {
        // User sigout
        [self checkStatus:strAccessToken];
    }
    else{
        // User Logged
        [self showLoginView];
    }
}

- (void)checkStatus:(NSString*)token
{
    //    [[AppDelegate sharedDelegate] goHomeViewController];
    //    return;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService checkStatus:token withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[LAStatusAccountObj class]])
        {
            LAStatusAccountObj *statusObj = (LAStatusAccountObj*)data;
            switch (statusObj.type) {
                case IN_ACTIVE_ACCOUNT:
                case WAITING_APPROVAL_ACCOUNT:
                {
                    [self showWaitingScreen:statusObj];
                }
                    break;
                case ACTIVE_ACCOUNT:{
                    [[AppDelegate sharedDelegate] goHomeViewController];
                }
                    break;
                default:
                    // TODO
                    [self showLoginView];
                    break;
            }
        }
        else
        {
            [self showLoginView];
            //            if (errorCode ==  AUTHORIZATION_FAILURE_CODE) {
            //                [[LADataCenter shareInstance]clearKeyChain];
            //                [self showLoginView];
            //            }
            //            else{
            //                [self showLoginView];
            //                [self showMessage:message withPoistion:nil];
            //            }
        }
    }];
}

-(void)showLoginView{
    LALoginViewController* loginVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LALoginViewController class])];
    [self.navigationController pushViewController:loginVC animated:YES];
}

-(void)showWaitingScreen:(LAStatusAccountObj*)status{
    LAStatusAccountViewController *waitingScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"LAStatusAccountViewController"];
    waitingScreen.objStatusAccount = status;
    [self.navigationController pushViewController:waitingScreen animated:YES];
}

@end

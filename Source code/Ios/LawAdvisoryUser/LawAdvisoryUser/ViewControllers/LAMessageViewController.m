//
//  LAMessageViewController.m
//  JustapLawyer
//
//  Created by Tùng Nguyễn on 2/18/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import "LAMessageViewController.h"
#import "MainService.h"
#import "LAActionResponseInfo.h"

@interface LAMessageViewController ()

@end

@implementation LAMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isShouldBackButton = YES;
    self.title = @"Mensagem";
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]
                            initWithImage:[UIImage imageNamed:@"ic_close_x"] style:UIBarButtonItemStylePlain target:self action:@selector(Close:)];
    self.navigationItem.rightBarButtonItem = btn;
    
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doSendMessage:(id)sender {
    if(![LAUtilitys isEmptyOrNull:_txtMessages.text]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
        NSDictionary *param = @{@"lawsuit_lawyer_id":@(self.Id),
                                @"message":_txtMessages.text};
        [MainService postMessage:param withBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            LAActionResponseInfo *respose = [[LAActionResponseInfo alloc]initWithDictionary:data];
//            [self addMessage:Message];
            if (respose.success) {
                [self showMessage:@"Mensagem enviada com sucesso" withPoistion:nil];
                [self.navigationController popViewControllerAnimated:true];
            }
            
            else{
                [self showMessage:message withPoistion:nil];
            }
            if(self.delegate){
                [_delegate addMessage:self.txtMessages.text];
            }
        }];
        
    }
    else {
        [self showMessage:@"Por favor, digite sua mensagem." withPoistion:nil];
    }
    
//    if (self.delegate&& [self.delegate respondsToSelector:@selector(getMessage:)]) {
//        [self.delegate getMessage:_txtMessages.text];
//        [self.navigationController popViewControllerAnimated:true];
//    }
}
-(IBAction)Close:(id)sender{
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)doClose:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end

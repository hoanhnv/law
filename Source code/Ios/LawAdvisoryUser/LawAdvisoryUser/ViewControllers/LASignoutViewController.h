//
//  LASignoutViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAAccountService.h"

@interface LASignoutViewController : LABaseViewController
{
    
}
- (IBAction)onSigout:(id)sender;
@end

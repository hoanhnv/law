//
//  FaleResponse.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAContactSubjects.h"

@interface LAPopupMessageChangePass : UIViewController{
    
}
@property (nonatomic, strong) LAContactSubjects *responseSubject;
@property ( nonatomic) IBOutlet UILabel *lbContent;
@property (nonatomic) BOOL isSuccess;
@property (nonatomic) id delegate;
- (IBAction)doClose:(id)sender;

@end
@protocol MessagePopupDelegate <NSObject>

- (void)doClosePopup:(BOOL)isSusccess;

@end
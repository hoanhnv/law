//
//  LAPricePlanosViewController.m
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPricePlanosViewController.h"
#import "LATesesHeaderView.h"
#import "LATesesMainTableViewCell.h"
#import "UIView+NibInitializer.h"
#import "LATesesPriceHeaderView.h"
#import "ServiceTesesFooterView.h"
#import "LACancelamentViewController.h"
#import "HeaderSeviceTeses.h"
#import "MainService.h"
#import "ServicePriceFooterView.h"
#import "LAPriceConfirmViewController.h"
#import "LAPriceConfirmViewController.h"
#import "LASearchEngine.h"
#import "AppDelegate.h"

@interface LAPricePlanosViewController ()<ServicePriceFooterViewDelegate,TesesPriceHeaderViewDelegate,UITextFieldDelegate>
{
    BOOL isSearch;
    NSMutableDictionary *mSelected;
    NSInteger iNumberRequest;
    NSMutableArray* arrCategoriesSearch;
    LATesesPriceHeaderView *headerTableView;
}
@end

@implementation LAPricePlanosViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = NO;
        self.isShouldLeftButton = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TPKeyboardAvoiding_keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.selectedTheseData = [NSMutableArray arrayWithArray:[AppDelegate sharedDelegate].selectedTheseData];
    if (self.selectedTheseData.count)
    {
        for (LALawSuitCategory* obj in self.selectedTheseData) {
            obj.isSelected = NO;
            if (obj.arrData.count)
            {
                for (LALawsuitObj* objSuit in obj.arrData)
                {
                    objSuit.isSelected = NO;
                }
            }
        }
    }
    self.currentSelected = [NSMutableArray new];
    CGRect frame = self.tblContent.frame;
    frame.size.height = CGRectGetHeight(self.view.frame)- 114;
    [self.tblContent setFrame:frame];
    mSelected = [NSMutableDictionary new];
    [mSelected setObject:@(0) forKey:@"0"];
    
    [self.tblContent registerNib:[UINib nibWithNibName:NSStringFromClass([LATesesMainTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LATesesMainTableViewCell class])];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    isSearch = NO;
    headerTableView = [[LATesesPriceHeaderView alloc]initWithNibNamed:NSStringFromClass([LATesesPriceHeaderView class])];
    headerTableView.txtTitleSelection.text = [NSString stringWithFormat:kTitleHeader,[self getCountSelectedThese],[AppDelegate sharedDelegate].countThese];
    headerTableView.delegate = self;
    [headerTableView.txtName setDelegate:self];
    [headerTableView.txtPriceSuccess setDelegate:self];
    [headerTableView.txtInitiaPrice setDelegate:self];
    ServicePriceFooterView *footerView = [[ServicePriceFooterView alloc]initWithNibNamed:NSStringFromClass([ServicePriceFooterView class])];
    footerView.delegate = self;
    self.tblContent.tableHeaderView = headerTableView;
    self.tblContent.tableFooterView = footerView;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - HEADER TABLEVIEW DELEGATE
- (void)onConfirmPricePressed:(LATesesPriceHeaderView*)header
{
    
    NSString* txtInitPrice = [header.txtInitiaPrice.text stringByReplacingOccurrencesOfString:@"." withString:@","];
    if ([txtInitPrice doubleValue]<0) {
        [self showMessage:@"Honorários iniciais incorreta!" withPoistion:nil];
    }
    else {
        NSString* txtSuccessPrice = [header.txtPriceSuccess.text stringByReplacingOccurrencesOfString:@"." withString:@","];
        if (![self.selectedTheseData count]) {
            self.selectedTheseData = [NSMutableArray arrayWithArray:[AppDelegate sharedDelegate].selectedTheseData];
        }
        if (self.selectedTheseData.count)
        {
            for (LALawSuitCategory* obj in self.selectedTheseData) {
                obj.isSelected = YES;
                if (obj.arrData.count)
                {
                    for (LALawsuitObj* objSuit in obj.arrData)
                    {
                        objSuit.isSelected = YES;
                        objSuit.initialPrice = txtInitPrice;
                        objSuit.percentagePrice = txtSuccessPrice;
                    //                    [arrSelected addObject:objSuit];
                    }
                }
            }
        }
    //    if (arrSelected)
    //    {
        LAPriceConfirmViewController* priceConfirmVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPriceConfirmViewController class])];
        priceConfirmVC.isAll = YES;
        priceConfirmVC.inititalPriceAll = txtInitPrice;
        priceConfirmVC.percentageSuccess = txtSuccessPrice;
        priceConfirmVC.selectedTheseData = self.selectedTheseData;
        [self.navigationController pushViewController:priceConfirmVC animated:YES];
    }
    //    }
}
#pragma mark - FOOTER TABLEVIEW DELEGATE
- (BOOL)checkValid:(NSString**)message
{
    BOOL bValid = YES;
    if ([self.selectedTheseData count])
    {
        for (LALawSuitCategory* objCate in self.selectedTheseData) {
            for (LALawsuitObj *objItem in objCate.arrData) {
                if ([LAUtilitys isEmptyOrNull:objItem.initialPrice] || [LAUtilitys isEmptyOrNull:objItem.percentagePrice])
                {
                    *message = [NSString stringWithFormat:@"%@ inválido input",objItem.legalName];
                    bValid = NO;
                    break;
                }
            }
            if (!bValid) {
                break;
            }
        }
    }
    else
    {
        *message = @"Inválido selected";
        bValid = NO;
    }
    return bValid;
}
- (void)onPrice
{
    NSString *strMessage = nil;
    if ([self checkValid:&strMessage])
    {
        LAPriceConfirmViewController* priceConfirmVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPriceConfirmViewController class])];
        priceConfirmVC.selectedTheseData = self.selectedTheseData;
        priceConfirmVC.isAll = NO;
        [self.navigationController pushViewController:priceConfirmVC animated:YES];
    }
    else{
        [self showMessage:strMessage withPoistion:nil];
    }
}
- (void)onRemoveSelection
{
    //    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REMOVEALL_SELECTED object:nil];
    NSMutableArray *temp = [NSMutableArray array];
    for (LALawSuitCategory*item in self.selectedTheseData) {
        if (!item.isSelected) {
            [temp addObject:item];
        }
        else{
            LALawSuitCategory*newCate = [item copy];
            [newCate.arrData removeAllObjects];
            newCate.arrData = [NSMutableArray array];
            for (LALawsuitObj *obj in item.arrData) {
                if (!obj.isSelected) {
                    [newCate.arrData addObject:obj];
                }
            }
            if ([newCate.arrData count]) {
                [temp addObject:newCate];
            }
        }
    }
    if ([temp count]) {
        self.selectedTheseData = temp;
    }
    headerTableView.txtTitleSelection.text = [NSString stringWithFormat:kTitleHeader,[self getCountSelectedThese],[AppDelegate sharedDelegate].countThese];
    
    [self.tblContent reloadData];
}
#pragma mark - TABLEVIEW DATASOUCE
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSMutableArray* arrCategoriesHeader = self.selectedTheseData;
    if (isSearch)
    {
        arrCategoriesHeader = arrCategoriesSearch;
    }
    return [arrCategoriesHeader count];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]]integerValue];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LATesesMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LATesesMainTableViewCell class])];
    cell.isPriceCell = YES;
    cell.delegate = self;
    NSMutableArray* arrCategoriesHeader = self.selectedTheseData;
    if (isSearch)
    {
        arrCategoriesHeader = arrCategoriesSearch;
    }
    if (indexPath.section < arrCategoriesHeader.count)
    {
        LALawSuitCategory *objData = [arrCategoriesHeader objectAtIndex:indexPath.section];
        [cell setupCell:objData];
    }
    return  cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSeviceTeses *headerView = [[HeaderSeviceTeses alloc]initWithNibNamed:NSStringFromClass([HeaderSeviceTeses class])];
    
    headerView.frame = CGRectMake(0, 0,tableView.frame.size.width ,headerView.frame.size.height );
    NSMutableArray* arrCategoriesHeader = self.selectedTheseData;
    if (isSearch)
    {
        arrCategoriesHeader = arrCategoriesSearch;
    }
    if (section < arrCategoriesHeader.count)
    {
        LALawSuitCategory *objData = [arrCategoriesHeader objectAtIndex:section];
        [headerView setupView:objData];
    }
    
    for (NSString * key in mSelected.allKeys) {
        if (section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]] integerValue] == 1;
            [headerView setupColor:isShow];
        }
    }
    [headerView setShowBlock:^(BOOL isShow){
        if (isShow) {
            [mSelected setObject:@(1) forKey:[NSString stringWithFormat:@"%ld",section]];
        } else{
            [mSelected setObject:@(0) forKey:[NSString stringWithFormat:@"%ld",section]];
        }
        [tableView reloadData];
    }];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    for (NSString * key in mSelected.allKeys) {
        if (indexPath.section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",indexPath.section]] integerValue] == 1;
            if (isShow)
            {
                NSMutableArray* arrCategoriesHeader = self.selectedTheseData;
                if (isSearch)
                {
                    arrCategoriesHeader = arrCategoriesSearch;
                }
                if (indexPath.section < arrCategoriesHeader.count)
                {
                    LALawSuitCategory *objData = [arrCategoriesHeader objectAtIndex:indexPath.section];
                    return [LATesesMainTableViewCell getHeightPrice:objData.arrData];
                }
            }
        }
    }
    return 0.01f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55.0f;
}

-(void)doSeeMore:(LALawsuitObj*)suitObj{
    LACancelamentViewController* cancelamengoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LACancelamentViewController class])];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    cancelamengoVC.objCategory = suitObj;
    [self.navigationController pushViewController:cancelamengoVC animated:YES];
}
#pragma mark - UITEXTFIELD DELEGATE
- (void)TPKeyboardAvoiding_keyboardWillHide:(NSNotification*)notification
{
    //    TPKeyboardAvoidingState *state = self.keyboardAvoidingState;
    //    if ( state.ignoringNotifications ) {
    //        return;
    //    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 123 ) {
        for (LALawSuitCategory* obj in self.selectedTheseData) {
            if (obj.arrData.count)
            {
                for (LALawsuitObj* objSuit in obj.arrData)
                {
                    objSuit.initialPrice = headerTableView.txtInitiaPrice.text;
                }
            }
        }
        [self.tblContent reloadData];
    }
    else if(textField.tag == 124){
        for (LALawSuitCategory* obj in self.selectedTheseData) {
            if (obj.arrData.count)
            {
                for (LALawsuitObj* objSuit in obj.arrData)
                {
                    objSuit.percentagePrice = headerTableView.txtPriceSuccess.text;
                }
            }
        }
        [self.tblContent reloadData];
    }
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    isSearch = NO;
    [self.tblContent reloadData];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    isSearch = YES;
    [textField resignFirstResponder];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LASearchEngine sharedInstance] filterNameWithKeyword:textField.text fromSourceData:self.selectedTheseData wComPletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[NSArray class]])
        {
            if (arrCategoriesSearch)
            {
                [arrCategoriesSearch removeAllObjects];
            }
            arrCategoriesSearch = [NSMutableArray new];
            for (LALawsuitObj* obj  in data)
            {
                if ([obj isKindOfClass:[LALawsuitObj class]])
                {
                    if ([arrCategoriesSearch containsObject:obj.category])
                    {
                        [obj.category.arrData addObject:obj];
                    }
                    else
                    {
                        obj.category.arrData = [NSMutableArray new];
                        [obj.category.arrData addObject:obj];
                        [arrCategoriesSearch addObject:obj.category];
                    }
                    NSLog(@"%@",((LALawsuitObj*)obj).description);
                }
                
            }
            if (((NSArray*)data).count == 0)
            {
                [self showMessage:@"Nenhuma tese foi encontrada" withPoistion:nil];
            }
        }
        else
        {
            [self showMessage:@"Nenhuma tese foi encontrada" withPoistion:nil];
        }
        [self.tblContent reloadData];
    }];
    return YES;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)changeSelectedData
{
    
}

-(void)doAddAllItem :(LALawSuitCategory*)cateObj{
    //    if (![self.selectedTheseData containsObject:cateObj]) {
    //        [self.selectedTheseData addObject:cateObj];
    //    }
    //    else{
    //        // TODO
    //    }
    [self changeSelectedData];
}
-(void)doRemoveAllNewItem :(LALawSuitCategory*)cateObj{
    //    if ([self.selectedTheseData containsObject:cateObj]) {
    //        [self.selectedTheseData removeObject:cateObj];
    //    }
    //    else{
    //        // TODO
    //    }
    [self changeSelectedData];
}

-(void)doAddNewItem:(LALawsuitObj *)suitObj andCategories:(LALawSuitCategory*)cateObj{
    BOOL bExistCate = NO;
    //    if ([self.selectedTheseData count]) {
    //        for (LALawSuitCategory *item in self.currentSelected) {
    //            if ([item.name isEqualToString:cateObj.name]) {
    //                bExistCate = YES;
    //                if (![item.arrData containsObject:suitObj]) {
    //                    [item.arrData addObject:suitObj];
    //                }
    //                else{
    //                    //TO DO
    //                }
    //            }
    //            else{
    //                // TODO
    //            }
    //        }
    //        if (!bExistCate) {
    //            LALawSuitCategory *cateNew = [cateObj copy];
    //            [cateNew.arrData removeAllObjects];
    //            cateNew.arrData = [NSMutableArray array];
    //            [cateNew.arrData addObject:suitObj];
    //            [self.currentSelected addObject:cateNew];
    //        }
    //    }
    //    else{
    //        self.currentSelected = [NSMutableArray array];
    //        LALawSuitCategory *cateNew = [cateObj copy];
    //        [cateNew.arrData removeAllObjects];
    //        cateNew.arrData = [NSMutableArray array];
    //        [cateNew.arrData addObject:suitObj];
    //        [self.currentSelected addObject:cateNew];
    //    }
    
    [self changeSelectedData];
}

-(void)doRemoveItem:(LALawsuitObj *)suitObj andCategories:(LALawSuitCategory*)cateObj{
    // Remove subItem (suitObj)
    //    for (LALawSuitCategory *item in self.currentSelected) {
    //        if ([item.name isEqualToString:cateObj.name]) {
    //            if ([item.arrData containsObject:suitObj]) {
    //                [item.arrData removeObject:suitObj];
    //            }
    //            else{
    //                // Not Found
    //                // TODO
    //            }
    //        }
    //    }
    //    
    //    // Remove category if theses havent any suitObj (suitObj)
    //    NSMutableArray *arrTemp = [NSMutableArray array];
    //    for (LALawSuitCategory *item in self.currentSelected) {
    //        if ([item.arrData count]) {
    //            [arrTemp addObject:item];
    //        }
    //    }
    //    self.currentSelected = arrTemp;
    [self changeSelectedData];
}


-(NSInteger)getCountSelectedThese{
    NSInteger count = 0;
    for (LALawSuitCategory *cate in self.selectedTheseData) {
        count += [cate.arrData count];
    }
    return count;
}


@end

//
//  LAPlanAddMoreComacarViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPlanObj.h"
#import "LASignatureObj.h"

@interface LAPlanAddMoreComacarViewController : LABaseViewController

@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic) NSMutableArray* arrSignature;

@property (weak, nonatomic) IBOutlet UILabel *lbComacar;
@property (weak, nonatomic) IBOutlet UILabel *lbTotal;

@end

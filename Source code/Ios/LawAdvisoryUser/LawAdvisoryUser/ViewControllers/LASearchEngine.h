//
//  LASearchEngine.h
//  JustapLawyer
//
//  Created by MAC on 10/10/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainService.h"
#import "LASearchLocalObj.h"
@interface LASearchEngine : NSObject
@property (nonatomic) NSMutableArray* arrAllData;
@property (nonatomic) NSMutableArray* arrResult;
@property (nonatomic) NSMutableArray* arrSearchResult;
@property (nonatomic) BOOL isGettingData;
@property (nonatomic) BOOL isSearching;
@property (nonatomic) NSString* currentKeyword;
@property (nonatomic) CompletionBlock searchBlock;
- (void)loadData;
- (void)filterWithKeyword:(NSString*)keyword wComPletionBlock:(CompletionBlock)block;
- (void)filterNameWithKeyword:(NSString*)keyword fromSourceData:(NSMutableArray*)arrData wComPletionBlock:(CompletionBlock)block;
+ (instancetype)sharedInstance;
@end

//
//  ServiceTesesViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAServiceTesesViewController.h"
#import "MainService.h"
#import "LALawSuitCategory.h"
#import "ServiceTesesFooterView.h"
#import "ServiceTesescell.h"
#import "HeaderSeviceTeses.h"
#import "LATesesHeaderView.h"
#import "LATesesMainTableViewCell.h"
#import "UIView+NibInitializer.h"
#import "LALawSuitCategory.h"
#import "LASearchEngine.h"
#import "LALawsuitObj.h"
#import "LAListThese.h"
#import "LACancelamentViewController.h"
#import "AppDelegate.h"
#import "IQKeyboardManager.h"

@interface LAServiceTesesViewController ()<LATesesHeaderViewDelegate>{
    NSMutableArray *arrCategories;
    NSMutableDictionary *mSelected;
    NSInteger iNumberRequest;
    NSMutableArray* arrCategoriesSearch;
    BOOL isSearch;
    LATesesHeaderView *headerTableView;
}
@property (nonatomic,assign) NSInteger numberThese;
@end

@implementation LAServiceTesesViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
        isSearch = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ShowMessageOutRangeThese:) name:@"ShowMessageOutRangeThese" object:nil];
    arrCategories = [NSMutableArray array];
    self.selectedTheseData = [NSMutableArray array];
    if ([self.buyTheseData count]) {
        self.selectedTheseData = self.buyTheseData;
    }
    
    self.allTheseData = [NSMutableArray array];
    mSelected = [NSMutableDictionary new];
    [mSelected setObject:@(0) forKey:@"0"];    
    [self.tblContent registerNib:[UINib nibWithNibName:NSStringFromClass([LATesesMainTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LATesesMainTableViewCell class])];
    ServiceTesesFooterView *footerView = [[ServiceTesesFooterView alloc]initWithNibNamed:NSStringFromClass([ServiceTesesFooterView class])];
    [footerView.btnAvancar addTarget:self action:@selector(doNextView:) forControlEvents:UIControlEventTouchUpInside];
    self.tblContent.tableFooterView = footerView;
    
    headerTableView = [[LATesesHeaderView alloc]initWithNibNamed:NSStringFromClass([LATesesHeaderView class])];
    [headerTableView setDelegate:self];
    self.tblContent.tableHeaderView = headerTableView;
    [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText:@"Done"];
    
    [self getCategoryList];
    [self loadTotalTheses];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Teses";
    
    headerTableView.lbTitleTeses.text = [NSString stringWithFormat:kTitleHeader,[self
                                                                                 getCountSelectedThese],[AppDelegate sharedDelegate].countThese];
    CGRect frame = self.tblContent.frame;
    if (frame.size.height == [UIScreen mainScreen].bounds.size.height) {
        frame.size.height = [UIScreen mainScreen].bounds.size.height - 114;
    }
    else if(frame.size.height < [UIScreen mainScreen].bounds.size.height - 114){
        frame.size.height += 64;
    }
    [self.tblContent setFrame:frame];
}
-(void)viewDidDisappear:(BOOL)animated{
    arrCategories = nil;
}
- (void)initUI
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITextFieldDelegate

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    isSearch = NO;
    [self.tblContent reloadData];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    isSearch = YES;
    [textField resignFirstResponder];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LASearchEngine sharedInstance] filterNameWithKeyword:textField.text fromSourceData:arrCategories wComPletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[NSArray class]])
        {
            if (arrCategoriesSearch)
            {
                [arrCategoriesSearch removeAllObjects];
            }
            arrCategoriesSearch = [NSMutableArray new];
            for (LALawsuitObj* obj  in data)
            {
                if ([obj isKindOfClass:[LALawsuitObj class]])
                {
                    if ([arrCategoriesSearch containsObject:obj.category])
                    {
                        [obj.category.arrData addObject:obj];
                    }
                    else
                    {
                        obj.category.arrData = [NSMutableArray new];
                        [obj.category.arrData addObject:obj];
                        [arrCategoriesSearch addObject:obj.category];
                    }
                    NSLog(@"%@",((LALawsuitObj*)obj).description);
                }
                
            }
            if (((NSArray*)data).count == 0)
            {
                [self showMessage:@"Nenhuma tese foi encontrada" withPoistion:nil];
            }
        }
        else
        {
            [self showMessage:@"Nenhuma tese foi encontrada" withPoistion:nil];
        }
        [self.tblContent reloadData];
    }];
    return YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSMutableArray* arrCategoriesGeneral = arrCategories;
    if (isSearch)
    {
        arrCategoriesGeneral = arrCategoriesSearch;
    }
    return [arrCategoriesGeneral count];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]]integerValue];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LATesesMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LATesesMainTableViewCell class])];
    cell.delegate = self;
    cell.lanObj = self.lanObj;
    NSMutableArray* arrCategoriesGeneral = arrCategories;
    cell.isSearch = isSearch;
    cell.numberCanSelected = self.numberThese;
    if (isSearch)
    {
        arrCategoriesGeneral = arrCategoriesSearch;
    }
    if (indexPath.section < arrCategoriesGeneral.count)
    {
        LALawSuitCategory *objData = [arrCategoriesGeneral objectAtIndex:indexPath.section];
        [cell setupCell:objData];
    }
    return  cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSeviceTeses *headerView = [[HeaderSeviceTeses alloc]initWithNibNamed:NSStringFromClass([HeaderSeviceTeses class])];
    
    headerView.frame = CGRectMake(0, 0,tableView.frame.size.width ,headerView.frame.size.height );
    NSMutableArray* arrCategoriesGeneral = arrCategories;
    if (isSearch)
    {
        arrCategoriesGeneral = arrCategoriesSearch;
    }
    if (section < arrCategoriesGeneral.count)
    {
        LALawSuitCategory *objData = [arrCategoriesGeneral objectAtIndex:section];
        [headerView setupView:objData];
    }
    for (NSString * key in mSelected.allKeys) {
        if (section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]] integerValue] == 1;
            [headerView setupColor:isShow];
        }
    }
    [headerView setShowBlock:^(BOOL isShow){
        if (isShow) {
            [mSelected setObject:@(1) forKey:[NSString stringWithFormat:@"%ld",section]];
        } else{
            [mSelected setObject:@(0) forKey:[NSString stringWithFormat:@"%ld",section]];
        }
        [tableView reloadData];
    }];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    for (NSString * key in mSelected.allKeys) {
        if (indexPath.section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%d",indexPath.section]] integerValue] == 1;
            if (isShow)
            {
                NSMutableArray* arrCategoriesGeneral = arrCategories;
                if (isSearch)
                {
                    arrCategoriesGeneral = arrCategoriesSearch;
                }
                if (indexPath.section < arrCategoriesGeneral.count)
                {
                    LALawSuitCategory *objData = [arrCategoriesGeneral objectAtIndex:indexPath.section];
                    return [LATesesMainTableViewCell getHeight:objData.arrData];
                }
            }
        }
    }
    return 0.01f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55.0f;
}
- (void)getCategoryList
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawsuitCategory:^(NSInteger errorCode, NSString *message, id data) {
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            if (success)
            {
                id responseData = [data objectForKey:KEY_RESPONSE_DATA];
                if (responseData && [responseData isKindOfClass:[NSArray class]])
                {
                    for (NSDictionary* dict in responseData)
                    {
                        LALawSuitCategory* obj = [[LALawSuitCategory alloc] initWithDictionary:dict];
                        if (obj)
                        {
                            [arrCategories addObject:obj];
                        }
                    }
                }
            }
        }
        [self getDataWithCategory];
        [self.tblContent reloadData];
    }];
}
- (void)getDataWithCategory
{
    iNumberRequest = 0;
    if (arrCategories.count)
    {
        for (int i = 0; i < arrCategories.count; i ++)
        {
            LALawSuitCategory* obj = [arrCategories objectAtIndex:i];
            obj.arrData = [NSMutableArray new];
            [MainService fetchLawsuitTheseByCategory:obj.categoryIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
                iNumberRequest++;
                LAListThese *listThese = [[LAListThese alloc]initWithDictionary:data];
                obj.arrData = [NSMutableArray arrayWithArray:listThese.data];
                [self updateTheseSelectedIfNeed:obj];
                if (iNumberRequest == arrCategories.count)
                {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    if (![self.selectedTheseData count]&& ![self.buyTheseData count]) {
                        [[AppDelegate sharedDelegate].selectedTheseData removeAllObjects];
                    }
                    else{
                        [self changeSelectedData];
                    }
                    
                    [self.tblContent reloadData];
                    CGRect frame = self.tblContent.frame;
                    if (frame.size.height == [UIScreen mainScreen].bounds.size.height) {
                        frame.size.height = [UIScreen mainScreen].bounds.size.height - 114;
                    }
                    else if(frame.size.height < [UIScreen mainScreen].bounds.size.height - 114){
                        frame.size.height += 64;
                    }
                    [self.tblContent setFrame:frame];
                }
            }];
        }
    }
}
-(void)doNextView:(id)sender{
    if ([self getCountSelectedThese] > 0) {
//        self.segmentedControl.selectedSegmentIndex = 1;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
        LASelectedTheseViewController *selectedTestTheses = [storyboard instantiateViewControllerWithIdentifier:@"LASelectedTheseViewController"];
        selectedTestTheses.selectedTheseData = self.selectedTheseData;
        selectedTestTheses.allTheseData = arrCategories;
        
        [self.navigationController pushViewController:selectedTestTheses animated:YES];
    }
    else{
        // TODO
    }
    
}

#pragma MaincellDelegate
#pragma mark-
-(void)doSeeMore:(LALawsuitObj*)suitObj{
    LACancelamentViewController* cancelamengoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LACancelamentViewController class])];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    cancelamengoVC.objCategory = suitObj;
    [self.navigationController pushViewController:cancelamengoVC animated:YES];
}

- (void)changeSelectedData
{
    [AppDelegate sharedDelegate].allTheseData = self.allTheseData;
    [AppDelegate sharedDelegate].selectedTheseData = self.selectedTheseData;
    
    LATesesHeaderView *headerView = (LATesesHeaderView*)self.tblContent.tableHeaderView;
    headerView.lbTitleTeses.text = [NSString stringWithFormat:kTitleHeader,[self getCountSelectedThese],[AppDelegate sharedDelegate].countThese];;
}

-(void)doAddAllItem :(LALawSuitCategory*)cateObj{
    NSString *strMessage = [NSString stringWithFormat:@"Você não pode selecionar mais de %d",self.numberThese];
    //    if ([self getCountSelectedThese] >= self.numberThese) {
    //        [self showMessage:strMessage withPoistion:nil];
    //        NSLog(@"Show message this1");
    //    }
    //    else if([self getCountSelectedThese] +  [cateObj.arrData count] > self.lanObj.quantityOfTheses){
    //        NSLog(@"Show message this2");
    //        [self showMessage:strMessage withPoistion:nil];
    //    }
    //    else{
    BOOL bExist = NO;
    LALawSuitCategory *current;
    for (LALawSuitCategory *item in self.selectedTheseData) {
        if (item.categoryIdentifier == cateObj.categoryIdentifier && [item.name isEqualToString:cateObj.name]) {
            current = item;
            bExist = YES;
        }
    }
    if (current) {
        [self.selectedTheseData removeObject:current];
    }
    [self.selectedTheseData addObject:cateObj];
    [self changeSelectedData];
    //    }
}
-(void)doRemoveAllNewItem :(LALawSuitCategory*)cateObj{
    if ([self.selectedTheseData containsObject:cateObj]) {
        [self.selectedTheseData removeObject:cateObj];
    }
    else{
        // TODO
    }
    [self changeSelectedData];
}

-(void)doAddNewItem:(LALawsuitObj *)suitObj andCategories:(LALawSuitCategory*)cateObj{
    BOOL bExistCate = NO;
    NSString *strMessage = [NSString stringWithFormat:@"Você não pode selecionar mais de %d",self.numberThese];
    
    //    if ([self getCountSelectedThese] >= self.numberThese) {
    //        [self showMessage:strMessage withPoistion:nil];
    //    }
    //    else{
    if ([self.selectedTheseData count]) {
        for (LALawSuitCategory *item in self.selectedTheseData) {
            if ([item.name isEqualToString:cateObj.name]) {
                bExistCate = YES;
                if (![item.arrData containsObject:suitObj]) {
                    [item.arrData addObject:suitObj];
                }
                else{
                    //TO DO
                }
            }
            else{
                // TODO
            }
        }
        if (!bExistCate) {
            LALawSuitCategory *cateNew = [cateObj copy];
            [cateNew.arrData removeAllObjects];
            cateNew.arrData = [NSMutableArray array];
            [cateNew.arrData addObject:suitObj];
            [self.selectedTheseData addObject:cateNew];
        }
    }
    else{
        if (![self.selectedTheseData count]) {
            self.selectedTheseData = [NSMutableArray array];
        }
        LALawSuitCategory *cateNew = [cateObj copy];
        [cateNew.arrData removeAllObjects];
        cateNew.arrData = [NSMutableArray array];
        [cateNew.arrData addObject:suitObj];
        [self.selectedTheseData addObject:cateNew];
    }
    
    [self changeSelectedData];
    //    }
}

-(void)doRemoveItem:(LALawsuitObj *)suitObj andCategories:(LALawSuitCategory*)cateObj{
    // Remove subItem (suitObj)
    for (LALawSuitCategory *item in self.selectedTheseData) {
        if ([item.name isEqualToString:cateObj.name]) {
            if ([item.arrData containsObject:suitObj]) {
                [item.arrData removeObject:suitObj];
            }
            else{
                // Not Found
                // TODO
            }
        }
    }
    
    // Remove category if theses havent any suitObj (suitObj)
    NSMutableArray *arrTemp = [NSMutableArray array];
    for (LALawSuitCategory *item in self.selectedTheseData) {
        if ([item.arrData count]) {
            [arrTemp addObject:item];
        }
    }
    self.selectedTheseData = arrTemp;
    [self changeSelectedData];
}


-(void)loadTotalTheses{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService lawyerTotalThese:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data != nil) {
            if ([data isKindOfClass:[NSDictionary class]]) {
                [AppDelegate sharedDelegate].countThese = [data[@"data"][@"total"] integerValue];
                headerTableView.lbTitleTeses.text = [NSString stringWithFormat:kTitleHeader,[self getCountSelectedThese],[AppDelegate sharedDelegate].countThese];
                self.numberThese = round((self.lanObj.quantityOfTheses/100.0f) *[data[@"data"][@"total"] integerValue]);
            }
            else{
                // TODO
            }
            
        }
    }];
}

-(NSInteger)getCountSelectedThese{
    NSInteger count = 0;
    for (LALawSuitCategory *cate in self.selectedTheseData) {
        count += [cate.arrData count];
    }
    return count;
}

-(void)ShowMessageOutRangeThese:(NSNotification*)notification{
    NSString *strMessage = [NSString stringWithFormat:@"Você não pode selecionar mais de %d",self.numberThese];
    [self showMessage:strMessage withPoistion:nil];
}
-(void)updateTheseSelectedIfNeed:(LALawSuitCategory*)cate{
    NSMutableArray *arrTemp = [NSMutableArray array];
    for (LALawSuitCategory *cateItem in self.selectedTheseData) {
        for (LALawsuitObj *objSelected in cateItem.arrData) {
            for (LALawsuitObj *obj in cate.arrData) {
                if (objSelected.dataIdentifier == obj.dataIdentifier) {
                    obj.isSelected
                    = YES;
                    obj.initialPrice = objSelected.initialPrice;
                    obj.percentagePrice = objSelected.percentagePrice;
                    cate.isSelected = YES;
                }
            }
        }
    }
    //    for (LALawSuitCategory *item in arrCategories) {
    //        if (item.categoryIdentifier == cate.categoryIdentifier) {
    //            [arrTemp addObject:cate];
    //        }
    //    }
    //    self.selectedTheseData = arrTemp;
    //    [self changeSelectedData];
}
@end

//
//  LACategoriesViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACategoriesViewController.h"
#import "LACategoriesTableViewCell.h"
#import "LACategoriesHeaderView.h"
#import "LAPageViewControl.h"
#import "LASubCatViewController.h"
#import "LALawSuitCategory.h"
#import "LACategoryObj.h"
#import "LAAddressService.h"
#import "LAListSuitCategory.h"
#import "MainService.h"
#import "MBProgressHUD.h"
#import "LABannerObj.h"
#import "LALawsuitObj.h"
#import "LASlideViewCategory.h"
#import "LACategory.h"
#import "UIColor+Law.h"
#import "LACancelamentViewController.h"
#import "LASuitThesesByCateViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+JMImageCache.h"
#import "LASearchEngine.h"
#import "LAThesisBannerObj.h"
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"
#import "IQKeyboardManager.h"
#import "ParallaxHeaderView.h"
#define HEIGHT_NAVIGATION_CUSTOM_FULL 160.0f
#define HEIGH_HEADER_SECTION_CATEGORIES 50.0f
#define HEIGH_CELL_CATEGORIES 90.0f
#define COLOR_BACKGOUND_SEARCH_FIELD [UIColor colorWithRed:149.0/255.0 green:216.0/255.0 blue:253.0/255.0 alpha:1.0]
@interface LACategoriesViewController ()<UINavigationBarDelegate,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>
{
    BOOL isShowSearch;
}
@property (nonatomic) IBOutlet LAPageViewControl* pageControl;
@end

@implementation LACategoriesViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = YES;
        self.isShouldShowBottombar  = YES;
        isShowSearch = NO;
        isHideSearchBar = NO;
        isSearch = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.headerview bringSubviewToFront:self.bannerView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TPKeyboardAvoiding_keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(IQDONE_ACTION:) name:@"IQDONE_ACTION" object:nil];
    
    [self.tbCategories setTableHeaderView:self.headerview];
    // Do any additional setup after loading the view.
//    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
//    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
//    [self.headerview addGestureRecognizer:swipeleft];
//    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
//    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
//    [self.headerview addGestureRecognizer:swiperight];
    
    // Do any additional setup after loading the view.
}
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.bannerView scrollToItemAtIndex:self.bannerView.currentItemIndex+1 animated:true];
}
-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.bannerView scrollToItemAtIndex:self.bannerView.currentItemIndex-1 animated:true];
}

- (void)initRightButton
{
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tbCategories)
    {
        // pass the current offset of the UITableView so that the ParallaxHeaderView layouts the subViews.
        [(ParallaxHeaderView *)self.tbCategories.tableHeaderView layoutHeaderViewForScrollViewOffset:scrollView.contentOffset];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [(ParallaxHeaderView *)self.tbCategories.tableHeaderView refreshBlurViewForNewImage];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideAndDisableRightNavigationItem];
    isHideSearchBar = YES;
    if ([self.txtSearch superview])
    {
        isShowSearch = NO;
        [self.txtSearch removeFromSuperview];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
    return UIBarPositionBottom;
}

- (void)initUI
{
    UIButton* btRight = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [btRight setImage:[UIImage imageNamed:@"ic_search_home"] forState:UIControlStateNormal];
    [btRight addTarget:self action:@selector(onRightButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    btRight.transform = CGAffineTransformMakeTranslation(0, 0);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 90)];
    [backButtonView addSubview:btRight];
    btSearchBarItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.rightBarButtonItem = btSearchBarItem;
    self.txtSearch = [[UITextField alloc] initWithFrame:CGRectMake(8, 10, CGRectGetWidth(self.view.bounds) - 16, 36)];
    [self.txtSearch setKeyboardType:UIKeyboardTypeWebSearch];
    [self.txtSearch setBackgroundColor:COLOR_BACKGOUND_SEARCH_FIELD];
    self.txtSearch.layer.cornerRadius = 7.0f;
    self.txtSearch.delegate = self;
    [self.txtSearch setClearsOnBeginEditing:YES];
    self.txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    //    self.pageControl.iNumberPage = arrSlider.count;
    self.bannerView.scrollEnabled = YES;
    self.bannerView.type = iCarouselTypeRotary;
    self.bannerView.decelerationRate = 0.5;
    [self.bannerView autoscroll];
    [self.pageControl setUpImageActive:@"ic_scrubactive_category" andInActive:@"ic_scrubinactive_category"];
}

- (void)onDone
{
    
}

- (void)initData
{
    [self.tbCategories registerNib:[UINib nibWithNibName:NSStringFromClass([LACategoriesTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LACategoriesTableViewCell class])];
    arrCategories = [NSMutableArray new];
    self.pageControl.iNumberPage = 0;
    [self.pageControl setHidden:YES];
    [self getData];
}
- (void)getData
{
    [self getBannerList];
    [self getCategoryList];
}
- (void)getBannerList
{
    [MainService fetchBanners:^(NSInteger errorCode, NSString *message, id data) {
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            if (success)
            {
                self.listBanner = [[LABanners alloc] initWithDictionary:data];
            }
        }
        if (self.listBanner)
        {
            [self.pageControl setHidden:NO];
            self.pageControl.iNumberPage = self.listBanner.data.count;
            [self.pageControl layoutSubviews];
        }
        else
        {
            [self.pageControl setHidden:YES];
        }
        [self.bannerView reloadData];
    }];
}
- (void)carouselDidScroll:(iCarousel *)carousel
{
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    [self.pageControl setCurrentPage:carousel.currentItemIndex];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:4
                                             target:self
                                           selector:@selector(autoScroll)
                                           userInfo:nil
                                            repeats:YES];
}
- (void)autoScroll
{
    
    NSInteger index = self.bannerView.currentItemIndex + 1;
    if (index >= self.bannerView.numberOfItems) {
        index = 0;
    }
    [self.bannerView scrollToItemAtIndex:index animated:YES];
}
- (void)getCategoryList
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawsuitCategory:^(NSInteger errorCode, NSString *message, id data) {
        
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            if (success)
            {
                id responseData = [data objectForKey:KEY_RESPONSE_DATA];
                if (responseData && [responseData isKindOfClass:[NSArray class]])
                {
                    for (NSDictionary* dict in responseData)
                    {
                        LALawSuitCategory* obj = [[LALawSuitCategory alloc] initWithDictionary:dict];
                        if (obj)
                        {
                            [arrCategories addObject:obj];
                        }
                    }
                }
            }
        }
        [self getDataWithCategory];
        [self.tbCategories reloadData];
    }];
}
- (void)getDataWithCategory
{
    iNumberRequest = 0;
    if (arrCategories.count)
    {
        for (int i = 0; i < arrCategories.count; i ++)
        {
            LALawSuitCategory* obj = [arrCategories objectAtIndex:i];
            obj.arrData = [NSMutableArray new];
            [MainService fetchLawsuitTheseByCategory:obj.categoryIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
                iNumberRequest++;
                if (data && [data isKindOfClass:[NSDictionary class]])
                {
                    BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                    id responseData = [data objectForKey:KEY_RESPONSE_DATA];
                    if (success)
                    {
                        if (responseData && [responseData isKindOfClass:[NSArray class]])
                        {
                            for (NSDictionary* dict in responseData)
                            {
                                LALawsuitObj* objSub = [[LALawsuitObj alloc] initWithDictionary:dict];
                                [obj.arrData addObject:objSub];
                            }
                        }
                        
                    }
                }
                [self reloadInputData];
            }];
        }
    }
}
- (void)reloadInputData
{
    if (iNumberRequest == arrCategories.count)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.tbCategories reloadData];
    }
}
-(void) hideAndDisableRightNavigationItem
{
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    self.navigationItem.rightBarButtonItem = nil;
    [btSearchBarItem setEnabled:NO];
}

-(void) showAndEnableRightNavigationItem
{
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor blackColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    self.navigationItem.rightBarButtonItem = btSearchBarItem;
    [btSearchBarItem setEnabled:YES];
}
- (IBAction)onRightButtonPressed:(id)sender
{
    if (isHideSearchBar)
    {
        return;
    }
    isShowSearch = !isShowSearch;
    CGFloat height = HEIGHT_BIG_NAVIGATION;
    if (isShowSearch)
    {
        height = 140.0f;
    }
    [self.navigationController.navigationBar setHeight:height];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if (isShowSearch)
    {
        [UIView animateWithDuration:0.2 animations:^{
            [self.txtSearch setCenter:CGPointMake(self.navigationController.navigationBar.center.x, self.navigationController.navigationBar.center.y - 60)];
            [self.navigationController.navigationBar addSubview:self.txtSearch];
            [LAUtilitys setRightImageWhenEditing:@"ic_search_home" forTextField:self.txtSearch];
            [LAUtilitys setPaddingForTextField:self.txtSearch andPaddingLeftMargin:3.0];
            [self hideAndDisableRightNavigationItem];
        } completion:^(BOOL finished){
            if([self.txtSearch canBecomeFirstResponder]){
                [self.txtSearch becomeFirstResponder];
                [self.txtSearch becomeFirstResponder];
            }
        }];
    }
    else
    {
        if ([self.txtSearch superview])
        {
            [self.txtSearch removeFromSuperview];
        }
        [self showAndEnableRightNavigationItem];
        if (isSearch) {
            isSearch = !isSearch;
        }
        [self.tbCategories reloadData];
        
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText:@"Cancel"];
    isHideSearchBar = NO;
    CGFloat height = 90.0f;
    if (isShowSearch)
    {
        height = 160.0f;
    }
    [self.navigationController.navigationBar setHeight:height];
    [self showAndEnableRightNavigationItem];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return self.listBanner.data.count;
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    LACancelamentViewController* cancelamengoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LACancelamentViewController class])];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if ([self.txtSearch superview])
    {
        isShowSearch = NO;
        [self.txtSearch removeFromSuperview];
    }
    [self hideAndDisableRightNavigationItem];
    
    LABannerObj* bannerObj = [self.listBanner.data objectAtIndex:index];
    LAThesisBannerObj* obj = bannerObj.thesis;
    LALawsuitObj* objSuit = [LALawsuitObj new];
    objSuit.legalName = obj.legalName;
    objSuit.categoryId = obj.categoryId;
    objSuit.deletedAt = obj.deletedAt;
    objSuit.createdAt = obj.createdAt;
    objSuit.documentation = obj.documentation;
    objSuit.name = obj.name;
    objSuit.howToAct = obj.howToAct;
    objSuit.identification = obj.identification;
    objSuit.status  = obj.status;
    objSuit.version = obj.version;
    objSuit.yourRights = obj.yourRights;
    objSuit.situationId = obj.situationId;
    objSuit.valueSignature = obj.valueSignature;
    objSuit.valueContraction = obj.valueContraction;
    
    LALawSuitCategory *suitCate = [LALawSuitCategory new];
    suitCate.color = obj.category.color;
    suitCate.name = obj.category.name;
    suitCate.icon = obj.category.icon;
    suitCate.iconWithUrl = obj.category.icon_with_url;
    suitCate.categoryIdentifier = obj.categoryId;
    
    objSuit.category =  suitCate;
    
    cancelamengoVC.objCategory = objSuit;
    [self.navigationController pushViewController:cancelamengoVC animated:YES];
    
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view
{
    LASlideViewCategory* viewHere = (LASlideViewCategory*)view;
    LABannerObj* obj= nil;
    if (viewHere == nil)
    {
        viewHere = [[[NSBundle mainBundle] loadNibNamed:@"LASlideViewCategory" owner:self options:nil] objectAtIndex:0];
        [viewHere setFrame:CGRectMake(0, 0, CGRectGetWidth(self.bannerView.bounds), CGRectGetHeight(self.bannerView.bounds))];
    }
    if (self.listBanner && index < self.listBanner.data.count)
    {
        obj = [self.listBanner.data objectAtIndex:index];
    }
    [viewHere.imgCover setimageWithString:obj.imageWithUrl placeholder:[UIImage imageNamed:obj.image]];
    [viewHere.txtTitle setText:obj.dataDescription];
    return viewHere;
}
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    isSearch = NO;
    [self.tbCategories reloadData];
    return YES;
}
- (void)TPKeyboardAvoiding_keyboardWillHide:(NSNotification*)notification
{
    
    //    TPKeyboardAvoidingState *state = self.keyboardAvoidingState;
    //    if ( state.ignoringNotifications ) {
    //        return;
    //    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    isSearch = YES;
    [textField resignFirstResponder];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LASearchEngine sharedInstance] filterWithKeyword:textField.text wComPletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[NSArray class]])
        {
            if (arrCategoriesSearch)
            {
                [arrCategoriesSearch removeAllObjects];
            }
            arrCategoriesSearch = [NSMutableArray new];
            for (LALawsuitObj* obj  in data)
            {
                if ([obj isKindOfClass:[LALawsuitObj class]])
                {
                    if ([arrCategoriesSearch containsObject:obj.category])
                    {
                        [obj.category.arrData addObject:obj];
                    }
                    else
                    {
                        obj.category.arrData = [NSMutableArray new];
                        [obj.category.arrData addObject:obj];
                        [arrCategoriesSearch addObject:obj.category];
                    }
                    NSLog(@"%@",((LALawsuitObj*)obj).description);
                }
                
            }
            if (((NSArray*)data).count == 0)
            {
                [self showMessage:@"Nenhuma tese foi encontrada" withPoistion:nil];
            }
        }
        else
        {
            [self showMessage:@"Nenhuma tese foi encontrada" withPoistion:nil];
        }
        [self.tbCategories reloadData];
    }];
    return YES;
}
#pragma mark UITABLEVIEW DATASOURCE
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    LACategoriesHeaderView *view = (LACategoriesHeaderView*)[[[NSBundle mainBundle]
                                                              loadNibNamed:@"LACategoriesHeaderView"
                                                              owner:self options:nil]
                                                             firstObject];
    CGRect rect = self.tbCategories.frame;
    rect.size.height = HEIGH_HEADER_SECTION_CATEGORIES;
    rect.size.width = CGRectGetWidth(self.tbCategories.frame);
    [view setFrame:rect];
    view.delegate = self;
    NSMutableArray* arrCategoriesHeader = arrCategories;
    if (isSearch)
    {
        arrCategoriesHeader = arrCategoriesSearch;
    }
    if (section < arrCategoriesHeader.count)
    {
        LALawSuitCategory* obj = [arrCategoriesHeader objectAtIndex:section];
        __weak UIImageView *weakImageView = view.imgIconHeader;
        [weakImageView sd_setImageWithURL:[NSURL URLWithString:obj.iconWithUrl] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            weakImageView.image = [weakImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [weakImageView setTintColor:[UIColor colorWithHexString:obj.color]];
        }];
        view.tag = section;
        view.txtTitle.text = obj.name;
        [view.txtTitle setTextColor:[UIColor colorWithHexString:obj.color]];
        view.suitCate = obj;
    }
    return view;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGH_CELL_CATEGORIES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEIGH_HEADER_SECTION_CATEGORIES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSMutableArray* arrCategoriesHeader = arrCategories;
    if (isSearch)
    {
        arrCategoriesHeader = arrCategoriesSearch;
    }
    return arrCategoriesHeader.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LACategoriesTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LACategoriesTableViewCell class]) forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.delegate = self;
    NSMutableArray* arrCategoriesGeneral = arrCategories;
    if (isSearch)
    {
        arrCategoriesGeneral = arrCategoriesSearch;
    }
    if (indexPath.section < arrCategoriesGeneral.count)
    {
        LALawSuitCategory* obj = [arrCategoriesGeneral objectAtIndex:indexPath.section];
        cell.arrData = obj.arrData;
        cell.colorOfSection = [UIColor colorWithHexString:obj.color];
        if (indexPath.section == arrCategoriesGeneral.count - 1)
        {
            [cell.imgBottomLine setHidden:YES];
        }
        else
        {
            [cell.imgBottomLine setHidden:NO];
        }
    }
    [cell.clCategories reloadData];
    return cell;
}
#pragma mark USER ACTION
- (void)onChooseAtIndex:(NSIndexPath*)indexPath
{
    LACancelamentViewController* cancelamengoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LACancelamentViewController class])];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if ([self.txtSearch superview])
    {
        isShowSearch = NO;
        [self.txtSearch removeFromSuperview];
    }
    [self hideAndDisableRightNavigationItem];
    NSMutableArray* arrCategoriesGeneral = arrCategories;
    if (isSearch)
    {
        arrCategoriesGeneral = arrCategoriesSearch;
    }
    if (indexPath.section < arrCategoriesGeneral.count)
    {
        LALawSuitCategory* obj = [arrCategoriesGeneral objectAtIndex:indexPath.section];
        if (obj && obj.arrData.count > indexPath.row)
        {
            LALawsuitObj* objSuit = [obj.arrData objectAtIndex:indexPath.row];
            cancelamengoVC.objCategory = objSuit;
        }
    }
    [self.navigationController pushViewController:cancelamengoVC animated:YES];
}
- (IBAction)onNavigationBarTap:(id)sender
{
    [self onRightButtonPressed:nil];
}
- (IBAction)onViewNavigationTap:(id)sender
{
    [self onRightButtonPressed:nil];
}

-(void)didSelectedCate:(NSInteger)index{
    NSInteger section = index;
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if ([self.txtSearch superview])
    {
        isShowSearch = NO;
        [self.txtSearch removeFromSuperview];
        [self showAndEnableRightNavigationItem];
    }
    NSMutableArray* arrCategoriesHeader = arrCategories;
    if (isSearch)
    {
        arrCategoriesHeader = arrCategoriesSearch;
    }
    LALawSuitCategory* obj = [arrCategoriesHeader objectAtIndex:section];
    LASuitThesesByCateViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LASuitThesesByCateViewController"];
    vc.cateId = obj.categoryIdentifier;
    vc.category = obj;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)IQDONE_ACTION:(NSNotification*)notification{
    //    isShowSearch = NO;
    //    isSearch = NO;
    //    [self.tbCategories reloadData];
    [self onRightButtonPressed:nil];
}
@end

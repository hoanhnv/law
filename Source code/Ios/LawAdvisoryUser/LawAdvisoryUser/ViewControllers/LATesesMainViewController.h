//
//  LATesesMainViewController.h
//  JustapLawyer
//
//  Created by Mac on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "HMSegmentedControl.h"
#import "LAAddNewComarcaViewController.h"

@interface LATesesMainViewController : LABaseViewController

@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic) NSMutableArray* arrSignature;
@property (nonatomic) NSMutableArray* buyThese;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic) HMSegmentedControl* segmentedControl;

@property (strong, nonatomic) NSMutableArray *arrViewController;

- (void)gotoIndex:(NSInteger)index;
@end

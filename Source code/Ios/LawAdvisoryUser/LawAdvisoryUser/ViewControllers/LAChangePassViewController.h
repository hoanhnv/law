//
//  LAChangePassViewController.h
//  JustapLawyer
//
//  Created by MAC on 10/4/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface LAChangePassViewController : LABaseViewController
@property (nonatomic) IBOutlet UITextField* txtPassword;
@property (nonatomic) IBOutlet UITextField* txtNewPassPassword;
@property (nonatomic) IBOutlet UITextField* txtConfirmPassword;

@end

//
//  LAPlanosSelectionViewController.h
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface LAPlanosSelectionViewController : LABaseViewController
{
    NSMutableArray* arrData;
}

@property (nonatomic) IBOutlet UITableView* tbData;
@property (nonatomic,strong) NSString* strComacar;
@property (nonatomic) NSMutableArray* listDistrictSelected;

@property (nonatomic,strong) NSMutableArray* arrSignature;

- (void)onSelectionAtIndex:(NSInteger)index;

@end

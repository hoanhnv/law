//
//  LAStatusAccountViewController.m
//  JustapLawyer
//
//  Created by MAC on 9/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAStatusAccountViewController.h"
#import "MainService.h"
#import "LALoginViewController.h"

static float kTimeMinuteAutoCheck = 3.0;
@interface LAStatusAccountViewController ()
{
    NSTimer* timeCheckStatus;
}
@property (nonatomic) IBOutlet UILabel* lbStatus;
@property (nonatomic) IBOutlet UILabel* lbMessage;
@property (nonatomic) IBOutlet UILabel* lbDescription;

@end

@implementation LAStatusAccountViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldBackButton = NO;
    }
    return self;
}
- (void)viewDidLoad {
    self.navigationController.navigationBar.hidden = YES;
    [super viewDidLoad];
    self.lbMessage.text = self.objStatusAccount.status;
    self.lbDescription.text = self.objStatusAccount.message;
    [self.lbDescription sizeToFit];
    NSLog(@"%@",self.objStatusAccount.status);
    NSLog(@"%@",self.objStatusAccount.message);
    self.btnBackToLogin.titleLabel.text = [self.btnBackToLogin.titleLabel.text uppercaseString];
    self.btnBackToLogin.titleLabel.textColor = [LAUtilitys jusTapColor];
    
    if (self.fromRegister && self.objStatusAccount.type == IN_ACTIVE_ACCOUNT) {
        self.btnBackToLogin.hidden = NO;
    }
    else{
        self.btnBackToLogin.hidden = YES;
    }
#ifdef DEBUG
    [self addGoHomeButton];
#endif
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = nil;
    
    // Importance!
    [self autoCheckAccountStatus];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)addGoHomeButton
{
    UIButton* btGohome = [[UIButton alloc] init];
    [btGohome setTitle:@"SAIR" forState:UIControlStateNormal];
    [btGohome.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [btGohome setTitleColor:[LAUtilitys jusTapColor] forState:UIControlStateNormal];
    [btGohome addTarget:self action:@selector(showLoginView) forControlEvents:UIControlEventTouchUpInside];
    CGRect rect = CGRectMake(0, CGRectGetMaxY(self.lbDescription.frame) + 30, self.btnBackToLogin.frame.size.width, 40);
    [btGohome setFrame:rect];
    CGPoint centerGohome = CGPointMake(self.view.center.x, CGRectGetMaxY(rect) - 20);
    [btGohome setCenter:centerGohome];
    [btGohome setBackgroundImage:[UIImage imageNamed:@"ic_bg_button_rect"] forState:UIControlStateNormal];
    [self.view addSubview:btGohome];
}
- (void)navigationWithStatusAccount:(LAStatusAccountObj*)statusAccountObj
{
    switch (statusAccountObj.type)
    {
        case IN_ACTIVE_ACCOUNT:
        case WAITING_APPROVAL_ACCOUNT:
            break;
        case ACTIVE_ACCOUNT:
        {
            if ([timeCheckStatus isValid])
            {
                [timeCheckStatus invalidate];
                timeCheckStatus = nil;
            }
            [self gotoHome];
        }
            break;
            
        default:
        {
        }
            break;
    }
}
- (void)autoCheckAccountStatus
{
    if (timeCheckStatus == nil)
    {
        timeCheckStatus = [NSTimer timerWithTimeInterval:100*kTimeMinuteAutoCheck target:self selector:@selector(autoCheckAccountStatus) userInfo:nil repeats:YES];
    }
    else
    {
        [MainService checkStatus:[LADataCenter shareInstance].currentAccessToken withBlock:^(NSInteger errorCode, NSString *message, id data) {
            if (data && [data isKindOfClass:[LAStatusAccountObj class]])
            {
                [self navigationWithStatusAccount:data];
            }
        }];
    }
}
- (IBAction)doBackToLogin:(id)sender {
    if (self.fromRegister) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        // TODO
    }
}
-(void)showLoginView{
    [[LADataCenter shareInstance]clearKeyChain];
    LALoginViewController* loginVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LALoginViewController class])];
    [self.navigationController pushViewController:loginVC animated:YES];
}
@end

//
//  LASignaturesViewController.h
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAAssinaturaHeaderView.h"
#import "LAAssinatureHeaderEmpty.h"
#import "LASignatureTese.h"
#import "LAAssinatureInfo.h"

@interface LASignaturesViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *vHeaderTese;
@property (weak, nonatomic) IBOutlet LAAssinaturaHeaderView *vAssintureHeader;
@property (weak, nonatomic) IBOutlet LAAssinatureHeaderEmpty *vEmpty;
@property (strong, nonatomic) LASignatureTese *lstTese;
@property (strong, nonatomic) LAAssinatureInfo *assinatureInfo;

@property (weak, nonatomic) IBOutlet UITableView *tblTeses;
@end

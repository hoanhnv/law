//
//  MainProfileViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/15/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "HMSegmentedControl.h"
#import "LAAddNewComarcaViewController.h"
#import "LASignatureTese.h"
#import "LAAssinature.h"
#import "LAAssinatureInfo.h"
#import "LAUFs.h"

@interface MainProfileViewController : LABaseViewController

@property (nonatomic) HMSegmentedControl* segmentedControl;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) NSMutableArray *arrViewController;

@property (assign, nonatomic) NSInteger selectedInitIndex;

@property (strong, nonatomic) LAAddNewComarcaViewController* addNewComarcaVC;

@property (strong, nonatomic) LASignatureTese *lstTese;
@property (strong, nonatomic) LAAssinatureInfo *assinatureInfo;
@property (assign, nonatomic) BOOL needReload;
@property (nonatomic, strong) LAUFs *listUFS;

- (void)gotoIndex:(NSInteger)index;
-(void)FinishBuyThesis;
@end

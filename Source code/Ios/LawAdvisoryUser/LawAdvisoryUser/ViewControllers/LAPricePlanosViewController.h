//
//  LAPricePlanosViewController.h
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LATesesMainTableViewCell.h"

@interface LAPricePlanosViewController : LABaseViewController<UITableViewDelegate,UITableViewDataSource,LATesesMainTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (strong, nonatomic)  NSMutableArray *selectedTheseData;
@property (strong, nonatomic)  NSMutableArray *allTheseData;
@property (strong, nonatomic)  NSMutableArray *currentSelected;
@end

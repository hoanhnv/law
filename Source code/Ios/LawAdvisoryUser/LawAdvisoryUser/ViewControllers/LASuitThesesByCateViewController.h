//
//  LASuitThesesByCateViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/4/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LALawSuitCategory.h"

@interface LASuitThesesByCateViewController : LABaseViewController{
    
}

@property (nonatomic,assign) double cateId;
@property (nonatomic, strong) LALawSuitCategory *category;
@property (weak, nonatomic) IBOutlet UITableView *tblData;

@end

//
//  LAPlanosBoletoViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanosBoletoViewController.h"
#import "LADistrictObj.h"
#import "LACartItemViewCell.h"
#import "MainService.h"
#import "FaleResponse.h"
#import "LAContactSubjects.h"
#import "MainProfileViewController.h"

@interface LAPlanosBoletoViewController ()<MessagePopupDelegate>

@end

@implementation LAPlanosBoletoViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tblSignature registerNib:[UINib nibWithNibName:@"LACartItemViewCell" bundle:nil] forCellReuseIdentifier:@"LACartItemViewCell"];
    
    self.scrFullData.frame = CGRectMake(self.scrFullData.frame.origin.x, self.scrFullData.frame.origin.y, self.scrFullData.frame.size.width, self.scrFullData.frame.size.height + 50);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initData
{
    self.lbPlanosName.text = [LAUtilitys getDefaultString:self.lanObj.name];
    LASignatureObj *lastSignature = [self.arrSignature lastObject];
    self.lbTotalPlanSinging.text = [NSString stringWithFormat:@"Valor Total: R$ %@",[ LAUtilitys getCurrentcyFromDecimal:lastSignature.totalPlanOnSigning]];
    [self.tblSignature reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"Planos";
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    [self setupFrame];
}

-(void)setupFrame{
    
    self.tblSignature.frame = CGRectMake(self.tblSignature.frame.origin.x, self.tblSignature.frame.origin.y, self.tblSignature.frame.size.width, [self.arrSignature count]*97);
    
    self.lbTotalPlanSinging.frame = CGRectMake(self.lbTotalPlanSinging.frame.origin.x, CGRectGetMaxY(self.tblSignature.frame) + 15, self.lbTotalPlanSinging.frame.size.width, self.lbTotalPlanSinging.frame.size.height);
    
    self.vBottom.frame = CGRectMake(self.vBottom.frame.origin.x, CGRectGetMaxY(self.lbTotalPlanSinging.frame) + 7, self.vBottom.frame.size.width,self.vBottom.frame.size.height);
    
    self.scrFullData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.vBottom.frame));
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrSignature count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LACartItemViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LACartItemViewCell" forIndexPath:indexPath];
    [cell setupCell:self.arrSignature[indexPath.row] andIndexPath:indexPath andLan:self.lanObj];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 97.0f;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath isEqual:((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject])]){
        [self setupFrame];
    }
}

- (IBAction)doConfirm:(id)sender {
    NSDictionary *params = @{@"payment_method":@"boleto"};
    LASignatureObj *signatureObj = [self.arrSignature lastObject];
    if (signatureObj) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [MainService lawSignatures4:params withSignaruteId:signatureObj.dataIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            LAContactSubjects *responseSubject = [LAContactSubjects new];
            if (data != nil) {
                if ([data isKindOfClass:[NSDictionary class]]) {
                    if ([data[@"success"] boolValue]) {
                        responseSubject.message = MSG_010;
                        responseSubject.success = YES;
                    }
                    else{
                        responseSubject.message = data[@"message"];
                        responseSubject.success = NO;
                    }
                }
                else{
                    responseSubject.message = message;
                    responseSubject.success = NO;
                }
            }
            else{
                responseSubject.message = message;
                responseSubject.success = NO;
            }
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            FaleResponse *falePoup = [storyboard instantiateViewControllerWithIdentifier:@"FaleResponse"];
            falePoup.delegate = self;
            falePoup.responseSubject = responseSubject;
            falePoup.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [self presentViewController:falePoup animated:YES completion:^{
            }];
        }];
    }
    else{
        // TODO
    }
}

-(void)doClosePopup:(BOOL)isSusccess{
    if (isSusccess) {
        //TODO
    }
    else{
        //TODO
    }
    MainProfileViewController* mainVC = nil;
    NSArray* arrVC = self.navigationController.viewControllers;
    for (UIViewController* vc  in arrVC) {
        if ([vc isKindOfClass:[MainProfileViewController class]])
        {
            mainVC = (MainProfileViewController*)vc;
            break;
        }
    }
    if (mainVC)
    {
        mainVC.segmentedControl.selectedSegmentIndex = 2;
        [mainVC gotoIndex:2];
        [self.navigationController popToViewController:mainVC animated:YES];
    }
    
}

@end

//
//  LADetalHesViewController.m
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LADetalHesViewController.h"
#import "HeaderSectionCencelMagento.h"
#import "InfoCell.h"
#import "LADetalhesHeaderView.h"
#import "LAdetalhesFooterView.h"
#import "HeaderSectionDelalhes.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LAMedias.h"
#import "LAPhotoShowViewController.h"
#import "RecordItemTableViewCell.h"
#import "LAAudioTableViewCell.h"
#import "LAAudioPlayerController.h"
#import "LAImageTableViewCell.H"
#import "LAClientTableViewCell.h"
#import "LAPagarCatartaoViewController.h"
#import "LAWitnessTableViewCell.h"
#import "LAWitnesses.h"
#import "MainService.h"
#import "LAAcoesViewController.h"
#import "IQKeyboardManager.h"
#import "LAActionDataObj.h"
#import "LAActionResponseInfo.h"
#import "LAWitnessDetailViewController.h"
#import "UIColor+Law.h"
#import "LAMessageViewController.h"
#import "LAMessageObj.h"
#import "LAMessagePopupViewController.h"
#import "LASignatureCancelViewController.h"

@import AVFoundation;
@import AVKit;
@interface LADetalHesViewController ()<UITextFieldDelegate>
{
    NSMutableArray* arrHeaderTitle;
    NSMutableDictionary * mSelected;
//    NSMutableArray* arrData;
    LADetalhesHeaderView* headerDetailView;
    LAdetalhesFooterView* footerView;
    LAMessageObj *mes;
    
}
@end

@implementation LADetalHesViewController
int tick1 = 0;
int Hours1 = 0;
int Minutes1 = 0;
int Seconds1 = 0;
NSTimer *timer1;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataTb) name:@"ChangeFooterView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(goFinishAction) name: @"finishAction" object: nil];
    self.arrData = [NSMutableArray new];
    self.tbdata.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [MainService getMessage:self.objData.dataIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data){
        [MBProgressHUD hideHUDForView:self.view animated:true];
        LAActionResponseInfo *respose = [[LAActionResponseInfo alloc]initWithDictionary:data];
        if(respose.success){
            NSMutableArray *arrMes = [[data objectForKey:@"data"] objectForKey:@"messages"];
//            self.arrData = arrMes;
            for (NSDictionary *dict in arrMes) {
                mes = [[LAMessageObj alloc] initWithDictionary:dict];
                [_arrData addObject:mes];
            }
            _arrData=[[[_arrData reverseObjectEnumerator] allObjects] mutableCopy];
        }
        [self setupData];
    }];

    // Do any additional setup after loading the view.
    self.backIndex = -1;
}
- (void)reloadDataTb
{
    self.tbdata.tableFooterView = self.tbdata.tableFooterView;
}
-(void)setupTime {
    NSDate *currentTime;
    currentTime = [NSDate date];
    double dateValue;
    double createAt = self.objData.createdAt;
    NSString *dateCreate;
    dateCreate =[LAUtilitys getDateStringFromTimeInterVal:createAt withFormat:FORMAT_DD_MM_YY];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    dateformate.timeZone =  [NSTimeZone timeZoneForSecondsFromGMT:3*3600];
    [dateformate setDateFormat:FORMAT_DD_MM_YY]; // Date formater
    NSString *dateTXT = [dateformate stringFromDate:currentTime];
    currentTime = [dateformate dateFromString:dateTXT];
    dateValue = currentTime.timeIntervalSince1970;
    int countTime = (dateValue - createAt);
    Hours1 = countTime/3600;
    countTime = countTime % 3600;
    Minutes1 = countTime/60;
    countTime = countTime % 60;
    Seconds1 = countTime;
    
    NSString *strOldTime = [[LADataCenter shareInstance]getTimerValue:self.objData.dataIdentifier];
    NSArray *arrTime = [strOldTime componentsSeparatedByString:@":"];
    if ([LAUtilitys NullOrEmpty:strOldTime]) {
        Hours1 = 0;
        Minutes1 = 0;
        Seconds1 = 0;
    }
    else{
        Hours1 = [[arrTime objectAtIndex:0] intValue];
        Minutes1 = [[arrTime objectAtIndex:1] intValue];
        Seconds1 = [[arrTime objectAtIndex:2] intValue];
    }
    //Change start timer
    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tickTime) userInfo:nil repeats:YES];
    
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShouldBackButton = YES;
        //        self.isShowGreenNavigation = YES;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText:@"Done"];
    
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    
    if (self.objData.happening.thesis.category.color && ![LAUtilitys isEmptyOrNull:self.objData.happening.thesis.category.color])
    {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:self.objData.happening.thesis.category.color];
    }
    else{
        self.navigationController.navigationBar.barTintColor = [LAUtilitys jusTapColor];
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LAAudioPlayerController sharedInstance] stop];
    [[LADataCenter shareInstance]saveTimerValue:self.objData.dataIdentifier withValue:[NSString stringWithFormat:@"%02i:%02i:%02i",Hours1,Minutes1,Seconds1]];
    [timer1 invalidate];
    timer1 = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupData
{
    
    mSelected = [NSMutableDictionary new];
    [self.tbdata registerNib:[UINib nibWithNibName:NSStringFromClass([InfoCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoCell class])];
    [self.tbdata registerNib:[UINib nibWithNibName:NSStringFromClass([LAAudioTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAAudioTableViewCell class])];
    [self.tbdata registerNib:[UINib nibWithNibName:NSStringFromClass([LAClientTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAClientTableViewCell class])];
    [self.tbdata registerNib:[UINib nibWithNibName:NSStringFromClass([LAImageTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAImageTableViewCell class])];
    [self.tbdata registerNib:[UINib nibWithNibName:NSStringFromClass([LAWitnessTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAWitnessTableViewCell class])];
    
    arrHeaderTitle = [NSMutableArray arrayWithObjects:@"DESCRIÇÃO",@"CLIENTE",@"DOCUMENTOS",@"TESTEMUNHAS", nil];
    NSArray* arrHeaders = [[NSBundle mainBundle] loadNibNamed:@"LADetalhesHeaderView"
                                                        owner:self
                                                      options:nil];
    
    headerDetailView = [ arrHeaders firstObject];
    headerDetailView.delegate = self;
    headerDetailView.txtNumber.delegate = self;
    //    [headerDetailView.btnOk addTarget:self action:@selector(onPressOkPressed:) forControlEvents:UIControlEventTouchUpInside];
    if (self.isShowButtonOk) {
        headerDetailView.btnOk.hidden = NO;
        headerDetailView.txtNumber.enabled = YES;
    }
    else{
        headerDetailView.btnOk.hidden = YES;
        headerDetailView.txtNumber.enabled = NO;
    }
    self.navigationItem.title = self.objData.happening.thesis.name;
    [self setUpHeaderData:headerDetailView];
    if ([self.objData.statusDescription isEqualToString:@"Em andamento"]) {
        headerDetailView.frame = CGRectMake(0,headerDetailView.frame.origin.y , headerDetailView.frame.size.width, headerDetailView.frame.size.height - headerDetailView.lbTime.frame.size.height);
    }
    else{
        if (self.fromTabIndex != 3) {
            [self setupTime];
        }
        else{
            [headerDetailView.lbTime setHidden:true];
            headerDetailView.frame = CGRectMake(0,headerDetailView.frame.origin.y , headerDetailView.frame.size.width, headerDetailView.frame.size.height - headerDetailView.lbTime.frame.size.height);
        }
    }
    NSArray* arrFooters = [[NSBundle mainBundle] loadNibNamed:@"LAdetalhesFooterView"
                                                        owner:self
                                                      options:nil];
    
        footerView = [arrFooters firstObject];
        footerView.delegate = self;
        footerView.arrData = self.arrData;
        footerView.isFirstLoad = YES;
        [footerView reloadDataFooter];
        [footerView.tbvMessage reloadData];
    [headerDetailView.titleRefusal setHidden:true];
    [headerDetailView.lbRefusal setHidden:true];
    if (self.fromTabIndex == 3) {
        [headerDetailView.titleRefusal setHidden:false];
        [headerDetailView.lbRefusal setHidden:false];
        [footerView.btEnviar setHidden:true];
        [footerView.btFinish setHidden:true];
        CGRect frame = footerView.frame;
        frame.size.height = CGRectGetMaxY(footerView.tbvMessage.frame);
    }
    self.tbdata.tableFooterView = footerView;
    self.tbdata.tableHeaderView = headerDetailView;
    [self.tbdata reloadData];
}
-(void)getMessage:(NSString *)message {
    footerView.txtMessage.text = message;
}
-(void) tickTime{
    tick1 ++;
    if (Seconds1 <59) {
        Seconds1 += 1;
    }
    else if (Seconds1 == 59 && Minutes1 <59) {
        Seconds1 = 0;
        Minutes1 += 1;
    }
    else {
        if (Hours1 <99) {
            Hours1 += 1;
            Minutes1 = 0;
        }
        else {
            Hours1 = 0;
            Minutes1 = 0;
            Seconds1 = 0;
        }
    }
    NSString * str = [NSString stringWithFormat:@"Você possui %02i:%02i:%02i para responder este pedido.Lembre-se que o tempo de resposta influencia em sua avaliação.",Hours1,Minutes1,Seconds1];
    headerDetailView.lbTime.text = str;
}

- (void)setUpHeaderData:(LADetalhesHeaderView*)headerView
{
    if (self.objData)
    {
        if ([self.objData.processNumber doubleValue] > 0) {
            headerView.txtNumber.text = self.objData.processNumber;
        }
        headerView.lbStatus.text = self.objData.statusDescription;
        headerView.lbClient.text = self.objData.user.name;
        headerView.lbCategoria.text = self.objData.happening.thesis.category.name;
        headerView.lbAssunto.text = self.objData.happening.thesis.name;
        headerView.lbData.text = [LAUtilitys getDateStringFromTimeInterVal:self.objData.updatedAt withFormat:FORMAT_DD_MM_YY];
        if (self.objData.lawsuitRefusalReason != nil){
            headerDetailView.lbRefusal.text = self.objData.lawsuitRefusalReason.name;
        }
    }
}
-(NSString*)strWithFormatNumber:(NSString*)strValue{
    if (![LAUtilitys NullOrEmpty:strValue]) {
        NSMutableString *mutaleString = [[NSMutableString alloc]initWithString:strValue];
        if([mutaleString length]==14)
        {
            if ([mutaleString length]>4) {
                [mutaleString insertString:@"." atIndex:4];
            }
            if ([mutaleString length]>7) {
                [mutaleString insertString:@"." atIndex:7];
            }
            if ([mutaleString length]>9) {
                [mutaleString insertString:@"." atIndex:9];
            }
            if ([mutaleString length]>16) {
                [mutaleString insertString:@"-" atIndex:16];
            }
        }
        else {
            if ([mutaleString length]>7) {
                [mutaleString insertString:@"-" atIndex:7];
            }
            if ([mutaleString length]>10) {
                [mutaleString insertString:@"." atIndex:10];
            }
            if ([mutaleString length]>15) {
                [mutaleString insertString:@"." atIndex:15];
            }
            if ([mutaleString length]>17) {
                [mutaleString insertString:@"." atIndex:17];
            }
            if ([mutaleString length]>20) {
                [mutaleString insertString:@"." atIndex:20];
            }
        }
        return mutaleString;
    }
    else{
        return STRING_EMPTY;
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma  mark - DetalHesPagamentoDelegate
- (void)onPressDetalhesPagementoPressed
{
    UIStoryboard *devStoryboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAPagarCatartaoViewController *creditCardVC = [devStoryboard instantiateViewControllerWithIdentifier:@"LAPagarCatartaoViewController"];
    creditCardVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:creditCardVC animated:YES];
}
#pragma  mark - DetalHesFooterDelegate
- (void)addMessage:(NSString*)message{
    LAMessageObj *objSend = [LAMessageObj new];
    objSend.body = message;
    NSDate *date = [NSDate date];
    int creatAt = date.timeIntervalSince1970;
    objSend.createdAt = creatAt;
    objSend.user = self.objData.user;
    
    NSInteger count = [_arrData count];
    [_arrData insertObject:objSend atIndex:count];
    footerView.arrData = self.arrData;
    [footerView reloadDataFooter];
    //Scroll to bottom of table
    [footerView.tbvMessage reloadData];
    [footerView.tbvMessage scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.arrData count] -1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}
- (void)onSendmessage:(NSString*)Message
{
    if(![LAUtilitys isEmptyOrNull:footerView.txtMessage.text]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
        NSDictionary *param = @{@"lawsuit_lawyer_id":@(self.objData.dataIdentifier),
                                @"message":footerView.txtMessage.text};
        [MainService postMessage:param withBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            LAActionResponseInfo *respose = [[LAActionResponseInfo alloc]initWithDictionary:data];
            [self addMessage:Message];
            if (respose.success) {
                [self showMessage:@"Mensagem enviada com sucesso" withPoistion:nil];
            }
            else{
                [self showMessage:message withPoistion:nil];
            }
        }];
    }
    else {
        [self showMessage:@"Por favor, digite sua mensagem." withPoistion:nil];
    }
    
    footerView.txtMessage.text = @"";
}
-(void)viewMessage {
    UIStoryboard *storyboardDev = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAMessagePopupViewController *popupMess = [storyboardDev instantiateViewControllerWithIdentifier:@"LAMessagePopupViewController"];
    popupMess.arrData = self.arrData;
    [self presentViewController:popupMess animated:YES completion:nil];
}
-(void)onWriteMessage {
    UIStoryboard *str = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAMessageViewController *mess = [str instantiateViewControllerWithIdentifier:@"LAMessageViewController"];
    mess.delegate = self;
    mess.Id = self.objData.dataIdentifier;
    [self.navigationController pushViewController:mess animated:YES];
}
- (void)onFinishAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LASignatureCancelViewController *cancelVC = [storyboard instantiateViewControllerWithIdentifier:@"LASignatureCancelViewController"];
    cancelVC.titleNavigation = self.navigationItem.title;
    cancelVC.navigationColor = self.navigationController.navigationBar.barTintColor;
    cancelVC.isFinishAction = true;
    [self.navigationController pushViewController:cancelVC animated:YES];
}
-(void)goFinishAction{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [MainService lawsuitFinalize:self.objData.dataIdentifier withSuccess:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        LAActionResponseInfo *respose = [[LAActionResponseInfo alloc]initWithDictionary:data];
        if (respose.success) {
            [self.tabBarController setSelectedIndex:1];
            UINavigationController *nav3 = (UINavigationController*)[self.tabBarController.viewControllers objectAtIndex:1];
            if ([nav3.viewControllers count]) {
                UIViewController *rootVC = [nav3.viewControllers objectAtIndex:0];
                if ([rootVC isKindOfClass:[LAAcoesViewController class]]) {
                    LAAcoesViewController *acoeVC = (LAAcoesViewController*)rootVC;
                    acoeVC.selectedInitIndex = 3;
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"ChangeSelectedTabIndex" object:[NSNumber numberWithInteger:3]];
        }
        else{
            [self showMessage:respose.message withPoistion:nil];
        }
        
    }];

}
#pragma mark - TABLE
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrHeaderTitle.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)section]] integerValue] == 1;
    switch (section) {
        case 0:
        {
            if (isShow) {
                return 1;
            }
            else{
                return 0;
            }
        }
        break;
        case 1:
        {
            if (isShow) {
                return 1;
            }
            else{
                return 0;
            }
        }
            break;
        case 2:
        {
            if (isShow) {
                return self.objData.happening.medias.count;
            }
            else{
                return 0;
            }

        }
        case 3:
        {
            if (isShow) {
                return self.objData.happening.witnesses.count;
            }
            else{
                return 0;
            }
        }
            break;
        default:
            break;
    }
    return [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)section]]integerValue];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]] integerValue] == 1;
    switch (indexPath.section) {
        case 0:{
            if (isShow) {
                return [self getHeightWithSection:indexPath.section];
            }
            else{
                return 0;
            }
            
        }
        break;
        case 1:
        {
            if (isShow) {
                return 360.0;
            }
            else{
                return 0;
            }
        }
        break;
        case 2:
        {
            if (isShow) {
                return 44.0;
            }
            else{
                return 0;
            }
        }
        break;
        case 3:
        {
            if (isShow) {
                return 44.0;
            }
            else{
                return 0;
            }
        }
        break;
        default:
            return 0.01f;
            break;
    }
}
- (CGFloat)getHeightWithSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            NSString *questionInfo = self.objData.happening.otherInformation;
            NSAttributedString *attr = [LAUtilitys getAtributeTextFromString:questionInfo];
            return [InfoCell getHeightAttr:attr inWith:self.tbdata];
        }
            break;
            
        default:
            break;
    }
    return 0.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]] integerValue] == 1;
    switch (indexPath.section) {
        case 0:
        {
            InfoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoCell class]) forIndexPath:indexPath];
            NSString *questionInfo = self.objData.happening.otherInformation;
            NSAttributedString* attrStr = [LAUtilitys getAtributeTextFromString:questionInfo];
            cell.lbContent.attributedText = attrStr;
            [cell.btnCancel setTag:indexPath.row];
            if (!isShow) {
                cell.frame = CGRectZero;
            }
            return cell;
        }
            break;
        case 1:
        {
            LAClientTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAClientTableViewCell class]) forIndexPath:indexPath];
            [cell setupCell:self.objData.user];
            if (!isShow) {
                cell.frame = CGRectZero;
            }
            return cell;
        }
            break;
        case 2:
        {
            LAMedias* obj = [self.objData.happening.medias objectAtIndex:indexPath.row];
            if ([obj.mimeType containsString:@"audio"] || [obj.mimeType containsString:@"video"]) {
                LAAudioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LAAudioTableViewCell" forIndexPath:indexPath];
                cell.lbName.text = obj.media;
                cell.strUrl = obj.mediaWithUrl;
                cell.backgroundColor = [UIColor whiteColor];
                if (!isShow) {
                    cell.frame = CGRectZero;
                }
                return  cell;
            }
            else
                if ([obj.mimeType containsString:@"image"]) {
                    LAImageTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"LAImageTableViewCell" forIndexPath:indexPath];
                    [cell.imgShow sd_setImageWithURL:[NSURL URLWithString:obj.mediaWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                    cell.lbName.text = obj.media;
                    cell.backgroundColor = [UIColor whiteColor];
                    if (!isShow) {
                        cell.frame = CGRectZero;
                    }
                    return  cell;
                }
                else
                {
                }
        }
            break;
        case 3:
        {
            LAWitnesses* obj = [self.objData.happening.witnesses objectAtIndex:indexPath.row];
            LAWitnessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LAWitnessTableViewCell" forIndexPath:indexPath];
            cell.lbName.text = obj.name;
            if (!isShow) {
                cell.frame = CGRectZero;
            }
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
    return nil;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSectionDelalhes * header = [[[NSBundle mainBundle] loadNibNamed:@"HeaderSectionDelalhes" owner:self options:nil] objectAtIndex:0];
    [header setupColor:NO];
    header.lblTitle.text = [arrHeaderTitle objectAtIndex:section];
    for (NSString * key in mSelected.allKeys) {
        if (section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)section]] integerValue] == 1;
            [header setupColor:isShow];
        }
    }
    [header setShowBlock:^(BOOL isShow){
        if (isShow) {
            [mSelected setObject:@(1) forKey:[NSString stringWithFormat:@"%ld",(long)section]];
        } else{
            [mSelected setObject:@(0) forKey:[NSString stringWithFormat:@"%ld",(long)section]];
        }
        [self.tbdata reloadData];
    }];
    return header;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 2:
        {
            LAMedias* obj = [self.objData.happening.medias objectAtIndex:indexPath.row];
            if ([obj.type isEqualToString:@"audio"]) {
            }
            else
                if ([obj.type isEqualToString:@"document"]) {
                    [self showPhoto:indexPath];
                }
                else
                {
                    [self onShowVideoPlayer:indexPath];
                }
        }
            break;
        case 3:{
            LAWitnesses *witnessObj = [self.objData.happening.witnesses objectAtIndex:indexPath.row];
            [self showWitness:witnessObj];
        }
            break;
        default:
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 53.0f;
}
- (void)onShowVideoPlayer:(NSIndexPath*)indexPath
{
    if (indexPath.row < self.objData.happening.medias.count)
    {
        LAMedias* obj = [self.objData.happening.medias objectAtIndex:indexPath.row];
        NSURL *videoURL = [NSURL URLWithString:obj.mediaWithUrl];
        // create an AVPlayer
        AVPlayer *player = [AVPlayer playerWithURL:videoURL];
        // create a player view controller
        AVPlayerViewController *controller = [[AVPlayerViewController alloc]init];
        controller.player = player;
        // show the view controller
        [self presentViewController:controller animated:YES completion:^{
            [player play];
        }];
    }
}
- (void)onPlayAudioPressed
{
    [[LAAudioPlayerController sharedInstance] stop];
}
- (void)showPhoto:(NSIndexPath*)indexPath
{
    if (indexPath.row < self.objData.happening.medias.count)
    {
        
        LAMedias* obj = [self.objData.happening.medias objectAtIndex:indexPath.row];
        LAPhotoShowViewController* photoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPhotoShowViewController class])];
        photoVC.delegate = self;
        [self.navigationController presentViewController:photoVC animated:YES completion:^{
            
        }];
        [photoVC.imgPhotoShow sd_setImageWithURL:[NSURL URLWithString:obj.mediaWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
}
-(void)showWitness:(LAWitnesses*)obj{
    LAWitnessDetailViewController *detailVC = [[UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil] instantiateViewControllerWithIdentifier:@"LAWitnessDetailViewController"];
    detailVC.witnessObj = obj;
    detailVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:detailVC animated:YES completion:^{
    }];
    
}
- (void)onPressOkPressed:(NSString *)strValue{
//    strValue = [strValue stringByReplacingOccurrencesOfString:@"." withString:STRING_EMPTY];
//    strValue = [strValue stringByReplacingOccurrencesOfString:@"-" withString:STRING_EMPTY];
    if ([LAUtilitys NullOrEmpty:strValue]||(strValue.length!=18&&strValue.length!=25)) {
        [self showMessage:@"*Número do processo inválidos" withPoistion:nil];
    }
    else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [MainService lawSuitProgress:self.objData.dataIdentifier withProcessNumber:strValue withBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            LAActionResponseInfo *response = [[LAActionResponseInfo alloc]initWithDictionary:data];
            if (response.success) {
                [self backAfterOk];
            }
            else{
                [self showMessage:response.message withPoistion:nil];
            }
        }];
    }
}
#pragma  mark - TextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (textField == headerDetailView.txtNumber) {
        // Here we check if the replacement text is equal to the string we are currently holding in the paste board
//        if ([string isEqualToString:[UIPasteboard generalPasteboard].string]) {
//            NSMutableString *mu = [NSMutableString stringWithString:string];
//            if ([string length] == 14) {
//                [mu insertString:@"." atIndex:5];
//                 [mu insertString:@"." atIndex:8];
//                 [mu insertString:@"." atIndex:10];
//                [mu insertString:@"-" atIndex:17];
//            }
//            else if([string length]==20){
//                [mu insertString:@"-" atIndex:8];
//                [mu insertString:@"." atIndex:10];
//                [mu insertString:@"." atIndex:15];
//                [mu insertString:@"." atIndex:17];
//                [mu insertString:@"." atIndex:20];
//
//            }
//            textField.text = mu;
//            
//        } else {
            if (newString.length == 5)
            {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                if (newLength > textField.text.length)
                {
                    textField.text = [textField.text stringByAppendingString:@"."];
                }
            }
            else if (newString.length == 8)
            {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                if (newLength > textField.text.length)
                {
                    textField.text = [textField.text stringByAppendingString:@"."];
                }
            }
            else if (newString.length == 10)
            {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                if (newLength > textField.text.length)
                {
                    textField.text = [textField.text stringByAppendingString:@"."];
                }
            }
            else if (newString.length == 17)
            {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                if (newLength > textField.text.length)
                {
                    textField.text = [textField.text stringByAppendingString:@"-"];
                }
            }
            else if (newString.length == 18){
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                if (newLength<textField.text.length) {
                    NSString *str = [textField.text stringByReplacingOccurrencesOfString:@"." withString:@""];
                    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    NSMutableString *mutaleString = [[NSMutableString alloc]initWithString:str];
                    [mutaleString insertString:@"." atIndex:4];
                    [mutaleString insertString:@"." atIndex:7];
                    [mutaleString insertString:@"." atIndex:9];
                    [mutaleString insertString:@"-" atIndex:16];
                    //str = mutaleString;
                    textField.text = mutaleString;
                }

            }
            else if(newString.length == 19) {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                if (newLength > textField.text.length){
                    NSString *str = [newString stringByReplacingOccurrencesOfString:@"." withString:@""];
                    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    NSMutableString *mutaleString = [[NSMutableString alloc]initWithString:str];
                    [mutaleString insertString:@"-" atIndex:7];
                    [mutaleString insertString:@"." atIndex:10];
                    [mutaleString insertString:@"." atIndex:15];
                    [mutaleString insertString:@"." atIndex:17];
                    
                   // [mutaleString deleteCharactersInRange:NSMakeRange(, <#NSUInteger len#>)]
                    
                    str = mutaleString;
                    str = [str substringToIndex:[str length] - 1];
                    textField.text = str;
                    
                }
                /*else if (newLength<textField.text.length) {
                    NSString *str = [textField.text stringByReplacingOccurrencesOfString:@"." withString:@""];
                    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    NSMutableString *mutaleString = [[NSMutableString alloc]initWithString:str];
                    [mutaleString insertString:@"." atIndex:4];
                    [mutaleString insertString:@"." atIndex:7];
                    [mutaleString insertString:@"." atIndex:9];
                    [mutaleString insertString:@"-" atIndex:16];
                    str = mutaleString;
                    //str = [str substringToIndex:[str length] - 1];
                    textField.text = str;
                }*/

            }
            else if (newString.length == 21) {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                if (newLength > textField.text.length)
                {
                    textField.text = [textField.text stringByAppendingString:@"."];
                }
            }
            else
            {
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 25;
            }
            return YES;
//        }
    }
    return YES;
}
-(void)backButtonPressed{
    if (self.backIndex >= 0) {
        [self.navigationController popViewControllerAnimated:YES];
        if (self.navigationController.viewControllers.count)
        {
            for (UIViewController* vc in self.navigationController.viewControllers)
            {
                if ([vc isKindOfClass:[LAAcoesViewController class]])
                {
                    [((LAAcoesViewController*)vc) gotoIndex:2];
                    break;
                }
            }
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
-(void)backAfterOk{
    [self.tabBarController setSelectedIndex:1];
    UINavigationController *nav3 = (UINavigationController*)[self.tabBarController.viewControllers objectAtIndex:1];
    if ([nav3.viewControllers count]) {
        UIViewController *rootVC = [nav3.viewControllers objectAtIndex:0];
        if ([rootVC isKindOfClass:[LAAcoesViewController class]]) {
            LAAcoesViewController *acoeVC = (LAAcoesViewController*)rootVC;
            acoeVC.selectedInitIndex = 2;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ChangeSelectedTabIndex" object:[NSNumber numberWithInteger:2]];
}
@end

//
//  LAAnswerForgotPass.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAnswerForgotPass.h"

@implementation LAAnswerForgotPass

-(void)viewDidLoad{
    [super viewDidLoad];
    if (!self.bSuccess) {
        self.lbMessage.text = MSG_005;
        [self setupText];
        [self resetFrame];
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
        tapGesture.numberOfTapsRequired = 1;
        [self.lbMessage addGestureRecognizer:tapGesture];
    }
    else{
        self.lbMessage.text = [NSString stringWithFormat:MSG_006,self.strEmail];
    }
    
    self.lbMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.lbMessage.numberOfLines = 0;
    
    self.btnClose.titleLabel.text =  [self.btnClose.titleLabel.text uppercaseString];
    self.btnClose.titleLabel.textColor = [LAUtilitys jusTapColor];
}

- (IBAction)doClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(onExitPressed)])
        {
            [self.delegate onExitPressed];
        }
    }];
}
- (void)setupText
{
    self.lbMessage.userInteractionEnabled = YES;
    UIColor *color1 = self.lbMessage.textColor;
    NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : color1 ,NSFontAttributeName :self.lbMessage.font};
    UIColor *color2 = [LAUtilitys jusTapColor];
    NSDictionary *attrs2 = @{ NSForegroundColorAttributeName : color2 ,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),NSFontAttributeName :self.lbMessage.font};
    NSAttributedString *nameStr = [[NSAttributedString alloc] initWithString:@"Senha incorreta! \nPor favor, tente novamente.\nCaso tenha esquecido sua senha, utilize o link " attributes:attrs1];
    NSAttributedString *countStr = [[NSAttributedString alloc] initWithString:@"Esqueci minha senha." attributes:attrs2];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] init];
    [string appendAttributedString:nameStr];
    [string appendAttributedString:countStr];
    self.lbMessage.attributedText = string;
}
- (void)resetFrame
{
    CGSize labelTextSize = [self.lbMessage.text boundingRectWithSize:CGSizeMake(self.lbMessage.frame.size.width, MAXFLOAT)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{
                                                                                  NSFontAttributeName : self.lbMessage.font
                                                                                  }
                                                                        context:nil].size;
    CGRect rect = self.lbMessage.frame;
    rect.size.height = labelTextSize.height;
    rect.origin.y = CGRectGetMinY(self.btnClose.frame) - CGRectGetHeight(rect) - 30;
    [self.lbMessage setFrame:rect];
}
- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    UILabel *textLabel = (UILabel *)recognizer.view;
    CGPoint tapLocation = [recognizer locationInView:textLabel];
    
    // init text storage
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:textLabel.attributedText];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [textStorage addLayoutManager:layoutManager];
    
    // init text container
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(textLabel.frame.size.width, textLabel.frame.size.height+100) ];
    textContainer.lineFragmentPadding  = 0;
    textContainer.maximumNumberOfLines = textLabel.numberOfLines;
    textContainer.lineBreakMode        = textLabel.lineBreakMode;
    
    [layoutManager addTextContainer:textContainer];
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:tapLocation
                                                      inTextContainer:textContainer
                             fractionOfDistanceBetweenInsertionPoints:NULL];
    NSString* str = @"Esqueci minha senha.";
    if (characterIndex >= self.lbMessage.text.length - str.length - 1)
    {
        [self gotoForgotPass];
    }
   
}
- (void)gotoForgotPass
{
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(onForgotPassPressed)])
        {
            [self.delegate onForgotPassPressed];
        }
    }];
}
@end

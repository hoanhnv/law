//
//  TermAndServiceViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/29/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "TermAndServiceViewController.h"
#import "MainService.h"

@interface TermAndServiceViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UILabel *lbContent;
@property (strong,nonatomic) NSString *strContent;
@end

@implementation TermAndServiceViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadData];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Termos";
    self.webContent.frame = CGRectMake(self.webContent.frame.origin.x, self.webContent.frame.origin.y,self.webContent.frame.size.width , self.webContent.frame.size.height + 50);
    self.tabBarController.tabBar.hidden = YES;
}
-(void)updateView{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.hyphenationFactor = 1;
    paragraphStyle.alignment = NSTextAlignmentJustified;
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleBody],
                                 NSParagraphStyleAttributeName: paragraphStyle
                                 };
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.strContent attributes:attributes];
    
    [self.webContent loadHTMLString:self.strContent baseURL:nil];
    [self.webContent reload];
    
    //    self.lbContent.attributedText = attributedString;
    //    [self.lbContent sizeToFit];
    //    self.mainScroll.contentSize = CGSizeMake(1,CGRectGetMaxY(self.lbContent.frame));
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchTermAndService:^(NSInteger errorCode, NSString *message, id data) {
        if (data != nil) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if ([data isKindOfClass:[NSDictionary class]]) {
                self.strContent = data[@"data"][@"description"];;
                [self updateView];
            }
            else{
                [self showMessage:message withPoistion:nil];
            }
            
        }
        else{
            [self showMessage:message withPoistion:nil];
        }
    }];
}
@end

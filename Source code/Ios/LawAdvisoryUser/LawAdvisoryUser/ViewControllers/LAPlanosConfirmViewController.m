//
//  LAPlanosConfirmViewController.m
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanosConfirmViewController.h"
#import "MainService.h"
#import "LASignatureInfo.h"
#import "LASignatureObj.h"
#import "LAPlanosSelectionComarcaViewController.h"
#import "LAPlanosEscolhaFormaPagementoVC.h"
#import "MainProfileViewController.h"
#import "LAPlanAddMoreComacarViewController.h"
#import "LACartItemViewCell.h"
#import "LASignatureDistrict.h"

@interface LAPlanosConfirmViewController ()<UITableViewDelegate,UITableViewDataSource>{
    LASignatureInfo *lstdata;
    LAPlanObj *prevousPlan;
    NSMutableArray *prevousDistrict;
}

@end

@implementation LAPlanosConfirmViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.tblSignature registerNib:[UINib nibWithNibName:@"LACartItemViewCell" bundle:nil] forCellReuseIdentifier:@"LACartItemViewCell"];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initUI
{
    self.title = @"Planos";
    self.scrData.frame = CGRectMake(self.scrData.frame.origin.x, self.scrData.frame.origin.y, self.scrData.frame.size.width, self.scrData.frame.size.height);
}
- (void)initData
{
    NSData *planData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_PLAN_CART_ITEM];
    prevousPlan = [NSKeyedUnarchiver unarchiveObjectWithData:planData];
    
    NSData *listDistrictData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_DISTRICT_CART_ITEM];
    prevousDistrict = [NSKeyedUnarchiver unarchiveObjectWithData:listDistrictData];
    
    if (prevousPlan == nil || prevousDistrict == nil) {
        [self loadData];
    }
    else if ([self.listDistric count] != [prevousDistrict count]) {
        [self loadData];
    }
    else{
        BOOL bSame = YES;
        for (int i=0;i< [self.listDistric count]; i++ ) {
            LADistrictObj *obj1 = self.listDistric[i];
            LADistrictObj *obj2 = prevousDistrict[i];
            if (![obj1.name isEqualToString:obj2.name] && obj1 != nil && obj2 != nil) {
                bSame = NO;
            }
        }
        if (bSame == NO) {
            [self loadData];
        }
        else{
            [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_CART_ITEM];
            NSData *listSignatureData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_CART_ITEM];
            self.arrSignature = [NSKeyedUnarchiver unarchiveObjectWithData:listSignatureData];
            [self reloadData];
        }
    }
}
- (void)reloadData
{
    self.planosName.text = [LAUtilitys getDefaultString:self.lanObj.name];
    self.planosTotalPlanSinging.text = [NSString stringWithFormat:@"Valor Total: R$ %@",[LAUtilitys getCurrentcyFromDecimal:[self getTotalValue]]];
    [self.tblSignature reloadData];
    [self setupFrame];
}

- (void)setupFrame
{
    self.tblSignature.frame = CGRectMake(self.tblSignature.frame.origin.x, self.tblSignature.frame.origin.y, self.tblSignature.frame.size.width, [self.arrSignature count]*97);
    
    self.planosTotalPlanSinging.frame = CGRectMake(self.planosTotalPlanSinging.frame.origin.x, CGRectGetMaxY(self.tblSignature.frame) + 15, self.planosTotalPlanSinging.frame.size.width, self.planosTotalPlanSinging.frame.size.height);
    
    self.viewInBottom.frame = CGRectMake(self.viewInBottom.frame.origin.x, CGRectGetMaxY(self.planosTotalPlanSinging.frame) + 7, self.viewInBottom.frame.size.width,self.viewInBottom.frame.size.height);
    self.scrData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.viewInBottom.frame) + 50);
}

- (IBAction)onConfirmPressed:(id)sender
{
    LAPlanosEscolhaFormaPagementoVC* pagementoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosEscolhaFormaPagementoVC class])];
    pagementoVC.lanObj = self.lanObj;
    pagementoVC.listDistric = self.listDistric;
    pagementoVC.arrSignature = self.arrSignature;
    [self.navigationController pushViewController:pagementoVC animated:YES];
}
- (IBAction)onAdditionPressed:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAPlanAddMoreComacarViewController *addMoreVC = [storyboard instantiateViewControllerWithIdentifier:@"LAPlanAddMoreComacarViewController"];
    addMoreVC.lanObj = self.lanObj;
    addMoreVC.arrSignature = self.arrSignature;
    
    addMoreVC.listDistric = self.listDistric;
    
    [self.navigationController pushViewController:addMoreVC animated:YES];
}

-(void)loadData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableArray* listIDs = [NSMutableArray new];
    if (self.listDistric.count)
    {
        for (LADistrictObj* obj in self.listDistric)
        {
            [listIDs addObject:[NSString stringWithFormat:@"%0.0f",obj.dataIdentifier]];
        }
    }
    
    [MainService lawSignatures:self.lanObj.dataIdentifier withDistricts:listIDs withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        lstdata = [[LASignatureInfo alloc]initWithDictionary:data];
        if (lstdata.data) {
            NSData *planData = [NSKeyedArchiver archivedDataWithRootObject:self.lanObj];
            [[NSUserDefaults standardUserDefaults]setObject:planData forKey:CURRENT_PLAN_CART_ITEM];
            
            NSData *listDistrictData = [NSKeyedArchiver archivedDataWithRootObject:self.listDistric];
            [[NSUserDefaults standardUserDefaults]setObject:listDistrictData forKey:CURRENT_DISTRICT_CART_ITEM];
            
            NSData *signatureData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_CART_ITEM];
            NSMutableArray *arrSignature = [NSKeyedUnarchiver unarchiveObjectWithData:signatureData];
            
            if (![arrSignature count]) {
                arrSignature = [NSMutableArray array];
            }
            BOOL bExist = NO;
            for (LASignatureObj *signInfo in arrSignature ) {
                if (signInfo.dataIdentifier == lstdata.data.dataIdentifier) {
                    bExist = YES;
                }
            }
            if (lstdata.data && !bExist) {
                [arrSignature addObject:lstdata.data];
            }
            self.arrSignature = arrSignature;
            
            NSData *listSignatureData = [NSKeyedArchiver archivedDataWithRootObject:arrSignature];
            [[NSUserDefaults standardUserDefaults]setObject:listSignatureData forKey:CURRENT_SIGNATURE_CART_ITEM];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self reloadData];
            
        }
        else{
            [self.listDistric removeLastObject];
            [self showMessage:message withPoistion:nil];
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrSignature count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LACartItemViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LACartItemViewCell" forIndexPath:indexPath];
    LASignatureObj *signatureObj = [self.arrSignature objectAtIndex:indexPath.row];
    [cell setupCell:signatureObj andIndexPath:indexPath andLan:self.lanObj];
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath isEqual:((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject])]){
        [self setupFrame];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 97.0f;
}

-(float)getTotalValue{
    LASignatureObj *signature = [self.arrSignature objectAtIndex:0];
    float totalValue = signature.valuePlanOnSigning;
    for (int i=1;i<[self.arrSignature count];i++) {
        signature = self.arrSignature[i];
        totalValue += signature.valuePlanOnSigning*(1-self.lanObj.percent_by_district/100.0f);
    }
    return totalValue;
}


@end

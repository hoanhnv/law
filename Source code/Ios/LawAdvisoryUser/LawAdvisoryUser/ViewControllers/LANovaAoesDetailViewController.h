//
//  LANovaAoesDetailViewController.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LACustomerOpenObj.h"
#import "LAMedias.h"
#import "LAListRefusal.h"
@interface LANovaAoesDetailViewController : LABaseViewController

{
    IBOutlet UITableView* tbData;
    NSMutableArray* arrTitleHeader;
    NSMutableArray* arrDocuments;
    NSMutableArray* arrAudio;
    NSInteger idPlay;
}
@property (nonatomic) LAListRefusal *lstRefusal;
@property (nonatomic) NSInteger type;
@property (nonatomic) LACustomerOpenObj* objData;

@end

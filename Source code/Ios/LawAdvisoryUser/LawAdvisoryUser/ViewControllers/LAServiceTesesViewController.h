//
//  ServiceTesesViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LATesesMainTableViewCell.h"
#import "LASelectedTheseViewController.h"
#import "LAPlanObj.h"

@interface LAServiceTesesViewController : LABaseViewController<UITableViewDelegate,UITableViewDataSource,LATesesMainTableViewCellDelegate>{
    
}
@property (nonatomic) LAPlanObj* lanObj;

@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@property (strong, nonatomic) NSMutableArray *selectedTheseData;
@property (strong, nonatomic) NSMutableArray *buyTheseData;
@property (strong, nonatomic) NSMutableArray *allTheseData;
@property (assign, nonatomic) NSInteger numberCanSelected;

@end

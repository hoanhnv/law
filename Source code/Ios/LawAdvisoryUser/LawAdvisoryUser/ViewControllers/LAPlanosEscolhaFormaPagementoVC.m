//
//  LAPlanosEscolhaFormaPagementoVC.m
//  JustapLawyer
//
//  Created by MAC on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanosEscolhaFormaPagementoVC.h"
#import "LAPlanosSelectionComarcaViewController.h"
#import "LAPlanosBoletoViewController.h"
#import "LAPagarCatartaoViewController.h"
#import "MainProfileViewController.h"
#import "LACartItemViewCell.h"
#import "LAAddNewComarcaViewController.h"
#import "LASignatureInfo.h"
#import "MainService.h"
#import "AppDelegate.h"

#define kTEXT_POSSUEM @"Comarcas adicionais possuem %.0f %% de desconto."

@interface LAPlanosEscolhaFormaPagementoVC (){
    LASignatureInfo *lstdata;
    LAPlanObj *prevousPlan;
    NSMutableArray *prevousDistrict;
}
@end

@implementation LAPlanosEscolhaFormaPagementoVC
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"Planos";
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    
    self.lbPlanosName.text = [LAUtilitys getDefaultString:self.lanObj.name];
    NSString *strFirst = [LAUtilitys getDefaultString:self.lanObj.name];
    NSString *strSecond = [NSString stringWithFormat:@"%.0f%% das teses",self.lanObj.quantityOfTheses];
    
    NSString *strPlanInfo = [NSString stringWithFormat:@"%@ %@",strFirst,strSecond];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strPlanInfo];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont robotoRegular:12]
                             range:[strPlanInfo rangeOfString:strFirst]];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont robotoRegular:12]
                             range:[strSecond rangeOfString:strSecond]];
    
    self.lbPlanosName.attributedText = attributedString;
    [self.lbPlanosName sizeToFit];
    self.lbTotalPlanSinging.text = [NSString stringWithFormat:@"Valor Total: R$ %@",[LAUtilitys getCurrentcyFromDecimal:[self getTotalValue]]];
    self.lbPercentName.text = [NSString stringWithFormat:kTEXT_POSSUEM,self.lanObj.percent_by_district];
    [self.tblSignature reloadData];
    [self setupFrame];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tblSignature registerNib:[UINib nibWithNibName:@"LACartItemViewCell" bundle:nil] forCellReuseIdentifier:@"LACartItemViewCell"];
    self.scrFullData.frame = CGRectMake(self.scrFullData.frame.origin.x, self.scrFullData.frame.origin.y, self.scrFullData.frame.size.width, self.scrFullData.frame.size.height + 50);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI
{
    
}
- (void)initData
{
    NSData *planData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_PLAN_CART_ITEM];
    prevousPlan = [NSKeyedUnarchiver unarchiveObjectWithData:planData];
    
    NSData *listDistrictData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_DISTRICT_CART_ITEM];
    prevousDistrict = [NSKeyedUnarchiver unarchiveObjectWithData:listDistrictData];
    
    if (prevousPlan == nil || prevousDistrict == nil) {
        [self loadData];
    }
    else if ([self.listDistric count] != [prevousDistrict count]) {
        [self loadData];
    }
    else{
        BOOL bSame = YES;
        for (int i=0;i< [self.listDistric count]; i++ ) {
            LADistrictObj *obj1 = self.listDistric[i];
            LADistrictObj *obj2 = prevousDistrict[i];
            if (![obj1.name isEqualToString:obj2.name] && obj1 != nil && obj2 != nil) {
                bSame = NO;
            }
        }
        if (bSame == NO) {
            [self loadData];
        }
        else{
            [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_CART_ITEM];
            NSData *listSignatureData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_CART_ITEM];
            self.arrSignature = [NSKeyedUnarchiver unarchiveObjectWithData:listSignatureData];
            [self reloadData];
        }
    }
}
- (void)reloadData
{
    //    self.lbPlanosName.text = [LAUtilitys getDefaultString:self.lanObj.name];
    //    LASignatureObj *lastSignature = [self.arrSignature lastObject];
    self.lbTotalPlanSinging.text = [NSString stringWithFormat:@"Valor Total: R$ %@",[LAUtilitys getCurrentcyFromDecimal:[self getTotalValue]]];
    [self.tblSignature reloadData];
    [self setupFrame];
}
-(void)loadData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableArray* listIDs = [NSMutableArray new];
    if (self.listDistric.count)
    {
        for (LADistrictObj* obj in self.listDistric)
        {
            [listIDs addObject:[NSString stringWithFormat:@"%0.0f",obj.dataIdentifier]];
        }
    }
    
    [MainService lawSignatures:self.lanObj.dataIdentifier withDistricts:listIDs withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        lstdata = [[LASignatureInfo alloc]initWithDictionary:data];
        if (lstdata.success) {
            NSData *planData = [NSKeyedArchiver archivedDataWithRootObject:self.lanObj];
            [[NSUserDefaults standardUserDefaults]setObject:planData forKey:CURRENT_PLAN_CART_ITEM];
            
            NSData *listDistrictData = [NSKeyedArchiver archivedDataWithRootObject:self.listDistric];
            [[NSUserDefaults standardUserDefaults]setObject:listDistrictData forKey:CURRENT_DISTRICT_CART_ITEM];
            
            NSData *signatureData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_CART_ITEM];
            NSMutableArray *arrSignature = [NSKeyedUnarchiver unarchiveObjectWithData:signatureData];
            
            if (![arrSignature count]) {
                arrSignature = [NSMutableArray array];
            }
            BOOL bExist = NO;
            for (LASignatureObj *signInfo in arrSignature ) {
                if (signInfo.dataIdentifier == lstdata.data.dataIdentifier) {
                    bExist = YES;
                }
            }
            if (lstdata.data && !bExist) {
                [arrSignature addObject:lstdata.data];
            }
            self.arrSignature = arrSignature;
            
            NSData *listSignatureData = [NSKeyedArchiver archivedDataWithRootObject:arrSignature];
            [[NSUserDefaults standardUserDefaults]setObject:listSignatureData forKey:CURRENT_SIGNATURE_CART_ITEM];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self reloadData];
            
        }
        else{
            [self.listDistric removeLastObject];
            //            [self.listDistric removeLastObject];
            [self showMessage:lstdata.message withPoistion:nil];
        }
    }];
}

- (void)setupFrame
{
    self.tblSignature.frame = CGRectMake(self.tblSignature.frame.origin.x, self.tblSignature.frame.origin.y, self.tblSignature.frame.size.width, [self.arrSignature count]*77);
    
    self.lbTotalPlanSinging.frame = CGRectMake(self.lbTotalPlanSinging.frame.origin.x, CGRectGetMaxY(self.tblSignature.frame) + 15, self.lbTotalPlanSinging.frame.size.width, self.lbTotalPlanSinging.frame.size.height);
    
    self.vSubBottom.frame = CGRectMake(self.vSubBottom.frame.origin.x, CGRectGetMaxY(self.lbTotalPlanSinging.frame) + 7, self.vSubBottom.frame.size.width,self.vSubBottom.frame.size.height);
    
    self.scrFullData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.vSubBottom.frame) + 50);
}

- (IBAction)onCreditcardPressed:(id)sender
{
    UIStoryboard *devStoryboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAPagarCatartaoViewController *creditCardVC = [devStoryboard instantiateViewControllerWithIdentifier:@"LAPagarCatartaoViewController"];
    creditCardVC.hidesBottomBarWhenPushed = YES;
    creditCardVC.lanObj = self.lanObj;
    creditCardVC.listDistric = self.listDistric;
    creditCardVC.arrSignature = self.arrSignature;
    [self.navigationController pushViewController:creditCardVC animated:YES];
}

- (IBAction)onBankingPressed:(id)sender
{
    UIStoryboard *devStoryboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAPlanosBoletoViewController *bankVC = [devStoryboard instantiateViewControllerWithIdentifier:@"LAPlanosBoletoViewController"];
    bankVC.lanObj = self.lanObj;
    bankVC.listDistric = self.listDistric;
    bankVC.arrSignature = self.arrSignature;
    
    [self.navigationController pushViewController:bankVC animated:YES];
}

- (IBAction)onAdicionComarcaPressed:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAAddNewComarcaViewController *planAddNew = [storyboard instantiateViewControllerWithIdentifier:@"LAAddNewComarcaViewController"];
    planAddNew.lanObj = self.lanObj;
    planAddNew.listDistric = self.listDistric;
    planAddNew.arrSignature = self.arrSignature;
    
    MainProfileViewController* mainVC = nil;
    NSArray* arrVC = self.navigationController.viewControllers;
    for (UIViewController* vc  in arrVC) {
        if ([vc isKindOfClass:[MainProfileViewController class]])
        {
            mainVC = (MainProfileViewController*)vc;
            break;
        }
    }
    if (mainVC)
    {
        mainVC.addNewComarcaVC = planAddNew;
        [mainVC  gotoIndex:0];
        [self.navigationController popToViewController:mainVC animated:YES];
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrSignature count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LACartItemViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LACartItemViewCell" forIndexPath:indexPath];
    [cell setupCell:self.arrSignature[indexPath.row] andIndexPath:indexPath andLan:self.lanObj];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77.0f;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath isEqual:((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject])]){
        [self setupFrame];
    }
}

-(float)getTotalValue{
    float totalValue = self.lanObj.value;
    if ([AppDelegate sharedDelegate].isDiscountAll) {
        totalValue = self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    for (int i=1;i<[self.arrSignature count];i++) {
        totalValue += self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    return totalValue;
}


@end

//
//  LAStatusAccountViewController.h
//  JustapLawyer
//
//  Created by MAC on 9/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAStatusAccountObj.h"
@interface LAStatusAccountViewController : LABaseViewController

@property (nonatomic) LAStatusAccountObj* objStatusAccount;

@property (weak, nonatomic) IBOutlet UIButton *btnBackToLogin;
@property (assign, nonatomic) BOOL fromRegister;

- (IBAction)doBackToLogin:(id)sender;
@end

//
//  LANovasPoupViewController.m
//  JustapLawyer
//
//  Created by Mac on 2/9/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import "LANovasPoupViewController.h"
#import "LAHTTPClient.h"
#import "MBProgressHUD.h"
#import "MainService.h"
#import "LAActionResponseInfo.h"

@interface LANovasPoupViewController ()

@end

@implementation LANovasPoupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self loadData];
    // Do any additional setup after loading the view.
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)doAccept:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate onAccept:self.objData];
    }];
}

- (IBAction)doRefuse:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate onRefuse:self.lstRefusal withIndex:self.indexPath];
    }];
    
}

- (IBAction)doClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)loadData {
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [MainService getActionById:self.objData.dataIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data){
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if(data && [data isKindOfClass:[NSDictionary class]]){
                
                LAActionResponseInfo *response = [[LAActionResponseInfo alloc]initWithDictionary:data];
                NSDictionary *dictData = [data objectForKey:@"data"];
                if (response.success) {
                    NSString *userName = [dictData objectForKey:@"lawyer"];
                    NSString *initValue = [dictData objectForKey:@"initial_fees"];
                    NSString *value = [dictData objectForKey:@"rate_of_operation_justap"];
                    NSString *Percent = [dictData objectForKey:@"financial_rate"];
                    NSString *Total = [dictData objectForKey:@"total_receivable"];
                    NSString *percentSuccess = [dictData objectForKey:@"percentage_in_success"];
                    self.lbUserName.text = [NSString stringWithFormat:@"Parabéns,você acaba de ser contratado(a) por %@.",userName];
                    self.lbInitValue.text = [NSString stringWithFormat:@"R$ %@",initValue];
                    self.lbValue.text = [NSString stringWithFormat:@"R$ %@",value];
                    self.lbTotal.text = [NSString stringWithFormat:@"Total a receber R$ %@",Total];
                    self.lbPercentSuccess.text = [NSString stringWithFormat:@"Percentual no êxito %@ %%",percentSuccess];
//                    self.lbPercentSuccess.text = [NSString stringWithFormat:@"Percentual no êxito %@ %%",percentSuccess];
                    [MainService getPercentAction:^(NSInteger errorCode2, NSString *message2, id data2){
                        if(data && [data isKindOfClass:[NSDictionary class]]){
                            LAActionResponseInfo *response2 = [[LAActionResponseInfo alloc]initWithDictionary:data];
                            NSDictionary *dictData2 = data2;
                            if (response2.success){
                                NSString *newPercent = [dictData2 objectForKey:@"data"];
                                self.lbPercent.text = [NSString stringWithFormat:@"R$ %@ (%@%% dos honorários iniciais)",Percent,newPercent];
                                
                            }
                            else{
                                [self showMessage:response2.message withPoistion:nil];
                            }
                        }
                        else{
                            [self showMessage:response.message withPoistion:nil];
                        }
                    }];
                    
                }
                else{
                    [self showMessage:response.message withPoistion:nil];
                }
            }
            else{
                [self showMessage:message withPoistion:nil];
            }
        }];
//    [[LAHTTPClient sharedClient] GET:strBuilPath parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
//        
//    }success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]){
//            id data = [responseObject objectForKey:@"data"];
//            if (data && [data isKindOfClass:[NSDictionary class]]) {
//                NSString *userName = [data objectForKey:@"lawyer"];
//                NSString *initValue = [data objectForKey:@"initial_fees"];
//                NSString *value = [data objectForKey:@"rate_of_operation_justap"];
//                NSString *Percent = [data objectForKey:@"financial_rate"];
//                NSString *Total = [data objectForKey:@"total_receivable"];
//                NSString *percentSuccess = [data objectForKey:@"percentage_in_success"];
//                self.lbUserName.text = [NSString stringWithFormat:@"Parabéns,você acaba de ser contratado(a) por %@.",userName];
//                self.lbInitValue.text = [NSString stringWithFormat:@"R$ %@",initValue];
//                self.lbValue.text = [NSString stringWithFormat:@"R$ %@",value];
//                self.lbPercent.text = [NSString stringWithFormat:@"R$ %@ (1%% dos honorários iniciais)",Percent];
//                self.lbTotal.text = [NSString stringWithFormat:@"Total a receber R$ %@",Total];
//                self.lbPercentSuccess.text = [NSString stringWithFormat:@"Percentual no êxito %@ %%",percentSuccess];
//                [MBProgressHUD hideHUDForView:self.view animated:true];
//            }
//        }
//        
//    }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSString *errorStr = error.localizedDescription;
//        [self showMessage:errorStr withPoistion:nil];
//        [MBProgressHUD hideHUDForView:self.view animated:true];
//    }];

}
@end

//
//  LAWitnessDetailViewController.m
//  JustapLawyer
//
//  Created by Mac on 11/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAWitnessDetailViewController.h"

@interface LAWitnessDetailViewController ()

@end

@implementation LAWitnessDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    self.lbName.text = self.witnessObj.name;
}
- (IBAction)doCloseView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
@end

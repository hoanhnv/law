//
//  LATesesMainViewController.m
//  JustapLawyer
//
//  Created by Mac on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LATesesMainViewController.h"
#import "LAServiceTesesViewController.h"
#import "LAPlanosSelectionComarcaViewController.h"
#import "LAPricePlanosViewController.h"
#import "AppDelegate.h"
@interface LATesesMainViewController ()

@end

@implementation LATesesMainViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onConfirmGotoPrice) name:@"GotoPriceConfirm" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoServiceTeses) name:@"gotoServiceTeses" object:nil];
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GotoPriceConfirm" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gotoServiceTeses" object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Teses";
    [self initUI12];
}

- (void)initUI12
{
    // Tying up the segmented control to a scroll view
    //    self.pageControl.iNumberPage = arrSlider.count;
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    self.segmentedControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 50)];
    self.segmentedControl.isCheck = NO;
    //    self.segmentedControl.sectionTitles = @[@"TESES", @"PRECIFICAÇÃO", @"ASSINE"];
    self.segmentedControl.sectionTitles = @[@"TESES", @"PRECIFICAÇÃO"];
    self.segmentedControl.backgroundColor = [LAUtilitys jusTapColor];
    self.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};;
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl.tag = 400;
    self.segmentedControl.userDraggable = NO;
    self.segmentedControl.type = HMSegmentedControlTypeText;
    self.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    [self.view addSubview:self.segmentedControl];
    
    [self.segmentedControl setSelectedSegmentIndex:0 animated:YES];
    [self.segmentedControl setFrame:CGRectMake(0, 0, viewWidth, 50)];
    self.segmentedControl.selectedSegmentIndex = 0;
    [self  gotoIndex:0];
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf gotoIndex:index];
    }];
}
- (void)onConfirmGotoPrice
{
    self.segmentedControl.selectedSegmentIndex = 1;
    [self performSelector:@selector(gotoPrice) withObject:nil afterDelay:0.2];
}
- (void)gotoServiceTeses
{
    self.segmentedControl.selectedSegmentIndex = 0;
    [self performSelector:@selector(gotoServiceTesesBack) withObject:nil afterDelay:0.2];
}
- (void)gotoServiceTesesBack
{
    [self resetStateAddNewComarca];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LAServiceTesesViewController *serviceTesesVC = [storyboard instantiateViewControllerWithIdentifier:@"LAServiceTesesViewController"];
    [self.mainView addSubview:serviceTesesVC.view];
    [self addChildViewController:serviceTesesVC];
    [serviceTesesVC didMoveToParentViewController:self];
}
- (void)gotoPrice
{
    self.segmentedControl.selectedSegmentIndex = 1;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LAPricePlanosViewController* priceVC = [storyboard instantiateViewControllerWithIdentifier:@"LAPricePlanosViewController"];
    [self.mainView addSubview:priceVC.view];
    [self addChildViewController:priceVC];
    [priceVC didMoveToParentViewController:self];
}
#pragma mark - UIScrollViewDelegate
- (void)gotoIndex:(NSInteger)index
{
    if (index == 0)
    {
        [self resetStateAddNewComarca];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LAServiceTesesViewController *serviceTesesVC = [storyboard instantiateViewControllerWithIdentifier:@"LAServiceTesesViewController"];
        serviceTesesVC.lanObj = self.lanObj;
        serviceTesesVC.buyTheseData = self.buyThese;
        [self.mainView addSubview:serviceTesesVC.view];
        [self addChildViewController:serviceTesesVC];
        [serviceTesesVC didMoveToParentViewController:self];
    }
    else if (index == 1) {
        if ([AppDelegate sharedDelegate].selectedTheseData.count)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LAPricePlanosViewController* priceVC = [storyboard instantiateViewControllerWithIdentifier:@"LAPricePlanosViewController"];
            [self.mainView addSubview:priceVC.view];
            [self addChildViewController:priceVC];
            [priceVC didMoveToParentViewController:self];
        }
        else
        {
            [self showMessage:MSG_021 withPoistion:nil];
            return;
        }
        
    }
    else if(index == 2)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LAPlanosSelectionComarcaViewController *planVC = [storyboard instantiateViewControllerWithIdentifier:@"LAPlanosSelectionComarcaViewController"];
        [self.mainView addSubview:planVC.view];
        [self addChildViewController:planVC];
        [planVC didMoveToParentViewController:self];
    }
}

-(void)resetStateAddNewComarca{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_PLAN_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_DISTRICT_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_SIGNATURE_CART_ITEM];
}
@end

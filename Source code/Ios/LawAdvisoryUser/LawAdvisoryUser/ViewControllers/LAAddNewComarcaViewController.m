//
//  LAAddNewComarcaViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAddNewComarcaViewController.h"
#import "LACountryViewController.h"
#import "LAMainTabbarViewController.h"
#import "AppDelegate.h"
#import "MainService.h"
#import "LAPlanosSelectionViewController.h"
#import "LAListDistrictByUF.h"
#import "LAPlanObj.h"
#import "LAPlanosConfirmViewController.h"
#import "LASignatureObj.h"
#import "LADistrictObj.h"
#import "LAAssinatureObj.h"
#import "LAPlanosEscolhaFormaPagementoVC.h"
#import "LASignatureObj.h"
#import "LADistrictObj.h"
#import "LAAssinatureBuyObj.h"
#import "LASignatureDistrict.h"
#import "LADistricts.h"
#import "LAThesisBuy.h"

#define PLANOS_BOUGHT @"Você já possui um plano Parceiro %@ ativo.Para adicionar mais comarcas,selecione abaixo.Caso prefira alterar seu plano,vá até ASSINATURA, cancele o plano atual e assine um novo."

@interface LAAddNewComarcaViewController ()
{
    LAAssinatureBuyObj *planSignature;
    
}
@end

@implementation LAAddNewComarcaViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    [self setupFrame];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.scrFullData.frame  = CGRectMake(self.scrFullData.frame.origin.x, self.scrFullData.frame.origin.y, self.scrFullData.frame.size.width, self.scrFullData.frame.size.height - 114);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initUI
{
    self.navigationItem.title = @"Planos";
    
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtCity];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtEstado];
    self.arrRequiredField = [NSMutableArray arrayWithObjects:self.txtEstado,self.txtCity, nil];
}

- (void)setupFrame
{
    NSData *registerData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_REGISTER];
    planSignature = [NSKeyedUnarchiver unarchiveObjectWithData:registerData];
    
    float mY = 0;
    if (self.assinatureInfo.data && self.assinatureInfo.data.plan != nil && self.assinatureInfo.data.plan.dataIdentifier>0) {
        self.listOldDistrictSelected = [self arrDistrictObj:self.assinatureInfo.data.districts];
        self.lbPlanosRegisted.text = [NSString stringWithFormat:PLANOS_BOUGHT,planSignature.plan.name];
        self.lbPlanosRegisted.numberOfLines = 0;
        [self.lbPlanosRegisted sizeToFit];
        mY = CGRectGetMaxY(self.lbPlanosRegisted.frame);
        self.listOldDistrictSelected = [self arrDistrictObj:planSignature.districts];
    }
    else{
        if (planSignature && ![LAUtilitys NullOrEmpty:planSignature.plan.name]) {
            self.lbPlanosRegisted.text = [NSString stringWithFormat:PLANOS_BOUGHT,planSignature.plan.name];
            self.lbPlanosRegisted.numberOfLines = 0;
            [self.lbPlanosRegisted sizeToFit];
            mY = CGRectGetMaxY(self.lbPlanosRegisted.frame);
            self.listOldDistrictSelected = [self arrDistrictObj:planSignature.districts];
        }
        else{
            self.lbPlanosRegisted.frame = CGRectMake(self.lbPlanosRegisted.frame.origin.x, self.lbPlanosRegisted.frame.origin.y, self.lbPlanosRegisted.frame.size.width, 0);
        }
    }
    
    self.vTopHeader.frame = CGRectMake(self.vTopHeader.frame.origin.x,mY , self.vTopHeader.frame.size.width, self.vTopHeader.frame.size.height);
    
    self.lbComacar.frame = CGRectMake(self.lbComacar.frame.origin.x,CGRectGetMaxY(self.vTopHeader.frame) , self.lbComacar.frame.size.width, self.lbComacar.frame.size.height);
    
    [self.lbComacar sizeToFit];
    
    self.vBottom.frame = CGRectMake(self.vBottom.frame.origin.x,CGRectGetMaxY(self.lbComacar.frame) + 34 , self.vBottom.frame.size.width, self.vBottom.frame.size.height);
    
    self.btConfirm.frame = CGRectMake(self.btConfirm.frame.origin.x, CGRectGetMaxY(self.vBottom.frame) + 28, self.btConfirm.frame.size.width,self.btConfirm.frame.size.height );
    
    self.scrFullData.contentSize = CGSizeMake(1, CGRectGetMaxY(self.btConfirm.frame) + 50);
}

- (void)initData
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didselectAddress:) name:@"didSelectCountry" object:nil];
    [self getUFs];
    [self updateTextComarcaIfNeed:self.listDistric];
}

-(void)updateTextComarcaIfNeed:(NSMutableArray*)listDistrict{
    NSString* strDistrict = @"";
    for (int i = 0; i < self.listDistric.count ; i++)
    {
        LADistrictObj* obj = [self.listDistric objectAtIndex:i];
        if (i == 0 && self.listDistric.count > 1)
        {
            strDistrict = [obj.name stringByAppendingString:@"\n"];
        }
        else
            if (i != self.listDistric.count - 1)
            {
                strDistrict = [NSString stringWithFormat:@"%@%@\n",strDistrict,obj.name];
            }
            else
            {
                strDistrict = [NSString stringWithFormat:@"%@%@",strDistrict,obj.name];
            }
    }
    self.lbComacar.text = [LAUtilitys getDefaultString:strDistrict];
    [self.lbComacar sizeToFit];
    self.lbTotal.text = [NSString stringWithFormat:@"R$ %@",[LAUtilitys getCurrentcyFromDecimal:[self getTotalValue]]];
    
}
-(void)getUFs{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchUFs:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        listUFS = [[LAUFs alloc]initWithDictionary:data];
    }];
}
- (IBAction)onShowCity:(id)sender
{
    if ([LAUtilitys NullOrEmpty:self.txtEstado.text])
    {
        [self.txtEstado becomeFirstResponder];
        [self onShowState:nil];
    }
    else
    {
        [self showCityList:TYPE_NEWCITY];
    }
    
}
- (BOOL)checkIsValid
{
    BOOL isCheck = YES;
    for (AwesomeTextField* txt in self.arrRequiredField)
    {
        if ([LAUtilitys isEmptyOrNull:txt.text])
        {
            [self showMessage:[NSString stringWithFormat:@"%@ not empty",txt.placeholder] withPoistion:nil];
            isCheck = NO;
            break;
        }
    }
    return isCheck;
}
- (IBAction)onShowState:(id)sender
{
    listEstado = [NSMutableArray arrayWithArray:listUFS.data];
    listEstado = [NSMutableArray arrayWithObjects:@"DF",@"RJ",@"SP", nil];
    [self showCityList:TYPE_STATE];
}
- (void)didselectedList:(NSMutableArray*)listSelected
{
    self.listDistrictSelected = listSelected;
}
- (void)showCityList:(TYPE_POPUP)type
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LACountryViewController* countryVC = [storyboard instantiateViewControllerWithIdentifier:@"LACountryViewController"];
    countryVC.isShouldBackButton = NO;
    if (type == TYPE_NEWCITY)
    {
        NSMutableArray *arrTemp = [NSMutableArray array];
        for (LADistrictObj*obj in listCity) {
            
            for (LADistrictObj *item in self.listOldDistrictSelected) {
                if ([obj.name isEqualToString:item.name]) {
                    [arrTemp addObject:obj];
                }
            }
        }
        for (LADistrictObj*obj in listCity) {
            for (LADistrictObj *item in self.listDistric) {
                if ([obj.name isEqualToString:item.name]) {
                    [arrTemp addObject:obj];
                }
            }
        }
        [listCity removeObjectsInArray:arrTemp];
        countryVC.arrCountry = listCity;
        countryVC.currentDistrict = self.currentDistrict;
        //        countryVC.isMultipleSelect = YES;
        countryVC.arrSelected = self.listDistrictSelected;
    }
    else
    {
        countryVC.arrCountry = listEstado;
        countryVC.currentState = self.currentState;
    }
    countryVC.delegate = self;
    countryVC.popType = type;
    countryVC.isTransparentNavigation = YES;
    
    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:countryVC];
    [self presentViewController:navi animated:YES completion:nil];
}

- (void)getCityList
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchDistrictByUF:self.txtEstado.text withBlock:^(NSInteger errorCode, NSString *message, id data) {
        self.txtCity.text = STRING_EMPTY;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        lstData = [[LAListDistrictByUF alloc]initWithDictionary:data];
        if (data) {
            listCity = [NSMutableArray arrayWithArray:lstData.data];
            [listCity removeObjectsInArray:self.listDistric];
        }
    }];
}
- (void)didselectAddress:(NSNotification*)notif
{
    id obj = [notif object];
    if (obj && [obj isKindOfClass:[LADistrictObj class]])
    {
        self.currentDistrict = obj;
        self.txtCity.text = self.currentDistrict.name;
    }
    else{
        long typePoup = [[[notif userInfo] objectForKey:@"TYPEPOUP"] integerValue];
        if (typePoup == TYPE_STATE) {
            self.currentState = obj;
            self.txtEstado.text = self.currentState;
            [self getCityList];
        }
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txtEstado || textField == self.txtCity)
    {
        return NO;
    }
    return YES;
}
- (IBAction)onConfirm:(id)sender
{
    if ([self checkIsValid])
    {
        if (self.currentDistrict)
        {
            self.listDistrictSelected = [NSMutableArray arrayWithObject:self.currentDistrict];
        }
        
        BOOL isContains = NO;
        for (LADistrictObj *district in self.listDistric) {
            if ([district.name isEqualToString:self.currentDistrict.name]) {
                isContains = YES;
            }
        }
        if (!isContains) {
            [self.listDistric addObject:self.currentDistrict];
        }
        UIStoryboard *storboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LAPlanosEscolhaFormaPagementoVC *pagarVC = [storboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosEscolhaFormaPagementoVC class])];
        pagarVC.lanObj = self.lanObj;
        pagarVC.arrSignature = self.arrSignature;
        pagarVC.listDistric = self.listDistric;
        
        [self.navigationController pushViewController:pagarVC animated:YES];
    }
}
- (void) hideTabBar:(UITabBarController *) tabbarcontroller {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, screenHeight, view.frame.size.width, view.frame.size.height)];
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, screenHeight)];
        }
        
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    //    CGFloat radius = 100.0;
    CGRect frame = CGRectMake(0, 0,
                              self.tabBarController.tabBar.frame.size.width,
                              self.tabBarController.tabBar.frame.size.height);
    
    if (CGRectContainsPoint(frame, point)) {
        return YES;
    }
    return [self.view pointInside:point withEvent:event];
}

-(float)getTotalValue{
    float totalValue = self.lanObj.value;
    if ([AppDelegate sharedDelegate].isDiscountAll) {
        totalValue = self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    
    for (int i=1;i<[self.arrSignature count];i++) {
        totalValue += self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    return totalValue;
}

-(NSMutableArray*)arrDistrictObj:(NSArray*)districts{
    NSMutableArray *arrData = [NSMutableArray array];
    for (LADistricts *item in districts) {
        LADistrictObj *obj = [LADistrictObj new];
        obj.dataIdentifier = item.district.districtIdentifier;
        obj.name = item.district.name;
        obj.createdAt = item.district.createdAt;
        obj.updatedAt = item.district.updatedAt;
        obj.deletedAt = item.district.deletedAt;
        obj.uf = item.district.uf;
        [arrData addObject:obj];
    }
    return arrData;
}
@end

//
//  LASuitThesesByCateViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/4/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASuitThesesByCateViewController.h"
#import "LALawsuitTheses.h"
#import "MainService.h"
#import "MBProgressHUD.h"
#import "LASuitByCategoryCell.h"
#import "LALawsuitObj.h"
#import "LACancelamentViewController.h"
#import "UIColor+Law.h"

@interface LASuitThesesByCateViewController()<UITableViewDelegate,UITableViewDataSource> {
    LALawsuitTheses *lstData;
}

@end

@implementation LASuitThesesByCateViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShouldBackButton = YES;
        self.isShowGreenNavigation = YES;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tblData registerNib:[UINib nibWithNibName:@"LASuitByCategoryCellView" bundle:nil] forCellReuseIdentifier:@"LASuitByCategoryCellView"];
    [self fetchData:self.cateId];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = self.category.name;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:self.category.color];
}

-(void)fetchData:(double)cateId{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawsuitTheseByCategory:cateId withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        lstData = [LALawsuitTheses modelObjectWithDictionary:data];
        if (lstData.success) {
            [self.tblData reloadData];
        }
        else{
            [self showMessage:lstData.message withPoistion:nil];
        }
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [lstData.data count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LASuitByCategoryCell *cell = (LASuitByCategoryCell*)[tableView dequeueReusableCellWithIdentifier:@"LASuitByCategoryCellView"];
    
    LALawsuitObj *obj = [lstData.data objectAtIndex:indexPath.row];
    cell.cateName.text = [obj.name uppercaseString];
    NSAttributedString * attrStr = [LAUtilitys getAtributeTextFromString:obj.identification];
    cell.lbContent.attributedText = attrStr;
    
    UITapGestureRecognizer *tapSelected = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSelectCell:)];
    cell.cateName.tag = indexPath.row;
    [cell.cateName addGestureRecognizer:tapSelected];
    
    UITapGestureRecognizer *tapSelected1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSelectCell:)];
    cell.lbContent.tag = indexPath.row;
    [cell.lbContent addGestureRecognizer:tapSelected1];
    
    cell.btnNext.tag = indexPath.row;
    [cell.btnNext addTarget:self action:@selector(didSelectCell:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.cateName.textColor = [UIColor colorWithHexString:obj.category.color];
    cell.lbLine.backgroundColor = [UIColor colorWithHexString:obj.category.color];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 145.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIButton *btn = [UIButton new];
    btn.tag = indexPath.row;
    [self didSelectCell:btn];
}
-(void)tapSelectCell:(UIGestureRecognizer *)gestureRecognizer{
    NSInteger index = gestureRecognizer.view.tag;
    LALawsuitObj *obj = [lstData.data objectAtIndex:index];
    LACancelamentViewController* cancelamengoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LACancelamentViewController class])];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    cancelamengoVC.objCategory = obj;
    [self.navigationController pushViewController:cancelamengoVC animated:YES];
    // Push to View
}

-(void)didSelectCell:(UIButton*)sender{
    NSInteger index = sender.tag;
    LALawsuitObj *obj = [lstData.data objectAtIndex:index];
    LACancelamentViewController* cancelamengoVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LACancelamentViewController class])];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    cancelamengoVC.objCategory = obj;
    [self.navigationController pushViewController:cancelamengoVC animated:YES];
    // Push to View
}
@end

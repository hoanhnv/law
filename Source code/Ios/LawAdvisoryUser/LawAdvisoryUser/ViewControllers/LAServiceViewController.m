//
//  LAServiceViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAServiceViewController.h"
#import "LAPageViewControl.h"
#import "HMSegmentedControl.h"
@interface LAServiceViewController ()<UIScrollViewDelegate>
@property (nonatomic) HMSegmentedControl* segmentedControl;
@property (nonatomic) IBOutlet LAPageViewControl* pageControl;
@end

@implementation LAServiceViewController
//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super initWithCoder:aDecoder];
//    if (self)
//    {
//        self.isShouldShowBigHeightNavigationBar = YES;
//        self.isShouldShowBottombar  = YES;
//    }
//    return self;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Serviços";
    [self.navigationController.navigationBar setHeight:64.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return arrBannerObj.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view
{
    return nil;
}
- (void)initUI
{
    // Tying up the segmented control to a scroll view
    self.pageControl.iNumberPage = 3;
    //    self.pageControl.iNumberPage = arrSlider.count;
    [self.pageControl setUpImageActive:@"ic_scrubactive_category" andInActive:@"ic_scrubinactive_category"];
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    self.segmentedControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 50)];
    self.segmentedControl.sectionTitles = @[@"TESES", @"PRECIFICACÃO", @"ASINE"];
    self.segmentedControl.selectedSegmentIndex = 1;
    self.segmentedControl.backgroundColor = NAVIGATION_COLOR;
    self.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};;
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl.tag = 3;
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.scrViewData scrollRectToVisible:CGRectMake(viewWidth * index, 0, viewWidth, 200) animated:YES];
    }];
    [self.viewFullData addSubview:self.segmentedControl];
    self.scrViewData = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.pageControl.frame) + 5, viewWidth, CGRectGetHeight(self.viewFullData.frame) - CGRectGetMaxY(self.pageControl.frame) + 5)];
    self.scrViewData.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    self.scrViewData.pagingEnabled = YES;
    self.scrViewData.showsHorizontalScrollIndicator = NO;
    self.scrViewData.contentSize = CGSizeMake(viewWidth * 3, 200);
    self.scrViewData.delegate = self;
    [self.scrViewData scrollRectToVisible:CGRectMake(viewWidth, 0, viewWidth, 200) animated:NO];
    [self.viewFullData addSubview:self.scrViewData];
}
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentedControl setSelectedSegmentIndex:page animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

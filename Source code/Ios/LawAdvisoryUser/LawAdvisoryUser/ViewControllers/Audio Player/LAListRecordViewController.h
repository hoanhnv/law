//
//  LAListRecordViewController.h
//  JustapUser
//
//  Created by MAC on 11/19/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AwesomeTextField.h"
@protocol ListRecordChooseDelegate <NSObject>

- (void)didSelectFile:(NSString*)filePath;

@end
@interface LAListRecordViewController : LABaseViewController<AVAudioRecorderDelegate>
{
    NSTimer* mytime;
    AVAudioRecorder *recorder;
    IBOutlet UIButton* btRecord;
    IBOutlet UIButton* btCancel;
    IBOutlet UIButton* btSubmit;
    IBOutlet AwesomeTextField* txtNameSave;
    IBOutlet UILabel* lbRecordTime;
    NSString* audioPath;
}
@property (nonatomic) id <ListRecordChooseDelegate> delegate;
- (void) onPlayButtonOfCell: (UITableViewCell*) cell;
@end

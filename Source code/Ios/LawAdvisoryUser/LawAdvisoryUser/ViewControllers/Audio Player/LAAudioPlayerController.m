//
//  LAAudioPlayerController.m
//  JustapLawyer
//
//  Created by MAC on 9/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAudioPlayerController.h"
#import "LARelatoDoCasoTableViewCell.h"
@implementation LAAudioPlayerController
LARelatoDoCasoTableViewCell *cell;
+ (instancetype)sharedInstance {
    static LAAudioPlayerController *_sharedMusicVC = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedMusicVC = [[LAAudioPlayerController alloc] init];
        _sharedMusicVC.streamer = [[DOUAudioStreamer alloc] init];
    });
    
    return _sharedMusicVC;
}
- (IBAction)didTouchMusicToggleButton:(id)sender {
    if (self.musicIsPlaying) {
        [_streamer pause];
    } else {
        [_streamer play];
    }
}
- (void)stop
{
    [self.streamer stop];
    @try {
        [self removeStreamerObserver];
    } @catch(id anException){
    }
}
- (void)createSteammerWithUrl:(NSURL*)url
{
    @try {
        [self removeStreamerObserver];
    } @catch(id anException){
    }    
    Track *track = [[Track alloc] init];
    track.audioFileURL = url;
    _streamer = nil;
    _streamer = [DOUAudioStreamer streamerWithAudioFile:track];
//    [self addStreamerObserver];
    [self.streamer play];
}
- (void)removeStreamerObserver {
    [_streamer stop];
    [_streamer removeObserver:self forKeyPath:@"status"];
    [_streamer removeObserver:self forKeyPath:@"duration"];
    [_streamer removeObserver:self forKeyPath:@"bufferingRatio"];
}

- (void)addStreamerObserver {
    [_streamer addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:kStatusKVOKey];
    [_streamer addObserver:self forKeyPath:@"duration" options:NSKeyValueObservingOptionNew context:kDurationKVOKey];
    [_streamer addObserver:self forKeyPath:@"bufferingRatio" options:NSKeyValueObservingOptionNew context:kBufferingRatioKVOKey];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == kStatusKVOKey) {
        [self performSelector:@selector(updateStatus)
                     onThread:[NSThread mainThread]
                   withObject:nil
                waitUntilDone:NO];
    } else if (context == kDurationKVOKey) {
        if (cell != nil){
            [self updateStatus];
        }
    } else if (context == kBufferingRatioKVOKey) {

    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
- (void)setMusicIsPlaying:(BOOL)musicIsPlaying {
    _musicIsPlaying = musicIsPlaying;
}
- (void)updateStatus {
    self.musicIsPlaying = NO;
    switch ([_streamer status]) {
        case DOUAudioStreamerPlaying:
            self.musicIsPlaying = YES;
            break;
            
        case DOUAudioStreamerPaused:
            break;
            
        case DOUAudioStreamerIdle:
            break;
            
        case DOUAudioStreamerFinished:
            break;
            
        case DOUAudioStreamerBuffering:
            break;
            
        case DOUAudioStreamerError:
            break;
    }
}
@end

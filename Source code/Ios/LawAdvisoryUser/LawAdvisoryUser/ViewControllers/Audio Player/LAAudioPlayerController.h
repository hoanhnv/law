//
//  LAAudioPlayerController.h
//  JustapLawyer
//
//  Created by MAC on 9/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DOUAudioStreamer.h"
#import "Track.h"
static void *kStatusKVOKey = &kStatusKVOKey;
static void *kDurationKVOKey = &kDurationKVOKey;
static void *kBufferingRatioKVOKey = &kBufferingRatioKVOKey;
#define MP3_TEST_FILE @"http://www.stephaniequinn.com/Music/Allegro%20from%20Duet%20in%20C%20Major.mp3"
@interface LAAudioPlayerController : NSObject
@property (nonatomic, strong) DOUAudioStreamer *streamer;
@property (nonatomic) BOOL musicIsPlaying;
+ (instancetype)sharedInstance;
- (void)createSteammerWithUrl:(NSURL*)url;
- (IBAction)didTouchMusicToggleButton:(id)sender;
- (void)stop;
@end

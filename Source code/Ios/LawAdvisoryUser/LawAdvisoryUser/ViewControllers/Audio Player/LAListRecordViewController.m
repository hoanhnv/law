//
//  LAListRecordViewController.m
//  JustapUser
//
//  Created by MAC on 11/19/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAListRecordViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "RecordItemTableViewCell.h"
#include <sys/time.h>
#import "MBProgressHUD.h"
static NSString* kStrKeyPath = @"path";
static NSString* kStrKeySize = @"size";
static NSString* kStrKeyCreationDate = @"creationDate";
static NSString* kStrDidRecordNotification = @"DidRecordNotification";
@interface LAListRecordViewController ()
{
    NSMutableArray* _objects;
    NSInteger       _iSelectedIndex;
    NSInteger       _iCurrentAction;
    AVAudioPlayer*  _audioPlayer;
}
@property (strong, nonatomic) IBOutlet UIButton* buttonAction;
@property (strong, nonatomic) IBOutlet UIButton* buttonDelete;
@property (strong, nonatomic) IBOutlet UIButton* buttonEdit;
@property (strong, nonatomic) UIDocumentInteractionController *docController;
@property (strong, nonatomic) IBOutlet UITableView* tableView;
@end

@implementation LAListRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _iSelectedIndex = -1;
    _iCurrentAction = -1;
    [self loadRecordItems];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(sortSettingsDidChangeNotificationHandler:) name: kStrDidRecordNotification object: nil];
    [self loadRecordItems];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) sortSettingsDidChangeNotificationHandler: (NSNotification*) notification
{
    [self loadRecordItems];
    [self.tableView reloadData];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.buttonAction.enabled = NO;
    self.buttonDelete.enabled = NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void) loadRecordItems
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_objects.count)
    {
        [_objects removeAllObjects];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Record"] ;
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSArray* subItems = [fileManager contentsOfDirectoryAtPath: documentsDirectory error: nil];
    NSString* fullPath;
    if (_objects == nil)
    {
        _objects = [NSMutableArray new];
    }
    if (subItems && subItems.count) {
        for (NSInteger i = subItems.count - 1; i >= 0 ; i -- )
        {
            NSString *item = [subItems objectAtIndex:i];
            //if(([item hasSuffix: @".mp4"]) || ([item hasSuffix: @".caf"]))//mainp comment
            //{
            fullPath = [documentsDirectory stringByAppendingPathComponent: item];
            NSDictionary* fileAttrs = [fileManager attributesOfItemAtPath: fullPath error: nil];
            
            NSInteger fileSize = [fileAttrs fileSize];
            NSDate* createDate = [fileAttrs fileCreationDate];
            
            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: fullPath,kStrKeyPath , [NSNumber numberWithInteger: fileSize] , kStrKeySize, createDate, kStrKeyCreationDate, nil];
            [_objects addObject: dict];
        }
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void) onPlayButtonOfCell: (UITableViewCell*) cell
{
    NSIndexPath* indexPath = [self.tableView indexPathForCell: cell];
    if(indexPath)
    {
        if(_audioPlayer == nil)
        {
            NSDictionary* item = [_objects objectAtIndex: indexPath.row];
            NSURL* url = [NSURL fileURLWithPath: [item objectForKey: kStrKeyPath]];
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];
            [_audioPlayer play];
        }
        else
        {
            if ([_audioPlayer isPlaying])
            {
                [_audioPlayer stop];
            }
            else
            {
                _audioPlayer = nil;
                [self onPlayButtonOfCell:cell];
            }
        }
    }
    
}
- (IBAction)onDonePressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Table View
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    RecordItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RecordItemTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    NSDictionary* object = [_objects objectAtIndex:indexPath.row];
    NSString* path = [object objectForKey: kStrKeyPath];
    
    cell.textLabel.text = [path lastPathComponent];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell.detailTextLabel.text = [LAUtilitys stringFileNameFromDate: [object objectForKey: kStrKeyCreationDate]];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    
    cell.detailTextLabel2.text = [LAUtilitys stringFromFileSize: [object objectForKey: kStrKeySize]];
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize: 14];
    cell.detailTextLabel.font = [UIFont systemFontOfSize: 12];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.delegate = self;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSDictionary* object = [_objects objectAtIndex: indexPath.row];
        
        NSString* strPath = [object objectForKey: kStrKeyPath];
        NSNumber* number = [object objectForKey: kStrKeySize];
        
        if(strPath && number)
        {
            NSString* strKey = [NSString stringWithFormat: @"%@-%@", [strPath lastPathComponent], number];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey: strKey];
        }
        
        [[NSFileManager defaultManager] removeItemAtPath: [object objectForKey: kStrKeyPath] error: nil];
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.buttonAction.enabled = NO;
        self.buttonDelete.enabled = NO;
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath* currentIndex = [tableView indexPathForSelectedRow];
    if(currentIndex)
    {
        if(currentIndex.row == indexPath.row)
        {
            if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                if(_audioPlayer == nil)
                {
                    NSDictionary* item = [_objects objectAtIndex: indexPath.row];
                    NSURL* url = [NSURL fileURLWithPath: [item objectForKey: kStrKeyPath]];
                    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];
                    [_audioPlayer play];
                    
                }
                else
                {
                    [_audioPlayer stop];
                    _audioPlayer = nil;
                }
                
                
                RecordItemTableViewCell *cell = (RecordItemTableViewCell*)[tableView cellForRowAtIndexPath: indexPath];
                [cell setPlayButton: YES];
            }
            
            
        }
        
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < _objects.count)
    {
        NSString* strPath = [[_objects objectAtIndex:indexPath.row] objectForKey:kStrKeyPath];
        if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectFile:)])
        {
            [self.delegate didSelectFile:strPath];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
#pragma mark - Media Player Delegate

- (void) moviePlayBackDidChangeState: (NSNotification*) notification
{
    // MPMoviePlayerController* controller = [notification object];
}

- (void) moviePlayBackDidStop: (NSNotification*) notification
{
    //MPMoviePlayerController* controller = [notification object];
    NSIndexPath* indexPath = [self.tableView indexPathForSelectedRow];
    
    if(indexPath)
    {
        RecordItemTableViewCell *cell = (RecordItemTableViewCell*)[self.tableView cellForRowAtIndexPath: indexPath];
        [cell setPlayButton: NO];
    }
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self.tableView reloadData];
}


@end

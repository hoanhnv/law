//
//  LAMessageViewController.h
//  JustapLawyer
//
//  Created by Tùng Nguyễn on 2/18/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LABaseViewController.h"
@protocol MessageDelegate <NSObject>
-(void)getMessage:(NSString*)message;
-(void)addMessage:(NSString*)message;
@end

@interface LAMessageViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UITextView *txtMessages;

//@property (nonatomic,strong) NSString *message;
@property (nonatomic,strong) id <MessageDelegate> delegate;
@property (nonatomic) double Id;
- (IBAction)doSendMessage:(id)sender;
- (IBAction)doClose:(id)sender;

@end

//
//  LAMessagePopupViewController.h
//  JustapLawyer
//
//  Created by Tùng Nguyễn on 2/23/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LABaseViewController.h"

@interface LAMessagePopupViewController : LABaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbvMessages;
- (IBAction)doClose:(id)sender;
@property (nonatomic, strong) NSMutableArray *arrData;

@end

//
//  LAServiceViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "iCarousel.h"
@interface LAServiceViewController : LABaseViewController<iCarouselDataSource, iCarouselDelegate>
{
    NSMutableArray* arrBannerObj;
    NSMutableArray* arrCategories;
    NSMutableArray* arrSlider;
    UIBarButtonItem* btSearchBarItem;
}

@property (nonatomic) IBOutlet iCarousel* bannerView;
@property (nonatomic) IBOutlet UIView* viewFullData;
@property (nonatomic) IBOutlet UIScrollView* scrViewData;
- (IBAction)onViewNavigationTap:(id)sender;
@end

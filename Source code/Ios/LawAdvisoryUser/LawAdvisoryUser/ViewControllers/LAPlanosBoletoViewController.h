//
//  LAPlanosBoletoViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPlanObj.h"
#import "LASignatureObj.h"

@interface LAPlanosBoletoViewController : LABaseViewController{
    
}

@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic) NSMutableArray* arrSignature;

@property (weak, nonatomic) IBOutlet UIView *vBottom;

@property (nonatomic) IBOutlet UILabel* lbPlanosName;
@property (nonatomic) IBOutlet UILabel* lbTotalPlanSinging;

@property (nonatomic, weak) IBOutlet UITableView *tblSignature;
@property (nonatomic) IBOutlet UIScrollView* scrFullData;

- (IBAction)doConfirm:(id)sender;
@end

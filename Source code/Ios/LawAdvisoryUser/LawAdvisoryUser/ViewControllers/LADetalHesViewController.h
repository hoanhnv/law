//
//  LADetalHesViewController.h
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LADetalhesHeaderView.h"
#import "LACustomerOpenObj.h"
#import "LAdetalhesFooterView.h"
#import "LAMessageViewController.h"
@interface LADetalHesViewController : LABaseViewController<DetalHesPagamentoDelegate,DetalHesFooterDelegate,MessageDelegate>
{
    
}
@property (nonatomic,weak) IBOutlet UITableView* tbdata;
@property (nonatomic,assign) BOOL isShowButtonOk;
@property (nonatomic,assign) NSInteger backIndex;
@property (nonatomic,assign) NSInteger fromTabIndex;
@property (nonatomic) LACustomerOpenObj* objData;
@property (nonatomic) LALawsuitObj* objCategory;
@property (nonatomic,strong) NSMutableArray* arrData;
@end

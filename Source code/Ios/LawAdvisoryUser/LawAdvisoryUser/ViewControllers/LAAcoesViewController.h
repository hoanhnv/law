//
//  LAAcoesViewController.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAListCustomerOpen.h"
#import "LAListRefusal.h"
typedef enum TYPE_PEDENTE_CELL
{
    TYPE_PEDENTE_CELL_1 = 1,
    TYPE_PEDENTE_CELL_2 = 2,
    TYPE_PEDENTE_CELL_3 = 3,
    TYPE_PEDENTE_CELL_4 = 4,
    TYPE_PEDENTE_CELL_5 = 5
}TYPE_PEDENTE_CELL;
@interface LAAcoesViewController : LABaseViewController
{
    NSMutableArray* arrPendente;
    NSMutableArray* arrNovas;
    NSMutableArray* arrEmAnDamento;
    NSMutableArray* arrHistorico;
    NSMutableArray* arrTableview;
    LAListCustomerOpen *lstSuitOpen;
    LAListRefusal* listRefusal;
    BOOL isGetAllReason;
    NSInteger currentTab;
}
@property (nonatomic, assign)NSInteger selectedInitIndex;
- (void)gotoIndex:(NSInteger)index;
@end

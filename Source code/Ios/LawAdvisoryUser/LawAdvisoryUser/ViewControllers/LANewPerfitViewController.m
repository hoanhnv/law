//
//  LANewPerfitViewController.m
//  JustapLawyer
//
//  Created by MAC on 10/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LANewPerfitViewController.h"
#import "MBProgressHUD.h"
#import "LAAccountObj.h"
#import "LACountryViewController.h"
#import "LAAccountService.h"
#import "UIViewController+MJPopupViewController.h"
#import "LAPopupMessageViewController.h"
#import "AppDelegate.h"
#import "LAUFs.h"
#import "MainService.h"
#import "DynamicTitleView.h"
#import "LAStatusAccountObj.h"
#import "LALawyerInfo.h"
#import "LALawyerObj.h"
#import "LALawyer.h"
#import "LAViewPhone.h"
#import "JMImageCache.h"
#import "LAChangePassViewController.h"
#import "Photos/Photos.h"
#import "IQKeyboardManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ResizeMagick.h"
#import "LABankInfo.h"

#define COLOR_BUTTON_CHANGEPASS [UIColor colorWithRed:0.0/255.0 green:94.0/255.0 blue:160.0/255.0 alpha:1.0]


#define MAX_SIZE_IMAGE_DEFAULT 1024
#define MAX_SIZE_IMAGE_AVATAR 512

@interface LANewPerfitViewController ()<UITextFieldDelegate,DynamicTitleViewDelegate,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate>
{
    UIDatePicker *datePicker;
    LALawyerInfo *lawyerInfo;
    LABankInfo *bankInfo;
    CGRect currentFrame;
    BOOL needFillData;
}
@end

@implementation LANewPerfitViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uploadImage:) name:ARGS_UPLOAD_IMAGE object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeImage:) name:ARGS_REMOVE_IMAGE object:nil];
    
    
    // Do any additional setup after loading the view.
    self.isEditView = NO;
    needFillData = YES;
    currentFrame = self.scrFullData.frame;
    currentFrame.size.height += 50;
    self.scrFullData.frame = currentFrame;
    [self.vDynamicTitle loadViewWithData:self.objInfo.data.titles];
    [self loadBank];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText:@"Done"];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden = YES;
    UITapGestureRecognizer *tapShowMail = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapShowMail)];
    [self.vShowMail addGestureRecognizer:tapShowMail];
    
    UITapGestureRecognizer *tapShowCellPhone = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeShowCellPhone)];
    [self.vShowCellPhone addGestureRecognizer:tapShowCellPhone];
    
    UITapGestureRecognizer *tapShowPhone = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeShowPhone)];
    [self.vShowPhone addGestureRecognizer:tapShowPhone];
    
    [self.vDynamicTitle setDelegate:self];
    [self setupDataIfNeed];
    [self updateViewIfNeed];
    [self onShowBirthDaySelect];
    
}
-(void)viewDidAppear:(BOOL)animated{
    currentFrame = self.scrFullData.frame;
}
- (void)initUI
{
    [self setupFrame];
    self.navigationItem.title = @"Perfil";
    
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtCity];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtUF];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtNewState];
    
    [LAUtilitys setButtonTextUnderLine:self.btChangePassword];
    [self.btChangePassword setTitleColor:COLOR_BUTTON_CHANGEPASS forState:UIControlStateNormal];
}
- (void)initData
{
    arrRequiredField = [NSMutableArray arrayWithObjects:self.txtEmail,self.txtName,self.txtCPF,self.txtCEP,self.txtAddress,self.txtNumber,self.txtUF,self.txtCity,self.txtMobilePhone,self.txtNumberOAB,self.txtNewState, nil];
    arrEmail = [NSMutableArray arrayWithObjects:self.txtEmail, nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didselectAddress:) name:@"didSelectCountry" object:nil];
    [self getUFs];
}
- (void)setupDataIfNeed
{
    if (needFillData) {
        if (self.objInfo)
        {
            self.txtEmail.text = self.objInfo.data.email;
            self.txtName.text = [LAUtilitys getDefaultString:self.objInfo.data.name];
            self.txtDateOfBirth.text = [LAUtilitys getDateStringFromTimeInterVal:self.objInfo.data.dateOfBirth withFormat:FORMAT_DD_MM_YY];
            
            NSString *strCPF = [LAUtilitys getDefaultString:self.objInfo.data.cpf];
            NSMutableString *mutableString = [NSMutableString new];
            [mutableString setString:strCPF];
            [mutableString insertString:@"." atIndex:3];
            [mutableString insertString:@"." atIndex:7];
            [mutableString insertString:@"-" atIndex:11];
            self.txtCPF.text = mutableString;
            
            self.txtAddress.text = [LAUtilitys getDefaultString:self.objInfo.data.address];
            self.txtNumber.text = [LAUtilitys getDefaultString:self.objInfo.data.number];
            self.txtNeighborhood.text = [LAUtilitys getDefaultString:self.objInfo.data.neighborhood];
            self.txtUF.text = [LAUtilitys getDefaultString:self.objInfo.data.uf];
            self.txtCity.text = [LAUtilitys getDefaultString:self.objInfo.data.city];
            self.txtMobilePhone.text = [LAUtilitys getDefaultString:self.objInfo.data.cellphone];
            self.txtPhone.text = [LAUtilitys getDefaultString:self.objInfo.data.phone];
            self.txtNumberOAB.text = [LAUtilitys getDefaultString:self.objInfo.data.lawyer.numberOab];
            self.txtSite.text = [LAUtilitys getDefaultString:self.objInfo.data.lawyer.site];
            self.txtLinkedin.text = [LAUtilitys getDefaultString:self.objInfo.data.lawyer.linkedin];
            self.txtCEP.text = [LAUtilitys getDefaultString:self.objInfo.data.postalCode];
            self.txtComplemento.text = [LAUtilitys getDefaultString:self.objInfo.data.complement];
            //            [self.cbShowPhone setSelected:self.objInfo.data.viewPhone.showPhone];
            //            [self.cbShowCellphone setSelected:self.objInfo.data.viewPhone.showCellphone];
            //            [self.ckShowEmail setSelected:self.objInfo.data.viewPhone.showEmail];
            self.txtAbout.text = self.objInfo.data.lawyer.about;
            
            if ([LAUtilitys NullOrEmpty:self.currentState]) {
                self.txtNewState.text = self.objInfo.data.lawyer.ufOab;
            }
            else{
                self.txtNewState.text = self.currentState;
                self.currentState = STRING_EMPTY;
            }
            self.txtBankConta.text = self.objInfo.data.lawyer.bank_account;
            self.txtBankAgenda.text = self.objInfo.data.lawyer.bank_agency;
            self.txtBankVerify.text = self.objInfo.data.lawyer.bank_account_dac;
            if (self.currentBank != nil) {
                self.txtBankName.text = self.currentBank.name;
            }
            [self loadImage];
        }
        else{
            // TODO
        }
    }
    else{
        // TODO
    }
    
}
-(void)loadImage{
    if (self.objInfo.data.lawyer.imageAvatarWithUrl != nil) {
        __weak UIImageView *weakImageView1 = self.uploadView1.imgThumb;
        self.uploadView1.btnUpload.enabled = false;
        self.uploadView1.bHasImage = YES;
        self.uploadView1.lbName.text = self.objInfo.data.lawyer.imageAvatar;
        
        [weakImageView1 sd_setImageWithURL:[NSURL URLWithString:self.objInfo.data.lawyer.imageAvatarWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.uploadView1.btnUpload.enabled = true;
        }];
    }
    else{
        self.uploadView1.bHasImage = NO;
        self.uploadView1.lbName.text = STRING_EMPTY;
    }
    if (self.objInfo.data.lawyer.imageHouseProofWithUrl != nil) {
        __weak UIImageView *weakImageView2 = self.uploadView2.imgThumb;
        self.uploadView2.btnUpload.enabled = false;
        self.uploadView2.bHasImage = YES;
        self.uploadView2.lbName.text = self.objInfo.data.lawyer.imageHouseProof;
        
        [weakImageView2 sd_setImageWithURL:[NSURL URLWithString:self.objInfo.data.lawyer.imageHouseProofWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.uploadView2.btnUpload.enabled = true;
        }];
    }
    else{
        self.uploadView2.bHasImage = NO;
        self.uploadView2.lbName.text = STRING_EMPTY;
        [self.uploadView2 layoutSubviews];
    }
    if (self.objInfo.data.lawyer.imageOabWithUrl != nil) {
        __weak UIImageView *weakImageView3 = self.uploadView3.imgThumb;
        self.uploadView3.btnUpload.enabled = false;
        self.uploadView3.bHasImage = YES;
        self.uploadView3.lbName.text = self.objInfo.data.lawyer.imageOab;
        
        [weakImageView3 sd_setImageWithURL:[NSURL URLWithString:self.objInfo.data.lawyer.imageOabWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.uploadView3.btnUpload.enabled = true;
        }];
    }
    else{
        self.uploadView3.bHasImage = NO;
        self.uploadView3.lbName.text = STRING_EMPTY;
    }
    
    if (self.objInfo.data.lawyer.imageHouseProofWithUrl != nil) {
        __weak UIImageView *weakImageView4 = self.uploadView4.imgThumb;
        self.uploadView4.btnUpload.enabled = false;
        self.uploadView4.bHasImage = YES;
        self.uploadView4.lbName.text = self.objInfo.data.lawyer.imageOabVerse;
        
        [weakImageView4 sd_setImageWithURL:[NSURL URLWithString:self.objInfo.data.lawyer.imageOabVerseWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.uploadView4.btnUpload.enabled = true;
        }];
    }
    else{
        self.uploadView4.bHasImage = NO;
        self.uploadView4.lbName.text = STRING_EMPTY;
    }
    
    [self.uploadView1 layoutSubviews];
    [self.uploadView2 layoutSubviews];
    [self.uploadView3 layoutSubviews];
    [self.uploadView4 layoutSubviews];
    [self.vDynamicTitle layoutSubviews];
    [self updateViewIfNeed];
}
#pragma mark Auto correct
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.txtCPF) {
        if (newString.length == 4 || newString.length == 8)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"."];
            }
        }
        else if (newString.length == 12)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 14;
        }
        return YES;
    }
    else if (textField == self.txtCEP)
    {
        if (newString.length == 6)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 9;
        }
        return YES;
    }
    else if (textField == self.txtMobilePhone)
    {
        if (range.location == 3 || range.location == 9 || (range.location == 0 && [self.txtMobilePhone.text containsString:@"("]))
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength < textField.text.length)
            {
                if (range.location == 9 && ![newString containsString:@"."])
                {
                    return YES;
                }
                if (range.location == 3 && ![newString containsString:@")"])
                {
                    return YES;
                }
                return NO;
            }
        }
        if (range.location == 2)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [NSString stringWithFormat:@"(%@)",textField.text];
            }
            else
            {
                textField.text = [textField.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
            }
        }
        else if (range.location == 9 && ![textField.text containsString:@"-"])
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 14;
        }
        return YES;
    }
    else if (textField == self.txtPhone)
    {
        if (range.location == 3 || range.location == 8 || (range.location == 0 && [self.txtPhone.text containsString:@"("]))
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength < textField.text.length)
            {
                if (newLength < textField.text.length)
                {
                    if (range.location == 8 && ![newString containsString:@"."])
                    {
                        return YES;
                    }
                    if (range.location == 3 && ![newString containsString:@")"])
                    {
                        return YES;
                    }
                    return NO;
                }
                return NO;
            }
        }
        if (range.location == 2)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [NSString stringWithFormat:@"(%@)",textField.text];
            }
            else
            {
                textField.text = [textField.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
            }
        }
        else if (range.location == 8 && ![textField.text containsString:@"-"])
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength > textField.text.length)
            {
                textField.text = [textField.text stringByAppendingString:@"-"];
            }
        }
        else
        {
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 13;
        }
        return YES;
    }
    else if(self.txtNumberOAB == textField){
        if (textField.text.length >= 8 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
    }
    else if(self.txtBankAgenda == textField){
        if (textField.text.length >= 5 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
    }
    else if(self.txtBankConta == textField){
        if (textField.text.length >= 20 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
        
    }
    else if(self.txtBankVerify == textField){
        if (textField.text.length >=1 && range.length == 0) {
            return NO;
        } else {
            return YES;
        }
        
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txtUF || textField == self.txtCity || textField == self.txtEmail)
    {
        return NO;
    }
    return YES;
}
- (void)getCityList
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [LAAddressService getlistCityWithUF:self.txtUF.text wCompleteBlock:^(NSInteger errorCode, NSString *message, id data, NSInteger total) {
        self.txtCity.text = STRING_EMPTY;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data) {
            listCity = [NSMutableArray arrayWithArray:data];
        }
    }];
}
-(void)getUFs{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchUFs:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        listUFS = [[LAUFs alloc]initWithDictionary:data];
    }];
}
- (void)setupFrame
{
    CGPoint pointCenter = self.scrFullData.center;
    pointCenter.x = self.view.center.x;
    [self.scrFullData setCenter:pointCenter];
    //CONTENT SIZE
    CGSize size = self.scrFullData.contentSize;
    size.height = CGRectGetMaxY(self.btnConfirm.frame) + 60;
    [self.scrFullData setContentSize:size];
    NSLog(@"current frame:%@",NSStringFromCGRect(self.scrFullData.frame));
    
    NSLog(@"size:%@",NSStringFromCGSize(self.scrFullData.frame.size));
    
}
- (BOOL)checkIsValid
{
    BOOL isCheck = YES;
    for (AwesomeTextField* txt in arrRequiredField)
    {
        if ([LAUtilitys isEmptyOrNull:txt.text])
        {
            [self showMessage:[NSString stringWithFormat:@"%@ inválido",txt.placeholder] withPoistion:nil];
            isCheck = NO;
            break;
        }
    }
    if (isCheck)
    {
        for (AwesomeTextField* txt in arrEmail)
        {
            if (![LAUtilitys validateEmailWithString:txt.text])
            {
                [self showMessage:[NSString stringWithFormat:@"%@ inválido",txt.placeholder] withPoistion:nil];
                isCheck = NO;
                break;
            }
        }
    }
    if (isCheck)
    {
        if (![LAUtilitys isValidCellPhone:self.txtMobilePhone.text])
        {
            [self showMessage:[NSString stringWithFormat:@"%@ inválido",self.txtMobilePhone.placeholder] withPoistion:nil];
            [self.txtMobilePhone becomeFirstResponder];
            isCheck = NO;
        }
    }
    if (isCheck)
    {
        if (![LAUtilitys isValidCPF:self.txtCPF.text])
        {
            [self showMessage:@" CPF* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if([self.txtCEP.text length]<=8){
            [self showMessage:@" CEP* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if(!self.uploadView1.bHasImage) {
            [self showMessage:@" Foto de perfil* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if(!self.uploadView2.bHasImage) {
            [self showMessage:@" Comprovante de endereço* inválido" withPoistion:nil];
            isCheck = NO;
        }
        else if(!self.uploadView3.bHasImage) {
            [self showMessage:@" Cópia da OAB* inválido " withPoistion:nil];
            isCheck = NO;
        }
        else if(!self.uploadView4.bHasImage) {
            [self showMessage:@" Cópia da OAB* inválido " withPoistion:nil];
            isCheck = NO;
        }
    }
    return isCheck;
}

- (IBAction)onChangePass:(id)sender
{
    LAChangePassViewController* changeVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAChangePassViewController class])];
    [self.navigationController pushViewController:changeVC animated:YES];
}

- (IBAction)changeGetNew:(id)sender {
    UIButton* bt = (UIButton*)sender;
    [bt setSelected:!bt.selected];
}

- (void)changeShowCellPhone {
    [self.cbShowCellphone setSelected:!self.cbShowCellphone.selected];
}

- (void)tapShowMail {
    [self.ckShowEmail setSelected:!self.ckShowEmail.selected];
}

- (void)changeShowPhone{
    [self.cbShowPhone setSelected:!self.cbShowPhone.selected];
}

- (void)fillData
{
    self.txtEmail.text = @"hoangmailong@gmail.com";
    self.txtPassWord.text = @"123456";
    self.txtName.text = @"Hoang Mai Long";
    self.txtDateOfBirth.text = @"24/11/1990";
    self.txtCPF.text = @"123.123.123";
    self.txtAddress.text = @"Cau giay, Ha Noi";
    self.txtNumber.text = @"123";
    self.txtNeighborhood.text = @"NeightBorHood Here";
    self.txtUF.text = @"CE";
    self.txtCity.text = @"Fortaleza";
    self.txtMobilePhone.text = @"(85) 99999.9999";
    self.txtPhone.text = @"(85) 99999.999";
    self.txtNumberOAB.text = @"12345678";
    self.txtSite.text = @"ttp://www.mobileplus.vn";;
    self.txtLinkedin.text = @"hoang mai long";
    self.txtCEP.text = @"12345-111";
    self.txtComplemento.text = @"Complement here";
    for (UITextField* txt in self.vDynamicTitle.arrData)
    {
        txt.text = @"Test title";
    }
}
- (IBAction)onUpdate:(id)sender
{
    isSuccess = NO;
    objAccount = [LAAccountObj new];;
    [self.view endEditing:YES];
    if ([self checkIsValid])
    {
        objAccount.accountEmail = self.txtEmail.text;
        objAccount.accountPassword = self.txtPassWord.text;
        objAccount.accountName = self.txtName.text;
        objAccount.accountCPF = self.txtCPF.text;
        if([LAUtilitys NullOrEmpty:self.txtDateOfBirth.text]){
            objAccount.accountDateOfBirth = 0;
        }
        else{
            objAccount.accountDateOfBirth = [LAUtilitys getNSDateFromDateString:self.txtDateOfBirth.text withFormat:FORMAT_DD_MM_YY].timeIntervalSince1970;
        }
        objAccount.accountCEP = self.txtCEP.text;
        objAccount.accountAddress = self.txtAddress.text;
        objAccount.accountNumber = self.txtNumber.text;
        objAccount.accountNeighborhood = self.txtNeighborhood.text;
        objAccount.accountUF = self.txtUF.text;
        objAccount.accountCity = self.txtCity.text;
        objAccount.accountCellphone = self.txtMobilePhone.text;
        objAccount.accountPhone = self.txtPhone.text;
        objAccount.avataURL = @"";
        objAccount.accountNumberOAB = self.txtNumberOAB.text;
        objAccount.accountSite = self.txtSite.text;
        objAccount.accountLinkedin = self.txtLinkedin.text;
        objAccount.accountPostalCode = self.txtCEP.text;
        objAccount.accountComplement = self.txtComplemento.text;
        objAccount.accountPassword = self.txtPassWord.text;
        NSMutableArray* arrTitles = [NSMutableArray new];
        for (UITextField* txt in self.vDynamicTitle.arrData)
        {
            [arrTitles addObject:txt.text];
        }
        objAccount.accountTitles = arrTitles;
        objAccount.accountShowEmail = self.ckShowEmail.selected;
        objAccount.accountShowPhone = self.cbShowPhone.selected;
        objAccount.accountShowMobilePhone = self.cbShowCellphone.selected;
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        objAccount.state = self.txtNewState.text;
        objAccount.about = self.txtAbout.text;
        
        objAccount.bank_id = self.currentBank.dataIdentifier;
        objAccount.bank_agency = self.txtBankAgenda.text;
        objAccount.bank_account = self.txtBankConta.text;
        objAccount.bank_account_dac = self.txtBankVerify.text;

        
        objAccount.image_avatar = [LAUtilitys encodeToBase64String:self.imgAttach1];
        objAccount.image_house_proof = [LAUtilitys encodeToBase64String:self.imgAttach2];
        objAccount.image_oab = [LAUtilitys encodeToBase64String:self.imgAttach3];
        objAccount.image_oab_verse = [LAUtilitys encodeToBase64String:self.imgAttach4];

        [LAAccountService updateAccountWithAccount:objAccount wCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                isSuccess = success;
                message = [data objectForKey:KEY_RESPONSE_MESSAGE];
                if (isSuccess)
                {
                   // [self checkStatus:[LADataCenter shareInstance].currentAccessToken];
                    [self uploadImageProcess];
                }
            }
            [self showMessage:message withPoistion:nil];
        }];
        
    }
}

- (void)showMessageViewControllerWithContent:(NSString*)contentText andTitleButton:(NSString*)titleButton
{
    NSString *className = NSStringFromClass([LAPopupMessageViewController class]);
    LAPopupMessageViewController *controller = [[LAPopupMessageViewController alloc] initWithNibName:className bundle:nil];
    controller.lbContent.text = contentText;
    [controller.btExit setTitle:titleButton forState:UIControlStateNormal];
    controller.delegate = self;
    controller.content = contentText;
    controller.titleButton = titleButton;
    [controller.view setFrame:[AppDelegate sharedDelegate].window.bounds];
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
    
}
- (void)onFinishPopupPressed
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self gotoHome];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    needFillData = NO;
    
    [self fillImageForUploadControl:picker didFinishPickingMediaWithInfo:info];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)onTakePic:(id)sender andTag:(NSInteger)tag
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        imagePickerController.view.tag = tag;
        [self.navigationController presentViewController:imagePickerController animated:YES completion:^{
        }];
    }
    else
    {
        [self showMessage:@"Camera not avaible" withPoistion:nil];
    }
}
- (IBAction)onChooseImage:(id)sender andTag:(NSInteger)tag
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:
                            @"Câmera",
                            @"Escolher dos arquivos",
                            nil];
    popup.tag = tag;
    [popup showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self onTakePic:nil andTag:popup.tag];
            break;
        case 1:
            [self chooseImageForControl:popup.tag  withSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        default:
            break;
    }
    
}
- (IBAction)onShowCity:(id)sender
{
    if ([LAUtilitys NullOrEmpty:self.txtUF.text])
    {
        [self.txtUF becomeFirstResponder];
        [self onShowState:nil];
    }
    else
    {
        [self showCityList:TYPE_CITY];
    }
    
}
- (IBAction)doShowBankName:(id)sender {
    [self showCityList:TYPE_BANK];
}
- (IBAction)onShowState:(id)sender
{
    listState = [NSMutableArray arrayWithArray:listUFS.data];
    [self showCityList:TYPE_STATE];
}
- (IBAction)onShowNewState:(id)sender
{
    listState = [NSMutableArray arrayWithArray:listUFS.data];
    [self showCityList:TYPE_NEWSTATE];
}
- (void)showCityList:(TYPE_POPUP)type
{
    LACountryViewController* countryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LACountryViewController"];
    countryVC.isShouldBackButton = NO;
    if (type == TYPE_CITY)
    {
        countryVC.arrCountry = listCity;
        countryVC.currentCity = self.currentCity;
    }
    else if(type == TYPE_BANK){
        countryVC.arrCountry = bankInfo.data;
        countryVC.currentBank = self.currentBank;
    }
    else
    {
        countryVC.arrCountry = listState;
        countryVC.currentState = self.currentState;
    }
    countryVC.popType = type;
    countryVC.isTransparentNavigation = YES;
    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:countryVC];
    [self presentViewController:navi animated:YES completion:nil];
}
- (void)didselectAddress:(NSNotification*)notif
{
    id obj = [notif object];
    if (obj && [obj isKindOfClass:[LACityObj class]])
    {
        self.currentCity = obj;
        self.txtCity.text = self.currentCity.cityName;
    }
    else{
        long typePoup = [[[notif userInfo] objectForKey:@"TYPEPOUP"] integerValue];
        if (typePoup == TYPE_STATE) {
            self.currentState = obj;
            self.txtUF.text = self.currentState;
            [self getCityList];
        }
        else if(typePoup == TYPE_NEWSTATE){
            self.currentState = obj;
            self.txtNewState.text = self.currentState;
            //            [self getCityList];
        }
        else if(typePoup == TYPE_BANK){
            self.currentBank = obj;
            self.txtBankName.text = self.currentBank.name;
        }
    }
}
-(void)fillImageForUploadControl:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    UIImage* img1;
    NSData *imgData;
    CGFloat ratio;
    if (picker.view.tag == self.uploadView1.tag) {
        img1 = [UIImage imageWithImage:img scaledToMaxWidth:MAX_SIZE_IMAGE_AVATAR maxHeight:MAX_SIZE_IMAGE_AVATAR];
        imgData = UIImageJPEGRepresentation(img, 1.0);
        ratio = MAX_SIZE_IMAGE_AVATAR*MAX_SIZE_IMAGE_AVATAR/([imgData length]);
    }
    else{
        img1 = [UIImage imageWithImage:img scaledToMaxWidth:MAX_SIZE_IMAGE_DEFAULT maxHeight:MAX_SIZE_IMAGE_DEFAULT];
        imgData = UIImageJPEGRepresentation(img, 1.0);
        ratio = MAX_SIZE_IMAGE_DEFAULT*MAX_SIZE_IMAGE_DEFAULT/([imgData length]);
    }
    img = img1;
    
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSString *filename = STRING_EMPTY;
    if (refURL == nil) {
        NSCalendar *gregorian = [[NSCalendar alloc]
                                 initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components =
        [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:[NSDate date]];
        
        NSInteger minute = [components minute];
        NSInteger second = [components second];
        if (minute < 10 && second < 10) {
            filename = [NSString stringWithFormat:@"IMG_0%ld%0ld.jpg",(long)minute,(long)second];
        }
        else if (minute < 10) {
            filename = [NSString stringWithFormat:@"IMG_0%ld%ld.jpg",(long)minute,(long)second];
        }
        else if (second < 10) {
            filename = [NSString stringWithFormat:@"IMG_%ld%0ld.jpg",(long)minute,(long)second];
        }
        else{
            filename = [NSString stringWithFormat:@"IMG_%ld%ld.jpg",(long)minute,(long)second];
        }
        
    }
    else{
        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
        filename = [[result firstObject] filename];
    }
    
    if (picker.view.tag == self.uploadView1.tag) {
        self.imgAttach1 = img;
        self.uploadView1.imgThumb.image = self.imgAttach1;
        self.uploadView1.bHasImage = YES;
        self.uploadView1.lbName.text = filename;
        [self.uploadView1 layoutSubviews];
    }
    else if(picker.view.tag == self.uploadView2.tag) {
        self.imgAttach2 = img;
        self.uploadView2.imgThumb.image = self.imgAttach2;
        self.uploadView2.bHasImage = YES;
        self.uploadView2.lbName.text = filename;
        [self.uploadView2 layoutSubviews];
        
    }
    else if(picker.view.tag == self.uploadView3.tag){
        self.imgAttach3 = img;
        self.uploadView3.lbName.text = filename;
        
        self.uploadView3.imgThumb.image = self.imgAttach3;
        self.uploadView3.bHasImage = YES;
        [self.uploadView3 layoutSubviews];
    }
    else if(picker.view.tag == self.uploadView4.tag){
        self.imgAttach4 = img;
        self.uploadView4.lbName.text = filename;
        
        self.uploadView4.imgThumb.image = self.imgAttach4;
        self.uploadView4.bHasImage = YES;
        [self.uploadView4 layoutSubviews];
    }
    //    NSURL *localPath = [NSURL fileURLWithPath:NSTemporaryDirectory().pa]
    [self updateViewIfNeed];
}
- (void)chooseImageForControl:(NSInteger)tagControl withSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.view.tag = tagControl;
    [self.navigationController presentViewController:imagePickerController animated:YES completion:^{}];
}
- (void)uploadImageProcess
{
    NSMutableArray* imgUploads = [NSMutableArray new];
    NSMutableArray* keyUploads = [NSMutableArray new];
    NSMutableArray* nameUploads = [NSMutableArray new];
    if (self.imgAttach1)
    {
        [imgUploads addObject:self.imgAttach1];
        //        [imgUploads addObject:[UIImage imageNamed:@"star"]];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_AVATA];
        [nameUploads addObject:self.uploadView1.lbName.text];
    }
    if (self.imgAttach2)
    {
        [imgUploads addObject:self.imgAttach2];
        //        [imgUploads addObject:[UIImage imageNamed:@"star"]];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF];
        [nameUploads addObject:self.uploadView2.lbName.text];
    }
    if (self.imgAttach3)
    {
        [imgUploads addObject:self.imgAttach3];
        //        [imgUploads addObject:[UIImage imageNamed:@"star"]];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_OAB];
        [nameUploads addObject:self.uploadView3.lbName.text];
    }
    if (self.imgAttach4)
    {
        [imgUploads addObject:self.imgAttach4];
        [keyUploads addObject:REQQUEST_ACCOUNT_KEY_IMAGE_OAB_VERSE];
        [nameUploads addObject:self.uploadView4.lbName.text];
    }
    
    
    if (imgUploads.count)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeAnnularDeterminate;
        hud.labelText = @"Image uploading...";
        NSLog(@"%@",[imgUploads description]);
        [LABaseService uploadImages:imgUploads andListKeys:keyUploads andListName:nameUploads andParams:nil andPath:API_PATH_UPLOAD_IMAGE_LAWYER_FORUPDATE withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
            NSLog(@"data upload:%@",data);
            if (isSuccess)
            {
                [self checkStatus:[LADataCenter shareInstance].currentAccessToken];
            }
            [hud hide:YES];
        } progressBlock:^(double currentProgress) {
            hud.progress = currentProgress;
        }];
    }
    //    else if (imgUploads.count < 3)
    //    {
    //        [self showMessage:@"All photo upload fields is required" withPoistion:nil];
    //    }
    else
    {
        if (isSuccess)
        {
            [self checkStatus:[LADataCenter shareInstance].currentAccessToken];
        }
    }
}


- (void)checkStatus:(NSString*)token
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService checkStatus:token withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[LAStatusAccountObj class]])
        {
            LAStatusAccountObj* statusAccountObj = data;
            switch (statusAccountObj.type)
            {
                case ACTIVE_ACCOUNT:
                    [self.navigationController popViewControllerAnimated:YES];
                    break;
                default:
                {
                    [self navigationWithStatusAccount:statusAccountObj];
                }
                    break;
            }
        }
        else
        {
            [self showMessage:message withPoistion:nil];
        }
    }];
}
-(void)uploadImage:(NSNotification*)notification{
    [self.view endEditing:YES];
    NSNumber *tagControl = (NSNumber*)[notification object];
    [self onChooseImage:nil andTag:[tagControl integerValue]];
}

-(void)updateViewIfNeed{
    self.lbComprovante.frame = CGRectMake(self.lbComprovante.frame.origin.x, CGRectGetMaxY(self.uploadView1.frame) + 10, self.lbComprovante.frame.size.width, self.lbComprovante.frame.size.height);
    
    self.lbSeu.frame = CGRectMake(self.lbSeu.frame.origin.x, CGRectGetMaxY(self.lbComprovante.frame), self.lbSeu.frame.size.width, self.lbSeu.frame.size.height);
    
    self.uploadView2.frame = CGRectMake(self.uploadView2.frame.origin.x, CGRectGetMaxY(self.lbSeu.frame) + 10, self.uploadView2.frame.size.width, self.uploadView2.frame.size.height);
    
    self.viewMiddle.frame = CGRectMake(self.viewMiddle.frame.origin.x, CGRectGetMaxY(self.uploadView2.frame) + 10, self.viewMiddle.frame.size.width, self.viewMiddle.frame.size.height);
    self.uploadView3.frame = CGRectMake(self.uploadView3.frame.origin.x, CGRectGetMaxY(self.viewMiddle.frame) + 10, self.uploadView3.frame.size.width, self.uploadView3.frame.size.height);
    
    self.lbCpioTitle.frame = CGRectMake(self.lbCpioTitle.frame.origin.x, CGRectGetMaxY(self.uploadView3.frame) + 10, self.lbCpioTitle.frame.size.width, self.lbCpioTitle.frame.size.height);
    
    self.uploadView4.frame = CGRectMake(self.uploadView4.frame.origin.x, CGRectGetMaxY(self.lbCpioTitle.frame) + 10, self.uploadView4.frame.size.width, self.uploadView4.frame.size.height);
    
    self.vDynamicTitle.frame = CGRectMake(self.vDynamicTitle.frame.origin.x, CGRectGetMaxY(self.uploadView4.frame) + 10, self.vDynamicTitle.frame.size.width, self.vDynamicTitle.frame.size.height);
    
    self.vBankInfo.frame = CGRectMake(self.vBankInfo.frame.origin.x, CGRectGetMaxY(self.vDynamicTitle.frame) + 10, self.vBankInfo.frame.size.width, self.vBankInfo.frame.size.height);
    
    self.btnConfirm.frame = CGRectMake(self.btnConfirm.frame.origin.x, CGRectGetMaxY(self.vBankInfo.frame) + 22, self.btnConfirm.frame.size.width, self.btnConfirm.frame.size.height);
    
    CGSize size = self.scrFullData.contentSize;
    size.height = CGRectGetMaxY(self.btnConfirm.frame) + self.btnConfirm.frame.size.height + 20;
    [self.scrFullData setContentSize:size];
    NSLog(@"content frame:%@",NSStringFromCGRect(self.scrFullData.frame));
    NSLog(@"content size:%@",NSStringFromCGSize(self.scrFullData.contentSize));
}
-(void)didFinishInsertSubView:(NSMutableArray *)arrData{
    [self.vDynamicTitle layoutSubviews];
    [self updateViewIfNeed];
}
- (IBAction)onShowEmailPressed:(id)sender;
{
    [self.ckShowEmail setSelected:!self.ckShowEmail.selected];
}
#pragma mark BirthDay
- (void)onShowBirthDaySelect
{
    datePicker = [[UIDatePicker alloc]init];
    if (![LAUtilitys NullOrEmpty:self.txtDateOfBirth.text]) {
        NSDate *date = [LAUtilitys getNSDateFromDateString:self.txtDateOfBirth.text withFormat:FORMAT_DD_MM_YY];
        if (date) {
            [datePicker setDate:date];
        }
    }
    //this returns today's date
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = FORMAT_DD_MM_YY;
    // converting string to date
    
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
    //    [datePicker setMaximumDate:[NSDate date]]; //the min age restriction
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    //    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"US"];
    //    [datePicker setLocale:locale];
    datePicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3*3600];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // and finally set the datePicker as the input mode of your textfield
    [self.txtDateOfBirth setInputView:datePicker];
}
-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)self.txtDateOfBirth.inputView;
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:3600*3];
    [dateFormatter setDateFormat:FORMAT_DD_MM_YY];
    self.txtDateOfBirth.text =  [dateFormatter stringFromDate:picker.date];
}


-(void)removeImage:(NSNotification*)notification{
    NSNumber *tagControl = (NSNumber*)[notification object];
    switch ([tagControl integerValue]) {
        case 10001:
            [self.uploadView1.imgThumb setImage:nil];
            self.uploadView1.bHasImage = NO;
            [self.uploadView1 layoutSubviews];
            break;
        case 10002:
            [self.uploadView2.imgThumb setImage:nil];
            self.uploadView2.bHasImage = NO;
            [self.uploadView2 layoutSubviews];
            break;
        case 10003:
            [self.uploadView3.imgThumb setImage:nil];
            self.uploadView3.bHasImage = NO;
            [self.uploadView3 layoutSubviews];
            break;
        case 10004:
            [self.uploadView4.imgThumb setImage:nil];
            self.uploadView4.bHasImage = NO;
            [self.uploadView4 layoutSubviews];
            break;
        default:
            break;
    }
    [self updateViewIfNeed];
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (self.txtCEP == textField) {
        [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        [MainService checkCEP:self.txtCEP.text withBlock:^(NSInteger errorCode, NSString *message, id data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                if ([data isKindOfClass:[NSDictionary class]] && [data count]) {
                    if (![LAUtilitys NullOrEmpty:data[@"uf"]]) {
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        [LAAddressService getlistCityWithUF:data[@"uf"] wCompleteBlock:^(NSInteger errorCode, NSString *message, id data, NSInteger total) {
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            if (data) {
                                listCity = [NSMutableArray arrayWithArray:data];
                            }
                        }];
                    }
                    self.txtUF.text = data[@"uf"];
                    self.txtAddress.text = data[@"logradouro"];
                    self.txtCity.text = data[@"localidade"];
                    self.txtNeighborhood.text = data[@"bairro"];
                }
            });
        }];
    }
    return YES;
}

-(void)loadBank{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchBanks:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        bankInfo = [[LABankInfo alloc]initWithDictionary:data];
        for (LABankObj *bankObj in bankInfo.data) {
            if (bankObj.dataIdentifier == self.objInfo.data.lawyer.bank_id) {
                self.txtBankName.text = bankObj.name;
                self.currentBank = bankObj;
                break;
            }
        }
    }];
}

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [self changeCancelToAnythingUWantOf1:viewController andReplacementString:@"Usar foto"];

}
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    navigationController.delegate = self;
    [self changeCancelToAnythingUWantOf:navigationController andReplacementString:@""];
    for (UIViewController* vc in navigationController.childViewControllers) {
        [self changeCancelToAnythingUWantOf:vc andReplacementString:@""];
    }
    [self changeCancelToAnythingUWantOf:viewController andReplacementString:@""];
    for (UIViewController* vc in viewController.childViewControllers) {
        [self changeCancelToAnythingUWantOf:vc andReplacementString:@""];
    }
    [self changeCancelToAnythingUWantOf:viewController andReplacementString:@""];

    [self changeCancelToAnythingUWantOf1:viewController andReplacementString:@"Usar foto"];
    
}
- (void)loopChangeTitle:(UIView*)viewChange
{
    for(UIView *subView in viewChange.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)subView;
            NSLog(@"BUTTON: %@",btn.titleLabel.text);
            if([btn.titleLabel.text isEqualToString:@"Retake"]) {
                [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Use Photo"]){
                [btn setTitle:@"Usar foto" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Cancel"]){
                [btn setTitle:@"Cancelar" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Photos"]){
                [btn setTitle:@"Fotos" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"PHOTO"]){
                [btn setTitle:@"FOTO" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"On"]){
                [btn setTitle:@"Em" forState:UIControlStateNormal];
                break;
            }
            else if([btn.titleLabel.text isEqualToString:@"Off"]){
                [btn setTitle:@"Fora" forState:UIControlStateNormal];
                break;
            }
        }
        else if([subView isKindOfClass:[UILabel class]]){
            UILabel *lb = (UILabel*)subView;
            if([lb.text isEqualToString:@"Photos"]){
                lb.text = @"Fotos";
                break;
            }
            else if([lb.text isEqualToString:@"PHOTO"]){
                lb.text = @"FOTO";
                break;
            }
            else if([lb.text isEqualToString:@"Use Photo"]){
                lb.text = @"Usar foto";
                break;
            }
            else if([lb.text isEqualToString:@"Cancel"]){
                lb.text = @"Cancelar";
                break;
            }
            else if([lb.text isEqualToString:@"Retake"]){
                lb.text = @"Tirar outra";
                break;
            }
            else if([lb.text isEqualToString:@"On"]){
                lb.text = @"Em";
                break;
            }
            else if([lb.text isEqualToString:@"Off"]){
                lb.text = @"Fora";
                break;
            }

        }
        if (subView.subviews) {
            [self loopChangeTitle:subView];
        }
    }
}
- (void)changeCancelToAnythingUWantOf1:(UIViewController*)vc andReplacementString:(NSString*)title {
    
    for(UIView *subView in vc.view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)subView;
            if([btn.titleLabel.text isEqualToString:@"Use"]) {
                [btn setTitle:title forState:UIControlStateNormal];
                break;
            }
        }
        
        if (subView.subviews) {
            for (UIView *subSubView in subView.subviews) {
                if ([subSubView isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)subSubView;
                    if([btn.titleLabel.text isEqualToString:@"Use"]) {
                        [btn setTitle:title forState:UIControlStateNormal];
                        break;
                    }
                }
                
                if (subSubView.subviews) {
                    for (UIView *subSubSubView in subSubView.subviews) {
                        if ([subSubSubView isKindOfClass:[UIButton class]]) {
                            UIButton *btn = (UIButton *)subSubSubView;
                            if([btn.titleLabel.text isEqualToString:@"Use"]) {
                                [btn setTitle:title forState:UIControlStateNormal];
                                break;
                            } 
                        }
                    }
                }
            }
        }
    }
}
- (void)changeCancelToAnythingUWantOf:(UIViewController*)vc andReplacementString:(NSString*)title {
    [self loopChangeTitle:vc.view];
    vc.view.backgroundColor = [UIColor clearColor];
//    NSString *classname  = @"PLUIImageViewController";
//    if([[[vc class] description] isEqualToString:classname]){
        for(UIView *subView in vc.view.subviews) {
            if ([subView isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)subView;
                if([btn.titleLabel.text isEqualToString:@"Retake"]) {
                    [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                    break;
                }
                else if([btn.titleLabel.text isEqualToString:@"Use Photo"]){
                    [btn setTitle:@"Usar foto" forState:UIControlStateNormal];
                    break;
                }
                else if([btn.titleLabel.text isEqualToString:@"Cancel"]){
                    [btn setTitle:@"Cancelar" forState:UIControlStateNormal];
                    break;
                }
                else if([btn.titleLabel.text isEqualToString:@"Photos"]){
                    [btn setTitle:@"Fotos" forState:UIControlStateNormal];
                    break;
                }
                else if([btn.titleLabel.text isEqualToString:@"Retake"]){
                    [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                    break;
                }
                else if([btn.titleLabel.text isEqualToString:@"Use Photo"]){
                    [btn setTitle:@"Usar foto" forState:UIControlStateNormal];
                    break;
                }
                else if([btn.titleLabel.text isEqualToString:@"PHOTO"]){
                    [btn setTitle:@"FOTO" forState:UIControlStateNormal];
                    break;
                }
            }
            else if([subView isKindOfClass:[UILabel class]]){
                UILabel *lb = (UILabel*)subView;
                if([lb.text isEqualToString:@"Photos"]){
                    lb.text = @"Fotos";
                    break;
                }
                else if([lb.text isEqualToString:@"PHOTO"]){
                    lb.text = @"FOTO";
                    break;
                }
            }
            
            if (subView.subviews) {
                for (UIView *subSubView in subView.subviews) {
                    if ([subSubView isKindOfClass:[UIButton class]]) {
                        UIButton *btn = (UIButton *)subSubView;
                        if([btn.titleLabel.text isEqualToString:@"Retake"]) {
                            [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                            break;
                        }
                        else if([btn.titleLabel.text isEqualToString:@"Use Photo"]){
                            [btn setTitle:@"Usar foto" forState:UIControlStateNormal];
                            break;
                        }
                        else if([btn.titleLabel.text isEqualToString:@"Cancel"]){
                            [btn setTitle:@"Cancelar" forState:UIControlStateNormal];
                            break;
                        }
                        else if([btn.titleLabel.text isEqualToString:@"Photos"]){
                            [btn setTitle:@"Fotos" forState:UIControlStateNormal];
                            break;
                        }
                        else if([btn.titleLabel.text isEqualToString:@"PHOTO"]){
                            [btn setTitle:@"FOTO" forState:UIControlStateNormal];
                            break;
                        }
                        else if([btn.titleLabel.text isEqualToString:@"Retake"]){
                            [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                            break;
                        }
                    }
                    else if([subView isKindOfClass:[UILabel class]]){
                        UILabel *lb = (UILabel*)subView;
                        if([lb.text isEqualToString:@"Photos"]){
                            lb.text = @"Fotos";
                            break;
                        }
                        else if([lb.text isEqualToString:@"PHOTO"]){
                            lb.text = @"FOTO";
                            break;
                        }
                    }
                    if (subSubView.subviews) {
                        for (UIView *subSubSubView in subSubView.subviews) {
                            if ([subSubSubView isKindOfClass:[UIButton class]]) {
                                UIButton *btn = (UIButton *)subSubSubView;
                                if([btn.titleLabel.text isEqualToString:@"Retake"]) {
                                    [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                                    break;
                                }
                                else if([btn.titleLabel.text isEqualToString:@"Use Photo"]){
                                    [btn setTitle:@"Usar foto" forState:UIControlStateNormal];
                                    break;
                                }
                                else if([btn.titleLabel.text isEqualToString:@"Cancel"]){
                                    [btn setTitle:@"Cancelar" forState:UIControlStateNormal];
                                    break;
                                }
                                else if([btn.titleLabel.text isEqualToString:@"Photos"]){
                                    [btn setTitle:@"Fotos" forState:UIControlStateNormal];
                                    break;
                                }
                                else if([btn.titleLabel.text isEqualToString:@"PHOTO"]){
                                    [btn setTitle:@"FOTO" forState:UIControlStateNormal];
                                    break;
                                }
                                else if([btn.titleLabel.text isEqualToString:@"Retake"]){
                                    [btn setTitle:@"Tirar outra" forState:UIControlStateNormal];
                                    break;
                                }
                            }
                            else if([subView isKindOfClass:[UILabel class]]){
                                UILabel *lb = (UILabel*)subView;
                                if([lb.text isEqualToString:@"Photos"]){
                                    lb.text = @"Fotos";
                                    break;
                                }
                                else if([lb.text isEqualToString:@"PHOTO"]){
                                    lb.text = @"FOTO";
                                    break;
                                }
                            }
                        }
                    }
                }
            }
//    }
}
}

@end

//
//  LAIntroViewController.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPageViewControl.h"
@interface LAIntroViewController : LABaseViewController<UIScrollViewDelegate>
{
    NSMutableArray* arrData;
    NSMutableArray* imgDots;
}
@property (nonatomic) IBOutlet UIScrollView* scrFull;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIScrollView *scrIntroPath;
@property (weak, nonatomic) IBOutlet LAPageViewControl *pgControl;
@property (weak, nonatomic) IBOutlet UIButton *btEntrant;
- (IBAction)onEntrantPressed:(id)sender;
@end

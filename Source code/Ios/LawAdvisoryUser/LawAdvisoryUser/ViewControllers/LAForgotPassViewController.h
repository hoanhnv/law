//
//  LAForgotPassViewController.h
//  LawAdvisoryUser
//
//  Created by Apple on 8/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "AwesomeTextField.h"
@interface LAForgotPassViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btSendEmailForgot;
- (IBAction)onForgotPassSendEmailPressed:(id)sender;

@end

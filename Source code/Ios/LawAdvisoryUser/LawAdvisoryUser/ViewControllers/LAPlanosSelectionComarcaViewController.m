//
//  LAPlanosSelectionComarcaViewController.m
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanosSelectionComarcaViewController.h"
#import "LACountryViewController.h"
#import "LAMainTabbarViewController.h"
#import "AppDelegate.h"
#import "MainService.h"
#import "LAPlanosSelectionViewController.h"
#import "LAListDistrictByUF.h"
#import "LAPlanObj.h"
#import "LAPlanosConfirmViewController.h"
#import "LASignatureObj.h"
#import "LADistrictObj.h"
#import "LAPlanosEscolhaFormaPagementoVC.h"
#import "LAAssinatureBuyObj.h"
#import "LADistricts.h"
#import "LASignatureDistrict.h"

#define PLANOS_BOUGHT @"Você já possui um plano Parceiro %@ ativo.Para adicionar mais comarcas,selecione abaixo.Caso prefira alterar seu plano,vá até ASSINATURA, cancele o plano atual e assine um novo."
#import "LAAssinatureObj.h"

@interface LAPlanosSelectionComarcaViewController (){
    LAListDistrictByUF *lstData;
    LAAssinatureBuyObj *planSignature;
}

@end

@implementation LAPlanosSelectionComarcaViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reset) name:@"RESET_DATA" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NeedUpdatePlanosView:) name:@"NeedUpdatePlanosView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NeedUpdatePlanosView:) name:@"FinishDeletedBuyThesis" object:nil];
    
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    [self reloadData];
}
-(void)reloadData{
    if (self.assinatureInfo.data && self.assinatureInfo.data.plan != nil && self.assinatureInfo.data.plan.dataIdentifier>0) {
        self.lbPlanosBought.text = [NSString stringWithFormat:PLANOS_BOUGHT,self.assinatureInfo.data.plan.name];
        self.lbPlanosBought.numberOfLines = 0;
        [self.lbPlanosBought sizeToFit];
        self.listOldDistrictSelected = [self arrDistrictObj:self.assinatureInfo.data.districts];
    }
    else{
        NSData *registerData = [[NSUserDefaults standardUserDefaults]objectForKey:CURRENT_SIGNATURE_REGISTER];
        planSignature = [NSKeyedUnarchiver unarchiveObjectWithData:registerData];
        if (planSignature && ![LAUtilitys NullOrEmpty:planSignature.plan.name]) {
            self.lbPlanosBought.text = [NSString stringWithFormat:PLANOS_BOUGHT,planSignature.plan.name];
            self.lbPlanosBought.numberOfLines = 0;
            [self.lbPlanosBought sizeToFit];
            self.listOldDistrictSelected = [self arrDistrictObj:planSignature.districts];
        }
        else{
            self.lbPlanosBought.frame = CGRectMake(self.lbPlanosBought.frame.origin.x, self.lbPlanosBought.frame.origin.y, self.lbPlanosBought.frame.size.width, 0);
        }
    }
    
    self.vTop.frame = CGRectMake(self.vTop.frame.origin.x, CGRectGetMaxY(self.lbPlanosBought.frame), self.vTop.frame.size.width, self.vTop.frame.size.height);
    self.viewMiddle.frame = CGRectMake(self.viewMiddle.frame.origin.x, CGRectGetMaxY(self.vTop.frame), self.viewMiddle.frame.size.width, self.viewMiddle.frame.size.height);
    self.scrFullData.frame = CGRectMake(self.scrFullData.frame.origin.x,0, self.scrFullData.frame.size.width, [UIScreen mainScreen].bounds.size.height - 114);
    [self.scrFullData setContentSize:CGSizeMake(1, CGRectGetMaxY(self.viewMiddle.frame))];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initUI
{
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtCity];
    [LAUtilitys setRightImage:@"60x60_dropdown_gray" forTextField:self.txtEstado];
    self.arrRequiredField = [NSMutableArray arrayWithObjects:self.txtEstado,self.txtCity, nil];
}
- (void)initData
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didselectAddress:) name:@"didSelectCountry" object:nil];
    //    [self getUFs];
}
-(void)getUFs{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchUFs:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        self.listUFS = [[LAUFs alloc]initWithDictionary:data];
    }];
}
- (void)reset
{
    self.currentState = @"";
    self.currentDistrict = nil;
    self.txtCity.text = @"";
    self.txtEstado.text = @"";
    if (self.listDistrictSelected)
    {
        [self.listDistrictSelected removeAllObjects];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)onShowCity:(id)sender
{
    if ([LAUtilitys NullOrEmpty:self.txtEstado.text])
    {
        [self.txtEstado becomeFirstResponder];
        [self onShowState:nil];
    }
    else
    {
        [self showCityList:TYPE_NEWCITY];
    }
    
}
- (BOOL)checkIsValid
{
    BOOL isCheck = YES;
    for (AwesomeTextField* txt in self.arrRequiredField)
    {
        if ([LAUtilitys isEmptyOrNull:txt.text])
        {
            [self showMessage:[NSString stringWithFormat:@"%@ not empty",txt.placeholder] withPoistion:nil];
            isCheck = NO;
            break;
        }
    }
    return isCheck;
}
- (IBAction)onShowState:(id)sender
{
    //    listEstado = [NSMutableArray arrayWithArray:self.listUFS.data];
    listEstado = [NSMutableArray arrayWithObjects:@"DF",@"RJ",@"SP", nil];
    [self showCityList:TYPE_STATE];
}
- (void)didselectedList:(NSMutableArray*)listSelected
{
    self.listDistrictSelected = listSelected;
}
- (void)showCityList:(TYPE_POPUP)type
{
    LACountryViewController* countryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LACountryViewController"];
    countryVC.isShouldBackButton = NO;
    if (type == TYPE_NEWCITY)
    {
        /**
         *  Note
         */
        NSMutableArray *arrTemp = [NSMutableArray array];
        for (LADistrictObj*obj in listCity) {
            for (LADistrictObj *item in self.listOldDistrictSelected) {
                if ([obj.name isEqualToString:item.name]) {
                    [arrTemp addObject:obj];
                }
            }
        }
        for (LADistrictObj*obj in listCity) {
            for (LADistrictObj *item in self.listDistrictSelected) {
                if ([obj.name isEqualToString:item.name]) {
                    [arrTemp addObject:obj];
                }
            }
        }
        [listCity removeObjectsInArray:arrTemp];
        countryVC.arrCountry = listCity;
        countryVC.currentDistrict = self.currentDistrict;
        //        countryVC.isMultipleSelect = YES;
        countryVC.arrSelected = self.listDistrictSelected;
    }
    else
    {
        countryVC.arrCountry = listEstado;
        countryVC.currentState = self.currentState;
    }
    countryVC.delegate = self;
    countryVC.popType = type;
    countryVC.isTransparentNavigation = YES;
    
    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:countryVC];
    [self presentViewController:navi animated:YES completion:nil];
}

- (void)getCityList
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchDistrictByUF:self.txtEstado.text withBlock:^(NSInteger errorCode, NSString *message, id data) {
        self.txtCity.text = STRING_EMPTY;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        lstData = [[LAListDistrictByUF alloc]initWithDictionary:data];
        if (data) {
            listCity = [NSMutableArray arrayWithArray:lstData.data];
        }
    }];
}
- (void)didselectAddress:(NSNotification*)notif
{
    id obj = [notif object];
    if (obj && [obj isKindOfClass:[LADistrictObj class]])
    {
        self.currentDistrict = obj;
        self.txtCity.text = self.currentDistrict.name;
    }
    else{
        long typePoup = [[[notif userInfo] objectForKey:@"TYPEPOUP"] integerValue];
        if (typePoup == TYPE_STATE) {
            self.currentState = obj;
            self.txtEstado.text = self.currentState;
            [self getCityList];
        }
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txtEstado || textField == self.txtCity)
    {
        return NO;
    }
    return YES;
}
- (IBAction)onConfirm:(id)sender
{
    if ([self checkIsValid])
    {
        if (self.currentDistrict)
        {
            self.listDistrictSelected = [NSMutableArray arrayWithObject:self.currentDistrict];
        }
        if (self.assinatureInfo.data && self.assinatureInfo.data.plan != nil && self.assinatureInfo.data.plan.dataIdentifier>0) {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_PLAN_CART_ITEM];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_DISTRICT_CART_ITEM];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_SIGNATURE_CART_ITEM];
            
            LAPlanosEscolhaFormaPagementoVC *pagarVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosEscolhaFormaPagementoVC class])];
            //            planSignature.plan.discountAll = YES;
            [AppDelegate sharedDelegate].isDiscountAll = YES;
            pagarVC.lanObj = self.assinatureInfo.data.plan;
            pagarVC.listDistric = self.listDistrictSelected;
            
            [self.navigationController pushViewController:pagarVC animated:YES];
        }
        else{
            if (planSignature && planSignature.plan != nil && planSignature.plan.dataIdentifier>0) {
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_PLAN_CART_ITEM];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_DISTRICT_CART_ITEM];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_SIGNATURE_CART_ITEM];
                
                LAPlanosEscolhaFormaPagementoVC *pagarVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosEscolhaFormaPagementoVC class])];
                planSignature.plan.discountAll = YES;
                [AppDelegate sharedDelegate].isDiscountAll = YES;
                pagarVC.lanObj = planSignature.plan;
                pagarVC.listDistric = self.listDistrictSelected;
                
                [self.navigationController pushViewController:pagarVC animated:YES];
                
            }
            else{
                LAPlanosSelectionViewController* planosSelectionVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosSelectionViewController class])];
                planosSelectionVC.strComacar = self.txtCity.text;
                planosSelectionVC.listDistrictSelected = self.listDistrictSelected;
                [self.navigationController pushViewController:planosSelectionVC animated:YES];
            }
        }
    }
}
-(NSMutableArray*)arrDistrictObj:(NSArray*)districts{
    NSMutableArray *arrData = [NSMutableArray array];
    for (LADistricts *item in districts) {
        LADistrictObj *obj = [LADistrictObj new];
        obj.dataIdentifier = item.district.districtIdentifier;
        obj.name = item.district.name;
        obj.createdAt = item.district.createdAt;
        obj.updatedAt = item.district.updatedAt;
        obj.deletedAt = item.district.deletedAt;
        obj.uf = item.district.uf;
        [arrData addObject:obj];
    }
    return arrData;
}
-(void)NeedUpdatePlanosView:(NSNotification*)notification{
    [self reloadData];
}
-(void)FinishDeletedBuyThesis:(NSNotification*)notification{
    self.assinatureInfo = nil;
    [self reloadData];
}
@end

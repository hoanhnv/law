//
//  LASignaturesViewController.m
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASignaturesViewController.h"
#import "LAAssinaturaTableViewCell.h"
#import "LAAssinaturaHeaderView.h"
#import "LAAssinaturaHeaderSection.h"
#import "MainService.h"
#import "UIView+NibInitializer.h"
#import "MBProgressHUD.h"
#import "LASignatureCancelViewController.h"
#import "LAAssinatureHeaderEmpty.h"
#import "LAAssinature.h"
#import "LAAssinatureObj.h"
#import "LASignatureHistoryViewController.h"
#import "LAPricePlanosViewController.h"
#import "LASignatureTese.h"
#import "LAAssignatureFooterView.h"
#import "UIView+NibInitializer.h"
#import "LALawSuitCategory.h"
#import "AppDelegate.h"
#import "LAPagarCatartaoViewController.h"
//#import "LAPagarCatartaoViewController.m"
#import "LAServiceTesesViewController.h"
#import "LATesesMainViewController.h"
#import "LAPlanObj.h"

@interface LASignaturesViewController ()<UITableViewDelegate,UITableViewDataSource,LAAssinaturaHeaderViewDelegate>
{
    NSDictionary *dictData;
    //    LASignatureTese *lstTese;
    //    LAAssinature *assinatureData;
    BOOL isLoaded1;
    BOOL isLoaded2;
}
@end

@implementation LASignaturesViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FinishDeletedBuyThesis:) name:@"FinishDeletedBuyThesis" object:nil];
    self.mainScrollView.frame = CGRectMake(self.mainScrollView.frame.origin.x, self.mainScrollView.frame.origin.y - 20, self.mainScrollView.frame.size.width,[UIScreen mainScreen].bounds.size.height - 64);
    [self.tblTeses registerNib:[UINib nibWithNibName:@"LAAssinaturaTableViewCell" bundle:nil] forCellReuseIdentifier:@"LAAssinaturaTableViewCell"];
    LAAssignatureFooterView* footer = [[LAAssignatureFooterView alloc]initWithNibNamed:@"LAAssignatureFooterView"];
    
    [footer.btnSelectionTeses addTarget:self action:@selector(doNextToTeses) forControlEvents:UIControlEventTouchUpInside];
    LAAssignatureFooterView* header = [[LAAssignatureFooterView alloc]initWithNibNamed:@"LAAssignatureFooterView"];
    
    [header.btnSelectionTeses addTarget:self action:@selector(doNextToTeses) forControlEvents:UIControlEventTouchUpInside];
    
    self.tblTeses.tableFooterView = footer;
    self.tblTeses.tableHeaderView = header;
    [self.tblTeses reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    [self reloadDataNew];
    
}

-(void)reloadDataNew{
    if (self.assinatureInfo.data && self.assinatureInfo.data.plan != nil && self.assinatureInfo.data.plan.dataIdentifier>0) {
        [self.vAssintureHeader setupView:self.assinatureInfo.data];
        self.vAssintureHeader.delegate = self;
        [self.vAssintureHeader.btnHistory addTarget:self action:@selector(doShowHistory) forControlEvents:UIControlEventTouchUpInside];
        [self.vAssintureHeader.btnChangeCard addTarget:self action:@selector(doChangeCard) forControlEvents:UIControlEventTouchUpInside];
        self.tblTeses.hidden = NO;
        self.vHeaderTese.hidden = NO;
        if (![self.lstTese.data count]) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [MainService lawyerGetSignatureTheses:^(NSInteger errorCode, NSString *message, id data) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (self.lstTese.success) {
                }
                else{
                    [self showMessage:self.lstTese.message withPoistion:nil];
                }
                self.tblTeses.frame = CGRectMake(self.tblTeses.frame.origin.x, CGRectGetMaxY(self.vHeaderTese.frame) +5, self.tblTeses.frame.size.width, 112*[self.lstTese.data count] + 50);
                self.tblTeses.scrollEnabled = NO;
                self.mainScrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.tblTeses.frame)+ 100);
            }];
        }
        else{
            [self.tblTeses reloadData];
            [self updateViewIfNeed];
        }
    }
    else{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //        [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_SIGNATURE_REGISTER];
        self.vEmpty.btnHistory.hidden = YES;
        self.tblTeses.hidden = YES;
        self.vHeaderTese.hidden = YES;
        [self updateViewIfNeed];
    }
}
-(void)updateViewIfNeed{
    if (self.assinatureInfo.data && self.assinatureInfo.data.plan != nil && self.assinatureInfo.data.plan.dataIdentifier>0) {
        self.vAssintureHeader.frame = CGRectMake(self.vAssintureHeader.frame.origin.x, self.vAssintureHeader.frame.origin.y, self.vAssintureHeader.frame.size.width, 340);
        self.vEmpty.frame = CGRectMake(self.vEmpty.frame.origin.x, self.vEmpty.frame.origin.y, self.vEmpty.frame.size.width, 0);
        self.vAssintureHeader.hidden = NO;
        self.vEmpty.hidden = YES;
        self.vHeaderTese.frame = CGRectMake(self.vHeaderTese.frame.origin.x, CGRectGetMaxY(self.vAssintureHeader.frame) +5, self.vHeaderTese.frame.size.width, self.vHeaderTese.frame.size.height);
    }
    else{
        self.vAssintureHeader.hidden = YES;
        self.vEmpty.hidden = NO;
        self.vAssintureHeader.frame = CGRectMake(self.vAssintureHeader.frame.origin.x, self.vAssintureHeader.frame.origin.y, self.vAssintureHeader.frame.size.width, 0);
        self.vEmpty.frame = CGRectMake(self.vEmpty.frame.origin.x, self.vEmpty.frame.origin.y, self.vEmpty.frame.size.width, 160);
        self.vHeaderTese.frame = CGRectMake(self.vHeaderTese.frame.origin.x, CGRectGetMaxY(self.vEmpty.frame) +5, self.vHeaderTese.frame.size.width, self.vHeaderTese.frame.size.height);
        
    }
    
    self.tblTeses.frame = CGRectMake(self.tblTeses.frame.origin.x, CGRectGetMaxY(self.vHeaderTese.frame) +5, self.tblTeses.frame.size.width, 112*[self.lstTese.data count] + 2*50);
    self.tblTeses.scrollEnabled = NO;
    self.mainScrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.tblTeses.frame)+ 100);
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.tblTeses == tableView) {
        return 1;
    }
    else{
        return 0;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.tblTeses == tableView) {
        return [self.lstTese.data count];
    }
    else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LAAssinaturaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LAAssinaturaTableViewCell" forIndexPath:indexPath];
    [cell setupCell:[self.lstTese.data objectAtIndex:indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 112.0f;
}

-(void)doRemovePay:(id)obj{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LASignatureCancelViewController *cancelVC = [storyboard instantiateViewControllerWithIdentifier:@"LASignatureCancelViewController"];
    cancelVC.objCancel = dictData;
    cancelVC.titleNavigation = obj;
    cancelVC.isFinishAction = false;
    [self.navigationController pushViewController:cancelVC animated:YES];
}
-(void)doChangeCard{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAPagarCatartaoViewController *card = [storyboard instantiateViewControllerWithIdentifier:@"LAPagarCatartaoViewController"];
    card.needUpdateView = YES;
    card.natureInfo = self.assinatureInfo;
//    card.checkChangeCard = YES;
    [self.navigationController pushViewController:card animated:YES];
        //LAPagarCatartaoViewController
}
-(void)doShowHistory{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LASignatureHistoryViewController *historyVC = [storyboard instantiateViewControllerWithIdentifier:@"LASignatureHistoryViewController"];
    historyVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:historyVC animated:YES];
}
-(void)doNextToTeses{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LATesesMainViewController *serviceTesesMain = [storyboard instantiateViewControllerWithIdentifier:@"LATesesMainViewController"];
    serviceTesesMain.buyThese = [self buildSelectedThese];
    serviceTesesMain.lanObj = self.assinatureInfo.data.plan;
    [self.navigationController pushViewController:serviceTesesMain animated:YES];
}

-(NSMutableArray*)buildSelectedThese{
    NSMutableArray *arrData = [NSMutableArray array];
    NSInteger countItem = [self.lstTese.data count];
    for (NSInteger i=0;i< countItem;i++) {
        LASignatureTeseObj *s1 = [self.lstTese.data objectAtIndex:i];
        s1.lawsuitThesis.initialPrice = [NSString stringWithFormat:@"%.2f",s1.initialFees];
        s1.lawsuitThesis.percentagePrice = [NSString stringWithFormat:@"%.2f",s1.percentageInSuccess];
        LALawSuitCategory* suitCate = [[LALawSuitCategory alloc]init];
        suitCate = s1.lawsuitThesis.category;
        if (!suitCate.arrData) {
            suitCate.arrData = [NSMutableArray array];
        }
        else{
            [suitCate.arrData removeAllObjects];
        }
        [suitCate.arrData addObject:s1.lawsuitThesis];
        
        for (NSInteger j = countItem - 1; j>i;j--) {
            LASignatureTeseObj *s2 = [self.lstTese.data objectAtIndex:j];
            if (s1.lawsuitThesis.categoryId == s2.lawsuitThesis.categoryId) {
                [suitCate.arrData addObject:s2.lawsuitThesis];
            }
            if (j == countItem - 1 && ![arrData containsObject:suitCate]) {
                [arrData addObject:suitCate];
            }
        }
        if (countItem == 1) {
            [arrData addObject:suitCate];
        }
    }
    return arrData;
}

-(void)FinishDeletedBuyThesis:(NSNotification*)notification{
    self.assinatureInfo = nil;
    [self reloadDataNew];
}
@end

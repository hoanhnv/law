//
//  LARecusaReasonViewController.h
//  JustapLawyer
//
//  Created by MAC on 11/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LARefusalObj.h"
#import "LAListRefusal.h"
@protocol RecusaReasonDelegate <NSObject>

- (void)onchooseRecusae:(LARefusalObj*)objChoose andIndexPath:(NSIndexPath*)indexPath;

@end
@interface LARecusaReasonViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UILabel *lbCateName;
@property (weak, nonatomic) IBOutlet UITableView *tbvInfo;
@property (weak, nonatomic) IBOutlet UIView *vHeaderCate;
@property (nonatomic) NSIndexPath* indexPath;
@property (assign) id<RecusaReasonDelegate> delegate;
@property (nonatomic) LAListRefusal *lstRefusal;
@property (nonatomic) IBOutlet UIButton* btChooseType;
@property (nonatomic) IBOutlet UILabel* txtCurrentType;
@property (nonatomic) IBOutlet UIView* popupType;
@property (nonatomic) IBOutlet UIButton* btEnviar;
@property (nonatomic) IBOutlet UITextView* txtEnterText;

- (IBAction)doBack:(id)sender;
@end

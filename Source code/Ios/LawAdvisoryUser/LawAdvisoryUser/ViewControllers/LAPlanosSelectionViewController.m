//
//  LAPlanosSelectionViewController.m
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanosSelectionViewController.h"
#import "LAHeaderSectionPlanosSelectionView.h"
#import "LAPlanosSelectionTableViewCell.h"
#import "LAHeaderPlanosSelectionView.h"
#import "AppDelegate.h"
#import "MainService.h"
#import "LAPlanObj.h"
#import "LAListPlans.h"
#import "UIView+NibInitializer.h"
#import "PlanSectionHeaderView.h"
#import "LAPlanosConfirmViewController.h"
#import "LAPlanosEscolhaFormaPagementoVC.h"

static float HEIGHT_SECTION_PLANOS_SELECTION = 50.0f;
static float HEIGHT_CELL_PLANOS_SELECTION = 190.0f;
@interface LAPlanosSelectionViewController ()
{
}
@property (nonatomic, strong) LAListPlans *lstData;
@end

@implementation LAPlanosSelectionViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.tbData.tableHeaderView = [[PlanSectionHeaderView alloc]initWithNibNamed:@"PlanSectionHeaderView"];
}

-(void)loadData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchListPlan:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.lstData = [[LAListPlans alloc]initWithDictionary:data];
        [self.tbData reloadData];
    }];
}
- (void)onSelectionAtIndex:(NSInteger)index
{
    
    //    LAPlanosConfirmViewController* confirmVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosConfirmViewController class])];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_PLAN_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_DISTRICT_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_SIGNATURE_CART_ITEM];
    
    //    confirmVC.lanObj = [self.lstData.data objectAtIndex:index];
    //    confirmVC.listDistric = self.listDistrictSelected;
    
    //    [self.navigationController pushViewController:confirmVC animated:YES];
    
    LAPlanosEscolhaFormaPagementoVC *pagarVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosEscolhaFormaPagementoVC class])];
    pagarVC.lanObj = [self.lstData.data objectAtIndex:index];
    pagarVC.listDistric = self.listDistrictSelected;
    
    [self.navigationController pushViewController:pagarVC animated:YES];
    
}
- (void)initUI
{
    self.title = @"Planos";
}
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self.tbData registerNib:[UINib nibWithNibName:NSStringFromClass([LAPlanosSelectionTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPlanosSelectionTableViewCell class])];
}
- (void)initData
{
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGHT_CELL_PLANOS_SELECTION;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.lstData.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LAPlanosSelectionTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LAPlanosSelectionTableViewCell class]) forIndexPath:indexPath];
    [cell setupCell:self.lstData.data[indexPath.row] withComacar:self.strComacar];
    cell.indexPath = indexPath;
    cell.delegate = self;
    return cell;
}

@end

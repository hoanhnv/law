//
//  LASignatureHistoryViewController.m
//  JustapLawyer
//
//  Created by Mac on 11/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASignatureHistoryViewController.h"
#import "LAListSignatureHistory.h"
#import "SignatureHistoryTableViewCell.h"
#import "LAHistoryDataObj.h"

@interface LASignatureHistoryViewController (){
    LAListSignatureHistory *signatureHistory;
}

@end

@implementation LASignatureHistoryViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tblHistory registerNib:[UINib nibWithNibName:@"SignatureHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"SignatureHistoryTableViewCell"];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    self.navigationItem.title = @"Planos";
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [signatureHistory.data count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SignatureHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SignatureHistoryTableViewCell" forIndexPath:indexPath];
    LAHistoryDataObj *obj = [signatureHistory.data objectAtIndex:indexPath.row];
    [cell setupCell:obj];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 144.0f;
}

-(void)reloadData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService lawyerGetBillSignature:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        signatureHistory = [[LAListSignatureHistory alloc]initWithDictionary:data];
        if (signatureHistory.success) {
            [self.tblHistory reloadData];
        }
        else{
            [self showMessage:message withPoistion:nil];
        }
        
    } ];
}

@end

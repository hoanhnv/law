//
//  LAWitnessDetailViewController.h
//  JustapLawyer
//
//  Created by Mac on 11/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAWitnesses.h"
@interface LAWitnessDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbRG;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbCEP;
@property (weak, nonatomic) IBOutlet UILabel *LBEstado;
@property (weak, nonatomic) IBOutlet UILabel *lbCidade;
@property (weak, nonatomic) IBOutlet UILabel *lbEndereco;
@property (weak, nonatomic) IBOutlet UILabel *lbComplement;
@property (nonatomic,strong) LAWitnesses *witnessObj;
- (IBAction)doCloseView:(id)sender;
@end

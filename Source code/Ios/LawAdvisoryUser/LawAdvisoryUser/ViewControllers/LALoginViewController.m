//
//  LALoginViewController.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LALoginViewController.h"
#import "LAForgotPassViewController.h"
#import "LARegisterViewController.h"
#import "LAFaleViewController.h"
#import "MBProgressHUD.h"
#import "LAAccountService.h"
#import "LAFrequentesViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>
#import <Twitter/Twitter.h>
#import "AppDelegate.h"
#import "MainService.h"
#import "LAStatusAccountObj.h"
#import "LAAnswerForgotPass.h"
#import <linkedin-sdk/LISDK.h>
#import <linkedin-sdk/LISDKSessionManager.h>
#import "TermAndServiceViewController.h"

static NSString * const kGooglePlus_ClientId = @"759901828094-u9ck4bouh1sdfkakqcafq5gldmcaqnvs.apps.googleusercontent.com";

@interface LALoginViewController()<LAAnswerPopupDelegate>{
    
}

@end

@implementation LALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}
- (void)initUI
{
    [LAUtilitys setButtonTextUnderLine:self.btFogotPass];
    [LAUtilitys setButtonTextUnderLine:self.btDuvidas];
    [LAUtilitys setButtonTextUnderLine:self.btFale];
    [LAUtilitys setButtonTextUnderLine:self.btnTermos];
    CGSize size = self.scrFullData.contentSize;
    size.height = CGRectGetMaxY(self.btnTermos.frame) + 10;
    [self.scrFullData setContentSize:size];
}
- (void)initData
{
    
}
- (IBAction)onForgotPassPressed:(id)sender
{
    LAForgotPassViewController* forgotPassVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAForgotPassViewController class])];
    forgotPassVC.isShouldBackButton = YES;
    forgotPassVC.isTransparentNavigation = YES;
    [self.navigationController pushViewController:forgotPassVC animated:YES];
}
- (void)getInfoWithAccount:(LAAccountObj*)accountObj
{
    if ([[FBSDKAccessToken currentAccessToken]hasGranted:@"email"])
    {
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id,name,email,birthday,about" forKey:@"fields"];
        FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:parameters];
        
        FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
        
        [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id resultHere, NSError *error) {
            
            if(resultHere)
            {
                if ([resultHere objectForKey:@"email"]) {
                    
                    accountObj.accountEmail = [resultHere objectForKey:@"email"];
                }
                if ([resultHere objectForKey:@"name"]) {
                    accountObj.accountName = [resultHere objectForKey:@"name"];
                    
                }
                if ([resultHere objectForKey:@"birthday"])
                {
                    accountObj.accountDateOfBirth = [LAUtilitys getNSDateFromDateString:[resultHere objectForKey:@"birthday"] withFormat:FORMAT_MM_DD_YY].timeIntervalSince1970;
                }
                if ([resultHere objectForKey:@"about"])
                {
                    accountObj.about = [LAUtilitys getDefaultString:[resultHere objectForKey:@"about"]];
                }
                if ([resultHere objectForKey:@"accountSite"])
                {
                    accountObj.accountSite = [LAUtilitys getDefaultString:[resultHere objectForKey:@"accountSite"]];
                }
            }
            [self goToRegisterViewController:accountObj];
        }];
        [connection start];
    }
    else
    {
        [self goToRegisterViewController:nil];
    }
}
- (IBAction)onFacebookPressed:(id)sender {
    LAAccountObj* accountObj = [LAAccountObj new];
    if ([FBSDKAccessToken currentAccessToken])
    {
        [self getInfoWithAccount:accountObj];
    }
    else
    {
        FBSDKLoginManager *loginFace = [[FBSDKLoginManager alloc] init];
        loginFace.loginBehavior = FBSDKLoginBehaviorWeb;
        [loginFace logInWithReadPermissions:@[@"public_profile",@"email",@"user_website"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (result.isCancelled)
            {
                [self showMessage:error.localizedFailureReason withPoistion:nil];
            }
            else
                if (error == nil) {
                    if ([FBSDKAccessToken currentAccessToken]) {
                        [self getInfoWithAccount:accountObj];
                    }
                    else{
                        // TODO
                        [self goToRegisterViewController:nil];
                    }
                    [LADataCenter shareInstance].facebookToken = [FBSDKAccessToken currentAccessToken].tokenString;
                    
                }
                else{
                    [self showMessage:error.localizedFailureReason withPoistion:nil];
                }
        }];
    }
    
}

- (IBAction)onTwitterPressed:(id)sender {
    if([Twitter sharedInstance].sessionStore.session != nil){
        [LADataCenter shareInstance].twitterID = [Twitter sharedInstance].sessionStore.session.userID;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self goLoginViewFromTwitter:[Twitter sharedInstance].sessionStore.session.userID];
    }
    else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (session) {
                [LADataCenter shareInstance].twitterID = session.userID;
                [self goLoginViewFromTwitter:session.userID];
            } else {
                [self showMessage:[error localizedDescription] withPoistion:nil];
            }
        }];
    }
}

- (IBAction)onGPlusPressed:(id)sender {
    [self onLinklePressed:nil];
    return;
    GIDGoogleUser *currentUser = [[GIDSignIn sharedInstance] currentUser];
    if(currentUser != nil){
        LAAccountObj* objAccount = [LAAccountObj new];
        objAccount.accountEmail = currentUser.profile.email;
        objAccount.accountName = currentUser.profile.name;
        objAccount.avataURL = [[currentUser.profile imageURLWithDimension:100]absoluteString];
        [self goToRegisterViewController:objAccount];
    }
    else{
        [[GIDSignIn sharedInstance] signIn];
    }
}
- (IBAction)onLinklePressed:(id)sender
{
    [LISDKSessionManager clearSession];
    [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION,LISDK_EMAILADDRESS_PERMISSION, nil] state:nil showGoToAppStoreDialog:YES successBlock:^(NSString *stringSuccess) {
        NSLog(@"%s","success called!");
        LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
        NSLog(@"Session : %@", session.description);
        [[LISDKAPIHelper sharedInstance] getRequest:@"https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,formatted-name,picture-url)?format=json"
                                            success:^(LISDKAPIResponse *response)
         {
             NSData* data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
             if (dictResponse)
             {
                 LAAccountObj* objAccount = [LAAccountObj new];
                 objAccount.accountEmail = dictResponse[@"emailAddress"];
                 objAccount.accountName = dictResponse[@"formattedName"];
                 [self performSelectorOnMainThread:@selector(goToRegisterViewController:) withObject:objAccount waitUntilDone:YES];
             }
             NSLog(@"Authenticated user name : %@ %@", [dictResponse valueForKey: @"firstName"], [dictResponse valueForKey: @"lastName"]);
         } error:^(LISDKAPIError *apiError)
         {
             NSLog(@"Error : %@", apiError);
         }];
        
    } errorBlock:^(NSError *error) {
        
    }];
    
}
- (BOOL)checkValid
{
    BOOL check = YES;
    if ([LAUtilitys isEmptyOrNull:self.txtPassword.text])
    {
        //        [self showMessage:NSLocalizedString(@"Please enter email", "@") withPoistion:nil];
        return NO;
    }
    if ( [LAUtilitys isEmptyOrNull:self.txtEmail.text])
    {
        //        [self showMessage:NSLocalizedString(@"Please enter password", "@") withPoistion:nil];
        return NO;
    }
    if(![LAUtilitys validateEmailWithString:_txtEmail.text]){
        //        [self showMessage:NSLocalizedString(@"Please input valid email", "@") withPoistion:nil];
        return NO;
    }
    return check;
}
- (IBAction)onLoginPressed:(id)sender {
//    self.txtEmail.text = @"badaraujo84@gmail.com";
//    self.txtPassword.text = @"123456";

    if ([self checkValid])
    {
        [self.view endEditing:YES];
        NSString *deviceToken = [AppDelegate sharedDelegate].strDeviceToken;
        if([LAUtilitys NullOrEmpty:deviceToken]){
            deviceToken = [[NSUserDefaults standardUserDefaults]stringForKey:@"DEVICE_TOKEN"];
        }
        NSString *strUUID = [LAUtilitys UUIDString];
        ;
        if ([LAUtilitys NullOrEmpty:deviceToken]) {
                deviceToken = @"justap";
        }
        NSDictionary *dictDeviceData = @{@"device_token":deviceToken,
                                             @"uuid":strUUID,
                                             @"type":[NSNumber numberWithInteger:1],
                                             @"model":[[UIDevice currentDevice]model],
                                             @"platform":[[UIDevice currentDevice]systemName],
                                             @"version":[[UIDevice currentDevice] systemVersion]};
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [LAAccountService loginWithEmail:self.txtEmail.text andPassword:self.txtPassword.text withDeviceData:dictDeviceData wCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (data && [data isKindOfClass:[NSDictionary class]])
            {
                BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
                message = [data objectForKey:KEY_RESPONSE_MESSAGE];
                if (success)
                {
                    NSString* token = data[KEY_RESPONSE_DATA][KEY_RESPONSE_TOKEN];
                    NSArray* arrProfile = data[KEY_RESPONSE_DATA][KEY_RESPONSE_PROFILE];
                    if (![arrProfile containsObject:@"lawyer"]) {
                        [self showMessage:@"Nome ou senha incorreta" withPoistion:nil];
                        return ;
                    }
                    if (![LAUtilitys NullOrEmpty:token]) {
                        [[LADataCenter shareInstance] saveToken:token error:nil];
                        [[LADataCenter shareInstance] saveEmail:self.txtEmail.text error:nil];
                        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"USER_NAME"];
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KEY_CHANGE_ACCOUNT object:nil];
                        [self checkStatus:[NSString stringWithFormat:@"Bearer %@",token]];
                    }
                    else{                        
                        [self showMessage:MSG_0001 withPoistion:nil];
                    }
                }
                else
                {
                    [self showFeedBackView:success];
                }
            }
            else
            {
                [self showMessage:message withPoistion:nil];
            }
        }];
    }
}
-(void)showFeedBackView:(BOOL)isFeedback{
    LAAnswerForgotPass *answerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LAAnswerForgotPass"];
    answerVC.bSuccess = isFeedback;
    answerVC.delegate = self;
    answerVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:answerVC animated:YES completion:^{
        
    }];
}
#pragma mark -- LAAnswerPopupDelegate
- (void)onForgotPassPressed
{
    [self onForgotPassPressed:nil];
}
-(void)onExitPressed{
    //    self.txtEmail.text = STRING_EMPTY;
    //    self.txtPassword.text = STRING_EMPTY;
}

- (void)goLoginViewFromTwitter:(NSString *)userID
{
    NSString *urlRequest = @"https://api.twitter.com/1.1/users/show.json";
    NSDictionary *params = @{@"user_id": userID};
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
    NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                     URL:urlRequest
                                              parameters:params
                                                   error:nil];
    
    [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSDictionary *resData = [NSJSONSerialization JSONObjectWithData: data options:kNilOptions error:nil];
        LAAccountObj* objAccount = [LAAccountObj new];
        objAccount.accountEmail = resData[@"screen_name"];
        objAccount.accountCity = resData[@"location"];
        objAccount.accountName = resData[@"name"];
        objAccount.avataURL = resData[@"profile_image_url"];
        objAccount.about = resData[@"description"];
        NSLog(@"Twitter info :%@",resData);
        [self goToRegisterViewController:objAccount];
    }];
}

- (IBAction)onRegisterPressed:(id)sender {
    LARegisterViewController* registerVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARegisterViewController class])];
    registerVC.isShouldBackButton = YES;
    registerVC.isTransparentNavigation = YES;
    [self.navigationController pushViewController:registerVC animated:YES];
}
-(void)goToRegisterViewController:(id)dataInfo{
    LARegisterViewController* registerVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARegisterViewController class])];
    registerVC.isShouldBackButton = YES;
    registerVC.isTransparentNavigation = YES;
    registerVC.currentAccount = dataInfo;
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (IBAction)onDuvidasPressed:(id)sender {
    LAFrequentesViewController* duvidasVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAFrequentesViewController class])];
    duvidasVC.isShouldBackButton = YES;
    duvidasVC.isShouldShowBottombar = YES;
    duvidasVC.isShowGreenNavigation = NO;
    duvidasVC.isShouldShowBigHeightNavigationBar = NO;
    [self.navigationController pushViewController:duvidasVC animated:YES];
}

- (IBAction)onFalePressed:(id)sender {
    LAFaleViewController* faleVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAFaleViewController class])];
    [self.navigationController pushViewController:faleVC animated:YES];
}


#pragma mark - GIDSignInDelegate
// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    if (error == nil) {
        [LADataCenter shareInstance].googlePlusToken = user.userID;
        LAAccountObj* objAccount = [LAAccountObj new];
        objAccount.accountEmail = user.profile.email;
        objAccount.accountName = user.profile.name;
        objAccount.avataURL = [[user.profile imageURLWithDimension:100]absoluteString];
        [self goToRegisterViewController:objAccount];
    }
    else
    {
        // TODO
    }
}

-(void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error{
    [self showMessage:MSG_0002 withPoistion:nil];
}

- (void)checkStatus:(NSString*)token
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService checkStatus:token withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[LAStatusAccountObj class]])
        {
            [self navigationWithStatusAccount:data];
        }
        else
        {
            [self showMessage:message withPoistion:nil];
        }
    }];
}
- (IBAction)doTermos:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    TermAndServiceViewController *termAndServiceVC  = [storyBoard instantiateViewControllerWithIdentifier:@"TermAndServiceViewController"];
    [self.navigationController pushViewController:termAndServiceVC animated:YES];
}
@end

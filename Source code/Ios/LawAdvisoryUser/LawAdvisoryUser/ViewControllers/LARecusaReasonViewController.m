//
//  LARecusaReasonViewController.m
//  JustapLawyer
//
//  Created by MAC on 11/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LARecusaReasonViewController.h"
#import "LARecusarTableViewCell.h"
#import "MainService.h"
@interface LARecusaReasonViewController ()<UITextViewDelegate>
{
    
    LARefusalObj* currentRecusa;
}
@end

@implementation LARecusaReasonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtEnterText.delegate = self;
    currentRecusa = [self.lstRefusal.data objectAtIndex:0];
    UITapGestureRecognizer *tapSelectCate = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doOpenCategories:)];
    [self.vHeaderCate addGestureRecognizer:tapSelectCate];
    [self.tbvInfo registerNib:[UINib nibWithNibName:NSStringFromClass([LARecusarTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LARecusarTableViewCell class])];
    [self fecthAllReason];
    [self.tbvInfo reloadData];
    self.tbvInfo.hidden = YES;
    // Do any additional setup after loading the view.
}
- (void)fecthAllReason{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawyerReasons:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        LAListRefusal *lstData = [[LAListRefusal alloc]initWithDictionary:data];
        if (lstData.success) {
            //Using lstData.data
            self.lstRefusal = lstData;
            LARefusalObj* objFirst = [self.lstRefusal.data objectAtIndex:0];
            self.lbCateName.text = objFirst.name;
        }
        else{
            [self showMessage:lstData.message withPoistion:nil];
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:0.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initUI{
    self.tbvInfo.layer.borderColor = NAVIGATION_COLOR.CGColor;
    self.tbvInfo.layer.borderWidth = 3.0f;
    self.tbvInfo.hidden = NO;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.lstRefusal.data.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LARecusarTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LARecusarTableViewCell class]) forIndexPath:indexPath];
    if (indexPath.row < self.lstRefusal.data.count)
    {
        LARefusalObj* obj = [self.lstRefusal.data objectAtIndex:indexPath.row];
        cell.lbName.text = obj.name;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row < self.lstRefusal.data.count)
    {
        LARefusalObj* obj = [self.lstRefusal.data objectAtIndex:indexPath.row];
        currentRecusa = obj;
        self.lbCateName.text = obj.name;
        self.tbvInfo.hidden = YES;
    }
    
}

- (IBAction)onConfirm:(id)sender
{
    if ([self.txtEnterText.text length] > 40) {
        [self showMessage:@"Reason inválido" withPoistion:nil];
        return;
    }
    currentRecusa.reasonDetail = self.txtEnterText.text;
    if ([currentRecusa.name isEqualToString:@"Outros*"])
    {
        if ([LAUtilitys NullOrEmpty:self.txtEnterText.text]) {
            [self showMessage:@"Reason inválido" withPoistion:nil];
            return;
        }
        else{
            if (self.delegate && [self.delegate respondsToSelector:@selector(onchooseRecusae: andIndexPath:)])
            {
                [self.delegate onchooseRecusae:currentRecusa andIndexPath:self.indexPath];
            }
        }
    }
    else
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(onchooseRecusae: andIndexPath:)])
        {
            [self.delegate onchooseRecusae:currentRecusa andIndexPath:self.indexPath];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)doOpenCategories:(id)sender {
    [self.txtEnterText resignFirstResponder];
    self.tbvInfo.hidden = !self.tbvInfo.hidden;
    [self.tbvInfo bringSubviewToFront:self.txtEnterText];
    self.tbvInfo.layer.zPosition = MAXFLOAT;
    
}
- (IBAction)doBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//- (BOOL)textField:(UITextField *)textField
//shouldChangeCharactersInRange:(NSRange)range
//replacementString:(NSString *)string{
//    
//}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    if(newString.length > textView.text.length){
        unichar c = [newString characterAtIndex:newString.length - 1];
        if ([charSet characterIsMember:c]) {
            return NO;
        }
    }
    return YES;
}
@end

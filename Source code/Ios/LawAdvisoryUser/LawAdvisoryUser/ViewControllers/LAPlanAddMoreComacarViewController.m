//
//  LAPlanAddMoreComacarViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanAddMoreComacarViewController.h"
#import "LADistrictObj.h"
#import "LAPlanosEscolhaFormaPagementoVC.h"
#import "LAAddNewComarcaViewController.h"
#import "LAPlanObj.h"
#import "MainProfileViewController.h"
#import "AppDelegate.h"

@interface LAPlanAddMoreComacarViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *srcMainView;
@property (weak, nonatomic) IBOutlet UIButton *btnAddComarca;
@property (weak, nonatomic) IBOutlet UIView *vBottom;

@end

@implementation LAPlanAddMoreComacarViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = YES;
        self.isShouldLeftButton = YES;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnAddComarca.titleLabel.text = [NSString stringWithFormat: @"Adicionar uma comarca extra por %.0f%% de desconto",self.lanObj.percent_by_district];
    [LAUtilitys setButtonTextUnderLine:self.btnAddComarca];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI
{
    self.navigationItem.title = @"Planos";
    [self setupFrame];
}
- (void)initData
{
    NSString* strDistrict = @"";
    for (int i = 0; i < self.listDistric.count ; i++)
    {
        LADistrictObj* obj = [self.listDistric objectAtIndex:i];
        if (i == 0 && self.listDistric.count > 1)
        {
            strDistrict = [obj.name stringByAppendingString:@"\n"];
        }
        else
            if (i != self.listDistric.count - 1)
            {
                strDistrict = [NSString stringWithFormat:@"%@%@\n",strDistrict,obj.name];
            }
            else
            {
                strDistrict = [NSString stringWithFormat:@"%@%@",strDistrict,obj.name];
            }
    }
    self.lbComacar.text = [LAUtilitys getDefaultString:strDistrict];
    [self.lbComacar sizeToFit];
    
    self.lbTotal.text = [NSString stringWithFormat:@"R$ %@",[LAUtilitys getCurrentcyFromDecimal:[self getTotalValue]]];
}
-(float)getTotalValue{
    float totalValue = self.lanObj.value;
    if ([AppDelegate sharedDelegate].isDiscountAll) {
        totalValue = self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    
    for (int i=1;i<[self.arrSignature count];i++) {
        totalValue += self.lanObj.value*(1-self.lanObj.percent_by_district/100.0f);
    }
    return totalValue;
}
- (void)setupFrame
{
    [self.lbComacar sizeToFit];
    self.vBottom.frame = CGRectMake(self.vBottom.frame.origin.x,CGRectGetMaxY(self.lbComacar.frame) + 34 , self.vBottom.frame.size.width, self.vBottom.frame.size.height);
    self.srcMainView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.vBottom.frame));
}

- (IBAction)onConfirmPressed:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LAPlanosEscolhaFormaPagementoVC* pagementoVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LAPlanosEscolhaFormaPagementoVC class])];
    pagementoVC.lanObj = self.lanObj;
    pagementoVC.listDistric = self.listDistric;
    pagementoVC.arrSignature = self.arrSignature;
    [self.navigationController pushViewController:pagementoVC animated:YES];
}

- (IBAction)onAdditionPressed:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LAAddNewComarcaViewController *newComarcaVC = [storyboard instantiateViewControllerWithIdentifier:@"LAAddNewComarcaViewController"];
    
    newComarcaVC.lanObj = self.lanObj;
    newComarcaVC.listDistric = self.listDistric;
    newComarcaVC.arrSignature = self.arrSignature;
    MainProfileViewController* mainVC = nil;
    NSArray* arrVC = self.navigationController.viewControllers;
    for (UIViewController* vc  in arrVC) {
        if ([vc isKindOfClass:[MainProfileViewController class]])
        {
            mainVC = (MainProfileViewController*)vc;
            break;
        }
    }
    if (mainVC)
    {
        mainVC.addNewComarcaVC = newComarcaVC;
        [mainVC  gotoIndex:0];
        [self.navigationController popToViewController:mainVC animated:YES];
    }
}

@end

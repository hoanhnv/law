//
//  TermAndServiceViewController.h
//  JustapLawyer
//
//  Created by Mac on 10/29/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"

@interface TermAndServiceViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UIWebView *webContent;

@end

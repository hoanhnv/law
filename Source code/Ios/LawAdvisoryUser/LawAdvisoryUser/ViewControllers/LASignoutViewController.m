//
//  LASignoutViewController.m
//  JustapLawyer
//
//  Created by Mac on 10/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASignoutViewController.h"
#import "AppDelegate.h"

@interface LASignoutViewController ()

@end

@implementation LASignoutViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = YES;
        self.isShowGreenNavigation = NO;
        self.isShouldBackButton = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden = NO;
}
- (IBAction)onSigout:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [LAAccountService logoutwCompleteBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            [[LADataCenter shareInstance] clearKeyChain];
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            NSString* messageHere = [data objectForKey:KEY_RESPONSE_MESSAGE];
            if (success)
            {
                [self showMessage:messageHere withPoistion:nil];
                [[LADataCenter shareInstance] clearKeyChain];
                UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LALoginViewController"]];
                [AppDelegate sharedDelegate].nav = nav;
                [AppDelegate sharedDelegate].window.rootViewController = [AppDelegate sharedDelegate].nav;
            }
            else
            {
                [self showMessage:message withPoistion:nil];
            }
        }
        else
        {
            [self showMessage:message withPoistion:nil];
        }
        
    }];
}

@end

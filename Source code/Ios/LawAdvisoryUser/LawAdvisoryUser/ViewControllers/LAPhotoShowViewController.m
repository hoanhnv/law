//
//  LAPhotoShowViewController.m
//  JustapLawyer
//
//  Created by MAC on 10/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPhotoShowViewController.h"

@interface LAPhotoShowViewController ()<UIScrollViewDelegate>

@end

@implementation LAPhotoShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.imgPhotoShow setImage:self.imgShow];
    self.scrPhoto.delegate = self;
    self.scrPhoto.contentSize = self.imgPhotoShow.image.size;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    // return which subview we want to zoom
    return self.imgPhotoShow;
}
- (IBAction)onClosePressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
@end

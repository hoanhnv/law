//
//  LAPlanosEscolhaFormaPagementoVC.h
//  JustapLawyer
//
//  Created by MAC on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPlanObj.h"
#import "LASignatureObj.h"

@interface LAPlanosEscolhaFormaPagementoVC : LABaseViewController{
    
}
@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic) NSMutableArray* arrSignature;

@property (nonatomic) IBOutlet UIScrollView* scrFullData;

@property (nonatomic) IBOutlet UIButton* btConfirm;

@property (weak, nonatomic) IBOutlet UIView *vSubBottom;

@property (nonatomic) IBOutlet UILabel* lbPlanosName;
@property (nonatomic) IBOutlet UILabel* lbTotalPlanSinging;

@property (weak, nonatomic) IBOutlet UILabel *lbPercentName;
@property (nonatomic, weak) IBOutlet UITableView *tblSignature;

@end

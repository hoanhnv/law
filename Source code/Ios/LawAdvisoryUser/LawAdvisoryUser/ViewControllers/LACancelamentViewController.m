//
//  LACancelamentViewController.m
//  JustapLawyer
//
//  Created by MAC on 9/30/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACancelamentViewController.h"
#import "InfoCell.h"
#import "LAQuestionObj.h"
#import "HeaderInfo.h"
#import "LAHeaderCancelMagentoView.h"
#import "HeaderSectionCencelMagento.h"
#import "MainService.h"
#import "MBProgressHUD.h"
#import "UIColor+Law.h"
#import "LAPlanosSelectionComarcaViewController.h"
#import "MainProfileViewController.h"



@interface LACancelamentViewController ()
{
    NSMutableDictionary * mSelected;
    LAHeaderCancelMagentoView* viewHeader;
    CGFloat heigtSection;
}
@end

@implementation LACancelamentViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBigHeightNavigationBar = NO;
        self.isShouldShowBottombar  = NO;
        self.isShouldBackButton = YES;
        self.isShowGreenNavigation = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    int Id = 0;
    Id = self.objCategory.dataIdentifier;
    NSString *thesisId = [NSString stringWithFormat:@"%d",Id];
    NSDictionary *dictParam = @{@"thesis_id":thesisId};
    [MainService postThesisClick:dictParam withBlock:^(NSInteger errorCode, NSString *message, id data){
        if ([data isKindOfClass:[NSDictionary class]]) {
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            message = [data objectForKey:KEY_RESPONSE_MESSAGE];
            if(success) {
                
            }
        }
    }];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:44.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    if (_objCategory.category && ![LAUtilitys isEmptyOrNull:_objCategory.category.color])
    {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:_objCategory.category.color];
    }
    else{
        self.navigationController.navigationBar.barTintColor = [LAUtilitys jusTapColor];
        _objCategory.category.color = MAIN_COLOR;
    }
    arrHeaderTitle = [NSMutableArray arrayWithObjects:@"DOCUMENTOS PARA A AÇÃO",@"COMO AGIR",@"SEUS DIREITOS", nil];
    
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    viewHeader = [[[NSBundle mainBundle] loadNibNamed:@"LAHeaderCancelMagentoView" owner:self options:nil] objectAtIndex:0];
    NSAttributedString* attrStr = [LAUtilitys getAtributeTextFromString:self.objCategory.identification];
    viewHeader.txtTitle.attributedText = attrStr;
    [viewHeader.txtTitle sizeToFit];
    [viewHeader layoutSubviews];
    CGRect frameHere = viewHeader.frame;
    frameHere.size.height = CGRectGetHeight(viewHeader.txtTitle.frame);
    viewHeader.frame = frameHere;
    self.tbData.tableHeaderView = viewHeader;
    [self.tbData reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)initUI
{
    
}
- (void)initData
{
    [self.tbData registerNib:[UINib nibWithNibName:NSStringFromClass([InfoCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoCell class])];
    [self getData];
}
- (void)jaq_setHTMLFromString:(NSString *)string {
    
    
}
- (void)getData
{
    if (self.objCategory)
    {
        mSelected = [NSMutableDictionary new];
        self.title = [LAUtilitys getDefaultString:self.objCategory.name];
        arrData = [NSMutableArray arrayWithObjects:[LAUtilitys getDefaultString:self.objCategory.documentation],[LAUtilitys getDefaultString:self.objCategory.howToAct],[LAUtilitys getDefaultString:self.objCategory.yourRights], nil];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrHeaderTitle.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)section]]integerValue];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (NSString * key in mSelected.allKeys) {
        if (indexPath.section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]] integerValue] == 1;
            if (isShow)
            {
                NSString *questionInfo = arrData[indexPath.section] ;
                NSAttributedString *attr = [LAUtilitys getAtributeTextFromString:questionInfo];
                    return [InfoCell getHeightAttr:attr inWith:self.tbData];
            }
        }
    }
    return 0.01f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *questionInfo = arrData[indexPath.section];
    NSAttributedString* attrStr = [LAUtilitys getAtributeTextFromString:questionInfo];
        InfoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoCell class]) forIndexPath:indexPath];
        cell.lbContent.attributedText = attrStr;
        [cell.btnCancel setTag:indexPath.row];
        return cell;
    
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderSectionCencelMagento * header = [[[NSBundle mainBundle] loadNibNamed:@"HeaderSectionCencelMagento" owner:self options:nil] objectAtIndex:0];
    UIImageView *btn = header.imgButton;
    heigtSection = [HeaderSectionCencelMagento getHeight:arrHeaderTitle[section] inWith:self.tbData];
    //btn.image = [UIImage imageNamed:@"dropdown_green"];
    
    [btn setTintColor:[UIColor colorWithHexString:self.objCategory.category.color]];
    header.imgButton = btn;
    if (_objCategory.category && ![LAUtilitys isEmptyOrNull:_objCategory.category.color])
    {
        header.imgBottom.backgroundColor = [UIColor colorWithHexString:self.objCategory.category.color];
        header.color = [UIColor colorWithHexString:self.objCategory.category.color];
        btn.image = [btn.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else
    {
        header.imgBottom.backgroundColor = [LAUtilitys jusTapColor];
        header.color = [LAUtilitys jusTapColor];
        btn.image = [btn.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        //        btn.image = [UIImage imageNamed:@"dropdown_green"];
    }
    header.strTitle = [arrHeaderTitle objectAtIndex:section];
    [header setupView];
    for (NSString * key in mSelected.allKeys) {
        if (section == [key integerValue]) {
            BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",(long)section]] integerValue] == 1;
            [header setupColor:isShow];
        }
    }
    [header setShowBlock:^(BOOL isShow){
        if (isShow) {
            [mSelected setObject:@(1) forKey:[NSString stringWithFormat:@"%ld",(long)section]];
        } else{
            [mSelected setObject:@(0) forKey:[NSString stringWithFormat:@"%ld",(long)section]];
        }
        //        if (section == arrHeaderTitle.count - 1)
        //        {
        //            [self displayBottomButton:!isShow];
        //        }
        [tableView reloadData];
    }];
    
    return header;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString *questionInfo = arrHeaderTitle[section];
    return  [HeaderSectionCencelMagento getHeight:questionInfo inWith:tableView];
}
- (void)displayBottomButton:(BOOL)isShow
{
    CGRect rectTable = self.tbData.frame;
    if (isShow)
    {
        rectTable.size.height = CGRectGetMinY(footerView.frame);
    }
    else
    {
        rectTable.size.height = CGRectGetMaxY(footerView.frame);
        
    }
    [self.tbData setFrame:rectTable];
    [self.tbData layoutSubviews];
    [footerView setHidden:!isShow];
}
- (IBAction)onBottomButtonPressed:(id)sender
{
    [self doShowPlanos];
}
-(void)doShowPlanos{
    
    [self.tabBarController setSelectedIndex:3];
    UINavigationController *nav3 = (UINavigationController*)[self.tabBarController.viewControllers objectAtIndex:3];
    if ([nav3.viewControllers count]) {
        UIViewController *rootVC = [nav3.viewControllers objectAtIndex:0];
        if ([rootVC isKindOfClass:[MainProfileViewController class]]) {
            MainProfileViewController *profile = (MainProfileViewController*)rootVC;
            profile.selectedInitIndex = 0;
        }
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ChangeTabSelectedIfNeed" object:nil];
}
@end

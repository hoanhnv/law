//
//  LAPlanosConfirmViewController.h
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAPlanObj.h"
#import "LADistrictObj.h"
#import "LASignatureObj.h"

@interface LAPlanosConfirmViewController : LABaseViewController
{
    
}
@property (nonatomic) NSMutableArray* listDistric;
@property (nonatomic) LAPlanObj* lanObj;
@property (nonatomic,strong) NSMutableArray* arrSignature;

@property (nonatomic, assign) BOOL isStep2;

@property (nonatomic) IBOutlet UIScrollView* scrData;
@property (nonatomic) IBOutlet UIView* viewBottom;
@property (nonatomic) IBOutlet UIButton* btAditionComarca;

@property (nonatomic) IBOutlet UILabel* planosName;
@property (nonatomic) IBOutlet UILabel* planosTotalPlanSinging;
@property (nonatomic) IBOutlet UIView* viewInBottom;

@property (nonatomic, weak) IBOutlet UITableView *tblSignature;

- (IBAction)onConfirmPressed:(id)sender;
- (IBAction)onAdditionPressed:(id)sender;
@end

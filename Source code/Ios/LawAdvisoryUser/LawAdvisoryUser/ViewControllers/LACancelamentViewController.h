//
//  LACancelamentViewController.h
//  JustapLawyer
//
//  Created by MAC on 9/30/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LALawSuitCategory.h"
#import "LALawsuitObj.h"
@interface LACancelamentViewController : LABaseViewController
{
    NSMutableArray* arrData;
    NSMutableArray* arrHeaderTitle;
    IBOutlet UIView* footerView;
}
//@property (nonatomic, strong) LALawSuitCategory *category;
@property (nonatomic) LALawsuitObj* objCategory;
@property (nonatomic) IBOutlet UITableView* tbData;
@property (nonatomic) IBOutlet UIView* headerView;
@end

//
//  LAAcoesViewController.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/5/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAcoesViewController.h"
#import "MBProgressHUD.h"
#import "HMSegmentedControl.h"
#import "LANovasTableViewCell.h"
#import "LAEmAndAmentoTableViewCell.h"
#import "LAHistoryTableViewCell.h"
#import "LAPendenteTableViewCellType1.h"
#import "LAPendenteTableViewCellType2.h"
#import "LAPendenteTableViewCellType3.h"
#import "LAPendenteTableViewCellType4.h"
#import "LAPendenteTableViewCellType5.h"
#import "LANovaAoesDetailViewController.h"
#import "MainService.h"
#import "LAListCustomerOpen.h"
#import "LAActionDataObj.h"
#import "LAActionResponseInfo.h"
#import "LACustomerOpenObj.h"
#import "LAListRefusal.h"
#import "LARefusalObj.h"
#import "LAGetHappeningByID.h"
#import "LADetalHesViewController.h"
#import "LARecusaReasonViewController.h"
#import "LANovasPoupViewController.h"


float const kLAHeightHistoryCell = 300.0f;
float const kLAHeightNovasCell = 265.0f;
float const kLAHeightPendenteCell = 215.0f;
float const kLAHeightEmandamentoCell = 218.0f;

@interface LAAcoesViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,AcoesNovaCellDelegate,AcoesEmAndamentoCellDelegate,AcoesHistoricoCellDelegate,AcoesPendenteCellType2Delegate,AcoesPendenteCellType3Delegate,AcoesPendenteCellType4Delegate,AcoesPendenteCellType5Delegate,NovasPopupDelegate,RecusaReasonDelegate>
{
    IBOutlet UITableView* tbNovas;
    IBOutlet UITableView* tbEmAndamento;
    IBOutlet UITableView* tbhistorico;
    IBOutlet UITableView* tbPendente;
    BOOL isLoadedNovas;
    BOOL isLoadedAndamento;
    BOOL isLoadedHistorico;
    BOOL isLoadedPendente;
}
@property (nonatomic) IBOutlet UIView* viewFullData;
@property (nonatomic) IBOutlet UIScrollView* scrViewData;
@property (nonatomic) HMSegmentedControl* segmentedControl;
@end

@implementation LAAcoesViewController
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.isShouldShowBottombar  = YES;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectedInitIndex = 0;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ChangeSelectedTabIndex:) name:@"ChangeSelectedTabIndex" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHeight:64.0f];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Ações";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initUI{
    // Tying up the segmented control to a scroll view
    //    self.pageControl.iNumberPage = arrSlider.count;
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    self.segmentedControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 50)];
    self.segmentedControl.sectionTitles = @[@"NOVAS",@"PENDENTE",@"EM ANDAMENTO",@"HISTÓRICO"];
    self.segmentedControl.selectedSegmentIndex = 1;
    self.segmentedControl.backgroundColor = [LAUtilitys jusTapColor];
    self.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};;
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl.tag = 300;
    self.segmentedControl.type = HMSegmentedControlTypeText;
    self.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    tbNovas.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tbPendente.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tbhistorico.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tbEmAndamento.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.segmentedControl];
    [self.scrViewData setFrame:CGRectMake(0, CGRectGetMaxY(self.segmentedControl.frame) + 5, viewWidth, CGRectGetHeight(self.view.frame) - CGRectGetMaxY(self.segmentedControl.frame) + 5 - 50)];
    self.scrViewData.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    self.scrViewData.pagingEnabled = YES;
    self.scrViewData.showsHorizontalScrollIndicator = NO;
    self.scrViewData.contentSize = CGSizeMake(self.segmentedControl.sectionTitles.count * viewWidth, 200);
    for (int i = 0; i < arrTableview.count; i ++)
    {
        UITableView* tb = [arrTableview objectAtIndex:i];
        [tb setFrame:CGRectMake(i*viewWidth, 0, viewWidth, CGRectGetHeight(self.scrViewData.frame))];
    }
    [self.scrViewData scrollRectToVisible:CGRectMake(0, 0, viewWidth, 200) animated:NO];
    self.segmentedControl.selectedSegmentIndex = self.selectedInitIndex;
    [self  gotoIndex:self.selectedInitIndex];
    
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.scrViewData scrollRectToVisible:CGRectMake(viewWidth * index, 0, viewWidth, 200) animated:YES];
        [weakSelf loadDataByTab:index];
    }];
    
}

- (void)gotoIndex:(NSInteger)index
{
    if (self.segmentedControl.sectionTitles.count > index)
    {
        [self.segmentedControl setSelectedSegmentIndex:index];
    }
    
}
- (void)initData
{
    isGetAllReason = NO;
    arrTableview = [NSMutableArray arrayWithObjects:tbNovas,tbPendente,tbEmAndamento,tbhistorico, nil];
    arrNovas = [NSMutableArray new];
    arrPendente = [NSMutableArray new];
    arrEmAnDamento = [NSMutableArray new];
    arrHistorico = [NSMutableArray new];
    [tbNovas registerNib:[UINib nibWithNibName:NSStringFromClass([LANovasTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LANovasTableViewCell class])];
    [tbEmAndamento registerNib:[UINib nibWithNibName:NSStringFromClass([LAEmAndAmentoTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAEmAndAmentoTableViewCell class])];
    [tbhistorico registerNib:[UINib nibWithNibName:NSStringFromClass([LAHistoryTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAHistoryTableViewCell class])];
    [tbPendente registerNib:[UINib nibWithNibName:NSStringFromClass([LAPendenteTableViewCellType1 class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPendenteTableViewCellType1 class])];
    [tbPendente registerNib:[UINib nibWithNibName:NSStringFromClass([LAPendenteTableViewCellType2 class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPendenteTableViewCellType2 class])];
    [tbPendente registerNib:[UINib nibWithNibName:NSStringFromClass([LAPendenteTableViewCellType3 class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPendenteTableViewCellType3 class])];
    [tbPendente registerNib:[UINib nibWithNibName:NSStringFromClass([LAPendenteTableViewCellType4 class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPendenteTableViewCellType4 class])];
    [tbPendente registerNib:[UINib nibWithNibName:NSStringFromClass([LAPendenteTableViewCellType5 class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAPendenteTableViewCellType5 class])];
    [self loadDataByTab:0];
    
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.scrViewData)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        
        [self.segmentedControl setSelectedSegmentIndex:page animated:YES];
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark PENDENTE
- (void)onAcharAdvogadoDidTouch:(NSIndexPath*)indexPath
{
    
}
- (void)onAcharNovoAdvogadoDidTouch:(NSIndexPath*)indexPath
{
    
}
- (void)onConfirmasMeusDadosDidTouch:(NSIndexPath*)indexPath
{
    
}
- (void)onPendenteVerDetalhesDidTouch:(NSIndexPath*)indexPath
{
    if (arrPendente.count > indexPath.row)
    {
        LACustomerOpenObj *obj = [arrPendente objectAtIndex:indexPath.row];
        LADetalHesViewController* detalHesVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LADetalHesViewController class])];
        detalHesVC.isShouldBackButton = YES;
        detalHesVC.isShouldRightButton = NO;
        detalHesVC.isShouldShowBottombar = NO;
        detalHesVC.objData = obj;
        detalHesVC.isShowButtonOk = YES;
        [self.navigationController pushViewController:detalHesVC animated:YES];
    }
    
}
#pragma  mark LARecusaReasonViewController
-(void)onchooseRecusae:(LARefusalObj *)objChoose andIndexPath:(NSIndexPath *)indexPath{
    if (objChoose)
    {
        if (indexPath.row < arrNovas.count)
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            LACustomerOpenObj *obj = [arrNovas objectAtIndex:indexPath.row];
            [MainService lawyerRefuse:obj.dataIdentifier withrefusal_id:objChoose.dataIdentifier refusalDescription:objChoose.reasonDetail withBlock:^(NSInteger errorCode, NSString *message, id data) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                LAActionResponseInfo *actionInfo = [[LAActionResponseInfo alloc]initWithDictionary:data];
                if (actionInfo.success) {
                    [self changeViewByIndex:3];
                }
                else{
                    [self showMessage:actionInfo.message withPoistion:nil];
                }
            }];
        }
        
    }

}
//- (void)onchooseRecusae:(LARefusalObj*)objChoose andIndexPath:(NSIndexPath*)indexPath
//{
//    
//}
#pragma mark NOVAS
//DELEGATE
- (void)onAceiterNovasDidTouch:(NSIndexPath*)indexPath
{
    LACustomerOpenObj *obj = [arrNovas objectAtIndex:indexPath.row];
    UIStoryboard *storyboardDev = [UIStoryboard storyboardWithName:@"StoryboardDev" bundle:nil];
    LANovasPoupViewController *poupNovas = [storyboardDev instantiateViewControllerWithIdentifier:@"LANovasPoupViewController"];
    poupNovas.indexPath = indexPath;
    poupNovas.delegate = self;
    poupNovas.objData = obj;
    poupNovas.lstRefusal = listRefusal;
    [self presentViewController:poupNovas animated:YES completion:nil];
    
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    [MainService lawSuitAccept:obj.dataIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
    //        [MBProgressHUD hideHUDForView:self.view animated:YES];
    //        LAActionResponseInfo *actionInfo = [[LAActionResponseInfo alloc]initWithDictionary:data];
    //        if (actionInfo.success) {
    //            if (actionInfo.data.dataIdentifier > 0) {
    //                [self changeViewByIndex:1];
    //            }
    //            else{
    //                [self showMessage:actionInfo.message withPoistion:nil];
    //            }
    //        }
    //        else{
    //            [self showMessage:actionInfo.message withPoistion:nil];
    //        }
    //    }];
}

- (void)onRecusarNovasDidTouch:(NSIndexPath*)indexPath
{
    if (indexPath.row < arrNovas.count)
    {
        LARecusaReasonViewController* reasonVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARecusaReasonViewController class])];
        reasonVC.indexPath = indexPath;
        reasonVC.delegate = self;
        reasonVC.lstRefusal = listRefusal;
        [self presentViewController:reasonVC animated:YES completion:nil];
    }
}

- (void)onDetalHesNovasDidTouch:(NSIndexPath*)indexPath
{
    if (arrNovas.count > indexPath.row)
    {
        LACustomerOpenObj *obj = [arrNovas objectAtIndex:indexPath.row];
        LANovaAoesDetailViewController* detalHesVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LANovaAoesDetailViewController class])];
        detalHesVC.isShouldBackButton = YES;
        detalHesVC.isShouldRightButton = NO;
        detalHesVC.isShouldShowBottombar = NO;
        detalHesVC.objData = obj;
        detalHesVC.lstRefusal = listRefusal;
        [self.navigationController pushViewController:detalHesVC animated:YES];
    }
    
}
#pragma mark - Poup Delegate
-(void)onAccept:(LACustomerOpenObj *)obj{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService lawSuitAccept:obj.dataIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        LAActionResponseInfo *actionInfo = [[LAActionResponseInfo alloc]initWithDictionary:data];
        if (actionInfo.success) {
            if (actionInfo.data.dataIdentifier > 0) {
                [self changeViewByIndex:1];
            }
            else{
                [self showMessage:actionInfo.message withPoistion:nil];
            }
        }
        else{
            [self showMessage:actionInfo.message withPoistion:nil];
        }
    }];
}
-(void)onRefuse:(LAListRefusal *)refusal withIndex:(NSIndexPath *)indexPath{
    if (indexPath.row < arrNovas.count)
    {
        LARecusaReasonViewController* reasonVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LARecusaReasonViewController class])];
        reasonVC.indexPath = indexPath;
        reasonVC.delegate = self;
        reasonVC.lstRefusal = refusal;
        [self presentViewController:reasonVC animated:YES completion:nil];
    }
}

#pragma mark EM ANDAMENTO
//DELEGATE
- (void)onVerDetalhesEmandAmentoDidTouch:(NSIndexPath*)indexPath
{
    LACustomerOpenObj *obj = [arrEmAnDamento objectAtIndex:indexPath.row];
    LADetalHesViewController* detalHesVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LADetalHesViewController class])];
    detalHesVC.isShouldBackButton = YES;
    detalHesVC.isShouldRightButton = NO;
    detalHesVC.isShouldShowBottombar = NO;
    detalHesVC.objData = obj;
    detalHesVC.isShowButtonOk = YES;
    [self.navigationController pushViewController:detalHesVC animated:YES];
}
#pragma mark HISTORICO
//DELEGATE
- (void)onVerDetalhesHistoryDidTouch:(NSIndexPath*)indexPath
{
    if (arrHistorico.count > indexPath.row)
    {
        LACustomerOpenObj *obj = [arrHistorico objectAtIndex:indexPath.row];
        LADetalHesViewController* detalHesVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LADetalHesViewController class])];
        detalHesVC.isShouldBackButton = YES;
        detalHesVC.isShouldRightButton = NO;
        detalHesVC.isShouldShowBottombar = NO;
        detalHesVC.objData = obj;
        detalHesVC.isShowButtonOk = NO;
        detalHesVC.fromTabIndex = 3;
        [self.navigationController pushViewController:detalHesVC animated:YES];
    }
}
#pragma mark UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tbNovas)
    {
        return arrNovas.count;
    }
    else if (tableView == tbEmAndamento)
    {
        return arrEmAnDamento.count;
    }
    if (tableView == tbhistorico)
    {
        return arrHistorico.count;
    }else
    {
        return arrPendente.count;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == tbNovas)
    {
        if ([arrNovas count]) {
            return 1;
        }
        else{
            [self addMessageEmpty:tableView withMessage:MSG_024 needShow:isLoadedNovas];
            return 0;
        }
    }
    else if(tableView == tbEmAndamento){
        if ([arrEmAnDamento count]) {
            return 1;
        }
        else{
            [self addMessageEmpty:tableView withMessage:MSG_026 needShow:isLoadedAndamento];
            return 0;
        }
    }else if(tableView == tbPendente){
        if ([arrPendente count]) {
            return 1;
        }
        else{
            [self addMessageEmpty:tableView withMessage:MSG_025 needShow:isLoadedPendente];
            return 0;
        }
    }
    else if(tableView == tbhistorico){
        if ([arrHistorico count]) {
            return 1;
        }
        else{
            [self addMessageEmpty:tableView withMessage:MSG_027 needShow:isLoadedHistorico];
            return 0;
        }
    }
    else{
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tbNovas)
    {
        LANovasTableViewCell* cell = [tbNovas dequeueReusableCellWithIdentifier:NSStringFromClass([LANovasTableViewCell class]) forIndexPath:indexPath];
        if (indexPath.row < arrNovas.count)
        {
            id obj = [arrNovas objectAtIndex:indexPath.row];
            [cell setupData:obj];
        }
        cell.delegate = self;
        cell.currentPath = indexPath;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (tableView == tbEmAndamento)
    {
        LAEmAndAmentoTableViewCell* cell = [tbEmAndamento dequeueReusableCellWithIdentifier:NSStringFromClass([LAEmAndAmentoTableViewCell class]) forIndexPath:indexPath];
        if (indexPath.row < arrEmAnDamento.count)
        {
            id obj = [arrEmAnDamento objectAtIndex:indexPath.row];
            [cell setupData:obj];
        }
        cell.delegate = self;
        cell.currentPath = indexPath;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    if (tableView == tbhistorico)
    {
        LAHistoryTableViewCell* cell = [tbhistorico dequeueReusableCellWithIdentifier:NSStringFromClass([LAHistoryTableViewCell class]) forIndexPath:indexPath];
        if (indexPath.row < arrHistorico.count)
        {
            id obj = [arrHistorico objectAtIndex:indexPath.row];
            [cell setupData:obj];
        }
        cell.delegate = self;
        cell.currentPath = indexPath;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else
    {
        if (indexPath.row < arrPendente.count)
        {
            UITableViewCell* cell = [self cellPedenteWithIndexPath:indexPath andType:TYPE_PEDENTE_CELL_5];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else
        {
            return  nil;
        }
        
    }
    
}
- (CGFloat)getHeightCellAtIndex:(NSIndexPath*)indexPath andType:(TYPE_PEDENTE_CELL)type
{
    switch (type) {
        case TYPE_PEDENTE_CELL_1:
        {
            return 115.0;
        }
            break;
        case TYPE_PEDENTE_CELL_2:
        {
            return 180.0;
        }
            break;
        case TYPE_PEDENTE_CELL_3:
        {
            return 180.0;
        }
            break;
        case TYPE_PEDENTE_CELL_4:
        {
            return 195.0;
        }
            break;
        case TYPE_PEDENTE_CELL_5:
        {
            return 200.0;
        }
            break;
        default:
        {
            return 0.0;
        }
            break;
    }
}
- (UITableViewCell*)cellPedenteWithIndexPath:(NSIndexPath*)indexPath andType:(TYPE_PEDENTE_CELL)type
{
    switch (type) {
        case TYPE_PEDENTE_CELL_1:
        {
            LAPendenteTableViewCellType1* cell = [tbPendente dequeueReusableCellWithIdentifier:NSStringFromClass([LAPendenteTableViewCellType1 class]) forIndexPath:indexPath];
            cell.delegate = self;
            cell.currentPath = indexPath;
            return cell;
        }
            break;
        case TYPE_PEDENTE_CELL_2:
        {
            LAPendenteTableViewCellType2* cell = [tbPendente dequeueReusableCellWithIdentifier:NSStringFromClass([LAPendenteTableViewCellType2 class]) forIndexPath:indexPath];
            cell.delegate = self;
            cell.currentPath = indexPath;
            return cell;
        }
            break;
        case TYPE_PEDENTE_CELL_3:
        {
            LAPendenteTableViewCellType3* cell = [tbPendente dequeueReusableCellWithIdentifier:NSStringFromClass([LAPendenteTableViewCellType3 class]) forIndexPath:indexPath];
            cell.delegate = self;
            cell.currentPath = indexPath;
            return cell;
        }
            break;
        case TYPE_PEDENTE_CELL_4:
        {
            LAPendenteTableViewCellType4* cell = [tbPendente dequeueReusableCellWithIdentifier:NSStringFromClass([LAPendenteTableViewCellType4 class]) forIndexPath:indexPath];
            cell.delegate = self;
            cell.currentPath = indexPath;
            return cell;
        }
            break;
        case TYPE_PEDENTE_CELL_5:
        {
            LAPendenteTableViewCellType5* cell = [tbPendente dequeueReusableCellWithIdentifier:NSStringFromClass([LAPendenteTableViewCellType5 class]) forIndexPath:indexPath];
            if (indexPath.row < arrPendente.count)
            {
                LACustomerOpenObj* obj = [arrPendente objectAtIndex:indexPath.row];
                [cell setupData:obj];
            }
            cell.delegate = self;
            cell.currentPath = indexPath;
            return cell;
        }
            break;
        default:
        {
            return nil;
        }
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tbhistorico)
    {
        return kLAHeightHistoryCell;
    }
    else if (tableView == tbEmAndamento)
    {
        return kLAHeightEmandamentoCell;
    }
    else if (tableView == tbPendente)
    {
        if (indexPath.row < arrPendente.count)
        {
            return [self getHeightCellAtIndex:indexPath andType:TYPE_PEDENTE_CELL_5];
        }
        
    }
    return kLAHeightNovasCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

/**
 * Using to get data from api filter by tab
 *
 *  @return void
 */
-(void)loadDataByTab:(NSInteger)index{
    STATUS_ACTION actionType = ACT_NEW;
    switch (index) {
        case 0:
            actionType = ACT_NEW;
            break;
        case 1:
            actionType = ACT_PENDING_PAYMENT;
            break;
        case 2:
            actionType = ACT_PROGRESS;
            break;
        case 3:
            actionType = ACT_CLOSED;
            break;
        default:
            break;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawyerActionBy:actionType withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        if (!isGetAllReason)
        {
            [self fetchLawsuitReasons];
        }
        
        LAListCustomerOpen *lstData = [[LAListCustomerOpen alloc]initWithDictionary:data];
        if (lstData.success) {
            switch (actionType) {
                case ACT_NEW:{
                    arrNovas = [NSMutableArray arrayWithArray:lstData.data];
                    isLoadedNovas = YES;
                    [tbNovas reloadData];
                }
                    break;
                case ACT_PENDING_PAYMENT:{
                    arrPendente = [NSMutableArray arrayWithArray:lstData.data];
                    isLoadedPendente = YES;
                    [tbPendente reloadData];
                }
                    break;
                case ACT_PROGRESS:{
                    isLoadedAndamento = YES;
                    arrEmAnDamento = [NSMutableArray arrayWithArray:lstData.data];
                    [tbEmAndamento reloadData];
                }
                    
                    break;
                case ACT_CLOSED:{
                    isLoadedHistorico = YES;
                    arrHistorico = [NSMutableArray arrayWithArray:lstData.data];
                    [tbhistorico reloadData];
                }
                    break;
                default:
                    break;
            }
        }
        else{
            [self showMessage:message withPoistion:nil];
            switch (actionType) {
                case ACT_NEW:{
                    arrNovas = [NSMutableArray arrayWithArray:lstData.data];
                    isLoadedNovas = YES;
                    [tbNovas reloadData];
                }
                    break;
                case ACT_PENDING_PAYMENT:{
                    arrPendente = [NSMutableArray arrayWithArray:lstData.data];
                    isLoadedPendente = YES;
                    [tbPendente reloadData];
                }
                    break;
                case ACT_PROGRESS:{
                    isLoadedAndamento = YES;
                    arrEmAnDamento = [NSMutableArray arrayWithArray:lstData.data];
                    [tbEmAndamento reloadData];
                }
                    
                    break;
                case ACT_CLOSED:{
                    isLoadedHistorico = YES;
                    arrHistorico = [NSMutableArray arrayWithArray:lstData.data];
                    [tbhistorico reloadData];
                }
                    break;
                default:
                    break;
            }
        }
    }];
}
/**
 *  Using to get reason and show poup
 */
- (void)fetchLawsuitReasons{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [MainService fetchLawyerReasons:^(NSInteger errorCode, NSString *message, id data) {
        isGetAllReason = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        LAListRefusal *lstData = [[LAListRefusal alloc]initWithDictionary:data];
        if (lstData.success) {
            //Using lstData.data
            listRefusal = lstData;
        }
        else{
            
        }
    }];
}
/**
 *  Using for screen detail
 *
 *  @param happeningId
 */
-(void)getHappeningById:(NSInteger)happeningId{
    [MainService fetchHappeningByID:happeningId withBlock:^(NSInteger errorCode, NSString *message, id data) {
        LAGetHappeningByID *response = [[LAGetHappeningByID alloc]initWithDictionary:data];
        LAHappeningObj *happeningObj = response.data;
    }];
}

-(void)addMessageEmpty:(UITableView*)tableView withMessage:(NSString*)strMessage needShow:(BOOL)isShow{
    if (!isShow) {
        return;
    }
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0,
                                                        tableView.bounds.size.width,
                                                        tableView.bounds.size.height)];
    UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 50,
                                                                    v.bounds.size.width - 20*2,
                                                                    v.bounds.size.height)];
    //set the message
    messageLbl.numberOfLines = 2;
    messageLbl.text = strMessage;
    messageLbl.textColor = [LAUtilitys jusTapColor];
    //center the text
    messageLbl.textAlignment = NSTextAlignmentLeft;
    //auto size the text
    [messageLbl sizeToFit];
    
    [v addSubview:messageLbl];
    //set back to label view
    tableView.backgroundView = v;
    //no separator
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)ChangeSelectedTabIndex:(NSNotification*)notification{
    NSInteger index = [[notification object]integerValue];
    [self changeViewByIndex:index];
}
-(void)changeViewByIndex:(NSInteger)index{
    [self.segmentedControl setSelectedSegmentIndex:index animated:YES];
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    [self.scrViewData scrollRectToVisible:CGRectMake(viewWidth * index, 0, viewWidth, 200) animated:YES];
    [self loadDataByTab:index];
}
@end

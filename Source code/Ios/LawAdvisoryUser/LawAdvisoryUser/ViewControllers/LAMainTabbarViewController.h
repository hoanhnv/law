//
//  LAMainTabbarViewController.h
//  LawAdvisoryUser
//
//  Created by Apple on 8/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAMainTabbarViewController : UITabBarController
- (void)setNumberBadge:(NSString*)numberBadge forTabIndex:(NSInteger)tabIndex;
- (void)resetAllBadgeTab;
@end

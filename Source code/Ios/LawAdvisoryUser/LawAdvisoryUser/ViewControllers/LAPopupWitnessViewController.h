//
//  LAPopupWitnessViewController.h
//  JustapLawyer
//
//  Created by MAC on 11/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "LAWitnesses.h"
#import "LAClientTableViewCell.h"

@interface LAPopupWitnessViewController : LABaseViewController
@property (weak, nonatomic) IBOutlet UILabel *txtNumber;
@property (weak, nonatomic) IBOutlet UILabel *txtName;
@property (weak, nonatomic) IBOutlet UILabel *txtNumberRG;
@property (weak, nonatomic) IBOutlet UILabel *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *txtCEP;
@property (weak, nonatomic) IBOutlet UILabel *txtEstado;
@property (weak, nonatomic) IBOutlet UILabel *txtCity;
@property (weak, nonatomic) IBOutlet UILabel *txtAddress;
@property (weak, nonatomic) IBOutlet UILabel *txtComplemento;
@property (nonatomic) NSDictionary* obj;
@property (nonatomic) NSArray* arrData;
- (IBAction)doClose:(id)sender;
@end

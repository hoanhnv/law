//
//  LADataCenter.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HOANHNV. All rights reserved.
//

#import "LADataCenter.h"
#import <SAMKeychain/SAMKeychain.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>
#import <Twitter/Twitter.h>

@implementation LADataCenter

+(instancetype)shareInstance{
    static  LADataCenter *_shareInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_shareInstance == nil) {
            _shareInstance = [LADataCenter new];
        }
    });
    return _shareInstance;
    
}
-(void)saveToken:(NSString *)token error:(NSError**)err{
    [SAMKeychain setPassword:[NSString stringWithFormat:@"Bearer %@",token] forService:@"JusTap" account:@"JusTapUser" error:err];
}
-(void)saveEmail:(NSString *)email error:(NSError**)err{
    [SAMKeychain setPassword:email forService:@"JusTap" account:@"JusTapEmail" error:err];
}
-(NSString*)currentEmail{
    NSError *err = nil;
    NSString *strEmail = [SAMKeychain passwordForService:@"JusTap" account:@"JusTapEmail" error:&err];
    if (err == nil) {
        return strEmail;
    }
    else{
        return STRING_EMPTY;
    }
    
}
-(void)setAccessToken:(NSString *)accessToken{
    
}

-(NSString *)currentAccessToken{
    NSError *err = nil;
    NSString *strAccessToken = [SAMKeychain passwordForService:@"JusTap" account:@"JusTapUser" error:&err];
    if (err == nil) {
        return strAccessToken;
    }
    else{
        return STRING_EMPTY;
    }
}
-(NSString*)currentStatus{
    NSError *err = nil;
    NSString *strAccessToken = [SAMKeychain passwordForService:@"JusTap" account:@"JusTapStatus" error:&err];
    if (err == nil) {
        return strAccessToken;
    }
    else{
        return STRING_EMPTY;
    }
    
}
-(void)saveAccountStatus:(NSString *)status error:(NSError *__autoreleasing *)err{
    [SAMKeychain setPassword:status forService:@"JusTap" account:@"JusTapStatus" error:err];
}
-(void)updateFlagShowScreenIntroduction:(BOOL)bValue{
    [[NSUserDefaults standardUserDefaults]setBool:bValue forKey:@"hasLaunchedOnce"];
}
+ (BOOL)isFirstTime{
    static BOOL flag=NO;
    static BOOL result;
    
    if(!flag){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"hasLaunchedOnce"]){
            result=NO;
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasLaunchedOnce"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            result=YES;
        }
        
        flag=YES;
    }
    return result;
}

-(void)saveTimerValue:(double)actionId withValue:(NSString *)strTime{
    [[NSUserDefaults standardUserDefaults]setValue:strTime forKey:[NSString stringWithFormat:@"ACT_%.0f",actionId]];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(NSString *)getTimerValue:(double)actionId{
    NSString *strValue = [[NSUserDefaults standardUserDefaults]valueForKey:[NSString stringWithFormat:@"ACT_%.0f",actionId]];
    return strValue;
}

-(void)clearKeyChain{
    
    [SAMKeychain deletePasswordForService:@"JusTap" account:@"JusTapUser"];
    [SAMKeychain deletePasswordForService:@"JusTap" account:@"JusTapStatus"];
    
    [[GIDSignIn sharedInstance] signOut];
    
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    NSString *userID = store.session.userID;
    [store logOutUserID:userID];
    
    [[FBSDKLoginManager new] logOut];
    
}
@end

//
//  LABaseService.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LAHTTPClient.h"
#define MESSAE_CLIENT_RESPONSE_SUCCESS @"success"
#define MESSAE_CLIENT_RESPONSE_FAILRE @"Token inválido"
#define API_PATH_UPLOAD_IMAGE @"uploads/user/rg"
#define API_PATH_UPLOAD_IMAGE_LAWYER @"lawyers/upload_files/new"
#define API_PATH_UPLOAD_IMAGE_LAWYER_FORUPDATE @"lawyers/upload_files"
typedef enum {
    NETWORK_ERROR_CODE         = -1,   // Lỗi mạng
    PARSE_ERROR_CODE           = -2,   // Lỗi parser
    NO_ERROR                    = 1,    // Không có lỗi
    CANNOT_IDENTIFY_ERROR_CODE = 2,    // Không nhận diện được thuê bao
    LIMIT_OTP                  = 402,
    REQUEST_FAIL               = 400,  // Request từ client bị lỗi
    IDENTIFY_USER_ERROR_CODE   = 401,//Chưa xác thực người dùng (login or idenntifi error)
    AUTHENTICATE_ERROR_CODE     = 403, // Chưa xác thực client (validate)
    PAYMENT_REQUIRED_ERROR_CODE = 405,
    INPUT_ERROR = 1001
}SERVICE_RESPONSE_CODE;
typedef void(^CompletionBlock)(NSInteger errorCode, NSString *message, id data);
typedef void(^FailureBlock)(NSInteger errorCode, NSString *message, id data);

typedef void(^FailreBlock)(id data);
typedef void(^ProgressBlockUpload)(double currentProgress);
typedef void(^CompletionBlockPagination)(NSInteger errorCode, NSString *message, id data, NSInteger total);

@interface LABaseService : NSObject
+ (void) requestAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block;
+ (void) postAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block;
+ (void) putAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block;
+ (void) deleteAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block;
+ (void) uploadImage:(UIImage*)imageUpload andParams:(NSDictionary*)params andPath:(NSString*)path withCompletionBlock:(CompletionBlock) block progressBlock:(ProgressBlockUpload)blockProgress;
+ (void) uploadImages:(NSMutableArray*)arrImgages andListKeys:(NSMutableArray*)keys andListName:(NSMutableArray*)names andParams:(NSDictionary*)params andPath:(NSString*)path withCompletionBlock:(CompletionBlock) block progressBlock:(ProgressBlockUpload)blockProgress;
+(void)postData:(NSString*)path params:(NSDictionary*)params withCompletionBlock:(CompletionBlock) block;
@end

//
//  LAAccountService.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAccountService.h"

@implementation LAAccountService
// Login with username (email) and password.
+ (void)loginWithEmail:(NSString*)email andPassword:(NSString*)password withDeviceData:(NSDictionary*)deviceData wCompleteBlock:(CompletionBlock)block
{
    //    if ([LAUtilitys isEmptyOrNull:email] || [LAUtilitys isEmptyOrNull:password])
    //    {
    //        block(INPUT_ERROR,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),nil);
    //    }
    //    else
    //    {
    [[LAHTTPClient sharedClient].requestSerializer setValue:@"" forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    NSMutableDictionary *dictParam = [NSMutableDictionary dictionary];
    if (![LAUtilitys NullOrEmpty:email]) {
        [dictParam setObject:email forKey:REQUEST_ACCOUNT_KEY_EMAIL];
    }
    if (![LAUtilitys NullOrEmpty:password]) {
        [dictParam setObject:password forKey:REQUEST_ACCOUNT_KEY_PASSWORD];

    }
    // ADD Device data for login
    
    if ([deviceData count]) {
        [dictParam setObject:deviceData forKey:@"device_data"];
        
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:@"Content-Type" forHTTPHeaderField:@"application/json"];
    [LABaseService postAndParseBasic:API_PATH_LOGIN params:dictParam withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    //}
    
}
+ (void)registerWithAccount:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block
{
    //    if ([LAUtilitys isEmptyOrNull:obj.accountEmail] || [LAUtilitys isEmptyOrNull:obj.accountPassword])
    //    {
    //        block(INPUT_ERROR,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),nil);
    //    }
    //    else
    //    {
    [[LAHTTPClient sharedClient].requestSerializer setValue:@"" forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    NSDictionary* dict = @{REQUEST_ACCOUNT_KEY_EMAIL:obj.accountEmail,REQUEST_ACCOUNT_KEY_PASSWORD:obj.accountPassword,REQUEST_ACCOUNT_KEY_NAME:obj.accountName,REQUEST_ACCOUNT_KEY_CELLPHONE:obj.accountCellphone,REQUEST_ACCOUNT_KEY_PHONE:obj.accountPhone,REQUEST_ACCOUNT_KEY_ADDRESS:obj.accountAddress,REQUEST_ACCOUNT_KEY_NUMBER:obj.accountNumber,REQUEST_ACCOUNT_KEY_COMPLEMENT:obj.accountComplement,REQUEST_ACCOUNT_KEY_NEIGHBORHOOD:obj.accountNeighborhood,REQUEST_ACCOUNT_KEY_CITY:obj.accountCity,REQUEST_ACCOUNT_KEY_UF:obj.accountUF,REQUEST_ACCOUNT_KEY_DATEOFBIRTH:[NSNumber numberWithDouble:obj.accountDateOfBirth],REQUEST_ACCOUNT_KEY_ACCEPTSNEW:[NSNumber numberWithInteger:obj.accountAcceptNew]};
    [LABaseService postAndParseBasic:API_PATH_REGISTER params:dict withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    //    }
    
}
+ (void)registerLawyerWithAccount:(LAAccountObj *)obj wCompleteBlock:(CompletionBlock)block
{
    //    if ([LAUtilitys isEmptyOrNull:obj.accountEmail] || [LAUtilitys isEmptyOrNull:obj.accountPassword])
    //    {
    //        block(INPUT_ERROR,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),nil);
    //    }
    //    else
    //    {
    [[LAHTTPClient sharedClient].requestSerializer setValue:@"" forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    NSMutableDictionary *dictParam = [NSMutableDictionary dictionary];
    if (![LAUtilitys isEmptyOrNull:obj.accountEmail])
    {
        [dictParam setObject:obj.accountEmail forKey:REQUEST_ACCOUNT_LAWYER_KEY_EMAIL];
    }
    if (![LAUtilitys isEmptyOrNull:obj.accountPassword])
    {
        [dictParam setObject:obj.accountPassword forKey:REQUEST_ACCOUNT_LAWYER_KEY_PASSWORD];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountName]) {
        [dictParam setObject:obj.accountName forKey:REQUEST_ACCOUNT_LAWYER_KEY_NAME];
    }
    [dictParam setObject:[NSNumber numberWithDouble:obj.accountDateOfBirth] forKey:REQUEST_ACCOUNT_LAWYER_KEY_DATEOFBIRTH];
    if (![LAUtilitys NullOrEmpty:obj.accountCPF]) {
        [dictParam setObject:obj.accountCPF forKey:REQUEST_ACCOUNT_LAWYER_KEY_CPF];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountAddress]) {
        [dictParam setObject:obj.accountAddress forKey:REQUEST_ACCOUNT_LAWYER_KEY_ADDRESS];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountNumber]) {
        [dictParam setObject:obj.accountNumber forKey:REQUEST_ACCOUNT_LAWYER_KEY_NUMBER];
    }
    
    if (![LAUtilitys NullOrEmpty:obj.accountNeighborhood]) {
        [dictParam setObject:obj.accountNeighborhood forKey:REQUEST_ACCOUNT_LAWYER_KEY_NEIGHBORHOOD];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountUF]) {
        [dictParam setObject:obj.accountUF forKey:REQUEST_ACCOUNT_LAWYER_KEY_UF];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountCellphone]) {
        [dictParam setObject:obj.accountCellphone forKey:REQUEST_ACCOUNT_LAWYER_KEY_CELLPHONE];
    }
    [dictParam setObject: [NSNumber numberWithBool:obj.accountShowMobilePhone] forKey:REQUEST_ACCOUNT_LAWYER_KEY_SHOW_CELLPHONE];
    if (![LAUtilitys NullOrEmpty:obj.accountPhone]) {
        [dictParam setObject: obj.accountPhone forKey:REQUEST_ACCOUNT_LAWYER_KEY_PHONE];
        
    }
    
    [dictParam setObject: [NSNumber numberWithBool:obj.accountShowPhone] forKey:REQUEST_ACCOUNT_LAWYER_KEY_SHOW_PHONE];
    if (![LAUtilitys NullOrEmpty:obj.accountCity]) {
        [dictParam setObject: obj.accountCity forKey:REQUEST_ACCOUNT_LAWYER_KEY_CITY];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountSite]) {
        [dictParam setObject: obj.accountSite forKey:REQUEST_ACCOUNT_LAWYER_KEY_SITE];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountLinkedin]) {
        [dictParam setObject: obj.accountLinkedin forKey:REQUEST_ACCOUNT_LAWYER_KEY_LINKEDIN];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountNumberOAB]) {
        [dictParam setObject: obj.accountNumberOAB forKey:REQUEST_ACCOUNT_LAWYER_KEY_NUMBER_OAB];
        
    }
    if ([obj.accountTitles count]) {
        [dictParam setObject: obj.accountTitles forKey:REQUEST_ACCOUNT_LAWYER_KEY_TITLES];
        
    }
    [dictParam setObject:[NSNumber numberWithBool:TRUE] forKey:REQUEST_ACCOUNT_KEY_LAWYER_ACCEPTSNEW];
    
    if (![LAUtilitys NullOrEmpty:obj.accountComplement]) {
        [dictParam setObject: obj.accountComplement forKey:REQUEST_ACCOUNT_KEY_LAWYER_COMPLEMENT];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountPostalCode]) {
        [dictParam setObject: obj.accountPostalCode forKey:REQUEST_ACCOUNT_LAWYER_KEY_POSTALCODE];
        
    }
    
    [dictParam setObject:[NSNumber numberWithBool:obj.accountShowEmail] forKey:REQUEST_ACCOUNT_LAWYER_KEY_SHOW_EMAIL];
    
    if (![LAUtilitys NullOrEmpty:obj.state]) {
        [dictParam setObject: obj.state forKey:REQQUEST_ACCOUNT_KEY_UF_OAB];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.about]) {
        [dictParam setObject: obj.about forKey:REQQUEST_ACCOUNT_KEY_ABOUT];
        
    }
    
    [dictParam setObject: [NSNumber numberWithDouble:obj.bank_id] forKey:@"bank_id"];
    if (![LAUtilitys NullOrEmpty:obj.bank_agency]) {
        [dictParam setObject: obj.bank_agency forKey:@"bank_agency"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.bank_account]) {
        [dictParam setObject: obj.bank_account forKey:@"bank_account"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.bank_account_dac]) {
        [dictParam setObject: obj.bank_account_dac forKey:@"bank_account_dac"];
        
    }
    
    if (![LAUtilitys NullOrEmpty:obj.image_oab]) {
        [dictParam setObject: obj.image_oab forKey:@"image_oab"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.image_avatar]) {
        [dictParam setObject: obj.image_avatar forKey:@"image_avatar"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.image_house_proof]) {
        [dictParam setObject: obj.image_house_proof forKey:@"image_house_proof"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.image_oab_verse]) {
        [dictParam setObject: obj.image_oab_verse forKey:@"image_oab_verse"];
        
    }
    
    [LABaseService postAndParseBasic:API_PATH_REGISTER_LAWYER_V2 params:dictParam withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    //}
}
+ (void)getAccountInfowCompleteBlock:(CompletionBlock)block
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
        block(-1000,@"Token not found",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_ACCOUNT_LAWYER_INFO params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+ (void)getAccountInfo:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
        block(-1000,@"Token not found",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_ACCOUNT_LAWYER_INFO params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+ (void)updateAccountWithAccount:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
        block(-1000,@"Token not found",nil);
        return;
    }
    
    NSMutableDictionary *dictParam = [NSMutableDictionary dictionary];
    if (![LAUtilitys NullOrEmpty:obj.accountName]) {
        [dictParam setObject:obj.accountName forKey:REQUEST_ACCOUNT_LAWYER_KEY_NAME];
    }
    [dictParam setObject:[NSNumber numberWithDouble:obj.accountDateOfBirth] forKey:REQUEST_ACCOUNT_LAWYER_KEY_DATEOFBIRTH];
    if (![LAUtilitys NullOrEmpty:obj.accountCPF]) {
        [dictParam setObject:obj.accountCPF forKey:REQUEST_ACCOUNT_LAWYER_KEY_CPF];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountAddress]) {
        [dictParam setObject:obj.accountAddress forKey:REQUEST_ACCOUNT_LAWYER_KEY_ADDRESS];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountNumber]) {
        [dictParam setObject:obj.accountNumber forKey:REQUEST_ACCOUNT_LAWYER_KEY_NUMBER];
    }
    
    if (![LAUtilitys NullOrEmpty:obj.accountNeighborhood]) {
        [dictParam setObject:obj.accountNeighborhood forKey:REQUEST_ACCOUNT_LAWYER_KEY_NEIGHBORHOOD];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountUF]) {
        [dictParam setObject:obj.accountUF forKey:REQUEST_ACCOUNT_LAWYER_KEY_UF];
    }
    if (![LAUtilitys NullOrEmpty:obj.accountCellphone]) {
        [dictParam setObject:obj.accountCellphone forKey:REQUEST_ACCOUNT_LAWYER_KEY_CELLPHONE];
    }
    [dictParam setObject: [NSNumber numberWithBool:obj.accountShowMobilePhone] forKey:REQUEST_ACCOUNT_LAWYER_KEY_SHOW_CELLPHONE];
    if (![LAUtilitys NullOrEmpty:obj.accountPhone]) {
        [dictParam setObject: obj.accountPhone forKey:REQUEST_ACCOUNT_LAWYER_KEY_PHONE];
        
    }
    
    [dictParam setObject: [NSNumber numberWithBool:obj.accountShowPhone] forKey:REQUEST_ACCOUNT_LAWYER_KEY_SHOW_PHONE];
    if (![LAUtilitys NullOrEmpty:obj.accountCity]) {
        [dictParam setObject: obj.accountCity forKey:REQUEST_ACCOUNT_LAWYER_KEY_CITY];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountSite]) {
        [dictParam setObject: obj.accountSite forKey:REQUEST_ACCOUNT_LAWYER_KEY_SITE];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountLinkedin]) {
        [dictParam setObject: obj.accountLinkedin forKey:REQUEST_ACCOUNT_LAWYER_KEY_LINKEDIN];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountNumberOAB]) {
        [dictParam setObject: obj.accountNumberOAB forKey:REQUEST_ACCOUNT_LAWYER_KEY_NUMBER_OAB];
        
    }
    if ([obj.accountTitles count]) {
        [dictParam setObject: obj.accountTitles forKey:REQUEST_ACCOUNT_LAWYER_KEY_TITLES];
        
    }
    [dictParam setObject:[NSNumber numberWithBool:TRUE] forKey:REQUEST_ACCOUNT_KEY_LAWYER_ACCEPTSNEW];
    
    if (![LAUtilitys NullOrEmpty:obj.accountComplement]) {
        [dictParam setObject: obj.accountComplement forKey:REQUEST_ACCOUNT_KEY_LAWYER_COMPLEMENT];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.accountPostalCode]) {
        [dictParam setObject: obj.accountPostalCode forKey:REQUEST_ACCOUNT_LAWYER_KEY_POSTALCODE];
        
    }
    [dictParam setObject:[NSNumber numberWithBool:obj.accountShowEmail] forKey:REQUEST_ACCOUNT_LAWYER_KEY_SHOW_EMAIL];
    
    if (![LAUtilitys NullOrEmpty:obj.state]) {
        [dictParam setObject: obj.state forKey:REQQUEST_ACCOUNT_KEY_UF_OAB];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.about]) {
        [dictParam setObject: obj.about forKey:REQQUEST_ACCOUNT_KEY_ABOUT];
        
    }
    
    [dictParam setObject: [NSNumber numberWithDouble:obj.bank_id] forKey:@"bank_id"];
    if (![LAUtilitys NullOrEmpty:obj.bank_agency]) {
        [dictParam setObject: obj.bank_agency forKey:@"bank_agency"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.bank_account]) {
        [dictParam setObject: obj.bank_account forKey:@"bank_account"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.bank_account_dac]) {
        [dictParam setObject: obj.bank_account_dac forKey:@"bank_account_dac"];
        
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    if (![LAUtilitys isEmptyOrNull:obj.accountEmail])
    {
        [dictParam setObject:obj.accountEmail forKey:REQUEST_ACCOUNT_LAWYER_KEY_EMAIL];
    }
    
    if (![LAUtilitys NullOrEmpty:obj.image_oab]) {
        [dictParam setObject: obj.image_oab forKey:@"image_oab"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.image_avatar]) {
        [dictParam setObject: obj.image_avatar forKey:@"image_avatar"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.image_house_proof]) {
        [dictParam setObject: obj.image_house_proof forKey:@"image_house_proof"];
        
    }
    if (![LAUtilitys NullOrEmpty:obj.image_oab_verse]) {
        [dictParam setObject: obj.image_oab_verse forKey:@"image_oab_verse"];
        
    }

    
    [LABaseService postAndParseBasic:API_PATH_UPDATE_LAWYER_ACCOUNT params:dictParam withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+ (void)logoutwCompleteBlock:(CompletionBlock)block
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
        block(-1000,@"Token not found",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    NSMutableDictionary *param1 = [NSMutableDictionary new];
    NSMutableDictionary *param = [NSMutableDictionary new];
    if (![LAUtilitys isEmptyOrNull:[[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"]]) {
        [param1 setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"DEVICE_TOKEN"] forKey:@"device_token"];
    }
    if (param1) {
        [param setObject:param1 forKey:@"device_data"];
    }
    [LABaseService postData:API_PATH_LOGOUT params:param withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];

//    [LABaseService postAndParseBasic:API_PATH_LOGOUT params:param withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
//        block(errorCode,message,data);
//    }];
}
+ (void)resetPassWithEmail:(NSString*)email wCompleteBlock:(CompletionBlock)block;
{
    //    if ([LAUtilitys isEmptyOrNull:email])
    //    {
    //        block(INPUT_ERROR,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),nil);
    //    }
    //    else
    //    {
    NSDictionary* dict = @{REQUEST_ACCOUNT_KEY_EMAIL:email};
    [LABaseService postAndParseBasic:API_PATH_RESET_PASS params:dict withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    //}
}
@end

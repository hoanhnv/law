//
//  CPHTTPClient.h
//  CompassUtility
//
//  Created by Long Hoang on 7/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#define KEY_RESPONSE_DATA @"data"
//API
#define KEY_RESPONSE_ERROR_CODE @"success"
#define KEY_RESPONSE_MESSAGE @"message"
#define KEY_RESPONSE_DATA @"data"
#define KEY_RESPONSE_TOKEN @"token"
#define KEY_RESPONSE_PROFILE    @"profiles"
#define KEY_HEADER_AUTHORIZED @"Authorization"
#define BASE_API_URL @"https://rest.justap.com.br/api/v1"
//LIST API
//ACCOUNT
#define API_PATH_LOGIN @"authenticate"
#define API_PATH_LOGOUT @"logout"
#define API_PATH_RESET_PASS @"password/reset"

// Register Screen
#define API_PATH_REGISTER @"register"
#define API_PATH_GET_LISTCITY @"cities/SP"
#define API_PATH_GET_LISTCITY_WITH_UF @"cities/"
#define API_PATH_REGISTER_LAWYER @"lawyers/register"
#define API_PATH_REGISTER_LAWYER_V2 @"lawyers/register/v2"
#define API_PATH_UPDATE_LAWYER_ACCOUNT @"lawyers/update"
#define API_PATH_UPDATE_LAWYER_ACCOUNT_V2 @"lawyers/update"
#define API_PATH_ACCOUNT_LAWYER_INFO @"lawyers/me"

@interface LAHTTPClient : AFHTTPSessionManager
+ (instancetype)sharedClientWithUrl:(NSString*)strUrl;
+ (instancetype)sharedClient;
- (void)loadCachedSessionUid;
@property (nonatomic) NSString* tokenRequest;
@end

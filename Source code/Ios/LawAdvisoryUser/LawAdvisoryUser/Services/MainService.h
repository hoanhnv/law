//
//  MainService.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseService.h"
/*
 NOVAS=new
 PENDENTE = pending-payment
 EM ANDAMENTO = progress
 HISTÓRICO = closed //all completed and closed and recused*/
typedef enum STATUS_ACTION
{
    ACT_NEW = 0,
    ACT_PENDING_PAYMENT,
    ACT_PROGRESS ,
    ACT_CLOSED,
    
}STATUS_ACTION;

#define API_PATH_FAQS @"faqs"
#define API_PATH_CATEGORY @"lawsuit_categories"
#define API_PATH_GET_CONTACT_SUBJECTS @"contact_subjects"
#define API_PATH_GET_FAQ_CATEGORIES @"faq_categories"
#define API_PATH_GET_FAQ_BY_CATEGORIES @"faqs/by_category/%ld"
#define API_PATH_UFS @"ufs"
#define API_PATH_SEND_CONTACT @"contacts"
#define API_PATH_CHECK_STATUS @"lawyers/status"
#define API_PATH_BANNER     @"banners"


#define API_PATH_LAWSUIT_THESES   @"lawsuit_theses"

#define API_PATH_LAWSUIT_THESES_BY_CATEGORIES   @"lawsuit_theses/category_id/%ld"
#define API_PATH_LAWSUIT_CATEGORIES   @"lawsuit_categories"
#define API_PATH_LAWSUIT_ALL @"lawsuit_theses"
#define API_PATH_LAWYER_INFO   @"lawyers/me"

#define API_PATH_CHECK_CEP   @"https://viacep.com.br/ws/%@/json/"

#define API_PATH_DISTRICT_BY_UF @"districts/uf/%@"
#define API_PATH_CHANGE_PASSWORD @"password/change"

#define API_PATH_CHANGE_PASSWORD @"password/change"
#define API_PATH_LAWYER_REFUSE   @"lawsuit_lawyers/refuse"

#define API_PATH_LAWYER_ACCEPT   @"lawsuit_lawyers/accept"

#define API_PATH_LAWYER_CANCEL   @"lawyers/signature/cancel"

#define API_PATH_LAWYER_PROGRESS   @"lawsuit_lawyers/progress"

#define API_PATH_GET_ACTION_BY_ID   @"lawsuit_lawyers/%ld"
#define API_PATH_GET_PERCENT   @"justap_percentage"

#define API_PATH_LAWYER_SIGNATURES  @"signatures"

#define API_PATH_LAWYER_UPDATE_THESES  @"signatures/update-theses"
#define API_PATH_LAWYER_TOTAL_THESES   @"lawsuit_theses/total"

#define API_PATH_LIST_PLAN @"plans"
#define API_PATH_TERM_AND_SERVICE @"terms_of_use"
#define API_PATH_LAWYER_LIST_SIGNATURES @"lawyers/signature" 
#define API_PATH_LAWYER_GET_BILL_SIGNATURES @"signature_bills"
#define API_PATH_LAWYER_GET_SIGNATURES_THESES @"signatures/theses"

#define API_PATH_LAWSUIT_LAWYER    @"lawsuit_lawyers/lawyer"
#define API_PATH_LAWSUIT_LAWYER_NEW  @"lawsuit_lawyers/lawyer/new"
#define API_PATH_LAWSUIT_LAWYER_CLOSED  @"lawsuit_lawyers/lawyer/closed"
#define API_PATH_LAWSUIT_LAWYER_PROGRESS  @"lawsuit_lawyers/lawyer/progress"
#define API_PATH_LAWSUIT_LAWYER_PAYMENT_PENDING @"lawsuit_lawyers/lawyer/pendent"

#define API_PATH_LAWSUIT_REFUSAL_REASON @"lawsuit_refusal_reasons"
#define API_PATH_GET_HAPPENING_BYID @"happenings/%ld"
#define API_PATH_LAWSUIT_FINALIZE  @"lawsuit_lawyers/finalize"

#define API_PATH_CONTRACTED_THESE  @"signatures/contracted_theses"
#define API_PATH_EDITCARD  @"signatures/update-credit-card"
#define API_PATH_AUTHENTICATE_VALIDATE  @"authenticate/validate"
#define API_PATH_THESIS_CLICK @"lawsuit_theses/click"
#define API_PATH_GET_MESSAGES @"messages/%ld"
#define API_PATH_POST_MESSAGE @"messages"
#define API_PATH_POST_LOCATION @"notifications/nearby"

#define API_PATH_BANKS @"banks"

#define AUTHORIZATION_FAILURE_CODE      401

@interface MainService : LABaseService

+(void)fetchFaqCategories:(CompletionBlock)block;

+(void)fetchFaqCategories:(NSInteger)cateId
              withSuccess: (CompletionBlock)block;

+(void)fetchFaqs:(CompletionBlock)block;

+(void)fetchCategories:(CompletionBlockPagination)block;

+(void)fetchContactSubjects:(CompletionBlock)block;

+(void)fetchUFs:(CompletionBlock)block;

+(void)sendContact:(double)subjectId
       withMessage:(NSString*)message
         withEmail:(NSString*)email
         withBlock:(CompletionBlock)block;

+(void)checkStatus:(NSString*)token
         withBlock:(CompletionBlock)block;

+(void)fetchBanners:(CompletionBlock)block;

+(void)fetchLawsuitTheseByCategory:(NSInteger)cateId
                         withBlock:(CompletionBlock)block;

+(void)fetchLawsuitCategory:(CompletionBlock)block;

+(void)fetchLawyerInfo:(CompletionBlock)block;
+(void)fetchLawsuitTheses:(CompletionBlock)block;
+(void)fetchAllsuit:(CompletionBlock)block;

+(void)checkCEP:(NSString*)strCEP
      withBlock:(CompletionBlock)block;
+(void)fetchDistrictByUF:(NSString*)strUF
               withBlock:(CompletionBlock)block;

+(void)uploadImage:(UIImage*)img1
          withImg2:(UIImage*)img2
          withImg3:(UIImage*)img3
         withBlock:(CompletionBlock)block;

+(void)changePassWord:(NSString*)currentPass
             passWord:(NSString*)passWord
      confirmPassWord:(NSString*)strPassConfirm:(CompletionBlock)block;

+(void)fetchListPlan:(CompletionBlock)block;

+(void)lawyerRefuse:(double)lawsuit_id
     withrefusal_id:(double)refusal_id
 refusalDescription:(NSString*)refusalDescription
          withBlock:(CompletionBlock)block;

+(void)lawSuitProgress:(double)lawsuit_id
     withProcessNumber:(NSString*)process_number
             withBlock:(CompletionBlock)block;

+(void)lawSuitAccept:(double)lawsuit_id
           withBlock:(CompletionBlock)block;

+(void)lawSignatures:(double)plan_id
       withDistricts:(NSArray*)districts
           withBlock:(CompletionBlock)block;

+(void)lawSignatures4:(NSDictionary*)params withSignaruteId:(NSInteger)signatureId withBlock:(CompletionBlock)block;

+(void)fetchTermAndService:(CompletionBlock)block;

+(void)fetchLawyerSignatures:(CompletionBlock)block;

+(void)lawyerCancelSignature:(NSDictionary*)params withSuccess:(CompletionBlock)block;

+(void)lawyerUpdateThese:(NSDictionary*)params withSuccess:(CompletionBlock)block;

+(void)lawyerTotalThese:(CompletionBlock)block;

+(void)lawyerGetBillSignature:(CompletionBlock)block;
+(void)lawyerGetSignatureTheses:(CompletionBlock)block;

+(void)fetchListSuitOpen:(CompletionBlock)block;
+(void)fetchListSuitClose:(CompletionBlock)block;

+(void)fetchLawyerActionBy:(STATUS_ACTION)actionType withBlock:(CompletionBlock)block;
+(void)fetchLawyerReasons:(CompletionBlock)block;
+(void)fetchHappeningByID:(NSInteger)Id withBlock:(CompletionBlock)block;

+(void)lawsuitFinalize:(NSInteger)lawsuit_id withSuccess:(CompletionBlock)block;
+(void)fetchContractedThese:(CompletionBlock)block;

+(void)fetchBanks:(CompletionBlock)block;
+(void)updateCard:(NSDictionary*)params withBlock:(CompletionBlock)block;
+(void)reValidateToken:(NSDictionary*)dictDeviceData withBlock:(CompletionBlock)block;
+(void)postThesisClick:(NSDictionary*)param withBlock:(CompletionBlock)block;
+(void)getMessage:(NSInteger)Id withBlock:(CompletionBlock)block;
+(void)postMessage:(NSDictionary*)param withBlock:(CompletionBlock)block;
+(void)getActionById:(NSInteger)Id withBlock:(CompletionBlock)block;
+(void)postLocation:(NSDictionary*)param withBlock:(CompletionBlock)block;
+(void)getPercentAction:(CompletionBlock)block;
@end

//
//  LAAddressService.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAddressService.h"
@implementation LAAddressService
+ (void)getlistCitywCompleteBlock:(CompletionBlockPagination)block
{
    NSDictionary* dict = nil;
    [LABaseService requestAndParseBasic:API_PATH_GET_LISTCITY params:dict withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            NSDictionary* trueData = [data objectForKey:KEY_RESPONSE_DATA];
            NSMutableArray* arr = [NSMutableArray new];
            if (trueData && [trueData isKindOfClass:[NSDictionary class]])
            {
                NSString* sigla = [data objectForKey:REQUEST_ACCOUNT_KEY_SIGLA];
                NSString* nome = [data objectForKey:REQUEST_ACCOUNT_KEY_NOME];
                NSArray* listCity = [trueData objectForKey:REQUEST_ACCOUNT_KEY_CITY_LIST];
                if (listCity && [listCity isKindOfClass:[NSArray class]])
                {
                    for (NSString* txt in listCity)
                    {
                        LACityObj* obj = [LACityObj new];
                        obj.cityName = txt;
                        obj.sigla = sigla;
                        obj.nome = nome;
                        [arr addObject:obj];
                    }
                }
            }
            block(0,@"",arr,arr.count);
        }
        else
        {
             block(0,@"",nil,0);
        }
       
    }];
}
+ (void)getlistCityWithUF:(NSString*)ufCode wCompleteBlock:(CompletionBlockPagination)block
{
    NSDictionary* dict = nil;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:ufCode]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil,0);
        return;
    }
    NSString* strApi = [NSString stringWithFormat:@"%@%@",API_PATH_GET_LISTCITY_WITH_UF,ufCode];
    [LABaseService requestAndParseBasic:strApi params:dict withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            NSDictionary* trueData = [data objectForKey:KEY_RESPONSE_DATA];
            NSMutableArray* arr = [NSMutableArray new];
            if (trueData && [trueData isKindOfClass:[NSDictionary class]])
            {
                NSString* sigla = [data objectForKey:REQUEST_ACCOUNT_KEY_SIGLA];
                NSString* nome = [data objectForKey:REQUEST_ACCOUNT_KEY_NOME];
                NSArray* listCity = [trueData objectForKey:REQUEST_ACCOUNT_KEY_CITY_LIST];
                if (listCity && [listCity isKindOfClass:[NSArray class]])
                {
                    for (NSString* txt in listCity)
                    {
                        LACityObj* obj = [LACityObj new];
                        obj.cityName = txt;
                        obj.sigla = sigla;
                        obj.nome = nome;
                        [arr addObject:obj];
                    }
                }
            }
            block(0,@"",arr,arr.count);
        }
        else
        {
            block(0,@"",nil,0);
        }
        
    }];
}
@end

//
//  LAAccountService.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseService.h"
#import "LAAccountObj.h"
//LOGIN KEY
#define REQUEST_ACCOUNT_KEY_EMAIL @"email"
#define REQUEST_ACCOUNT_KEY_PASSWORD @"password"
//REGISTER KEY
#define REQUEST_ACCOUNT_KEY_NAME @"name"
#define REQUEST_ACCOUNT_KEY_CELLPHONE @"cellphone"
#define REQUEST_ACCOUNT_KEY_PHONE @"phone"
#define REQUEST_ACCOUNT_KEY_ADDRESS @"address"
#define REQUEST_ACCOUNT_KEY_NUMBER @"number"
#define REQUEST_ACCOUNT_KEY_COMPLEMENT @"complement"
#define REQUEST_ACCOUNT_KEY_NEIGHBORHOOD @"neighborhood"
#define REQUEST_ACCOUNT_KEY_CITY @"city"
#define REQUEST_ACCOUNT_KEY_UF @"uf"
#define REQUEST_ACCOUNT_KEY_DATEOFBIRTH @"date_of_birth"
#define REQUEST_ACCOUNT_KEY_ACCEPTSNEW @"accepts_news"
//REGISTER LAWYER KEY
#define REQUEST_ACCOUNT_KEY_LAWYER_COMPLEMENT @"complement"
#define REQUEST_ACCOUNT_LAWYER_KEY_EMAIL @"email"
#define REQUEST_ACCOUNT_LAWYER_KEY_SHOW_EMAIL @"show_email"
#define REQUEST_ACCOUNT_LAWYER_KEY_PASSWORD @"password"
#define REQUEST_ACCOUNT_LAWYER_KEY_NAME @"name"
#define REQUEST_ACCOUNT_LAWYER_KEY_CPF @"cpf"
#define REQUEST_ACCOUNT_LAWYER_KEY_DATEOFBIRTH @"date_of_birth"
#define REQUEST_ACCOUNT_LAWYER_KEY_ADDRESS @"address"
#define REQUEST_ACCOUNT_LAWYER_KEY_NUMBER @"number"
#define REQUEST_ACCOUNT_LAWYER_KEY_NEIGHBORHOOD @"neighborhood"
#define REQUEST_ACCOUNT_LAWYER_KEY_UF @"uf"
#define REQUEST_ACCOUNT_LAWYER_KEY_CITY @"city"
#define REQUEST_ACCOUNT_LAWYER_KEY_CELLPHONE @"cellphone"
#define REQUEST_ACCOUNT_LAWYER_KEY_SHOW_CELLPHONE @"show_cellphone"
#define REQUEST_ACCOUNT_LAWYER_KEY_PHONE @"phone"
#define REQUEST_ACCOUNT_LAWYER_KEY_SHOW_PHONE @"show_phone"
#define REQUEST_ACCOUNT_LAWYER_KEY_SITE @"site"
#define REQUEST_ACCOUNT_LAWYER_KEY_LINKEDIN @"linkedin"
#define REQUEST_ACCOUNT_LAWYER_KEY_NUMBER_OAB @"number_oab"
#define REQUEST_ACCOUNT_LAWYER_KEY_TITLES @"titles"
#define REQUEST_ACCOUNT_LAWYER_KEY_POSTALCODE @"postal_code"
#define REQUEST_ACCOUNT_KEY_LAWYER_ACCEPTSNEW @"accepts_news"
#define REQQUEST_ACCOUNT_KEY_UF_OAB @"uf_oab"
#define REQQUEST_ACCOUNT_KEY_ABOUT @"about"
//UPLOAD IMAGE LAWYER KEY

#define REQQUEST_ACCOUNT_KEY_IMAGE_AVATA @"image_avatar"
#define REQQUEST_ACCOUNT_KEY_IMAGE_OAB @"image_oab"
#define REQQUEST_ACCOUNT_KEY_IMAGE_OAB_VERSE @"image_oab_verse"
#define REQQUEST_ACCOUNT_KEY_IMAGE_HOUSE_PROOF @"image_house_proof"


@interface LAAccountService : LABaseService
// Login with username (email) and password.
+ (void)loginWithEmail:(NSString*)email andPassword:(NSString*)password withDeviceData:(NSDictionary*)deviceData wCompleteBlock:(CompletionBlock)block;

+ (void)registerWithAccount:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block;
+ (void)logoutwCompleteBlock:(CompletionBlock)block;
+ (void)resetPassWithEmail:(NSString*)email wCompleteBlock:(CompletionBlock)block;
+ (void)registerLawyerWithAccount:(LAAccountObj *)obj wCompleteBlock:(CompletionBlock)block;
+ (void)updateAccountWithAccount:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block;
+ (void)getAccountInfo:(LAAccountObj*)obj wCompleteBlock:(CompletionBlock)block;
+ (void)getAccountInfowCompleteBlock:(CompletionBlock)block;
@end

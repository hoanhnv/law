//
//  MainService.m
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "MainService.h"
#import "LAHTTPClient.h"
#import "LACategoryObj.h"
#import "LAContactObj.h"
#import "LAStatusAccountObj.h"


@implementation MainService

+(void)fetchFaqCategories:(NSInteger)cateId withSuccess:(CompletionBlock)block{
    
    NSString *strBuilPath = [NSString stringWithFormat:API_PATH_GET_FAQ_BY_CATEGORIES,(long)cateId];
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:strBuilPath params:@{@"":@""} withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchFaqs:(CompletionBlock)block{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_FAQS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)fetchCategories:(CompletionBlockPagination)block
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil,0);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_CATEGORY params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        NSMutableArray* listCategories = [NSMutableArray new];
        NSString* responseMessage = message;
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            NSArray* arr = [data objectForKey:KEY_RESPONSE_DATA];
            responseMessage = [data objectForKey:KEY_RESPONSE_MESSAGE];
            if (arr && [arr isKindOfClass:[NSArray class]] && arr.count)
            {
                for (NSDictionary* dict in arr)
                {
                    LACategoryObj* obj = [[LACategoryObj alloc] initWithDictionary:dict];
                    if (obj)
                    {
                        [listCategories addObject:obj];
                    }
                }
            }
        }
        block(errorCode,message,listCategories,listCategories.count);
    }];
}
+(void)fetchContactSubjects:(CompletionBlock)block
{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_GET_CONTACT_SUBJECTS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)fetchFaqCategories:(CompletionBlock)block
{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_GET_FAQ_CATEGORIES params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)fetchUFs:(CompletionBlock)block
{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_UFS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)sendContact:(double)subjectId withMessage:(NSString *)message
         withEmail:(NSString *)email withBlock:(CompletionBlock)block{
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:message] ||[LAUtilitys NullOrEmpty:message]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [LABaseService postAndParseBasic:API_PATH_SEND_CONTACT params:@{@"subject_id":[NSNumber numberWithDouble:subjectId],@"message":message,@"email":email} withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)checkStatus:(NSString*)token  withBlock:(CompletionBlock)block
{
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:token]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:token forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_CHECK_STATUS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        NSString* strMessage = message;
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            strMessage = [data objectForKey:KEY_RESPONSE_MESSAGE];
            BOOL success = [[data objectForKey:KEY_RESPONSE_ERROR_CODE] boolValue];
            if (success)
            {
                id responseObject = [data objectForKey:KEY_RESPONSE_DATA];
                NSDictionary *dict = [responseObject objectAtIndex:0];
                NSString* str = [dict objectForKey:@"status"];
                LAStatusAccountObj* statusAccount = [LAStatusAccountObj new];
                statusAccount.status = str;
                statusAccount.message = strMessage;
                statusAccount.type = [LAUtilitys getAccountStatusFromString:str];
                block(0,strMessage,statusAccount);
            }
            else
            {
                block(-1000,strMessage,data);
            }
        }
        else
        {
            // Check authen failure
            block(errorCode,message,data);
        }
        
    }];
}

+(void)fetchBanners:(CompletionBlock)block{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_BANNER params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchLawsuitTheseByCategory:(NSInteger)cateId withBlock:(CompletionBlock)block{
    
    NSString *strBuilPath = [NSString stringWithFormat:API_PATH_LAWSUIT_THESES_BY_CATEGORIES,(long)cateId];
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:strBuilPath params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchLawsuitCategory:(CompletionBlock)block
{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_LAWSUIT_CATEGORIES params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)fetchAllsuit:(CompletionBlock)block
{
    [[LAHTTPClient sharedClient].requestSerializer setValue:STRING_EMPTY forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_LAWSUIT_ALL params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)fetchLawyerInfo:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_LAWYER_INFO params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchLawsuitTheses:(CompletionBlock)block{
    [LABaseService requestAndParseBasic:API_PATH_LAWSUIT_THESES params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)checkCEP:(NSString*)strCEP withBlock:(CompletionBlock)block{
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession]dataTaskWithURL:[NSURL URLWithString:[NSString stringWithFormat:API_PATH_CHECK_CEP,strCEP]] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSError *err = nil;
        if (data != nil) {
            id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            if ([json isKindOfClass:[NSDictionary class]]) {
                block([error code],[error localizedFailureReason],json);
            }
            else{
                block([err code],[err localizedFailureReason],nil);
            }
        }
        else{
            block([error code],[error localizedFailureReason],nil);
        }
        
        
    }];
    [dataTask resume];
}

+(void)fetchDistrictByUF:(NSString *)strUF withBlock:(CompletionBlock)block{
    if ([LAUtilitys NullOrEmpty:strUF]) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    NSString *strURLPath = [NSString stringWithFormat:API_PATH_DISTRICT_BY_UF,strUF];
    [LABaseService requestAndParseBasic:strURLPath params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)uploadImage:(NSDictionary*)aImageDic withBlock:(CompletionBlock)block{
    
    NSString *returnString;
    NSDictionary *aParametersDic; // It's contains other parameters.
    //    NSDictionary *aImageDic; // It's contains multiple image data as value and a image name as key
    NSString *urlString; // an url where the request to be posted
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] initWithURL:url] ;
    
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *postbody = [NSMutableData data];
    NSString *postData = [self getHTTPBodyParamsFromDictionary:aParametersDic boundary:boundary];
    [postbody appendData:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    [aImageDic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if(obj != nil)
        {
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"PostedImage\"; filetype=\"image/png\"; filename=\"%@\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[NSData dataWithData:obj]];
        }
    }];
    
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
}

+(NSString *) getHTTPBodyParamsFromDictionary: (NSDictionary *)params boundary:(NSString *)boundary
{
    NSMutableString *tempVal = [[NSMutableString alloc] init];
    for(NSString * key in params)
    {
        [tempVal appendFormat:@"\r\n--%@\r\n", boundary];
        [tempVal appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key,[params objectForKey:key]];
    }
    return [tempVal description];
}


+ (void)changePassWord:(NSString*)currentPass passWord:(NSString*)passWord confirmPassWord:(NSString*)strPassConfirm:(CompletionBlock)block{
    BOOL bValid = YES;
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if ([LAUtilitys NullOrEmpty:currentPass] || [LAUtilitys NullOrEmpty:passWord]
        || [LAUtilitys NullOrEmpty:strPassConfirm]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService postAndParseBasic:API_PATH_CHANGE_PASSWORD params:@{@"current_password":currentPass,@"password":passWord,@"confirm_password":strPassConfirm} withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchListPlan:(CompletionBlock)block{
    [LABaseService requestAndParseBasic:API_PATH_LIST_PLAN params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawyerRefuse:(double)lawsuit_id withrefusal_id:(double)refusal_id refusalDescription:(NSString*)refusalDescription withBlock:(CompletionBlock)block{
    if ([LAUtilitys NullOrEmpty:refusalDescription]) {
        refusalDescription = STRING_EMPTY;
    }
    NSDictionary *dictParams = @{@"lawsuit_id":[NSNumber numberWithDouble:lawsuit_id],@"refusal_id":[NSNumber numberWithDouble:refusal_id],@"refusal_description":refusalDescription};
    [LABaseService postAndParseBasic:API_PATH_LAWYER_REFUSE params:dictParams withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawSuitProgress:(double)lawsuit_id
     withProcessNumber:(NSString*)process_number
             withBlock:(CompletionBlock)block{
    
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    NSDictionary *params = @{@"lawsuit_id":[NSNumber numberWithDouble:lawsuit_id],@"process_number":process_number};
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    
    [LABaseService postAndParseBasic:API_PATH_LAWYER_PROGRESS params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawSuitAccept:(double)lawsuit_id
           withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    NSDictionary *params = @{@"lawsuit_id":[NSNumber numberWithDouble:lawsuit_id]};
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    [LABaseService postAndParseBasic:API_PATH_LAWYER_ACCEPT params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawSignatures:(double)plan_id
       withDistricts:(NSArray *)districts
           withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    
    if ([LAUtilitys NullOrEmpty:accessToken] || districts.count == 0) {
        block(-1000,@"Params must be not null",nil);
    }
    
    NSDictionary *params = @{@"plan_id":[NSNumber numberWithDouble:plan_id],@"districts":districts};
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    [LABaseService postAndParseBasic:API_PATH_LAWYER_SIGNATURES params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    
}

+(void)lawSignatures4:(NSDictionary*)params withSignaruteId:(NSInteger)signatureId withBlock:(CompletionBlock)block{
    
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    NSString *strPath = [NSString stringWithFormat:@"%@/%ld",API_PATH_LAWYER_SIGNATURES,signatureId];
    [LABaseService putAndParseBasic:strPath params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)fetchTermAndService:(CompletionBlock)block{
    [LABaseService requestAndParseBasic:API_PATH_TERM_AND_SERVICE params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchLawyerSignatures:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_LAWYER_LIST_SIGNATURES params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawyerCancelSignature:(NSDictionary*)params withSuccess:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    [LABaseService postAndParseBasic:API_PATH_LAWYER_CANCEL params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawyerTotalThese:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    [LABaseService requestAndParseBasic:API_PATH_LAWYER_TOTAL_THESES params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawyerUpdateThese:(NSDictionary *)params withSuccess:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    [LABaseService postAndParseBasic:API_PATH_LAWYER_UPDATE_THESES params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawyerGetBillSignature:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    [LABaseService requestAndParseBasic:API_PATH_LAWYER_GET_BILL_SIGNATURES params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    
}
+(void)lawyerGetSignatureTheses:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Params must be not null",nil);
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    [LABaseService requestAndParseBasic:API_PATH_LAWYER_GET_SIGNATURES_THESES params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}


+(void)fetchLawyerActionBy:(STATUS_ACTION)actionType withBlock:(CompletionBlock)block{    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    NSString *strAPIPath = STRING_EMPTY;
    switch (actionType) {
        case ACT_NEW:
            strAPIPath = API_PATH_LAWSUIT_LAWYER_NEW;
            break;
        case ACT_PENDING_PAYMENT:
            strAPIPath = API_PATH_LAWSUIT_LAWYER_PAYMENT_PENDING;
            break;
        case ACT_PROGRESS:
            strAPIPath = API_PATH_LAWSUIT_LAWYER_PROGRESS;
            break;
        case ACT_CLOSED:
            strAPIPath = API_PATH_LAWSUIT_LAWYER_CLOSED;
            break;
        default:
            break;
    }
    
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:strAPIPath params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    
}
+(void)fetchLawyerReasons:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_LAWSUIT_REFUSAL_REASON params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
    
}
+(void)fetchHappeningByID:(NSInteger)Id withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    NSString *strPath = [NSString stringWithFormat:API_PATH_GET_HAPPENING_BYID,Id];
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:strPath params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)lawsuitFinalize:(NSInteger)lawsuit_id withSuccess:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    NSDictionary *dictParam = @{@"lawsuit_id":[NSNumber numberWithInteger:lawsuit_id]};
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService postAndParseBasic:API_PATH_LAWSUIT_FINALIZE params:dictParam withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchContractedThese:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_CONTRACTED_THESE params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)fetchBanks:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    //    if ([LAUtilitys NullOrEmpty:accessToken]) {
    //        bValid = NO;
    //    }
    //    if (!bValid) {
    //        block(-1000,@"Params must be not null",nil);
    //        return;
    //    }
    //    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService requestAndParseBasic:API_PATH_BANKS params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

+(void)updateCard:(NSDictionary *)params withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
//    [LABaseService postData:API_PATH_EDITCARD params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
//        block(errorCode,message,data);
//    }];
    [LABaseService postAndParseBasic:API_PATH_EDITCARD params:params withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)reValidateToken:(NSDictionary *)dictDeviceData withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService postAndParseBasic:API_PATH_AUTHENTICATE_VALIDATE params:dictDeviceData withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];

}
+(void)postThesisClick:(NSDictionary*)param withBlock:(CompletionBlock)block{
    [LABaseService postAndParseBasic:API_PATH_THESIS_CLICK params:param withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)getMessage:(NSInteger)Id withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    NSString *strPath = [NSString stringWithFormat:API_PATH_GET_MESSAGES,Id];
    [LABaseService requestAndParseBasic:strPath params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)postMessage:(NSDictionary*)param withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService postAndParseBasic:API_PATH_POST_MESSAGE params:param withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)getActionById:(NSInteger)Id withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    NSString *strBuilPath = [NSString stringWithFormat:API_PATH_GET_ACTION_BY_ID,(long)Id];
    [LABaseService requestAndParseBasic:strBuilPath params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)getPercentAction:(CompletionBlock)block{
    //API_PATH_GET_PERCENT
    [LABaseService requestAndParseBasic:API_PATH_GET_PERCENT params:nil withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}
+(void)postLocation:(NSDictionary*)param withBlock:(CompletionBlock)block{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    BOOL bValid = YES;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        bValid = NO;
    }
    if (!bValid) {
        block(-1000,@"Params must be not null",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [LABaseService postAndParseBasic:API_PATH_POST_LOCATION params:param withCompletionBlock:^(NSInteger errorCode, NSString *message, id data) {
        block(errorCode,message,data);
    }];
}

@end

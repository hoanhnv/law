//
//  LABaseService.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseService.h"

@implementation LABaseService
+ (void) requestAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    [[LAHTTPClient sharedClient] GET:path parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSInteger statusCode = 0;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        if ([httpResponse isKindOfClass:[NSHTTPURLResponse class]]) {
            statusCode = httpResponse.statusCode;
        }
        switch (statusCode) {
                // Not Authencate
            case 401:{
                block(statusCode,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),error);
            }
                break;
            default:
                block(error.code,NSLocalizedString(MESSAE_CLIENT_RESPONSE_FAILRE, @""),error);
                break;
        }
        
        
    }];
}
+ (void) postAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    NSLog(@"path_postAndParseBasic %@", path);
    [[LAHTTPClient sharedClient] POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        if (error.localizedFailureReason == nil){
            block(error.code,error.localizedDescription,resData);
        }else{
            block(error.code,error.localizedFailureReason,resData);
        }
    }];
}
+ (void) putAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    [[LAHTTPClient sharedClient]PUT:path parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        if (error.localizedFailureReason == nil){
            block(error.code,error.localizedDescription,resData);
        }else{
            block(error.code,error.localizedFailureReason,resData);
        }
    }];
}
+ (void) deleteAndParseBasic:(NSString*) path params:(NSDictionary*) params withCompletionBlock:(CompletionBlock) block
{
    [[LAHTTPClient sharedClient] DELETE:path parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        block(0,nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        if (error.localizedFailureReason == nil){
            block(error.code,error.localizedDescription,resData);
        }else{
            block(error.code,error.localizedFailureReason,resData);
        }
    }];
}

+ (void) uploadImages:(NSMutableArray*)arrImgages andListKeys:(NSMutableArray*)keys andListName:(NSMutableArray*)names andParams:(NSDictionary*)params andPath:(NSString*)path withCompletionBlock:(CompletionBlock) block progressBlock:(ProgressBlockUpload)blockProgress
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        accessToken = [LADataCenter shareInstance].registerToken;
    }
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil);
        return;
    }
    if (arrImgages.count == 0 || arrImgages == nil)
    {
        block(-1000,@"Don't have any image",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [[LAHTTPClient sharedClient] POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < arrImgages.count; i ++)
        {
            UIImage* imageUpload = [arrImgages objectAtIndex:i];
            NSString* strName = [names objectAtIndex:i];
            NSString* strKey = [keys objectAtIndex:i];
            [formData appendPartWithFileData: UIImagePNGRepresentation(imageUpload) name:strKey fileName:strName mimeType:@"image/jpeg"];
            //            [formData appendPartWithFormData:UIImageJPEGRepresentation(imageUpload, 1.0) name:strKey];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        double currentProgress = uploadProgress.fractionCompleted;
        blockProgress(currentProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"data response upload:%@",responseObject);
        block(0,@"",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        NSLog(@"data response upload error:%@",resData);
        
        if (error.localizedFailureReason == nil){
            block(error.code,error.localizedDescription,resData);
        }else{
            block(error.code,error.localizedFailureReason,resData);
        }
    }];
}
+ (void) uploadImage:(UIImage*)imageUpload andParams:(NSDictionary*)params andPath:(NSString*)path withCompletionBlock:(CompletionBlock) block progressBlock:(ProgressBlockUpload)blockProgress
{
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil);
        return;
    }
    [[LAHTTPClient sharedClient].requestSerializer setValue:accessToken forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    [[LAHTTPClient sharedClient] POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData: UIImagePNGRepresentation(imageUpload) name:@"image_rg" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        double currentProgress = uploadProgress.fractionCompleted;
        blockProgress(currentProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        block(0,@"",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *resData = nil;
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            resData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        }
        if (error.localizedFailureReason == nil){
            block(error.code,error.localizedDescription,resData);
        }else{
            block(error.code,error.localizedFailureReason,resData);
        }
    }];
}

+(void)postData:(NSString*)path params:(NSDictionary*)params withCompletionBlock:(CompletionBlock) block{
    NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",BASE_API_URL,path]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:10];
    
    [request setHTTPMethod:@"POST"];
    NSString *accessToken = [LADataCenter shareInstance].currentAccessToken;
    
    if ([LAUtilitys NullOrEmpty:accessToken]) {
        block(-1000,@"Token not found",nil);
        return;
    }
    
    [request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    [request setValue: @"text/html" forHTTPHeaderField:@"Content-Type"];
    
    NSData *JSONData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    NSString *contentJSONString = [[NSString alloc] initWithData:JSONData encoding:NSUTF8StringEncoding];
    
    [request setHTTPBody: [contentJSONString dataUsingEncoding:NSUTF8StringEncoding]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:BASE_API_URL]];
    __block NSURLSessionDataTask *task = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
        if (error) {
            // error handling
            block(error.code,error.localizedDescription,responseObject);
        } else {
            // success
            block(0,nil,responseObject);
        }
    }];
    
    [task resume];
    
}

@end

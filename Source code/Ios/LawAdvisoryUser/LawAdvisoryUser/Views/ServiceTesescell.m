//
//  ServiceTesescell.m
//  JustapLawyer
//
//  Created by Tung Nguyen on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "ServiceTesescell.h"
#import "UIFont+Law.h"
@implementation ServiceTesescell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)layoutSubviews{
    UITapGestureRecognizer *tapCheck = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeCheckbox:)];
    [self.viewCheck addGestureRecognizer:tapCheck];
    
    //    [LAUtilitys setButtonTextUnderLine:self.btnVerDetalhes];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
+(CGFloat)getHeight:(NSString*)string inWith:(UITableView*)table{
    CGSize labelTextSize = [string boundingRectWithSize:CGSizeMake(table.frame.size.width, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{
                                                          NSFontAttributeName : [UIFont robotoRegular:16.0]
                                                          }
                                                context:nil].size;
    if (labelTextSize.height > 84) {
        return labelTextSize.height + (84 - 55);
    }
    else{
        return 84;
    }
}

- (IBAction)doSeemore:(id)sender {
    [self.delegate doSeeMore:self.indexPath];
}

- (IBAction)changeCheckbox:(id)sender {
    [self.btnCheck setSelected:!self.btnCheck.selected];
    [self.delegate doChangeSelect:self.indexPath];
}

@end

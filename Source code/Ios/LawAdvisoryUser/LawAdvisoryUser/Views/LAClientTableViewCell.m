//
//  LAClientTableViewCell.m
//  JustapUser
//
//  Created by MAC on 11/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAClientTableViewCell.h"

@implementation LAClientTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setupCell:(LAUserObj*)user{
    self.lbName.text = [LAUtilitys getDefaultString:user.name];
    self.lbEmail.text = [LAUtilitys getDefaultString:user.email];
    self.lbPhone.text = [LAUtilitys getDefaultString:user.phone];
    self.lbCEP.text = user.postalCode;
    self.lbCPF.text = user.cpf;
    self.lbEnde.text = user.complement;
    self.lbMobile.text = user.cellphone;
    self.lbCidade.text = user.city;
    self.lbEstado.text = user.uf;
    self.lbNumber.text = user.number;
    self.lbBirthDay.text = [LAUtilitys getDateStringFromTimeInterVal:user.dateOfBirth withFormat:FORMAT_DD_MM_YY];
    self.lbNeightbor.text = user.neighborhood;
}
- (void)setupData:(LAWitnesses*)data{
    self.lbName.text = data.name;
    self.lbName.textColor = [UIColor whiteColor];
}

@end

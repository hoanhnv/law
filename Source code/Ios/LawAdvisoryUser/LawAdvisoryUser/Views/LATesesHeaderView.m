//
//  LATesesHeaderView.m
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LATesesHeaderView.h"

@implementation LATesesHeaderView

#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [self.delegate textFieldShouldClear:textField];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.delegate textFieldShouldReturn:textField];
    return YES;
}
@end

//
//  LAMoreInfoDetailCell.m
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAMoreInfoDetailCell.h"

@implementation LAMoreInfoDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(id)dataObj
{
    
}
+(CGFloat)getHeight:(NSString*)obj inWith:(UITableView*)table{
    CGSize size = table.frame.size;
    size.width = CGRectGetWidth(table.frame) - 45;
    CGSize labelTextSize = [obj boundingRectWithSize:CGSizeMake(size.width, MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{
                                                       NSFontAttributeName : [UIFont robotoRegular:15.0]
                                                       }
                                             context:nil].size;
    if (labelTextSize.height > 110) {
        return labelTextSize.height;
    }
    else{
        return 110;
    }
    
}
@end

//
//  LAPendenteTableViewCell.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAPendenteTableViewCellType4 : UITableViewCell
@property (nonatomic) IBOutlet UIButton* btArcharAdvogado;
@property (nonatomic) IBOutlet UIButton* btConfirmarMeusDados;
@property (nonatomic) IBOutlet UIButton* btArcharNovoAdvogado;
@property (nonatomic) IBOutlet UIButton* btVerdetalhes;
@property (nonatomic) IBOutlet UILabel* lbCategoria;
@property (nonatomic) IBOutlet UILabel* lbAssunto;
@property (nonatomic) IBOutlet UILabel* lbData;
@property (nonatomic) IBOutlet UILabel* lbAdvogado;
@property (nonatomic) IBOutlet UILabel* lbProtocolo;
@property (nonatomic) id delegate;
@property (nonatomic) NSIndexPath* currentPath;
- (void)setupData:(id)dataObj;
- (IBAction)onConfirmasMeusDadosPresserd:(id)sender;
@end

@protocol AcoesPendenteCellType4Delegate <NSObject>

- (void)onConfirmasMeusDadosDidTouch:(NSIndexPath*)indexPath;

@end
//
//  LATesesHeaderView.h
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "AwesomeTextField.h"

@interface LATesesPriceHeaderView : UIView
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtName;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtInitiaPrice;
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtPriceSuccess;
@property (weak, nonatomic) IBOutlet UIButton *btSelectionTeses;
@property (weak, nonatomic) IBOutlet UILabel* txtTitleSelection;
@property (nonatomic) id delegate;
@end
@protocol TesesPriceHeaderViewDelegate <NSObject>

- (void)onTesesPriceHeaderPressed;
- (void)onConfirmPricePressed:(LATesesPriceHeaderView*)header;
@end

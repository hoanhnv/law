//
//  LADetalhesHeaderView.h
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AwesomeTextField.h"
#import "LACustomerOpenObj.h"
@protocol DetalHesPagamentoDelegate <NSObject>

- (void)onPressDetalhesPagementoPressed;
- (void)onPressOkPressed:(NSString*)strValue;

@end
@interface LADetalhesHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (nonatomic) IBOutlet UIButton* btVerdetalhes;
@property (nonatomic) IBOutlet UILabel* lbStatus;
@property (nonatomic) IBOutlet UILabel* lbCategoria;
@property (nonatomic) IBOutlet UILabel* lbAssunto;
@property (nonatomic) IBOutlet UILabel* lbData;
@property (nonatomic) IBOutlet UILabel* lbClient;
@property (nonatomic) IBOutlet UIButton* btnOk;
@property (weak, nonatomic) IBOutlet UILabel *titleRefusal;
@property (weak, nonatomic) IBOutlet UILabel *lbRefusal;
@property (nonatomic) IBOutlet AwesomeTextField* txtNumber;
@property (nonatomic) LACustomerOpenObj* objData;
@property (nonatomic) id <DetalHesPagamentoDelegate> delegate;
@end

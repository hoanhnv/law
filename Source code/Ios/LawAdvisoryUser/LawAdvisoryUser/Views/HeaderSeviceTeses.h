//
//  headerSeviceTeses.h
//  JustapLawyer
//
//  Created by Tung Nguyen on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LALawSuitCategory;

typedef void (^ShowBlock)(BOOL);

@interface HeaderSeviceTeses : UIView{
    
}

@property  (nonatomic) BOOL isGreeen;
@property (nonatomic,strong) ShowBlock  showBlock;
@property (nonatomic,assign) BOOL isShowContent;

@property (weak, nonatomic) IBOutlet UIImageView *imgLine;
@property (weak, nonatomic) IBOutlet UIButton *btnDowUp;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbTitle;

@property (nonatomic, strong) LALawSuitCategory *obj;

- (IBAction)showContent:(id)sender;
- (void)setupColor:(BOOL)isShow;

-(void)setupView:(LALawSuitCategory*)obj;

@end



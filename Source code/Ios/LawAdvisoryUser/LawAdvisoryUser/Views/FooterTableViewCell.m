//
//  FooterTableViewCell.m
//  JustapLawyer
//
//  Created by Tùng Nguyễn on 2/14/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import "FooterTableViewCell.h"

@implementation FooterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+(CGFloat)getHeight:(NSString*)string inWith:(UITableView*)table{
    CGSize labelTextSize = [string boundingRectWithSize:CGSizeMake(table.frame.size.width - 64, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{
                                                          NSFontAttributeName : [UIFont robotoMediumItalic:15.0]
                                                          }
                                                context:nil].size;
    if (labelTextSize.height > 0) {
        return labelTextSize.height + 59;
    }
    else{
        return 59;
    }
}
@end

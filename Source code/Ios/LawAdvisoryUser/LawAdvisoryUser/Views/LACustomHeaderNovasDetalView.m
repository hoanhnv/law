//
//  LACustomHeaderNovasDetalView.m
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACustomHeaderNovasDetalView.h"

@implementation LACustomHeaderNovasDetalView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.imgBottomLine setBackgroundColor:[LAUtilitys jusTapColor]];
}
- (IBAction)onHeaderPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectNovasDetalHeader)])
    {
        [self.delegate didSelectNovasDetalHeader];
    }
}
@end

//
//  LAMoreInfoDetailCell.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAMoreInfoDetailCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel* lbDescription;
- (void)setupData:(id)dataObj;
+(CGFloat)getHeight:(NSString*)obj inWith:(UITableView*)table;
@end

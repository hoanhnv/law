//
//  LACustomHeaderNovasDetalView.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAHeaderSectionPlanosSelectionView : UIView
@property (nonatomic) IBOutlet UILabel* lbTitle;
@property (nonatomic) IBOutlet UIImageView* imgBottomLine;
+ (CGSize)getHeight:(NSString*)txtTitle andUIlabelSize:(CGRect)rect;
@end

//
//  LACategoriesTableViewCell.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/9/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LACategoriesTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) IBOutlet NSMutableArray* arrData;
@property (nonatomic) IBOutlet UICollectionView* clCategories;
@property (nonatomic) IBOutlet UIImageView* imgBottomLine;
@property (nonatomic) id delegate;
@property (nonatomic) NSIndexPath* indexPath;
@property (nonatomic) UIColor* colorOfSection;
@end
@protocol CategoriesChooseDelegate <NSObject>

- (void)onChooseAtIndex:(NSIndexPath*)indexPath;

@end
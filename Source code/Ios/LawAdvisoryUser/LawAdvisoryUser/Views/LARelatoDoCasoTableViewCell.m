//
//  LARelatoDoCasoTableViewCell.m
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LARelatoDoCasoTableViewCell.h"
#import "LAAudioPlayerController.h"
#import "AppDelegate.h"

@implementation LARelatoDoCasoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.sldTimeAudio.value = 0.0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus:) name:@"changeFilePlay" object:nil];
    // Initialization code
}
- (void)changeStatus:(NSNotification*)notif
{
    if (notif != nil && notif.object != nil) {
        NSInteger idPlayer = [notif.object integerValue];
        if (idPlayer != self.Id) {
            self.isPlaying = false;
            [self.sldTimeAudio setValue:0.0f animated:NO];
            [self.btPausePlay setImage:[UIImage imageNamed:@"ic_play_audio"] forState:UIControlStateNormal];
        }
        else
        {
            [self startTime];
        }
    }
}
- (void)dealloc
{
    [self stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)addStreamerObserver {
    [[LAAudioPlayerController sharedInstance].streamer addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:kStatusKVOKey];
    [[LAAudioPlayerController sharedInstance].streamer addObserver:self forKeyPath:@"duration" options:NSKeyValueObservingOptionNew context:kDurationKVOKey];
    [[LAAudioPlayerController sharedInstance].streamer addObserver:self forKeyPath:@"bufferingRatio" options:NSKeyValueObservingOptionNew context:kBufferingRatioKVOKey];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
        if (context == kStatusKVOKey) {
            [self performSelector:@selector(updateStatus)
                         onThread:[NSThread mainThread]
                       withObject:nil
                    waitUntilDone:NO];
        } else if (context == kDurationKVOKey) {
            [self performSelector:@selector(_timerAction:)
                         onThread:[NSThread mainThread]
                       withObject:nil
                    waitUntilDone:NO];
            
        } else if (context == kBufferingRatioKVOKey) {
            
        } else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    
}
- (void)_timerAction:(id)timer
{
//    if (self.Id == [AppDelegate sharedDelegate].currentID){
//    if ([[LAAudioPlayerController sharedInstance].streamer duration] == 0.0) {
//        [self.sldTimeAudio setValue:0.0f animated:NO];
//    }
//    else {
//        [self.sldTimeAudio setValue:[[LAAudioPlayerController sharedInstance].streamer currentTime] / [[LAAudioPlayerController sharedInstance].streamer duration] animated:YES];
//    }
//    }
    [self updateValue];
}
- (void)updateStatus {
     self.isPlaying = false;
    switch ([[LAAudioPlayerController sharedInstance].streamer status]) {
           
        case DOUAudioStreamerPlaying:
        {
            self.isPlaying = true;
             [[NSNotificationCenter defaultCenter] postNotificationName:@"changeFilePlay" object:[NSNumber numberWithInteger:self.Id]];
            if (self.Id == [AppDelegate sharedDelegate].currentID)
            {
                [self.btPausePlay setImage:[UIImage imageNamed:@"ic_stop_audio"] forState:UIControlStateNormal];
                [self startTime];
            }
            
        }
            break;
            
        case DOUAudioStreamerPaused:
        {
            [self.btPausePlay setImage:[UIImage imageNamed:@"ic_play_audio"] forState:UIControlStateNormal];
        }
            break;
        case DOUAudioStreamerIdle:
        case DOUAudioStreamerBuffering:
        case DOUAudioStreamerError:
        {
            [self.btPausePlay setImage:[UIImage imageNamed:@"ic_play_audio"] forState:UIControlStateNormal];
        }
            break;
        case DOUAudioStreamerFinished:
        {
            NSLog(@"ID:%ld",(long)self.Id);
            [self stop];
            [self.sldTimeAudio setValue:0.0f animated:NO];
            [self.btPausePlay setImage:[UIImage imageNamed:@"ic_play_audio"] forState:UIControlStateNormal];
        }
            break;
            
    }
}
- (IBAction)actionSliderProgress:(id)sender
{
    if ([AppDelegate sharedDelegate].currentID == self.Id)
    {
        [[LAAudioPlayerController sharedInstance].streamer setCurrentTime:[[LAAudioPlayerController sharedInstance].streamer duration] * [self.sldTimeAudio value]];
    }
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.sldTimeAudio setCenter:CGPointMake(self.sldTimeAudio.center.x, 49)];
}
- (void)setupData
{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)stop
{
    if (self.Id != [AppDelegate sharedDelegate].currentID)
    {
        if (timeDuration != nil && timeDuration.isValid) {
            [timeDuration invalidate];
        }
        @try {
            [self removeStreamerObserver];
        } @catch(id anException){
        }
    }
    
}
- (void)removeStreamerObserver {
    [[LAAudioPlayerController sharedInstance].streamer removeObserver:self forKeyPath:@"status"];
    [[LAAudioPlayerController sharedInstance].streamer removeObserver:self forKeyPath:@"duration"];
    [[LAAudioPlayerController sharedInstance].streamer removeObserver:self forKeyPath:@"bufferingRatio"];
}
- (IBAction)onPausePlayPressed:(id)sender
{
   
    if (timeDuration && timeDuration.isValid)
    {
        [timeDuration invalidate];
    }
    if (self.objAudio)
    {
        if ([AppDelegate sharedDelegate].currentID == self.Id)
        {
            if(self.isPlaying || [LAAudioPlayerController sharedInstance].streamer.status == DOUAudioStreamerPaused)
            {
                [[LAAudioPlayerController sharedInstance] didTouchMusicToggleButton:nil];
                if (self.isPlaying) {
                    [[LAAudioPlayerController sharedInstance] .streamer pause];
                }
                else
                {
                    [[LAAudioPlayerController sharedInstance] .streamer play];
                    [self startTime];
                }
            }
            else
            {
//                [[LAAudioPlayerController sharedInstance].streamer stop];
                
                @try {
                    [self removeStreamerObserver];
                } @catch(id anException){
                }
                [[LAAudioPlayerController sharedInstance] createSteammerWithUrl:[NSURL URLWithString:self.objAudio.mediaWithUrl]];
                [self addStreamerObserver];
                [self.sldTimeAudio setValue:0.0 animated:YES];
                [self startTime];
                
                //
            }
        }
        else
        {
            
            @try {
                [self removeStreamerObserver];
            } @catch(id anException){
            }
            [[LAAudioPlayerController sharedInstance] createSteammerWithUrl:[NSURL URLWithString:self.objAudio.mediaWithUrl]];
            [self addStreamerObserver];
            [self.sldTimeAudio setValue:0.0 animated:YES];
            [self startTime];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeFilePlay" object:[NSNumber numberWithInteger:self.Id]];
     [AppDelegate sharedDelegate].currentID = self.Id;
}
- (void)startTime
{
    if (timeDuration != nil ) {
        [timeDuration invalidate];
        timeDuration = nil;
    }
    if (timeDuration == nil)
    {
        timeDuration = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateValue) userInfo:nil repeats:YES];
    }
    
}
-(void) actionClick{
    
}
- (void)updateValue
{
    if (self.Id == [AppDelegate sharedDelegate].currentID)
    {
        if ([[LAAudioPlayerController sharedInstance].streamer duration] == 0.0) {
            [self.sldTimeAudio setValue:0.0f animated:NO];
        }
        else {
            [self.sldTimeAudio setValue:[[LAAudioPlayerController sharedInstance].streamer currentTime] / [[LAAudioPlayerController sharedInstance].streamer duration] animated:YES];
        }
    }
    else
    {
        [self.sldTimeAudio setValue:0.0f animated:NO];
        [self.btPausePlay setImage:[UIImage imageNamed:@"ic_play_audio"] forState:UIControlStateNormal];
        [self stop];
    }
    
}
@end

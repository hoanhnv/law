//
//  LAServiceTeses.h
//  JustapLawyer
//
//  Created by Tung Nguyen on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABaseViewController.h"
#import "AwesomeTextField.h"

@interface LAServiceTeses : UIView
@property (strong, nonatomic) IBOutlet UILabel *txtTitle;
//@property (weak, nonatomic) IBOutlet AwesomeTextField *txtSeachTeses;
@property (strong, nonatomic) IBOutlet AwesomeTextField *txtSeachTeses;
@property (strong, nonatomic) IBOutlet UITableView *tbvCategoria;
@property (strong, nonatomic) IBOutlet UIButton *btnAvancar;
@property (strong, nonatomic) IBOutlet UIButton *doAvancar;



@end

//
//  LAPlanosSelectionTableViewCell.m
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPlanosSelectionTableViewCell.h"

@implementation LAPlanosSelectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)onSelectionPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onSelectionAtIndex:)])
    {
        [self.delegate onSelectionAtIndex:self.indexPath.row];
    }
}
-(void)setupCell:(LAPlanObj*)planObj withComacar:(NSString *)strCom{
    self.lbName.text = [planObj.name uppercaseString];
    self.lbValue.text = [NSString stringWithFormat:@"R$ %@",[LAUtilitys getCurrentcyFromDecimal:planObj.value]];
    self.lbQuantity.text = [NSString stringWithFormat:@"%.0f %%",planObj.quantityOfTheses];
    self.lbComarca.text = strCom;
}
@end

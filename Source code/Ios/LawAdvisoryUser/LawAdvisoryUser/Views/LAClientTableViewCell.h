//
//  LAClientTableViewCell.h
//  JustapUser
//
//  Created by MAC on 11/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAUserObj.h"
#import "LAWitnesses.h"
@interface LAClientTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIImageView* imgAvatar;
@property (nonatomic,weak) IBOutlet UILabel* lbName;
@property (nonatomic,weak) IBOutlet UILabel* lbEmail;
@property (nonatomic,weak) IBOutlet UILabel* lbPhone;
@property (weak, nonatomic) IBOutlet UILabel *lbCPF;
@property (weak, nonatomic) IBOutlet UILabel *lbCEP;
@property (weak, nonatomic) IBOutlet UILabel *lbBirthDay;
@property (weak, nonatomic) IBOutlet UILabel *lbEnde;
@property (weak, nonatomic) IBOutlet UILabel *lbNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbEstado;
@property (weak, nonatomic) IBOutlet UILabel *lbNeightbor;
@property (weak, nonatomic) IBOutlet UILabel *lbCidade;
@property (weak, nonatomic) IBOutlet UILabel *lbMobile;

-(void)setupCell:(LAUserObj*)user;
- (void)setupData:(LAWitnesses*)data;
@end

//
//  HeaderInfo.m
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "HeaderSectionCencelMagento.h"
#import "UIColor+Law.h"
#import "UIFont+Law.h"
@implementation HeaderSectionCencelMagento
#define DefaultColor [UIColor colorWithRed:209.0/255.0 green:211.0/255.0 blue:94.0/255.0 alpha:1.0]

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.isGreeen = YES;
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showContent:)];
        tap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tap];
        
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        // Call a common method to setup gesture and state of UIView
        self.isGreeen = YES;
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showContent:)];
        tap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tap];
    }
    return self;
}
-(void)setupView{
    _lblTitle.textColor = [UIColor blackColor];
    _imgBottom.backgroundColor = _color;
    if (self.strTitle) {
        self.lblTitle.text = [self.strTitle uppercaseString];
        if (CGRectGetMaxY(self.lblTitle.frame) > CGRectGetMaxY(self.btnDownUp.frame)) {
            self.imgBottom.frame = CGRectMake(self.imgBottom.frame.origin.x, CGRectGetMaxY(self.lblTitle.frame) + 8, self.imgBottom.frame.size.width, self.imgBottom.frame.size.height);
        }
    }
}
- (IBAction)showContent:(id)sender {
    if (!_isShowContent) {
        _isShowContent = YES;
    } else {
        _isShowContent = NO;
    }
    if (_showBlock) {
        _showBlock(_isShowContent);
    }
    [self setupColor:_isShowContent];
    [self setupView];
}
- (void)setupColor:(BOOL)isShow
{
    _isShowContent = isShow;
    if (!isShow) {
        //_lblTitle.textColor = [UIColor colorWithRed:46.0/255.0 green:138.0/255.0 blue:90.0/255.0 alpha:1.0];
        
        
        //Đổi màu image
        UIImageView *btn = _imgButton;
        //btn.image = [UIImage imageNamed:@"dropdown_green"];
        btn.image = [btn.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [btn setTintColor:_color];
        _imgButton = btn;
        //[_btnDownUp setBackgroundImage:btn.image forState:UIControlStateNormal];
        
        
    } else {
        
        //[_btnDownUp setBackgroundImage:[UIImage imageNamed:@"60x60_dropup"] forState:UIControlStateNormal];
        UIImageView *btn;
        [_imgButton setImage:[UIImage imageNamed:@"dropup_green"]];
        btn = _imgButton;
        //btn.image = [UIImage imageNamed:@"dropup_green"];
        btn.image = [btn.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [btn setTintColor:[UIColor blackColor]];
        _imgButton = btn;
        //_imgBottom.backgroundColor = DefaultColor;
        
    }
    
}
+(CGFloat)getHeight:(NSString*)title inWith:(UITableView*)table{
    CGSize labelTextSize = [[title uppercaseString] boundingRectWithSize:CGSizeMake(table.frame.size.width, MAXFLOAT)
                                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                                              attributes:@{
                                                                           NSFontAttributeName : [UIFont robotoRegular:14.0]
                                                                           }
                                                                 context:nil].size;
    if (labelTextSize.height > 48) {
        return labelTextSize.height + 30;
    }
    else{
        return 48;
    }
}
@end

//
//  LASelectedTheseHeader.h
//  JustapLawyer
//
//  Created by Mac on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LASelectedTheseHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

@end

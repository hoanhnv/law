//
//  RecordItemTableViewCell.h
//  DisplayRecorder
//
//  Created by Le Van Thang on 4/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordItemTableViewCell : UITableViewCell
{
    UIButton*       _btnPlay;
    UILabel*        _detailTextLabel2;
    UIImageView*    _imvBackground;
    BOOL            _bIsPlaying;
    id              delegate;
}


@property (nonatomic, retain) UIButton* buttonPlay;
@property (nonatomic, retain) UILabel* detailTextLabel2;
@property (nonatomic, assign) id delegate;

- (void) togglePlayButton;
- (void) setPlayButton: (BOOL) play;

@end

//
//  LAAssinaturaTableViewCell.h
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LASignatureTeseObj.h"

@interface LAAssinaturaTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbPercentSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lbInitFees;

-(void)setupCell:(LASignatureTeseObj*)obj;

@end

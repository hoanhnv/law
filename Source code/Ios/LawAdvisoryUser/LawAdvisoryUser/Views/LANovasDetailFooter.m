//
//  LANovasDetailFooter.m
//  JustapLawyer
//
//  Created by Mac on 3/18/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import "LANovasDetailFooter.h"

@implementation LANovasDetailFooter

- (IBAction)onAceiterDetailPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onAceiterDetailDidTouch:)])
    {
        [self.delegate onAceiterDetailDidTouch:self.currentPath];
    }
}
- (IBAction)onRecusarDetailPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onRecusarDetailDidTouch:)])
    {
        [self.delegate onRecusarDetailDidTouch:self.currentPath];
    }
}
@end

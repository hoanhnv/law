//
//  LAListAudioTableViewCell.h
//  JustapLawyer
//
//  Created by Mac on 11/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAListAudioTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *lstAudio;
@property (nonatomic, strong)NSArray *arrWitness;

@end

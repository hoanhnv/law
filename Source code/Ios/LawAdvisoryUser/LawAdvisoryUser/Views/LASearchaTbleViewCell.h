//
//  LASearchaTbleViewCell.h
//  JustapLawyer
//
//  Created by MAC on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LASearchLocalObj.h"
#import "LAUtilitys.h"
@interface LASearchaTbleViewCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel* txtTypeSearch;
@property (nonatomic) IBOutlet UILabel* txtTitle;
@property (nonatomic) IBOutlet UILabel* txtDescript;
@property (nonatomic) TYPE_SEARCH_CELL typeCell;
@property (nonatomic) LASearchLocalObj* objSearch;
- (void)setUpdata:(LASearchLocalObj*)data;
@end

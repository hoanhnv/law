//
//  LADetalhesHeaderView.m
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LADetalhesHeaderView.h"

@implementation LADetalhesHeaderView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)layoutSubviews{
    self.btnOk.layer.cornerRadius = 3;
    self.btnOk.layer.borderColor = [LAUtilitys jusTapColor].CGColor;
    self.btnOk.layer.borderWidth = 1.0;
    self.lbTime.textColor = [UIColor redColor];
}
- (IBAction)onDetalhesPagamento:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onPressDetalhesPagementoPressed)])
    {
        [self.delegate onPressDetalhesPagementoPressed];
    }
}

- (IBAction)onOkPressd:(id)sender {
    [self.txtNumber resignFirstResponder];
    [self.delegate onPressOkPressed:self.txtNumber.text];
}

@end

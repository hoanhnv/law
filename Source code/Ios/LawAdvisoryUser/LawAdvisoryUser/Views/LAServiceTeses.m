//
//  LAServiceTeses.m
//  JustapLawyer
//
//  Created by Tung Nguyen on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAServiceTeses.h"
#import "ServiceTesescell.h"
#import "LAQuestionObj.h"
@interface LAServiceTeses ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableDictionary * mSelected;
}

@end

@implementation LAServiceTeses

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupView];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self setupView];
}
-(void)setupView{
    self.txtTitle.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_bottom_line_category"]];
    _tbvCategoria.estimatedRowHeight = 180;
    _tbvCategoria.rowHeight = UITableViewAutomaticDimension;
    [_tbvCategoria registerNib:[UINib nibWithNibName:NSStringFromClass([LAServiceTeses class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([LAServiceTeses class])];
    mSelected = [NSMutableDictionary new];
    [mSelected setObject:@(1) forKey:@"0"];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.tbvCategoria) {
        return 1;
    }
    else{
        return 0;
        //return [lstFaqs.data count];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tbvCategoria) {
        return [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",section]]integerValue];
    }
    else{
        return 5;
        //return [lstFaqCategory.data count];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tbvCategoria) {
        for (NSString * key in mSelected.allKeys) {
            if (indexPath.section == [key integerValue]) {
                BOOL isShow = [[mSelected objectForKey:[NSString stringWithFormat:@"%ld",indexPath.section]] integerValue] == 1;
                if (isShow)
                {
                    //LAQuestionObj *questionInfo = (LAQuestionObj*)lstFaqs.data[indexPath.section] ;
                    //return [ServiceTesescell getHeight:questionInfo.answer inWith:_tbvCategoria];
                }
            }
        }
        return 0.01f;
    }
    else
    {
        return 44.0f;
    }
}

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view from its nib.
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

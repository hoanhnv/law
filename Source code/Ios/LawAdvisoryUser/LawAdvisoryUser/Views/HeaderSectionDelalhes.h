//
//  HeaderSectionDelalhes.h
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderSectionCencelMagento.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
@interface HeaderSectionDelalhes : UIView

@property (nonatomic,strong) ShowBlock  showBlock;
@property (nonatomic,assign) BOOL isShowContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottom;
@property (weak, nonatomic) IBOutlet UIImageView *imgDownUp;
+ (CGSize)getHeight:(NSAttributedString*)txtTitle andUIlabelSize:(CGRect)rect;
- (void)setupColor:(BOOL)isShow;
@end

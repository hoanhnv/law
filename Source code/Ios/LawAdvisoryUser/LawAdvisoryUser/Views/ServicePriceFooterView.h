//
//  ServiceTesesFooterView.h
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//


@interface ServicePriceFooterView : UIView

@property (weak, nonatomic) IBOutlet UIButton *btPrice;
@property (weak, nonatomic) IBOutlet UIButton *btRemoveSelection;
@property (nonatomic) id delegate;
@end
@protocol ServicePriceFooterViewDelegate <NSObject>

- (void)onPrice;
- (void)onRemoveSelection;
@end
//
//  LAListAudioTableViewCell.m
//  JustapLawyer
//
//  Created by Mac on 11/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAListAudioTableViewCell.h"
#import "LAWitnesses.h"

@implementation LAListAudioTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lstAudio registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellID"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrWitness count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    LAWitnesses *witness = [self.arrWitness objectAtIndex:indexPath.row];
    cell.textLabel.text = witness.name;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

@end

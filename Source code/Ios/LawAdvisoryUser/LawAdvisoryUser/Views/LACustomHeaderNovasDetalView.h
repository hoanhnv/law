//
//  LACustomHeaderNovasDetalView.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LACustomHeaderNovasDetalView : UIView
@property (nonatomic) IBOutlet UILabel* lbTitle;
@property (nonatomic) IBOutlet UILabel* lbNumbers;
@property (nonatomic) IBOutlet UIImageView* imgBottomLine;
@property (nonatomic) id delegate;
@end
@protocol LAHeaderNovasDetalView <NSObject>

- (void)didSelectNovasDetalHeader;

@end

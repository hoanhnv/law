//
//  LAHeaderCancelMagentoView.h
//  JustapLawyer
//
//  Created by MAC on 10/4/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>
@interface LAHeaderCancelMagentoView : UIView
@property (nonatomic,weak) IBOutlet UILabel* txtTitle;
+ (CGSize)getHeight:(NSAttributedString*)txtTitle andUIlabelSize:(CGRect)rect;
@end

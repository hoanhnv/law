//
//  DynamicTitleView.h
//  JustapLawyer
//
//  Created by Mac on 9/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewFromXib.h"
#import "AwesomeTextField.h"

@protocol DynamicTitleViewDelegate <NSObject>

@optional
-(void)limitedMessage;
-(void)didFinishInsertSubView :(NSMutableArray*)arrData;
@end
@interface DynamicTitleView : CustomViewFromXib{
    
}
@property (strong, nonatomic) NSMutableArray *arrData;

@property (weak, nonatomic) IBOutlet AwesomeTextField *txtFirst;

@property (weak, nonatomic) IBOutlet UIButton *btnAddMore;

@property (nonatomic,assign) id<DynamicTitleViewDelegate> delegate;

- (IBAction)doAddMore:(id)sender;

-(void)loadViewWithData:(NSArray*)arrTitles;

@end

//
//  LATesesHeaderView.h
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "AwesomeTextField.h"

@protocol  LATesesHeaderViewDelegate<NSObject>

-(BOOL)textFieldShouldClear:(UITextField *)textField;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
@end

@interface LATesesHeaderView : UIView<UITextFieldDelegate>{
    
}
@property (weak, nonatomic) IBOutlet AwesomeTextField *txtName;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleTeses;
@property (assign) id<LATesesHeaderViewDelegate> delegate;
@end

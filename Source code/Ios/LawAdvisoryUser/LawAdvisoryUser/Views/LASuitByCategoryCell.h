//
//  LASuitByCategoryCell.h
//  JustapLawyer
//
//  Created by Mac on 10/4/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LALawsuitObj.h"

@interface LASuitByCategoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cateName;
@property (weak, nonatomic) IBOutlet UILabel *lbContent;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lbLine;
@property (nonatomic, strong) LALawsuitObj *obj;

- (IBAction)doNextView:(id)sender;

@end

//
//  LAAssinaturaHeaderView.m
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAssinaturaHeaderView.h"


@implementation LAAssinaturaHeaderView

- (IBAction)doClose:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(doRemovePay:)])
    {
        [self.delegate doRemovePay:self.lbPlanosName.text];
    }
}

-(void)setupView:(LAAssinatureBuyObj*)obj{
    if (obj) {
        self.lbPlanosName.text = obj.plan.name;
        self.lbTotalTeses.text = [NSString stringWithFormat:@"%.0f %%",obj.plan.quantityOfTheses];
        self.lbTotalValue.text = [NSString stringWithFormat:@"Custo: R$ %@",[LAUtilitys getCurrentcyFromDecimal:obj.totalPlanOnSigning]];
        NSString *strDate = [LAUtilitys getDateStringFromTimeInterVal:obj.updatedAt withFormat:FORMAT_DD_MM_YY];
        self.lbDate.text = [NSString stringWithFormat:@"Contratado em %@",strDate];
    }
}

@end

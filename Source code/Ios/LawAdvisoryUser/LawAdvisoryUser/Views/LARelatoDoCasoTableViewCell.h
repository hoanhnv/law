//
//  LARelatoDoCasoTableViewCell.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LACustomSliderTime.h"
#import "LAMedias.h"
@protocol RelatoDelegate <NSObject>

- (void)onChooseAtIndex:(NSIndexPath*)indexPath;

@end
@interface LARelatoDoCasoTableViewCell : UITableViewCell
{
    NSTimer* timeDuration;
}
@property (nonatomic) BOOL isPlaying;
@property (nonatomic) IBOutlet UIButton* btPausePlay;
@property (nonatomic) IBOutlet LACustomSliderTime* sldTimeAudio;
@property (nonatomic) LAMedias* objAudio;
@property (nonatomic, assign) NSInteger Id;
@property (nonatomic) id delegate;
- (IBAction)onPausePlayPressed:(id)sender;
@end

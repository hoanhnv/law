//
//  LAAudioTableViewCell.m
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAudioTableViewCell.h"
#import "LAAudioPlayerController.h"
@implementation LAAudioTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)onPlay:(id)sender
{
    if([LAAudioPlayerController sharedInstance].musicIsPlaying)
    {
        [[LAAudioPlayerController sharedInstance] didTouchMusicToggleButton:nil];
        
    }
    else
    {
        [[LAAudioPlayerController sharedInstance] createSteammerWithUrl:[NSURL URLWithString:self.strUrl]];
    }

}
@end

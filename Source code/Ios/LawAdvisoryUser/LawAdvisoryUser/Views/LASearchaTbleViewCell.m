//
//  LASearchaTbleViewCell.m
//  JustapLawyer
//
//  Created by MAC on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LASearchaTbleViewCell.h"
#import "UILabel+Law.h"
@implementation LASearchaTbleViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setUpdata:(LASearchLocalObj*)data
{
    if (data)
    {
        switch (data.typeSearch) {
            case TYPE_SEARCH_DUVIDAS:
            {
                 self.txtTypeSearch.text = @"Dúvidas frequentes";
            }
                break;
            case TYPE_SEARCH_TESES:
            {
                self.txtTypeSearch.text = @"Teses";
            }
                break;
            case TYPE_SEARCH_CATEGORY:
            {
                self.txtTypeSearch.text = @"Categorias";
            }
                break;
                
            default:
            {
                self.txtTypeSearch.text = @"";
            }
                break;
        }
        self.txtTitle.text = data.title;
        if (![LAUtilitys isEmptyOrNull:data.textShown] && [LAUtilitys isEmptyOrNull:data.keywordSearch])
        {
            [UILabel setHightLightText:self.txtDescript andTextHightLight:data.keywordSearch];
        }
        if (![LAUtilitys isEmptyOrNull:data.title] && [LAUtilitys isEmptyOrNull:data.keywordSearch])
        {
            [UILabel setHightLightText:self.txtTitle andTextHightLight:data.keywordSearch];
        }
    }
}
+ (int)getSplitIndexWithString:(NSString *)str frame:(CGRect)frame andFont:(UIFont *)font
{
    int length = 1;
    int lastSpace = 1;
    NSString *cutText = [str substringToIndex:length];
    CGSize textSize = [cutText sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, frame.size.height + 500)];
    while (textSize.height <= frame.size.height)
    {
        NSRange range = NSMakeRange (length, 1);
        if ([[str substringWithRange:range] isEqualToString:@" "])
        {
            lastSpace = length;
        }
        length++;
        cutText = [str substringToIndex:length];
        textSize = [cutText sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, frame.size.height + 500)];
    }
    return lastSpace;
}
@end

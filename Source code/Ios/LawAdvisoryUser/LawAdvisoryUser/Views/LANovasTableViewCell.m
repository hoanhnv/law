//
//  LANovasTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LANovasTableViewCell.h"
#import "LAUtilitys.h"
#import "LAHappeningObj.h"

@implementation LANovasTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect rectLeft = self.btAceitar.frame;
    rectLeft.origin.x = CGRectGetMinX(self.btDetalHes.frame);
    [self.btAceitar setFrame:rectLeft];
    CGRect rectRight = self.btRecusar.frame;
    rectRight.origin.x = CGRectGetMaxX(self.btDetalHes.frame) - CGRectGetWidth(self.btRecusar.frame);
    [self.btRecusar setFrame:rectRight];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)onAceiterPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onAceiterNovasDidTouch:)])
    {
        [self.delegate onAceiterNovasDidTouch:self.currentPath];
    }
}
- (IBAction)onRecusarPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onRecusarNovasDidTouch:)])
    {
        [self.delegate onRecusarNovasDidTouch:self.currentPath];
    }
}
- (IBAction)onDetalhesPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onDetalHesNovasDidTouch:)])
    {
        [self.delegate onDetalHesNovasDidTouch:self.currentPath];
    }
}
- (void)setupData:(id)dataObj
{
    if (dataObj && [dataObj isKindOfClass:[LACustomerOpenObj class]])
    {
        self.objData = dataObj;
        self.lbStatus.text = self.objData.statusDescription;
        self.lbData.text = [LAUtilitys getDateStringFromTimeInterVal:self.objData.updatedAt withFormat:FORMAT_DD_MM_YY];
        self.lbAssunto.text = self.objData.happening.thesis.name;
        self.lbCategoria.text = self.objData.happening.thesis.category.name;
        self.lbCliente.text = self.objData.user.name;
    }
}
@end

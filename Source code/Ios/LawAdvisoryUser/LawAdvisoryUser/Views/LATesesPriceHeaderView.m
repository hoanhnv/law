//
//  LATesesHeaderView.m
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LATesesPriceHeaderView.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"
@implementation LATesesPriceHeaderView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (void)layoutSubviews
{
    [super layoutSubviews];
    [LAUtilitys setButtonTextUnderLine:self.btSelectionTeses];
    
}

- (IBAction)onConfirmTesesPressed:(id)sender
{
    [self endEditing:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(onConfirmPricePressed:)])
    {
        
        if ([LAUtilitys isEmptyOrNull:self.txtInitiaPrice.text])
        {
            [[AppDelegate sharedDelegate].window makeToast:[NSString stringWithFormat:@"%@ inválido",self.txtInitiaPrice.placeholder]
                                                  duration:3.0
                                                  position:CSToastPositionBottom
                                                     title:nil
                                                     image:nil
                                                     style:nil
                                                completion:^(BOOL didTap) {
                                                    if (didTap) {
                                                    } else {
                                                    }
                                                }];
        }
        else if ([LAUtilitys isEmptyOrNull:self.txtPriceSuccess.text])
        {
            [[AppDelegate sharedDelegate].window makeToast:[NSString stringWithFormat:@"%@ inválido",self.txtPriceSuccess.placeholder]
                                                  duration:3.0
                                                  position:CSToastPositionBottom
                                                     title:nil
                                                     image:nil
                                                     style:nil
                                                completion:^(BOOL didTap) {
                                                    if (didTap) {
                                                    } else {
                                                    }
                                                }];
        }
        else
        {
            [self.delegate onConfirmPricePressed:self];
        }
    }
}
- (IBAction)onSelectionTesesPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTesesPriceHeaderPressed)])
    {
        [self.delegate onTesesPriceHeaderPressed];
    }
}
@end

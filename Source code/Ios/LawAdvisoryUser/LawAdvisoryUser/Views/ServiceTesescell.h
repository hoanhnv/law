//
//  ServiceTesescell.h
//  JustapLawyer
//
//  Created by Tung Nguyen on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ServiceTestCellDelegate <NSObject>

- (void)doChangeSelect:(NSIndexPath*)indexPath;
- (void)doSeeMore:(NSIndexPath*)indexPath;

@end

@interface ServiceTesescell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnVerDetalhes;
@property (strong,nonatomic) NSIndexPath *indexPath;
@property (assign) id<ServiceTestCellDelegate> delegate;

- (IBAction)doSeemore:(id)sender;
- (IBAction)changeCheckbox:(id)sender;

@end

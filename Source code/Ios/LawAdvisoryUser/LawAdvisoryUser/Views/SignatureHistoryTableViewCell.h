//
//  SignatureHistoryTableViewCell.h
//  JustapLawyer
//
//  Created by Mac on 11/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAHistoryDataObj.h"

@interface SignatureHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UILabel *lbPlanName;
@property (weak, nonatomic) IBOutlet UILabel *lbDistrictname;
@property (weak, nonatomic) IBOutlet UILabel *lbValor;

-(void)setupCell:(LAHistoryDataObj*)obj;
@end

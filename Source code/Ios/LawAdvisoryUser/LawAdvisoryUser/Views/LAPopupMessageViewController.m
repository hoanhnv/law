//
//  LAPopupMessageViewController.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPopupMessageViewController.h"

@interface LAPopupMessageViewController ()

@end

@implementation LAPopupMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:.2]];
    if (![LAUtilitys isEmptyOrNull:self.content])
    {
        self.lbContent.text  = self.content;
    }
    if (![LAUtilitys isEmptyOrNull:self.titleButton])
    {
        [self.btExit setTitle:[self.titleButton uppercaseString] forState:UIControlStateNormal];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onExitPressed:(id)sender
{
    if (self.delegate && [self.delegate isKindOfClass:[LABaseViewController class]])
    {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        if ([self.delegate respondsToSelector:@selector(onFinishPopupPressed)]) {
            [self.delegate onFinishPopupPressed];
        }
    }
}
@end

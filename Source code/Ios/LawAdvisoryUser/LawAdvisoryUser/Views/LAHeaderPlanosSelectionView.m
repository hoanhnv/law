//
//  LAHeaderPlanosSelectionView.m
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAHeaderPlanosSelectionView.h"

@implementation LAHeaderPlanosSelectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (CGSize)getHeight:(NSString*)txtTitle andUIlabelSize:(CGRect)rect
{
    CGSize labelTextSize = [txtTitle boundingRectWithSize:CGSizeMake(rect.size.width - 28, MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{
                                                            NSFontAttributeName : [UIFont robotoRegular:16.0]
                                                            }
                                                  context:nil].size;
    if (labelTextSize.height > 0) {
        return CGSizeMake(rect.size.width, labelTextSize.height + 15);
    }
    else{
        return CGSizeMake(0, 0);
    }
}
+ (CGSize)getHeightText:(NSString*)txtTitle andUIlabelSize:(CGRect)rect
{
    CGSize labelTextSize = [txtTitle boundingRectWithSize:CGSizeMake(rect.size.width - 28, MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{
                                                            NSFontAttributeName : [UIFont robotoRegular:16.0]
                                                            }
                                                  context:nil].size;
    if (labelTextSize.height > 0) {
        return CGSizeMake(rect.size.width, labelTextSize.height + 15);
    }
    else{
        return CGSizeMake(0, 0);
    }
}
@end

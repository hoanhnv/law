//
//  LANovasTableViewCell.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LACustomerOpenObj.h"
@interface LANovasTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UIButton* btAceitar;
@property (nonatomic) IBOutlet UIButton* btRecusar;
@property (nonatomic) IBOutlet UIButton* btDetalHes;
@property (nonatomic) IBOutlet UILabel* lbStatus;
@property (nonatomic) IBOutlet UILabel* lbCategoria;
@property (nonatomic) IBOutlet UILabel* lbAssunto;
@property (nonatomic) IBOutlet UILabel* lbData;
@property (nonatomic) IBOutlet UILabel* lbCliente;
@property (nonatomic) IBOutlet UIImageView* imgStatus;
@property (nonatomic) IBOutlet UIImageView* imgFavorite;
@property (nonatomic) LACustomerOpenObj* objData;
@property (nonatomic) id delegate;
@property (nonatomic) NSIndexPath* currentPath;
- (void)setupData:(id)dataObj;
- (IBAction)onAceiterPresserd:(id)sender;
- (IBAction)onRecusarPresserd:(id)sender;
- (IBAction)onDetalhesPresserd:(id)sender;
@end
@protocol AcoesNovaCellDelegate <NSObject>

- (void)onAceiterNovasDidTouch:(NSIndexPath*)indexPath;
- (void)onRecusarNovasDidTouch:(NSIndexPath*)indexPath;
- (void)onDetalHesNovasDidTouch:(NSIndexPath*)indexPath;
@end
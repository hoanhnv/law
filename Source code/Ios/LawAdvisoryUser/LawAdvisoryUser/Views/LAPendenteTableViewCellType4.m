//
//  LAPendenteTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPendenteTableViewCellType4.h"

@implementation LAPendenteTableViewCellType4

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(id)dataObj
{
    
}
- (IBAction)onConfirmasMeusDadosPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onConfirmasMeusDadosDidTouch:)]) {
        [self.delegate onConfirmasMeusDadosDidTouch:self.currentPath];
    }
}
@end

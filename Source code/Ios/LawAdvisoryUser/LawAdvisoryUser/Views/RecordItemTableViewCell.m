//
//  RecordItemTableViewCell.m
//  DisplayRecorder
//
//  Created by Le Van Thang on 4/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecordItemTableViewCell.h"
#import "LAListRecordViewController.h"
@implementation RecordItemTableViewCell

@synthesize buttonPlay = _btnPlay;
@synthesize detailTextLabel2 = _detailTextLabel2;
@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _btnPlay = [UIButton buttonWithType: UIButtonTypeCustom];
        _btnPlay.frame = CGRectMake(0, 0, 26, 26);
        [_btnPlay setImage: [UIImage imageNamed: @"Start.png"] forState: UIControlStateNormal];
        [_btnPlay addTarget: self action: @selector(onPlayButton) forControlEvents: UIControlEventTouchUpInside];
        [self.contentView addSubview: _btnPlay];
        _btnPlay.hidden = NO;
        self.detailTextLabel2 = [[UILabel alloc] initWithFrame: self.detailTextLabel.frame];
        self.detailTextLabel2.textColor = [UIColor whiteColor];
        self.detailTextLabel2.backgroundColor = [UIColor clearColor];
        self.detailTextLabel2.textAlignment = NSTextAlignmentRight;
        [self setBackgroundColor:[UIColor blackColor]];
        [self.contentView addSubview: self.detailTextLabel2];
        _bIsPlaying = NO;

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
//    _btnPlay.hidden = !selected;
    if (selected)
    {
        [self setBackgroundColor:NAVIGATION_COLOR];
    }
    else
    {
        [self setBackgroundColor:[UIColor blackColor]];
    }
}

- (void) setPlayButton: (BOOL) play
{
    _bIsPlaying = play;
    
    [_btnPlay setImage: [UIImage imageNamed: _bIsPlaying ? @"Pause.png" : @"Start.png"] forState: UIControlStateNormal];
}

- (void) togglePlayButton
{
    _bIsPlaying = !_bIsPlaying;
    
    [_btnPlay setImage: [UIImage imageNamed: _bIsPlaying ? @"Pause.png" : @"Start.png"] forState: UIControlStateNormal];
}

- (void) onPlayButton
{
    [self togglePlayButton];
    if(_delegate && [_delegate respondsToSelector: @selector(onPlayButtonOfCell:)])
    {
        [_delegate onPlayButtonOfCell: self];
    }

}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    
    _btnPlay.center = CGPointMake(25, self.contentView.center.y);
    
    NSInteger width = self.textLabel.frame.size.width;
    if(width > self.contentView.frame.size.width - (self.textLabel.frame.origin.x + 35))
    {
        width -= 35;
    }
    
    
    self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x + 35, self.textLabel.frame.origin.y, width, self.textLabel.frame.size.height);
    
    if(self.detailTextLabel)
    {
        self.detailTextLabel.frame = CGRectMake(self.detailTextLabel.frame.origin.x + 35, self.detailTextLabel.frame.origin.y, self.contentView.frame.size.width - (self.textLabel.frame.origin.x + 10), self.detailTextLabel.frame.size.height);
    }
    
    self.detailTextLabel2.frame = self.detailTextLabel.frame;
    self.detailTextLabel2.font = self.detailTextLabel.font;
    
    _imvBackground.frame = CGRectMake(self.contentView.frame.origin.x - 100, self.contentView.frame.origin.y, self.contentView.frame.size.width + 200, self.contentView.frame.size.height);

    
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.detailTextLabel.backgroundColor = [UIColor clearColor];
    
}
@end

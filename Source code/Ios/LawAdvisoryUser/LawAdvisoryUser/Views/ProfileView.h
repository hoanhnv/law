//
//  ProfileView.h
//  JustapLawyer
//
//  Created by Mac on 10/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewFromXib.h"
#import <EZRatingView/EZRatingView.h>
#import "LALawyerInfo.h"

#define ARGS_SHOW_EDIT_FORM @"ShowEditForm"

@interface ProfileView:UIView

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *txtName;
@property (strong, nonatomic) IBOutlet UILabel *txtTitle;
@property (strong, nonatomic) IBOutlet UILabel *txtSite;
@property (strong, nonatomic) IBOutlet UILabel *txtAbout;
@property (strong, nonatomic) IBOutlet UILabel *txtPer;
@property (weak, nonatomic) IBOutlet UIButton *btnEditImage;
@property (strong, nonatomic) IBOutlet EZRatingView *viewRating1;
@property (strong, nonatomic) IBOutlet EZRatingView *viewRating2;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

@property (nonatomic, strong) LALawyerObj *objInfo;

- (IBAction)doEditImage:(id)sender;

- (IBAction)doEdit:(id)sender;

- (IBAction)btnEditImage:(id)sender;
- (IBAction)changeRate1:(id)sender;
- (IBAction)changeRate2:(id)sender;

-(void)loadData:(LALawyerInfo *)objInfo;

@end

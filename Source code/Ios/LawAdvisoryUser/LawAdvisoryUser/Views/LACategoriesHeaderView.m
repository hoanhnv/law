//
//  LACategoriesHeaderView.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/9/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACategoriesHeaderView.h"
#import "UIImageView+JMImageCache.h"
@implementation LACategoriesHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doSelectedCate:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}
- (IBAction)doSelectedCate:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedCate:)])
    {
        [self.delegate didSelectedCate:self.tag];
    }
}
@end

//
//  LATesesMainTableViewCell.h
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LALawSuitCategory.h"
#import "ServiceTesesSubHeader.h"
#import "LAListThese.h"
#import "ServiceTesescell.h"
#import "ServicePricecell.h"
#import "LAPlanObj.h"

#define NOTIFICATION_REMOVEALL_SELECTED @"removeAllSelection"
@class LALawsuitObj;

@protocol LATesesMainTableViewCellDelegate <NSObject>
- (void)doSeeMore:(LALawsuitObj*)suitObj;
- (void)doAddNewItem:(LALawsuitObj*)suitObj andCategories:(LALawSuitCategory*)cateObj;
- (void)doRemoveItem:(LALawsuitObj*)suitObj andCategories:(LALawSuitCategory*)cateObj;
- (void)doAddAllItem :(LALawSuitCategory*)cateObj;
- (void)doRemoveAllNewItem :(LALawSuitCategory*)cateObj;

@end

@interface LATesesMainTableViewCell : UITableViewCell<UITableViewDataSource,UITableViewDelegate,ServiceTesesDelegate,ServiceTestCellDelegate,ServicePricecellDelegate>{
}
@property (nonatomic) BOOL isSearch;
@property (weak, nonatomic) IBOutlet UITableView *tblSubList;
@property (strong, nonatomic) NSMutableArray *arrDataList;
@property (strong, nonatomic) LAListThese *listThese;
@property (nonatomic) BOOL isPriceCell;
@property (nonatomic) id<LATesesMainTableViewCellDelegate> delegate;
@property (nonatomic, strong) LALawSuitCategory* suitCateObj;

@property (strong,nonatomic) LAPlanObj* lanObj;
@property (strong, nonatomic) NSMutableArray *selectedTheseData;
@property (assign, nonatomic) NSInteger numberCanSelected;

-(void)setupCell:(LALawSuitCategory*)obj;

+(CGFloat)getHeight:(NSArray*)arrRow;
+(CGFloat)getHeightPrice:(NSArray*)arrRow;
@end

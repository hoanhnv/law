//
//  LAImageTableViewCell.h
//  JustapUser
//
//  Created by MAC on 11/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAImageTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UIImageView* imgShow;
@property (nonatomic) IBOutlet UILabel* lbName;
@end

//
//  LAPageViewControl.m
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPageViewControl.h"
#import "LAUtilitys.h"
@implementation LAPageViewControl
@synthesize activeImage,inactiveImage;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        arrImage = [NSMutableArray new];
        activeImage = [UIImage imageNamed:@"scrubActive"];
        inactiveImage = [UIImage imageNamed:@"scrubInactive"];
    }
    return self;
}
- (void)setUpImage
{
    
}

- (void)setUpImageActive:(NSString*)activeImageHere andInActive:(NSString*)inActiveImageHere
{
    if (![LAUtilitys isEmptyOrNull:activeImageHere]) {
        activeImage = [UIImage imageNamed:activeImageHere];
    }
    if (![LAUtilitys isEmptyOrNull:inActiveImageHere]) {
        inactiveImage = [UIImage imageNamed:inActiveImageHere];
    }
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect rectFrame = self.frame;
    rectFrame.size.width = self.iNumberPage*WIDTH_DOT + (self.iNumberPage - 1)* 2;
    rectFrame.size.height = HEIGHT_DOT + 4;
    [self setFrame:rectFrame];
    CGPoint pointCenter = self.center;
    pointCenter.x = [self superview].center.x;
    [self setCenter:pointCenter];
    for (int i = 0; i < self.iNumberPage; i++) {
        UIImageView * newDot = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH_DOT*i + 2*(i - 1), 2, WIDTH_DOT, WIDTH_DOT)];
        [newDot setContentMode:UIViewContentModeScaleAspectFit];
        [self addSubview:newDot];
        [arrImage addObject:newDot];
    }
    [self setCurrentPage:0];
}

-(void)setCurrentPage:(NSInteger)page
{
    
    for (int i = 0; i < arrImage.count; i ++)
    {
        UIImageView* img = [arrImage objectAtIndex:i];
        if (i == page)
        {
            [img setImage:activeImage];
        }
        else
        {
            [img setImage:inactiveImage];
        }
    }
}
@end

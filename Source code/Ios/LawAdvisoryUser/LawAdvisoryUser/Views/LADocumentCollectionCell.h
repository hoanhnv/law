//
//  LADocumentCollectionCell.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LADocumentCollectionCell : UICollectionViewCell
@property (nonatomic) IBOutlet UIImageView* imgDocThumb;
@property (nonatomic) IBOutlet UILabel* imgDocName;
@end

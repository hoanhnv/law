//
//  HeaderSectionDelalhes.m
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "HeaderSectionDelalhes.h"
#import "LAUtilitys.h"
@implementation HeaderSectionDelalhes

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)layoutSubviews{
    
}
- (IBAction)showContent:(id)sender {
    _isShowContent = !_isShowContent;
    if (_showBlock) {
        _showBlock(_isShowContent);
    }
    [self setupColor:_isShowContent];
}

- (void)setupColor:(BOOL)isShow
{
    _isShowContent = isShow;
    if (!isShow) {
        self.lblTitle.textColor = [LAUtilitys jusTapColor];
        //_lblTitle.textColor = [UIColor colorWithRed:46.0/255.0 green:138.0/255.0 blue:90.0/255.0 alpha:1.0];
        //Đổi màu image
        UIImageView *btn = self.imgDownUp;
        //btn.image = [UIImage imageNamed:@"dropdown_green"];
        btn.image = [btn.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [btn setTintColor:[LAUtilitys jusTapColor]];
        self.imgDownUp = btn;
        //[_btnDownUp setBackgroundImage:btn.image forState:UIControlStateNormal];
        self.imgBottom.backgroundColor = [LAUtilitys jusTapColor];
        
    } else {
        self.lblTitle.textColor = [UIColor grayColor];
        //[_btnDownUp setBackgroundImage:[UIImage imageNamed:@"60x60_dropup"] forState:UIControlStateNormal];
        UIImageView *btn;
        [self.imgDownUp setImage:[UIImage imageNamed:@"dropup_green"]];
        btn = self.imgDownUp;
        //btn.image = [UIImage imageNamed:@"dropup_green"];
        btn.image = [btn.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [btn setTintColor:[UIColor grayColor]];
        self.imgDownUp = btn;
        self.imgBottom.backgroundColor = [UIColor grayColor];
        
        //_imgBottom.backgroundColor = DefaultColor;
        
    }
    
}
+ (CGSize)getHeight:(NSAttributedString*)txtTitle andUIlabelSize:(CGRect)rect
{
    CGSize labelTextSize =
    [txtTitle boundingRectWithSize:CGSizeMake(rect.size.width, MAXFLOAT)
                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                           context:nil].size;
    
    if (labelTextSize.height > 0) {
        return CGSizeMake(rect.size.width, labelTextSize.height);
    }
    else{
        return CGSizeMake(0, 0);
    }
}
@end

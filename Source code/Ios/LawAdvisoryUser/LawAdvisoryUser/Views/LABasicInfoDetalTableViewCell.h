//
//  LABasicInfoDetalTableViewCell.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LACustomerOpenObj.h"
@interface LABasicInfoDetalTableViewCell : UITableViewCell
@property (nonatomic) LACustomerOpenObj* objData;
@property (nonatomic) IBOutlet UILabel* lbSolocitacao;
@property (nonatomic) IBOutlet UILabel* lbCategoria;
@property (nonatomic) IBOutlet UILabel* lbAssunto;
@property (nonatomic) IBOutlet UILabel* lbMoreInfomation;
@property (nonatomic,assign) BOOL bReset ;
- (void)setupData:(id)data;
-(void)saveTimer:(BOOL)isReset;
+(CGFloat)getHeightAttr:(NSAttributedString*)string inWith:(UITableView*)table;
@end

//
//  ServicePricecell.m
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "ServicePricecell.h"

@implementation ServicePricecell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)layoutSubviews{
    UITapGestureRecognizer *tapCheck = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeCheckbox:)];
    [self.viewCheck addGestureRecognizer:tapCheck];
    
    //    [LAUtilitys setButtonTextUnderLine:self.btnVerDetalhes];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.txtInitialPrice)
    {
        self.obj.initialPrice = proposedNewString;
    }
    if (textField == self.txtSuccessPrice)
    {
        self.obj.percentagePrice = proposedNewString;
    }
    return  YES;
}
+(CGFloat)getHeight:(NSString*)string inWith:(UITableView*)table{
    CGSize labelTextSize = [string boundingRectWithSize:CGSizeMake(table.frame.size.width, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{
                                                          NSFontAttributeName : [UIFont robotoRegular:16.0]
                                                          }
                                                context:nil].size;
    if (labelTextSize.height > 84) {
        return labelTextSize.height + (84 - 55);
    }
    else{
        return 132;
    }
}

- (IBAction)doSeemore:(id)sender {
    [self.delegate doSeeMore:self.indexPath];
}

- (IBAction)changeCheckbox:(id)sender {
    [self.btnCheck setSelected:!self.btnCheck.selected];
    [self.delegate doChangeSelect:self.indexPath];
}

@end

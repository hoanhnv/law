//
//  UploadView.h
//  JustapLawyer
//
//  Created by Mac on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewFromXib.h"

#define ARGS_UPLOAD_IMAGE    @"UploadImage"
#define ARGS_REMOVE_IMAGE    @"RemoveImage"

@protocol UploadViewDelegate <NSObject>

-(void)doUpload;

@end

@interface UploadView : CustomViewFromXib{
    
}
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (assign, nonatomic) BOOL bHasImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hContraintsImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgThumb;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (assign, nonatomic) id<UploadViewDelegate> delegate;

- (IBAction)doUpload:(id)sender;

@end

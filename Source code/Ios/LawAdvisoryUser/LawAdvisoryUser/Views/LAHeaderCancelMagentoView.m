//
//  LAHeaderCancelMagentoView.m
//  JustapLawyer
//
//  Created by MAC on 10/4/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAHeaderCancelMagentoView.h"
#import "UIFont+Law.h"
@implementation LAHeaderCancelMagentoView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)layoutSubviews{
    
    [super layoutSubviews];
    
}
+ (CGSize)getHeight:(NSAttributedString*)txtTitle andUIlabelSize:(CGRect)rect
{
    CGSize labelTextSize =
    [txtTitle boundingRectWithSize:CGSizeMake(rect.size.width, MAXFLOAT)
                           options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                           context:nil].size;
    
    if (labelTextSize.height > 0) {
        return CGSizeMake(rect.size.width, labelTextSize.height);
    }
    else{
        return CGSizeMake(0, 0);
        
        
    }
}
@end

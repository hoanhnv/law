//
//  LABasicInfoDetalTableViewCell.m
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LABasicInfoDetalTableViewCell.h"
#import "UIFont+Law.h"
@implementation LABasicInfoDetalTableViewCell
NSTimer *timer;
- (void)awakeFromNib {
    [super awakeFromNib];
    self.lbMoreInfomation.textColor = [UIColor redColor];
    //    [self.lbMoreInfomation sizeToFit];
    if ([timer isValid]) {
        [timer invalidate];
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tickTime) userInfo:nil repeats:YES];
    
    //    [timer invalidate];
    //    [NSTimer scheduledTimerWithTimeInterval:1.0
    //                                     target:self
    //                                   selector:@selector(tickTime)
    //                                   userInfo:nil
    //                                    repeats:YES];
    //    // Initialization code
    
}
-(void)layoutSubviews{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
int tick = 0;
int Hours = 0;
int Minutes = 0;
int Seconds = 0;

- (void)setupData:(id)data
{
    if (data && [data isKindOfClass:[LACustomerOpenObj class]])
    {
        
        //        timer = nil;
        self.objData = data;
        self.lbSolocitacao.text = [LAUtilitys getDateStringFromTimeInterVal:self.objData.updatedAt withFormat:FORMAT_DD_MM_YY];
        self.lbAssunto.text = self.objData.happening.thesis.name;
        self.lbCategoria.text = self.objData.happening.thesis.category.name;
        
        NSDate *currentTime;
        currentTime = [NSDate date];
        double dateValue;
        double createAt = self.objData.createdAt;
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        dateformate.timeZone =  [NSTimeZone timeZoneForSecondsFromGMT:3*3600];
        [dateformate setDateFormat:FORMAT_DD_MM_YY]; // Date formater
        NSString *dateTXT = [dateformate stringFromDate:currentTime];
        currentTime = [dateformate dateFromString:dateTXT];
        dateValue = currentTime.timeIntervalSince1970;
        int countTime = dateValue - createAt;
        //        Hours = countTime/3600;
        //        countTime = countTime % 3600;
        //        Minutes = countTime/60;
        //        countTime = countTime % 60;
        //        Seconds = countTime;
        //        NSString * str = [NSString stringWithFormat:@"Você possui %i:%i:%i para responder este pedido.Lembre-se que o tempo de resposta influencia em sua avaliação.",Hours,Minutes,Seconds];
        //        
        ////        NSAttributedString* attrStr = [LAUtilitys getAtributeTextFromString:self.objData.happening.otherInformation];
        //        NSAttributedString* attrStr = [LAUtilitys getAtributeTextFromString:str];
        //        self.lbMoreInfomation.attributedText = attrStr;
        
        // Change time start
        BOOL bValue = [[NSUserDefaults standardUserDefaults]boolForKey:@"RESETCELL"];
        if (bValue == YES) {
            NSString *strOldTime = [[LADataCenter shareInstance]getTimerValue:self.objData.dataIdentifier];
            NSArray *arrTime = [strOldTime componentsSeparatedByString:@":"];
            if ([LAUtilitys NullOrEmpty:strOldTime]) {
                Hours = 0;
                Minutes = 0;
                Seconds = 0;
            }
            else{
                Hours = [[arrTime objectAtIndex:0] intValue];
                Minutes = [[arrTime objectAtIndex:1] intValue];
                Seconds = [[arrTime objectAtIndex:2] intValue];
            }
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"RESETCELL"];
        }
        else{
        }
        
    }
}

-(void)saveTimer:(BOOL)isReset{
    if (isReset) {
        [[LADataCenter shareInstance]saveTimerValue:self.objData.dataIdentifier withValue:@"00:00:00"];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"RESETCELL"];
        
    }
    else{
        [[LADataCenter shareInstance]saveTimerValue:self.objData.dataIdentifier withValue:[NSString stringWithFormat:@"%02i:%02i:%02i",Hours,Minutes,Seconds]];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"RESETCELL"];
    }
}
-(void) tickTime{
    tick ++;
    if (Seconds <59) {
        Seconds += 1;
    }
    else if (Seconds == 59 && Minutes <59) {
        Seconds = 0;
        Minutes += 1;
    }
    else {
        if (Hours <99) {
            Hours += 1;
            Minutes = 0;
        }
        else {
            Hours = 0;
            Minutes = 0;
            Seconds = 0;
            //            if ([timer isValid]) {
            //                [timer invalidate];
            //            }
            timer = nil;
            return;
        }
    }
    NSString * str = [NSString stringWithFormat:@"Você possui %02i:%02i:%02i para responder este pedido.Lembre-se que o tempo de resposta influencia em sua avaliação.",Hours,Minutes,Seconds];
    //            NSAttributedString* attrStr = [LAUtilitys getAtributeTextFromString:self.objData.happening.otherInformation];
    //    NSAttributedString* attrStr = [LAUtilitys getAtributeTextFromString:str];
    self.lbMoreInfomation.text = str;
    
}
+(CGFloat)getHeightAttr:(NSAttributedString*)string inWith:(UITableView*)table{
    CGSize size = table.frame.size;
    size.width = CGRectGetWidth(table.frame) - 40;
    CGSize labelTextSize =
    [string boundingRectWithSize:CGSizeMake(size.width, MAXFLOAT)
                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                         context:nil].size;
    if (labelTextSize.height > 85) {
        return labelTextSize.height + 85;
    }
    else{
        return 85;
    }
}
@end

//
//  ServicePricecell.h
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AwesomeTextField.h"
#import "LALawsuitObj.h"
@protocol ServicePricecellDelegate <NSObject>

- (void)doChangeSelect:(NSIndexPath*)indexPath;
- (void)doSeeMore:(NSIndexPath*)indexPath;

@end
@interface ServicePricecell : UITableViewCell<UITextFieldDelegate>
@property (nonatomic) LALawsuitObj* obj;
@property (weak, nonatomic) IBOutlet AwesomeTextField* txtInitialPrice;
@property (weak, nonatomic) IBOutlet AwesomeTextField* txtSuccessPrice;
@property (weak, nonatomic) IBOutlet UIView *viewCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnVerDetalhes;
@property (assign,nonatomic) NSIndexPath *indexPath;
@property (assign) id<ServicePricecellDelegate> delegate;

- (IBAction)doSeemore:(id)sender;
- (IBAction)changeCheckbox:(id)sender;
@end

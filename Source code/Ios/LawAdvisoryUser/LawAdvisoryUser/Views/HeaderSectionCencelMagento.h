//
//  HeaderInfo.h
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAQuestionObj.h"
#import "LALawSuitCategory.h"
#import "LALawsuitObj.h"
#import "LASuitThesesByCateViewController.h"
#define CGREEN_LINE_CANCELAMENTO_STRING @"#288b58" 
typedef void (^ShowBlock)(BOOL);
@interface HeaderSectionCencelMagento : UIView
@property  (nonatomic) BOOL isGreeen;
@property (nonatomic,assign) double cateId;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnDownUp;
@property (nonatomic,strong) ShowBlock  showBlock;
@property (nonatomic,assign) BOOL isShowContent;
@property (nonatomic,strong) NSString *strTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgButton;
@property (nonatomic)UIColor *color;
- (IBAction)showContent:(id)sender;
- (void)setupColor:(BOOL)isShow;
-(void)setupView;
+(CGFloat)getHeight:(NSString*)title inWith:(UITableView*)table;
@end

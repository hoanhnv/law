//
//  LAWitnessTableViewCell.h
//  JustapUser
//
//  Created by MAC on 11/23/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAWitnessTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel* lbName;
@end

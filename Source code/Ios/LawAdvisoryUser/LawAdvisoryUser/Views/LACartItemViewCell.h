//
//  LACartItemViewCell.h
//  JustapLawyer
//
//  Created by Mac on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LASignatureObj.h"
#import "LADistrictObj.h"
#import "LAPlanObj.h"

@interface LACartItemViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbDistrictName;
@property (weak, nonatomic) IBOutlet UILabel *lbTeses;
@property (weak, nonatomic) IBOutlet UILabel *lbValor;

-(void)setupCell:(LASignatureObj *)signature andDistrict:(LADistrictObj*)district;
-(void)setupCell:(LASignatureObj *)signature andIndexPath:(NSIndexPath*)indexPath andLan:(LAPlanObj*)planObj;
@end

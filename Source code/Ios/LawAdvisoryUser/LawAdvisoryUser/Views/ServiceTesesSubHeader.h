//
//  ServiceTesesSubHeader.h
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ServiceTesesDelegate <NSObject>

- (void)doCheckAll;
- (void)doUnCheckAll;

@end
@interface ServiceTesesSubHeader : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnAtivar;
@property (weak, nonatomic) IBOutlet UIButton *btnDesativar;

@property (nonatomic, assign) id<ServiceTesesDelegate> delegate;

- (IBAction)doSelectAll:(id)sender;
- (IBAction)doUncheckAll:(id)sender;

@end

//
//  LAdetalhesFooterView.h
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AwesomeTextField.h"
@protocol DetalHesFooterDelegate <NSObject>

- (void)onSendmessage:(NSString*)Message;
- (void)onFinishAction;
-(void)onWriteMessage;
-(void)viewMessage;
@end
@interface LAdetalhesFooterView : UIView
@property (weak, nonatomic) IBOutlet UITableView *tbvMessage;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (nonatomic) IBOutlet AwesomeTextField* txtMessage;
@property (nonatomic) id <DetalHesFooterDelegate> delegate;
@property (nonatomic) IBOutlet UIButton* btFinish;
@property (weak, nonatomic) IBOutlet UIButton *btEnviar;
@property (nonatomic, strong) NSMutableArray *arrData;
@property (nonatomic, strong) NSMutableArray *arrShow;
@property (nonatomic, assign) BOOL isFirstLoad;
- (void)reloadDataFooter;
@end

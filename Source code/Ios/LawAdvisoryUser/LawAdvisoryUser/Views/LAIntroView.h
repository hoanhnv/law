//
//  LAIntroView.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#define KEY_IMAGE @"KEY_IMAGE"
#define KEY_TITLE @"KEY_TITLE"
#define KEY_DESCRIPTION @"KEY_DESCRIPTION"
@interface LAIntroView : UIView
@property (nonatomic) IBOutlet UIImageView* imgShow;
@property (nonatomic) IBOutlet UILabel*     lbTitle;
@property (nonatomic) IBOutlet UILabel*     lbDescription;
- (void)setupDataWith:(NSDictionary*)dictData;
@end

//
//  HeaderInfo.h
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAQuestionObj.h"
typedef void (^ShowBlock)(BOOL);
@interface HeaderInfo : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnDownUp;
@property (nonatomic,strong) ShowBlock  showBlock;
@property (nonatomic,assign) BOOL isShowContent;
@property (nonatomic,strong) LAQuestionObj *questionInfo;

- (IBAction)showContent:(id)sender;
- (void)setupColor:(BOOL)isShow;
-(void)setupView;
+(CGFloat)getHeight:(LAQuestionObj*)obj inWith:(UITableView*)table;
@end

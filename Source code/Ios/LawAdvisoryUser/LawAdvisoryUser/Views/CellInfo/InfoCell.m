//
//  InfoCell.m
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "InfoCell.h"
#import "UIFont+Law.h"
@implementation InfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)layoutSubviews{
//    [self.lbContent sizeToFit];
//    CGRect fram = self.contentView.frame;
//    fram.size.height = CGRectGetHeight(self.lbContent.frame)+ 17;
//    self.contentView.frame = fram;
//    
//    CGRect newCellSubViewsFrame = CGRectMake(0, 0, self.frame.size.width, fram.size.height);
//    CGRect newCellViewFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fram.size.height);
//    
//    self.contentView.frame = self.contentView.bounds = self.backgroundView.frame = self.accessoryView.frame = newCellSubViewsFrame;
//    self.frame = newCellViewFrame;
    
    [super layoutSubviews];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
+(CGFloat)getHeight:(NSString*)string inWith:(UITableView*)table{
    CGSize labelTextSize = [string boundingRectWithSize:CGSizeMake(table.frame.size.width - 40, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{
                                                          NSFontAttributeName : [UIFont robotoRegular:14.0]
                                                          }
                                                context:nil].size;
    if (labelTextSize.height > 29) {
        return labelTextSize.height + (84 - 55);
    }
    else{
        return 29;
    }
}

+(CGFloat)getHeightAttr:(NSAttributedString*)string inWith:(UITableView*)table{
    CGSize labelTextSize =
    [string boundingRectWithSize:CGSizeMake(table.frame.size.width - 40, MAXFLOAT)
                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                         context:nil].size;
    
    if (labelTextSize.height > 0) {
        return labelTextSize.height + (79 - 55);
    }
    else{
        return 0;
    }
}

@end

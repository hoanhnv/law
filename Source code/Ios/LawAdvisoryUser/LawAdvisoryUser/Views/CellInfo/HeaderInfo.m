//
//  HeaderInfo.m
//  LawAdvisoryUser
//
//  Created by Dao Minh Nha on 9/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "HeaderInfo.h"
#import "UIColor+Law.h"
@implementation HeaderInfo
#define DefaultColor [UIColor colorWithRed:75.0/255.0 green:187.0/255.0 blue:253.0/255.0 alpha:1.0]

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        UIView * custom = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        custom.frame = self.bounds;
        // 2. Add as a subview
        UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showContent:)];
        [self addGestureRecognizer:tapView];
        [self addSubview:custom];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        // Call a common method to setup gesture and state of UIView
        NSString *className = NSStringFromClass([self class]);
        UIView * custom = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        custom.frame = self.bounds;
        // 2. Add as a subview
        UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showContent:)];
        [self addGestureRecognizer:tapView];
        
        [self addSubview:custom];
        [self setupColor:_isShowContent];
    }
    return self;
}
-(void)setupView{
    if (self.questionInfo) {
        self.lblTitle.text = [self.questionInfo.question uppercaseString];
        [self.lblTitle sizeToFit];
        if (CGRectGetMaxY(self.lblTitle.frame) > CGRectGetMaxY(self.btnDownUp.frame)) {
            self.imgBottom.frame = CGRectMake(self.imgBottom.frame.origin.x, CGRectGetMaxY(self.lblTitle.frame) + 8, self.imgBottom.frame.size.width, self.imgBottom.frame.size.height);
        }
    }
    //    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(self.imgBottom.frame) + 10);
}
- (IBAction)showContent:(id)sender {
    if (!_isShowContent) {
        _isShowContent = YES;
    } else {
        _isShowContent = NO;
    }
    if (_showBlock) {
        _showBlock(_isShowContent);
    }
    [self setupColor:_isShowContent];
    [self setupView];
}
- (void)setupColor:(BOOL)isShow
{
    _isShowContent = isShow;
    if (!isShow) {
        _lblTitle.textColor = [UIColor grayColor];
            [_btnDownUp setBackgroundImage:[UIImage imageNamed:@"60x60_dropup"] forState:UIControlStateNormal];
            _imgBottom.backgroundColor = [UIColor grayColor];
        
    } else {
        _lblTitle.textColor = DefaultColor;
            [_btnDownUp setBackgroundImage:[UIImage imageNamed:@"60x60_dropdown"] forState:UIControlStateNormal];
            _imgBottom.backgroundColor = DefaultColor;
        
    }
    
}
+(CGFloat)getHeight:(LAQuestionObj*)obj inWith:(UITableView*)table{
    CGSize labelTextSize = [[obj.question uppercaseString] boundingRectWithSize:CGSizeMake(table.frame.size.width, MAXFLOAT)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{
                                                                                  NSFontAttributeName : [UIFont systemFontOfSize:17]
                                                                                  }
                                                                        context:nil].size;
    if (labelTextSize.height > 33) {
        return labelTextSize.height + 30;
    }
    else{
        return 48;
    }
    
}
@end

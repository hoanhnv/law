//
//  DynamicTitleView.m
//  JustapLawyer
//
//  Created by Mac on 9/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "DynamicTitleView.h"
#import "LATitles.h"

#define MAX_FILED 3
#define kTEXTFIELD_PADDING  20
#define kPLACE_HOLDER   @"Título"

@implementation DynamicTitleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self setupView];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
}

-(void)setupView{
    self.arrData = [NSMutableArray arrayWithObjects:self.txtFirst, nil];
}

- (IBAction)doAddMore:(id)sender {
    if ([self.arrData count] >= MAX_FILED) {
        // TODO
        [self.btnAddMore setHidden:YES];
    }
    else{
        AwesomeTextField *txtLast = [self.arrData lastObject];
        CGRect frame = txtLast.frame;
        frame.origin.y = CGRectGetMaxY(frame) + kTEXTFIELD_PADDING;
        AwesomeTextField *txtNew = [[AwesomeTextField alloc]initWithFrame:frame];
        txtNew.underlineColor = txtLast.underlineColor;
        txtNew.font = txtLast.font;
        txtNew.placeholder = kPLACE_HOLDER;
        [self addSubview:txtNew];
        self.btnAddMore.frame = CGRectMake(self.btnAddMore.frame.origin.x, CGRectGetMaxY(txtNew.frame) + kTEXTFIELD_PADDING, self.btnAddMore.frame.size.width,self.btnAddMore.frame.size.height);
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,CGRectGetMaxY(self.btnAddMore.frame) + kTEXTFIELD_PADDING);
        
        [self.arrData addObject:txtNew];
        if ([self.arrData count] >= MAX_FILED) {
            // TODO
            [self.btnAddMore setHidden:YES];
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,CGRectGetMaxY(self.btnAddMore.frame));
        }
    }
    [self.delegate didFinishInsertSubView:self.arrData];
}

-(void)loadViewWithData:(NSArray*)arrTitles{
    for (int i = 0; i < [arrTitles count]; i++) {
        LATitles *titleObj = [arrTitles objectAtIndex:i];
        if (i == 0) {
            AwesomeTextField *txtLast = [self.arrData lastObject];
            txtLast.text = titleObj.title;
        }
        else{
            AwesomeTextField *txtLast = [self.arrData lastObject];
            CGRect frame = txtLast.frame;
            frame.origin.y = CGRectGetMaxY(frame) + kTEXTFIELD_PADDING;
            AwesomeTextField *txtNew = [[AwesomeTextField alloc]initWithFrame:frame];
            txtNew.underlineColor = txtLast.underlineColor;
            txtNew.font = txtLast.font;
            txtNew.text = titleObj.title;
            txtNew.placeholder = kPLACE_HOLDER;
            [self addSubview:txtNew];
            self.btnAddMore.frame = CGRectMake(self.btnAddMore.frame.origin.x, CGRectGetMaxY(txtNew.frame) + kTEXTFIELD_PADDING, self.btnAddMore.frame.size.width,self.btnAddMore.frame.size.height);
            
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,CGRectGetMaxY(self.btnAddMore.frame) + kTEXTFIELD_PADDING);
            
            [self.arrData addObject:txtNew];
            if ([self.arrData count] >= MAX_FILED) {
                // TODO
                [self.btnAddMore setHidden:YES];
                self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,CGRectGetMaxY(self.btnAddMore.frame));
            }
        }
    }
}

@end

//
//  ProfileView.m
//  JustapLawyer
//
//  Created by Mac on 10/11/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "ProfileView.h"
#import <EZRatingView/EZRatingView.h>
#import "LALawyerInfo.h"
#import "LALawyerObj.h"
#import "LALawyer.h"
#import "LATitles.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ProfileView

NSMutableArray *_ratingNumbers;
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupView];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self setupView];
}
-(void)setupView{
    
    _ratingNumbers = [NSMutableArray array];
    for (NSUInteger index = 0; index < 10000; index++) {
        [_ratingNumbers addObject:@(rand() % 6)];
    }
    [self.viewRating1 setStepInterval:1.0];
    self.viewRating1.userInteractionEnabled = YES;
    [self.viewRating1 sizeToFit];
    
    self.viewRating2.userInteractionEnabled = YES;
    [self.viewRating2 sizeToFit];
    
}
- (IBAction)changeRate1:(EZRatingView *)sender
{
    // Save rating, because the cell will be reused.
//    EZRatingView *ezView = (EZRatingView *)sender;
//    _ratingNumbers[ezView.tag] = @(ezView.value);
}

- (IBAction)changeRate2:(id)sender {
//    EZRatingView *ezView = (EZRatingView *)sender;
//    _ratingNumbers[ezView.tag] = @(ezView.value);
}

- (IBAction)doEditImage:(id)sender {
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowEditForm" object:nil];
}

- (IBAction)doEdit:(id)sender {
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowEditForm" object:nil];
}

-(void)loadData:(LALawyerInfo *)objInfo{
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:objInfo.data.lawyer.imageAvatarWithUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    self.txtName.text = objInfo.data.name;
    NSString *strTitle;
    UILabel *lbNew;
    UILabel *lbNew2;
    for (int i=0;i<[objInfo.data.titles count];i++) {
        LATitles* titleObj = objInfo.data.titles[i];
        if (i ==0) {
            self.txtTitle.text = titleObj.title;
        }
        else if(i==1){
            lbNew = [[UILabel alloc]initWithFrame:self.txtTitle.frame];
            lbNew.frame = CGRectMake(lbNew.frame.origin.x, CGRectGetMaxY(self.txtTitle.frame),lbNew.frame.size.width ,lbNew.frame.size.height);
            lbNew.text = titleObj.title;
            lbNew.textColor = self.txtTitle.textColor;
            lbNew.font = self.txtTitle.font;
            [self addSubview:lbNew];
        }
        else if(i==2){
            lbNew2 = [[UILabel alloc]initWithFrame:self.txtTitle.frame];
            lbNew2.frame = CGRectMake(lbNew2.frame.origin.x, CGRectGetMaxY(lbNew.frame),lbNew2.frame.size.width ,lbNew2.frame.size.height);
            lbNew2.text = titleObj.title;
            lbNew2.textColor = self.txtTitle.textColor;
            lbNew2.font = self.txtTitle.font;
            [self addSubview:lbNew2];
        }
    }
    self.viewRating1.enabled = false;
    self.viewRating2.enabled = false;
    self.viewRating1.value = objInfo.data.lawyer.response_time;
    self.viewRating2.value = objInfo.data.lawyer.rating;
    //[self.txtTitle sizeToFit];
    self.txtSite.text = objInfo.data.lawyer.site;
    
    if ([objInfo.data.titles count] == 2) {
        self.txtSite.frame = CGRectMake(self.txtSite.frame.origin.x,CGRectGetMaxY(lbNew.frame) ,self.txtSite.frame.size.width , self.txtSite.frame.size.height);
    }
    else if([objInfo.data.titles count] == 3){
        self.txtSite.frame = CGRectMake(self.txtSite.frame.origin.x,CGRectGetMaxY(lbNew2.frame) ,self.txtSite.frame.size.width , self.txtSite.frame.size.height);
    }
    else{
        self.txtSite.frame = CGRectMake(self.txtSite.frame.origin.x,CGRectGetMaxY(self.txtTitle.frame) ,self.txtSite.frame.size.width , self.txtSite.frame.size.height);
    }
    
    UIView *vHeader = (UIView*)[self viewWithTag:121];
    vHeader.frame = CGRectMake(0,vHeader.frame.origin.y ,vHeader.frame.size.width , CGRectGetMaxY(self.txtSite.frame) + 24);
    
    self.txtAbout.text = objInfo.data.lawyer.about;
    [self.txtAbout sizeToFit];
//    UIView *vRate = (UIView*)[self viewWithTag:123];
    UIView *vBody = (UIView*)[self viewWithTag:122];
    
    self.btnEdit.frame = CGRectMake(self.btnEdit.frame.origin.x, CGRectGetMaxY(self.viewRating2.frame) + 30 , self.btnEdit.frame.size.width, self.btnEdit.frame.size.height);
    vBody.frame = CGRectMake(0,CGRectGetMaxY(vHeader.frame) ,vBody.frame.size.width , CGRectGetMaxY(self.btnEdit.frame) + 24);
}
@end

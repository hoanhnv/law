//
//  LAHistoryTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAHistoryTableViewCell.h"
#import "LACustomerOpenObj.h"
#import "LAHappeningObj.h"

@implementation LAHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)onVerdetalhesPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onVerDetalhesHistoryDidTouch:)])
    {
        [self.delegate onVerDetalhesHistoryDidTouch:self.currentPath];
    }
}
- (void)setupData:(id)dataObj
{
    if (dataObj && [dataObj isKindOfClass:[LACustomerOpenObj class]])
    {
        self.objData = dataObj;
        self.lbStatus.text = self.objData.statusDescription;
        self.lbInicio.text = [LAUtilitys getDateStringFromTimeInterVal:self.objData.createdAt withFormat:FORMAT_DD_MM_YY];
        self.lbTermino.text = [LAUtilitys getDateStringFromTimeInterVal:self.objData.updatedAt withFormat:FORMAT_DD_MM_YY];
        
        self.lbAssunto.text = self.objData.happening.thesis.name;
        self.lbCategoria.text = self.objData.happening.thesis.category.name;
        self.lbCliente.text = self.objData.user.name;
        self.ratingView.value = self.objData.rating;
    }
}

@end

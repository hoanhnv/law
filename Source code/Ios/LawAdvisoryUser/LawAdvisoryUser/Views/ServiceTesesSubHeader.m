//
//  ServiceTesesSubHeader.m
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "ServiceTesesSubHeader.h"

@implementation ServiceTesesSubHeader

-(void)layoutSubviews{
    [LAUtilitys setButtonTextUnderLine:self.btnAtivar];
    [LAUtilitys setButtonTextUnderLine:self.btnDesativar];
}

- (IBAction)doSelectAll:(id)sender {
    if (self.delegate) {
        [self.delegate doCheckAll];
    }
}

- (IBAction)doUncheckAll:(id)sender {
    if (self.delegate) {
        [self.delegate doUnCheckAll];
    }
}
@end

//
//  LASlideViewCategory.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LASlideViewCategory : UIView
@property (nonatomic) IBOutlet UIImageView* imgCover;
@property (nonatomic) IBOutlet UILabel* txtTitle;
@end

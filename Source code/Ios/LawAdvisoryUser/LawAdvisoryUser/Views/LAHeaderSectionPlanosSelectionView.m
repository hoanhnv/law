//
//  LACustomHeaderNovasDetalView.m
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAHeaderSectionPlanosSelectionView.h"

@implementation LAHeaderSectionPlanosSelectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.imgBottomLine setBackgroundColor:[LAUtilitys jusTapColor]];
}
+ (CGSize)getHeight:(NSString*)txtTitle andUIlabelSize:(CGRect)rect
{
    CGSize labelTextSize = [txtTitle boundingRectWithSize:CGSizeMake(rect.size.width - 28, MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{
                                                            NSFontAttributeName : [UIFont robotoRegular:17.0]
                                                            }
                                                  context:nil].size;
    if (labelTextSize.height > 0) {
        return CGSizeMake(rect.size.width, labelTextSize.height + 20);
    }
    else{
        return CGSizeMake(0, 0);
    }
}
@end

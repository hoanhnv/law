//
//  LAPlanosSelectionTableViewCell.h
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAPlanObj.h"


@protocol LAPlanosSectionCellDelegate <NSObject>

- (void)onSelectionAtIndex:(NSInteger)index;

@end


@interface LAPlanosSelectionTableViewCell : UITableViewCell
@property (nonatomic) NSIndexPath* indexPath;
@property (nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lbComarca;
@property (weak, nonatomic) IBOutlet UILabel *lbValue;
@property (weak, nonatomic) IBOutlet UILabel *lbName;

-(void)setupCell:(LAPlanObj*)planObj withComacar:(NSString*)strCom;

@end


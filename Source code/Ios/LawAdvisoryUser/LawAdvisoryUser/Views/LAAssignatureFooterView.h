//
//  LAAssignatureFooterView.h
//  JustapLawyer
//
//  Created by Mac on 11/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAAssignatureFooterView : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnSelectionTeses;

@end

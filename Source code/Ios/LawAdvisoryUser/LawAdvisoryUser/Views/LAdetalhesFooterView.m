//
//  LAdetalhesFooterView.m
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAdetalhesFooterView.h"
#import "FooterTableViewCell.h"
#import "MainService.h"
#import "LAMessageObj.h"
#import "LAActionResponseInfo.h"
#import "LAUtilitys.h"

@interface LAdetalhesFooterView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation LAdetalhesFooterView
NSMutableArray *checkShow;
//LAMessageObj *mes;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self reloadDataFooter];
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
//    self.tbvMessage.rowHeight = UITableViewAutomaticDimension;
//    self.tbvMessage.estimatedRowHeight = 44;
    //    self.txtMessage.enabled = NO;
    //    [self loadData];
    checkShow = [NSMutableArray new];
    //    for (int i = 0; i<_arrData.count; i++) {
    //        <#statements#>
    //    }
    [self.tbvMessage registerNib:[UINib nibWithNibName:@"FooterTableViewCell" bundle:nil] forCellReuseIdentifier:@"FooterTableViewCell"];
    self.btFinish.layer.borderColor = self.btFinish.currentTitleColor.CGColor;
    self.btFinish.layer.borderWidth = 2.0;
//    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                 action:@selector(onMessageView:)];
//    UITapGestureRecognizer *tapTitle = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                               action:@selector(viewAllMessage:)];
//    [self.lbTitle setUserInteractionEnabled:YES];
//    [self.lbTitle addGestureRecognizer:tapTitle];
//    [self.txtMessage addGestureRecognizer:tapgesture];
    
}
- (void)reloadDataFooter
{
    
    if (self.arrShow == nil) {
        self.arrShow = [NSMutableArray new];
    }
    if (self.arrShow) {
        [self.arrShow removeAllObjects];
    }
    if (self.arrData.count) {
        for (int i = 0; i < self.arrData.count; i ++) {
            [self.arrShow addObject:[NSNumber numberWithInt:0]];
        }
    }
}
//-(void)loadData{
//    [MainService getMessage:self.Id withBlock:^(NSInteger errorCode, NSString *message, id data){
//        LAActionResponseInfo *respose = [[LAActionResponseInfo alloc]initWithDictionary:data];
//        if(respose.success){
//            NSMutableArray *arrMes = [[data objectForKey:@"data"] objectForKey:@"messages"];
//            for (NSDictionary *dict in arrMes) {
//                mes = [[LAMessageObj alloc] initWithDictionary:dict];
//                [_arrData addObject:mes];
//            }
//
//        }
//    }];
//}
- (IBAction)onMessagePressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onWriteMessage)])
    {
        [self.delegate onWriteMessage];
    }
}
- (IBAction)onFinish:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onFinishAction)])
    {
        [self.delegate onFinishAction];
    }
}
-(void)viewAllMessage:(UITapGestureRecognizer*)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(viewMessage)])
    {
        [self.delegate viewMessage];
    }
}
//- (void)onMessageView:(UITapGestureRecognizer*)sender{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(onWriteMessage)])
//    {
//        [self.delegate onWriteMessage];
//    }
//}
//static CGFloat padding = 20.0;
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FooterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FooterTableViewCell" forIndexPath:indexPath];
    //    cell.userInteractionEnabled = false;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.enabled = false;
    [cell updateConstraintsIfNeeded];
    //    cell.lbContentSms.text = _arrMes[indexPath.row];
    UIImage *img = [UIImage new];
    LAMessageObj *mess = self.arrData[indexPath.row];
    cell.lbContentSms.text = mess.body;
    //"mess.body;
    NSString *dateFormat = @"dd/MM/yyy";
//    NSString *dateFormat2 = @"dd/MM/yyyy";
    cell.lbDate.text = [LAUtilitys getDateStringFromTimeInterVal:mess.createdAt withFormat:dateFormat];
    cell.lbHour.text = [LAUtilitys getDateStringFromTimeInterVal:mess.createdAt withFormat:@"HH:mm"];
    cell.lbName.text = mess.user.name;
    if(mess.user) {
        img = [[UIImage imageNamed:@"bg_sms_content_send"] stretchableImageWithLeftCapWidth:24  topCapHeight:40];
        //        img.fram = cell.vAll.frame;
        cell.imgBgr.image = img;
    }
    else {
        img = [[UIImage imageNamed:@"bg_sms_content_receive"] stretchableImageWithLeftCapWidth:24  topCapHeight:40];
        //        img = cell.vAll.frame;
        cell.imgBgr.image=img;
    }
    img = [[UIImage imageNamed:@"bg_sms_content_send"] stretchableImageWithLeftCapWidth:24  topCapHeight:40];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.arrData.count;
    return self.arrData.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row < self.arrShow.count)
    {
        NSInteger i = 1 - labs([self.arrShow[indexPath.row] integerValue]);
        [self.arrShow replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:i]];
        [self.tbvMessage reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeFooterView" object:nil];
    }
    
}
//-(void)detailMessage:(NSInteger)row{
//    bool show = [checkShow objectAtIndex:row];
//
//}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row < self.arrShow.count)
    {
        if ([[self.arrShow objectAtIndex:indexPath.row] intValue] == 1)
        {
            LAMessageObj *mess = self.arrData[indexPath.row];
            return [FooterTableViewCell getHeight:mess.body inWith:tableView];
        }
        else
        {
            LAMessageObj *mess = self.arrData[indexPath.row];
            CGFloat height =  [FooterTableViewCell getHeight:mess.body inWith:tableView];
            return MIN(120, height);
        }
    }
    else
    {
        LAMessageObj *mess = self.arrData[indexPath.row];
        CGFloat height =  [FooterTableViewCell getHeight:mess.body inWith:tableView];
        return MIN(120, height);
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isFirstLoad) {
        if([indexPath isEqual:((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject])]){
            [self.tbvMessage scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.arrData count] -1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            self.isFirstLoad = NO;
        }
    }
}

@end

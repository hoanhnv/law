//
//  LAAssinaturaHeaderView.h
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAAssinatureBuyObj.h"
#import "LAPlanObj.h"

@protocol LAAssinaturaHeaderViewDelegate<NSObject>
-(void)doRemovePay:(id)obj;
@end

@interface LAAssinaturaHeaderView  : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnHistory;

@property (weak, nonatomic) IBOutlet UILabel *lbPlanosName;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalTeses;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeCard;

@property (assign) id<LAAssinaturaHeaderViewDelegate> delegate;

- (IBAction)doClose:(id)sender;

-(void)setupView:(LAAssinatureBuyObj*)obj;

@end

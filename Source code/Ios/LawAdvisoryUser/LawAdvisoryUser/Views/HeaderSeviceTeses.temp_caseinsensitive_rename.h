//
//  headerSeviceTeses.h
//  JustapLawyer
//
//  Created by Tung Nguyen on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAQuestionObj.h"
typedef void (^ShowBlock)(BOOL);
@interface HeaderSeviceTeses : UIView
@property (nonatomic,strong) ShowBlock  showBlock;
@property (nonatomic,assign) BOOL isShowContent;
@property (nonatomic,strong) LAQuestionObj *questionInfo;
@property (strong, nonatomic) IBOutlet UIImageView *imgLine;
@property (strong, nonatomic) IBOutlet UILabel *txtTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnDowUp;
- (IBAction)showContent:(id)sender;
- (void)setupColor:(BOOL)isShow;
-(void)setupView;
+(CGFloat)getHeight:(LAQuestionObj*)obj inWith:(UITableView*)table;
@end



//
//  UploadView.m
//  JustapLawyer
//
//  Created by Mac on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "UploadView.h"

@implementation UploadView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupView];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self setupView];
}

-(void)setupView{
    UIImage *image = [[UIImage imageNamed:@"ic_close_cancel"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.btnClose setImage:image forState:UIControlStateNormal];
    self.btnClose.tintColor = [UIColor redColor];
    
    self.lbName.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.5];
    CGRect frame = self.frame;
    if (self.bHasImage) {
        self.hContraintsImage.constant = 120;
        frame.size.height = 185;
        self.btnClose.hidden = NO;
        self.lbName.hidden = NO;
    }
    else{
        self.hContraintsImage.constant = 0;
        frame.size.height = 64;
        self.btnClose.hidden = YES;
        self.lbName.hidden = YES;
    }
    self.frame = frame;
    [self setNeedsUpdateConstraints];
    [self layoutIfNeeded];
}

- (IBAction)doRemoveImage:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:ARGS_REMOVE_IMAGE object:[NSNumber numberWithInteger:self.tag]];
}

- (IBAction)doUpload:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:ARGS_UPLOAD_IMAGE object:[NSNumber numberWithInteger:self.tag]];
}

@end

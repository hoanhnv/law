//
//  SignatureHistoryTableViewCell.m
//  JustapLawyer
//
//  Created by Mac on 11/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "SignatureHistoryTableViewCell.h"
#import "LAAssinatureObj.h"
#import "LADistricts.h"
#import "LAPlanObj.h"
#import "LASignatureDistrict.h"

@implementation SignatureHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setupCell:(LAHistoryDataObj *)obj{
    self.lbDate.text = [LAUtilitys getDateStringFromTimeInterVal:obj.updatedAt withFormat:FORMAT_DD_MM_YY];
    self.lbValor.text = [NSString stringWithFormat:@"Valor: R$ %@",[LAUtilitys getCurrentcyFromDecimal:obj.signature.valuePlanOnSigning]];
    self.lbPlanName.text = obj.signature.plan.name;
    LADistricts *districts = [obj.districts objectAtIndex:0];
    self.lbDistrictname.text = districts.district.name;
}

@end

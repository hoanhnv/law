//
//  LAPopupListView.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/15/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RZCellSizeManager.h"
#import "RZPopUplistTableViewCell.h"
@interface LAPopupListView : UIView <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) IBOutlet UITableView* tbViewList;
@property (nonatomic) NSMutableArray* arrTitle;
@property (strong, nonatomic) RZCellSizeManager *sizeManager;
@end

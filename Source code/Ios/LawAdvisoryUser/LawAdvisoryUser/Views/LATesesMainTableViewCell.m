//
//  LATesesMainTableViewCell.m
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LATesesMainTableViewCell.h"
#import "ServiceTesescell.h"
#import "ServiceTesesSubHeader.h"
#import "UIView+NibInitializer.h"
#import "LALawSuitCategory.h"
#import "MainService.h"
#import "LALawsuitObj.h"
#import "LAListThese.h"
#import "LACancelamentViewController.h"
#import "AppDelegate.h"

@implementation LATesesMainTableViewCell
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doUnCheckAll) name:NOTIFICATION_REMOVEALL_SELECTED object:nil];
    self.isPriceCell = NO;
    self.isSearch = NO;
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if (!self.isPriceCell)
    {
        ServiceTesesSubHeader *header = [[ServiceTesesSubHeader alloc]initWithNibNamed:NSStringFromClass([ServiceTesesSubHeader class])];
        CGRect frameHeader = header.frame;
        frameHeader.size.height = 50.0;
        [header setFrame:frameHeader];
        header.delegate = self;
        self.tblSubList.tableHeaderView = header;
    }
    [self.tblSubList registerNib:[UINib nibWithNibName:NSStringFromClass([ServicePricecell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ServicePricecell class])];
    
    if (self.isPriceCell)
    {
        [self.tblSubList registerNib:[UINib nibWithNibName:NSStringFromClass([ServicePricecell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ServicePricecell class])];
    }
    else
    {
        [self.tblSubList registerNib:[UINib nibWithNibName:NSStringFromClass([ServiceTesescell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ServiceTesescell class])];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setupCell:(LALawSuitCategory*)obj{
    self.suitCateObj = obj;
    self.arrDataList = [NSMutableArray array];
    self.arrDataList = obj.arrData;
    [self.tblSubList reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrDataList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isPriceCell)
    {
        return 132;
    }
    else
    {
        return 56;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isPriceCell)
    {
        ServicePricecell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ServicePricecell class]) forIndexPath:indexPath];
        cell.indexPath = indexPath;
        cell.delegate = self;
        LALawsuitObj *obj = [self.arrDataList objectAtIndex:indexPath.row];
        cell.obj = obj;
        cell.lbTitle.text = obj.legalName;
        cell.btnCheck.selected = obj.isSelected;
        cell.txtInitialPrice.text = obj.initialPrice;
        cell.txtSuccessPrice.text = obj.percentagePrice;
        return  cell;
    }
    else
    {
        ServiceTesescell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ServiceTesescell class])];
        cell.indexPath = indexPath;
        cell.delegate = self;
        LALawsuitObj *obj = [self.arrDataList objectAtIndex:indexPath.row];
        cell.lbTitle.text = obj.legalName;
        cell.btnCheck.selected = obj.isSelected;
        return  cell;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
+(CGFloat)getHeight:(NSArray*)arrRow{
    return [arrRow count]*50 + 50;
}
+(CGFloat)getHeightPrice:(NSArray*)arrRow
{
    return [arrRow count]*132 + 50;
}
-(void)reloadData:(LALawSuitCategory*)obj{
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    [MainService fetchLawsuitTheseByCategory:obj.categoryIdentifier withBlock:^(NSInteger errorCode, NSString *message, id data) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        if (data && [data isKindOfClass:[NSDictionary class]])
        {
            self.listThese = [[LAListThese alloc]initWithDictionary:data];
            if (self.listThese.success)
            {
                self.arrDataList = [NSMutableArray arrayWithArray:self.listThese.data];
                [self.tblSubList reloadData];
            }
            else{
            }
        }
    }];
}

-(void)doCheckAll{
    if ([self getCountSelectedThese] >= self.numberCanSelected && !self.isPriceCell) {
        // Show message
        [[NSNotificationCenter defaultCenter]postNotificationName:@"ShowMessageOutRangeThese" object:nil];
    }
    else if([self getCountSelectedThese] +  [self.suitCateObj.arrData count] > self.numberCanSelected && !self.isPriceCell){
        [[NSNotificationCenter defaultCenter]postNotificationName:@"ShowMessageOutRangeThese" object:nil];
    }
    else{
        self.suitCateObj.isSelected = YES;
        for (LALawsuitObj *item in self.suitCateObj.arrData) {
            item.isSelected = YES;
        }
        [self.tblSubList reloadData];
        [self.delegate doAddAllItem:self.suitCateObj];
    }
    
}

-(void)doUnCheckAll{
    self.suitCateObj.isSelected = NO;
    for (LALawsuitObj *item in self.suitCateObj.arrData) {
        item.isSelected = NO;
    }
    [self.tblSubList reloadData];
    [self.delegate doRemoveAllNewItem:self.suitCateObj];
}

-(void)doChangeSelect:(NSIndexPath *)indexPath{
    if (indexPath.row < self.arrDataList.count)
    {
        LALawsuitObj *obj = [self.arrDataList objectAtIndex:indexPath.row];
        if (obj.isSelected) {
            obj.isSelected = NO;
            
            BOOL bCheckNotSelectAll = YES;
            for (LALawsuitObj *item in self.suitCateObj.arrData) {
                if (item.isSelected) {
                    bCheckNotSelectAll = NO;
                }
            }
            if (bCheckNotSelectAll) {
                self.suitCateObj.isSelected = NO;
            }
            [self.tblSubList reloadData];
            [self.delegate doRemoveItem:obj andCategories:self.suitCateObj];
        }
        else{
            if ([self getCountSelectedThese] >= self.numberCanSelected && !self.isPriceCell) {
                // Show message
                [self.tblSubList reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"ShowMessageOutRangeThese" object:nil];
            }
            else{
                self.suitCateObj.isSelected = YES;
                obj.isSelected = YES;
                [self.tblSubList reloadData];
                [self.delegate doAddNewItem:obj andCategories:self.suitCateObj];
            }
        }
    }
}

-(void)doSeeMore:(NSIndexPath *)indexPath{
    if (self.delegate) {
        LALawsuitObj *obj = self.arrDataList[indexPath.row];
        [self.delegate doSeeMore:obj];
    }
}

-(NSInteger)getCountSelectedThese{
    NSInteger count = 0;
    
    for (LALawSuitCategory *cate in [AppDelegate sharedDelegate].selectedTheseData) {
        count += [cate.arrData count];
    }
    return count;
}
@end

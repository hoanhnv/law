//
//  LAAssinaturaTableViewCell.m
//  JustapLawyer
//
//  Created by Mac on 11/1/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAAssinaturaTableViewCell.h"
#import "LALawsuitObj.h"

@implementation LAAssinaturaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setupCell:(LASignatureTeseObj*)obj{
    
    self.lbName.text = obj.lawsuitThesis.name;
    self.lbInitFees.text = [NSString stringWithFormat:@"R$ %.2f",obj.initialFees];
    self.lbPercentSuccess.text = [NSString stringWithFormat:@" %.2f%%",obj.percentageInSuccess];
}
@end

//
//  LAPendenteTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPendenteTableViewCellType3.h"

@implementation LAPendenteTableViewCellType3

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(id)dataObj
{
    
}
- (IBAction)onAcharNovoAdvogadoPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onAcharNovoAdvogadoDidTouch:)]) {
        [self.delegate onAcharNovoAdvogadoDidTouch:self.currentPath];
    }
}
@end

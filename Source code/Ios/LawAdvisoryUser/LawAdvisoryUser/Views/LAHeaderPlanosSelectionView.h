//
//  LAHeaderPlanosSelectionView.h
//  JustapLawyer
//
//  Created by MAC on 10/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAHeaderPlanosSelectionView : UIView
@property (nonatomic) IBOutlet UILabel* lbTitle;
+ (CGSize)getHeight:(NSString*)txtTitle andUIlabelSize:(CGRect)rect;
+ (CGSize)getHeightText:(NSString*)txtTitle andUIlabelSize:(CGRect)rect;
@end

//
//  ServiceTesesFooterView.m
//  JustapLawyer
//
//  Created by Mac on 10/16/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "ServicePriceFooterView.h"

@implementation ServicePriceFooterView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (IBAction)onPricePressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onPrice)])
    {
        [self.delegate onPrice];
    }
}
- (IBAction)onRemoveSelectionPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onRemoveSelection)])
    {
        [self.delegate onRemoveSelection];
    }
}
@end

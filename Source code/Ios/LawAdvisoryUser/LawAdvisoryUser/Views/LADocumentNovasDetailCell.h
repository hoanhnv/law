//
//  LADocumentNovasCell.h
//  JustapLawyer
//
//  Created by MAC on 9/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LADocumentNovasDetailCell : UITableViewCell
@property (nonatomic) IBOutlet UICollectionView* clDocuments;
@property (nonatomic) NSMutableArray* arrDocument;
@end

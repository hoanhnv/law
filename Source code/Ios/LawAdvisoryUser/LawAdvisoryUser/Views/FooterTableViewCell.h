//
//  FooterTableViewCell.h
//  JustapLawyer
//
//  Created by Tùng Nguyễn on 2/14/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbContentSms;
@property (weak, nonatomic) IBOutlet UIView *vAll;
@property (weak, nonatomic) IBOutlet UIImageView *imgBgr;
@property (weak, nonatomic) IBOutlet UILabel *lbHour;

+(CGFloat)getHeight:(NSString*)string inWith:(UITableView*)table;
@end

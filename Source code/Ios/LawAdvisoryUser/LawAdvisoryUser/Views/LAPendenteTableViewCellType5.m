//
//  LAPendenteTableViewCell.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPendenteTableViewCellType5.h"
#import "LAHappeningObj.h"

@implementation LAPendenteTableViewCellType5

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setupData:(id)dataObj
{
    if (dataObj && [dataObj isKindOfClass:[LACustomerOpenObj class]])
    {
        self.objData = dataObj;
        self.lbStatus.text = self.objData.statusDescription;
        self.lbData.text = [LAUtilitys getDateStringFromTimeInterVal:self.objData.updatedAt withFormat:FORMAT_DD_MM_YY];
        self.lbAssunto.text = self.objData.happening.thesis.name;
        self.lbCategoria.text = self.objData.happening.thesis.category.name;
        self.lbClient.text = self.objData.user.name;
    }
}
- (IBAction)onVerDetalhesPresserd:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onPendenteVerDetalhesDidTouch:)]) {
        [self.delegate onPendenteVerDetalhesDidTouch:self.currentPath];
    }
}
@end

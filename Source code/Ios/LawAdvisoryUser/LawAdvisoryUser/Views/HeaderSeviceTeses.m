//
//  headerSeviceTeses.m
//  JustapLawyer
//
//  Created by Tung Nguyen on 10/14/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "HeaderSeviceTeses.h"
#import "LALawsuitObj.h"
#import "LALawSuitCategory.h"

#define DefaultColor [UIColor colorWithRed:75.0/255.0 green:187.0/255.0 blue:253.0/255.0 alpha:1.0]

@implementation HeaderSeviceTeses

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.isGreeen = NO;
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showContent:)];
        tap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tap];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        // Call a common method to setup gesture and state of UIView
        self.isGreeen = NO;
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showContent:)];
        tap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tap];
    }
    return self;
}
-(void)setupView:(LALawSuitCategory*)obj{
    self.obj = obj;
    NSString *strCount = [NSString stringWithFormat:@"(%ld assuntos)",[obj.arrData count]];
    self.lbTitle.text = [NSString stringWithFormat:@"%@%@",[obj.name uppercaseString],strCount];
    self.lbTitle.numberOfLines = 0;
    [self.lbTitle sizeToFit];
    
    if (CGRectGetMaxY(self.lbTitle.frame) > CGRectGetMaxY(self.btnDowUp.frame)) {
        self.imgLine.frame = CGRectMake(self.imgLine.frame.origin.x, CGRectGetMaxY(self.lbTitle.frame) + 8, self.imgLine.frame.size.width, self.imgLine.frame.size.height);
    }
}
- (IBAction)showContent:(id)sender {
    if (!_isShowContent) {
        _isShowContent = YES;
    } else {
        _isShowContent = NO;
    }
    if (_showBlock) {
        _showBlock(_isShowContent);
    }
    [self setupColor:_isShowContent];
    [self setupView:self.obj];
}
- (void)setupColor:(BOOL)isShow
{
    _isShowContent = isShow;
    if (!isShow) {
        _lbTitle.textColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        [_btnDowUp setBackgroundImage:[UIImage imageNamed:@"60x60_dropdown_gray"] forState:UIControlStateNormal];
        _imgLine.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
        
    } else {
        _lbTitle.textColor = DefaultColor;
        UIImage *image = [[UIImage imageNamed:@"60x60_dropup"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_btnDowUp setBackgroundImage:image forState:UIControlStateNormal];
        _btnDowUp.tintColor = DefaultColor;
        
        //        [_btnDowUp setBackgroundImage:[UIImage imageNamed:@"60x60_dropup"] forState:UIControlStateNormal];
        _imgLine.backgroundColor = DefaultColor;
        
    }
    
}
@end

//
//  LAPriceConfirmTableViewCell.h
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LALawsuitObj.h"
@interface LAPriceAllConfirmTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel* lbInitalPrice;
@property (nonatomic,weak) IBOutlet UILabel* lbPerceltuanExito;
- (void)setupData:(LALawsuitObj*)obj;
@end

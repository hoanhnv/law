//
//  LACartItemViewCell.m
//  JustapLawyer
//
//  Created by Mac on 10/26/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACartItemViewCell.h"
#import "LADistrictObj.h"
#import "LASignatureDistrict.h"
#import "LADistricts.h"
#import "LAPlanObj.h"
#import "AppDelegate.h"

@implementation LACartItemViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setupCell:(LASignatureObj *)signature andDistrict:(LADistrictObj*)district{
    self.lbDistrictName.text = district.name;
    self.lbTeses.text = [NSString stringWithFormat:@"%.0f %% das teses",signature.totalThesesOnSigning];
    self.lbValor.text = [NSString stringWithFormat:@"Valor: R$ %@",[LAUtilitys getCurrentcyFromDecimal:signature.valuePlanOnSigning]];
}

-(void)setupCell:(LASignatureObj *)signature andIndexPath:(NSIndexPath*)indexPath andLan:(LAPlanObj*)planObj {
    LADistricts* districts = (LADistricts*)[signature.districts lastObject];
    self.lbDistrictName.text = districts.district.name;
    if ([AppDelegate sharedDelegate].isDiscountAll) {
        self.lbValor.text = [NSString stringWithFormat:@"Valor: R$ %@",[LAUtilitys getCurrentcyFromDecimal:planObj.value*(1 - planObj.percent_by_district/100.0f)]];
    }
    else{
        if (indexPath.row > 0) {
            self.lbValor.text = [NSString stringWithFormat:@"Valor: R$ %@",[LAUtilitys getCurrentcyFromDecimal:planObj.value*(1 - planObj.percent_by_district/100.0f)]];
        }
        else{
            self.lbValor.text = [NSString stringWithFormat:@"Valor: R$ %@",[LAUtilitys getCurrentcyFromDecimal:planObj.value]];
        }
        
    }
}

@end

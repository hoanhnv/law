//
//  LACategoriesHeaderView.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/9/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LALawSuitCategory.h"

@interface LACategoriesHeaderView : UIView{
    
}
@property (nonatomic, strong) LALawSuitCategory *suitCate;
@property (nonatomic) id delegate;
@property (nonatomic) IBOutlet UIImageView* imgIconHeader;
@property (nonatomic) IBOutlet UILabel* txtTitle;

- (IBAction)doSelectedCate:(id)sender;
@end

@protocol LACategoryHeaderDelegate <NSObject>

- (void)didSelectedCate:(NSInteger)index;

@end
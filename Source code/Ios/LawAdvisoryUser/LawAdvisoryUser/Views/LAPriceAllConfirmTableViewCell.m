//
//  LAPriceConfirmTableViewCell.m
//  JustapLawyer
//
//  Created by MAC on 11/3/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAPriceAllConfirmTableViewCell.h"

@implementation LAPriceAllConfirmTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(LALawsuitObj*)obj
{
    if (obj)
    {
        self.lbInitalPrice.text = [NSString stringWithFormat:@"R$ %@",obj.initialPrice];
        self.lbPerceltuanExito.text = [NSString stringWithFormat:@"%@%%",obj.percentagePrice];
    }
    
}
@end

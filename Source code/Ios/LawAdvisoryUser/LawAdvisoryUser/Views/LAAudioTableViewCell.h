//
//  LAAudioTableViewCell.h
//  JustapUser
//
//  Created by MAC on 11/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LAAudioTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIButton* btPlay;
@property (nonatomic,weak) IBOutlet UILabel* lbName;
@property (nonatomic) NSString* strUrl;
@end

//
//  LANovasDetailFooter.h
//  JustapLawyer
//
//  Created by Mac on 3/18/17.
//  Copyright © 2017 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LANovasDetailFooter : UIView{
    
}
@property (nonatomic) id delegate;
@property (nonatomic) NSIndexPath* currentPath;

- (IBAction)onAceiterDetailPresserd:(id)sender;
- (IBAction)onRecusarDetailPresserd:(id)sender;

@end
@protocol AcoesNovaDetailCellDelegate <NSObject>

- (void)onAceiterDetailDidTouch:(NSIndexPath*)indexPath;
- (void)onRecusarDetailDidTouch:(NSIndexPath*)indexPath;

@end

//
//  LAData.h
//
//  Created by   on 10/15/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LADistrictObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *uf;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, strong) NSArray *neighborhoods;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAUserLawyerInfo.m
//  JustapUser
//
//  Created by Mac on 11/20/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAUserLawyerInfo.h"
#import "LAUserLawyerObj.h"


NSString *const kLAUserLawyerInfoSuccess = @"success";
NSString *const kLAUserLawyerInfoData = @"data";
NSString *const kLAUserLawyerInfoMessage = @"message";

@interface LAUserLawyerInfo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAUserLawyerInfo

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAUserLawyerInfoSuccess fromDictionary:dict] boolValue];
        NSObject *receivedLAData = [dict objectForKey:kLAUserLawyerInfoData];
        NSMutableArray *parsedLAData = [NSMutableArray array];
        if ([receivedLAData isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAData) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAData addObject:[LAUserLawyerObj modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAData isKindOfClass:[NSDictionary class]]) {
            [parsedLAData addObject:[LAUserLawyerObj modelObjectWithDictionary:(NSDictionary *)receivedLAData]];
        }
        self.data = [NSArray arrayWithArray:parsedLAData];
        self.message = [self objectOrNilForKey:kLAUserLawyerInfoMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAUserLawyerInfoSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAUserLawyerInfoData];
    self.message = [aDecoder decodeObjectForKey:kLAUserLawyerInfoMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeBool:_success forKey:kLAUserLawyerInfoSuccess];
    [aCoder encodeObject:_data forKey:kLAUserLawyerInfoData];
    [aCoder encodeObject:_message forKey:kLAUserLawyerInfoMessage];
}
@end

//
//  LADistrictBuy.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LADistrictBuy.h"


NSString *const kLADistrictBuyDeletedAt = @"deleted_at";
NSString *const kLADistrictBuyId = @"id";
NSString *const kLADistrictBuyUf = @"uf";
NSString *const kLADistrictBuyName = @"name";
NSString *const kLADistrictBuyUpdatedAt = @"updated_at";
NSString *const kLADistrictBuyCreatedAt = @"created_at";


@interface LADistrictBuy ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LADistrictBuy

@synthesize deletedAt = _deletedAt;
@synthesize districtIdentifier = _districtIdentifier;
@synthesize uf = _uf;
@synthesize name = _name;
@synthesize updatedAt = _updatedAt;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [self objectOrNilForKey:kLADistrictBuyDeletedAt fromDictionary:dict];
        self.districtIdentifier = [[self objectOrNilForKey:kLADistrictBuyId fromDictionary:dict] doubleValue];
        self.uf = [self objectOrNilForKey:kLADistrictBuyUf fromDictionary:dict];
        self.name = [self objectOrNilForKey:kLADistrictBuyName fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLADistrictBuyUpdatedAt fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLADistrictBuyCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLADistrictBuyDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtIdentifier] forKey:kLADistrictBuyId];
    [mutableDict setValue:self.uf forKey:kLADistrictBuyUf];
    [mutableDict setValue:self.name forKey:kLADistrictBuyName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLADistrictBuyUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLADistrictBuyCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.deletedAt = [aDecoder decodeObjectForKey:kLADistrictBuyDeletedAt];
    self.districtIdentifier = [aDecoder decodeDoubleForKey:kLADistrictBuyId];
    self.uf = [aDecoder decodeObjectForKey:kLADistrictBuyUf];
    self.name = [aDecoder decodeObjectForKey:kLADistrictBuyName];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLADistrictBuyUpdatedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLADistrictBuyCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_deletedAt forKey:kLADistrictBuyDeletedAt];
    [aCoder encodeDouble:_districtIdentifier forKey:kLADistrictBuyId];
    [aCoder encodeObject:_uf forKey:kLADistrictBuyUf];
    [aCoder encodeObject:_name forKey:kLADistrictBuyName];
    [aCoder encodeDouble:_updatedAt forKey:kLADistrictBuyUpdatedAt];
    [aCoder encodeDouble:_createdAt forKey:kLADistrictBuyCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LADistrictBuy *copy = [[LADistrictBuy alloc] init];
    
    if (copy) {
        
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.districtIdentifier = self.districtIdentifier;
        copy.uf = [self.uf copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

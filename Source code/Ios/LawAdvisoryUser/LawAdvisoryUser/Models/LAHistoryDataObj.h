//
//  LAData.h
//
//  Created by   on 11/14/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LAAssinatureObj;

@interface LAHistoryDataObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double endAt;
@property (nonatomic, assign) double signatureId;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double vindiId;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double startAt;
@property (nonatomic, strong) LAAssinatureObj *signature;
@property (nonatomic, strong) NSArray *districts;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *status;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

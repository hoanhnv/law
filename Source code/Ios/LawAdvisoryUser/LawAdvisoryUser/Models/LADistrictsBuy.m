//
//  LADistrictsBuy.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LADistrictsBuy.h"
#import "LADistrictBuy.h"


NSString *const kLADistrictsBuyDistrict = @"district";
NSString *const kLADistrictsBuyId = @"id";
NSString *const kLADistrictsBuyDeletedAt = @"deleted_at";
NSString *const kLADistrictsBuyPrice = @"price";
NSString *const kLADistrictsBuyCreatedAt = @"created_at";
NSString *const kLADistrictsBuySignatureId = @"signature_id";
NSString *const kLADistrictsBuyDistrictId = @"district_id";
NSString *const kLADistrictsBuyUpdatedAt = @"updated_at";


@interface LADistrictsBuy ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LADistrictsBuy

@synthesize district = _district;
@synthesize districtsIdentifier = _districtsIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize price = _price;
@synthesize createdAt = _createdAt;
@synthesize signatureId = _signatureId;
@synthesize districtId = _districtId;
@synthesize updatedAt = _updatedAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.district = [LADistrictBuy modelObjectWithDictionary:[dict objectForKey:kLADistrictsBuyDistrict]];
        self.districtsIdentifier = [[self objectOrNilForKey:kLADistrictsBuyId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLADistrictsBuyDeletedAt fromDictionary:dict];
        self.price = [[self objectOrNilForKey:kLADistrictsBuyPrice fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLADistrictsBuyCreatedAt fromDictionary:dict] doubleValue];
        self.signatureId = [[self objectOrNilForKey:kLADistrictsBuySignatureId fromDictionary:dict] doubleValue];
        self.districtId = [[self objectOrNilForKey:kLADistrictsBuyDistrictId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLADistrictsBuyUpdatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.district dictionaryRepresentation] forKey:kLADistrictsBuyDistrict];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtsIdentifier] forKey:kLADistrictsBuyId];
    [mutableDict setValue:self.deletedAt forKey:kLADistrictsBuyDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.price] forKey:kLADistrictsBuyPrice];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLADistrictsBuyCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureId] forKey:kLADistrictsBuySignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtId] forKey:kLADistrictsBuyDistrictId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLADistrictsBuyUpdatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.district = [aDecoder decodeObjectForKey:kLADistrictsBuyDistrict];
    self.districtsIdentifier = [aDecoder decodeDoubleForKey:kLADistrictsBuyId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLADistrictsBuyDeletedAt];
    self.price = [aDecoder decodeDoubleForKey:kLADistrictsBuyPrice];
    self.createdAt = [aDecoder decodeDoubleForKey:kLADistrictsBuyCreatedAt];
    self.signatureId = [aDecoder decodeDoubleForKey:kLADistrictsBuySignatureId];
    self.districtId = [aDecoder decodeDoubleForKey:kLADistrictsBuyDistrictId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLADistrictsBuyUpdatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_district forKey:kLADistrictsBuyDistrict];
    [aCoder encodeDouble:_districtsIdentifier forKey:kLADistrictsBuyId];
    [aCoder encodeObject:_deletedAt forKey:kLADistrictsBuyDeletedAt];
    [aCoder encodeDouble:_price forKey:kLADistrictsBuyPrice];
    [aCoder encodeDouble:_createdAt forKey:kLADistrictsBuyCreatedAt];
    [aCoder encodeDouble:_signatureId forKey:kLADistrictsBuySignatureId];
    [aCoder encodeDouble:_districtId forKey:kLADistrictsBuyDistrictId];
    [aCoder encodeDouble:_updatedAt forKey:kLADistrictsBuyUpdatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LADistrictsBuy *copy = [[LADistrictsBuy alloc] init];
    
    if (copy) {
        
        copy.district = [self.district copyWithZone:zone];
        copy.districtsIdentifier = self.districtsIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.price = self.price;
        copy.createdAt = self.createdAt;
        copy.signatureId = self.signatureId;
        copy.districtId = self.districtId;
        copy.updatedAt = self.updatedAt;
    }
    
    return copy;
}


@end

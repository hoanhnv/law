//
//  LALawSuitCategory.m
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawSuitCategory.h"


NSString *const kLALawSuitCategoryColor = @"color";
NSString *const kLALawSuitCategoryIconWithUrl = @"icon_with_url";
NSString *const kLALawSuitCategoryId = @"id";
NSString *const kLALawSuitCategoryDeletedAt = @"deleted_at";
NSString *const kLALawSuitCategoryCreatedAt = @"created_at";
NSString *const kLALawSuitCategoryDescription = @"description";
NSString *const kLALawSuitCategoryUpdatedAt = @"updated_at";
NSString *const kLALawSuitCategoryName = @"name";
NSString *const kLALawSuitCategoryIcon = @"icon";


@interface LALawSuitCategory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawSuitCategory

@synthesize color = _color;
@synthesize iconWithUrl = _iconWithUrl;
@synthesize categoryIdentifier = _categoryIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize createdAt = _createdAt;
@synthesize categoryDescription = _categoryDescription;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize icon = _icon;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.color = [self objectOrNilForKey:kLALawSuitCategoryColor fromDictionary:dict];
            self.iconWithUrl = [self objectOrNilForKey:kLALawSuitCategoryIconWithUrl fromDictionary:dict];
            self.categoryIdentifier = [[self objectOrNilForKey:kLALawSuitCategoryId fromDictionary:dict] doubleValue];
            self.deletedAt = [self objectOrNilForKey:kLALawSuitCategoryDeletedAt fromDictionary:dict];
            self.createdAt = [[self objectOrNilForKey:kLALawSuitCategoryCreatedAt fromDictionary:dict] doubleValue];
            self.categoryDescription = [self objectOrNilForKey:kLALawSuitCategoryDescription fromDictionary:dict];
            self.updatedAt = [[self objectOrNilForKey:kLALawSuitCategoryUpdatedAt fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kLALawSuitCategoryName fromDictionary:dict];
            self.icon = [self objectOrNilForKey:kLALawSuitCategoryIcon fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.color forKey:kLALawSuitCategoryColor];
    [mutableDict setValue:self.iconWithUrl forKey:kLALawSuitCategoryIconWithUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryIdentifier] forKey:kLALawSuitCategoryId];
    [mutableDict setValue:self.deletedAt forKey:kLALawSuitCategoryDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLALawSuitCategoryCreatedAt];
    [mutableDict setValue:self.categoryDescription forKey:kLALawSuitCategoryDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLALawSuitCategoryUpdatedAt];
    [mutableDict setValue:self.name forKey:kLALawSuitCategoryName];
    [mutableDict setValue:self.icon forKey:kLALawSuitCategoryIcon];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.color = [aDecoder decodeObjectForKey:kLALawSuitCategoryColor];
    self.iconWithUrl = [aDecoder decodeObjectForKey:kLALawSuitCategoryIconWithUrl];
    self.categoryIdentifier = [aDecoder decodeDoubleForKey:kLALawSuitCategoryId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLALawSuitCategoryDeletedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLALawSuitCategoryCreatedAt];
    self.categoryDescription = [aDecoder decodeObjectForKey:kLALawSuitCategoryDescription];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLALawSuitCategoryUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLALawSuitCategoryName];
    self.icon = [aDecoder decodeObjectForKey:kLALawSuitCategoryIcon];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_color forKey:kLALawSuitCategoryColor];
    [aCoder encodeObject:_iconWithUrl forKey:kLALawSuitCategoryIconWithUrl];
    [aCoder encodeDouble:_categoryIdentifier forKey:kLALawSuitCategoryId];
    [aCoder encodeObject:_deletedAt forKey:kLALawSuitCategoryDeletedAt];
    [aCoder encodeDouble:_createdAt forKey:kLALawSuitCategoryCreatedAt];
    [aCoder encodeObject:_categoryDescription forKey:kLALawSuitCategoryDescription];
    [aCoder encodeDouble:_updatedAt forKey:kLALawSuitCategoryUpdatedAt];
    [aCoder encodeObject:_name forKey:kLALawSuitCategoryName];
    [aCoder encodeObject:_icon forKey:kLALawSuitCategoryIcon];
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawSuitCategory *copy = [[LALawSuitCategory alloc] init];
    
    if (copy) {

        copy.color = [self.color copyWithZone:zone];
        copy.iconWithUrl = [self.iconWithUrl copyWithZone:zone];
        copy.categoryIdentifier = self.categoryIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.categoryDescription = [self.categoryDescription copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.icon = [self.icon copyWithZone:zone];
    }
    
    return copy;
}
- (BOOL)isEqual:(id)object
{
    return (![object isKindOfClass:[NSNull class]] && self.categoryIdentifier == ((LALawSuitCategory*)object).categoryIdentifier);
}

@end

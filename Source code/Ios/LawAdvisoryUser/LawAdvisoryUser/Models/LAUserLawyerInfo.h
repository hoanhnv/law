//
//  LAUserLawyerInfo.h
//  JustapUser
//
//  Created by Mac on 11/20/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
@class LAUserLawyerObj;
@interface LAUserLawyerInfo : NSObject<NSCoding, NSCopying>

@property (nonatomic, assign) BOOL success;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSString *message;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;
@end

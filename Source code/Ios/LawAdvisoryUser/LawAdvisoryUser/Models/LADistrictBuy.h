//
//  LADistrict.h
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LADistrictBuy : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double districtIdentifier;
@property (nonatomic, strong) NSString *uf;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

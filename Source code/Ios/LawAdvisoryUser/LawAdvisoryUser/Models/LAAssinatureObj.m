//
//  LAAssinatureObj.m
//
//  Created by   on 11/9/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAAssinatureObj.h"
#import "LAPlanObj.h"


NSString *const kLAAssinatureObjId = @"id";
NSString *const kLAAssinatureObjPlanId = @"plan_id";
NSString *const kLAAssinatureObjValuePlanOnSigning = @"value_plan_on_signing";
NSString *const kLAAssinatureObjTotalThesesOnSigning = @"total_theses_on_signing";
NSString *const kLAAssinatureObjTotalPlanOnSigning = @"total_plan_on_signing";
NSString *const kLAAssinatureObjLawyerId = @"lawyer_id";
NSString *const kLAAssinatureObjTotalDistrictsSigning = @"total_districts_signing";
NSString *const kLAAssinatureObjCreatedAt = @"created_at";
NSString *const kLAAssinatureObjPaymentMethod = @"payment_method";
NSString *const kLAAssinatureObjDeletedAt = @"deleted_at";
NSString *const kLAAssinatureObjUpdatedAt = @"updated_at";
NSString *const kLAAssinatureObjPlan = @"plan";
NSString *const kLAAssinatureObjStatus = @"status";
NSString *const kLAAssinatureObjValuePerExtraDistrict = @"value_per_extra_district";


@interface LAAssinatureObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAAssinatureObj

@synthesize dataIdentifier = _dataIdentifier;
@synthesize planId = _planId;
@synthesize valuePlanOnSigning = _valuePlanOnSigning;
@synthesize totalThesesOnSigning = _totalThesesOnSigning;
@synthesize totalPlanOnSigning = _totalPlanOnSigning;
@synthesize lawyerId = _lawyerId;
@synthesize totalDistrictsSigning = _totalDistrictsSigning;
@synthesize createdAt = _createdAt;
@synthesize paymentMethod = _paymentMethod;
@synthesize deletedAt = _deletedAt;
@synthesize updatedAt = _updatedAt;
@synthesize plan = _plan;
@synthesize status = _status;
@synthesize valuePerExtraDistrict = _valuePerExtraDistrict;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.dataIdentifier = [[self objectOrNilForKey:kLAAssinatureObjId fromDictionary:dict] doubleValue];
        self.planId = [[self objectOrNilForKey:kLAAssinatureObjPlanId fromDictionary:dict] doubleValue];
        self.valuePlanOnSigning = [[self objectOrNilForKey:kLAAssinatureObjValuePlanOnSigning fromDictionary:dict] doubleValue];
        self.totalThesesOnSigning = [[self objectOrNilForKey:kLAAssinatureObjTotalThesesOnSigning fromDictionary:dict] doubleValue];
        self.totalPlanOnSigning = [[self objectOrNilForKey:kLAAssinatureObjTotalPlanOnSigning fromDictionary:dict] doubleValue];
        self.lawyerId = [[self objectOrNilForKey:kLAAssinatureObjLawyerId fromDictionary:dict] doubleValue];
        self.totalDistrictsSigning = [[self objectOrNilForKey:kLAAssinatureObjTotalDistrictsSigning fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLAAssinatureObjCreatedAt fromDictionary:dict] doubleValue];
        self.paymentMethod = [self objectOrNilForKey:kLAAssinatureObjPaymentMethod fromDictionary:dict];
        self.deletedAt = [self objectOrNilForKey:kLAAssinatureObjDeletedAt fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLAAssinatureObjUpdatedAt fromDictionary:dict] doubleValue];
        self.plan = [LAPlanObj modelObjectWithDictionary:[dict objectForKey:kLAAssinatureObjPlan]];
        self.status = [self objectOrNilForKey:kLAAssinatureObjStatus fromDictionary:dict];
        self.valuePerExtraDistrict = [[self objectOrNilForKey:kLAAssinatureObjValuePerExtraDistrict fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLAAssinatureObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.planId] forKey:kLAAssinatureObjPlanId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePlanOnSigning] forKey:kLAAssinatureObjValuePlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalThesesOnSigning] forKey:kLAAssinatureObjTotalThesesOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalPlanOnSigning] forKey:kLAAssinatureObjTotalPlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerId] forKey:kLAAssinatureObjLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalDistrictsSigning] forKey:kLAAssinatureObjTotalDistrictsSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAAssinatureObjCreatedAt];
    [mutableDict setValue:self.paymentMethod forKey:kLAAssinatureObjPaymentMethod];
    [mutableDict setValue:self.deletedAt forKey:kLAAssinatureObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAAssinatureObjUpdatedAt];
    [mutableDict setValue:self.status forKey:kLAAssinatureObjStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePerExtraDistrict] forKey:kLAAssinatureObjValuePerExtraDistrict];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAAssinatureObjId];
    self.planId = [aDecoder decodeDoubleForKey:kLAAssinatureObjPlanId];
    self.valuePlanOnSigning = [aDecoder decodeDoubleForKey:kLAAssinatureObjValuePlanOnSigning];
    self.totalThesesOnSigning = [aDecoder decodeDoubleForKey:kLAAssinatureObjTotalThesesOnSigning];
    self.totalPlanOnSigning = [aDecoder decodeDoubleForKey:kLAAssinatureObjTotalPlanOnSigning];
    self.lawyerId = [aDecoder decodeDoubleForKey:kLAAssinatureObjLawyerId];
    self.totalDistrictsSigning = [aDecoder decodeDoubleForKey:kLAAssinatureObjTotalDistrictsSigning];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAAssinatureObjCreatedAt];
    self.paymentMethod = [aDecoder decodeObjectForKey:kLAAssinatureObjPaymentMethod];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAAssinatureObjDeletedAt];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAAssinatureObjUpdatedAt];
    self.plan = [aDecoder decodeObjectForKey:kLAAssinatureObjPlan];
    self.status = [aDecoder decodeObjectForKey:kLAAssinatureObjStatus];
    self.valuePerExtraDistrict = [aDecoder decodeDoubleForKey:kLAAssinatureObjValuePerExtraDistrict];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_dataIdentifier forKey:kLAAssinatureObjId];
    [aCoder encodeDouble:_planId forKey:kLAAssinatureObjPlanId];
    [aCoder encodeDouble:_valuePlanOnSigning forKey:kLAAssinatureObjValuePlanOnSigning];
    [aCoder encodeDouble:_totalThesesOnSigning forKey:kLAAssinatureObjTotalThesesOnSigning];
    [aCoder encodeDouble:_totalPlanOnSigning forKey:kLAAssinatureObjTotalPlanOnSigning];
    [aCoder encodeDouble:_lawyerId forKey:kLAAssinatureObjLawyerId];
    [aCoder encodeDouble:_totalDistrictsSigning forKey:kLAAssinatureObjTotalDistrictsSigning];
    [aCoder encodeDouble:_createdAt forKey:kLAAssinatureObjCreatedAt];
    [aCoder encodeObject:_paymentMethod forKey:kLAAssinatureObjPaymentMethod];
    [aCoder encodeObject:_deletedAt forKey:kLAAssinatureObjDeletedAt];
    [aCoder encodeDouble:_updatedAt forKey:kLAAssinatureObjUpdatedAt];
    [aCoder encodeObject:_plan forKey:kLAAssinatureObjPlan];
    [aCoder encodeObject:_status forKey:kLAAssinatureObjStatus];
    [aCoder encodeDouble:_valuePerExtraDistrict forKey:kLAAssinatureObjValuePerExtraDistrict];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAAssinatureObj *copy = [[LAAssinatureObj alloc] init];
    
    if (copy) {
        
        copy.dataIdentifier = self.dataIdentifier;
        copy.planId = self.planId;
        copy.valuePlanOnSigning = self.valuePlanOnSigning;
        copy.totalThesesOnSigning = self.totalThesesOnSigning;
        copy.totalPlanOnSigning = self.totalPlanOnSigning;
        copy.lawyerId = self.lawyerId;
        copy.totalDistrictsSigning = self.totalDistrictsSigning;
        copy.createdAt = self.createdAt;
        copy.paymentMethod = [self.paymentMethod copyWithZone:zone];
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.plan = [self.plan copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.valuePerExtraDistrict = self.valuePerExtraDistrict;
    }
    
    return copy;
}


@end

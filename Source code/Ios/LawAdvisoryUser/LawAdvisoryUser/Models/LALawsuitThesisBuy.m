//
//  LALawsuitThesisBuy.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawsuitThesisBuy.h"
#import "LALawSuitCategory.h"


NSString *const kLALawsuitThesisBuyDocumentation = @"documentation";
NSString *const kLALawsuitThesisBuyCategory = @"category";
NSString *const kLALawsuitThesisBuyId = @"id";
NSString *const kLALawsuitThesisBuyVersion = @"version";
NSString *const kLALawsuitThesisBuyYourRights = @"your_rights";
NSString *const kLALawsuitThesisBuyCreatedAt = @"created_at";
NSString *const kLALawsuitThesisBuyCategoryId = @"category_id";
NSString *const kLALawsuitThesisBuyLegalName = @"legal_name";
NSString *const kLALawsuitThesisBuyIdentification = @"identification";
NSString *const kLALawsuitThesisBuyHowToAct = @"how_to_act";
NSString *const kLALawsuitThesisBuyValueContraction = @"value_contraction";
NSString *const kLALawsuitThesisBuyDeletedAt = @"deleted_at";
NSString *const kLALawsuitThesisBuySituationId = @"situation_id";
NSString *const kLALawsuitThesisBuyValueSignature = @"value_signature";
NSString *const kLALawsuitThesisBuyUpdatedAt = @"updated_at";
NSString *const kLALawsuitThesisBuyName = @"name";
NSString *const kLALawsuitThesisBuyStatus = @"status";


@interface LALawsuitThesisBuy ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawsuitThesisBuy

@synthesize documentation = _documentation;
@synthesize category = _category;
@synthesize lawsuitThesisIdentifier = _lawsuitThesisIdentifier;
@synthesize version = _version;
@synthesize yourRights = _yourRights;
@synthesize createdAt = _createdAt;
@synthesize categoryId = _categoryId;
@synthesize legalName = _legalName;
@synthesize identification = _identification;
@synthesize howToAct = _howToAct;
@synthesize valueContraction = _valueContraction;
@synthesize deletedAt = _deletedAt;
@synthesize situationId = _situationId;
@synthesize valueSignature = _valueSignature;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.documentation = [self objectOrNilForKey:kLALawsuitThesisBuyDocumentation fromDictionary:dict];
        self.category = [LALawSuitCategory modelObjectWithDictionary:[dict objectForKey:kLALawsuitThesisBuyCategory]];
        self.lawsuitThesisIdentifier = [[self objectOrNilForKey:kLALawsuitThesisBuyId fromDictionary:dict] doubleValue];
        self.version = [[self objectOrNilForKey:kLALawsuitThesisBuyVersion fromDictionary:dict] doubleValue];
        self.yourRights = [self objectOrNilForKey:kLALawsuitThesisBuyYourRights fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLALawsuitThesisBuyCreatedAt fromDictionary:dict] doubleValue];
        self.categoryId = [[self objectOrNilForKey:kLALawsuitThesisBuyCategoryId fromDictionary:dict] doubleValue];
        self.legalName = [self objectOrNilForKey:kLALawsuitThesisBuyLegalName fromDictionary:dict];
        self.identification = [self objectOrNilForKey:kLALawsuitThesisBuyIdentification fromDictionary:dict];
        self.howToAct = [self objectOrNilForKey:kLALawsuitThesisBuyHowToAct fromDictionary:dict];
        self.valueContraction = [[self objectOrNilForKey:kLALawsuitThesisBuyValueContraction fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLALawsuitThesisBuyDeletedAt fromDictionary:dict];
        self.situationId = [[self objectOrNilForKey:kLALawsuitThesisBuySituationId fromDictionary:dict] doubleValue];
        self.valueSignature = [[self objectOrNilForKey:kLALawsuitThesisBuyValueSignature fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLALawsuitThesisBuyUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLALawsuitThesisBuyName fromDictionary:dict];
        self.status = [self objectOrNilForKey:kLALawsuitThesisBuyStatus fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.documentation forKey:kLALawsuitThesisBuyDocumentation];
    //    [mutableDict setValue:[self.category dictionaryRepresentation] forKey:kLALawsuitThesisBuyCategory];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawsuitThesisIdentifier] forKey:kLALawsuitThesisBuyId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.version] forKey:kLALawsuitThesisBuyVersion];
    [mutableDict setValue:self.yourRights forKey:kLALawsuitThesisBuyYourRights];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLALawsuitThesisBuyCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryId] forKey:kLALawsuitThesisBuyCategoryId];
    [mutableDict setValue:self.legalName forKey:kLALawsuitThesisBuyLegalName];
    [mutableDict setValue:self.identification forKey:kLALawsuitThesisBuyIdentification];
    [mutableDict setValue:self.howToAct forKey:kLALawsuitThesisBuyHowToAct];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueContraction] forKey:kLALawsuitThesisBuyValueContraction];
    [mutableDict setValue:self.deletedAt forKey:kLALawsuitThesisBuyDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.situationId] forKey:kLALawsuitThesisBuySituationId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueSignature] forKey:kLALawsuitThesisBuyValueSignature];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLALawsuitThesisBuyUpdatedAt];
    [mutableDict setValue:self.name forKey:kLALawsuitThesisBuyName];
    [mutableDict setValue:self.status forKey:kLALawsuitThesisBuyStatus];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.documentation = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyDocumentation];
    self.category = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyCategory];
    self.lawsuitThesisIdentifier = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuyId];
    self.version = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuyVersion];
    self.yourRights = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyYourRights];
    self.createdAt = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuyCreatedAt];
    self.categoryId = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuyCategoryId];
    self.legalName = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyLegalName];
    self.identification = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyIdentification];
    self.howToAct = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyHowToAct];
    self.valueContraction = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuyValueContraction];
    self.deletedAt = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyDeletedAt];
    self.situationId = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuySituationId];
    self.valueSignature = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuyValueSignature];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLALawsuitThesisBuyUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyName];
    self.status = [aDecoder decodeObjectForKey:kLALawsuitThesisBuyStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_documentation forKey:kLALawsuitThesisBuyDocumentation];
    [aCoder encodeObject:_category forKey:kLALawsuitThesisBuyCategory];
    [aCoder encodeDouble:_lawsuitThesisIdentifier forKey:kLALawsuitThesisBuyId];
    [aCoder encodeDouble:_version forKey:kLALawsuitThesisBuyVersion];
    [aCoder encodeObject:_yourRights forKey:kLALawsuitThesisBuyYourRights];
    [aCoder encodeDouble:_createdAt forKey:kLALawsuitThesisBuyCreatedAt];
    [aCoder encodeDouble:_categoryId forKey:kLALawsuitThesisBuyCategoryId];
    [aCoder encodeObject:_legalName forKey:kLALawsuitThesisBuyLegalName];
    [aCoder encodeObject:_identification forKey:kLALawsuitThesisBuyIdentification];
    [aCoder encodeObject:_howToAct forKey:kLALawsuitThesisBuyHowToAct];
    [aCoder encodeDouble:_valueContraction forKey:kLALawsuitThesisBuyValueContraction];
    [aCoder encodeObject:_deletedAt forKey:kLALawsuitThesisBuyDeletedAt];
    [aCoder encodeDouble:_situationId forKey:kLALawsuitThesisBuySituationId];
    [aCoder encodeDouble:_valueSignature forKey:kLALawsuitThesisBuyValueSignature];
    [aCoder encodeDouble:_updatedAt forKey:kLALawsuitThesisBuyUpdatedAt];
    [aCoder encodeObject:_name forKey:kLALawsuitThesisBuyName];
    [aCoder encodeObject:_status forKey:kLALawsuitThesisBuyStatus];
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawsuitThesisBuy *copy = [[LALawsuitThesisBuy alloc] init];
    
    if (copy) {
        
        copy.documentation = [self.documentation copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.lawsuitThesisIdentifier = self.lawsuitThesisIdentifier;
        copy.version = self.version;
        copy.yourRights = [self.yourRights copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.categoryId = self.categoryId;
        copy.legalName = [self.legalName copyWithZone:zone];
        copy.identification = [self.identification copyWithZone:zone];
        copy.howToAct = [self.howToAct copyWithZone:zone];
        copy.valueContraction = self.valueContraction;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.situationId = self.situationId;
        copy.valueSignature = self.valueSignature;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
    }
    
    return copy;
}


@end

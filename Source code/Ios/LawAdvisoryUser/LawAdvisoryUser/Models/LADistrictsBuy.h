//
//  LADistricts.h
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LADistrictBuy;

@interface LADistrictsBuy : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) LADistrictBuy *district;
@property (nonatomic, assign) double districtsIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double price;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double signatureId;
@property (nonatomic, assign) double districtId;
@property (nonatomic, assign) double updatedAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAGetHappeningByID.m
//
//  Created by   on 11/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAGetHappeningByID.h"
#import "LAHappeningObj.h"


NSString *const kLAGetHappeningByIDSuccess = @"success";
NSString *const kLAGetHappeningByIDData = @"data";
NSString *const kLAGetHappeningByIDMessage = @"message";


@interface LAGetHappeningByID ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAGetHappeningByID

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAGetHappeningByIDSuccess fromDictionary:dict] boolValue];
        self.data = [LAHappeningObj modelObjectWithDictionary:[dict objectForKey:kLAGetHappeningByIDData]];
        self.message = [self objectOrNilForKey:kLAGetHappeningByIDMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAGetHappeningByIDSuccess];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kLAGetHappeningByIDData];
    [mutableDict setValue:self.message forKey:kLAGetHappeningByIDMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAGetHappeningByIDSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAGetHappeningByIDData];
    self.message = [aDecoder decodeObjectForKey:kLAGetHappeningByIDMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAGetHappeningByIDSuccess];
    [aCoder encodeObject:_data forKey:kLAGetHappeningByIDData];
    [aCoder encodeObject:_message forKey:kLAGetHappeningByIDMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAGetHappeningByID *copy = [[LAGetHappeningByID alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

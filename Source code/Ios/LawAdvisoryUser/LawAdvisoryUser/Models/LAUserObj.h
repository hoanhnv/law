//
//  LAData.h
//
//  Created by   on 11/19/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LAUserObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *imageRgWithUrl;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double dateOfBirth;
@property (nonatomic, strong) NSString *cellphone;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, strong) NSString *complement;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, strong) NSString *neighborhood;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *imageRg;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *cpf;
@property (nonatomic, strong) NSString *uf;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *acceptsNews;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAData.m
//
//  Created by   on 9/26/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAFaqCategoriesObj.h"


NSString *const kLADataDeletedAt = @"deleted_at";
NSString *const kLADataId = @"id";
NSString *const kLADataUpdatedAt = @"updated_at";
NSString *const kLADataName = @"name";
NSString *const kLADataDescription = @"description";
NSString *const kLADataCreatedAt = @"created_at";


@interface LAFaqCategoriesObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAFaqCategoriesObj

@synthesize deletedAt = _deletedAt;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize dataDescription = _dataDescription;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [[self objectOrNilForKey:kLADataDeletedAt fromDictionary:dict] doubleValue];
        self.dataIdentifier = [[self objectOrNilForKey:kLADataId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLADataUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLADataName fromDictionary:dict];
        self.dataDescription = [self objectOrNilForKey:kLADataDescription fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLADataCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    //    [mutableDict setValue:self.deletedAt forKey:kLADataDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLADataId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLADataUpdatedAt];
    [mutableDict setValue:self.name forKey:kLADataName];
    [mutableDict setValue:self.dataDescription forKey:kLADataDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLADataCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    //    self.deletedAt = [aDecoder decodeObjectForKey:kLADataDeletedAt];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLADataId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLADataUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLADataName];
    self.dataDescription = [aDecoder decodeObjectForKey:kLADataDescription];
    self.createdAt = [aDecoder decodeDoubleForKey:kLADataCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    //    [aCoder encodeObject:_deletedAt forKey:kLADataDeletedAt];
    [aCoder encodeDouble:_dataIdentifier forKey:kLADataId];
    [aCoder encodeDouble:_updatedAt forKey:kLADataUpdatedAt];
    [aCoder encodeObject:_name forKey:kLADataName];
    [aCoder encodeObject:_dataDescription forKey:kLADataDescription];
    [aCoder encodeDouble:_createdAt forKey:kLADataCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAFaqCategoriesObj *copy = [[LAFaqCategoriesObj alloc] init];
    
    if (copy) {
        
        //        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

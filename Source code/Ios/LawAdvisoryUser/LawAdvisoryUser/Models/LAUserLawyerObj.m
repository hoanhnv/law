//
//  LAUserLawyer.m
//
//  Created by   on 11/20/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAUserLawyerObj.h"
#import "LAUserSignature.h"


NSString *const kLAUserLawyerId = @"id";
NSString *const kLAUserLawyerSignatureId = @"signature_id";
NSString *const kLAUserLawyerPercentageInSuccess = @"percentage_in_success";
NSString *const kLAUserLawyerCreatedAt = @"created_at";
NSString *const kLAUserLawyerDeletedAt = @"deleted_at";
NSString *const kLAUserLawyerPrice = @"price";
NSString *const kLAUserLawyerSignature = @"signature";
NSString *const kLAUserLawyerStars = @"stars";
NSString *const kLAUserLawyerUpdatedAt = @"updated_at";
NSString *const kLAUserLawyerTotalClients = @"total_clients";
NSString *const kLAUserLawyerThesisId = @"thesis_id";
NSString *const kLAUserLawyerInitialFees = @"initial_fees";


@interface LAUserLawyerObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAUserLawyerObj

@synthesize dataIdentifier = _dataIdentifier;
@synthesize signatureId = _signatureId;
@synthesize percentageInSuccess = _percentageInSuccess;
@synthesize createdAt = _createdAt;
@synthesize deletedAt = _deletedAt;
@synthesize price = _price;
@synthesize signature = _signature;
@synthesize stars = _stars;
@synthesize updatedAt = _updatedAt;
@synthesize totalClients = _totalClients;
@synthesize thesisId = _thesisId;
@synthesize initialFees = _initialFees;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.dataIdentifier = [[self objectOrNilForKey:kLAUserLawyerId fromDictionary:dict] doubleValue];
        self.signatureId = [[self objectOrNilForKey:kLAUserLawyerSignatureId fromDictionary:dict] doubleValue];
        self.percentageInSuccess = [[self objectOrNilForKey:kLAUserLawyerPercentageInSuccess fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLAUserLawyerCreatedAt fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLAUserLawyerDeletedAt fromDictionary:dict];
        self.price = [[self objectOrNilForKey:kLAUserLawyerPrice fromDictionary:dict] doubleValue];
        self.signature = [LAUserSignature modelObjectWithDictionary:[dict objectForKey:kLAUserLawyerSignature]];
        self.stars = [[self objectOrNilForKey:kLAUserLawyerStars fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLAUserLawyerUpdatedAt fromDictionary:dict] doubleValue];
        self.totalClients = [[self objectOrNilForKey:kLAUserLawyerTotalClients fromDictionary:dict] doubleValue];
        self.thesisId = [[self objectOrNilForKey:kLAUserLawyerThesisId fromDictionary:dict] doubleValue];
        self.initialFees = [[self objectOrNilForKey:kLAUserLawyerInitialFees fromDictionary:dict] doubleValue];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLAUserLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureId] forKey:kLAUserLawyerSignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.percentageInSuccess] forKey:kLAUserLawyerPercentageInSuccess];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAUserLawyerCreatedAt];
    [mutableDict setValue:self.deletedAt forKey:kLAUserLawyerDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.price] forKey:kLAUserLawyerPrice];
    [mutableDict setValue:[self.signature dictionaryRepresentation] forKey:kLAUserLawyerSignature];
    [mutableDict setValue:[NSNumber numberWithDouble:self.stars] forKey:kLAUserLawyerStars];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAUserLawyerUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalClients] forKey:kLAUserLawyerTotalClients];
    [mutableDict setValue:[NSNumber numberWithDouble:self.thesisId] forKey:kLAUserLawyerThesisId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.initialFees] forKey:kLAUserLawyerInitialFees];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAUserLawyerId];
    self.signatureId = [aDecoder decodeDoubleForKey:kLAUserLawyerSignatureId];
    self.percentageInSuccess = [aDecoder decodeDoubleForKey:kLAUserLawyerPercentageInSuccess];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAUserLawyerCreatedAt];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAUserLawyerDeletedAt];
    self.price = [aDecoder decodeDoubleForKey:kLAUserLawyerPrice];
    self.signature = [aDecoder decodeObjectForKey:kLAUserLawyerSignature];
    self.stars = [aDecoder decodeDoubleForKey:kLAUserLawyerStars];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAUserLawyerUpdatedAt];
    self.totalClients = [aDecoder decodeDoubleForKey:kLAUserLawyerTotalClients];
    self.thesisId = [aDecoder decodeDoubleForKey:kLAUserLawyerThesisId];
    self.initialFees = [aDecoder decodeDoubleForKey:kLAUserLawyerInitialFees];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_dataIdentifier forKey:kLAUserLawyerId];
    [aCoder encodeDouble:_signatureId forKey:kLAUserLawyerSignatureId];
    [aCoder encodeDouble:_percentageInSuccess forKey:kLAUserLawyerPercentageInSuccess];
    [aCoder encodeDouble:_createdAt forKey:kLAUserLawyerCreatedAt];
    [aCoder encodeObject:_deletedAt forKey:kLAUserLawyerDeletedAt];
    [aCoder encodeDouble:_price forKey:kLAUserLawyerPrice];
    [aCoder encodeObject:_signature forKey:kLAUserLawyerSignature];
    [aCoder encodeDouble:_stars forKey:kLAUserLawyerStars];
    [aCoder encodeDouble:_updatedAt forKey:kLAUserLawyerUpdatedAt];
    [aCoder encodeDouble:_totalClients forKey:kLAUserLawyerTotalClients];
    [aCoder encodeDouble:_thesisId forKey:kLAUserLawyerThesisId];
    [aCoder encodeDouble:_initialFees forKey:kLAUserLawyerInitialFees];
}
@end

//
//  LASignature.h
//
//  Created by   on 11/14/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LAPlanObj;

@interface LAHistorySignature : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double signatureIdentifier;
@property (nonatomic, assign) double planId;
@property (nonatomic, assign) double valuePlanOnSigning;
@property (nonatomic, assign) double totalThesesOnSigning;
@property (nonatomic, assign) double totalPlanOnSigning;
@property (nonatomic, assign) double lawyerId;
@property (nonatomic, assign) double totalDistrictsSigning;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double vindiId;
@property (nonatomic, strong) NSString *paymentMethod;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) LAPlanObj *plan;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, assign) double valuePerExtraDistrict;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

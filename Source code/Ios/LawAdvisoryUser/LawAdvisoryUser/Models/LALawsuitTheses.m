//
//  LALawsuitTheses.m
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawsuitTheses.h"
#import "LALawsuitObj.h"


NSString *const kLALawsuitThesesSuccess = @"success";
NSString *const kLALawsuitThesesData = @"data";
NSString *const kLALawsuitThesesMessage = @"message";


@interface LALawsuitTheses ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawsuitTheses

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.success = [[self objectOrNilForKey:kLALawsuitThesesSuccess fromDictionary:dict] boolValue];
    NSObject *receivedLALawsuitObj = [dict objectForKey:kLALawsuitThesesData];
    NSMutableArray *parsedLALawsuitObj = [NSMutableArray array];
    if ([receivedLALawsuitObj isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedLALawsuitObj) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedLALawsuitObj addObject:[LALawsuitObj modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedLALawsuitObj isKindOfClass:[NSDictionary class]]) {
       [parsedLALawsuitObj addObject:[LALawsuitObj modelObjectWithDictionary:(NSDictionary *)receivedLALawsuitObj]];
    }

    self.data = [NSArray arrayWithArray:parsedLALawsuitObj];
            self.message = [self objectOrNilForKey:kLALawsuitThesesMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLALawsuitThesesSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLALawsuitThesesData];
    [mutableDict setValue:self.message forKey:kLALawsuitThesesMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.success = [aDecoder decodeBoolForKey:kLALawsuitThesesSuccess];
    self.data = [aDecoder decodeObjectForKey:kLALawsuitThesesData];
    self.message = [aDecoder decodeObjectForKey:kLALawsuitThesesMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeBool:_success forKey:kLALawsuitThesesSuccess];
    [aCoder encodeObject:_data forKey:kLALawsuitThesesData];
    [aCoder encodeObject:_message forKey:kLALawsuitThesesMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawsuitTheses *copy = [[LALawsuitTheses alloc] init];
    
    if (copy) {

        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LANeighborhoods.h
//
//  Created by   on 10/15/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LANeighborhoods : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double districtId;
@property (nonatomic, assign) double neighborhoodsIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

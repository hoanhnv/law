//
//  LAData.h
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LACategory;
@class LAThesisBannerObj;

@interface LABannerObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) LACategory *category;
@property (nonatomic, strong) NSString *imageWithUrl;
@property (nonatomic, assign) double position;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic,assign) double deletedAt;
@property (nonatomic, assign) double categoryId;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *dataDescription;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) LAThesisBannerObj *thesis;
@property (nonatomic, assign) double thesisId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAFaqCategories.m
//
//  Created by   on 9/26/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAFaqCategories.h"
#import "LAFaqCategoriesObj.h"


NSString *const kLAFaqCategoriesSuccess = @"success";
NSString *const kLAFaqCategoriesData = @"data";
NSString *const kLAFaqCategoriesMessage = @"message";


@interface LAFaqCategories ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAFaqCategories

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAFaqCategoriesSuccess fromDictionary:dict] boolValue];
        NSObject *receivedLAData = [dict objectForKey:kLAFaqCategoriesData];
        NSMutableArray *parsedLAData = [NSMutableArray array];
        if ([receivedLAData isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAData) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAData addObject:[LAFaqCategoriesObj modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAData isKindOfClass:[NSDictionary class]]) {
            [parsedLAData addObject:[LAFaqCategoriesObj modelObjectWithDictionary:(NSDictionary *)receivedLAData]];
        }
        
        self.data = [NSArray arrayWithArray:parsedLAData];
        self.message = [self objectOrNilForKey:kLAFaqCategoriesMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAFaqCategoriesSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLAFaqCategoriesData];
    [mutableDict setValue:self.message forKey:kLAFaqCategoriesMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAFaqCategoriesSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAFaqCategoriesData];
    self.message = [aDecoder decodeObjectForKey:kLAFaqCategoriesMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAFaqCategoriesSuccess];
    [aCoder encodeObject:_data forKey:kLAFaqCategoriesData];
    [aCoder encodeObject:_message forKey:kLAFaqCategoriesMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAFaqCategories *copy = [[LAFaqCategories alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

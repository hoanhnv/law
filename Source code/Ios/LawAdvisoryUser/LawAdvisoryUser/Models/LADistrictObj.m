//
//  LADistrict.m
//
//  Created by   on 10/15/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LADistrictObj.h"


NSString *const kLADistrictUf = @"uf";
NSString *const kLADistrictId = @"id";
NSString *const kLADistrictDeletedAt = @"deleted_at";
NSString *const kLADistrictNeighborhoods = @"neighborhoods";
NSString *const kLADistrictCreatedAt = @"created_at";
NSString *const kLADistrictUpdatedAt = @"updated_at";
NSString *const kLADistrictName = @"name";


@interface LADistrictObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LADistrictObj

@synthesize uf = _uf;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize neighborhoods = _neighborhoods;
@synthesize createdAt = _createdAt;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.uf = [self objectOrNilForKey:kLADistrictUf fromDictionary:dict];
        self.dataIdentifier = [[self objectOrNilForKey:kLADistrictId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLADistrictDeletedAt fromDictionary:dict];
        self.neighborhoods = [self objectOrNilForKey:kLADistrictNeighborhoods fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLADistrictCreatedAt fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLADistrictUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLADistrictName fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.uf forKey:kLADistrictUf];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLADistrictId];
    [mutableDict setValue:self.deletedAt forKey:kLADistrictDeletedAt];
    NSMutableArray *tempArrayForNeighborhoods = [NSMutableArray array];
    for (NSObject *subArrayObject in self.neighborhoods) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForNeighborhoods addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForNeighborhoods addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForNeighborhoods] forKey:kLADistrictNeighborhoods];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLADistrictCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLADistrictUpdatedAt];
    [mutableDict setValue:self.name forKey:kLADistrictName];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.uf = [aDecoder decodeObjectForKey:kLADistrictUf];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLADistrictId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLADistrictDeletedAt];
    self.neighborhoods = [aDecoder decodeObjectForKey:kLADistrictNeighborhoods];
    self.createdAt = [aDecoder decodeDoubleForKey:kLADistrictCreatedAt];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLADistrictUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLADistrictName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_uf forKey:kLADistrictUf];
    [aCoder encodeDouble:_dataIdentifier forKey:kLADistrictId];
    [aCoder encodeObject:_deletedAt forKey:kLADistrictDeletedAt];
    [aCoder encodeObject:_neighborhoods forKey:kLADistrictNeighborhoods];
    [aCoder encodeDouble:_createdAt forKey:kLADistrictCreatedAt];
    [aCoder encodeDouble:_updatedAt forKey:kLADistrictUpdatedAt];
    [aCoder encodeObject:_name forKey:kLADistrictName];
}

- (id)copyWithZone:(NSZone *)zone
{
    LADistrictObj *copy = [[LADistrictObj alloc] init];
    
    if (copy) {
        
        copy.uf = [self.uf copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.neighborhoods = [self.neighborhoods copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}

- (BOOL)isEqual:(id)object
{
    return (self.dataIdentifier == ((LADistrictObj*)object).dataIdentifier);
}
@end

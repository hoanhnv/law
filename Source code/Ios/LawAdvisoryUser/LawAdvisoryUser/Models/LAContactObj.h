//
//  LAContactObj.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/22/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDictionary+Law.h"

@interface LAContactObj : NSObject
@property (nonatomic) NSInteger contactID;
@property (nonatomic) NSString* contactName;
@property (nonatomic) NSString* contactDescription;
@property (nonatomic) double contactDeleteAt;
@property (nonatomic) double contactCreateAt;
@property (nonatomic) double contactUpdateAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;
@end

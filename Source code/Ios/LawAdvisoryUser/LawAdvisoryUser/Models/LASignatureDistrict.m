//
//  LASignatureDistrict.m
//
//  Created by   on 10/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LASignatureDistrict.h"


NSString *const kLASignatureDistrictDeletedAt = @"deleted_at";
NSString *const kLASignatureDistrictId = @"id";
NSString *const kLASignatureDistrictUf = @"uf";
NSString *const kLASignatureDistrictName = @"name";
NSString *const kLASignatureDistrictUpdatedAt = @"updated_at";
NSString *const kLASignatureDistrictCreatedAt = @"created_at";


@interface LASignatureDistrict ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LASignatureDistrict

@synthesize deletedAt = _deletedAt;
@synthesize districtIdentifier = _districtIdentifier;
@synthesize uf = _uf;
@synthesize name = _name;
@synthesize updatedAt = _updatedAt;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.deletedAt = [self objectOrNilForKey:kLASignatureDistrictDeletedAt fromDictionary:dict];
            self.districtIdentifier = [[self objectOrNilForKey:kLASignatureDistrictId fromDictionary:dict] doubleValue];
            self.uf = [self objectOrNilForKey:kLASignatureDistrictUf fromDictionary:dict];
            self.name = [self objectOrNilForKey:kLASignatureDistrictName fromDictionary:dict];
            self.updatedAt = [[self objectOrNilForKey:kLASignatureDistrictUpdatedAt fromDictionary:dict] doubleValue];
            self.createdAt = [[self objectOrNilForKey:kLASignatureDistrictCreatedAt fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLASignatureDistrictDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtIdentifier] forKey:kLASignatureDistrictId];
    [mutableDict setValue:self.uf forKey:kLASignatureDistrictUf];
    [mutableDict setValue:self.name forKey:kLASignatureDistrictName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLASignatureDistrictUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLASignatureDistrictCreatedAt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.deletedAt = [aDecoder decodeObjectForKey:kLASignatureDistrictDeletedAt];
    self.districtIdentifier = [aDecoder decodeDoubleForKey:kLASignatureDistrictId];
    self.uf = [aDecoder decodeObjectForKey:kLASignatureDistrictUf];
    self.name = [aDecoder decodeObjectForKey:kLASignatureDistrictName];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLASignatureDistrictUpdatedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLASignatureDistrictCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_deletedAt forKey:kLASignatureDistrictDeletedAt];
    [aCoder encodeDouble:_districtIdentifier forKey:kLASignatureDistrictId];
    [aCoder encodeObject:_uf forKey:kLASignatureDistrictUf];
    [aCoder encodeObject:_name forKey:kLASignatureDistrictName];
    [aCoder encodeDouble:_updatedAt forKey:kLASignatureDistrictUpdatedAt];
    [aCoder encodeDouble:_createdAt forKey:kLASignatureDistrictCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LASignatureDistrict *copy = [[LASignatureDistrict alloc] init];
    
    if (copy) {

        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.districtIdentifier = self.districtIdentifier;
        copy.uf = [self.uf copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

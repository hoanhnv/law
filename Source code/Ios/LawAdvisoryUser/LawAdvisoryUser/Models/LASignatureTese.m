//
//  LASignatureTese.m
//
//  Created by   on 11/25/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LASignatureTese.h"
#import "LASignatureTeseObj.h"


NSString *const kLASignatureTeseSuccess = @"success";
NSString *const kLASignatureTeseData = @"data";
NSString *const kLASignatureTeseMessage = @"message";


@interface LASignatureTese ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LASignatureTese

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLASignatureTeseSuccess fromDictionary:dict] boolValue];
        NSObject *receivedLAData = [dict objectForKey:kLASignatureTeseData];
        NSMutableArray *parsedLAData = [NSMutableArray array];
        if ([receivedLAData isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAData) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAData addObject:[LASignatureTeseObj modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAData isKindOfClass:[NSDictionary class]]) {
            [parsedLAData addObject:[LASignatureTeseObj modelObjectWithDictionary:(NSDictionary *)receivedLAData]];
        }
        
        self.data = [NSArray arrayWithArray:parsedLAData];
        self.message = [self objectOrNilForKey:kLASignatureTeseMessage fromDictionary:dict];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLASignatureTeseSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLASignatureTeseData];
    [mutableDict setValue:self.message forKey:kLASignatureTeseMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLASignatureTeseSuccess];
    self.data = [aDecoder decodeObjectForKey:kLASignatureTeseData];
    self.message = [aDecoder decodeObjectForKey:kLASignatureTeseMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLASignatureTeseSuccess];
    [aCoder encodeObject:_data forKey:kLASignatureTeseData];
    [aCoder encodeObject:_message forKey:kLASignatureTeseMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LASignatureTese *copy = [[LASignatureTese alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

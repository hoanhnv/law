//
//  LARefusalObj.m
//
//  Created by   on 11/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LARefusalObj.h"


NSString *const kLARefusalObjDeletedAt = @"deleted_at";
NSString *const kLARefusalObjId = @"id";
NSString *const kLARefusalObjUpdatedAt = @"updated_at";
NSString *const kLARefusalObjName = @"name";
NSString *const kLARefusalObjCreatedAt = @"created_at";


@interface LARefusalObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LARefusalObj

@synthesize deletedAt = _deletedAt;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [self objectOrNilForKey:kLARefusalObjDeletedAt fromDictionary:dict];
        self.dataIdentifier = [[self objectOrNilForKey:kLARefusalObjId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLARefusalObjUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLARefusalObjName fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLARefusalObjCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLARefusalObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLARefusalObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLARefusalObjUpdatedAt];
    [mutableDict setValue:self.name forKey:kLARefusalObjName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLARefusalObjCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.deletedAt = [aDecoder decodeObjectForKey:kLARefusalObjDeletedAt];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLARefusalObjId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLARefusalObjUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLARefusalObjName];
    self.createdAt = [aDecoder decodeDoubleForKey:kLARefusalObjCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_deletedAt forKey:kLARefusalObjDeletedAt];
    [aCoder encodeDouble:_dataIdentifier forKey:kLARefusalObjId];
    [aCoder encodeDouble:_updatedAt forKey:kLARefusalObjUpdatedAt];
    [aCoder encodeObject:_name forKey:kLARefusalObjName];
    [aCoder encodeDouble:_createdAt forKey:kLARefusalObjCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LARefusalObj *copy = [[LARefusalObj alloc] init];
    
    if (copy) {
        
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

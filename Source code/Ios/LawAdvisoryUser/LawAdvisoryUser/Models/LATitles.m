//
//  LATitles.m
//
//  Created by   on 10/14/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LATitles.h"


NSString *const kLATitlesDeletedAt = @"deleted_at";
NSString *const kLATitlesId = @"id";
NSString *const kLATitlesTitle = @"title";
NSString *const kLATitlesUserId = @"user_id";
NSString *const kLATitlesUpdatedAt = @"updated_at";
NSString *const kLATitlesCreatedAt = @"created_at";


@interface LATitles ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LATitles

@synthesize deletedAt = _deletedAt;
@synthesize titlesIdentifier = _titlesIdentifier;
@synthesize title = _title;
@synthesize userId = _userId;
@synthesize updatedAt = _updatedAt;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.deletedAt = [self objectOrNilForKey:kLATitlesDeletedAt fromDictionary:dict];
            self.titlesIdentifier = [[self objectOrNilForKey:kLATitlesId fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kLATitlesTitle fromDictionary:dict];
            self.userId = [[self objectOrNilForKey:kLATitlesUserId fromDictionary:dict] doubleValue];
            self.updatedAt = [[self objectOrNilForKey:kLATitlesUpdatedAt fromDictionary:dict] doubleValue];
            self.createdAt = [[self objectOrNilForKey:kLATitlesCreatedAt fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLATitlesDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.titlesIdentifier] forKey:kLATitlesId];
    [mutableDict setValue:self.title forKey:kLATitlesTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLATitlesUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLATitlesUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLATitlesCreatedAt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.deletedAt = [aDecoder decodeObjectForKey:kLATitlesDeletedAt];
    self.titlesIdentifier = [aDecoder decodeDoubleForKey:kLATitlesId];
    self.title = [aDecoder decodeObjectForKey:kLATitlesTitle];
    self.userId = [aDecoder decodeDoubleForKey:kLATitlesUserId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLATitlesUpdatedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLATitlesCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_deletedAt forKey:kLATitlesDeletedAt];
    [aCoder encodeDouble:_titlesIdentifier forKey:kLATitlesId];
    [aCoder encodeObject:_title forKey:kLATitlesTitle];
    [aCoder encodeDouble:_userId forKey:kLATitlesUserId];
    [aCoder encodeDouble:_updatedAt forKey:kLATitlesUpdatedAt];
    [aCoder encodeDouble:_createdAt forKey:kLATitlesCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LATitles *copy = [[LATitles alloc] init];
    
    if (copy) {

        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.titlesIdentifier = self.titlesIdentifier;
        copy.title = [self.title copyWithZone:zone];
        copy.userId = self.userId;
        copy.updatedAt = self.updatedAt;
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

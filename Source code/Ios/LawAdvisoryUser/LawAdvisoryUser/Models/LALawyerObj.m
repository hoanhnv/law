//
//  LAData.m
//
//  Created by   on 10/11/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawyerObj.h"
#import "LALawyer.h"
#import "LAViewPhone.h"
#import "LATitles.h"

NSString *const kLADataPostalCode = @"postal_code";
NSString *const kLADataStatus = @"status";
NSString *const _kLADataUpdatedAt = @"updated_at";
NSString *const kLADataDateOfBirth = @"date_of_birth";
NSString *const kLADataCellphone = @"cellphone";
NSString *const kLADataCity = @"city";
NSString *const _kLADataName = @"name";
NSString *const kLADataLawyer = @"lawyer";
NSString *const _kLADataId = @"id";
NSString *const kLADataComplement = @"complement";
NSString *const _kLADataDeletedAt = @"deleted_at";
NSString *const kLADataNeighborhood = @"neighborhood";
NSString *const kLADataNumber = @"number";
NSString *const kLADataPhone = @"phone";
NSString *const kLADataEmail = @"email";
NSString *const kLADataViewPhone = @"view_phone";
NSString *const kLADataImageRg = @"image_rg";
NSString *const _kLADataCreatedAt = @"created_at";
NSString *const kLADataCpf = @"cpf";
NSString *const kLADataUf = @"uf";
NSString *const kLADataAddress = @"address";
NSString *const kLADataAcceptsNews = @"accepts_news";
NSString *const kLADataTitles = @"titles";

@interface LALawyerObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawyerObj

@synthesize postalCode = _postalCode;
@synthesize status = _status;
@synthesize updatedAt = _updatedAt;
@synthesize dateOfBirth = _dateOfBirth;
@synthesize cellphone = _cellphone;
@synthesize city = _city;
@synthesize name = _name;
@synthesize lawyer = _lawyer;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize complement = _complement;
@synthesize deletedAt = _deletedAt;
@synthesize neighborhood = _neighborhood;
@synthesize number = _number;
@synthesize phone = _phone;
@synthesize email = _email;
@synthesize viewPhone = _viewPhone;
@synthesize imageRg = _imageRg;
@synthesize createdAt = _createdAt;
@synthesize cpf = _cpf;
@synthesize uf = _uf;
@synthesize address = _address;
@synthesize acceptsNews = _acceptsNews;
@synthesize titles = _titles;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.postalCode = [self objectOrNilForKey:kLADataPostalCode fromDictionary:dict];
        self.status = [self objectOrNilForKey:kLADataStatus fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:_kLADataUpdatedAt fromDictionary:dict] doubleValue];
        self.dateOfBirth = [[self objectOrNilForKey:kLADataDateOfBirth fromDictionary:dict] doubleValue] ;
        self.cellphone = [self objectOrNilForKey:kLADataCellphone fromDictionary:dict];
        self.city = [self objectOrNilForKey:kLADataCity fromDictionary:dict];
        self.name = [self objectOrNilForKey:_kLADataName fromDictionary:dict];
        self.lawyer = [LALawyer modelObjectWithDictionary:[dict objectForKey:kLADataLawyer]];
        self.dataIdentifier = [[self objectOrNilForKey:_kLADataId fromDictionary:dict] doubleValue];
        self.complement = [self objectOrNilForKey:kLADataComplement fromDictionary:dict];
        self.deletedAt = [[self objectOrNilForKey:_kLADataDeletedAt fromDictionary:dict] doubleValue];
        self.neighborhood = [self objectOrNilForKey:kLADataNeighborhood fromDictionary:dict];
        self.number = [self objectOrNilForKey:kLADataNumber fromDictionary:dict];
        self.phone = [self objectOrNilForKey:kLADataPhone fromDictionary:dict];
        self.email = [self objectOrNilForKey:kLADataEmail fromDictionary:dict];
        self.viewPhone = [LAViewPhone modelObjectWithDictionary:[dict objectForKey:kLADataViewPhone]];
        self.imageRg = [self objectOrNilForKey:kLADataImageRg fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:_kLADataCreatedAt fromDictionary:dict] doubleValue];
        self.cpf = [self objectOrNilForKey:kLADataCpf fromDictionary:dict];
        self.uf = [self objectOrNilForKey:kLADataUf fromDictionary:dict];
        self.address = [self objectOrNilForKey:kLADataAddress fromDictionary:dict];
        self.acceptsNews = [self objectOrNilForKey:kLADataAcceptsNews fromDictionary:dict];
        
        NSObject *receivedLATitles = [dict objectForKey:kLADataTitles];
        NSMutableArray *parsedLATitles = [NSMutableArray array];
        if ([receivedLATitles isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLATitles) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLATitles addObject:[LATitles modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLATitles isKindOfClass:[NSDictionary class]]) {
            [parsedLATitles addObject:[LATitles modelObjectWithDictionary:(NSDictionary *)receivedLATitles]];
        }
        
        self.titles = [NSArray arrayWithArray:parsedLATitles];
        
        self.bank_id = [[self objectOrNilForKey:@"bank_id" fromDictionary:dict]doubleValue];
        self.bank_agency = [self objectOrNilForKey:@"bank_agency" fromDictionary:dict];
        self.bank_account_dac = [self objectOrNilForKey:@"bank_account_dac" fromDictionary:dict];
        self.bank_account = [self objectOrNilForKey:@"bank_account" fromDictionary:dict];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.postalCode forKey:kLADataPostalCode];
    [mutableDict setValue:self.status forKey:kLADataStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:_kLADataUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble :self.dateOfBirth] forKey:kLADataDateOfBirth];
    [mutableDict setValue:self.cellphone forKey:kLADataCellphone];
    [mutableDict setValue:self.city forKey:kLADataCity];
    [mutableDict setValue:self.name forKey:_kLADataName];
    [mutableDict setValue:[self.lawyer dictionaryRepresentation] forKey:kLADataLawyer];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:_kLADataId];
    [mutableDict setValue:self.complement forKey:kLADataComplement];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deletedAt] forKey:_kLADataDeletedAt];
    [mutableDict setValue:self.neighborhood forKey:kLADataNeighborhood];
    [mutableDict setValue:self.number forKey:kLADataNumber];
    [mutableDict setValue:self.phone forKey:kLADataPhone];
    [mutableDict setValue:self.email forKey:kLADataEmail];
    [mutableDict setValue:[self.viewPhone dictionaryRepresentation] forKey:kLADataViewPhone];
    [mutableDict setValue:self.imageRg forKey:kLADataImageRg];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:_kLADataCreatedAt];
    [mutableDict setValue:self.cpf forKey:kLADataCpf];
    [mutableDict setValue:self.uf forKey:kLADataUf];
    [mutableDict setValue:self.address forKey:kLADataAddress];
    [mutableDict setValue:self.acceptsNews forKey:kLADataAcceptsNews];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.postalCode = [aDecoder decodeObjectForKey:kLADataPostalCode];
    self.status = [aDecoder decodeObjectForKey:kLADataStatus];
    self.updatedAt = [aDecoder decodeDoubleForKey:_kLADataUpdatedAt];
    self.dateOfBirth = [aDecoder decodeDoubleForKey:kLADataDateOfBirth];
    self.cellphone = [aDecoder decodeObjectForKey:kLADataCellphone];
    self.city = [aDecoder decodeObjectForKey:kLADataCity];
    self.name = [aDecoder decodeObjectForKey:_kLADataName];
    self.lawyer = [aDecoder decodeObjectForKey:kLADataLawyer];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:_kLADataId];
    self.complement = [aDecoder decodeObjectForKey:kLADataComplement];
    //    self.deletedAt = [aDecoder decodeObjectForKey:_kLADataDeletedAt];
    self.neighborhood = [aDecoder decodeObjectForKey:kLADataNeighborhood];
    self.number = [aDecoder decodeObjectForKey:kLADataNumber];
    self.phone = [aDecoder decodeObjectForKey:kLADataPhone];
    self.email = [aDecoder decodeObjectForKey:kLADataEmail];
    self.viewPhone = [aDecoder decodeObjectForKey:kLADataViewPhone];
    self.imageRg = [aDecoder decodeObjectForKey:kLADataImageRg];
    self.createdAt = [aDecoder decodeDoubleForKey:_kLADataCreatedAt];
    self.cpf = [aDecoder decodeObjectForKey:kLADataCpf];
    self.uf = [aDecoder decodeObjectForKey:kLADataUf];
    self.address = [aDecoder decodeObjectForKey:kLADataAddress];
    self.acceptsNews = [aDecoder decodeObjectForKey:kLADataAcceptsNews];
    self.titles = [aDecoder decodeObjectForKey:kLADataTitles];
    self.bank_id = [aDecoder decodeDoubleForKey:@"bank_id"];
    self.bank_agency = [aDecoder decodeObjectForKey:@"bank_agency"];
    self.bank_account = [aDecoder decodeObjectForKey:@"bank_account"];
    self.bank_account_dac = [aDecoder decodeObjectForKey:@"bank_account_dac"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_postalCode forKey:kLADataPostalCode];
    [aCoder encodeObject:_status forKey:kLADataStatus];
    [aCoder encodeDouble:_updatedAt forKey:_kLADataUpdatedAt];
    [aCoder encodeDouble:_dateOfBirth forKey:kLADataDateOfBirth];
    [aCoder encodeObject:_cellphone forKey:kLADataCellphone];
    [aCoder encodeObject:_city forKey:kLADataCity];
    [aCoder encodeObject:_name forKey:_kLADataName];
    [aCoder encodeObject:_lawyer forKey:kLADataLawyer];
    [aCoder encodeDouble:_dataIdentifier forKey:_kLADataId];
    [aCoder encodeObject:_complement forKey:kLADataComplement];
    //    [aCoder encodeObject:_deletedAt forKey:_kLADataDeletedAt];
    [aCoder encodeObject:_neighborhood forKey:kLADataNeighborhood];
    [aCoder encodeObject:_number forKey:kLADataNumber];
    [aCoder encodeObject:_phone forKey:kLADataPhone];
    [aCoder encodeObject:_email forKey:kLADataEmail];
    [aCoder encodeObject:_viewPhone forKey:kLADataViewPhone];
    [aCoder encodeObject:_imageRg forKey:kLADataImageRg];
    [aCoder encodeDouble:_createdAt forKey:_kLADataCreatedAt];
    [aCoder encodeObject:_cpf forKey:kLADataCpf];
    [aCoder encodeObject:_uf forKey:kLADataUf];
    [aCoder encodeObject:_address forKey:kLADataAddress];
    [aCoder encodeObject:_acceptsNews forKey:kLADataAcceptsNews];
    [aCoder encodeObject:_titles forKey:kLADataTitles];
    [aCoder encodeDouble:_bank_id forKey:@"bank_id"];
    [aCoder encodeObject:_bank_agency forKey:@"bank_agency"];
    [aCoder encodeObject:_bank_account forKey:@"bank_account"];
    [aCoder encodeObject:_bank_account_dac forKey:@"bank_account_dac"];
    
    
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawyerObj *copy = [[LALawyerObj alloc] init];
    
    if (copy) {
        
        copy.postalCode = [self.postalCode copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.dateOfBirth = self.dateOfBirth ;
        copy.cellphone = [self.cellphone copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.lawyer = [self.lawyer copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.complement = [self.complement copyWithZone:zone];
        //        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.neighborhood = [self.neighborhood copyWithZone:zone];
        copy.number = [self.number copyWithZone:zone];
        copy.phone = [self.phone copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.viewPhone = [self.viewPhone copyWithZone:zone];
        copy.imageRg = [self.imageRg copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.cpf = [self.cpf copyWithZone:zone];
        copy.uf = [self.uf copyWithZone:zone];
        copy.address = [self.address copyWithZone:zone];
        copy.acceptsNews = [self.acceptsNews copyWithZone:zone];
    }
    
    return copy;
}


@end

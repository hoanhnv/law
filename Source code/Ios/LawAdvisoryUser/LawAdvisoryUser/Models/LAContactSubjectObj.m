//
//  LAContactSubjectObj.m
//
//  Created by   on 9/23/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAContactSubjectObj.h"


NSString *const kLAContactSubjectObjDeletedAt = @"deleted_at";
NSString *const kLAContactSubjectObjId = @"id";
NSString *const kLAContactSubjectObjUpdatedAt = @"updated_at";
NSString *const kLAContactSubjectObjName = @"name";
NSString *const kLAContactSubjectObjDescription = @"description";
NSString *const kLAContactSubjectObjCreatedAt = @"created_at";


@interface LAContactSubjectObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAContactSubjectObj

@synthesize deletedAt = _deletedAt;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize dataDescription = _dataDescription;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [[self objectOrNilForKey:kLAContactSubjectObjDeletedAt fromDictionary:dict] doubleValue];
        self.dataIdentifier = [[self objectOrNilForKey:kLAContactSubjectObjId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLAContactSubjectObjUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLAContactSubjectObjName fromDictionary:dict];
        self.dataDescription = [self objectOrNilForKey:kLAContactSubjectObjDescription fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAContactSubjectObjCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    //    [mutableDict setValue:self.deletedAt forKey:kLAContactSubjectObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLAContactSubjectObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAContactSubjectObjUpdatedAt];
    [mutableDict setValue:self.name forKey:kLAContactSubjectObjName];
    [mutableDict setValue:self.dataDescription forKey:kLAContactSubjectObjDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAContactSubjectObjCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    //    self.deletedAt = [aDecoder decodeObjectForKey:kLAContactSubjectObjDeletedAt];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAContactSubjectObjId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAContactSubjectObjUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLAContactSubjectObjName];
    self.dataDescription = [aDecoder decodeObjectForKey:kLAContactSubjectObjDescription];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAContactSubjectObjCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    //    [aCoder encodeObject:_deletedAt forKey:kLAContactSubjectObjDeletedAt];
    [aCoder encodeDouble:_dataIdentifier forKey:kLAContactSubjectObjId];
    [aCoder encodeDouble:_updatedAt forKey:kLAContactSubjectObjUpdatedAt];
    [aCoder encodeObject:_name forKey:kLAContactSubjectObjName];
    [aCoder encodeObject:_dataDescription forKey:kLAContactSubjectObjDescription];
    [aCoder encodeDouble:_createdAt forKey:kLAContactSubjectObjCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAContactSubjectObj *copy = [[LAContactSubjectObj alloc] init];
    
    if (copy) {
        
        //        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

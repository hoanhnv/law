//
//  LABankObj.m
//
//  Created by   on 12/17/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LABankObj.h"


NSString *const kLABankObjDeletedAt = @"deleted_at";
NSString *const kLABankObjId = @"id";
NSString *const kLABankObjCode = @"code";
NSString *const kLABankObjUpdatedAt = @"updated_at";
NSString *const kLABankObjName = @"name";
NSString *const kLABankObjCreatedAt = @"created_at";


@interface LABankObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LABankObj

@synthesize deletedAt = _deletedAt;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize code = _code;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [self objectOrNilForKey:kLABankObjDeletedAt fromDictionary:dict];
        self.dataIdentifier = [[self objectOrNilForKey:kLABankObjId fromDictionary:dict] doubleValue];
        self.code = [self objectOrNilForKey:kLABankObjCode fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLABankObjUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLABankObjName fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLABankObjCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLABankObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLABankObjId];
    [mutableDict setValue:self.code forKey:kLABankObjCode];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLABankObjUpdatedAt];
    [mutableDict setValue:self.name forKey:kLABankObjName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLABankObjCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.deletedAt = [aDecoder decodeObjectForKey:kLABankObjDeletedAt];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLABankObjId];
    self.code = [aDecoder decodeObjectForKey:kLABankObjCode];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLABankObjUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLABankObjName];
    self.createdAt = [aDecoder decodeDoubleForKey:kLABankObjCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_deletedAt forKey:kLABankObjDeletedAt];
    [aCoder encodeDouble:_dataIdentifier forKey:kLABankObjId];
    [aCoder encodeObject:_code forKey:kLABankObjCode];
    [aCoder encodeDouble:_updatedAt forKey:kLABankObjUpdatedAt];
    [aCoder encodeObject:_name forKey:kLABankObjName];
    [aCoder encodeDouble:_createdAt forKey:kLABankObjCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LABankObj *copy = [[LABankObj alloc] init];
    
    if (copy) {
        
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.code = [self.code copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

//
//  LASignatureInfo.m
//
//  Created by   on 10/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LASignatureInfo.h"
#import "LASignatureObj.h"


NSString *const kLASignatureInfoSuccess = @"success";
NSString *const kLASignatureInfoData = @"data";
NSString *const kLASignatureInfoMessage = @"message";


@interface LASignatureInfo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LASignatureInfo

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLASignatureInfoSuccess fromDictionary:dict] boolValue];
        self.data = [LASignatureObj modelObjectWithDictionary:[dict objectForKey:kLASignatureInfoData]];
        self.message = [self objectOrNilForKey:kLASignatureInfoMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLASignatureInfoSuccess];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kLASignatureInfoData];
    [mutableDict setValue:self.message forKey:kLASignatureInfoMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLASignatureInfoSuccess];
    self.data = [aDecoder decodeObjectForKey:kLASignatureInfoData];
    self.message = [aDecoder decodeObjectForKey:kLASignatureInfoMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLASignatureInfoSuccess];
    [aCoder encodeObject:_data forKey:kLASignatureInfoData];
    [aCoder encodeObject:_message forKey:kLASignatureInfoMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LASignatureInfo *copy = [[LASignatureInfo alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LAData.h
//
//  Created by   on 10/11/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LALawyer, LAViewPhone;

@interface LALawyerObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double dateOfBirth;
@property (nonatomic, strong) NSString *cellphone;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) LALawyer *lawyer;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, strong) NSString *complement;
@property (nonatomic, assign) double deletedAt;
@property (nonatomic, strong) NSString *neighborhood;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) LAViewPhone *viewPhone;
@property (nonatomic, strong) NSString* imageRg;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *cpf;
@property (nonatomic, strong) NSString *uf;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *acceptsNews;
@property (nonatomic, strong) NSArray *titles;

@property (nonatomic, assign) double bank_id;
@property (nonatomic, strong) NSString *bank_agency;
@property (nonatomic, strong) NSString *bank_account;
@property (nonatomic, strong) NSString *bank_account_dac;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

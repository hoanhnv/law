//
//  LALawyer.h
//
//  Created by   on 10/11/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LALawyer : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double lawyerIdentifier;
@property (nonatomic, strong) NSString *ufOab;
@property (nonatomic, strong) NSString *groundForRefusal;
@property (nonatomic, strong) NSString *linkedin;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *imageOab;
@property (nonatomic, assign) double userId;
@property (nonatomic, assign) double deletedAt;
@property (nonatomic, strong) NSString *about;
@property (nonatomic, strong) NSString *imageHouseProof;
@property (nonatomic, strong) NSString* imageAvatar;
@property (nonatomic, strong) NSString *imageOabWithUrl;
@property (nonatomic, strong) NSString  *imageOabVerse;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *numberOab;
@property (nonatomic, strong) NSString *site;
@property (nonatomic, strong) NSString* imageAvatarWithUrl;
@property (nonatomic, strong) NSString *imageHouseProofWithUrl;
@property (nonatomic, strong) NSString *imageOabVerseWithUrl;
@property (nonatomic, strong) NSString *bank_account;
@property (nonatomic, strong) NSString *bank_account_dac;
@property (nonatomic, strong) NSString *bank_agency;
@property (nonatomic, assign) double bank_id;
@property (nonatomic, assign) double rating;
@property (nonatomic, assign) double response_time;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LABannerObj.m
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LABannerObj.h"
#import "LACategory.h"
#import "LAThesisBannerObj.h"


NSString *const kLABannerObjCategory = @"category";
NSString *const kLABannerObjImageWithUrl = @"image_with_url";
NSString *const kLABannerObjPosition = @"position";
NSString *const kLABannerObjId = @"id";
NSString *const kLABannerObjDeletedAt = @"deleted_at";
NSString *const kLABannerObjCategoryId = @"category_id";
NSString *const kLABannerObjImage = @"image";
NSString *const kLABannerObjCreatedAt = @"created_at";
NSString *const kLABannerObjDescription = @"description";
NSString *const kLABannerObjUpdatedAt = @"updated_at";
NSString *const kLADataThesis = @"thesis";
NSString *const kLADataThesisId = @"thesis_id";

@interface LABannerObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LABannerObj

@synthesize category = _category;
@synthesize imageWithUrl = _imageWithUrl;
@synthesize position = _position;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize categoryId = _categoryId;
@synthesize image = _image;
@synthesize createdAt = _createdAt;
@synthesize dataDescription = _dataDescription;
@synthesize updatedAt = _updatedAt;
@synthesize thesis = _thesis;
@synthesize thesisId = _thesisId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.category = [LACategory modelObjectWithDictionary:[dict objectForKey:kLABannerObjCategory]];
        self.imageWithUrl = [self objectOrNilForKey:kLABannerObjImageWithUrl fromDictionary:dict];
        self.position = [[self objectOrNilForKey:kLABannerObjPosition fromDictionary:dict] doubleValue];
        self.dataIdentifier = [[self objectOrNilForKey:kLABannerObjId fromDictionary:dict] doubleValue];
        self.deletedAt = [[self objectOrNilForKey:kLABannerObjDeletedAt fromDictionary:dict] doubleValue];
        self.categoryId = [[self objectOrNilForKey:kLABannerObjCategoryId fromDictionary:dict] doubleValue];
        self.image = [self objectOrNilForKey:kLABannerObjImage fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLABannerObjCreatedAt fromDictionary:dict] doubleValue];
        self.dataDescription = [self objectOrNilForKey:kLABannerObjDescription fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLABannerObjUpdatedAt fromDictionary:dict] doubleValue];
        self.thesis = [LAThesisBannerObj modelObjectWithDictionary:[dict objectForKey:kLADataThesis]];
        self.thesisId = [[self objectOrNilForKey:kLADataThesisId fromDictionary:dict] doubleValue];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.category dictionaryRepresentation] forKey:kLABannerObjCategory];
    [mutableDict setValue:self.imageWithUrl forKey:kLABannerObjImageWithUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.position] forKey:kLABannerObjPosition];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLABannerObjId];
    //    [mutableDict setValue:self.deletedAt forKey:kLABannerObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryId] forKey:kLABannerObjCategoryId];
    [mutableDict setValue:self.image forKey:kLABannerObjImage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLABannerObjCreatedAt];
    [mutableDict setValue:self.dataDescription forKey:kLABannerObjDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLABannerObjUpdatedAt];
    [mutableDict setValue:[self.thesis dictionaryRepresentation] forKey:kLADataThesis];
    [mutableDict setValue:[NSNumber numberWithDouble:self.thesisId] forKey:kLADataThesisId];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.category = [aDecoder decodeObjectForKey:kLABannerObjCategory];
    self.imageWithUrl = [aDecoder decodeObjectForKey:kLABannerObjImageWithUrl];
    self.position = [aDecoder decodeDoubleForKey:kLABannerObjPosition];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLABannerObjId];
    //    self.deletedAt = [aDecoder decodeObjectForKey:kLABannerObjDeletedAt];
    self.categoryId = [aDecoder decodeDoubleForKey:kLABannerObjCategoryId];
    self.image = [aDecoder decodeObjectForKey:kLABannerObjImage];
    self.createdAt = [aDecoder decodeDoubleForKey:kLABannerObjCreatedAt];
    self.dataDescription = [aDecoder decodeObjectForKey:kLABannerObjDescription];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLABannerObjUpdatedAt];
    self.thesis = [aDecoder decodeObjectForKey:kLADataThesis];
    self.thesisId = [aDecoder decodeDoubleForKey:kLADataThesisId];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_category forKey:kLABannerObjCategory];
    [aCoder encodeObject:_imageWithUrl forKey:kLABannerObjImageWithUrl];
    [aCoder encodeDouble:_position forKey:kLABannerObjPosition];
    [aCoder encodeDouble:_dataIdentifier forKey:kLABannerObjId];
    //    [aCoder encodeObject:_deletedAt forKey:kLABannerObjDeletedAt];
    [aCoder encodeDouble:_categoryId forKey:kLABannerObjCategoryId];
    [aCoder encodeObject:_image forKey:kLABannerObjImage];
    [aCoder encodeDouble:_createdAt forKey:kLABannerObjCreatedAt];
    [aCoder encodeObject:_dataDescription forKey:kLABannerObjDescription];
    [aCoder encodeDouble:_updatedAt forKey:kLABannerObjUpdatedAt];
    [aCoder encodeObject:_thesis forKey:kLADataThesis];
    [aCoder encodeDouble:_thesisId forKey:kLADataThesisId];
}

- (id)copyWithZone:(NSZone *)zone
{
    LABannerObj *copy = [[LABannerObj alloc] init];
    
    if (copy) {
        
        copy.category = [self.category copyWithZone:zone];
        copy.imageWithUrl = [self.imageWithUrl copyWithZone:zone];
        copy.position = self.position;
        copy.dataIdentifier = self.dataIdentifier;
        //        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.categoryId = self.categoryId;
        copy.image = [self.image copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.thesis = [self.thesis copyWithZone:zone];
        copy.thesisId = self.thesisId;
    }
    
    return copy;
}


@end

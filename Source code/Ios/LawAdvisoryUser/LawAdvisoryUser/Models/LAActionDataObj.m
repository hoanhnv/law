//
//  LAActionDataObj.m
//
//  Created by   on 11/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAActionDataObj.h"


NSString *const kLAActionDataObjId = @"id";
NSString *const kLAActionDataObjHappeningId = @"happening_id";
NSString *const kLAActionDataObjLawyerId = @"lawyer_id";
NSString *const kLAActionDataObjCreatedAt = @"created_at";
NSString *const kLAActionDataObjUserId = @"user_id";
NSString *const kLAActionDataObjDeletedAt = @"deleted_at";
NSString *const kLAActionDataObjRefusalId = @"refusal_id";
NSString *const kLAActionDataObjRefusalDescription = @"refusal_description";
NSString *const kLAActionDataObjUpdatedAt = @"updated_at";
NSString *const kLAActionDataObjStatusDescription = @"status_description";
NSString *const kLAActionDataObjStatus = @"status";
NSString *const kLAActionDataObjProcessNumber = @"process_number";


@interface LAActionDataObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAActionDataObj

@synthesize dataIdentifier = _dataIdentifier;
@synthesize happeningId = _happeningId;
@synthesize lawyerId = _lawyerId;
@synthesize createdAt = _createdAt;
@synthesize userId = _userId;
@synthesize deletedAt = _deletedAt;
@synthesize refusalId = _refusalId;
@synthesize refusalDescription = _refusalDescription;
@synthesize updatedAt = _updatedAt;
@synthesize statusDescription = _statusDescription;
@synthesize status = _status;
@synthesize processNumber = _processNumber;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.dataIdentifier = [[self objectOrNilForKey:kLAActionDataObjId fromDictionary:dict] doubleValue];
        self.happeningId = [[self objectOrNilForKey:kLAActionDataObjHappeningId fromDictionary:dict] doubleValue];
        self.lawyerId = [[self objectOrNilForKey:kLAActionDataObjLawyerId fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLAActionDataObjCreatedAt fromDictionary:dict] doubleValue];
        self.userId = [[self objectOrNilForKey:kLAActionDataObjUserId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLAActionDataObjDeletedAt fromDictionary:dict];
        self.refusalId = [[self objectOrNilForKey:kLAActionDataObjRefusalId fromDictionary:dict] doubleValue];
        self.refusalDescription = [self objectOrNilForKey:kLAActionDataObjRefusalDescription fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLAActionDataObjUpdatedAt fromDictionary:dict] doubleValue];
        self.statusDescription = [self objectOrNilForKey:kLAActionDataObjStatusDescription fromDictionary:dict];
        self.status = [self objectOrNilForKey:kLAActionDataObjStatus fromDictionary:dict];
        self.processNumber = [self objectOrNilForKey:kLAActionDataObjProcessNumber fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLAActionDataObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.happeningId] forKey:kLAActionDataObjHappeningId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerId] forKey:kLAActionDataObjLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAActionDataObjCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLAActionDataObjUserId];
    [mutableDict setValue:self.deletedAt forKey:kLAActionDataObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.refusalId] forKey:kLAActionDataObjRefusalId];
    [mutableDict setValue:self.refusalDescription forKey:kLAActionDataObjRefusalDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAActionDataObjUpdatedAt];
    [mutableDict setValue:self.statusDescription forKey:kLAActionDataObjStatusDescription];
    [mutableDict setValue:self.status forKey:kLAActionDataObjStatus];
    [mutableDict setValue:self.processNumber forKey:kLAActionDataObjProcessNumber];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAActionDataObjId];
    self.happeningId = [aDecoder decodeDoubleForKey:kLAActionDataObjHappeningId];
    self.lawyerId = [aDecoder decodeDoubleForKey:kLAActionDataObjLawyerId];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAActionDataObjCreatedAt];
    self.userId = [aDecoder decodeDoubleForKey:kLAActionDataObjUserId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAActionDataObjDeletedAt];
    self.refusalId = [aDecoder decodeDoubleForKey:kLAActionDataObjRefusalId];
    self.refusalDescription = [aDecoder decodeObjectForKey:kLAActionDataObjRefusalDescription];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAActionDataObjUpdatedAt];
    self.statusDescription = [aDecoder decodeObjectForKey:kLAActionDataObjStatusDescription];
    self.status = [aDecoder decodeObjectForKey:kLAActionDataObjStatus];
    self.processNumber = [aDecoder decodeObjectForKey:kLAActionDataObjProcessNumber];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_dataIdentifier forKey:kLAActionDataObjId];
    [aCoder encodeDouble:_happeningId forKey:kLAActionDataObjHappeningId];
    [aCoder encodeDouble:_lawyerId forKey:kLAActionDataObjLawyerId];
    [aCoder encodeDouble:_createdAt forKey:kLAActionDataObjCreatedAt];
    [aCoder encodeDouble:_userId forKey:kLAActionDataObjUserId];
    [aCoder encodeObject:_deletedAt forKey:kLAActionDataObjDeletedAt];
    [aCoder encodeDouble:_refusalId forKey:kLAActionDataObjRefusalId];
    [aCoder encodeObject:_refusalDescription forKey:kLAActionDataObjRefusalDescription];
    [aCoder encodeDouble:_updatedAt forKey:kLAActionDataObjUpdatedAt];
    [aCoder encodeObject:_statusDescription forKey:kLAActionDataObjStatusDescription];
    [aCoder encodeObject:_status forKey:kLAActionDataObjStatus];
    [aCoder encodeObject:_processNumber forKey:kLAActionDataObjProcessNumber];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAActionDataObj *copy = [[LAActionDataObj alloc] init];
    
    if (copy) {
        
        copy.dataIdentifier = self.dataIdentifier;
        copy.happeningId = self.happeningId;
        copy.lawyerId = self.lawyerId;
        copy.createdAt = self.createdAt;
        copy.userId = self.userId;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.refusalId = self.refusalId;
        copy.refusalDescription = [self.refusalDescription copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.statusDescription = [self.statusDescription copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.processNumber = [self.processNumber copyWithZone:zone];
    }
    
    return copy;
}


@end

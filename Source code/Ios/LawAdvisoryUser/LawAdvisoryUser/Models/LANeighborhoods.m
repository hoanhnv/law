//
//  LANeighborhoods.m
//
//  Created by   on 10/15/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LANeighborhoods.h"


NSString *const kLANeighborhoodsDistrictId = @"district_id";
NSString *const kLANeighborhoodsId = @"id";
NSString *const kLANeighborhoodsDeletedAt = @"deleted_at";
NSString *const kLANeighborhoodsUpdatedAt = @"updated_at";
NSString *const kLANeighborhoodsName = @"name";
NSString *const kLANeighborhoodsCreatedAt = @"created_at";


@interface LANeighborhoods ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LANeighborhoods

@synthesize districtId = _districtId;
@synthesize neighborhoodsIdentifier = _neighborhoodsIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.districtId = [[self objectOrNilForKey:kLANeighborhoodsDistrictId fromDictionary:dict] doubleValue];
            self.neighborhoodsIdentifier = [[self objectOrNilForKey:kLANeighborhoodsId fromDictionary:dict] doubleValue];
            self.deletedAt = [self objectOrNilForKey:kLANeighborhoodsDeletedAt fromDictionary:dict];
            self.updatedAt = [[self objectOrNilForKey:kLANeighborhoodsUpdatedAt fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kLANeighborhoodsName fromDictionary:dict];
            self.createdAt = [[self objectOrNilForKey:kLANeighborhoodsCreatedAt fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtId] forKey:kLANeighborhoodsDistrictId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.neighborhoodsIdentifier] forKey:kLANeighborhoodsId];
    [mutableDict setValue:self.deletedAt forKey:kLANeighborhoodsDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLANeighborhoodsUpdatedAt];
    [mutableDict setValue:self.name forKey:kLANeighborhoodsName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLANeighborhoodsCreatedAt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.districtId = [aDecoder decodeDoubleForKey:kLANeighborhoodsDistrictId];
    self.neighborhoodsIdentifier = [aDecoder decodeDoubleForKey:kLANeighborhoodsId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLANeighborhoodsDeletedAt];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLANeighborhoodsUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLANeighborhoodsName];
    self.createdAt = [aDecoder decodeDoubleForKey:kLANeighborhoodsCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_districtId forKey:kLANeighborhoodsDistrictId];
    [aCoder encodeDouble:_neighborhoodsIdentifier forKey:kLANeighborhoodsId];
    [aCoder encodeObject:_deletedAt forKey:kLANeighborhoodsDeletedAt];
    [aCoder encodeDouble:_updatedAt forKey:kLANeighborhoodsUpdatedAt];
    [aCoder encodeObject:_name forKey:kLANeighborhoodsName];
    [aCoder encodeDouble:_createdAt forKey:kLANeighborhoodsCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LANeighborhoods *copy = [[LANeighborhoods alloc] init];
    
    if (copy) {

        copy.districtId = self.districtId;
        copy.neighborhoodsIdentifier = self.neighborhoodsIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

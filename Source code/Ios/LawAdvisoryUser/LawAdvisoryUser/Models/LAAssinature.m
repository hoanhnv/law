//
//  LAAssinature.m
//
//  Created by   on 11/9/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAAssinature.h"
#import "LAAssinatureObj.h"


NSString *const kLAAssinatureSuccess = @"success";
NSString *const kLAAssinatureData = @"data";
NSString *const kLAAssinatureMessage = @"message";


@interface LAAssinature ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAAssinature

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAAssinatureSuccess fromDictionary:dict] boolValue];
        self.data = [LAAssinatureObj modelObjectWithDictionary:[dict objectForKey:kLAAssinatureData]];
        self.message = [self objectOrNilForKey:kLAAssinatureMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAAssinatureSuccess];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kLAAssinatureData];
    [mutableDict setValue:self.message forKey:kLAAssinatureMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAAssinatureSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAAssinatureData];
    self.message = [aDecoder decodeObjectForKey:kLAAssinatureMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAAssinatureSuccess];
    [aCoder encodeObject:_data forKey:kLAAssinatureData];
    [aCoder encodeObject:_message forKey:kLAAssinatureMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAAssinature *copy = [[LAAssinature alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

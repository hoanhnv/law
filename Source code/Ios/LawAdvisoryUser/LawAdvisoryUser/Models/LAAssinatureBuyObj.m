//
//  LAAssinatureBuyObj.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAAssinatureBuyObj.h"
#import "LADistricts.h"
#import "LAThesisBuy.h"
#import "LAPlanObj.h"


NSString *const kLAAssinatureBuyObjId = @"id";
NSString *const kLAAssinatureBuyObjPlanId = @"plan_id";
NSString *const kLAAssinatureBuyObjValuePlanOnSigning = @"value_plan_on_signing";
NSString *const kLAAssinatureBuyObjValuePerExtraDistrict = @"value_per_extra_district";
NSString *const kLAAssinatureBuyObjTotalPlanOnSigning = @"total_plan_on_signing";
NSString *const kLAAssinatureBuyObjLawyerId = @"lawyer_id";
NSString *const kLAAssinatureBuyObjTotalDistrictsSigning = @"total_districts_signing";
NSString *const kLAAssinatureBuyObjPaymentMethod = @"payment_method";
NSString *const kLAAssinatureBuyObjVindiId = @"vindi_id";
NSString *const kLAAssinatureBuyObjCreatedAt = @"created_at";
NSString *const kLAAssinatureBuyObjDeletedAt = @"deleted_at";
NSString *const kLAAssinatureBuyObjDistricts = @"districts";
NSString *const kLAAssinatureBuyObjUpdatedAt = @"updated_at";
NSString *const kLAAssinatureBuyObjThesis = @"thesis";
NSString *const kLAAssinatureBuyObjPlan = @"plan";
NSString *const kLAAssinatureBuyObjStatus = @"status";
NSString *const kLAAssinatureBuyObjTotalThesesOnSigning = @"total_theses_on_signing";


@interface LAAssinatureBuyObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAAssinatureBuyObj

@synthesize dataIdentifier = _dataIdentifier;
@synthesize planId = _planId;
@synthesize valuePlanOnSigning = _valuePlanOnSigning;
@synthesize valuePerExtraDistrict = _valuePerExtraDistrict;
@synthesize totalPlanOnSigning = _totalPlanOnSigning;
@synthesize lawyerId = _lawyerId;
@synthesize totalDistrictsSigning = _totalDistrictsSigning;
@synthesize paymentMethod = _paymentMethod;
@synthesize vindiId = _vindiId;
@synthesize createdAt = _createdAt;
@synthesize deletedAt = _deletedAt;
@synthesize districts = _districts;
@synthesize updatedAt = _updatedAt;
@synthesize thesis = _thesis;
@synthesize plan = _plan;
@synthesize status = _status;
@synthesize totalThesesOnSigning = _totalThesesOnSigning;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.dataIdentifier = [[self objectOrNilForKey:kLAAssinatureBuyObjId fromDictionary:dict] doubleValue];
        self.planId = [[self objectOrNilForKey:kLAAssinatureBuyObjPlanId fromDictionary:dict] doubleValue];
        self.valuePlanOnSigning = [[self objectOrNilForKey:kLAAssinatureBuyObjValuePlanOnSigning fromDictionary:dict] doubleValue];
        self.valuePerExtraDistrict = [[self objectOrNilForKey:kLAAssinatureBuyObjValuePerExtraDistrict fromDictionary:dict] doubleValue];
        self.totalPlanOnSigning = [[self objectOrNilForKey:kLAAssinatureBuyObjTotalPlanOnSigning fromDictionary:dict] doubleValue];
        self.lawyerId = [[self objectOrNilForKey:kLAAssinatureBuyObjLawyerId fromDictionary:dict] doubleValue];
        self.totalDistrictsSigning = [[self objectOrNilForKey:kLAAssinatureBuyObjTotalDistrictsSigning fromDictionary:dict] doubleValue];
        self.paymentMethod = [self objectOrNilForKey:kLAAssinatureBuyObjPaymentMethod fromDictionary:dict];
        self.vindiId = [[self objectOrNilForKey:kLAAssinatureBuyObjVindiId fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLAAssinatureBuyObjCreatedAt fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLAAssinatureBuyObjDeletedAt fromDictionary:dict];
        NSObject *receivedLADistricts = [dict objectForKey:kLAAssinatureBuyObjDistricts];
        NSMutableArray *parsedLADistricts = [NSMutableArray array];
        if ([receivedLADistricts isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLADistricts) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLADistricts addObject:[LADistricts modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLADistricts isKindOfClass:[NSDictionary class]]) {
            [parsedLADistricts addObject:[LADistricts modelObjectWithDictionary:(NSDictionary *)receivedLADistricts]];
        }
        
        self.districts = [NSArray arrayWithArray:parsedLADistricts];
        self.updatedAt = [[self objectOrNilForKey:kLAAssinatureBuyObjUpdatedAt fromDictionary:dict] doubleValue];
        NSObject *receivedLAThesis = [dict objectForKey:kLAAssinatureBuyObjThesis];
        NSMutableArray *parsedLAThesis = [NSMutableArray array];
        if ([receivedLAThesis isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAThesis) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAThesis addObject:[LAThesisBuy modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAThesis isKindOfClass:[NSDictionary class]]) {
            [parsedLAThesis addObject:[LAThesisBuy modelObjectWithDictionary:(NSDictionary *)receivedLAThesis]];
        }
        
        self.thesis = [NSArray arrayWithArray:parsedLAThesis];
        self.plan = [LAPlanObj modelObjectWithDictionary:[dict objectForKey:kLAAssinatureBuyObjPlan]];
        self.status = [self objectOrNilForKey:kLAAssinatureBuyObjStatus fromDictionary:dict];
        self.totalThesesOnSigning = [[self objectOrNilForKey:kLAAssinatureBuyObjTotalThesesOnSigning fromDictionary:dict] doubleValue];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLAAssinatureBuyObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.planId] forKey:kLAAssinatureBuyObjPlanId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePlanOnSigning] forKey:kLAAssinatureBuyObjValuePlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePerExtraDistrict] forKey:kLAAssinatureBuyObjValuePerExtraDistrict];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalPlanOnSigning] forKey:kLAAssinatureBuyObjTotalPlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerId] forKey:kLAAssinatureBuyObjLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalDistrictsSigning] forKey:kLAAssinatureBuyObjTotalDistrictsSigning];
    [mutableDict setValue:self.paymentMethod forKey:kLAAssinatureBuyObjPaymentMethod];
    [mutableDict setValue:[NSNumber numberWithDouble:self.vindiId] forKey:kLAAssinatureBuyObjVindiId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAAssinatureBuyObjCreatedAt];
    [mutableDict setValue:self.deletedAt forKey:kLAAssinatureBuyObjDeletedAt];
    NSMutableArray *tempArrayForDistricts = [NSMutableArray array];
    for (NSObject *subArrayObject in self.districts) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDistricts addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDistricts addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDistricts] forKey:kLAAssinatureBuyObjDistricts];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAAssinatureBuyObjUpdatedAt];
    NSMutableArray *tempArrayForThesis = [NSMutableArray array];
    for (NSObject *subArrayObject in self.thesis) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForThesis addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForThesis addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForThesis] forKey:kLAAssinatureBuyObjThesis];
    //    [mutableDict setValue:[self.plan dictionaryRepresentation] forKey:kLAAssinatureBuyObjPlan];
    [mutableDict setValue:self.status forKey:kLAAssinatureBuyObjStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalThesesOnSigning] forKey:kLAAssinatureBuyObjTotalThesesOnSigning];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjId];
    self.planId = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjPlanId];
    self.valuePlanOnSigning = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjValuePlanOnSigning];
    self.valuePerExtraDistrict = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjValuePerExtraDistrict];
    self.totalPlanOnSigning = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjTotalPlanOnSigning];
    self.lawyerId = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjLawyerId];
    self.totalDistrictsSigning = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjTotalDistrictsSigning];
    self.paymentMethod = [aDecoder decodeObjectForKey:kLAAssinatureBuyObjPaymentMethod];
    self.vindiId = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjVindiId];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjCreatedAt];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAAssinatureBuyObjDeletedAt];
    self.districts = [aDecoder decodeObjectForKey:kLAAssinatureBuyObjDistricts];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjUpdatedAt];
    self.thesis = [aDecoder decodeObjectForKey:kLAAssinatureBuyObjThesis];
    self.plan = [aDecoder decodeObjectForKey:kLAAssinatureBuyObjPlan];
    self.status = [aDecoder decodeObjectForKey:kLAAssinatureBuyObjStatus];
    self.totalThesesOnSigning = [aDecoder decodeDoubleForKey:kLAAssinatureBuyObjTotalThesesOnSigning];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_dataIdentifier forKey:kLAAssinatureBuyObjId];
    [aCoder encodeDouble:_planId forKey:kLAAssinatureBuyObjPlanId];
    [aCoder encodeDouble:_valuePlanOnSigning forKey:kLAAssinatureBuyObjValuePlanOnSigning];
    [aCoder encodeDouble:_valuePerExtraDistrict forKey:kLAAssinatureBuyObjValuePerExtraDistrict];
    [aCoder encodeDouble:_totalPlanOnSigning forKey:kLAAssinatureBuyObjTotalPlanOnSigning];
    [aCoder encodeDouble:_lawyerId forKey:kLAAssinatureBuyObjLawyerId];
    [aCoder encodeDouble:_totalDistrictsSigning forKey:kLAAssinatureBuyObjTotalDistrictsSigning];
    [aCoder encodeObject:_paymentMethod forKey:kLAAssinatureBuyObjPaymentMethod];
    [aCoder encodeDouble:_vindiId forKey:kLAAssinatureBuyObjVindiId];
    [aCoder encodeDouble:_createdAt forKey:kLAAssinatureBuyObjCreatedAt];
    [aCoder encodeObject:_deletedAt forKey:kLAAssinatureBuyObjDeletedAt];
    [aCoder encodeObject:_districts forKey:kLAAssinatureBuyObjDistricts];
    [aCoder encodeDouble:_updatedAt forKey:kLAAssinatureBuyObjUpdatedAt];
    [aCoder encodeObject:_thesis forKey:kLAAssinatureBuyObjThesis];
    [aCoder encodeObject:_plan forKey:kLAAssinatureBuyObjPlan];
    [aCoder encodeObject:_status forKey:kLAAssinatureBuyObjStatus];
    [aCoder encodeDouble:_totalThesesOnSigning forKey:kLAAssinatureBuyObjTotalThesesOnSigning];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAAssinatureBuyObj *copy = [[LAAssinatureBuyObj alloc] init];
    
    if (copy) {
        
        copy.dataIdentifier = self.dataIdentifier;
        copy.planId = self.planId;
        copy.valuePlanOnSigning = self.valuePlanOnSigning;
        copy.valuePerExtraDistrict = self.valuePerExtraDistrict;
        copy.totalPlanOnSigning = self.totalPlanOnSigning;
        copy.lawyerId = self.lawyerId;
        copy.totalDistrictsSigning = self.totalDistrictsSigning;
        copy.paymentMethod = [self.paymentMethod copyWithZone:zone];
        copy.vindiId = self.vindiId;
        copy.createdAt = self.createdAt;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.districts = [self.districts copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.thesis = [self.thesis copyWithZone:zone];
        copy.plan = [self.plan copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.totalThesesOnSigning = self.totalThesesOnSigning;
    }
    
    return copy;
}


@end

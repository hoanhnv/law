//
//  LACategory.h
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LALawSuitCategory : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *color;
@property (nonatomic, strong) NSString *iconWithUrl;
@property (nonatomic, assign) double categoryIdentifier;
@property (nonatomic) id deletedAt;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *categoryDescription;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic ,strong) NSMutableArray* arrData;

@property (nonatomic, assign) BOOL isSelected;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

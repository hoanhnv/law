//
//  LAStatusAccountObj.h
//  JustapLawyer
//
//  Created by MAC on 9/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LAStatusAccountObj : NSObject

@property (nonatomic) NSString* email;
@property (nonatomic) NSString* status;
@property (nonatomic) NSString* message;
@property (nonatomic) ACCOUNT_STATUS type;

@end

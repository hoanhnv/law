//
//  LAData.h
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LALawSuitCategory.h"
#import "LASituation.h"

@interface LALawsuitObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *documentation;
@property (nonatomic, strong) LALawSuitCategory *category;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double version;
@property (nonatomic, strong) NSString *yourRights;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double categoryId;
@property (nonatomic, strong) NSString *legalName;
@property (nonatomic, strong) NSString *identification;
@property (nonatomic, strong) NSString *howToAct;
@property (nonatomic, assign) double valueContraction;
@property (nonatomic) id deletedAt;
@property (nonatomic, assign) double situationId;
@property (nonatomic, assign) double valueSignature;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) LASituation *situation;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *status;

#pragma Used for Plan and Signature
@property (assign) BOOL isSelected;
@property (nonatomic,strong) NSString *percentagePrice;
@property (nonatomic,strong) NSString *initialPrice;
@property (nonatomic,strong) NSString *signatureId;
@property (nonatomic, assign) double signatureCreatedAt;
@property (nonatomic, assign) double signatureUpdatedAt;
@property (nonatomic, assign) double signatureDeleteAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

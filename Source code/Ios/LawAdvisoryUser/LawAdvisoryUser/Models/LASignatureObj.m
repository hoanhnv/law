//
//  LAData.m
//
//  Created by   on 10/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LASignatureObj.h"
#import "LADistricts.h"


NSString *const kLASignatureObjId = @"id";
NSString *const kLASignatureObjPlanId = @"plan_id";
NSString *const kLASignatureObjValuePlanOnSigning = @"value_plan_on_signing";
NSString *const kLASignatureObjTotalPlanOnSigning = @"total_plan_on_signing";
NSString *const kLASignatureObjLawyerId = @"lawyer_id";
NSString *const kLASignatureObjTotalDistrictsSigning = @"total_districts_signing";
NSString *const kLASignatureObjCreatedAt = @"created_at";
NSString *const kLASignatureObjPaymentMethod = @"payment_method";
NSString *const kLASignatureObjDeletedAt = @"deleted_at";
NSString *const kLASignatureObjDistricts = @"districts";
NSString *const kLASignatureObjUpdatedAt = @"updated_at";
NSString *const kLASignatureObjStatus = @"status";
NSString *const kLASignatureObjTotalThesesOnSigning = @"total_theses_on_signing";


@interface LASignatureObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LASignatureObj

@synthesize dataIdentifier = _dataIdentifier;
@synthesize planId = _planId;
@synthesize valuePlanOnSigning = _valuePlanOnSigning;
@synthesize totalPlanOnSigning = _totalPlanOnSigning;
@synthesize lawyerId = _lawyerId;
@synthesize totalDistrictsSigning = _totalDistrictsSigning;
@synthesize createdAt = _createdAt;
@synthesize paymentMethod = _paymentMethod;
@synthesize deletedAt = _deletedAt;
@synthesize districts = _districts;
@synthesize updatedAt = _updatedAt;
@synthesize status = _status;
@synthesize totalThesesOnSigning = _totalThesesOnSigning;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.dataIdentifier = [[self objectOrNilForKey:kLASignatureObjId fromDictionary:dict] doubleValue];
        self.planId = [[self objectOrNilForKey:kLASignatureObjPlanId fromDictionary:dict] doubleValue];
        self.valuePlanOnSigning = [[self objectOrNilForKey:kLASignatureObjValuePlanOnSigning fromDictionary:dict] doubleValue];
        self.totalPlanOnSigning = [[self objectOrNilForKey:kLASignatureObjTotalPlanOnSigning fromDictionary:dict] doubleValue];
        self.lawyerId = [[self objectOrNilForKey:kLASignatureObjLawyerId fromDictionary:dict] doubleValue];
        self.totalDistrictsSigning = [[self objectOrNilForKey:kLASignatureObjTotalDistrictsSigning fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLASignatureObjCreatedAt fromDictionary:dict] doubleValue];
        self.paymentMethod = [self objectOrNilForKey:kLASignatureObjPaymentMethod fromDictionary:dict];
        self.deletedAt = [self objectOrNilForKey:kLASignatureObjDeletedAt fromDictionary:dict];
        NSObject *receivedLADistricts = [dict objectForKey:kLASignatureObjDistricts];
        NSMutableArray *parsedLADistricts = [NSMutableArray array];
        if ([receivedLADistricts isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLADistricts) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLADistricts addObject:[LADistricts modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLADistricts isKindOfClass:[NSDictionary class]]) {
            [parsedLADistricts addObject:[LADistricts modelObjectWithDictionary:(NSDictionary *)receivedLADistricts]];
        }
        
        self.districts = [NSArray arrayWithArray:parsedLADistricts];
        self.updatedAt = [[self objectOrNilForKey:kLASignatureObjUpdatedAt fromDictionary:dict] doubleValue];
        self.status = [self objectOrNilForKey:kLASignatureObjStatus fromDictionary:dict];
        self.totalThesesOnSigning = [[self objectOrNilForKey:kLASignatureObjTotalThesesOnSigning fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLASignatureObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.planId] forKey:kLASignatureObjPlanId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePlanOnSigning] forKey:kLASignatureObjValuePlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalPlanOnSigning] forKey:kLASignatureObjTotalPlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerId] forKey:kLASignatureObjLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalDistrictsSigning] forKey:kLASignatureObjTotalDistrictsSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLASignatureObjCreatedAt];
    [mutableDict setValue:self.paymentMethod forKey:kLASignatureObjPaymentMethod];
    [mutableDict setValue:self.deletedAt forKey:kLASignatureObjDeletedAt];
    NSMutableArray *tempArrayForDistricts = [NSMutableArray array];
    for (NSObject *subArrayObject in self.districts) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDistricts addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDistricts addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDistricts] forKey:kLASignatureObjDistricts];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLASignatureObjUpdatedAt];
    [mutableDict setValue:self.status forKey:kLASignatureObjStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalThesesOnSigning] forKey:kLASignatureObjTotalThesesOnSigning];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLASignatureObjId];
    self.planId = [aDecoder decodeDoubleForKey:kLASignatureObjPlanId];
    self.valuePlanOnSigning = [aDecoder decodeDoubleForKey:kLASignatureObjValuePlanOnSigning];
    self.totalPlanOnSigning = [aDecoder decodeDoubleForKey:kLASignatureObjTotalPlanOnSigning];
    self.lawyerId = [aDecoder decodeDoubleForKey:kLASignatureObjLawyerId];
    self.totalDistrictsSigning = [aDecoder decodeDoubleForKey:kLASignatureObjTotalDistrictsSigning];
    self.createdAt = [aDecoder decodeDoubleForKey:kLASignatureObjCreatedAt];
    self.paymentMethod = [aDecoder decodeObjectForKey:kLASignatureObjPaymentMethod];
    self.deletedAt = [aDecoder decodeObjectForKey:kLASignatureObjDeletedAt];
    self.districts = [aDecoder decodeObjectForKey:kLASignatureObjDistricts];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLASignatureObjUpdatedAt];
    self.status = [aDecoder decodeObjectForKey:kLASignatureObjStatus];
    self.totalThesesOnSigning = [aDecoder decodeDoubleForKey:kLASignatureObjTotalThesesOnSigning];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_dataIdentifier forKey:kLASignatureObjId];
    [aCoder encodeDouble:_planId forKey:kLASignatureObjPlanId];
    [aCoder encodeDouble:_valuePlanOnSigning forKey:kLASignatureObjValuePlanOnSigning];
    [aCoder encodeDouble:_totalPlanOnSigning forKey:kLASignatureObjTotalPlanOnSigning];
    [aCoder encodeDouble:_lawyerId forKey:kLASignatureObjLawyerId];
    [aCoder encodeDouble:_totalDistrictsSigning forKey:kLASignatureObjTotalDistrictsSigning];
    [aCoder encodeDouble:_createdAt forKey:kLASignatureObjCreatedAt];
    [aCoder encodeObject:_paymentMethod forKey:kLASignatureObjPaymentMethod];
    [aCoder encodeObject:_deletedAt forKey:kLASignatureObjDeletedAt];
    [aCoder encodeObject:_districts forKey:kLASignatureObjDistricts];
    [aCoder encodeDouble:_updatedAt forKey:kLASignatureObjUpdatedAt];
    [aCoder encodeObject:_status forKey:kLASignatureObjStatus];
    [aCoder encodeDouble:_totalThesesOnSigning forKey:kLASignatureObjTotalThesesOnSigning];
}

- (id)copyWithZone:(NSZone *)zone
{
    LASignatureObj *copy = [[LASignatureObj alloc] init];
    
    if (copy) {
        
        copy.dataIdentifier = self.dataIdentifier;
        copy.planId = self.planId;
        copy.valuePlanOnSigning = self.valuePlanOnSigning;
        copy.totalPlanOnSigning = self.totalPlanOnSigning;
        copy.lawyerId = self.lawyerId;
        copy.totalDistrictsSigning = self.totalDistrictsSigning;
        copy.createdAt = self.createdAt;
        copy.paymentMethod = [self.paymentMethod copyWithZone:zone];
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.districts = [self.districts copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.status = [self.status copyWithZone:zone];
        copy.totalThesesOnSigning = self.totalThesesOnSigning;
    }
    
    return copy;
}


@end

//
//  LALawyerInfo.m
//
//  Created by   on 10/11/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawyerInfo.h"
#import "LALawyerObj.h"


NSString *const kLALawyerInfoSuccess = @"success";
NSString *const kLALawyerInfoData = @"data";
NSString *const kLALawyerInfoMessage = @"message";


@interface LALawyerInfo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawyerInfo

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLALawyerInfoSuccess fromDictionary:dict] boolValue];
        self.data = [LALawyerObj modelObjectWithDictionary:[dict objectForKey:kLALawyerInfoData]];
        self.message = [self objectOrNilForKey:kLALawyerInfoMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLALawyerInfoSuccess];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kLALawyerInfoData];
    [mutableDict setValue:self.message forKey:kLALawyerInfoMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLALawyerInfoSuccess];
    self.data = [aDecoder decodeObjectForKey:kLALawyerInfoData];
    self.message = [aDecoder decodeObjectForKey:kLALawyerInfoMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLALawyerInfoSuccess];
    [aCoder encodeObject:_data forKey:kLALawyerInfoData];
    [aCoder encodeObject:_message forKey:kLALawyerInfoMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawyerInfo *copy = [[LALawyerInfo alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

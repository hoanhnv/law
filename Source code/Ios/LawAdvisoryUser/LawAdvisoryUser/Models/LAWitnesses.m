//
//  LAWitnesses.m
//
//  Created by   on 11/19/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAWitnesses.h"


NSString *const kLAWitnessesDeletedAt = @"deleted_at";
NSString *const kLAWitnessesId = @"id";
NSString *const kLAWitnessesUpdatedAt = @"updated_at";
NSString *const kLAWitnessesHappeningId = @"happening_id";
NSString *const kLAWitnessesName = @"name";
NSString *const kLAWitnessesCreatedAt = @"created_at";


@interface LAWitnesses ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAWitnesses

@synthesize deletedAt = _deletedAt;
@synthesize witnessesIdentifier = _witnessesIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize happeningId = _happeningId;
@synthesize name = _name;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [self objectOrNilForKey:kLAWitnessesDeletedAt fromDictionary:dict];
        self.witnessesIdentifier = [[self objectOrNilForKey:kLAWitnessesId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLAWitnessesUpdatedAt fromDictionary:dict] doubleValue];
        self.happeningId = [[self objectOrNilForKey:kLAWitnessesHappeningId fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLAWitnessesName fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAWitnessesCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLAWitnessesDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.witnessesIdentifier] forKey:kLAWitnessesId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAWitnessesUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.happeningId] forKey:kLAWitnessesHappeningId];
    [mutableDict setValue:self.name forKey:kLAWitnessesName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAWitnessesCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.deletedAt = [aDecoder decodeObjectForKey:kLAWitnessesDeletedAt];
    self.witnessesIdentifier = [aDecoder decodeDoubleForKey:kLAWitnessesId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAWitnessesUpdatedAt];
    self.happeningId = [aDecoder decodeDoubleForKey:kLAWitnessesHappeningId];
    self.name = [aDecoder decodeObjectForKey:kLAWitnessesName];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAWitnessesCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_deletedAt forKey:kLAWitnessesDeletedAt];
    [aCoder encodeDouble:_witnessesIdentifier forKey:kLAWitnessesId];
    [aCoder encodeDouble:_updatedAt forKey:kLAWitnessesUpdatedAt];
    [aCoder encodeDouble:_happeningId forKey:kLAWitnessesHappeningId];
    [aCoder encodeObject:_name forKey:kLAWitnessesName];
    [aCoder encodeDouble:_createdAt forKey:kLAWitnessesCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAWitnesses *copy = [[LAWitnesses alloc] init];
    
    if (copy) {
        
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.witnessesIdentifier = self.witnessesIdentifier;
        copy.updatedAt = self.updatedAt;
        copy.happeningId = self.happeningId;
        copy.name = [self.name copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

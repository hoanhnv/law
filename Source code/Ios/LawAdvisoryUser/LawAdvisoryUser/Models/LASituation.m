//
//  LASituation.m
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LASituation.h"


NSString *const kLASituationDeletedAt = @"deleted_at";
NSString *const kLASituationId = @"id";
NSString *const kLASituationUpdatedAt = @"updated_at";
NSString *const kLASituationName = @"name";
NSString *const kLASituationDescription = @"description";
NSString *const kLASituationCreatedAt = @"created_at";


@interface LASituation ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LASituation

@synthesize deletedAt = _deletedAt;
@synthesize situationIdentifier = _situationIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize situationDescription = _situationDescription;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [[self objectOrNilForKey:kLASituationDeletedAt fromDictionary:dict] doubleValue];
        self.situationIdentifier = [[self objectOrNilForKey:kLASituationId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLASituationUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLASituationName fromDictionary:dict];
        self.situationDescription = [self objectOrNilForKey:kLASituationDescription fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLASituationCreatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    //    [mutableDict setValue:self.deletedAt forKey:kLASituationDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.situationIdentifier] forKey:kLASituationId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLASituationUpdatedAt];
    [mutableDict setValue:self.name forKey:kLASituationName];
    [mutableDict setValue:self.situationDescription forKey:kLASituationDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLASituationCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    //    self.deletedAt = [aDecoder decodeObjectForKey:kLASituationDeletedAt];
    self.situationIdentifier = [aDecoder decodeDoubleForKey:kLASituationId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLASituationUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLASituationName];
    self.situationDescription = [aDecoder decodeObjectForKey:kLASituationDescription];
    self.createdAt = [aDecoder decodeDoubleForKey:kLASituationCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    //    [aCoder encodeObject:_deletedAt forKey:kLASituationDeletedAt];
    [aCoder encodeDouble:_situationIdentifier forKey:kLASituationId];
    [aCoder encodeDouble:_updatedAt forKey:kLASituationUpdatedAt];
    [aCoder encodeObject:_name forKey:kLASituationName];
    [aCoder encodeObject:_situationDescription forKey:kLASituationDescription];
    [aCoder encodeDouble:_createdAt forKey:kLASituationCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LASituation *copy = [[LASituation alloc] init];
    
    if (copy) {
        
        //        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.situationIdentifier = self.situationIdentifier;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.situationDescription = [self.situationDescription copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

//
//  LAThesis.h
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LALawsuitThesisBuy;

@interface LAThesisBuy : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) LALawsuitThesisBuy *lawsuitThesis;
@property (nonatomic, assign) double thesisId;
@property (nonatomic, assign) double initialFees;
@property (nonatomic, assign) double thesisIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double signatureId;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double percentageInSuccess;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

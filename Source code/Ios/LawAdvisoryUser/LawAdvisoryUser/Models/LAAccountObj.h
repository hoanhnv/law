//
//  LAAccountObj.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/18/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LAAccountObj : NSObject
@property (nonatomic) NSString* accountEmail;
@property (nonatomic) NSString* accountPassword;
@property (nonatomic) NSString* accountName;
@property (nonatomic) NSString* accountCellphone;
@property (nonatomic) NSString* accountCPF;
@property (nonatomic) NSString* accountPhone;
@property (nonatomic) NSString* accountAddress;
@property (nonatomic) NSString* accountNumber;
@property (nonatomic) NSString* accountComplement;
@property (nonatomic) NSString* accountNeighborhood;
@property (nonatomic) NSString* accountCity;
@property (nonatomic) NSString* accountUF;
@property (nonatomic) NSString* accountNumberOAB;
@property (nonatomic) NSString* accountSite;
@property (nonatomic) NSString* accountLinkedin;
@property (nonatomic) NSMutableArray* accountTitles;
@property (nonatomic) NSString* accountPostalCode;
@property (nonatomic) NSString* accountCEP;
@property (nonatomic) BOOL accountShowEmail;
@property (nonatomic) BOOL accountShowPhone;
@property (nonatomic) BOOL accountShowMobilePhone;
@property (nonatomic) double accountDateOfBirth;
@property (nonatomic) BOOL accountAcceptNew;
@property (nonatomic) NSString* avataURL;
@property (nonatomic) NSString* state;
@property (nonatomic) NSString* about;

@property (nonatomic) double bank_id;
@property (nonatomic) NSString* bank_agency;
@property (nonatomic) NSString* bank_account;
@property (nonatomic) NSString* bank_account_dac;

@property (nonatomic) NSString *image_oab;
@property (nonatomic) NSString *image_avatar;
@property (nonatomic) NSString *image_house_proof;
@property (nonatomic) NSString *image_oab_verse;

@end

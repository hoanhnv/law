//
//  LALawsuitObj.m
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawsuitObj.h"
#import "LALawSuitCategory.h"
#import "LASituation.h"


NSString *const kLALawsuitObjDocumentation = @"documentation";
NSString *const kLALawsuitObjCategory = @"category";
NSString *const kLALawsuitObjId = @"id";
NSString *const kLALawsuitObjVersion = @"version";
NSString *const kLALawsuitObjYourRights = @"your_rights";
NSString *const kLALawsuitObjCreatedAt = @"created_at";
NSString *const kLALawsuitObjCategoryId = @"category_id";
NSString *const kLALawsuitObjLegalName = @"legal_name";
NSString *const kLALawsuitObjIdentification = @"identification";
NSString *const kLALawsuitObjHowToAct = @"how_to_act";
NSString *const kLALawsuitObjValueContraction = @"value_contraction";
NSString *const kLALawsuitObjDeletedAt = @"deleted_at";
NSString *const kLALawsuitObjSituationId = @"situation_id";
NSString *const kLALawsuitObjValueSignature = @"value_signature";
NSString *const kLALawsuitObjUpdatedAt = @"updated_at";
NSString *const kLALawsuitObjSituation = @"situation";
NSString *const kLALawsuitObjName = @"name";
NSString *const kLALawsuitObjStatus = @"status";


@interface LALawsuitObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawsuitObj

@synthesize documentation = _documentation;
@synthesize category = _category;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize version = _version;
@synthesize yourRights = _yourRights;
@synthesize createdAt = _createdAt;
@synthesize categoryId = _categoryId;
@synthesize legalName = _legalName;
@synthesize identification = _identification;
@synthesize howToAct = _howToAct;
@synthesize valueContraction = _valueContraction;
@synthesize deletedAt = _deletedAt;
@synthesize situationId = _situationId;
@synthesize valueSignature = _valueSignature;
@synthesize updatedAt = _updatedAt;
@synthesize situation = _situation;
@synthesize name = _name;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.documentation = [self objectOrNilForKey:kLALawsuitObjDocumentation fromDictionary:dict];
            self.category = [LALawSuitCategory modelObjectWithDictionary:[dict objectForKey:kLALawsuitObjCategory]];
            self.dataIdentifier = [[self objectOrNilForKey:kLALawsuitObjId fromDictionary:dict] doubleValue];
            self.version = [[self objectOrNilForKey:kLALawsuitObjVersion fromDictionary:dict] doubleValue];
            self.yourRights = [self objectOrNilForKey:kLALawsuitObjYourRights fromDictionary:dict];
            self.createdAt = [[self objectOrNilForKey:kLALawsuitObjCreatedAt fromDictionary:dict] doubleValue];
            self.categoryId = [[self objectOrNilForKey:kLALawsuitObjCategoryId fromDictionary:dict] doubleValue];
            self.legalName = [self objectOrNilForKey:kLALawsuitObjLegalName fromDictionary:dict];
            self.identification = [self objectOrNilForKey:kLALawsuitObjIdentification fromDictionary:dict];
            self.howToAct = [self objectOrNilForKey:kLALawsuitObjHowToAct fromDictionary:dict];
            self.valueContraction = [[self objectOrNilForKey:kLALawsuitObjValueContraction fromDictionary:dict] doubleValue];
            self.deletedAt = [self objectOrNilForKey:kLALawsuitObjDeletedAt fromDictionary:dict];
            self.situationId = [[self objectOrNilForKey:kLALawsuitObjSituationId fromDictionary:dict] doubleValue];
            self.valueSignature = [[self objectOrNilForKey:kLALawsuitObjValueSignature fromDictionary:dict] doubleValue];
            self.updatedAt = [[self objectOrNilForKey:kLALawsuitObjUpdatedAt fromDictionary:dict] doubleValue];
            self.situation = [LASituation modelObjectWithDictionary:[dict objectForKey:kLALawsuitObjSituation]];
            self.name = [self objectOrNilForKey:kLALawsuitObjName fromDictionary:dict];
            self.status = [self objectOrNilForKey:kLALawsuitObjStatus fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.documentation forKey:kLALawsuitObjDocumentation];
    [mutableDict setValue:[self.category dictionaryRepresentation] forKey:kLALawsuitObjCategory];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLALawsuitObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.version] forKey:kLALawsuitObjVersion];
    [mutableDict setValue:self.yourRights forKey:kLALawsuitObjYourRights];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLALawsuitObjCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryId] forKey:kLALawsuitObjCategoryId];
    [mutableDict setValue:self.legalName forKey:kLALawsuitObjLegalName];
    [mutableDict setValue:self.identification forKey:kLALawsuitObjIdentification];
    [mutableDict setValue:self.howToAct forKey:kLALawsuitObjHowToAct];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueContraction] forKey:kLALawsuitObjValueContraction];
    [mutableDict setValue:self.deletedAt forKey:kLALawsuitObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.situationId] forKey:kLALawsuitObjSituationId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueSignature] forKey:kLALawsuitObjValueSignature];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLALawsuitObjUpdatedAt];
    [mutableDict setValue:[self.situation dictionaryRepresentation] forKey:kLALawsuitObjSituation];
    [mutableDict setValue:self.name forKey:kLALawsuitObjName];
    [mutableDict setValue:self.status forKey:kLALawsuitObjStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.documentation = [aDecoder decodeObjectForKey:kLALawsuitObjDocumentation];
    self.category = [aDecoder decodeObjectForKey:kLALawsuitObjCategory];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLALawsuitObjId];
    self.version = [aDecoder decodeDoubleForKey:kLALawsuitObjVersion];
    self.yourRights = [aDecoder decodeObjectForKey:kLALawsuitObjYourRights];
    self.createdAt = [aDecoder decodeDoubleForKey:kLALawsuitObjCreatedAt];
    self.categoryId = [aDecoder decodeDoubleForKey:kLALawsuitObjCategoryId];
    self.legalName = [aDecoder decodeObjectForKey:kLALawsuitObjLegalName];
    self.identification = [aDecoder decodeObjectForKey:kLALawsuitObjIdentification];
    self.howToAct = [aDecoder decodeObjectForKey:kLALawsuitObjHowToAct];
    self.valueContraction = [aDecoder decodeDoubleForKey:kLALawsuitObjValueContraction];
    self.deletedAt = [aDecoder decodeObjectForKey:kLALawsuitObjDeletedAt];
    self.situationId = [aDecoder decodeDoubleForKey:kLALawsuitObjSituationId];
    self.valueSignature = [aDecoder decodeDoubleForKey:kLALawsuitObjValueSignature];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLALawsuitObjUpdatedAt];
    self.situation = [aDecoder decodeObjectForKey:kLALawsuitObjSituation];
    self.name = [aDecoder decodeObjectForKey:kLALawsuitObjName];
    self.status = [aDecoder decodeObjectForKey:kLALawsuitObjStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_documentation forKey:kLALawsuitObjDocumentation];
    [aCoder encodeObject:_category forKey:kLALawsuitObjCategory];
    [aCoder encodeDouble:_dataIdentifier forKey:kLALawsuitObjId];
    [aCoder encodeDouble:_version forKey:kLALawsuitObjVersion];
    [aCoder encodeObject:_yourRights forKey:kLALawsuitObjYourRights];
    [aCoder encodeDouble:_createdAt forKey:kLALawsuitObjCreatedAt];
    [aCoder encodeDouble:_categoryId forKey:kLALawsuitObjCategoryId];
    [aCoder encodeObject:_legalName forKey:kLALawsuitObjLegalName];
    [aCoder encodeObject:_identification forKey:kLALawsuitObjIdentification];
    [aCoder encodeObject:_howToAct forKey:kLALawsuitObjHowToAct];
    [aCoder encodeDouble:_valueContraction forKey:kLALawsuitObjValueContraction];
    [aCoder encodeObject:_deletedAt forKey:kLALawsuitObjDeletedAt];
    [aCoder encodeDouble:_situationId forKey:kLALawsuitObjSituationId];
    [aCoder encodeDouble:_valueSignature forKey:kLALawsuitObjValueSignature];
    [aCoder encodeDouble:_updatedAt forKey:kLALawsuitObjUpdatedAt];
    [aCoder encodeObject:_situation forKey:kLALawsuitObjSituation];
    [aCoder encodeObject:_name forKey:kLALawsuitObjName];
    [aCoder encodeObject:_status forKey:kLALawsuitObjStatus];
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawsuitObj *copy = [[LALawsuitObj alloc] init];
    
    if (copy) {

        copy.documentation = [self.documentation copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.version = self.version;
        copy.yourRights = [self.yourRights copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.categoryId = self.categoryId;
        copy.legalName = [self.legalName copyWithZone:zone];
        copy.identification = [self.identification copyWithZone:zone];
        copy.howToAct = [self.howToAct copyWithZone:zone];
        copy.valueContraction = self.valueContraction;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.situationId = self.situationId;
        copy.valueSignature = self.valueSignature;
        copy.updatedAt = self.updatedAt;
        copy.situation = [self.situation copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
    }
    
    return copy;
}
- (BOOL)isEqual:(id)object
{
    return ((self.dataIdentifier == ((LALawsuitObj*)object).dataIdentifier) && (self.category.categoryIdentifier == ((LALawsuitObj*)object).category.categoryIdentifier));
}
@end

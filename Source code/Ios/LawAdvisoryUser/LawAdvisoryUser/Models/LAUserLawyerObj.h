//
//  LAData.h
//
//  Created by   on 11/20/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LAUserSignature;

@interface LAUserLawyerObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double signatureId;
@property (nonatomic, assign) double percentageInSuccess;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double price;
@property (nonatomic, strong) LAUserSignature *signature;
@property (nonatomic, assign) double stars;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double totalClients;
@property (nonatomic, assign) double thesisId;
@property (nonatomic, assign) double initialFees;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

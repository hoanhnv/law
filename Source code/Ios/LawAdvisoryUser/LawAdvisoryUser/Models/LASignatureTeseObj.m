//
//  LASignatureTeseObj.m
//
//  Created by   on 11/25/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LASignatureTeseObj.h"
#import "LALawsuitObj.h"


NSString *const kLASignatureTeseObjLawsuitThesis = @"lawsuit_thesis";
NSString *const kLASignatureTeseObjThesisId = @"thesis_id";
NSString *const kLASignatureTeseObjInitialFees = @"initial_fees";
NSString *const kLASignatureTeseObjId = @"id";
NSString *const kLASignatureTeseObjDeletedAt = @"deleted_at";
NSString *const kLASignatureTeseObjCreatedAt = @"created_at";
NSString *const kLASignatureTeseObjSignatureId = @"signature_id";
NSString *const kLASignatureTeseObjUpdatedAt = @"updated_at";
NSString *const kLASignatureTeseObjPercentageInSuccess = @"percentage_in_success";


@interface LASignatureTeseObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LASignatureTeseObj

@synthesize lawsuitThesis = _lawsuitThesis;
@synthesize thesisId = _thesisId;
@synthesize initialFees = _initialFees;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize createdAt = _createdAt;
@synthesize signatureId = _signatureId;
@synthesize updatedAt = _updatedAt;
@synthesize percentageInSuccess = _percentageInSuccess;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.lawsuitThesis = [LALawsuitObj modelObjectWithDictionary:[dict objectForKey:kLASignatureTeseObjLawsuitThesis]];
        self.thesisId = [[self objectOrNilForKey:kLASignatureTeseObjThesisId fromDictionary:dict] doubleValue];
        self.initialFees = [[self objectOrNilForKey:kLASignatureTeseObjInitialFees fromDictionary:dict] doubleValue];
        self.dataIdentifier = [[self objectOrNilForKey:kLASignatureTeseObjId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLASignatureTeseObjDeletedAt fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLASignatureTeseObjCreatedAt fromDictionary:dict] doubleValue];
        self.signatureId = [[self objectOrNilForKey:kLASignatureTeseObjSignatureId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLASignatureTeseObjUpdatedAt fromDictionary:dict] doubleValue];
        self.percentageInSuccess = [[self objectOrNilForKey:kLASignatureTeseObjPercentageInSuccess fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.lawsuitThesis dictionaryRepresentation] forKey:kLASignatureTeseObjLawsuitThesis];
    [mutableDict setValue:[NSNumber numberWithDouble:self.thesisId] forKey:kLASignatureTeseObjThesisId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.initialFees] forKey:kLASignatureTeseObjInitialFees];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLASignatureTeseObjId];
    [mutableDict setValue:self.deletedAt forKey:kLASignatureTeseObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLASignatureTeseObjCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureId] forKey:kLASignatureTeseObjSignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLASignatureTeseObjUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.percentageInSuccess] forKey:kLASignatureTeseObjPercentageInSuccess];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.lawsuitThesis = [aDecoder decodeObjectForKey:kLASignatureTeseObjLawsuitThesis];
    self.thesisId = [aDecoder decodeDoubleForKey:kLASignatureTeseObjThesisId];
    self.initialFees = [aDecoder decodeDoubleForKey:kLASignatureTeseObjInitialFees];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLASignatureTeseObjId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLASignatureTeseObjDeletedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLASignatureTeseObjCreatedAt];
    self.signatureId = [aDecoder decodeDoubleForKey:kLASignatureTeseObjSignatureId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLASignatureTeseObjUpdatedAt];
    self.percentageInSuccess = [aDecoder decodeDoubleForKey:kLASignatureTeseObjPercentageInSuccess];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_lawsuitThesis forKey:kLASignatureTeseObjLawsuitThesis];
    [aCoder encodeDouble:_thesisId forKey:kLASignatureTeseObjThesisId];
    [aCoder encodeDouble:_initialFees forKey:kLASignatureTeseObjInitialFees];
    [aCoder encodeDouble:_dataIdentifier forKey:kLASignatureTeseObjId];
    [aCoder encodeObject:_deletedAt forKey:kLASignatureTeseObjDeletedAt];
    [aCoder encodeDouble:_createdAt forKey:kLASignatureTeseObjCreatedAt];
    [aCoder encodeDouble:_signatureId forKey:kLASignatureTeseObjSignatureId];
    [aCoder encodeDouble:_updatedAt forKey:kLASignatureTeseObjUpdatedAt];
    [aCoder encodeDouble:_percentageInSuccess forKey:kLASignatureTeseObjPercentageInSuccess];
}

- (id)copyWithZone:(NSZone *)zone
{
    LASignatureTeseObj *copy = [[LASignatureTeseObj alloc] init];
    
    if (copy) {
        
        copy.lawsuitThesis = [self.lawsuitThesis copyWithZone:zone];
        copy.thesisId = self.thesisId;
        copy.initialFees = self.initialFees;
        copy.dataIdentifier = self.dataIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.signatureId = self.signatureId;
        copy.updatedAt = self.updatedAt;
        copy.percentageInSuccess = self.percentageInSuccess;
    }
    
    return copy;
}


@end

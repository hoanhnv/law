//
//  LAMedias.h
//
//  Created by   on 11/22/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LAMedias : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mediasIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *media;
@property (nonatomic, assign) double happeningId;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *mimeType;
@property (nonatomic, strong) NSString *mediaWithUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

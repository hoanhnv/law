//
//  LAThesisBuy.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAThesisBuy.h"
#import "LALawsuitThesisBuy.h"


NSString *const kLAThesisBuyLawsuitThesis = @"lawsuit_thesis";
NSString *const kLAThesisBuyThesisId = @"thesis_id";
NSString *const kLAThesisBuyInitialFees = @"initial_fees";
NSString *const kLAThesisBuyId = @"id";
NSString *const kLAThesisBuyDeletedAt = @"deleted_at";
NSString *const kLAThesisBuyCreatedAt = @"created_at";
NSString *const kLAThesisBuySignatureId = @"signature_id";
NSString *const kLAThesisBuyUpdatedAt = @"updated_at";
NSString *const kLAThesisBuyPercentageInSuccess = @"percentage_in_success";


@interface LAThesisBuy ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAThesisBuy

@synthesize lawsuitThesis = _lawsuitThesis;
@synthesize thesisId = _thesisId;
@synthesize initialFees = _initialFees;
@synthesize thesisIdentifier = _thesisIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize createdAt = _createdAt;
@synthesize signatureId = _signatureId;
@synthesize updatedAt = _updatedAt;
@synthesize percentageInSuccess = _percentageInSuccess;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.lawsuitThesis = [LALawsuitThesisBuy modelObjectWithDictionary:[dict objectForKey:kLAThesisBuyLawsuitThesis]];
        self.thesisId = [[self objectOrNilForKey:kLAThesisBuyThesisId fromDictionary:dict] doubleValue];
        self.initialFees = [[self objectOrNilForKey:kLAThesisBuyInitialFees fromDictionary:dict] doubleValue];
        self.thesisIdentifier = [[self objectOrNilForKey:kLAThesisBuyId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLAThesisBuyDeletedAt fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAThesisBuyCreatedAt fromDictionary:dict] doubleValue];
        self.signatureId = [[self objectOrNilForKey:kLAThesisBuySignatureId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLAThesisBuyUpdatedAt fromDictionary:dict] doubleValue];
        self.percentageInSuccess = [[self objectOrNilForKey:kLAThesisBuyPercentageInSuccess fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.lawsuitThesis dictionaryRepresentation] forKey:kLAThesisBuyLawsuitThesis];
    [mutableDict setValue:[NSNumber numberWithDouble:self.thesisId] forKey:kLAThesisBuyThesisId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.initialFees] forKey:kLAThesisBuyInitialFees];
    [mutableDict setValue:[NSNumber numberWithDouble:self.thesisIdentifier] forKey:kLAThesisBuyId];
    [mutableDict setValue:self.deletedAt forKey:kLAThesisBuyDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAThesisBuyCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureId] forKey:kLAThesisBuySignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAThesisBuyUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.percentageInSuccess] forKey:kLAThesisBuyPercentageInSuccess];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.lawsuitThesis = [aDecoder decodeObjectForKey:kLAThesisBuyLawsuitThesis];
    self.thesisId = [aDecoder decodeDoubleForKey:kLAThesisBuyThesisId];
    self.initialFees = [aDecoder decodeDoubleForKey:kLAThesisBuyInitialFees];
    self.thesisIdentifier = [aDecoder decodeDoubleForKey:kLAThesisBuyId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAThesisBuyDeletedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAThesisBuyCreatedAt];
    self.signatureId = [aDecoder decodeDoubleForKey:kLAThesisBuySignatureId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAThesisBuyUpdatedAt];
    self.percentageInSuccess = [aDecoder decodeDoubleForKey:kLAThesisBuyPercentageInSuccess];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_lawsuitThesis forKey:kLAThesisBuyLawsuitThesis];
    [aCoder encodeDouble:_thesisId forKey:kLAThesisBuyThesisId];
    [aCoder encodeDouble:_initialFees forKey:kLAThesisBuyInitialFees];
    [aCoder encodeDouble:_thesisIdentifier forKey:kLAThesisBuyId];
    [aCoder encodeObject:_deletedAt forKey:kLAThesisBuyDeletedAt];
    [aCoder encodeDouble:_createdAt forKey:kLAThesisBuyCreatedAt];
    [aCoder encodeDouble:_signatureId forKey:kLAThesisBuySignatureId];
    [aCoder encodeDouble:_updatedAt forKey:kLAThesisBuyUpdatedAt];
    [aCoder encodeDouble:_percentageInSuccess forKey:kLAThesisBuyPercentageInSuccess];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAThesisBuy *copy = [[LAThesisBuy alloc] init];
    
    if (copy) {
        
        copy.lawsuitThesis = [self.lawsuitThesis copyWithZone:zone];
        copy.thesisId = self.thesisId;
        copy.initialFees = self.initialFees;
        copy.thesisIdentifier = self.thesisIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.signatureId = self.signatureId;
        copy.updatedAt = self.updatedAt;
        copy.percentageInSuccess = self.percentageInSuccess;
    }
    
    return copy;
}


@end

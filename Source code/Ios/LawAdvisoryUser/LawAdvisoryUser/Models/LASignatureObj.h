//
//  LAData.h
//
//  Created by   on 10/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LASignatureObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double planId;
@property (nonatomic, assign) double valuePlanOnSigning;
@property (nonatomic, assign) double totalPlanOnSigning;
@property (nonatomic, assign) double lawyerId;
@property (nonatomic, assign) double totalDistrictsSigning;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) id paymentMethod;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, strong) NSArray *districts;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, assign) double totalThesesOnSigning;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

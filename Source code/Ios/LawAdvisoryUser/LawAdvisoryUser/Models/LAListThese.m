//
//  LAListThese.m
//
//  Created by   on 10/5/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAListThese.h"
#import "LALawsuitObj.h"


NSString *const kLAListTheseSuccess = @"success";
NSString *const kLAListTheseData = @"data";
NSString *const kLAListTheseMessage = @"message";


@interface LAListThese ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAListThese

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAListTheseSuccess fromDictionary:dict] boolValue];
        NSObject *receivedLAData = [dict objectForKey:kLAListTheseData];
        NSMutableArray *parsedLAData = [NSMutableArray array];
        if ([receivedLAData isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAData) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAData addObject:[LALawsuitObj modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAData isKindOfClass:[NSDictionary class]]) {
            [parsedLAData addObject:[LALawsuitObj modelObjectWithDictionary:(NSDictionary *)receivedLAData]];
        }
        
        self.data = [NSArray arrayWithArray:parsedLAData];
        self.message = [self objectOrNilForKey:kLAListTheseMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAListTheseSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLAListTheseData];
    [mutableDict setValue:self.message forKey:kLAListTheseMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAListTheseSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAListTheseData];
    self.message = [aDecoder decodeObjectForKey:kLAListTheseMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAListTheseSuccess];
    [aCoder encodeObject:_data forKey:kLAListTheseData];
    [aCoder encodeObject:_message forKey:kLAListTheseMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAListThese *copy = [[LAListThese alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LADistricts.h
//
//  Created by   on 10/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LASignatureDistrict;

@interface LADistricts : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) LASignatureDistrict *district;
@property (nonatomic, assign) double districtsIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double signatureId;
@property (nonatomic, assign) double districtId;
@property (nonatomic, assign) double updatedAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

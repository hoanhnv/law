//
//  LAThesis.m
//
//  Created by   on 10/10/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAThesisBannerObj.h"
#import "LACategory.h"


NSString *const kLAThesisDocumentation = @"documentation";
NSString *const kLAThesisId = @"id";
NSString *const kLAThesisVersion = @"version";
NSString *const kLAThesisYourRights = @"your_rights";
NSString *const kLAThesisCreatedAt = @"created_at";
NSString *const kLAThesisCategoryId = @"category_id";
NSString *const kLAThesisLegalName = @"legal_name";
NSString *const kLAThesisIdentification = @"identification";
NSString *const kLAThesisValueContraction = @"value_contraction";
NSString *const kLAThesisHowToAct = @"how_to_act";
NSString *const kLAThesisDeletedAt = @"deleted_at";
NSString *const kLAThesisSituationId = @"situation_id";
NSString *const kLAThesisValueSignature = @"value_signature";
NSString *const kLAThesisUpdatedAt = @"updated_at";
NSString *const kLAThesisName = @"name";
NSString *const kLAThesisStatus = @"status";


@interface LAThesisBannerObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAThesisBannerObj

@synthesize documentation = _documentation;
@synthesize thesisIdentifier = _thesisIdentifier;
@synthesize version = _version;
@synthesize yourRights = _yourRights;
@synthesize createdAt = _createdAt;
@synthesize categoryId = _categoryId;
@synthesize legalName = _legalName;
@synthesize identification = _identification;
@synthesize valueContraction = _valueContraction;
@synthesize howToAct = _howToAct;
@synthesize deletedAt = _deletedAt;
@synthesize situationId = _situationId;
@synthesize valueSignature = _valueSignature;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.documentation = [self objectOrNilForKey:kLAThesisDocumentation fromDictionary:dict];
        self.thesisIdentifier = [[self objectOrNilForKey:kLAThesisId fromDictionary:dict] doubleValue];
        self.version = [[self objectOrNilForKey:kLAThesisVersion fromDictionary:dict] doubleValue];
        self.yourRights = [self objectOrNilForKey:kLAThesisYourRights fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAThesisCreatedAt fromDictionary:dict] doubleValue];
        self.categoryId = [[self objectOrNilForKey:kLAThesisCategoryId fromDictionary:dict] doubleValue];
        self.legalName = [self objectOrNilForKey:kLAThesisLegalName fromDictionary:dict];
        self.identification = [self objectOrNilForKey:kLAThesisIdentification fromDictionary:dict];
        self.valueContraction = [[self objectOrNilForKey:kLAThesisValueContraction fromDictionary:dict] doubleValue];
        self.howToAct = [self objectOrNilForKey:kLAThesisHowToAct fromDictionary:dict];
        self.deletedAt = [self objectOrNilForKey:kLAThesisDeletedAt fromDictionary:dict];
        self.situationId = [[self objectOrNilForKey:kLAThesisSituationId fromDictionary:dict] doubleValue];
        self.valueSignature = [[self objectOrNilForKey:kLAThesisValueSignature fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLAThesisUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLAThesisName fromDictionary:dict];
        self.status = [self objectOrNilForKey:kLAThesisStatus fromDictionary:dict];
        self.category = [LATheiseCategory modelObjectWithDictionary:[dict objectForKey:@"category"]];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.documentation forKey:kLAThesisDocumentation];
    [mutableDict setValue:[NSNumber numberWithDouble:self.thesisIdentifier] forKey:kLAThesisId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.version] forKey:kLAThesisVersion];
    [mutableDict setValue:self.yourRights forKey:kLAThesisYourRights];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAThesisCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryId] forKey:kLAThesisCategoryId];
    [mutableDict setValue:self.legalName forKey:kLAThesisLegalName];
    [mutableDict setValue:self.identification forKey:kLAThesisIdentification];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueContraction] forKey:kLAThesisValueContraction];
    [mutableDict setValue:self.howToAct forKey:kLAThesisHowToAct];
    [mutableDict setValue:self.deletedAt forKey:kLAThesisDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.situationId] forKey:kLAThesisSituationId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueSignature] forKey:kLAThesisValueSignature];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAThesisUpdatedAt];
    [mutableDict setValue:self.name forKey:kLAThesisName];
    [mutableDict setValue:self.status forKey:kLAThesisStatus];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.documentation = [aDecoder decodeObjectForKey:kLAThesisDocumentation];
    self.thesisIdentifier = [aDecoder decodeDoubleForKey:kLAThesisId];
    self.version = [aDecoder decodeDoubleForKey:kLAThesisVersion];
    self.yourRights = [aDecoder decodeObjectForKey:kLAThesisYourRights];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAThesisCreatedAt];
    self.categoryId = [aDecoder decodeDoubleForKey:kLAThesisCategoryId];
    self.legalName = [aDecoder decodeObjectForKey:kLAThesisLegalName];
    self.identification = [aDecoder decodeObjectForKey:kLAThesisIdentification];
    self.valueContraction = [aDecoder decodeDoubleForKey:kLAThesisValueContraction];
    self.howToAct = [aDecoder decodeObjectForKey:kLAThesisHowToAct];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAThesisDeletedAt];
    self.situationId = [aDecoder decodeDoubleForKey:kLAThesisSituationId];
    self.valueSignature = [aDecoder decodeDoubleForKey:kLAThesisValueSignature];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAThesisUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLAThesisName];
    self.status = [aDecoder decodeObjectForKey:kLAThesisStatus];
    self.category = [aDecoder decodeObjectForKey:@"category"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_documentation forKey:kLAThesisDocumentation];
    [aCoder encodeDouble:_thesisIdentifier forKey:kLAThesisId];
    [aCoder encodeDouble:_version forKey:kLAThesisVersion];
    [aCoder encodeObject:_yourRights forKey:kLAThesisYourRights];
    [aCoder encodeDouble:_createdAt forKey:kLAThesisCreatedAt];
    [aCoder encodeDouble:_categoryId forKey:kLAThesisCategoryId];
    [aCoder encodeObject:_legalName forKey:kLAThesisLegalName];
    [aCoder encodeObject:_identification forKey:kLAThesisIdentification];
    [aCoder encodeDouble:_valueContraction forKey:kLAThesisValueContraction];
    [aCoder encodeObject:_howToAct forKey:kLAThesisHowToAct];
    [aCoder encodeObject:_deletedAt forKey:kLAThesisDeletedAt];
    [aCoder encodeDouble:_situationId forKey:kLAThesisSituationId];
    [aCoder encodeDouble:_valueSignature forKey:kLAThesisValueSignature];
    [aCoder encodeDouble:_updatedAt forKey:kLAThesisUpdatedAt];
    [aCoder encodeObject:_name forKey:kLAThesisName];
    [aCoder encodeObject:_status forKey:kLAThesisStatus];
    [aCoder encodeObject:self.category forKey:@"category"];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAThesisBannerObj *copy = [[LAThesisBannerObj alloc] init];
    
    if (copy) {
        
        copy.documentation = [self.documentation copyWithZone:zone];
        copy.thesisIdentifier = self.thesisIdentifier;
        copy.version = self.version;
        copy.yourRights = [self.yourRights copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.categoryId = self.categoryId;
        copy.legalName = [self.legalName copyWithZone:zone];
        copy.identification = [self.identification copyWithZone:zone];
        copy.valueContraction = self.valueContraction;
        copy.howToAct = [self.howToAct copyWithZone:zone];
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.situationId = self.situationId;
        copy.valueSignature = self.valueSignature;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LAListCustomerOpen.m
//
//  Created by   on 11/22/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAListCustomerOpen.h"
#import "LACustomerOpenObj.h"


NSString *const kLAListCustomerOpenSuccess = @"success";
NSString *const kLAListCustomerOpenData = @"data";
NSString *const kLAListCustomerOpenMessage = @"message";


@interface LAListCustomerOpen ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAListCustomerOpen

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAListCustomerOpenSuccess fromDictionary:dict] boolValue];
        NSObject *receivedLACustomerOpenObj = [dict objectForKey:kLAListCustomerOpenData];
        NSMutableArray *parsedLACustomerOpenObj = [NSMutableArray array];
        if ([receivedLACustomerOpenObj isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLACustomerOpenObj) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLACustomerOpenObj addObject:[LACustomerOpenObj modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLACustomerOpenObj isKindOfClass:[NSDictionary class]]) {
            [parsedLACustomerOpenObj addObject:[LACustomerOpenObj modelObjectWithDictionary:(NSDictionary *)receivedLACustomerOpenObj]];
        }
        
        self.data = [NSArray arrayWithArray:parsedLACustomerOpenObj];
        self.message = [self objectOrNilForKey:kLAListCustomerOpenMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAListCustomerOpenSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLAListCustomerOpenData];
    [mutableDict setValue:self.message forKey:kLAListCustomerOpenMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAListCustomerOpenSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAListCustomerOpenData];
    self.message = [aDecoder decodeObjectForKey:kLAListCustomerOpenMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAListCustomerOpenSuccess];
    [aCoder encodeObject:_data forKey:kLAListCustomerOpenData];
    [aCoder encodeObject:_message forKey:kLAListCustomerOpenMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAListCustomerOpen *copy = [[LAListCustomerOpen alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

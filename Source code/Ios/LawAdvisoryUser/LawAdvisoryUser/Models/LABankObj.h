//
//  LAData.h
//
//  Created by   on 12/17/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LABankObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

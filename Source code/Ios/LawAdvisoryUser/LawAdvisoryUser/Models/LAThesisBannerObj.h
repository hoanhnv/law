//
//  LAThesis.h
//
//  Created by   on 10/10/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LATheiseCategory;

@interface LAThesisBannerObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *documentation;
@property (nonatomic, assign) double thesisIdentifier;
@property (nonatomic, assign) double version;
@property (nonatomic, strong) NSString *yourRights;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double categoryId;
@property (nonatomic, strong) NSString *legalName;
@property (nonatomic, strong) NSString *identification;
@property (nonatomic, assign) double valueContraction;
@property (nonatomic, strong) NSString *howToAct;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double situationId;
@property (nonatomic, assign) double valueSignature;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) LATheiseCategory *category;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

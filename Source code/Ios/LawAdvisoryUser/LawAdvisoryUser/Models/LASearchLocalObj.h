//
//  LASearchLocalObj.h
//  JustapLawyer
//
//  Created by MAC on 10/27/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum TYPE_SEARCH_CELL
{
    TYPE_SEARCH_CATEGORY,
    TYPE_SEARCH_TESES,
    TYPE_SEARCH_DUVIDAS
}TYPE_SEARCH_CELL;
@interface LASearchLocalObj : NSObject
@property (nonatomic) TYPE_SEARCH_CELL typeSearch;
@property (nonatomic) NSString* textShown;
@property (nonatomic) NSString* keywordSearch;
@property (nonatomic) NSString* title;
@property (nonatomic) id data;
@end

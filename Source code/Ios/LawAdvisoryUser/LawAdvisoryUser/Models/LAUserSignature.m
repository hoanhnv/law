//
//  LAUserSignature.m
//
//  Created by   on 11/20/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAUserSignature.h"
#import "LALawyerObj.h"


NSString *const kLAUserSignatureId = @"id";
NSString *const kLAUserSignaturePlanId = @"plan_id";
NSString *const kLAUserSignatureValuePlanOnSigning = @"value_plan_on_signing";
NSString *const kLAUserSignatureValuePerExtraDistrict = @"value_per_extra_district";
NSString *const kLAUserSignatureTotalPlanOnSigning = @"total_plan_on_signing";
NSString *const kLAUserSignatureLawyerId = @"lawyer_id";
NSString *const kLAUserSignatureTotalDistrictsSigning = @"total_districts_signing";
NSString *const kLAUserSignatureCreatedAt = @"created_at";
NSString *const kLAUserSignatureVindiId = @"vindi_id";
NSString *const kLAUserSignaturePaymentMethod = @"payment_method";
NSString *const kLAUserSignatureDeletedAt = @"deleted_at";
NSString *const kLAUserSignatureLawyer = @"lawyer";
NSString *const kLAUserSignatureUpdatedAt = @"updated_at";
NSString *const kLAUserSignatureStatus = @"status";
NSString *const kLAUserSignatureTotalThesesOnSigning = @"total_theses_on_signing";


@interface LAUserSignature ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAUserSignature

@synthesize signatureIdentifier = _signatureIdentifier;
@synthesize planId = _planId;
@synthesize valuePlanOnSigning = _valuePlanOnSigning;
@synthesize valuePerExtraDistrict = _valuePerExtraDistrict;
@synthesize totalPlanOnSigning = _totalPlanOnSigning;
@synthesize lawyerId = _lawyerId;
@synthesize totalDistrictsSigning = _totalDistrictsSigning;
@synthesize createdAt = _createdAt;
@synthesize vindiId = _vindiId;
@synthesize paymentMethod = _paymentMethod;
@synthesize deletedAt = _deletedAt;
@synthesize lawyer = _lawyer;
@synthesize updatedAt = _updatedAt;
@synthesize status = _status;
@synthesize totalThesesOnSigning = _totalThesesOnSigning;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.signatureIdentifier = [[self objectOrNilForKey:kLAUserSignatureId fromDictionary:dict] doubleValue];
        self.planId = [[self objectOrNilForKey:kLAUserSignaturePlanId fromDictionary:dict] doubleValue];
        self.valuePlanOnSigning = [[self objectOrNilForKey:kLAUserSignatureValuePlanOnSigning fromDictionary:dict] doubleValue];
        self.valuePerExtraDistrict = [self objectOrNilForKey:kLAUserSignatureValuePerExtraDistrict fromDictionary:dict];
        self.totalPlanOnSigning = [[self objectOrNilForKey:kLAUserSignatureTotalPlanOnSigning fromDictionary:dict] doubleValue];
        self.lawyerId = [[self objectOrNilForKey:kLAUserSignatureLawyerId fromDictionary:dict] doubleValue];
        self.totalDistrictsSigning = [[self objectOrNilForKey:kLAUserSignatureTotalDistrictsSigning fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLAUserSignatureCreatedAt fromDictionary:dict] doubleValue];
        self.vindiId = [self objectOrNilForKey:kLAUserSignatureVindiId fromDictionary:dict];
        self.paymentMethod = [self objectOrNilForKey:kLAUserSignaturePaymentMethod fromDictionary:dict];
        self.deletedAt = [self objectOrNilForKey:kLAUserSignatureDeletedAt fromDictionary:dict];
        self.lawyer = [LALawyerObj modelObjectWithDictionary:[dict objectForKey:kLAUserSignatureLawyer]];
        self.updatedAt = [[self objectOrNilForKey:kLAUserSignatureUpdatedAt fromDictionary:dict] doubleValue];
        self.status = [self objectOrNilForKey:kLAUserSignatureStatus fromDictionary:dict];
        self.totalThesesOnSigning = [[self objectOrNilForKey:kLAUserSignatureTotalThesesOnSigning fromDictionary:dict] doubleValue];
    }
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureIdentifier] forKey:kLAUserSignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.planId] forKey:kLAUserSignaturePlanId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePlanOnSigning] forKey:kLAUserSignatureValuePlanOnSigning];
    [mutableDict setValue:self.valuePerExtraDistrict forKey:kLAUserSignatureValuePerExtraDistrict];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalPlanOnSigning] forKey:kLAUserSignatureTotalPlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerId] forKey:kLAUserSignatureLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalDistrictsSigning] forKey:kLAUserSignatureTotalDistrictsSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAUserSignatureCreatedAt];
    [mutableDict setValue:self.vindiId forKey:kLAUserSignatureVindiId];
    [mutableDict setValue:self.paymentMethod forKey:kLAUserSignaturePaymentMethod];
    [mutableDict setValue:self.deletedAt forKey:kLAUserSignatureDeletedAt];
    [mutableDict setValue:[self.lawyer dictionaryRepresentation] forKey:kLAUserSignatureLawyer];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAUserSignatureUpdatedAt];
    [mutableDict setValue:self.status forKey:kLAUserSignatureStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalThesesOnSigning] forKey:kLAUserSignatureTotalThesesOnSigning];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.signatureIdentifier = [aDecoder decodeDoubleForKey:kLAUserSignatureId];
    self.planId = [aDecoder decodeDoubleForKey:kLAUserSignaturePlanId];
    self.valuePlanOnSigning = [aDecoder decodeDoubleForKey:kLAUserSignatureValuePlanOnSigning];
    self.valuePerExtraDistrict = [aDecoder decodeObjectForKey:kLAUserSignatureValuePerExtraDistrict];
    self.totalPlanOnSigning = [aDecoder decodeDoubleForKey:kLAUserSignatureTotalPlanOnSigning];
    self.lawyerId = [aDecoder decodeDoubleForKey:kLAUserSignatureLawyerId];
    self.totalDistrictsSigning = [aDecoder decodeDoubleForKey:kLAUserSignatureTotalDistrictsSigning];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAUserSignatureCreatedAt];
    self.vindiId = [aDecoder decodeObjectForKey:kLAUserSignatureVindiId];
    self.paymentMethod = [aDecoder decodeObjectForKey:kLAUserSignaturePaymentMethod];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAUserSignatureDeletedAt];
    self.lawyer = [aDecoder decodeObjectForKey:kLAUserSignatureLawyer];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAUserSignatureUpdatedAt];
    self.status = [aDecoder decodeObjectForKey:kLAUserSignatureStatus];
    self.totalThesesOnSigning = [aDecoder decodeDoubleForKey:kLAUserSignatureTotalThesesOnSigning];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_signatureIdentifier forKey:kLAUserSignatureId];
    [aCoder encodeDouble:_planId forKey:kLAUserSignaturePlanId];
    [aCoder encodeDouble:_valuePlanOnSigning forKey:kLAUserSignatureValuePlanOnSigning];
    [aCoder encodeObject:_valuePerExtraDistrict forKey:kLAUserSignatureValuePerExtraDistrict];
    [aCoder encodeDouble:_totalPlanOnSigning forKey:kLAUserSignatureTotalPlanOnSigning];
    [aCoder encodeDouble:_lawyerId forKey:kLAUserSignatureLawyerId];
    [aCoder encodeDouble:_totalDistrictsSigning forKey:kLAUserSignatureTotalDistrictsSigning];
    [aCoder encodeDouble:_createdAt forKey:kLAUserSignatureCreatedAt];
    [aCoder encodeObject:_vindiId forKey:kLAUserSignatureVindiId];
    [aCoder encodeObject:_paymentMethod forKey:kLAUserSignaturePaymentMethod];
    [aCoder encodeObject:_deletedAt forKey:kLAUserSignatureDeletedAt];
    [aCoder encodeObject:_lawyer forKey:kLAUserSignatureLawyer];
    [aCoder encodeDouble:_updatedAt forKey:kLAUserSignatureUpdatedAt];
    [aCoder encodeObject:_status forKey:kLAUserSignatureStatus];
    [aCoder encodeDouble:_totalThesesOnSigning forKey:kLAUserSignatureTotalThesesOnSigning];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAUserSignature *copy = [[LAUserSignature alloc] init];
    
    if (copy) {
        
        copy.signatureIdentifier = self.signatureIdentifier;
        copy.planId = self.planId;
        copy.valuePlanOnSigning = self.valuePlanOnSigning;
        copy.valuePerExtraDistrict = [self.valuePerExtraDistrict copyWithZone:zone];
        copy.totalPlanOnSigning = self.totalPlanOnSigning;
        copy.lawyerId = self.lawyerId;
        copy.totalDistrictsSigning = self.totalDistrictsSigning;
        copy.createdAt = self.createdAt;
        copy.vindiId = [self.vindiId copyWithZone:zone];
        copy.paymentMethod = [self.paymentMethod copyWithZone:zone];
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.lawyer = [self.lawyer copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.status = [self.status copyWithZone:zone];
        copy.totalThesesOnSigning = self.totalThesesOnSigning;
    }
    
    return copy;
}


@end

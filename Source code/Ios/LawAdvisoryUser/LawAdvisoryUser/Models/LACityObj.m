//
//  LACityObj.m
//  LawAdvisoryUser
//
//  Created by MAC on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LACityObj.h"

@implementation LACityObj
- (BOOL)isEqual:(id)object
{
    return [self.cityName isEqualToString:((LACityObj*)object).cityName];
}
@end

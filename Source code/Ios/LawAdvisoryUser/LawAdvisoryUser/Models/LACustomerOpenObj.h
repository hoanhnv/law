//
//  LAData.h
//
//  Created by   on 11/25/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LAUserObj.h"
#import "LAHappeningObj.h"
#import "LARefusalObj.h"

@interface LACustomerOpenObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double rating;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double happeningId;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, assign) double lawyerId;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double userId;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double refusalId;
@property (nonatomic, strong) NSString *refusalDescription;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *statusDescription;
@property (nonatomic, strong) LAUserObj *user;
@property (nonatomic, strong) NSString *processNumber;
@property (nonatomic, strong) LAHappeningObj *happening;
@property (nonatomic, strong) LARefusalObj *lawsuitRefusalReason;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

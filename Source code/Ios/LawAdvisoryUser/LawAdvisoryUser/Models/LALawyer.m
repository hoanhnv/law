//
//  LALawyer.m
//
//  Created by   on 10/11/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawyer.h"


NSString *const kLALawyerId = @"id";
NSString *const kLALawyerUfOab = @"uf_oab";
NSString *const kLALawyerGroundForRefusal = @"ground_for_refusal";
NSString *const kLALawyerLinkedin = @"linkedin";
NSString *const kLALawyerCreatedAt = @"created_at";
NSString *const kLALawyerImageOab = @"image_oab";
NSString *const kLALawyerUserId = @"user_id";
NSString *const kLALawyerDeletedAt = @"deleted_at";
NSString *const kLALawyerAbout = @"about";
NSString *const kLALawyerImageHouseProof = @"image_house_proof";
NSString *const kLALawyerImageAvatar = @"image_avatar";
NSString *const kLALawyerImageOabWithUrl = @"image_oab_with_url";
NSString *const kLALawyerUpdatedAt = @"updated_at";
NSString *const kLALawyerNumberOab = @"number_oab";
NSString *const kLALawyerSite = @"site";
NSString *const kLALawyerImageAvatarWithUrl = @"image_avatar_with_url";
NSString *const kLALawyerImageHouseProofWithUrl = @"image_house_proof_with_url";
NSString *const kimage_oab_verse = @"image_oab_verse";
NSString *const kimage_oab_verse_with_url = @"image_oab_verse_with_url";
NSString *const kbank_account = @"bank_account";
NSString *const kbank_account_dac = @"bank_account_dac";
NSString *const kbank_agency = @"bank_agency";
NSString *const kbank_id = @"bank_id";
NSString *const krating = @"rating";
NSString *const kresponse_time = @"response_time";

@interface LALawyer ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawyer

@synthesize lawyerIdentifier = _lawyerIdentifier;
@synthesize ufOab = _ufOab;
@synthesize groundForRefusal = _groundForRefusal;
@synthesize linkedin = _linkedin;
@synthesize createdAt = _createdAt;
@synthesize imageOab = _imageOab;
@synthesize userId = _userId;
@synthesize deletedAt = _deletedAt;
@synthesize about = _about;
@synthesize imageHouseProof = _imageHouseProof;
@synthesize imageAvatar = _imageAvatar;
@synthesize imageOabWithUrl = _imageOabWithUrl;
@synthesize updatedAt = _updatedAt;
@synthesize numberOab = _numberOab;
@synthesize site = _site;
@synthesize imageAvatarWithUrl = _imageAvatarWithUrl;
@synthesize imageHouseProofWithUrl = _imageHouseProofWithUrl;
@synthesize imageOabVerse = _imageOabVerse;
@synthesize imageOabVerseWithUrl = _imageOabVerseWithUrl;
@synthesize bank_id = _bank_id;
@synthesize bank_agency = _bank_agency;
@synthesize bank_account = _bank_account;
@synthesize bank_account_dac = _bank_account_dac;
@synthesize rating = _rating;
@synthesize response_time = _response_time;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.lawyerIdentifier = [[self objectOrNilForKey:kLALawyerId fromDictionary:dict] doubleValue];
        self.ufOab = [self objectOrNilForKey:kLALawyerUfOab fromDictionary:dict];
        self.groundForRefusal = [self objectOrNilForKey:kLALawyerGroundForRefusal fromDictionary:dict];
        self.linkedin = [self objectOrNilForKey:kLALawyerLinkedin fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLALawyerCreatedAt fromDictionary:dict] doubleValue];
        self.imageOab = [self objectOrNilForKey:kLALawyerImageOab fromDictionary:dict];
        self.userId = [[self objectOrNilForKey:kLALawyerUserId fromDictionary:dict] doubleValue];
        self.deletedAt = [[self objectOrNilForKey:kLALawyerDeletedAt fromDictionary:dict] doubleValue];
        self.about = [self objectOrNilForKey:kLALawyerAbout fromDictionary:dict];
        self.imageHouseProof = [self objectOrNilForKey:kLALawyerImageHouseProof fromDictionary:dict];
        self.imageAvatar = [self objectOrNilForKey:kLALawyerImageAvatar fromDictionary:dict];
        self.imageOabWithUrl = [self objectOrNilForKey:kLALawyerImageOabWithUrl fromDictionary:dict];
        
        self.imageOabVerseWithUrl = [self objectOrNilForKey:kimage_oab_verse_with_url fromDictionary:dict];
        self.imageOabVerse = [self objectOrNilForKey:kimage_oab_verse fromDictionary:dict];
        
        self.updatedAt = [[self objectOrNilForKey:kLALawyerUpdatedAt fromDictionary:dict] doubleValue];
        self.numberOab = [self objectOrNilForKey:kLALawyerNumberOab fromDictionary:dict];
        self.site = [self objectOrNilForKey:kLALawyerSite fromDictionary:dict];
        self.imageAvatarWithUrl = [self objectOrNilForKey:kLALawyerImageAvatarWithUrl fromDictionary:dict];
        self.imageHouseProofWithUrl = [self objectOrNilForKey:kLALawyerImageHouseProofWithUrl fromDictionary:dict];
        self.bank_account_dac = [self objectOrNilForKey:kbank_account_dac fromDictionary:dict];
        self.bank_account = [self objectOrNilForKey:kbank_account fromDictionary:dict];
        self.bank_agency = [self objectOrNilForKey:kbank_agency fromDictionary:dict];
        self.bank_id = [[self objectOrNilForKey:kbank_id fromDictionary:dict] doubleValue];
        self.rating = [[self objectOrNilForKey:krating fromDictionary:dict] doubleValue];
        self.response_time = [[self objectOrNilForKey:kresponse_time fromDictionary:dict] doubleValue];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerIdentifier] forKey:kLALawyerId];
    [mutableDict setValue:self.ufOab forKey:kLALawyerUfOab];
    [mutableDict setValue:self.groundForRefusal forKey:kLALawyerGroundForRefusal];
    [mutableDict setValue:self.linkedin forKey:kLALawyerLinkedin];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLALawyerCreatedAt];
    [mutableDict setValue:self.imageOab forKey:kLALawyerImageOab];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLALawyerUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deletedAt] forKey:kLALawyerDeletedAt];
    [mutableDict setValue:self.about forKey:kLALawyerAbout];
    [mutableDict setValue:self.imageHouseProof forKey:kLALawyerImageHouseProof];
    [mutableDict setValue:self.imageAvatar forKey:kLALawyerImageAvatar];
    [mutableDict setValue:self.imageOabWithUrl forKey:kLALawyerImageOabWithUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLALawyerUpdatedAt];
    [mutableDict setValue:self.numberOab forKey:kLALawyerNumberOab];
    [mutableDict setValue:self.site forKey:kLALawyerSite];
    [mutableDict setValue:self.imageAvatarWithUrl forKey:kLALawyerImageAvatarWithUrl];
    [mutableDict setValue:self.imageHouseProofWithUrl forKey:kLALawyerImageHouseProofWithUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.response_time] forKey:kresponse_time];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rating] forKey:krating];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.lawyerIdentifier = [aDecoder decodeDoubleForKey:kLALawyerId];
    self.ufOab = [aDecoder decodeObjectForKey:kLALawyerUfOab];
    self.groundForRefusal = [aDecoder decodeObjectForKey:kLALawyerGroundForRefusal];
    self.linkedin = [aDecoder decodeObjectForKey:kLALawyerLinkedin];
    self.createdAt = [aDecoder decodeDoubleForKey:kLALawyerCreatedAt];
    self.imageOab = [aDecoder decodeObjectForKey:kLALawyerImageOab];
    self.userId = [aDecoder decodeDoubleForKey:kLALawyerUserId];
    //    self.deletedAt = [aDecoder decodeObjectForKey:kLALawyerDeletedAt];
    self.about = [aDecoder decodeObjectForKey:kLALawyerAbout];
    self.imageHouseProof = [aDecoder decodeObjectForKey:kLALawyerImageHouseProof];
    self.imageAvatar = [aDecoder decodeObjectForKey:kLALawyerImageAvatar];
    self.imageOabWithUrl = [aDecoder decodeObjectForKey:kLALawyerImageOabWithUrl];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLALawyerUpdatedAt];
    self.numberOab = [aDecoder decodeObjectForKey:kLALawyerNumberOab];
    self.site = [aDecoder decodeObjectForKey:kLALawyerSite];
    self.imageAvatarWithUrl = [aDecoder decodeObjectForKey:kLALawyerImageAvatarWithUrl];
    self.imageHouseProofWithUrl = [aDecoder decodeObjectForKey:kLALawyerImageHouseProofWithUrl];
    self.response_time = [aDecoder decodeDoubleForKey:kresponse_time];
    self.rating = [aDecoder decodeDoubleForKey:krating];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_lawyerIdentifier forKey:kLALawyerId];
    [aCoder encodeObject:_ufOab forKey:kLALawyerUfOab];
    [aCoder encodeObject:_groundForRefusal forKey:kLALawyerGroundForRefusal];
    [aCoder encodeObject:_linkedin forKey:kLALawyerLinkedin];
    [aCoder encodeDouble:_createdAt forKey:kLALawyerCreatedAt];
    [aCoder encodeObject:_imageOab forKey:kLALawyerImageOab];
    [aCoder encodeDouble:_userId forKey:kLALawyerUserId];
    //    [aCoder encodeObject:_deletedAt forKey:kLALawyerDeletedAt];
    [aCoder encodeObject:_about forKey:kLALawyerAbout];
    [aCoder encodeObject:_imageHouseProof forKey:kLALawyerImageHouseProof];
    [aCoder encodeObject:_imageAvatar forKey:kLALawyerImageAvatar];
    [aCoder encodeObject:_imageOabWithUrl forKey:kLALawyerImageOabWithUrl];
    [aCoder encodeDouble:_updatedAt forKey:kLALawyerUpdatedAt];
    [aCoder encodeObject:_numberOab forKey:kLALawyerNumberOab];
    [aCoder encodeObject:_site forKey:kLALawyerSite];
    [aCoder encodeObject:_imageAvatarWithUrl forKey:kLALawyerImageAvatarWithUrl];
    [aCoder encodeObject:_imageHouseProofWithUrl forKey:kLALawyerImageHouseProofWithUrl];
    [aCoder encodeDouble:_response_time forKey:kresponse_time];
    [aCoder encodeDouble:_rating forKey:krating];
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawyer *copy = [[LALawyer alloc] init];
    
    if (copy) {
        
        copy.lawyerIdentifier = self.lawyerIdentifier;
        copy.ufOab = [self.ufOab copyWithZone:zone];
        copy.groundForRefusal = [self.groundForRefusal copyWithZone:zone];
        copy.linkedin = [self.linkedin copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.imageOab = [self.imageOab copyWithZone:zone];
        copy.userId = self.userId;
        //        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.about = [self.about copyWithZone:zone];
        copy.imageHouseProof = [self.imageHouseProof copyWithZone:zone];
        copy.imageAvatar = [self.imageAvatar copyWithZone:zone];
        copy.imageOabWithUrl = [self.imageOabWithUrl copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.numberOab = [self.numberOab copyWithZone:zone];
        copy.site = [self.site copyWithZone:zone];
        copy.imageAvatarWithUrl = [self.imageAvatarWithUrl copyWithZone:zone];
        copy.imageHouseProofWithUrl = [self.imageHouseProofWithUrl copyWithZone:zone];
        copy.response_time = self.response_time;
        copy.rating = self.rating;
    }
    
    return copy;
}


@end

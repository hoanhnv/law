//
//  LACustomerOpenObj.m
//
//  Created by   on 11/25/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LACustomerOpenObj.h"
#import "LAUserObj.h"
#import "LAHappeningObj.h"
#import "LARefusalObj.h"


NSString *const kLACustomerOpenObjRating = @"rating";
NSString *const kLACustomerOpenObjId = @"id";
NSString *const kLACustomerOpenObjHappeningId = @"happening_id";
NSString *const kLACustomerOpenObjStatus = @"status";
NSString *const kLACustomerOpenObjLawyerId = @"lawyer_id";
NSString *const kLACustomerOpenObjCreatedAt = @"created_at";
NSString *const kLACustomerOpenObjUserId = @"user_id";
NSString *const kLACustomerOpenObjDeletedAt = @"deleted_at";
NSString *const kLACustomerOpenObjRefusalId = @"refusal_id";
NSString *const kLACustomerOpenObjRefusalDescription = @"refusal_description";
NSString *const kLACustomerOpenObjUpdatedAt = @"updated_at";
NSString *const kLACustomerOpenObjStatusDescription = @"status_description";
NSString *const kLACustomerOpenObjUser = @"user";
NSString *const kLACustomerOpenObjProcessNumber = @"process_number";
NSString *const kLACustomerOpenObjHappening = @"happening";
NSString *const kLACustomerOpenObjLawsuitRefusalReason = @"lawsuit_refusal_reason";



@interface LACustomerOpenObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LACustomerOpenObj

@synthesize rating = _rating;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize happeningId = _happeningId;
@synthesize status = _status;
@synthesize lawyerId = _lawyerId;
@synthesize createdAt = _createdAt;
@synthesize userId = _userId;
@synthesize deletedAt = _deletedAt;
@synthesize refusalId = _refusalId;
@synthesize refusalDescription = _refusalDescription;
@synthesize updatedAt = _updatedAt;
@synthesize statusDescription = _statusDescription;
@synthesize user = _user;
@synthesize processNumber = _processNumber;
@synthesize happening = _happening;
@synthesize lawsuitRefusalReason = _lawsuitRefusalReason;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.rating = [[self objectOrNilForKey:kLACustomerOpenObjRating fromDictionary:dict] doubleValue];
        self.dataIdentifier = [[self objectOrNilForKey:kLACustomerOpenObjId fromDictionary:dict] doubleValue];
        self.happeningId = [[self objectOrNilForKey:kLACustomerOpenObjHappeningId fromDictionary:dict] doubleValue];
        self.status = [self objectOrNilForKey:kLACustomerOpenObjStatus fromDictionary:dict];
        self.lawyerId = [[self objectOrNilForKey:kLACustomerOpenObjLawyerId fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLACustomerOpenObjCreatedAt fromDictionary:dict] doubleValue];
        self.userId = [[self objectOrNilForKey:kLACustomerOpenObjUserId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLACustomerOpenObjDeletedAt fromDictionary:dict];
        self.refusalId = [[self objectOrNilForKey:kLACustomerOpenObjRefusalId fromDictionary:dict] doubleValue];
        self.refusalDescription = [self objectOrNilForKey:kLACustomerOpenObjRefusalDescription fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLACustomerOpenObjUpdatedAt fromDictionary:dict] doubleValue];
        self.statusDescription = [self objectOrNilForKey:kLACustomerOpenObjStatusDescription fromDictionary:dict];
        self.user = [LAUserObj modelObjectWithDictionary:[dict objectForKey:kLACustomerOpenObjUser]];
        self.processNumber = [self objectOrNilForKey:kLACustomerOpenObjProcessNumber fromDictionary:dict];
        self.happening = [LAHappeningObj modelObjectWithDictionary:[dict objectForKey:kLACustomerOpenObjHappening]];
        self.lawsuitRefusalReason = [LARefusalObj modelObjectWithDictionary:[dict objectForKey:kLACustomerOpenObjLawsuitRefusalReason]];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rating] forKey:kLACustomerOpenObjRating];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLACustomerOpenObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.happeningId] forKey:kLACustomerOpenObjHappeningId];
    [mutableDict setValue:self.status forKey:kLACustomerOpenObjStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerId] forKey:kLACustomerOpenObjLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLACustomerOpenObjCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLACustomerOpenObjUserId];
    [mutableDict setValue:self.deletedAt forKey:kLACustomerOpenObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.refusalId] forKey:kLACustomerOpenObjRefusalId];
    [mutableDict setValue:self.refusalDescription forKey:kLACustomerOpenObjRefusalDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLACustomerOpenObjUpdatedAt];
    [mutableDict setValue:self.statusDescription forKey:kLACustomerOpenObjStatusDescription];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kLACustomerOpenObjUser];
    [mutableDict setValue:self.processNumber forKey:kLACustomerOpenObjProcessNumber];
    [mutableDict setValue:[self.happening dictionaryRepresentation] forKey:kLACustomerOpenObjHappening];
    [mutableDict setValue:[self.lawsuitRefusalReason dictionaryRepresentation] forKey:kLACustomerOpenObjLawsuitRefusalReason];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.rating = [aDecoder decodeDoubleForKey:kLACustomerOpenObjRating];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLACustomerOpenObjId];
    self.happeningId = [aDecoder decodeDoubleForKey:kLACustomerOpenObjHappeningId];
    self.status = [aDecoder decodeObjectForKey:kLACustomerOpenObjStatus];
    self.lawyerId = [aDecoder decodeDoubleForKey:kLACustomerOpenObjLawyerId];
    self.createdAt = [aDecoder decodeDoubleForKey:kLACustomerOpenObjCreatedAt];
    self.userId = [aDecoder decodeDoubleForKey:kLACustomerOpenObjUserId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLACustomerOpenObjDeletedAt];
    self.refusalId = [aDecoder decodeDoubleForKey:kLACustomerOpenObjRefusalId];
    self.refusalDescription = [aDecoder decodeObjectForKey:kLACustomerOpenObjRefusalDescription];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLACustomerOpenObjUpdatedAt];
    self.statusDescription = [aDecoder decodeObjectForKey:kLACustomerOpenObjStatusDescription];
    self.user = [aDecoder decodeObjectForKey:kLACustomerOpenObjUser];
    self.processNumber = [aDecoder decodeObjectForKey:kLACustomerOpenObjProcessNumber];
    self.happening = [aDecoder decodeObjectForKey:kLACustomerOpenObjHappening];
    self.lawsuitRefusalReason = [aDecoder decodeObjectForKey:kLACustomerOpenObjLawsuitRefusalReason];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_rating forKey:kLACustomerOpenObjRating];
    [aCoder encodeDouble:_dataIdentifier forKey:kLACustomerOpenObjId];
    [aCoder encodeDouble:_happeningId forKey:kLACustomerOpenObjHappeningId];
    [aCoder encodeObject:_status forKey:kLACustomerOpenObjStatus];
    [aCoder encodeDouble:_lawyerId forKey:kLACustomerOpenObjLawyerId];
    [aCoder encodeDouble:_createdAt forKey:kLACustomerOpenObjCreatedAt];
    [aCoder encodeDouble:_userId forKey:kLACustomerOpenObjUserId];
    [aCoder encodeObject:_deletedAt forKey:kLACustomerOpenObjDeletedAt];
    [aCoder encodeDouble:_refusalId forKey:kLACustomerOpenObjRefusalId];
    [aCoder encodeObject:_refusalDescription forKey:kLACustomerOpenObjRefusalDescription];
    [aCoder encodeDouble:_updatedAt forKey:kLACustomerOpenObjUpdatedAt];
    [aCoder encodeObject:_statusDescription forKey:kLACustomerOpenObjStatusDescription];
    [aCoder encodeObject:_user forKey:kLACustomerOpenObjUser];
    [aCoder encodeObject:_processNumber forKey:kLACustomerOpenObjProcessNumber];
    [aCoder encodeObject:_happening forKey:kLACustomerOpenObjHappening];
    [aCoder encodeObject:_lawsuitRefusalReason forKey:kLACustomerOpenObjLawsuitRefusalReason];
}

- (id)copyWithZone:(NSZone *)zone
{
    LACustomerOpenObj *copy = [[LACustomerOpenObj alloc] init];
    
    if (copy) {
        
        copy.rating = self.rating;
        copy.dataIdentifier = self.dataIdentifier;
        copy.happeningId = self.happeningId;
        copy.status = [self.status copyWithZone:zone];
        copy.lawyerId = self.lawyerId;
        copy.createdAt = self.createdAt;
        copy.userId = self.userId;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.refusalId = self.refusalId;
        copy.refusalDescription = [self.refusalDescription copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.statusDescription = [self.statusDescription copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
        copy.processNumber = [self.processNumber copyWithZone:zone];
        copy.happening = [self.happening copyWithZone:zone];
        copy.refusalDescription = [self.refusalDescription copyWithZone:zone];
    }
    
    return copy;
}


@end

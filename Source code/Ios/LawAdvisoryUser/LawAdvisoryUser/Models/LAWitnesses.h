//
//  LAWitnesses.h
//
//  Created by   on 11/19/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LAWitnesses : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double witnessesIdentifier;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double happeningId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

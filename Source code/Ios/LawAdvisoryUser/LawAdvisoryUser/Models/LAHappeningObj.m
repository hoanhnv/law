//
//  LAHappeningObj.m
//
//  Created by   on 11/22/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAHappeningObj.h"
#import "LAWitnesses.h"
#import "LAMedias.h"
#import "LALawsuitObj.h"

NSString *const kLAHappeningObjThesisId = @"thesis_id";
NSString *const kLAHappeningObjMedias = @"medias";
NSString *const kLAHappeningObjThesis = @"thesis";
NSString *const kLAHappeningObjId = @"id";
NSString *const kLAHappeningObjDeletedAt = @"deleted_at";
NSString *const kLAHappeningObjCreatedAt = @"created_at";
NSString *const kLAHappeningObjOtherInformation = @"other_information";
NSString *const kLAHappeningObjUserId = @"user_id";
NSString *const kLAHappeningObjUpdatedAt = @"updated_at";
NSString *const kLAHappeningObjWitnesses = @"witnesses";


@interface LAHappeningObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAHappeningObj

@synthesize thesisId = _thesisId;
@synthesize medias = _medias;
@synthesize thesis = _thesis;
@synthesize happeningIdentifier = _happeningIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize createdAt = _createdAt;
@synthesize otherInformation = _otherInformation;
@synthesize userId = _userId;
@synthesize updatedAt = _updatedAt;
@synthesize witnesses = _witnesses;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.thesisId = [[self objectOrNilForKey:kLAHappeningObjThesisId fromDictionary:dict] doubleValue];
        NSObject *receivedLAMedias = [dict objectForKey:kLAHappeningObjMedias];
        NSMutableArray *parsedLAMedias = [NSMutableArray array];
        if ([receivedLAMedias isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAMedias) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAMedias addObject:[LAMedias modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAMedias isKindOfClass:[NSDictionary class]]) {
            [parsedLAMedias addObject:[LAMedias modelObjectWithDictionary:(NSDictionary *)receivedLAMedias]];
        }
        
        self.medias = [NSArray arrayWithArray:parsedLAMedias];
        self.thesis = [LALawsuitObj modelObjectWithDictionary:[dict objectForKey:kLAHappeningObjThesis]];
        self.happeningIdentifier = [[self objectOrNilForKey:kLAHappeningObjId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLAHappeningObjDeletedAt fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAHappeningObjCreatedAt fromDictionary:dict] doubleValue];
        self.otherInformation = [self objectOrNilForKey:kLAHappeningObjOtherInformation fromDictionary:dict];
        self.userId = [[self objectOrNilForKey:kLAHappeningObjUserId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLAHappeningObjUpdatedAt fromDictionary:dict] doubleValue];
        NSObject *receivedLAWitnesses = [dict objectForKey:kLAHappeningObjWitnesses];
        NSMutableArray *parsedLAWitnesses = [NSMutableArray array];
        if ([receivedLAWitnesses isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAWitnesses) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAWitnesses addObject:[LAWitnesses modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAWitnesses isKindOfClass:[NSDictionary class]]) {
            [parsedLAWitnesses addObject:[LAWitnesses modelObjectWithDictionary:(NSDictionary *)receivedLAWitnesses]];
        }
        
        self.witnesses = [NSArray arrayWithArray:parsedLAWitnesses];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.thesisId] forKey:kLAHappeningObjThesisId];
    NSMutableArray *tempArrayForMedias = [NSMutableArray array];
    for (NSObject *subArrayObject in self.medias) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForMedias addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForMedias addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForMedias] forKey:kLAHappeningObjMedias];
    [mutableDict setValue:self.thesis forKey:kLAHappeningObjThesis];
    [mutableDict setValue:[NSNumber numberWithDouble:self.happeningIdentifier] forKey:kLAHappeningObjId];
    [mutableDict setValue:self.deletedAt forKey:kLAHappeningObjDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAHappeningObjCreatedAt];
    [mutableDict setValue:self.otherInformation forKey:kLAHappeningObjOtherInformation];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLAHappeningObjUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAHappeningObjUpdatedAt];
    NSMutableArray *tempArrayForWitnesses = [NSMutableArray array];
    for (NSObject *subArrayObject in self.witnesses) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForWitnesses addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForWitnesses addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForWitnesses] forKey:kLAHappeningObjWitnesses];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.thesisId = [aDecoder decodeDoubleForKey:kLAHappeningObjThesisId];
    self.medias = [aDecoder decodeObjectForKey:kLAHappeningObjMedias];
    self.thesis = [aDecoder decodeObjectForKey:kLAHappeningObjThesis];
    self.happeningIdentifier = [aDecoder decodeDoubleForKey:kLAHappeningObjId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAHappeningObjDeletedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAHappeningObjCreatedAt];
    self.otherInformation = [aDecoder decodeObjectForKey:kLAHappeningObjOtherInformation];
    self.userId = [aDecoder decodeDoubleForKey:kLAHappeningObjUserId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAHappeningObjUpdatedAt];
    self.witnesses = [aDecoder decodeObjectForKey:kLAHappeningObjWitnesses];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_thesisId forKey:kLAHappeningObjThesisId];
    [aCoder encodeObject:_medias forKey:kLAHappeningObjMedias];
    [aCoder encodeObject:_thesis forKey:kLAHappeningObjThesis];
    [aCoder encodeDouble:_happeningIdentifier forKey:kLAHappeningObjId];
    [aCoder encodeObject:_deletedAt forKey:kLAHappeningObjDeletedAt];
    [aCoder encodeDouble:_createdAt forKey:kLAHappeningObjCreatedAt];
    [aCoder encodeObject:_otherInformation forKey:kLAHappeningObjOtherInformation];
    [aCoder encodeDouble:_userId forKey:kLAHappeningObjUserId];
    [aCoder encodeDouble:_updatedAt forKey:kLAHappeningObjUpdatedAt];
    [aCoder encodeObject:_witnesses forKey:kLAHappeningObjWitnesses];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAHappeningObj *copy = [[LAHappeningObj alloc] init];
    
    if (copy) {
        
        copy.thesisId = self.thesisId;
        copy.medias = [self.medias copyWithZone:zone];
        copy.thesis = [self.thesis copyWithZone:zone];
        copy.happeningIdentifier = self.happeningIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.otherInformation = [self.otherInformation copyWithZone:zone];
        copy.userId = self.userId;
        copy.updatedAt = self.updatedAt;
        copy.witnesses = [self.witnesses copyWithZone:zone];
    }
    
    return copy;
}


@end

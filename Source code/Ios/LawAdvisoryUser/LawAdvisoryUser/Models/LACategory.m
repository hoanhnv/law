//
//  LACategory.m
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LACategory.h"


NSString *const kLACategoryDeletedAt = @"deleted_at";
NSString *const kLACategoryId = @"id";
NSString *const kLACategoryUpdatedAt = @"updated_at";
NSString *const kLACategoryName = @"name";
NSString *const kLACategoryDescription = @"description";
NSString *const kLACategoryCreatedAt = @"created_at";


@interface LACategory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LACategory

@synthesize deletedAt = _deletedAt;
@synthesize categoryIdentifier = _categoryIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize categoryDescription = _categoryDescription;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [self objectOrNilForKey:kLACategoryDeletedAt fromDictionary:dict];
        self.categoryIdentifier = [[self objectOrNilForKey:kLACategoryId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLACategoryUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLACategoryName fromDictionary:dict];
        self.categoryDescription = [self objectOrNilForKey:kLACategoryDescription fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLACategoryCreatedAt fromDictionary:dict] doubleValue];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLACategoryDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryIdentifier] forKey:kLACategoryId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLACategoryUpdatedAt];
    [mutableDict setValue:self.name forKey:kLACategoryName];
    [mutableDict setValue:self.categoryDescription forKey:kLACategoryDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLACategoryCreatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.deletedAt = [aDecoder decodeObjectForKey:kLACategoryDeletedAt];
    self.categoryIdentifier = [aDecoder decodeDoubleForKey:kLACategoryId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLACategoryUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLACategoryName];
    self.categoryDescription = [aDecoder decodeObjectForKey:kLACategoryDescription];
    self.createdAt = [aDecoder decodeDoubleForKey:kLACategoryCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_deletedAt forKey:kLACategoryDeletedAt];
    [aCoder encodeDouble:_categoryIdentifier forKey:kLACategoryId];
    [aCoder encodeDouble:_updatedAt forKey:kLACategoryUpdatedAt];
    [aCoder encodeObject:_name forKey:kLACategoryName];
    [aCoder encodeObject:_categoryDescription forKey:kLACategoryDescription];
    [aCoder encodeDouble:_createdAt forKey:kLACategoryCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LACategory *copy = [[LACategory alloc] init];
    
    if (copy) {
        
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.categoryIdentifier = self.categoryIdentifier;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.categoryDescription = [self.categoryDescription copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}
@end


@interface LATheiseCategory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LATheiseCategory

@synthesize deletedAt = _deletedAt;
@synthesize categoryIdentifier = _categoryIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize categoryDescription = _categoryDescription;
@synthesize createdAt = _createdAt;
@synthesize color = _color;
@synthesize icon = _icon;
@synthesize icon_with_url = _icon_with_url;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.deletedAt = [self objectOrNilForKey:kLACategoryDeletedAt fromDictionary:dict];
        self.categoryIdentifier = [[self objectOrNilForKey:kLACategoryId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLACategoryUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLACategoryName fromDictionary:dict];
        self.categoryDescription = [self objectOrNilForKey:kLACategoryDescription fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLACategoryCreatedAt fromDictionary:dict] doubleValue];
        self.color = [self objectOrNilForKey:@"color" fromDictionary:dict];
        self.icon_with_url = [self objectOrNilForKey:@"icon_with_url" fromDictionary:dict];
        self.icon = [self objectOrNilForKey:@"icon" fromDictionary:dict];
    }
    
    return self;
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.deletedAt = [aDecoder decodeObjectForKey:kLACategoryDeletedAt];
    self.categoryIdentifier = [aDecoder decodeDoubleForKey:kLACategoryId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLACategoryUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLACategoryName];
    self.categoryDescription = [aDecoder decodeObjectForKey:kLACategoryDescription];
    self.createdAt = [aDecoder decodeDoubleForKey:kLACategoryCreatedAt];
    self.color = [aDecoder decodeObjectForKey:@"color"];
    self.icon = [aDecoder decodeObjectForKey:@"icon"];
    self.icon_with_url = [aDecoder decodeObjectForKey:@"icon_with_url"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_deletedAt forKey:kLACategoryDeletedAt];
    [aCoder encodeDouble:_categoryIdentifier forKey:kLACategoryId];
    [aCoder encodeDouble:_updatedAt forKey:kLACategoryUpdatedAt];
    [aCoder encodeObject:_name forKey:kLACategoryName];
    [aCoder encodeObject:_categoryDescription forKey:kLACategoryDescription];
    [aCoder encodeDouble:_createdAt forKey:kLACategoryCreatedAt];
    [aCoder encodeObject:@"color"];
    [aCoder encodeObject:@"icon"];
    [aCoder encodeObject:@"icon_with_url"];
}

- (id)copyWithZone:(NSZone *)zone
{
    LACategory *copy = [[LACategory alloc] init];
    
    if (copy) {
        
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.categoryIdentifier = self.categoryIdentifier;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.categoryDescription = [self.categoryDescription copyWithZone:zone];
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}
@end

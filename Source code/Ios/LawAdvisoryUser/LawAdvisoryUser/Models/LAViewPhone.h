//
//  LAViewPhone.h
//
//  Created by   on 10/11/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LAViewPhone : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double viewPhoneIdentifier;
@property (nonatomic, assign) double deletedAt;
@property (nonatomic, strong) NSString *showPhone;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *showEmail;
@property (nonatomic, assign) double userId;
@property (nonatomic, strong) NSString *showCellphone;
@property (nonatomic, assign) double updatedAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

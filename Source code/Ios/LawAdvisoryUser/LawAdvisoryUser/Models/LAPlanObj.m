//
//  LAData.m
//
//  Created by   on 10/20/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAPlanObj.h"


NSString *const kLAPlanObjId = @"id";
NSString *const kLAPlanObjDeletedAt = @"deleted_at";
NSString *const kLAPlanObjQuantityOfTheses = @"quantity_of_theses";
NSString *const kLAPlanObjPercentByDistrict = @"percent_by_district";
NSString *const kLAPlanVindiId = @"vindi_id";


NSString *const kLAPlanObjCreatedAt = @"created_at";
NSString *const kLAPlanObjValue = @"value";
NSString *const kLAPlanObjUpdatedAt = @"updated_at";
NSString *const kLAPlanObjName = @"name";


@interface LAPlanObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAPlanObj

@synthesize dataIdentifier = _dataIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize quantityOfTheses = _quantityOfTheses;
@synthesize createdAt = _createdAt;
@synthesize value = _value;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize percent_by_district = _percent_by_district;
@synthesize vindiId = _vindiId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.dataIdentifier = [[self objectOrNilForKey:kLAPlanObjId fromDictionary:dict] doubleValue];
        self.deletedAt = [[self objectOrNilForKey:kLAPlanObjDeletedAt fromDictionary:dict] doubleValue];
        self.quantityOfTheses = [[self objectOrNilForKey:kLAPlanObjQuantityOfTheses fromDictionary:dict] doubleValue];
        self.percent_by_district = [[self objectOrNilForKey:kLAPlanObjPercentByDistrict fromDictionary:dict] doubleValue];
        
        self.createdAt = [[self objectOrNilForKey:kLAPlanObjCreatedAt fromDictionary:dict] doubleValue];
        self.value = [[self objectOrNilForKey:kLAPlanObjValue fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLAPlanObjUpdatedAt fromDictionary:dict] doubleValue];
        self.name = [self objectOrNilForKey:kLAPlanObjName fromDictionary:dict];
        self.vindiId = [[self objectOrNilForKey:kLAPlanVindiId fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAPlanObjId];
    self.deletedAt = [aDecoder decodeDoubleForKey:kLAPlanObjDeletedAt];
    self.quantityOfTheses = [aDecoder decodeDoubleForKey:kLAPlanObjQuantityOfTheses];
    self.percent_by_district = [aDecoder decodeDoubleForKey:kLAPlanObjPercentByDistrict];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAPlanObjCreatedAt];
    self.value = [aDecoder decodeDoubleForKey:kLAPlanObjValue];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAPlanObjUpdatedAt];
    self.vindiId = [aDecoder decodeDoubleForKey:kLAPlanVindiId];
    self.name = [aDecoder decodeObjectForKey:kLAPlanObjName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_dataIdentifier forKey:kLAPlanObjId];
    [aCoder encodeDouble:_deletedAt forKey:kLAPlanObjDeletedAt];
    [aCoder encodeDouble:_quantityOfTheses forKey:kLAPlanObjQuantityOfTheses];
    [aCoder encodeDouble:_percent_by_district forKey:kLAPlanObjPercentByDistrict];
    [aCoder encodeDouble:_createdAt forKey:kLAPlanObjCreatedAt];
    [aCoder encodeDouble:_value forKey:kLAPlanObjValue];
    [aCoder encodeDouble:_updatedAt forKey:kLAPlanObjUpdatedAt];
    [aCoder encodeObject:_name forKey:kLAPlanObjName];
    [aCoder encodeDouble:_vindiId forKey:kLAPlanVindiId];
}


- (id)copyWithZone:(NSZone *)zone
{
    LAPlanObj *copy = [[LAPlanObj alloc] init];
    
    if (copy) {
        
        copy.dataIdentifier = self.dataIdentifier;
        copy.deletedAt = self.deletedAt;
        copy.quantityOfTheses = self.quantityOfTheses;
        copy.percent_by_district = self.percent_by_district;
        copy.createdAt = self.createdAt;
        copy.value = self.value;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.vindiId = self.vindiId;
    }
    
    return copy;
}


@end

//
//  LAActionResponseInfo.m
//
//  Created by   on 11/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAActionResponseInfo.h"
#import "LAActionDataObj.h"


NSString *const kLAActionResponseInfoSuccess = @"success";
NSString *const kLAActionResponseInfoData = @"data";
NSString *const kLAActionResponseInfoMessage = @"message";


@interface LAActionResponseInfo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAActionResponseInfo

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAActionResponseInfoSuccess fromDictionary:dict] boolValue];
        self.data = [LAActionDataObj modelObjectWithDictionary:[dict objectForKey:kLAActionResponseInfoData]];
        self.message = [self objectOrNilForKey:kLAActionResponseInfoMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAActionResponseInfoSuccess];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kLAActionResponseInfoData];
    [mutableDict setValue:self.message forKey:kLAActionResponseInfoMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAActionResponseInfoSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAActionResponseInfoData];
    self.message = [aDecoder decodeObjectForKey:kLAActionResponseInfoMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAActionResponseInfoSuccess];
    [aCoder encodeObject:_data forKey:kLAActionResponseInfoData];
    [aCoder encodeObject:_message forKey:kLAActionResponseInfoMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAActionResponseInfo *copy = [[LAActionResponseInfo alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

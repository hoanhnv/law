//
//  LAUserData.m
//
//  Created by   on 11/19/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAUserObj.h"


NSString *const kLAUserDataPostalCode = @"postal_code";
NSString *const kLAUserDataStatus = @"status";
NSString *const kLAUserDataImageRgWithUrl = @"image_rg_with_url";
NSString *const kLAUserDataUpdatedAt = @"updated_at";
NSString *const kLAUserDataDateOfBirth = @"date_of_birth";
NSString *const kLAUserDataCellphone = @"cellphone";
NSString *const kLAUserDataCity = @"city";
NSString *const kLAUserDataName = @"name";
NSString *const kLAUserDataId = @"id";
NSString *const kLAUserDataComplement = @"complement";
NSString *const kLAUserDataDeletedAt = @"deleted_at";
NSString *const kLAUserDataNeighborhood = @"neighborhood";
NSString *const kLAUserDataNumber = @"number";
NSString *const kLAUserDataPhone = @"phone";
NSString *const kLAUserDataEmail = @"email";
NSString *const kLAUserDataImageRg = @"image_rg";
NSString *const kLAUserDataCreatedAt = @"created_at";
NSString *const kLAUserDataCpf = @"cpf";
NSString *const kLAUserDataUf = @"uf";
NSString *const kLAUserDataAddress = @"address";
NSString *const kLAUserDataAcceptsNews = @"accepts_news";


@interface LAUserObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAUserObj

@synthesize postalCode = _postalCode;
@synthesize status = _status;
@synthesize imageRgWithUrl = _imageRgWithUrl;
@synthesize updatedAt = _updatedAt;
@synthesize dateOfBirth = _dateOfBirth;
@synthesize cellphone = _cellphone;
@synthesize city = _city;
@synthesize name = _name;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize complement = _complement;
@synthesize deletedAt = _deletedAt;
@synthesize neighborhood = _neighborhood;
@synthesize number = _number;
@synthesize phone = _phone;
@synthesize email = _email;
@synthesize imageRg = _imageRg;
@synthesize createdAt = _createdAt;
@synthesize cpf = _cpf;
@synthesize uf = _uf;
@synthesize address = _address;
@synthesize acceptsNews = _acceptsNews;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.postalCode = [self objectOrNilForKey:kLAUserDataPostalCode fromDictionary:dict];
        self.status = [self objectOrNilForKey:kLAUserDataStatus fromDictionary:dict];
        self.imageRgWithUrl = [self objectOrNilForKey:kLAUserDataImageRgWithUrl fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLAUserDataUpdatedAt fromDictionary:dict] doubleValue];
        self.dateOfBirth = [[self objectOrNilForKey:kLAUserDataDateOfBirth fromDictionary:dict] doubleValue];
        self.cellphone = [self objectOrNilForKey:kLAUserDataCellphone fromDictionary:dict];
        self.city = [self objectOrNilForKey:kLAUserDataCity fromDictionary:dict];
        self.name = [self objectOrNilForKey:kLAUserDataName fromDictionary:dict];
        self.dataIdentifier = [[self objectOrNilForKey:kLAUserDataId fromDictionary:dict] doubleValue];
        self.complement = [self objectOrNilForKey:kLAUserDataComplement fromDictionary:dict];
        self.deletedAt = [self objectOrNilForKey:kLAUserDataDeletedAt fromDictionary:dict];
        self.neighborhood = [self objectOrNilForKey:kLAUserDataNeighborhood fromDictionary:dict];
        self.number = [self objectOrNilForKey:kLAUserDataNumber fromDictionary:dict];
        self.phone = [self objectOrNilForKey:kLAUserDataPhone fromDictionary:dict];
        self.email = [self objectOrNilForKey:kLAUserDataEmail fromDictionary:dict];
        self.imageRg = [self objectOrNilForKey:kLAUserDataImageRg fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAUserDataCreatedAt fromDictionary:dict] doubleValue];
        self.cpf = [self objectOrNilForKey:kLAUserDataCpf fromDictionary:dict];
        self.uf = [self objectOrNilForKey:kLAUserDataUf fromDictionary:dict];
        self.address = [self objectOrNilForKey:kLAUserDataAddress fromDictionary:dict];
        self.acceptsNews = [self objectOrNilForKey:kLAUserDataAcceptsNews fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.postalCode forKey:kLAUserDataPostalCode];
    [mutableDict setValue:self.status forKey:kLAUserDataStatus];
    [mutableDict setValue:self.imageRgWithUrl forKey:kLAUserDataImageRgWithUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAUserDataUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dateOfBirth] forKey:kLAUserDataDateOfBirth];
    [mutableDict setValue:self.cellphone forKey:kLAUserDataCellphone];
    [mutableDict setValue:self.city forKey:kLAUserDataCity];
    [mutableDict setValue:self.name forKey:kLAUserDataName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLAUserDataId];
    [mutableDict setValue:self.complement forKey:kLAUserDataComplement];
    [mutableDict setValue:self.deletedAt forKey:kLAUserDataDeletedAt];
    [mutableDict setValue:self.neighborhood forKey:kLAUserDataNeighborhood];
    [mutableDict setValue:self.number forKey:kLAUserDataNumber];
    [mutableDict setValue:self.phone forKey:kLAUserDataPhone];
    [mutableDict setValue:self.email forKey:kLAUserDataEmail];
    [mutableDict setValue:self.imageRg forKey:kLAUserDataImageRg];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAUserDataCreatedAt];
    [mutableDict setValue:self.cpf forKey:kLAUserDataCpf];
    [mutableDict setValue:self.uf forKey:kLAUserDataUf];
    [mutableDict setValue:self.address forKey:kLAUserDataAddress];
    [mutableDict setValue:self.acceptsNews forKey:kLAUserDataAcceptsNews];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.postalCode = [aDecoder decodeObjectForKey:kLAUserDataPostalCode];
    self.status = [aDecoder decodeObjectForKey:kLAUserDataStatus];
    self.imageRgWithUrl = [aDecoder decodeObjectForKey:kLAUserDataImageRgWithUrl];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAUserDataUpdatedAt];
    self.dateOfBirth = [aDecoder decodeDoubleForKey:kLAUserDataDateOfBirth];
    self.cellphone = [aDecoder decodeObjectForKey:kLAUserDataCellphone];
    self.city = [aDecoder decodeObjectForKey:kLAUserDataCity];
    self.name = [aDecoder decodeObjectForKey:kLAUserDataName];
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLAUserDataId];
    self.complement = [aDecoder decodeObjectForKey:kLAUserDataComplement];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAUserDataDeletedAt];
    self.neighborhood = [aDecoder decodeObjectForKey:kLAUserDataNeighborhood];
    self.number = [aDecoder decodeObjectForKey:kLAUserDataNumber];
    self.phone = [aDecoder decodeObjectForKey:kLAUserDataPhone];
    self.email = [aDecoder decodeObjectForKey:kLAUserDataEmail];
    self.imageRg = [aDecoder decodeObjectForKey:kLAUserDataImageRg];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAUserDataCreatedAt];
    self.cpf = [aDecoder decodeObjectForKey:kLAUserDataCpf];
    self.uf = [aDecoder decodeObjectForKey:kLAUserDataUf];
    self.address = [aDecoder decodeObjectForKey:kLAUserDataAddress];
    self.acceptsNews = [aDecoder decodeObjectForKey:kLAUserDataAcceptsNews];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_postalCode forKey:kLAUserDataPostalCode];
    [aCoder encodeObject:_status forKey:kLAUserDataStatus];
    [aCoder encodeObject:_imageRgWithUrl forKey:kLAUserDataImageRgWithUrl];
    [aCoder encodeDouble:_updatedAt forKey:kLAUserDataUpdatedAt];
    [aCoder encodeDouble:_dateOfBirth forKey:kLAUserDataDateOfBirth];
    [aCoder encodeObject:_cellphone forKey:kLAUserDataCellphone];
    [aCoder encodeObject:_city forKey:kLAUserDataCity];
    [aCoder encodeObject:_name forKey:kLAUserDataName];
    [aCoder encodeDouble:_dataIdentifier forKey:kLAUserDataId];
    [aCoder encodeObject:_complement forKey:kLAUserDataComplement];
    [aCoder encodeObject:_deletedAt forKey:kLAUserDataDeletedAt];
    [aCoder encodeObject:_neighborhood forKey:kLAUserDataNeighborhood];
    [aCoder encodeObject:_number forKey:kLAUserDataNumber];
    [aCoder encodeObject:_phone forKey:kLAUserDataPhone];
    [aCoder encodeObject:_email forKey:kLAUserDataEmail];
    [aCoder encodeObject:_imageRg forKey:kLAUserDataImageRg];
    [aCoder encodeDouble:_createdAt forKey:kLAUserDataCreatedAt];
    [aCoder encodeObject:_cpf forKey:kLAUserDataCpf];
    [aCoder encodeObject:_uf forKey:kLAUserDataUf];
    [aCoder encodeObject:_address forKey:kLAUserDataAddress];
    [aCoder encodeObject:_acceptsNews forKey:kLAUserDataAcceptsNews];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAUserObj *copy = [[LAUserObj alloc] init];
    
    if (copy) {
        
        copy.postalCode = [self.postalCode copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.imageRgWithUrl = [self.imageRgWithUrl copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.dateOfBirth = self.dateOfBirth;
        copy.cellphone = [self.cellphone copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.dataIdentifier = self.dataIdentifier;
        copy.complement = [self.complement copyWithZone:zone];
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.neighborhood = [self.neighborhood copyWithZone:zone];
        copy.number = [self.number copyWithZone:zone];
        copy.phone = [self.phone copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.imageRg = [self.imageRg copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.cpf = [self.cpf copyWithZone:zone];
        copy.uf = [self.uf copyWithZone:zone];
        copy.address = [self.address copyWithZone:zone];
        copy.acceptsNews = [self.acceptsNews copyWithZone:zone];
    }
    
    return copy;
}


@end

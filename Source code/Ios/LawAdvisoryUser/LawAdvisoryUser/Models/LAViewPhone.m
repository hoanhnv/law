//
//  LAViewPhone.m
//
//  Created by   on 10/11/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAViewPhone.h"


NSString *const kLAViewPhoneId = @"id";
NSString *const kLAViewPhoneDeletedAt = @"deleted_at";
NSString *const kLAViewPhoneShowPhone = @"show_phone";
NSString *const kLAViewPhoneCreatedAt = @"created_at";
NSString *const kLAViewPhoneShowEmail = @"show_email";
NSString *const kLAViewPhoneUserId = @"user_id";
NSString *const kLAViewPhoneShowCellphone = @"show_cellphone";
NSString *const kLAViewPhoneUpdatedAt = @"updated_at";


@interface LAViewPhone ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAViewPhone

@synthesize viewPhoneIdentifier = _viewPhoneIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize showPhone = _showPhone;
@synthesize createdAt = _createdAt;
@synthesize showEmail = _showEmail;
@synthesize userId = _userId;
@synthesize showCellphone = _showCellphone;
@synthesize updatedAt = _updatedAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.viewPhoneIdentifier = [[self objectOrNilForKey:kLAViewPhoneId fromDictionary:dict] doubleValue];
        self.deletedAt = [[self objectOrNilForKey:kLAViewPhoneDeletedAt fromDictionary:dict] doubleValue];
        self.showPhone = [self objectOrNilForKey:kLAViewPhoneShowPhone fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLAViewPhoneCreatedAt fromDictionary:dict] doubleValue];
        self.showEmail = [self objectOrNilForKey:kLAViewPhoneShowEmail fromDictionary:dict];
        self.userId = [[self objectOrNilForKey:kLAViewPhoneUserId fromDictionary:dict] doubleValue];
        self.showCellphone = [self objectOrNilForKey:kLAViewPhoneShowCellphone fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLAViewPhoneUpdatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.viewPhoneIdentifier] forKey:kLAViewPhoneId];
    //    [mutableDict setValue:self.deletedAt forKey:kLAViewPhoneDeletedAt];
    [mutableDict setValue:self.showPhone forKey:kLAViewPhoneShowPhone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAViewPhoneCreatedAt];
    [mutableDict setValue:self.showEmail forKey:kLAViewPhoneShowEmail];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLAViewPhoneUserId];
    [mutableDict setValue:self.showCellphone forKey:kLAViewPhoneShowCellphone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAViewPhoneUpdatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.viewPhoneIdentifier = [aDecoder decodeDoubleForKey:kLAViewPhoneId];
    //    self.deletedAt = [aDecoder decodeObjectForKey:kLAViewPhoneDeletedAt];
    self.showPhone = [aDecoder decodeObjectForKey:kLAViewPhoneShowPhone];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAViewPhoneCreatedAt];
    self.showEmail = [aDecoder decodeObjectForKey:kLAViewPhoneShowEmail];
    self.userId = [aDecoder decodeDoubleForKey:kLAViewPhoneUserId];
    self.showCellphone = [aDecoder decodeObjectForKey:kLAViewPhoneShowCellphone];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAViewPhoneUpdatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_viewPhoneIdentifier forKey:kLAViewPhoneId];
    //    [aCoder encodeObject:_deletedAt forKey:kLAViewPhoneDeletedAt];
    [aCoder encodeObject:_showPhone forKey:kLAViewPhoneShowPhone];
    [aCoder encodeDouble:_createdAt forKey:kLAViewPhoneCreatedAt];
    [aCoder encodeObject:_showEmail forKey:kLAViewPhoneShowEmail];
    [aCoder encodeDouble:_userId forKey:kLAViewPhoneUserId];
    [aCoder encodeObject:_showCellphone forKey:kLAViewPhoneShowCellphone];
    [aCoder encodeDouble:_updatedAt forKey:kLAViewPhoneUpdatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAViewPhone *copy = [[LAViewPhone alloc] init];
    
    if (copy) {
        
        copy.viewPhoneIdentifier = self.viewPhoneIdentifier;
        //        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.showPhone = [self.showPhone copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.showEmail = [self.showEmail copyWithZone:zone];
        copy.userId = self.userId;
        copy.showCellphone = [self.showCellphone copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
    }
    
    return copy;
}


@end

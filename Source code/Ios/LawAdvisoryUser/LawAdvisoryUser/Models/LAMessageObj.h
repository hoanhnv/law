//
//  LAMessageObj.h
//
//  Created by   on 2/20/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LAUserObj.h"



@interface LAMessageObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *body;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, assign) double threadId;
@property (nonatomic, assign) double userId;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) LAUserObj *user;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

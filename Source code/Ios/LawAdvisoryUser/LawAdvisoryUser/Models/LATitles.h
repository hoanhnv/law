//
//  LATitles.h
//
//  Created by   on 10/14/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LATitles : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double titlesIdentifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double userId;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

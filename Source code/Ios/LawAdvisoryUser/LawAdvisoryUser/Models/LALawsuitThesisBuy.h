//
//  LALawsuitThesis.h
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LALawSuitCategory;

@interface LALawsuitThesisBuy : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *documentation;
@property (nonatomic, strong) LALawSuitCategory *category;
@property (nonatomic, assign) double lawsuitThesisIdentifier;
@property (nonatomic, assign) double version;
@property (nonatomic, strong) NSString *yourRights;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double categoryId;
@property (nonatomic, strong) NSString *legalName;
@property (nonatomic, strong) NSString *identification;
@property (nonatomic, strong) NSString *howToAct;
@property (nonatomic, assign) double valueContraction;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double situationId;
@property (nonatomic, assign) double valueSignature;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *status;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAMedias.m
//
//  Created by   on 11/22/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAMedias.h"


NSString *const kLAMediasId = @"id";
NSString *const kLAMediasDeletedAt = @"deleted_at";
NSString *const kLAMediasCreatedAt = @"created_at";
NSString *const kLAMediasMedia = @"media";
NSString *const kLAMediasHappeningId = @"happening_id";
NSString *const kLAMediasUpdatedAt = @"updated_at";
NSString *const kLAMediasType = @"type";
NSString *const kLAMediasMimeType = @"mime_type";
NSString *const kLAMediasMediaWithUrl = @"media_with_url";


@interface LAMedias ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAMedias

@synthesize mediasIdentifier = _mediasIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize createdAt = _createdAt;
@synthesize media = _media;
@synthesize happeningId = _happeningId;
@synthesize updatedAt = _updatedAt;
@synthesize type = _type;
@synthesize mimeType = _mimeType;
@synthesize mediaWithUrl = _mediaWithUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.mediasIdentifier = [[self objectOrNilForKey:kLAMediasId fromDictionary:dict] doubleValue];
            self.deletedAt = [self objectOrNilForKey:kLAMediasDeletedAt fromDictionary:dict];
            self.createdAt = [[self objectOrNilForKey:kLAMediasCreatedAt fromDictionary:dict] doubleValue];
            self.media = [self objectOrNilForKey:kLAMediasMedia fromDictionary:dict];
            self.happeningId = [[self objectOrNilForKey:kLAMediasHappeningId fromDictionary:dict] doubleValue];
            self.updatedAt = [[self objectOrNilForKey:kLAMediasUpdatedAt fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kLAMediasType fromDictionary:dict];
            self.mimeType = [self objectOrNilForKey:kLAMediasMimeType fromDictionary:dict];
            self.mediaWithUrl = [self objectOrNilForKey:kLAMediasMediaWithUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mediasIdentifier] forKey:kLAMediasId];
    [mutableDict setValue:self.deletedAt forKey:kLAMediasDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAMediasCreatedAt];
    [mutableDict setValue:self.media forKey:kLAMediasMedia];
    [mutableDict setValue:[NSNumber numberWithDouble:self.happeningId] forKey:kLAMediasHappeningId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAMediasUpdatedAt];
    [mutableDict setValue:self.type forKey:kLAMediasType];
    [mutableDict setValue:self.mimeType forKey:kLAMediasMimeType];
    [mutableDict setValue:self.mediaWithUrl forKey:kLAMediasMediaWithUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.mediasIdentifier = [aDecoder decodeDoubleForKey:kLAMediasId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAMediasDeletedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAMediasCreatedAt];
    self.media = [aDecoder decodeObjectForKey:kLAMediasMedia];
    self.happeningId = [aDecoder decodeDoubleForKey:kLAMediasHappeningId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAMediasUpdatedAt];
    self.type = [aDecoder decodeObjectForKey:kLAMediasType];
    self.mimeType = [aDecoder decodeObjectForKey:kLAMediasMimeType];
    self.mediaWithUrl = [aDecoder decodeObjectForKey:kLAMediasMediaWithUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_mediasIdentifier forKey:kLAMediasId];
    [aCoder encodeObject:_deletedAt forKey:kLAMediasDeletedAt];
    [aCoder encodeDouble:_createdAt forKey:kLAMediasCreatedAt];
    [aCoder encodeObject:_media forKey:kLAMediasMedia];
    [aCoder encodeDouble:_happeningId forKey:kLAMediasHappeningId];
    [aCoder encodeDouble:_updatedAt forKey:kLAMediasUpdatedAt];
    [aCoder encodeObject:_type forKey:kLAMediasType];
    [aCoder encodeObject:_mimeType forKey:kLAMediasMimeType];
    [aCoder encodeObject:_mediaWithUrl forKey:kLAMediasMediaWithUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAMedias *copy = [[LAMedias alloc] init];
    
    if (copy) {

        copy.mediasIdentifier = self.mediasIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.media = [self.media copyWithZone:zone];
        copy.happeningId = self.happeningId;
        copy.updatedAt = self.updatedAt;
        copy.type = [self.type copyWithZone:zone];
        copy.mimeType = [self.mimeType copyWithZone:zone];
        copy.mediaWithUrl = [self.mediaWithUrl copyWithZone:zone];
    }
    
    return copy;
}


@end

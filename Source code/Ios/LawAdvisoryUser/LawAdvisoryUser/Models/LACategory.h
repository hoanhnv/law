//
//  LACategory.h
//
//  Created by   on 10/2/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LACategory : NSObject <NSCoding, NSCopying>

@property (nonatomic) id deletedAt;
@property (nonatomic, assign) double categoryIdentifier;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *categoryDescription;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

@interface LATheiseCategory : NSObject <NSCoding, NSCopying>

@property (nonatomic) id deletedAt;
@property (nonatomic, assign) double categoryIdentifier;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *categoryDescription;
@property (nonatomic, assign) double createdAt;

@property (nonatomic, strong) NSString *color;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *icon_with_url;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
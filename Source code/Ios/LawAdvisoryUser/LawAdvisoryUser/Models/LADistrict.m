//
//  LADistrict.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LADistrict.h"


NSString *const kLADistrictDeletedAt = @"deleted_at";
NSString *const kLADistrictId = @"id";
NSString *const kLADistrictUf = @"uf";
NSString *const kLADistrictName = @"name";
NSString *const kLADistrictUpdatedAt = @"updated_at";
NSString *const kLADistrictCreatedAt = @"created_at";


@interface LADistrict ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LADistrict

@synthesize deletedAt = _deletedAt;
@synthesize districtIdentifier = _districtIdentifier;
@synthesize uf = _uf;
@synthesize name = _name;
@synthesize updatedAt = _updatedAt;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.deletedAt = [self objectOrNilForKey:kLADistrictDeletedAt fromDictionary:dict];
            self.districtIdentifier = [[self objectOrNilForKey:kLADistrictId fromDictionary:dict] doubleValue];
            self.uf = [self objectOrNilForKey:kLADistrictUf fromDictionary:dict];
            self.name = [self objectOrNilForKey:kLADistrictName fromDictionary:dict];
            self.updatedAt = [[self objectOrNilForKey:kLADistrictUpdatedAt fromDictionary:dict] doubleValue];
            self.createdAt = [[self objectOrNilForKey:kLADistrictCreatedAt fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.deletedAt forKey:kLADistrictDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtIdentifier] forKey:kLADistrictId];
    [mutableDict setValue:self.uf forKey:kLADistrictUf];
    [mutableDict setValue:self.name forKey:kLADistrictName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLADistrictUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLADistrictCreatedAt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.deletedAt = [aDecoder decodeObjectForKey:kLADistrictDeletedAt];
    self.districtIdentifier = [aDecoder decodeDoubleForKey:kLADistrictId];
    self.uf = [aDecoder decodeObjectForKey:kLADistrictUf];
    self.name = [aDecoder decodeObjectForKey:kLADistrictName];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLADistrictUpdatedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLADistrictCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_deletedAt forKey:kLADistrictDeletedAt];
    [aCoder encodeDouble:_districtIdentifier forKey:kLADistrictId];
    [aCoder encodeObject:_uf forKey:kLADistrictUf];
    [aCoder encodeObject:_name forKey:kLADistrictName];
    [aCoder encodeDouble:_updatedAt forKey:kLADistrictUpdatedAt];
    [aCoder encodeDouble:_createdAt forKey:kLADistrictCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LADistrict *copy = [[LADistrict alloc] init];
    
    if (copy) {

        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.districtIdentifier = self.districtIdentifier;
        copy.uf = [self.uf copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.updatedAt = self.updatedAt;
        copy.createdAt = self.createdAt;
    }
    
    return copy;
}


@end

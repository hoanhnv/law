//
//  LAHappening.h
//
//  Created by   on 11/22/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LALawsuitObj.h"

@interface LAHappeningObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double thesisId;
@property (nonatomic, strong) NSArray *medias;
@property (nonatomic, strong) LALawsuitObj *thesis;
@property (nonatomic, assign) double happeningIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *otherInformation;
@property (nonatomic, assign) double userId;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSArray *witnesses;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAQuestionObj.h
//
//  Created by   on 9/21/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LAQuestionObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double deletedAt;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, strong) NSString *answer;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *question;
@property (nonatomic, assign) double createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAData.h
//
//  Created by   on 10/20/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LAPlanObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double deletedAt;
@property (nonatomic, assign) double quantityOfTheses;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double value;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double percent_by_district;
@property (nonatomic, assign) double vindiId;
@property (nonatomic, assign) BOOL discountAll;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
+ (NSDictionary*)objectToDictionary:(LAPlanObj*)planObj;

@end

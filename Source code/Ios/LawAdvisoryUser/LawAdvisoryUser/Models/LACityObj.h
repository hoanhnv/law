//
//  LACityObj.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LACityObj : NSObject
@property (nonatomic) NSString* cityName;
@property (nonatomic) NSString* sigla;
@property (nonatomic) NSString* nome;
@end

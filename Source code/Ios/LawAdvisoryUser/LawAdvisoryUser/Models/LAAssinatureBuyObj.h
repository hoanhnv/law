//
//  LAData.h
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LAPlanObj;

@interface LAAssinatureBuyObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double planId;
@property (nonatomic, assign) double valuePlanOnSigning;
@property (nonatomic, assign) double valuePerExtraDistrict;
@property (nonatomic, assign) double totalPlanOnSigning;
@property (nonatomic, assign) double lawyerId;
@property (nonatomic, assign) double totalDistrictsSigning;
@property (nonatomic, strong) NSString *paymentMethod;
@property (nonatomic, assign) double vindiId;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, strong) NSArray *districts;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSArray *thesis;
@property (nonatomic, strong) LAPlanObj *plan;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, assign) double totalThesesOnSigning;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  LAData.h
//
//  Created by   on 11/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LARefusalObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *reasonDetail;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

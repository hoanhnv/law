//
//  LASignature.m
//
//  Created by   on 11/14/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAHistorySignature.h"
#import "LAPlanObj.h"


NSString *const kLASignatureId = @"id";
NSString *const kLASignaturePlanId = @"plan_id";
NSString *const kLASignatureValuePlanOnSigning = @"value_plan_on_signing";
NSString *const kLASignatureTotalThesesOnSigning = @"total_theses_on_signing";
NSString *const kLASignatureTotalPlanOnSigning = @"total_plan_on_signing";
NSString *const kLASignatureLawyerId = @"lawyer_id";
NSString *const kLASignatureTotalDistrictsSigning = @"total_districts_signing";
NSString *const kLASignatureCreatedAt = @"created_at";
NSString *const kLASignatureVindiId = @"vindi_id";
NSString *const kLASignaturePaymentMethod = @"payment_method";
NSString *const kLASignatureDeletedAt = @"deleted_at";
NSString *const kLASignatureUpdatedAt = @"updated_at";
NSString *const kLASignaturePlan = @"plan";
NSString *const kLASignatureStatus = @"status";
NSString *const kLASignatureValuePerExtraDistrict = @"value_per_extra_district";


@interface LAHistorySignature ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAHistorySignature

@synthesize signatureIdentifier = _signatureIdentifier;
@synthesize planId = _planId;
@synthesize valuePlanOnSigning = _valuePlanOnSigning;
@synthesize totalThesesOnSigning = _totalThesesOnSigning;
@synthesize totalPlanOnSigning = _totalPlanOnSigning;
@synthesize lawyerId = _lawyerId;
@synthesize totalDistrictsSigning = _totalDistrictsSigning;
@synthesize createdAt = _createdAt;
@synthesize vindiId = _vindiId;
@synthesize paymentMethod = _paymentMethod;
@synthesize deletedAt = _deletedAt;
@synthesize updatedAt = _updatedAt;
@synthesize plan = _plan;
@synthesize status = _status;
@synthesize valuePerExtraDistrict = _valuePerExtraDistrict;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.signatureIdentifier = [[self objectOrNilForKey:kLASignatureId fromDictionary:dict] doubleValue];
        self.planId = [[self objectOrNilForKey:kLASignaturePlanId fromDictionary:dict] doubleValue];
        self.valuePlanOnSigning = [[self objectOrNilForKey:kLASignatureValuePlanOnSigning fromDictionary:dict] doubleValue];
        self.totalThesesOnSigning = [[self objectOrNilForKey:kLASignatureTotalThesesOnSigning fromDictionary:dict] doubleValue];
        self.totalPlanOnSigning = [[self objectOrNilForKey:kLASignatureTotalPlanOnSigning fromDictionary:dict] doubleValue];
        self.lawyerId = [[self objectOrNilForKey:kLASignatureLawyerId fromDictionary:dict] doubleValue];
        self.totalDistrictsSigning = [[self objectOrNilForKey:kLASignatureTotalDistrictsSigning fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLASignatureCreatedAt fromDictionary:dict] doubleValue];
        self.vindiId = [[self objectOrNilForKey:kLASignatureVindiId fromDictionary:dict] doubleValue];
        self.paymentMethod = [self objectOrNilForKey:kLASignaturePaymentMethod fromDictionary:dict];
        self.deletedAt = [self objectOrNilForKey:kLASignatureDeletedAt fromDictionary:dict];
        self.updatedAt = [[self objectOrNilForKey:kLASignatureUpdatedAt fromDictionary:dict] doubleValue];
        self.plan = [LAPlanObj modelObjectWithDictionary:[dict objectForKey:kLASignaturePlan]];
        self.status = [self objectOrNilForKey:kLASignatureStatus fromDictionary:dict];
        self.valuePerExtraDistrict = [[self objectOrNilForKey:kLASignatureValuePerExtraDistrict fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureIdentifier] forKey:kLASignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.planId] forKey:kLASignaturePlanId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePlanOnSigning] forKey:kLASignatureValuePlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalThesesOnSigning] forKey:kLASignatureTotalThesesOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalPlanOnSigning] forKey:kLASignatureTotalPlanOnSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawyerId] forKey:kLASignatureLawyerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalDistrictsSigning] forKey:kLASignatureTotalDistrictsSigning];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLASignatureCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.vindiId] forKey:kLASignatureVindiId];
    [mutableDict setValue:self.paymentMethod forKey:kLASignaturePaymentMethod];
    [mutableDict setValue:self.deletedAt forKey:kLASignatureDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLASignatureUpdatedAt];
    //   [mutableDict setValue:[self.plan dictionaryRepresentation] forKey:kLASignaturePlan];
    [mutableDict setValue:self.status forKey:kLASignatureStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valuePerExtraDistrict] forKey:kLASignatureValuePerExtraDistrict];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.signatureIdentifier = [aDecoder decodeDoubleForKey:kLASignatureId];
    self.planId = [aDecoder decodeDoubleForKey:kLASignaturePlanId];
    self.valuePlanOnSigning = [aDecoder decodeDoubleForKey:kLASignatureValuePlanOnSigning];
    self.totalThesesOnSigning = [aDecoder decodeDoubleForKey:kLASignatureTotalThesesOnSigning];
    self.totalPlanOnSigning = [aDecoder decodeDoubleForKey:kLASignatureTotalPlanOnSigning];
    self.lawyerId = [aDecoder decodeDoubleForKey:kLASignatureLawyerId];
    self.totalDistrictsSigning = [aDecoder decodeDoubleForKey:kLASignatureTotalDistrictsSigning];
    self.createdAt = [aDecoder decodeDoubleForKey:kLASignatureCreatedAt];
    self.vindiId = [aDecoder decodeDoubleForKey:kLASignatureVindiId];
    self.paymentMethod = [aDecoder decodeObjectForKey:kLASignaturePaymentMethod];
    self.deletedAt = [aDecoder decodeObjectForKey:kLASignatureDeletedAt];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLASignatureUpdatedAt];
    self.plan = [aDecoder decodeObjectForKey:kLASignaturePlan];
    self.status = [aDecoder decodeObjectForKey:kLASignatureStatus];
    self.valuePerExtraDistrict = [aDecoder decodeDoubleForKey:kLASignatureValuePerExtraDistrict];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_signatureIdentifier forKey:kLASignatureId];
    [aCoder encodeDouble:_planId forKey:kLASignaturePlanId];
    [aCoder encodeDouble:_valuePlanOnSigning forKey:kLASignatureValuePlanOnSigning];
    [aCoder encodeDouble:_totalThesesOnSigning forKey:kLASignatureTotalThesesOnSigning];
    [aCoder encodeDouble:_totalPlanOnSigning forKey:kLASignatureTotalPlanOnSigning];
    [aCoder encodeDouble:_lawyerId forKey:kLASignatureLawyerId];
    [aCoder encodeDouble:_totalDistrictsSigning forKey:kLASignatureTotalDistrictsSigning];
    [aCoder encodeDouble:_createdAt forKey:kLASignatureCreatedAt];
    [aCoder encodeDouble:_vindiId forKey:kLASignatureVindiId];
    [aCoder encodeObject:_paymentMethod forKey:kLASignaturePaymentMethod];
    [aCoder encodeObject:_deletedAt forKey:kLASignatureDeletedAt];
    [aCoder encodeDouble:_updatedAt forKey:kLASignatureUpdatedAt];
    [aCoder encodeObject:_plan forKey:kLASignaturePlan];
    [aCoder encodeObject:_status forKey:kLASignatureStatus];
    [aCoder encodeDouble:_valuePerExtraDistrict forKey:kLASignatureValuePerExtraDistrict];
}

@end

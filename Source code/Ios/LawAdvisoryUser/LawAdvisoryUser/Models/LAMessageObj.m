//
//  LAMessageObj.m
//
//  Created by   on 2/20/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "LAMessageObj.h"
#import "LAUserObj.h"


NSString *const kLAMessageObjBody = @"body";
NSString *const kLAMessageObjId = @"id";
NSString *const kLAMessageObjThreadId = @"thread_id";
NSString *const kLAMessageObjUserId = @"user_id";
NSString *const kLAMessageObjUpdatedAt = @"updated_at";
NSString *const kLAMessageObjCreatedAt = @"created_at";
NSString *const kLAMessageObjUser = @"user";


@interface LAMessageObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAMessageObj

@synthesize body = _body;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize threadId = _threadId;
@synthesize userId = _userId;
@synthesize updatedAt = _updatedAt;
@synthesize createdAt = _createdAt;
@synthesize user = _user;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.body = [self objectOrNilForKey:kLAMessageObjBody fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kLAMessageObjId fromDictionary:dict] doubleValue];
            self.threadId = [[self objectOrNilForKey:kLAMessageObjThreadId fromDictionary:dict] doubleValue];
            self.userId = [[self objectOrNilForKey:kLAMessageObjUserId fromDictionary:dict] doubleValue];
            self.updatedAt = [[self objectOrNilForKey:kLAMessageObjUpdatedAt fromDictionary:dict] doubleValue];
            self.createdAt = [[self objectOrNilForKey:kLAMessageObjCreatedAt fromDictionary:dict] doubleValue];
            self.user = [LAUserObj modelObjectWithDictionary:[dict objectForKey:kLAMessageObjUser]];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.body forKey:kLAMessageObjBody];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kLAMessageObjId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.threadId] forKey:kLAMessageObjThreadId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLAMessageObjUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAMessageObjUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAMessageObjCreatedAt];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kLAMessageObjUser];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.body = [aDecoder decodeObjectForKey:kLAMessageObjBody];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kLAMessageObjId];
    self.threadId = [aDecoder decodeDoubleForKey:kLAMessageObjThreadId];
    self.userId = [aDecoder decodeDoubleForKey:kLAMessageObjUserId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAMessageObjUpdatedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAMessageObjCreatedAt];
    self.user = [aDecoder decodeObjectForKey:kLAMessageObjUser];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_body forKey:kLAMessageObjBody];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kLAMessageObjId];
    [aCoder encodeDouble:_threadId forKey:kLAMessageObjThreadId];
    [aCoder encodeDouble:_userId forKey:kLAMessageObjUserId];
    [aCoder encodeDouble:_updatedAt forKey:kLAMessageObjUpdatedAt];
    [aCoder encodeDouble:_createdAt forKey:kLAMessageObjCreatedAt];
    [aCoder encodeObject:_user forKey:kLAMessageObjUser];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAMessageObj *copy = [[LAMessageObj alloc] init];
    
    if (copy) {

        copy.body = [self.body copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.threadId = self.threadId;
        copy.userId = self.userId;
        copy.updatedAt = self.updatedAt;
        copy.createdAt = self.createdAt;
        copy.user = [self.user copyWithZone:zone];
    }
    
    return copy;
}


@end

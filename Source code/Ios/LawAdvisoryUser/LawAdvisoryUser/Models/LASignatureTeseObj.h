//
//  LAData.h
//
//  Created by   on 11/25/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LALawsuitObj;

@interface LASignatureTeseObj : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) LALawsuitObj *lawsuitThesis;
@property (nonatomic, assign) double thesisId;
@property (nonatomic, assign) double initialFees;
@property (nonatomic, assign) double dataIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, assign) double signatureId;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, assign) double percentageInSuccess;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

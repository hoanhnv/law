//
//  LAPlan.h
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LAPlan : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double planIdentifier;
@property (nonatomic, assign) id deletedAt;
@property (nonatomic, assign) double vindiId;
@property (nonatomic, assign) double quantityOfTheses;
@property (nonatomic, assign) double createdAt;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, assign) double percentByDistrict;
@property (nonatomic, assign) double updatedAt;
@property (nonatomic, strong) NSString *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

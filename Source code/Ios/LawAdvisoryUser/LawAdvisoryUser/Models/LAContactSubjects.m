//
//  LAContactSubject.m
//
//  Created by   on 9/23/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAContactSubjects.h"
#import "LAContactSubjectObj.h"


NSString *const kLAContactSubjectSuccess = @"success";
NSString *const kLAContactSubjectData = @"data";
NSString *const kLAContactSubjectMessage = @"message";


@interface LAContactSubjects ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAContactSubjects

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAContactSubjectSuccess fromDictionary:dict] boolValue];
        NSObject *receivedLAContactSubjectObjObj = [dict objectForKey:kLAContactSubjectData];
        NSMutableArray *parsedLAContactSubjectObjObj = [NSMutableArray array];
        if ([receivedLAContactSubjectObjObj isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLAContactSubjectObjObj) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLAContactSubjectObjObj addObject:[LAContactSubjectObj modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLAContactSubjectObjObj isKindOfClass:[NSDictionary class]]) {
            [parsedLAContactSubjectObjObj addObject:[LAContactSubjectObj modelObjectWithDictionary:(NSDictionary *)receivedLAContactSubjectObjObj]];
        }
        
        self.data = [NSArray arrayWithArray:parsedLAContactSubjectObjObj];
        self.message = [self objectOrNilForKey:kLAContactSubjectMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.success] forKey:kLAContactSubjectSuccess];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kLAContactSubjectData];
    [mutableDict setValue:self.message forKey:kLAContactSubjectMessage];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAContactSubjectSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAContactSubjectData];
    self.message = [aDecoder decodeObjectForKey:kLAContactSubjectMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAContactSubjectSuccess];
    [aCoder encodeObject:_data forKey:kLAContactSubjectData];
    [aCoder encodeObject:_message forKey:kLAContactSubjectMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAContactSubjects *copy = [[LAContactSubjects alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LAData.m
//
//  Created by   on 11/14/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAHistoryDataObj.h"
#import "LAAssinatureObj.h"
#import "LADistricts.h"


NSString *const kLADataHistoryId = @"id";
NSString *const kLADataHistoryEndAt = @"end_at";
NSString *const kLADataHistorySignatureId = @"signature_id";
NSString *const kLADataHistoryCreatedAt = @"created_at";
NSString *const kLADataHistoryVindiId = @"vindi_id";
NSString *const kLADataHistoryUrl = @"url";
NSString *const kLADataHistoryDeletedAt = @"deleted_at";
NSString *const kLADataHistoryStartAt = @"start_at";
NSString *const kLADataHistorySignature = @"signature";
NSString *const kLADataHistoryDistricts = @"districts";
NSString *const kLADataHistoryUpdatedAt = @"updated_at";
NSString *const kLADataHistoryStatus = @"status";


@interface LAHistoryDataObj ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAHistoryDataObj

@synthesize dataIdentifier = _dataIdentifier;
@synthesize endAt = _endAt;
@synthesize signatureId = _signatureId;
@synthesize createdAt = _createdAt;
@synthesize vindiId = _vindiId;
@synthesize url = _url;
@synthesize deletedAt = _deletedAt;
@synthesize startAt = _startAt;
@synthesize signature = _signature;
@synthesize districts = _districts;
@synthesize updatedAt = _updatedAt;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.dataIdentifier = [[self objectOrNilForKey:kLADataHistoryId fromDictionary:dict] doubleValue];
        self.endAt = [[self objectOrNilForKey:kLADataHistoryEndAt fromDictionary:dict] doubleValue];
        self.signatureId = [[self objectOrNilForKey:kLADataHistorySignatureId fromDictionary:dict] doubleValue];
        self.createdAt = [[self objectOrNilForKey:kLADataHistoryCreatedAt fromDictionary:dict] doubleValue];
        self.vindiId = [[self objectOrNilForKey:kLADataHistoryVindiId fromDictionary:dict] doubleValue];
        self.url = [self objectOrNilForKey:kLADataHistoryUrl fromDictionary:dict];
        self.deletedAt = [self objectOrNilForKey:kLADataHistoryDeletedAt fromDictionary:dict];
        self.startAt = [[self objectOrNilForKey:kLADataHistoryStartAt fromDictionary:dict] doubleValue];
        self.signature = [LAAssinatureObj modelObjectWithDictionary:[dict objectForKey:kLADataHistorySignature]];
        NSObject *receivedLADistricts = [dict objectForKey:kLADataHistoryDistricts];
        NSMutableArray *parsedLADistricts = [NSMutableArray array];
        if ([receivedLADistricts isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLADistricts) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedLADistricts addObject:[LADistricts modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedLADistricts isKindOfClass:[NSDictionary class]]) {
            [parsedLADistricts addObject:[LADistricts modelObjectWithDictionary:(NSDictionary *)receivedLADistricts]];
        }
        
        self.districts = [NSArray arrayWithArray:parsedLADistricts];
        self.updatedAt = [[self objectOrNilForKey:kLADataHistoryUpdatedAt fromDictionary:dict] doubleValue];
        self.status = [self objectOrNilForKey:kLADataHistoryStatus fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.dataIdentifier] forKey:kLADataHistoryId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.endAt] forKey:kLADataHistoryEndAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureId] forKey:kLADataHistorySignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLADataHistoryCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.vindiId] forKey:kLADataHistoryVindiId];
    [mutableDict setValue:self.url forKey:kLADataHistoryUrl];
    [mutableDict setValue:self.deletedAt forKey:kLADataHistoryDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.startAt] forKey:kLADataHistoryStartAt];
    [mutableDict setValue:[self.signature dictionaryRepresentation] forKey:kLADataHistorySignature];
    NSMutableArray *tempArrayForDistricts = [NSMutableArray array];
    for (NSObject *subArrayObject in self.districts) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDistricts addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDistricts addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDistricts] forKey:kLADataHistoryDistricts];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLADataHistoryUpdatedAt];
    [mutableDict setValue:self.status forKey:kLADataHistoryStatus];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.dataIdentifier = [aDecoder decodeDoubleForKey:kLADataHistoryId];
    self.endAt = [aDecoder decodeDoubleForKey:kLADataHistoryEndAt];
    self.signatureId = [aDecoder decodeDoubleForKey:kLADataHistorySignatureId];
    self.createdAt = [aDecoder decodeDoubleForKey:kLADataHistoryCreatedAt];
    self.vindiId = [aDecoder decodeDoubleForKey:kLADataHistoryVindiId];
    self.url = [aDecoder decodeObjectForKey:kLADataHistoryUrl];
    self.deletedAt = [aDecoder decodeObjectForKey:kLADataHistoryDeletedAt];
    self.startAt = [aDecoder decodeDoubleForKey:kLADataHistoryStartAt];
    self.signature = [aDecoder decodeObjectForKey:kLADataHistorySignature];
    self.districts = [aDecoder decodeObjectForKey:kLADataHistoryDistricts];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLADataHistoryUpdatedAt];
    self.status = [aDecoder decodeObjectForKey:kLADataHistoryStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_dataIdentifier forKey:kLADataHistoryId];
    [aCoder encodeDouble:_endAt forKey:kLADataHistoryEndAt];
    [aCoder encodeDouble:_signatureId forKey:kLADataHistorySignatureId];
    [aCoder encodeDouble:_createdAt forKey:kLADataHistoryCreatedAt];
    [aCoder encodeDouble:_vindiId forKey:kLADataHistoryVindiId];
    [aCoder encodeObject:_url forKey:kLADataHistoryUrl];
    [aCoder encodeObject:_deletedAt forKey:kLADataHistoryDeletedAt];
    [aCoder encodeDouble:_startAt forKey:kLADataHistoryStartAt];
    [aCoder encodeObject:_signature forKey:kLADataHistorySignature];
    [aCoder encodeObject:_districts forKey:kLADataHistoryDistricts];
    [aCoder encodeDouble:_updatedAt forKey:kLADataHistoryUpdatedAt];
    [aCoder encodeObject:_status forKey:kLADataHistoryStatus];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAHistoryDataObj *copy = [[LAHistoryDataObj alloc] init];
    
    if (copy) {
        
        copy.dataIdentifier = self.dataIdentifier;
        copy.endAt = self.endAt;
        copy.signatureId = self.signatureId;
        copy.createdAt = self.createdAt;
        copy.vindiId = self.vindiId;
        copy.url = [self.url copyWithZone:zone];
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.startAt = self.startAt;
        //        copy.signature = [self.signature copyWithZone:zone];
        //        copy.districts = [self.districts copyWithZone:zone];
        //        copy.updatedAt = self.updatedAt;
        //        copy.status = [self.status copyWithZone:zone];
    }
    
    return copy;
}


@end

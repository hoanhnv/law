//
//  LALawsuitThesis.m
//
//  Created by   on 11/25/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LALawsuitThesis.h"


NSString *const kLALawsuitThesisDocumentation = @"documentation";
NSString *const kLALawsuitThesisId = @"id";
NSString *const kLALawsuitThesisVersion = @"version";
NSString *const kLALawsuitThesisYourRights = @"your_rights";
NSString *const kLALawsuitThesisCreatedAt = @"created_at";
NSString *const kLALawsuitThesisCategoryId = @"category_id";
NSString *const kLALawsuitThesisLegalName = @"legal_name";
NSString *const kLALawsuitThesisIdentification = @"identification";
NSString *const kLALawsuitThesisValueContraction = @"value_contraction";
NSString *const kLALawsuitThesisHowToAct = @"how_to_act";
NSString *const kLALawsuitThesisDeletedAt = @"deleted_at";
NSString *const kLALawsuitThesisSituationId = @"situation_id";
NSString *const kLALawsuitThesisValueSignature = @"value_signature";
NSString *const kLALawsuitThesisUpdatedAt = @"updated_at";
NSString *const kLALawsuitThesisName = @"name";
NSString *const kLALawsuitThesisStatus = @"status";


@interface LALawsuitThesis ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LALawsuitThesis

@synthesize documentation = _documentation;
@synthesize lawsuitThesisIdentifier = _lawsuitThesisIdentifier;
@synthesize version = _version;
@synthesize yourRights = _yourRights;
@synthesize createdAt = _createdAt;
@synthesize categoryId = _categoryId;
@synthesize legalName = _legalName;
@synthesize identification = _identification;
@synthesize valueContraction = _valueContraction;
@synthesize howToAct = _howToAct;
@synthesize deletedAt = _deletedAt;
@synthesize situationId = _situationId;
@synthesize valueSignature = _valueSignature;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.documentation = [self objectOrNilForKey:kLALawsuitThesisDocumentation fromDictionary:dict];
            self.lawsuitThesisIdentifier = [[self objectOrNilForKey:kLALawsuitThesisId fromDictionary:dict] doubleValue];
            self.version = [[self objectOrNilForKey:kLALawsuitThesisVersion fromDictionary:dict] doubleValue];
            self.yourRights = [self objectOrNilForKey:kLALawsuitThesisYourRights fromDictionary:dict];
            self.createdAt = [[self objectOrNilForKey:kLALawsuitThesisCreatedAt fromDictionary:dict] doubleValue];
            self.categoryId = [[self objectOrNilForKey:kLALawsuitThesisCategoryId fromDictionary:dict] doubleValue];
            self.legalName = [self objectOrNilForKey:kLALawsuitThesisLegalName fromDictionary:dict];
            self.identification = [self objectOrNilForKey:kLALawsuitThesisIdentification fromDictionary:dict];
            self.valueContraction = [[self objectOrNilForKey:kLALawsuitThesisValueContraction fromDictionary:dict] doubleValue];
            self.howToAct = [self objectOrNilForKey:kLALawsuitThesisHowToAct fromDictionary:dict];
            self.deletedAt = [self objectOrNilForKey:kLALawsuitThesisDeletedAt fromDictionary:dict];
            self.situationId = [[self objectOrNilForKey:kLALawsuitThesisSituationId fromDictionary:dict] doubleValue];
            self.valueSignature = [[self objectOrNilForKey:kLALawsuitThesisValueSignature fromDictionary:dict] doubleValue];
            self.updatedAt = [[self objectOrNilForKey:kLALawsuitThesisUpdatedAt fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kLALawsuitThesisName fromDictionary:dict];
            self.status = [self objectOrNilForKey:kLALawsuitThesisStatus fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.documentation forKey:kLALawsuitThesisDocumentation];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lawsuitThesisIdentifier] forKey:kLALawsuitThesisId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.version] forKey:kLALawsuitThesisVersion];
    [mutableDict setValue:self.yourRights forKey:kLALawsuitThesisYourRights];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLALawsuitThesisCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryId] forKey:kLALawsuitThesisCategoryId];
    [mutableDict setValue:self.legalName forKey:kLALawsuitThesisLegalName];
    [mutableDict setValue:self.identification forKey:kLALawsuitThesisIdentification];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueContraction] forKey:kLALawsuitThesisValueContraction];
    [mutableDict setValue:self.howToAct forKey:kLALawsuitThesisHowToAct];
    [mutableDict setValue:self.deletedAt forKey:kLALawsuitThesisDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.situationId] forKey:kLALawsuitThesisSituationId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.valueSignature] forKey:kLALawsuitThesisValueSignature];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLALawsuitThesisUpdatedAt];
    [mutableDict setValue:self.name forKey:kLALawsuitThesisName];
    [mutableDict setValue:self.status forKey:kLALawsuitThesisStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.documentation = [aDecoder decodeObjectForKey:kLALawsuitThesisDocumentation];
    self.lawsuitThesisIdentifier = [aDecoder decodeDoubleForKey:kLALawsuitThesisId];
    self.version = [aDecoder decodeDoubleForKey:kLALawsuitThesisVersion];
    self.yourRights = [aDecoder decodeObjectForKey:kLALawsuitThesisYourRights];
    self.createdAt = [aDecoder decodeDoubleForKey:kLALawsuitThesisCreatedAt];
    self.categoryId = [aDecoder decodeDoubleForKey:kLALawsuitThesisCategoryId];
    self.legalName = [aDecoder decodeObjectForKey:kLALawsuitThesisLegalName];
    self.identification = [aDecoder decodeObjectForKey:kLALawsuitThesisIdentification];
    self.valueContraction = [aDecoder decodeDoubleForKey:kLALawsuitThesisValueContraction];
    self.howToAct = [aDecoder decodeObjectForKey:kLALawsuitThesisHowToAct];
    self.deletedAt = [aDecoder decodeObjectForKey:kLALawsuitThesisDeletedAt];
    self.situationId = [aDecoder decodeDoubleForKey:kLALawsuitThesisSituationId];
    self.valueSignature = [aDecoder decodeDoubleForKey:kLALawsuitThesisValueSignature];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLALawsuitThesisUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLALawsuitThesisName];
    self.status = [aDecoder decodeObjectForKey:kLALawsuitThesisStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_documentation forKey:kLALawsuitThesisDocumentation];
    [aCoder encodeDouble:_lawsuitThesisIdentifier forKey:kLALawsuitThesisId];
    [aCoder encodeDouble:_version forKey:kLALawsuitThesisVersion];
    [aCoder encodeObject:_yourRights forKey:kLALawsuitThesisYourRights];
    [aCoder encodeDouble:_createdAt forKey:kLALawsuitThesisCreatedAt];
    [aCoder encodeDouble:_categoryId forKey:kLALawsuitThesisCategoryId];
    [aCoder encodeObject:_legalName forKey:kLALawsuitThesisLegalName];
    [aCoder encodeObject:_identification forKey:kLALawsuitThesisIdentification];
    [aCoder encodeDouble:_valueContraction forKey:kLALawsuitThesisValueContraction];
    [aCoder encodeObject:_howToAct forKey:kLALawsuitThesisHowToAct];
    [aCoder encodeObject:_deletedAt forKey:kLALawsuitThesisDeletedAt];
    [aCoder encodeDouble:_situationId forKey:kLALawsuitThesisSituationId];
    [aCoder encodeDouble:_valueSignature forKey:kLALawsuitThesisValueSignature];
    [aCoder encodeDouble:_updatedAt forKey:kLALawsuitThesisUpdatedAt];
    [aCoder encodeObject:_name forKey:kLALawsuitThesisName];
    [aCoder encodeObject:_status forKey:kLALawsuitThesisStatus];
}

- (id)copyWithZone:(NSZone *)zone
{
    LALawsuitThesis *copy = [[LALawsuitThesis alloc] init];
    
    if (copy) {

        copy.documentation = [self.documentation copyWithZone:zone];
        copy.lawsuitThesisIdentifier = self.lawsuitThesisIdentifier;
        copy.version = self.version;
        copy.yourRights = [self.yourRights copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.categoryId = self.categoryId;
        copy.legalName = [self.legalName copyWithZone:zone];
        copy.identification = [self.identification copyWithZone:zone];
        copy.valueContraction = self.valueContraction;
        copy.howToAct = [self.howToAct copyWithZone:zone];
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.situationId = self.situationId;
        copy.valueSignature = self.valueSignature;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LACategoryObj.h
//  LawAdvisoryUser
//
//  Created by MAC on 9/13/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDictionary+Law.h"
@interface LACategoryObj : NSObject
@property (nonatomic) NSString* catName;
@property (nonatomic) NSInteger catID;
@property (nonatomic) NSString* catDescription;
@property (nonatomic) double catCreatedAt;
@property (nonatomic) double catUpdateAt;
@property (nonatomic) double catDeleteAt;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
@end

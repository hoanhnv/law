//
//  LAPlan.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAPlan.h"


NSString *const kLAPlanId = @"id";
NSString *const kLAPlanDeletedAt = @"deleted_at";
NSString *const kLAPlanVindiId = @"vindi_id";
NSString *const kLAPlanQuantityOfTheses = @"quantity_of_theses";
NSString *const kLAPlanCreatedAt = @"created_at";
NSString *const kLAPlanValue = @"value";
NSString *const kLAPlanPercentByDistrict = @"percent_by_district";
NSString *const kLAPlanUpdatedAt = @"updated_at";
NSString *const kLAPlanName = @"name";


@interface LAPlan ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAPlan

@synthesize planIdentifier = _planIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize vindiId = _vindiId;
@synthesize quantityOfTheses = _quantityOfTheses;
@synthesize createdAt = _createdAt;
@synthesize value = _value;
@synthesize percentByDistrict = _percentByDistrict;
@synthesize updatedAt = _updatedAt;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.planIdentifier = [[self objectOrNilForKey:kLAPlanId fromDictionary:dict] doubleValue];
            self.deletedAt = [self objectOrNilForKey:kLAPlanDeletedAt fromDictionary:dict];
            self.vindiId = [[self objectOrNilForKey:kLAPlanVindiId fromDictionary:dict] doubleValue];
            self.quantityOfTheses = [[self objectOrNilForKey:kLAPlanQuantityOfTheses fromDictionary:dict] doubleValue];
            self.createdAt = [[self objectOrNilForKey:kLAPlanCreatedAt fromDictionary:dict] doubleValue];
            self.value = [self objectOrNilForKey:kLAPlanValue fromDictionary:dict];
            self.percentByDistrict = [[self objectOrNilForKey:kLAPlanPercentByDistrict fromDictionary:dict] doubleValue];
            self.updatedAt = [[self objectOrNilForKey:kLAPlanUpdatedAt fromDictionary:dict] doubleValue];
            self.name = [self objectOrNilForKey:kLAPlanName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.planIdentifier] forKey:kLAPlanId];
    [mutableDict setValue:self.deletedAt forKey:kLAPlanDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.vindiId] forKey:kLAPlanVindiId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.quantityOfTheses] forKey:kLAPlanQuantityOfTheses];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLAPlanCreatedAt];
    [mutableDict setValue:self.value forKey:kLAPlanValue];
    [mutableDict setValue:[NSNumber numberWithDouble:self.percentByDistrict] forKey:kLAPlanPercentByDistrict];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLAPlanUpdatedAt];
    [mutableDict setValue:self.name forKey:kLAPlanName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.planIdentifier = [aDecoder decodeDoubleForKey:kLAPlanId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLAPlanDeletedAt];
    self.vindiId = [aDecoder decodeDoubleForKey:kLAPlanVindiId];
    self.quantityOfTheses = [aDecoder decodeDoubleForKey:kLAPlanQuantityOfTheses];
    self.createdAt = [aDecoder decodeDoubleForKey:kLAPlanCreatedAt];
    self.value = [aDecoder decodeObjectForKey:kLAPlanValue];
    self.percentByDistrict = [aDecoder decodeDoubleForKey:kLAPlanPercentByDistrict];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLAPlanUpdatedAt];
    self.name = [aDecoder decodeObjectForKey:kLAPlanName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_planIdentifier forKey:kLAPlanId];
    [aCoder encodeObject:_deletedAt forKey:kLAPlanDeletedAt];
    [aCoder encodeDouble:_vindiId forKey:kLAPlanVindiId];
    [aCoder encodeDouble:_quantityOfTheses forKey:kLAPlanQuantityOfTheses];
    [aCoder encodeDouble:_createdAt forKey:kLAPlanCreatedAt];
    [aCoder encodeObject:_value forKey:kLAPlanValue];
    [aCoder encodeDouble:_percentByDistrict forKey:kLAPlanPercentByDistrict];
    [aCoder encodeDouble:_updatedAt forKey:kLAPlanUpdatedAt];
    [aCoder encodeObject:_name forKey:kLAPlanName];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAPlan *copy = [[LAPlan alloc] init];
    
    if (copy) {

        copy.planIdentifier = self.planIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.vindiId = self.vindiId;
        copy.quantityOfTheses = self.quantityOfTheses;
        copy.createdAt = self.createdAt;
        copy.value = [self.value copyWithZone:zone];
        copy.percentByDistrict = self.percentByDistrict;
        copy.updatedAt = self.updatedAt;
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end

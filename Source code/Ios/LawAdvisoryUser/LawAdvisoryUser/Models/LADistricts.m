//
//  LADistricts.m
//
//  Created by   on 10/24/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LADistricts.h"
#import "LASignatureDistrict.h"


NSString *const kLADistrictsDistrict = @"district";
NSString *const kLADistrictsId = @"id";
NSString *const kLADistrictsDeletedAt = @"deleted_at";
NSString *const kLADistrictsCreatedAt = @"created_at";
NSString *const kLADistrictsSignatureId = @"signature_id";
NSString *const kLADistrictsDistrictId = @"district_id";
NSString *const kLADistrictsUpdatedAt = @"updated_at";


@interface LADistricts ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LADistricts

@synthesize district = _district;
@synthesize districtsIdentifier = _districtsIdentifier;
@synthesize deletedAt = _deletedAt;
@synthesize createdAt = _createdAt;
@synthesize signatureId = _signatureId;
@synthesize districtId = _districtId;
@synthesize updatedAt = _updatedAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.district = [LASignatureDistrict modelObjectWithDictionary:[dict objectForKey:kLADistrictsDistrict]];
        self.districtsIdentifier = [[self objectOrNilForKey:kLADistrictsId fromDictionary:dict] doubleValue];
        self.deletedAt = [self objectOrNilForKey:kLADistrictsDeletedAt fromDictionary:dict];
        self.createdAt = [[self objectOrNilForKey:kLADistrictsCreatedAt fromDictionary:dict] doubleValue];
        self.signatureId = [[self objectOrNilForKey:kLADistrictsSignatureId fromDictionary:dict] doubleValue];
        self.districtId = [[self objectOrNilForKey:kLADistrictsDistrictId fromDictionary:dict] doubleValue];
        self.updatedAt = [[self objectOrNilForKey:kLADistrictsUpdatedAt fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.district dictionaryRepresentation] forKey:kLADistrictsDistrict];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtsIdentifier] forKey:kLADistrictsId];
    [mutableDict setValue:self.deletedAt forKey:kLADistrictsDeletedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdAt] forKey:kLADistrictsCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.signatureId] forKey:kLADistrictsSignatureId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.districtId] forKey:kLADistrictsDistrictId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.updatedAt] forKey:kLADistrictsUpdatedAt];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.district = [aDecoder decodeObjectForKey:kLADistrictsDistrict];
    self.districtsIdentifier = [aDecoder decodeDoubleForKey:kLADistrictsId];
    self.deletedAt = [aDecoder decodeObjectForKey:kLADistrictsDeletedAt];
    self.createdAt = [aDecoder decodeDoubleForKey:kLADistrictsCreatedAt];
    self.signatureId = [aDecoder decodeDoubleForKey:kLADistrictsSignatureId];
    self.districtId = [aDecoder decodeDoubleForKey:kLADistrictsDistrictId];
    self.updatedAt = [aDecoder decodeDoubleForKey:kLADistrictsUpdatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_district forKey:kLADistrictsDistrict];
    [aCoder encodeDouble:_districtsIdentifier forKey:kLADistrictsId];
    [aCoder encodeObject:_deletedAt forKey:kLADistrictsDeletedAt];
    [aCoder encodeDouble:_createdAt forKey:kLADistrictsCreatedAt];
    [aCoder encodeDouble:_signatureId forKey:kLADistrictsSignatureId];
    [aCoder encodeDouble:_districtId forKey:kLADistrictsDistrictId];
    [aCoder encodeDouble:_updatedAt forKey:kLADistrictsUpdatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    LADistricts *copy = [[LADistricts alloc] init];
    
    if (copy) {
        
        copy.district = [self.district copyWithZone:zone];
        copy.districtsIdentifier = self.districtsIdentifier;
        copy.deletedAt = [self.deletedAt copyWithZone:zone];
        copy.createdAt = self.createdAt;
        copy.signatureId = self.signatureId;
        copy.districtId = self.districtId;
        copy.updatedAt = self.updatedAt;
    }
    
    return copy;
}


@end

//
//  LAAssinatureInfo.m
//
//  Created by   on 11/29/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LAAssinatureInfo.h"
#import "LAAssinatureBuyObj.h"


NSString *const kLAAssinatureInfoSuccess = @"success";
NSString *const kLAAssinatureInfoData = @"data";
NSString *const kLAAssinatureInfoMessage = @"message";


@interface LAAssinatureInfo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LAAssinatureInfo

@synthesize success = _success;
@synthesize data = _data;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.success = [[self objectOrNilForKey:kLAAssinatureInfoSuccess fromDictionary:dict] boolValue];
        self.data = [LAAssinatureBuyObj modelObjectWithDictionary:[dict objectForKey:kLAAssinatureInfoData]];
        self.message = [self objectOrNilForKey:kLAAssinatureInfoMessage fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.success = [aDecoder decodeBoolForKey:kLAAssinatureInfoSuccess];
    self.data = [aDecoder decodeObjectForKey:kLAAssinatureInfoData];
    self.message = [aDecoder decodeObjectForKey:kLAAssinatureInfoMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeBool:_success forKey:kLAAssinatureInfoSuccess];
    [aCoder encodeObject:_data forKey:kLAAssinatureInfoData];
    [aCoder encodeObject:_message forKey:kLAAssinatureInfoMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    LAAssinatureInfo *copy = [[LAAssinatureInfo alloc] init];
    
    if (copy) {
        
        copy.success = self.success;
        copy.data = [self.data copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  LAUtilitys.m
//  LawAdvisoryUser
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "LAUtilitys.h"
#import "UIColor+Law.h"

@implementation LAUtilitys
#pragma mark BUTTON UTILITYS
+ (void)setButtonTextUnderLine:(UIButton*)sender
{
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:sender.titleLabel.text];
    // making text property to underline text-
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    // using text on button
    [sender setAttributedTitle: titleString forState:UIControlStateNormal];
}
#pragma mark STRING UTILITYS
+ (BOOL)isEmptyOrNull:(NSString*) cached
{
    if ([cached class] == [NSNull class] || cached == Nil || [cached isKindOfClass:[NSNull class]])
    {
        return YES;
    }
    NSString* trim = [cached stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([trim length] == 0) {
        return YES;
    }
    return NO;
}
+ (NSString*)getDefaultString:(NSString*)sourceString
{
    if ([LAUtilitys isEmptyOrNull:sourceString] || [LAUtilitys NullOrEmpty:sourceString])
    {
        return @"";
    }
    return sourceString;
}
+ (BOOL)validateEmailWithString:(NSString*)checkString {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)NullOrEmpty:(NSString *)strInput{
    BOOL bResult = NO;
    if (strInput == nil || strInput == [NSNull class]
        || [strInput isEqualToString:@""]
        || [strInput isEqualToString:@"(null)"]) {
        
        bResult = YES;
    }
    return bResult;
}
#pragma mark UITEXTFIELD UTILITYS
//Create image right mode for textfield
+ (void)setRightImageWhenEditing:(NSString*)imgName forTextField:(UITextField*)textField
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(textField.frame)) - 15, 13, 13)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    imgView.image = [UIImage imageNamed:imgName];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame))];
    [paddingView addSubview:imgView];
    [textField setRightViewMode:UITextFieldViewModeUnlessEditing];
    [textField setRightView:paddingView];
    CGPoint centerIMG = imgView.center;
    centerIMG.y = paddingView.center.y;
    [imgView setCenter:centerIMG];
}
+ (void)setRightImage:(NSString*)imgName forTextField:(UITextField*)textField
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(textField.frame)) - 15, 13, 13)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    imgView.image = [UIImage imageNamed:imgName];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame))];
    [paddingView addSubview:imgView];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    [textField setRightView:paddingView];
    CGPoint centerIMG = imgView.center;
    centerIMG.y = paddingView.center.y;
    [imgView setCenter:centerIMG];
}
+ (void) setPaddingForTextField:(UITextField *) textField andPaddingLeftMargin:(CGFloat)left {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, left, CGRectGetHeight(textField.bounds))];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
#pragma mark INTRO UTILITYS
+ (void)setIntroIsPresented
{
    NSUserDefaults* defaultUser = [NSUserDefaults standardUserDefaults];
    [defaultUser setObject:[NSNumber numberWithBool:YES] forKey:@"introShow"];
    [defaultUser synchronize];
}
+ (BOOL)checkIntroIsPresent
{
    BOOL isCheck = NO;
    NSUserDefaults* defaultUser = [NSUserDefaults standardUserDefaults];
    NSNumber* nNumber = [defaultUser objectForKey:@"introShow"];
    if (nNumber && [nNumber boolValue])
    {
        isCheck = YES;
    }
    return isCheck;
}
#pragma mark DATE UTILITY
+ (NSDate*)getNSDateFromDateString:(NSString*)dateTxt withFormat:(NSString*)strFormat
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:-3600*3];
    [dateFormat setDateFormat:strFormat];
    NSDate *date = [dateFormat dateFromString:dateTxt];
    return date;
}
+ (long long)getLongDateFromDateString:(NSString*)dateTxt withFormat:(NSString*)strFormat
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:-3600*3];
    [dateFormat setDateFormat:strFormat];
    NSDate *date = [dateFormat dateFromString:dateTxt];
    return (long long)date.timeIntervalSince1970;
}

+ (NSString*)getDateStringFromTimeInterVal:(double)time withFormat:(NSString*)strFormat
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    dateformate.timeZone =  [NSTimeZone timeZoneForSecondsFromGMT:-3600*3];
    [dateformate setDateFormat:strFormat]; // Date formater
    NSString *dateTXT = [dateformate stringFromDate:date];
    return dateTXT;
}

+(UIColor *)jusTapColor{
    return [UIColor colorWithHexString:MAIN_COLOR];
}
+ (ACCOUNT_STATUS)getAccountStatusFromString:(NSString*)strStatus
{
    ACCOUNT_STATUS status = UNKNOWN_ACCOUNT;
    if ([[strStatus uppercaseString] isEqualToString:[@"Reprovado" uppercaseString]])
    {
        return IN_ACTIVE_ACCOUNT;
    }
    else if ([[strStatus uppercaseString] isEqualToString:[@"Aguardando aprovação" uppercaseString]])
    {
        return WAITING_APPROVAL_ACCOUNT;
    }
    else if ([[strStatus uppercaseString] isEqualToString:[@"Aprovado" uppercaseString]])
    {
        return ACTIVE_ACCOUNT;
    }
    return status;
}
+ (BOOL)isValidCellPhone:(NSString*)stringMobile
{
    if ([LAUtilitys isEmptyOrNull:stringMobile])
    {
        return NO;
    }
    BOOL check = NO;
    @try {
        NSString *pattern1 = @"[\\(]?[0-9]{2}[\\)]?[0-9]{5}[\\-]?[0-9]{4}";
        NSString *pattern2 = @"[\\(]?[0-9]{2}[\\)]?[0-9]{5}[\\-]?[0-9]{3}";
        NSPredicate *myTest1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern1];
        NSPredicate *myTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern2];
        check = [myTest1 evaluateWithObject: stringMobile] || [myTest2 evaluateWithObject:stringMobile];
    } @catch (NSException *exception) {
        check = NO;
    }
    return check;
}
+ (BOOL)isValidPhone:(NSString*)stringMobile
{
    if ([LAUtilitys isEmptyOrNull:stringMobile])
    {
        return NO;
    }
    BOOL check = NO;
    @try {
        NSString *pattern1 = @"[\\(]?[0-9]{2}[\\)]?[0-9]{4}[\\-]?[0-9]{4}";
        NSPredicate *myTest1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern1];
        //        NSString *pattern2 = @"[\\(]?[0-9]{2}[\\)]?[0-9]{4}[\\-]?[0-9]{3}";
        //        NSPredicate *myTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern2];
        
        check = [myTest1 evaluateWithObject: stringMobile] ;
    } @catch (NSException *exception) {
        check = NO;
    }
    return check;
}
+ (BOOL)isValidCPF:(NSString*)CPF
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^\\d]" options:NSRegularExpressionCaseInsensitive error:&error];
    CPF = [regex stringByReplacingMatchesInString:CPF options:0 range:NSMakeRange(0, [CPF length]) withTemplate:@""];
    if([CPF isEqualToString:@""])
        return false;
    // Elimina CPFs invalidos conhecidos
    if (CPF.length != 11 ||
        [CPF isEqualToString:@"00000000000"] ||
        [CPF isEqualToString:@"11111111111"] ||
        [CPF isEqualToString:@"22222222222"] ||
        [CPF isEqualToString:@"33333333333"] ||
        [CPF isEqualToString:@"44444444444"] ||
        [CPF isEqualToString:@"55555555555"] ||
        [CPF isEqualToString:@"66666666666"] ||
        [CPF isEqualToString:@"77777777777"] ||
        [CPF isEqualToString:@"88888888888"] ||
        [CPF isEqualToString:@"99999999999"])
        return false;
    // Valida 1o digito
    int add = 0;
    for (int i=0; i < 9; i ++)
    {
        if (i < CPF.length)
        {
            add += [[CPF substringWithRange:NSMakeRange(i, 1)] intValue]* (10 - i);
        }
    }
    int rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != [[CPF substringWithRange:NSMakeRange(9, 1)] intValue])
        return false;
    // Valida 2o digito
    add = 0;
    for (int i = 0; i < 10; i ++)
        if (i < CPF.length)
        {
            add += [[CPF substringWithRange:NSMakeRange(i, 1)] intValue] * (11 - i);
        }
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != [[CPF substringWithRange:NSMakeRange(10, 1)] intValue])
        return false;
    return true;
}
+ (BOOL)isValidCEPCode:(NSString*)CEPcode
{
    NSString *pinRegex = @"^[0-9]{3}.[0-9]{3}.[0-9]{3}.[0-9]{2}$";
    NSPredicate *pinTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pinRegex];
    
    BOOL pinValidates = [pinTest evaluateWithObject:CEPcode];
    return pinValidates;
}
+ (BOOL)isValidCPFCode:(NSString*)CPFcode
{
    NSString *pinRegex = @"^[0-9]{3}.[0-9]{3}.[0-9]{3}.[0-9]{2}$";
    NSPredicate *pinTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pinRegex];
    
    BOOL pinValidates = [pinTest evaluateWithObject:CPFcode];
    return pinValidates;
}

+(void)savePhotoToAlbum:(UIImage*)imageToSave withName:(NSString*)imageName {
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSURL *fileURL = [[tmpDirURL URLByAppendingPathComponent:imageName] URLByAppendingPathExtension:@"png"];
    NSLog(@"fileURL: %@", [fileURL path]);
    NSData *pngData = UIImagePNGRepresentation(imageToSave);
    
    [pngData writeToFile:[fileURL path] atomically:YES]; //Write the file
}
+(NSURL*)fileURLWithImage:(NSString*)strFileName{
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSURL *fileURL = [[tmpDirURL URLByAppendingPathComponent:strFileName] URLByAppendingPathExtension:@"png"];
    NSLog(@"fileURL: %@", [fileURL path]);
    
    return fileURL;
}
+(NSString*)getImageURL:(NSString*)strFileName{
    NSString *tmpDirectory = NSTemporaryDirectory();
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *cacheFiles = [fileManager contentsOfDirectoryAtPath:tmpDirectory error:&error];
    for (NSString *file in cacheFiles)
    {
        NSLog(@"%@",file);
        error = nil;
        if ([file.pathExtension isEqual:@"png"]){
            NSString *filePath = [tmpDirectory stringByAppendingPathComponent:file];
            UIImage *image = [[UIImage alloc] initWithContentsOfFile:filePath];
        }
    }
    return STRING_EMPTY;
}
+ (NSString*)getCurrentcyFromDecimal:(double)value
{
    //    NSNumberFormatter *formatterCurrency;
    //    formatterCurrency = [[NSNumberFormatter alloc] init];
    //    formatterCurrency.numberStyle = NSNumberFormatterDecimalStyle;
    ////    [formatterCurrency setGroupingSeparator:@""];
    ////    [formatterCurrency setDecimalSeparator:@"."];
    //    formatterCurrency.maximumFractionDigits = 2;
    //    [formatterCurrency setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]];
    //    NSString* textReturn = [formatterCurrency stringFromNumber:[NSNumber numberWithDouble:value]];
    //    [textReturn stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    if (![textReturn containsString:@","])
    //    {
    //        textReturn = [textReturn stringByAppendingString:@",00"];
    //    }
    NSString *strMoney = [NSString stringWithFormat:@"%.2f",value];
    strMoney = [strMoney stringByReplacingOccurrencesOfString:@"." withString:@","];
    return strMoney;
}
+ (NSAttributedString*)getAtributeTextFromString:(NSString*)text
{
    NSString* string = [text stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx;}</style>",
                                                      @"Roboto-Regular",
                                                      14.0]];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[string dataUsingEncoding:NSUnicodeStringEncoding]
                                                                    options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                              NSCharacterEncodingDocumentAttribute: @(NSUnicodeStringEncoding)}
                                                         documentAttributes:nil
                                                                      error:nil];
    return attrStr;
}

+ (NSString*) cardTypeFromCardNumber:(NSString*)cardNumber andCVV:(NSString *)cvv
{
    NSString *regVisaCard = @"^4[0-9]{12}(?:[0-9]{3})?$";
    
    NSString *regMasterCard = @"^5[1-5][0-9]{14}$";
    
    NSString *regAmex = @"^3[47][0-9]{13}$";
    
    NSString *regDiner = @"^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
    
    NSString *regElo = @"^[456](?:011|38935|51416|576|04175|067|06699|36368|36297)\\d{10}(?:\\d{2})?$";
    
    NSString *regHiperCard = @"^(38[0-9]{17}|60[0-9]{14})$";
    
    NSError *error = nil;
    
    // visa
    NSRegularExpression *reg = [NSRegularExpression regularExpressionWithPattern:regVisaCard options:0 error:&error];
    NSUInteger matches = [reg numberOfMatchesInString:cardNumber options:0 range:NSMakeRange(0, [cardNumber length])];
    if ((matches == 1) && (cvv.length == 3)) {
        return @"visa";
    }
    
    // mastercard
    reg = [NSRegularExpression regularExpressionWithPattern:regMasterCard options:0 error:&error];
    matches = [reg numberOfMatchesInString:cardNumber options:0 range:NSMakeRange(0, [cardNumber length])];
    if ((matches == 1) && (cvv.length == 3)) {
        return @"mastercard";
    }
    
    // american_express
    reg = [NSRegularExpression regularExpressionWithPattern:regAmex options:0 error:&error];
    matches = [reg numberOfMatchesInString:cardNumber options:0 range:NSMakeRange(0, [cardNumber length])];
    if ((matches == 1) && ((cvv.length == 3) || (cvv.length == 4))) {
        return @"american_express";
    }
    
    // diners_club
    reg = [NSRegularExpression regularExpressionWithPattern:regDiner options:0 error:&error];
    matches = [reg numberOfMatchesInString:cardNumber options:0 range:NSMakeRange(0, [cardNumber length])];
    if ((matches == 1) && (cvv.length == 3)) {
        return @"diners_club";
    }
    
    // elo
    reg = [NSRegularExpression regularExpressionWithPattern:regElo options:0 error:&error];
    matches = [reg numberOfMatchesInString:cardNumber options:0 range:NSMakeRange(0, [cardNumber length])];
    if ((matches == 1) && (cvv.length == 3)) {
        return @"elo";
    }
    
    // hipercard
    reg = [NSRegularExpression regularExpressionWithPattern:regHiperCard options:0 error:&error];
    matches = [reg numberOfMatchesInString:cardNumber options:0 range:NSMakeRange(0, [cardNumber length])];
    if ((matches == 1) && (cvv.length == 3)) {
        return @"hipercard";
    }
    
    return nil;
}
+ (STATUS_OPENED_ACTION)getStatusActionWithText:(NSString*)txtAction
{
    STATUS_OPENED_ACTION status = ACTION_PENDING;
    if ([txtAction isEqualToString:@"Pendente de pagamento"])
    {
        status = ACTION_OPEN;
    }
    return status;
}

+ (NSString*) stringFileNameFromDate: (NSDate*)date
{
    NSString* retString = nil;
    if (date)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setLocale:[NSLocale currentLocale]]; //for localize
        retString = [dateFormatter stringFromDate:date];
        retString = [retString stringByReplacingOccurrencesOfString: @"/" withString: @"."];
        retString = [retString stringByReplacingOccurrencesOfString: @":" withString: @"-"];
    }
    
    return retString;
}
+ (NSString*) stringFromFileSize: (NSNumber*)size
{
    if (size == nil)
        return nil;
    
    double floatSize = (double)[size longLongValue];
    if (floatSize < 1000.0f)
        return([NSString stringWithFormat:@"%1.0f Bytes",floatSize]);
    floatSize = floatSize / 1024.0;
    if (floatSize < 1000.0f)
        return([NSString stringWithFormat:@"%1.1f KB",floatSize]);
    floatSize = floatSize / 1024.0;
    if (floatSize < 1000.0f)
        return([NSString stringWithFormat:@"%1.1f MB",floatSize]);
    floatSize = floatSize / 1024.0;
    return([NSString stringWithFormat:@"%1.1f GB",floatSize]);
}

+ (NSString *)encodeToBase64String:(UIImage *)image {
   NSString *strImage = [NSString stringWithFormat:@"data:image/png;base64,%@",[UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    return strImage;
}
+ (NSString *) UUIDString {
    
    CFUUIDRef cfUUID = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString =  (NSString*)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, cfUUID));
    
    CFRelease(cfUUID);
    return uuidString;
}
@end

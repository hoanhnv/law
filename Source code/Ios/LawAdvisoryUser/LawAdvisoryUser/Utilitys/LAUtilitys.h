//
//  LAUtilitys.h
//  LawAdvisoryUser
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum STATUS_OPENED_ACTION
{
    ACTION_PENDING = 0,
    ACTION_OPEN
}STATUS_OPENED_ACTION;


@interface LAUtilitys : NSObject
#pragma mark BUTTON UTILITYS
+ (void)setButtonTextUnderLine:(UIButton*)sender;
#pragma mark STRING UTILITYS
+ (BOOL)isEmptyOrNull:(NSString*) cached;
+ (NSString*)getDefaultString:(NSString*)sourceString;
+ (BOOL)validateEmailWithString:(NSString*)checkString;
+ (BOOL)NullOrEmpty:(NSString *)strInput;
#pragma mark UITEXTFIELD UTILITYS
+ (void)setRightImage:(NSString*)imgName forTextField:(UITextField*)textField;
+ (void) setPaddingForTextField:(UITextField *) textField andPaddingLeftMargin:(CGFloat)left;
#pragma mark INTRO UTILITYS
+ (void)setIntroIsPresented;
+ (BOOL)checkIntroIsPresent;
#pragma mark DATE UTILITY
+ (NSDate*)getNSDateFromDateString:(NSString*)dateTxt withFormat:(NSString*)strFormat;
+ (NSString*)getDateStringFromNSDate:(NSDate*)date withFormat:(NSString*)strFormat;
+ (NSString*)getDateStringFromTimeInterVal:(double)time withFormat:(NSString*)strFormat;

#pragma OPTIONS
+ (ACCOUNT_STATUS)getAccountStatusFromString:(NSString*)strStatus;
+ (UIColor *)jusTapColor;
+ (BOOL)isValidCPFCode:(NSString*)CPFcode;
+ (BOOL)isValidCEPCode:(NSString*)CEPcode;
+ (void)setRightImageWhenEditing:(NSString*)imgName forTextField:(UITextField*)textField;
+ (BOOL)isValidCPF:(NSString*)CPFString;
+ (BOOL)isValidCellPhone:(NSString*)stringMobile;
+ (BOOL)isValidPhone:(NSString*)stringMobile;
+(void)savePhotoToAlbum:(UIImage*)imageToSave withName:(NSString*)imageName;
+(NSURL*)fileURLWithImage:(NSString*)strFileName;
+ (NSString*)getCurrentcyFromDecimal:(double)value;
+ (NSAttributedString*)getAtributeTextFromString:(NSString*)text;

+ (NSString*) cardTypeFromCardNumber:(NSString*)cardNumber andCVV:(NSString *)cvv;
+ (STATUS_OPENED_ACTION)getStatusActionWithText:(NSString*)txtAction;
+ (NSString*)stringFileNameFromDate:(NSDate*)date;
+ (NSString*) stringFromFileSize: (NSNumber*)size;
+ (long long)getLongDateFromDateString:(NSString*)dateTxt withFormat:(NSString*)strFormat
;
+ (NSString *)encodeToBase64String:(UIImage *)image;
+ (NSString *) UUIDString;
@end

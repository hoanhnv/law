//
//  AppDelegate.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LAMainTabbarViewController.h"
#import "LABaseViewController.h"
#import "LASignatureTese.h"
#import <UserNotifications/UserNotifications.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (AppDelegate *)sharedDelegate;
@property (nonatomic,assign) NSInteger currentID;
@property (nonatomic) LAMainTabbarViewController* mainTabbar;
@property (nonatomic) LABaseViewController* currentViewController;
@property (strong, nonatomic)  NSMutableArray *selectedTheseData;
@property (strong, nonatomic)  NSMutableArray *buyTheseData;
@property (assign, nonatomic)  BOOL isDiscountAll;
@property (assign, nonatomic)  NSInteger countThese;
@property (strong, nonatomic) NSMutableArray *allTheseData;
@property (strong, nonatomic) LASignatureTese *lstTese;
@property (nonatomic, strong) UINavigationController *nav;
@property (nonatomic,strong) NSString*strDeviceToken;

-(void)goLoginViewController;
-(void)goHomeViewController;
- (void)gotoLoginViewControllerNew;
@end


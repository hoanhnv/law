//
//  LAConstant.h
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TWITTER_CONSUMER_KEY @"gzzz7gvbSva4arm9ZxKvsIdZB"
#define TWITTER_CONSUMER_SECRET_KEY @"O6gQt4ig1hnIbNCzMxrsVIy23DZN7RAbUrbafJvn9M7Uw8YNGw"
#define kTitleHeader @"Você está utilizando %d de %d teses contratadas"
#define STRING_EMPTY    @""
#define AUDIO_TEST_LINK @"https://archive.org/details/Test_Audio_MP3_File"
//COLOR
#define MAIN_COLOR @"#4bbbfd"
#define MAIL_CONTACT   @"hoanhvp@gmail.com"
#define NAVIGATION_COLOR [UIColor colorWithRed:75.0/255.0 green:187.0/255.0 blue:253.0/255.0 alpha:1.0]
#define NAVIGATION_COLOR_GREEN [UIColor colorWithRed:46.0/255.0 green:138.0/255.0 blue:90.0/255.0 alpha:1.0]
#define COLOR_CATEGORY_HEADER_TITLE_1 [UIColor colorWithRed:40.0/255.0 green:139.0/255.0 blue:88.0/255.0 alpha:1.0]
#define COLOR_CATEGORY_HEADER_TITLE_2 [UIColor colorWithRed:255.0/255.0 green:162.0/255.0 blue:0.0/255.0 alpha:1.0]
//HEIGHT
#define HEIGHT_BIG_NAVIGATION 90.0f

#define FORGOT_PASS_SEND_EMAIL @"forgot pass mail was sent"
#define FORGOT_PASS_SEND_EMAIL_TITLE_BUTTON @"IR PARA TELA DE ACESSO"
#define REGISTER_PRESSED_MESSAGE @"Foi enviada uma nova senha para o seu email"
#define REGISTER_PRESSED_TITLE_BUTTON @"IR PARA TELA DE ACESSO"

// NOTIFICATION KEY
#define NOTIFICATION_KEY_CHANGE_ACCOUNT @"accountChange"
// SegueID
#define PushToRegister @"PushToRegister"
#define PushToLogin @"PushToLogin"
//FORMAT DATE
#define FORMAT_BIRTHDAY_STRING @"MM/dd/yyyy"
#define FORMAT_BIRTHDAY_STRING1 @"yyyy-MM-dd"
#define testText  @"is simply is dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
#define FORMAT_MM_DD_YY @"MM/dd/yyyy"
#define FORMAT_DD_MM_YY @"dd/MM/yyyy"
#define FORMAT_YYYY_MM_DD @"yyyy-MM-dd"
#define FORMAT_DATE_LONG_STYLE @"yyyy-MM-dd HH:mm:ss"

typedef enum ACCOUNT_STATUS
{
    IN_ACTIVE_ACCOUNT = 0,
    WAITING_APPROVAL_ACCOUNT,
    ACTIVE_ACCOUNT,
    UNKNOWN_ACCOUNT
}ACCOUNT_STATUS;

@interface LAConstant : NSObject

@end

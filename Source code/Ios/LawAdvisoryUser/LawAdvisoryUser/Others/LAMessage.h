//
//  LAMessage.h
//  LawAdvisoryUser
//
//  Created by Mac on 9/21/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MSG_0001    @"The response return token null"
#define MSG_0002    @"Can not signIn with google"
#define MSG_0003    @"No subjects to contact"
#define MSG_0004    @"The message must be has value"
#define MSG_005     @"Senha incorreta! \nPor favor, tente novamente.\nCaso tenha esquecido sua senha, utilize o link Esqueci minha senha."
#define MSG_006     @"Foi enviada uma nova senha para o seu email %@.\n\n Sigas as instruções para alterá-la."

#define MSG_007     @"This account is waiting approve"
#define MSG_008     @"This account is inactive"
#define MSG_009     @"Please input email"
#define MSG_010    @"Por favor, aguarde a compensação de seu boleto. Isso pode levar até 3 dias úteis.\nEm breve você poderá escolher e precificar as teses que irá atuar."

#define MSG_011     @"Nome impresso no cartão de crédito inválido"
#define MSG_012     @"Número do cartão de crédito inválido"
#define MSG_013     @"Número do cartão de crédito inválido"
#define MSG_014     @"Mês val inválido"
#define MSG_015     @"Mês val inválido"
#define MSG_016     @"Ano val inválido"
#define MSG_017     @"Ano val inválido"
#define MSG_018     @"Cód de segurança inválido"
#define MSG_019     @"Cód de segurança inválido"

#define MSG_020     @"Cartão inválido"
#define MSG_021     @"Você não possui teses para precificar."
#define MSG_022     @"Você não possui teses para precificar."
#define MSG_023     @"Assinatura atualizada com sucesso."

#define MSG_024     @"Você não possui novas ações no momento"
#define MSG_025     @"Você não possui ações pendentes no momento."
#define MSG_026     @"Você não possui ações em andamento no momento."
#define MSG_027     @"Você não possui histórico de ações no momento."

@interface LAMessage : NSObject

@end

//
//  AppDelegate.m
//  LawAdvisoryUser
//
//  Created by Long Hoang on 8/24/16.
//  Copyright © 2016 HoangMaiLong. All rights reserved.
//

#import "AppDelegate.h"
//@import UserNotifications;

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "LAAddressService.h"
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LALoginViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "MBProgressHUD.h"
#import "MainService.h"
#import "LAStatusAccountObj.h"
#import "UIView+Toast.h"
#import "LAAccountService.h"
#import "LASearchEngine.h"
#import <linkedin-sdk/LISDKCallbackHandler.h>
#import <CoreLocation/CoreLocation.h>
static NSString * const kClientID =
@"759901828094-u9ck4bouh1sdfkakqcafq5gldmcaqnvs.apps.googleusercontent.com";
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()
@end

@implementation AppDelegate
CLLocationManager *locationManager;
CLLocation *currentLocation;
//NSTimer *time;
int tickTime = 0;
int number = 0;

//SINGLETON
+ (AppDelegate *)sharedDelegate
{
    static id <UIApplicationDelegate> _sharedDelegate;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDelegate = [[UIApplication sharedApplication] delegate];
    });
    return (AppDelegate *)_sharedDelegate;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.currentID = NSNotFound;
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];  
    }
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(checkTime)
                                   userInfo:nil
                                    repeats:YES];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    [locationManager requestAlwaysAuthorization];
    // Override point for customization after application launch.
    [Fabric with:@[[Crashlytics class],[Twitter class]]];
    [GIDSignIn sharedInstance].clientID = kClientID;
    // Remove bottom line view of navigationbar
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [UINavigationBar appearance].titleTextAttributes = @{
                                                         NSFontAttributeName:[UIFont systemFontOfSize:21.0f],
                                                         NSForegroundColorAttributeName: [UIColor whiteColor]
                                                         };
    NSLog(@"Token:%@",[LADataCenter shareInstance].currentAccessToken);
    [self registerForRemoteNotification];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_SIGNATURE_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_DISTRICT_CART_ITEM];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:CURRENT_PLAN_CART_ITEM];
       

    [self goIntroViewController];
    return YES;
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Called when a notification is delivered to a foreground app.
    
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    //Called to let your app know which action was selected by the user for a given notification.
    
    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    UIBackgroundTaskIdentifier taskID = [application beginBackgroundTaskWithExpirationHandler:^{
        // Code to ensure your background processing stops executing
        // so it reaches the call to endBackgroundTask:
    }];
    
    // Put the code you want executed in the background here
    
    if (taskID != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:taskID];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                       openURL:url
                                             sourceApplication:sourceApplication
                                                    annotation:annotation]) {
        
        return  [[FBSDKApplicationDelegate sharedInstance] application:application
                                                               openURL:url
                                                     sourceApplication:sourceApplication
                                                            annotation:annotation];
    }
    else if([[GIDSignIn sharedInstance] handleURL:url
                                sourceApplication:sourceApplication
                                       annotation:annotation]){
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
    else if ([LISDKCallbackHandler shouldHandleUrl:url])
    {
        return [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    return NO;
}
#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    [[NSUserDefaults standardUserDefaults]setObject:strDevicetoken forKey:@"DEVICE_TOKEN"];
    NSLog(@"Device Token = %@",strDevicetoken);
    self.strDeviceToken = strDevicetoken;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification Information : %@",userInfo);
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10
//
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//    
//    NSLog(@"User Info = %@",notification.request.content.userInfo);
//    
//    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
//}
//
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
//    
//    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
//    completionHandler();
//}

#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        //        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        //        center.delegate = self;
        //        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        //            if( !error ){
        //                [[UIApplication sharedApplication] registerForRemoteNotifications];
        //            }
        //        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

-(void)goHomeViewController{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LAMainTabbarViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"LAMainTabbarViewControllery"];
    self.window.rootViewController = rootViewController;
    [self.window makeKeyAndVisible];
}
- (void)gotoLoginViewControllerNew
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LALoginViewController* loginVC = [storyboard instantiateViewControllerWithIdentifier:@"LALoginViewController"];
    UINavigationController* navigationView = [(UINavigationController*)[UINavigationController alloc] initWithRootViewController:loginVC];
    self.nav = navigationView;
    self.window.rootViewController = self.nav;
}
-(void)goLoginViewController{
    self.nav = (UINavigationController *) self.window.rootViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.nav pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"LALoginViewController"] animated:NO];
    self.window.rootViewController = self.nav;
}
-(void)goIntroViewController{
    self.nav = (UINavigationController *) self.window.rootViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.nav pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"LAIntroViewController"] animated:NO];
    self.window.rootViewController = self.nav;
}
#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        if([CLLocationManager isMonitoringAvailableForClass:CLBeaconRegion.self]){
            if([CLLocationManager isRangingAvailable]){
                [manager startUpdatingLocation];
            }
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    if(locations[0] != nil){
        currentLocation = locations[0];
    }
}
-(void)checkTime{
    if(number == 0 && currentLocation != nil){
        [self sendLocation:currentLocation];
        number++;
    }
    if(tickTime == 3600*number && currentLocation != nil){
        [self sendLocation:currentLocation];
        number++;
    }
    tickTime++;
}
-(void)sendLocation:(CLLocation*)location{
    NSDictionary *param = @{@"lat":[NSNumber numberWithLong:location.coordinate.latitude] ,
                            @"lng": [NSNumber numberWithLong:location.coordinate.longitude]};
    [MainService postLocation:param withBlock:^(NSInteger errorCode, NSString *message, id data){
        if([data objectForKey:@"success"]){
            return ;
        }
    }];
}

@end
